{*

* 2007-2017 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2017 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*}



<div class="ps-emailsubscription-block">
<h5>Suscríbete</h5>
<div class="col-md content-subscription">
<p class="text-info-subs"> {l s='Quieres más información? Suscríbete.' d='Shop.Forms.Labels'} <p>
    <form action="{url entity=index params=['fc' => 'module', 'module' => 'iqitemailsubscriptionconf', 'controller' => 'subscription']}"

          method="post">

                <div class="input-group newsletter-input-group ">

                    <input

                            name="email"

                            type="email"

                            value="{$value}"

                            class="form-control input-subscription"

                            placeholder="{l s='Ingresar Correo' d='Shop.Forms.Labels'}"

                            aria-label="{l s='Your email address' d='Shop.Forms.Labels'}"

                    >

                    <button

                            class="btn btn-primary btn-subscribe btn-iconic"

                            name="submitNewsletter"

                            type="submit"

                            aria-label="{l s='Subscribe' d='Shop.Theme.Actions'}">

                    {l s='Enviar' d='Shop.Forms.Labels'}</button>

                    

                </div>

        {if isset($id_module)}

            <div class="mt-2 text-muted"> {hook h='displayGDPRConsent' id_module=$id_module}</div>

        {/if}

                <input type="hidden" name="action" value="0">

    </form>
<p class="text-info-subs"> <span> {l s='Al enviar acepto los términos y condiciones.' d='Shop.Forms.Labels'}</span> <p>

</div>
<div class="payment-method">
<ul class="d-flex">

<li>
<div class="t-payment"><img src="/img/cms/iconos/visa-pay.png" alt="Visa">
<div></div>
</div>
</li>
<li>
<div class="t-payment"><img src="/img/cms/iconos/mastercard.png" alt="Masterd Card"></div>
</li>
<li>
<div class="t-payment"><img src="/img/cms/iconos/american-express.png" alt="American Express"></div>
</li>
<li>
<div class="t-payment"><img src="/img/cms/iconos/paypal.png" alt="Paypal"></div>
</li>
</ul>
<div class="text-center mt-4 ComplaintBookFooter">
<a href="/content/12-libro-de-reclamaciones"><img src="/img/cms/libro-de-reclamaciones-aghaso-white.png" alt="Libro de Reclamaciones"></a>
</div>

</div>
</div>



