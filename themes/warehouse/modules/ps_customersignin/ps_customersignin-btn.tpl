<div id="header-user-btn" class="col col-auto header-btn-w header-user-btn-w">

    {if $logged}

        {if isset($iqitTheme.h_user_dropdown) && $iqitTheme.h_user_dropdown}

            <div class="dropdown">

            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"

               class="header-btn header-user-btn">

                <!--i class="fa fa-user fa-fw icon" aria-hidden="true"></i> --ORIGIN-->

                <img class="fa-btn-login svg-menu" src="/img/cms/svg/login.svg">

                <span class="title">{$customer.firstname|truncate:15:'...'}</span>

            </a>

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{$urls.pages.identity}">

                        <i class="fa fa-user fa-fw" aria-hidden="true"></i> 

                        {l s='Information' d='Shop.Theme.Customeraccount'}

                    </a>

                    {if $customer.addresses|count}

                        <a class="dropdown-item" href="{$urls.pages.addresses}">

                            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>

                            {l s='Addresses' d='Shop.Theme.Customeraccount'}

                        </a>

                    {else}

                        <a class="dropdown-item" href="{$urls.pages.address}">

                            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>

                            {l s='Add first address' d='Shop.Theme.Customeraccount'}

                        </a>

                    {/if}

                    {if !$configuration.is_catalog}

                        <a class="dropdown-item" href="{$urls.pages.history}">

                            <i class="fa fa-history fa-fw" aria-hidden="true"></i>

                            {l s='Order history and details' d='Shop.Theme.Customeraccount'}

                        </a>

                    {/if}
                    
                     <a class="dropdown-item" href="{url entity='module' name='iqitwishlist' controller='view'}" >
                             
                                
                            <span class="link-item">
                            <i class="fa fa-heart-o fa-fw" aria-hidden="true"></i>
                             {l s='My wishlist'  d='Shop.Theme.Customeraccount'}
                            </span>
                        </a>

                    {if !$configuration.is_catalog}

                        <a class="dropdown-item"href="{$urls.pages.order_slip}">

                            <i class="fa fa-file-o fa-fw" aria-hidden="true"></i>

                            {l s='Credit slips' d='Shop.Theme.Customeraccount'}

                        </a>

                    {/if}



                    {if $configuration.voucher_enabled && !$configuration.is_catalog}

                        <a class="dropdown-item" href="{$urls.pages.discount}">

                            <i class="fa fa-tags fa-fw" aria-hidden="true"></i>

                            {l s='Vouchers' d='Shop.Theme.Customeraccount'}

                        </a>

                    {/if}


                        <a class="dropdown-item" href="{$link->getModuleLink('idxrprivatedownloads','downloads')|escape:'htmlall':'UTF-8'}">

                            <i class="fa fa-download" aria-hidden="true"></i>
                            
                            {l s='Archivos Autocad' d='Shop.Theme.Customeraccount'}
                        </a>

                    {if $configuration.return_enabled && !$configuration.is_catalog}

                        <a class="dropdown-item" href="{$urls.pages.order_follow}">

                            <i class="fa fa-undo fa-fw"" aria-hidden="true"></i>

                            {l s='Merchandise returns' d='Shop.Theme.Customeraccount'}

                        </a>

                    {/if}

                    <a class="dropdown-item" href="{url entity='index' params=['mylogout' => '']}">

                        <i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>

                        {l s='Sign out' d='Shop.Theme.Actions'}

                    </a>
                    

                </div>

            </div>

        {else}

            <a href="{$my_account_url}"

               title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"

               rel="nofollow" class="header-btn header-user-btn">

                <!--i class="fa fa-user fa-fw icon" aria-hidden="true"></i> --ORIGIN-->

                    <img class="fa-btn-login svg-menu" src="/img/cms/svg/login.svg">

                <span class="title">{$customer.firstname|truncate:15:'...'}</span>

            </a>

        {/if}

    {else}

        <a href="{$my_account_url}"

           title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"

           rel="nofollow" class="header-btn header-user-btn">

            <!--i class="fa fa-user fa-fw icon" aria-hidden="true"></i>--ORIGIN-->

                <img class="fa-btn-login svg-menu" src="/img/cms/svg/login.svg">

            <span class="title">{l s='Ingresar' d='Shop.Theme.Actions'}</span>

        </a>

    {/if}

</div>



















