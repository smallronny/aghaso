﻿/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */


$('#showStore').click(function(e) {

  if( $('#more-store').is(":visible") ) {
    $('#more-store').css('display', 'none');
     $(this).text('Ver tiendas');
            e.preventDefault(); 
  } else {
    $('#more-store').css('display', 'inline-flex');
     $(this).text('Cerrar');
            e.preventDefault();
  }
});

  

  
/* Menu Mobile close */
$( document ).ready(function() {
 $('.menu-close').click(function() {
      $('#_mobile_iqitmegamenu-mobile').collapse('toggle');
    });
 });


/**** HERO CATEGORY-PRODUCT *****/
$( document ).ready(function() {
    var html = `
  <div class="row align-items-center justify-content-center ">
   <div class="col-md-4">
      <div class="text-enlaces">
         <div class="">
            <h1 class="blue">Productos</h1>
         </div>
         <div class="hero-text">
            <p>Te ofrecemos productos con diferentes capacidades para diferentes necesidades </p>
         </div>
      </div>
   </div>
   <div class="col-md-8">
      <img class="d-none d-sm-none d-md-block w-100" src="/img/cms/hero-producto-category.jpg" alt="Todos nuestros productos">  
      <img class="d-block d-sm-block d-md-none w-100" src="/img/cms/mobile/producto-mobile-img.jpg" alt="Todos nuestros productos">  
   </div>
</div>
`;
$( ".category-todos-nuestros-productos nav.breadcrumb" ).html(html);
});

/**** HERO NOVEDADES *****/
$( document ).ready(function() {
    var html = `
  <div class="row align-items-center justify-content-center ">
   <div class="col-md-3">
      <div class="text-enlaces">
         <div class="">
            <h1 class="blue">Productos</h1>
         </div>
         <div class="hero-text">
            <p>"Lo mejor en casa est&aacute; con Aghaso" Secadoras - Termas - Cocinas</p>
         </div>
      </div>
   </div>
   <div class="col-md-9">
      <img class="d-none d-sm-none d-md-block w-100" src="/img/cms/hero-novedades-aghaso.jpg" alt="Todos nuestros productos">  
      <img class="d-block d-sm-block d-md-none w-100" src="/img/cms/mobile/novedades-mobile-aghaso.jpg" alt="Todos nuestros productos">  
   </div>
</div>
`;
$( "#module-prestablog-novedades nav.breadcrumb" ).html(html);
});




/**** HERO NUESTRAS TIENDAS *****/
$( document ).ready(function() {
    var html = `
  <div class="row align-items-center justify-content-center ">
   <div class="col-md-3 d-flex justify-content-center">
      <div class="text-enlaces my-3">
         <div class="">
            <h1 class="blue d-none d-sm-none d-md-block w-100">Nuestras<br>Tiendas</h1>
            <h1 class="blue d-block d-sm-block d-md-none w-100">Nuestras Tiendas</h1>
         </div>
      </div>
   </div>
   <div class="col-md-9">
      <img class="d-none d-sm-none d-md-block w-100" src="/img/cms/nuestras-tiendas-hero-aghaso.png" alt="Nuestras Tiendas">  
      <img class="d-block d-sm-block d-md-none w-100" src="/img/cms/mobile/nuestras-tiendas-mobil.jpg" alt="Nuestras Tiendas">  
   </div>
</div>
`;
$( "#module-kbstorelocatorpickup-stores nav.breadcrumb" ).html(html);
});

/**** PROXIMANENTE - MAS TIENDAS *****/
$( document ).ready(function() {
    var html = `
<div class="row my-3">
<div class="col-md col-12 p-0">
<div class=" "><img src="/img/cms/cta-aghaso-nuevas-tiendas.gif" class="card-img" alt="...">
</div>
</div>
</div>
`;
$( "#ComingSoonStore" ).html(html);
});


/**** SIMULADOR AHORRO *****/
$( document ).ready(function() {
    var html = `
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="luz-tab" data-toggle="tab" href="#luz" role="tab" aria-controls="luz" aria-selected="true">Luz</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="gas-tab" data-toggle="tab" href="#gas" role="tab" aria-controls="gas" aria-selected="false">Gas</a>
  </li>
  
</ul>
<div class="tab-content" id="simulador">
  <div class="tab-pane fade show active" id="luz" role="tabpanel" aria-labelledby="luz-tab"><p class="blue"><strong>Seleccionar el producto que desee simular y las horas de uso:</strong></p>
 

<div class="form-group">
   <small>Selecionar producto<span class="green">*</span></small> 
    <select class="form-control" id="exampleFormControlLuz">
      <option>Secadora el&eacute;ctrica 8kg</option>
      <option>Lavadora el&eacute;ctrica silver</option>
      <option>Lavadora el&eacute;crica 8kg</option>
      <option>Refrigeradora 249L</option>
      <option>Campana Extractora 60cm</option>
    </select>
   
</div>

 <div class="row align-content-center">
  	<div class="col-md-4">
  		<div class="form-group row">
  <small class="col-md-12">horas de uso<span class="green">*</span></small>
  <div class="col-10">
    <input class="form-control" type="number" value="2" id="">
  </div>
</div>
  	</div>

<div class="col-md-8">
<small><span class="green">(*)</span>cálculo estimo en una facturaci&oacute;n mensual</small>
</div>
 
</div>
 <button type="submit" class="btn btn-primary btn-block mb-2 ">Calcular</button>
</div>

<!-- FORMULARIO GAS -->
  <div class="tab-pane fade" id="gas" role="tabpanel" aria-labelledby="gas-tab">

<p class="blue"><strong>Seleccionar el producto que desee simular y las horas de uso:</strong></p>

<div class="form-group">
    <small>Selecionar producto<span class="green">*</span></small> 
    <select class="form-control" id="exampleFormControlGas">
      <option>Encimera vidrio 60cm</option>
      <option>Terma 5.5L</option>
      <option>Secadora a gas 19k</option>
      <option>Terma 12L Flujo Continuo </option>
    </select>
  </div>

 <div class="row align-content-center align-items-center">
  	<div class="col-md-4">
  		<div class="form-group row">
  <small class="col-md-12">horas de uso<span class="green">*</span></small>
  <div class="col-10">
    <input class="form-control" type="number" value="2" id="">
  </div>
</div>
  	</div>

<div class="col-md-8">
<small><span class="green">(*)</span>cálculo estimo en una facturaci&oacute;n mensual</small>
</div>

</div>
 <button type="" class="btn btn-primary btn-block mb-2 ">Calcular</button>
</div>

</div>

`;
$( "#simulador-ahorro" ).html(html);
});


/* INTEGRACIÓN DE FORMULARIOS CON CRM FIDELITYTOOLS */
$(document).ready(function () {
    $("#wpcfu-f3-o1>form").submit(function () {
        $.enviofrm(this,'https://tienda.aghaso.com/ft/integ-contacto.php');
    });
    
    $("#wpcfu-f4-o1>form").submit(function () {
        $.enviofrm(this,'https://tienda.aghaso.com/ft/integ-inmobiliaria.php');
    });
        
    $("#wpcfu-f5-o1>form").submit(function () {
        $.enviofrm(this,'https://tienda.aghaso.com/ft/integ-financiamiento.php');
    });
});

jQuery.enviofrm = function (obj,link) {
    var src = $(obj).serialize();
    $.ajax({
        url: link,
        data: src,
        type: 'post',
        success: function (data) {
            console.log(src)
        },
        error: function () {
            console.log("No se ha podido obtener la informaci�n");
        }
    });
}
 



























