<?php
/* Smarty version 3.1.33, created on 2020-10-01 01:50:42
  from 'module:iqitcompareviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f757c4252a2e3_59891385',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd4a200912ea9e133f46cf39b7eadc37ea7dd3d2f' => 
    array (
      0 => 'module:iqitcompareviewstemplates',
      1 => 1598938146,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f757c4252a2e3_59891385 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="iqitcompare-notification" class="ns-box ns-effect-thumbslider ns-text-only">
    <div class="ns-box-inner">
        <div class="ns-content">
            <span class="ns-title"><i class="fa fa-check" aria-hidden="true"></i> <strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product added to compare.','mod'=>'iqitcompare'),$_smarty_tpl ) );?>
</strong></span>
        </div>
    </div>
</div><?php }
}
