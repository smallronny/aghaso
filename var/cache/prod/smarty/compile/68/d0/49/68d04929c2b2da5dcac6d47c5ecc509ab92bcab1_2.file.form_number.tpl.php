<?php
/* Smarty version 3.1.33, created on 2020-10-01 01:56:02
  from '/home/desarrollo1webti/public_html/aghaso/modules/ets_cfultimate/views/templates/hook/form_number.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f757d8228a509_51166278',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '68d04929c2b2da5dcac6d47c5ecc509ab92bcab1' => 
    array (
      0 => '/home/desarrollo1webti/public_html/aghaso/modules/ets_cfultimate/views/templates/hook/form_number.tpl',
      1 => 1600098673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f757d8228a509_51166278 (Smarty_Internal_Template $_smarty_tpl) {
?><span class="ets_cfu_form-control-wrap <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['html_class']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php if (isset($_smarty_tpl->tpl_vars['atts']->value['type']) && $_smarty_tpl->tpl_vars['atts']->value['type'] == 'number') {?>access_numer<?php }?>">
<?php if ($_smarty_tpl->tpl_vars['atts']->value['type'] == 'range' && isset($_smarty_tpl->tpl_vars['atts']->value['min'])) {?>
    <span class="rang-min"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['atts']->value['min'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
<?php }?>
    <?php if ($_smarty_tpl->tpl_vars['atts']->value['type'] == 'range' && isset($_smarty_tpl->tpl_vars['atts']->value['max'])) {?>
        <span class="rang-max"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['atts']->value['max'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['atts']->value['type'] == 'range') {?>
        <span class="rang-value">&nbsp;</span>
    <?php }?>
<input 
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['atts']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
        <?php if ($_smarty_tpl->tpl_vars['item']->value) {?>
            <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['key']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
        <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
/>
    <?php if (isset($_smarty_tpl->tpl_vars['atts']->value['type']) && $_smarty_tpl->tpl_vars['atts']->value['type'] == 'number') {?>
        <span class="number_select">
            <span class="number_plus"></span>
            <span class="number_minus"></span>
        </span>
    <?php }
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['validation_error']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span><?php }
}
