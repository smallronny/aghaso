<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb74e3d14_96135136',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '685991904112b3316b5c5000e85614bdf0bcfc10' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb74e3d14_96135136 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_smarty_tpl->tpl_vars['truncate']->value)) {?>
    <?php $_smarty_tpl->_assignInScope('truncate', 65);
}?>

<div class="product-description">
    <a class="lapo-view" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->product_link,'html' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->name,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" data-id-association="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_association), ENT_QUOTES, 'UTF-8');?>
" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
">
        <span><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['additional_product']->value->description_short),$_smarty_tpl->tpl_vars['truncate']->value,'...' ));?>
</span>
    </a>
</div><?php }
}
