<?php
/* Smarty version 3.1.33, created on 2020-10-08 22:10:46
  from '/home/desarrollo1webti/public_html/aghaso/modules/psproductcountdownpro/views/templates/hook/pspc.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f7fd4b68841b0_25542590',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6afc6433edd7a70156c870790f4d1864c5cf4ddd' => 
    array (
      0 => '/home/desarrollo1webti/public_html/aghaso/modules/psproductcountdownpro/views/templates/hook/pspc.tpl',
      1 => 1601583596,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f7fd4b68841b0_25542590 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="pspc-wrp <?php if ($_smarty_tpl->tpl_vars['pspc_hook']->value) {?>pspc_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_hook']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?> <?php if (strpos($_smarty_tpl->tpl_vars['pspc_product_list_position']->value,'over_img') !== false) {?>pspc-wrp-over-img<?php }?> pspc-valign-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_vertical_align']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
    <div class="psproductcountdown pspc-inactive pspc<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_psv']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

        <?php if ($_smarty_tpl->tpl_vars['pspc_ipa']->value) {?>pspc-combi-wrp pspc-cw-<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['pspc_ipa']->value), ENT_QUOTES, 'UTF-8');
}?>
        <?php if (strpos($_smarty_tpl->tpl_vars['pspc_product_list_position']->value,'over_img') !== false) {?>pspc-over-img<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pspc_compact_view']->value && !$_smarty_tpl->tpl_vars['pspc_too_long']->value) {?>compact_view<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pspc_show_promo_text']->value || $_smarty_tpl->tpl_vars['pspc_too_long']->value) {?>pspc-show-promo-text<?php } else { ?>pspc-hide-promo-text<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pspc_background_image']->value) {?>background_image<?php }?> pspc-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_theme']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

        <?php if ($_smarty_tpl->tpl_vars['pspc_too_long']->value) {?>pspc-long<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pspc_colon']->value) {?>pspc-show-colon<?php }?>
        pspc-highlight-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_highlight']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
         data-to="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc']->value->to_time,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
         data-name="<?php if ($_smarty_tpl->tpl_vars['pspc']->value->name) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc']->value->name,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');
} elseif ($_smarty_tpl->tpl_vars['pspc_promo_text']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_promo_text']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Offer ends in:','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );
}?>"
         data-personal="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['pspc']->value->personal), ENT_QUOTES, 'UTF-8');?>
"
         data-personal-hours="<?php echo htmlspecialchars(floatval($_smarty_tpl->tpl_vars['pspc']->value->personal_hours), ENT_QUOTES, 'UTF-8');?>
"
         data-personal-hours-restart="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc']->value->hours_restart,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
         data-id-countdown="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['pspc']->value->id), ENT_QUOTES, 'UTF-8');?>
"
    >
        <div class="pspc-main days-diff-<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['pspc_days_diff']->value), ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['pspc_days_diff']->value >= 100) {?>pspc-diff-m100<?php }?>">
            <div class="pspc-offer-ends">
                <?php if ($_smarty_tpl->tpl_vars['pspc_too_long']->value && $_smarty_tpl->tpl_vars['pspc']->value->to_date) {?>
                    <?php if ($_smarty_tpl->tpl_vars['pspc']->value->name) {?>
                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc']->value->name,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                    <?php } else { ?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Offer ends on %s','sprintf'=>array($_smarty_tpl->tpl_vars['pspc']->value->to_date),'mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>

                    <?php }?>
                <?php } else { ?>
                    <?php if ($_smarty_tpl->tpl_vars['pspc']->value->name) {?>
                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc']->value->name,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['pspc_promo_text']->value) {?>
                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_promo_text']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                    <?php } else { ?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Offer ends in:','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>

                    <?php }?>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    if (typeof pspc_initCountdown === 'function') {
        pspc_initCountdown();
    }
<?php echo '</script'; ?>
>
<?php }
}
