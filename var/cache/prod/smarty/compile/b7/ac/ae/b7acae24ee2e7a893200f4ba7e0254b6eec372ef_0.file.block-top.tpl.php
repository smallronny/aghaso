<?php
/* Smarty version 3.1.33, created on 2021-01-13 16:13:06
  from '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/block-top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff6262625816_58061808',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7acae24ee2e7a893200f4ba7e0254b6eec372ef' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/block-top.tpl',
      1 => 1600098673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fff6262625816_58061808 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
    var ets_cfu_default_lang = <?php echo intval($_smarty_tpl->tpl_vars['ets_cfu_default_lang']->value);?>
;
    var ets_cfu_is_updating = <?php echo intval($_smarty_tpl->tpl_vars['ets_cfu_is_updating']->value);?>
;
    var PS_ALLOW_ACCENTED_CHARS_URL = true;
    var detele_confirm = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Do you want to delete?','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_msg_email_required = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email is required.','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_msg_email_invalid = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email %s is invalid.','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_msg_email_exist = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This email is exist. Please enter another email address.','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_label_delete = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_copy_msg = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Copied','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_delete_msg = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Do you want to delete this item?','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_btn_back_label = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Back','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_btn_close_label = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Close','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_add_input_field = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add input field:','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_edit_input_field = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit input field:','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_add_row_title = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add row','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_edit_row_title = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit row','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
";
    var ets_cfu_languages = <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'json_encode' ][ 0 ], array( $_smarty_tpl->tpl_vars['languages']->value ));?>
;
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['ets_cfu_js_dir_path']->value,'quotes','UTF-8' ));?>
contact_form7_admin.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['ets_cfu_js_dir_path']->value,'quotes','UTF-8' ));?>
other.js"><?php echo '</script'; ?>
>
<div class="cfu-top-menu">
    <ul>
        <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateDashboard') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateDashboard',true),'html','UTF-8' ));?>
"><i class="icon icon-desktop"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Dashboard','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</a></li>
        <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateContactForm' || isset($_REQUEST['etsCfuEditContact']) || isset($_REQUEST['etsCfuAddContact'])) {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
"><i class="icon icon-envelope-o"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact forms','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</a></li>
        <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateMessage') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateMessage',true),'html','UTF-8' ));?>
"><i class="icon icon-comments"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Messages','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
&nbsp;<span class="count_messages <?php if (!$_smarty_tpl->tpl_vars['count_messages']->value) {?>hide<?php }?>"><?php echo intval($_smarty_tpl->tpl_vars['count_messages']->value);?>
</span></a></li>
        <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateStatistics') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateStatistics',true),'html','UTF-8' ));?>
"><i class="icon icon-area-chart"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Statistics','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</a></li>
        <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateIpBlacklist') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateIpBlacklist',true),'html','UTF-8' ));?>
"><i class="icon icon-user-times"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'IP & Email blacklist','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</a></li>
        <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateEmail' || $_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateImportExport' || $_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateIntegration') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateEmail',true),'html','UTF-8' ));?>
"><i class="icon icon-cog"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Settings','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</a></li>
        <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminContactFormUltimateHelp') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateHelp',true),'html','UTF-8' ));?>
"><i class="icon icon-question-circle"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Help','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</a></li>
        <?php if (isset($_smarty_tpl->tpl_vars['intro']->value) && $_smarty_tpl->tpl_vars['intro']->value) {?>
        <li class="li_othermodules ">
            <a class="<?php if (isset($_smarty_tpl->tpl_vars['refsLink']->value) && $_smarty_tpl->tpl_vars['refsLink']->value) {?>refs_othermodules<?php } else { ?>link_othermodules<?php }?>" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['other_modules_link']->value,'html','UTF-8' ));?>
" <?php if (isset($_smarty_tpl->tpl_vars['refsLink']->value) && $_smarty_tpl->tpl_vars['refsLink']->value) {?>target="_blank" <?php }?>>
                <span class="tab-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Other modules','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</span>
                <span class="tab-sub-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Made by ETS-Soft','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</span>
            </a></li>
        <?php }?>
    </ul>
</div>
<div class="cfu-top-menu-height"></div>
<?php }
}
