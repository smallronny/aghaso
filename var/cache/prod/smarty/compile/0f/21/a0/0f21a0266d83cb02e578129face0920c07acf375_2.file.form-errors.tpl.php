<?php
/* Smarty version 3.1.33, created on 2020-10-01 01:51:55
  from '/home/desarrollo1webti/public_html/aghaso/themes/warehousechild/templates/_partials/form-errors.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f757c8b3e3529_35131517',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0f21a0266d83cb02e578129face0920c07acf375' => 
    array (
      0 => '/home/desarrollo1webti/public_html/aghaso/themes/warehousechild/templates/_partials/form-errors.tpl',
      1 => 1599157620,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f757c8b3e3529_35131517 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
if (count($_smarty_tpl->tpl_vars['errors']->value)) {?>
  <div class="help-block">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10680420235f757c8b3e1d05_46479456', 'form_errors');
?>

  </div>
<?php }
}
/* {block 'form_errors'} */
class Block_10680420235f757c8b3e1d05_46479456 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'form_errors' => 
  array (
    0 => 'Block_10680420235f757c8b3e1d05_46479456',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value, 'error');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
?>
          <li class="alert alert-danger"><?php echo nl2br($_smarty_tpl->tpl_vars['error']->value);?>
</li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </ul>
    <?php
}
}
/* {/block 'form_errors'} */
}
