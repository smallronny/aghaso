<?php
/* Smarty version 3.1.33, created on 2021-01-13 23:13:40
  from '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/form_text.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fffc4f40518a1_88865170',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a9c13a896e4de40995f7cb75e0ee6768825bc756' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/form_text.tpl',
      1 => 1600098673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fffc4f40518a1_88865170 (Smarty_Internal_Template $_smarty_tpl) {
?><span class="ets_cfu_form-control-wrap <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['html_class']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
<input 
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['atts']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
        <?php if ($_smarty_tpl->tpl_vars['item']->value) {?>
            <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['key']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
        <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
 />
    <?php if (isset($_smarty_tpl->tpl_vars['atts']->value['maxlength']) && $_smarty_tpl->tpl_vars['atts']->value['maxlength']) {?>
        <span class="number_max"><span class="index_type">0</span>/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['atts']->value['maxlength'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['show_hide_password']->value) && $_smarty_tpl->tpl_vars['show_hide_password']->value) {?>
        <span class="ets_cfu_show_hide"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
    <?php }?>
    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['validation_error']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
<?php }
}
