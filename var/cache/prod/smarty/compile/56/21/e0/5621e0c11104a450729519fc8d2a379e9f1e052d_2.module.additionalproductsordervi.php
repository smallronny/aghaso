<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb752b3d8_61059142',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5621e0c11104a450729519fc8d2a379e9f1e052d' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb752b3d8_61059142 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="lapo-tooltip-content" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
"
    data-id-product-attribute="<?php if (isset($_smarty_tpl->tpl_vars['additional_product']->value->id_product_attribute)) {
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_product_attribute), ENT_QUOTES, 'UTF-8');
} else { ?>0<?php }?>">
    <div id="lapo-tooltip-product-<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
-<?php if (isset($_smarty_tpl->tpl_vars['additional_product']->value->id_product_attribute)) {
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_product_attribute), ENT_QUOTES, 'UTF-8');
} else { ?>0<?php }?>"
        class="lapo-tooltip-content-product">
        <div class="lapo-tooltip-product-image">
            <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->image_link,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                 width="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['image_width'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                 height="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['image_height'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                 alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->name,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
        </div>
        <div class="lapo-tooltip-product-description">
            <span><?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['additional_product']->value->description_short);?>
</span>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['additional_product']->value->has_attributes && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['combinations_all'] && isset($_smarty_tpl->tpl_vars['product_attribute_designation']->value)) {?>
            <div class="lapo-tooltip-product-attribute-designation">
                <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_attribute_designation']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

            </div>
        <?php }?>
    </div>
</div>
<?php }
}
