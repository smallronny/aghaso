<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:psproductcountdownproview' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb75d16a5_64697244',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e296920c3644bd21f7307f907a65539b401360cc' => 
    array (
      0 => 'module:psproductcountdownproview',
      1 => 1601583596,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb75d16a5_64697244 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '10333144126000ceb75cd2c7_92864142';
if (isset($_smarty_tpl->tpl_vars['pspc_blocks']->value) && count($_smarty_tpl->tpl_vars['pspc_blocks']->value)) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pspc_blocks']->value, 'pspc_block');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['pspc_block']->value) {
?>
        <?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['pspc_block_tpl_file']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array('pspc_products'=>$_smarty_tpl->tpl_vars['pspc_block']->value->getProductsFront()), 0, true);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
}
