<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:28:28
  from '/home/renatonunez/public_html/tienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007f3c4f5ae5_74226971',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f58b18589ee248027b8db3061b56745747b02363' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-search.tpl',
      1 => 1599962465,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007f3c4f5ae5_74226971 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<div class="block-categories">
	<h4 class="title_block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search on blog','mod'=>'prestablog'),$_smarty_tpl ) );?>
</h4>
	<div class="block_content">
		<form action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );?>
" method="post" id="prestablog_bloc_search">
			<input id="prestablog_search" class="search_query form-control ac_input" type="text" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search on blog','mod'=>'prestablog'),$_smarty_tpl ) );?>
" name="prestablog_search" autocomplete="off">
			<button class="btn btn-default button-search" type="submit">
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search on blog','mod'=>'prestablog'),$_smarty_tpl ) );?>
</span>
			</button>
			<div class="clear"></div>
		</form>
	</div>
</div>
<!-- /Module Presta Blog -->
<?php }
}
