<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:28:26
  from 'module:prestablogviewstemplatesf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007f3aa659e0_40752998',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '92a1050667be6cb00bb293996b364401505adc59' => 
    array (
      0 => 'module:prestablogviewstemplatesf',
      1 => 1599962465,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007f3aa659e0_40752998 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Module Presta Blog -->
<!-- Pagination -->
<?php if (isset($_smarty_tpl->tpl_vars['Pagination']->value['NombreTotalPages']) && $_smarty_tpl->tpl_vars['Pagination']->value['NombreTotalPages'] > 1) {?>
	<div class="prestablog_pagination">
		<?php if ($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] > 1) {?>
				<?php if ($_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value) {?>
		<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'start'=>$_smarty_tpl->tpl_vars['Pagination']->value['StartPrecedent'],'p'=>$_smarty_tpl->tpl_vars['Pagination']->value['PagePrecedente'],'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">&lt;&lt;</a>
				<?php } else { ?>
			<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">&lt;&lt;</a>
<?php }?>
		<?php } else { ?>
			<span class="disabled">&lt;&lt;</span>
		<?php }?>

		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Pagination']->value['PremieresPages'], 'value_page', false, 'key_page');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key_page']->value => $_smarty_tpl->tpl_vars['value_page']->value) {
?>
			<?php if (($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] == $_smarty_tpl->tpl_vars['key_page']->value) || (!$_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] && $_smarty_tpl->tpl_vars['key_page']->value == 1)) {?>
				<span class="current"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['key_page']->value), ENT_QUOTES, 'UTF-8');?>
</span>
			<?php } else { ?>
				<?php if ($_smarty_tpl->tpl_vars['key_page']->value == 1) {?>
				<?php if ($_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value) {?>
					<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['key_page']->value), ENT_QUOTES, 'UTF-8');?>
</a>
				<?php } else { ?>
				<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['key_page']->value), ENT_QUOTES, 'UTF-8');?>
</a><?php }?>
				<?php } else { ?>
					<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'start'=>$_smarty_tpl->tpl_vars['value_page']->value,'p'=>$_smarty_tpl->tpl_vars['key_page']->value,'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
					><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['key_page']->value), ENT_QUOTES, 'UTF-8');?>
</a>
				<?php }?>
			<?php }?>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		<?php if (isset($_smarty_tpl->tpl_vars['Pagination']->value['Pages']) && $_smarty_tpl->tpl_vars['Pagination']->value['Pages']) {?>
			<span class="more">...</span>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Pagination']->value['Pages'], 'value_page', false, 'key_page');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key_page']->value => $_smarty_tpl->tpl_vars['value_page']->value) {
?>
				<?php if (!in_array($_smarty_tpl->tpl_vars['value_page']->value,$_smarty_tpl->tpl_vars['Pagination']->value['PremieresPages'])) {?>
					<?php if (($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] == $_smarty_tpl->tpl_vars['key_page']->value) || (!$_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] && $_smarty_tpl->tpl_vars['key_page']->value == 1)) {?>
						<span class="current"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['key_page']->value), ENT_QUOTES, 'UTF-8');?>
</span>
					<?php } else { ?>
						<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'start'=>$_smarty_tpl->tpl_vars['value_page']->value,'p'=>$_smarty_tpl->tpl_vars['key_page']->value,'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
						><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['key_page']->value), ENT_QUOTES, 'UTF-8');?>
</a>
					<?php }?>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] < $_smarty_tpl->tpl_vars['Pagination']->value['NombreTotalPages']) {?>
			<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'start'=>$_smarty_tpl->tpl_vars['Pagination']->value['StartSuivant'],'p'=>$_smarty_tpl->tpl_vars['Pagination']->value['PageSuivante'],'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">&gt;&gt;</a>
		<?php } else { ?>
			<span class="disabled">&gt;&gt;</span>
		<?php }?>
	</div>
<?php }?>
<!-- /Pagination -->
<!-- /Module Presta Blog -->
<?php }
}
