<?php
/* Smarty version 3.1.33, created on 2020-10-01 01:56:02
  from '/home/desarrollo1webti/public_html/aghaso/modules/ets_cfultimate/views/templates/hook/contact-form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f757d822f0b68_13797199',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '557e8c925afabcaf3b04948f920c4c37a44f7ea1' => 
    array (
      0 => '/home/desarrollo1webti/public_html/aghaso/modules/ets_cfultimate/views/templates/hook/contact-form.tpl',
      1 => 1600098673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f757d822f0b68_13797199 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['open_form_by_button']->value) {?>
<span id="button_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->unit_tag,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="ctf_click_open_contactform7 btn btn-primary" data-id="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->unit_tag,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->button_label,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
<div class="ctf-popup-wapper" id="ctf-popup-wapper-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->unit_tag,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
    <div class="ctf-popup-table">
        <div class="ctf-popup-tablecell">
            <div class="ctf-popup-content">
                <div class="ctf_close_popup">close</div><?php }?>
                <?php if (!trim($_smarty_tpl->tpl_vars['form_elements']->value)) {?><p class="ets_cfu_alert alert alert-warning"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact form is empty','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</p><?php } else { ?>
                <div role="form" class="wpcfu<?php if ($_smarty_tpl->tpl_vars['displayHook']->value) {?> hook<?php }?>" id="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->unit_tag,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" dir="ltr" data-id="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['contact_form']->value->id), ENT_QUOTES, 'UTF-8');?>
">
                    <form action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('ets_cfultimate','submit'),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data" autocomplete="false" novalidate="novalidate">
                        <?php if ($_smarty_tpl->tpl_vars['displayHook']->value) {?><h3><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->title,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</h3><?php }?>
                        <input type="hidden" name="_wpcfu" value="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['contact_form']->value->id), ENT_QUOTES, 'UTF-8');?>
"/>
                        <input type="hidden" name="_ets_cfu_version" value="5.0.1"/>
                        <input type="hidden" name="_ets_cfu_locale" value="en_US"/>
                        <input type="hidden" name="_ets_cfu_unit_tag" value="wpcfu-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->unit_tag,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                        <input type="hidden" name="form_unit_tag" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact_form']->value->form_unit_tag,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                        <input type="hidden" name="_ets_cfu_container_post" value="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['contact_form']->value->id), ENT_QUOTES, 'UTF-8');?>
"/>
                        <?php echo $_smarty_tpl->tpl_vars['form_elements']->value;?>

                        <div class="wpcfu-response-output wpcfu-display-none" id="wpcfu-response-output"></div>
                    </form>
                    <div class="clearfix">&nbsp;</div>
                </div><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['open_form_by_button']->value) {?>
            </div>
        </div>
    </div>
</div>
<?php }
}
}
