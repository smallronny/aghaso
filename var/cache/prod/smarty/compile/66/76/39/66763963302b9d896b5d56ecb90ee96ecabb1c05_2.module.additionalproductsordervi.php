<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb749f2c6_03805176',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '66763963302b9d896b5d56ecb90ee96ecabb1c05' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb749f2c6_03805176 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['combinations_all'] == 0 && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['combinations_display_options'] && $_smarty_tpl->tpl_vars['additional_product']->value->has_attributes == 1) {?>
	<div class="product-options">
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This product is available with options.','mod'=>'additionalproductsorder'),$_smarty_tpl ) );?>

	</div>
<?php }
}
}
