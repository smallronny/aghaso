<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:23:13
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007e01970712_61025968',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0e2ed7d1e2abc6b0a82714afad00d8c3881c002' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:stfacetedsearch/views/templates/listing/center_column.tpl' => 1,
  ),
),false)) {
function content_60007e01970712_61025968 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_32521976060007e0196ed92_38238854', 'product_list_top');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_103715150960007e019700e8_41077501', 'product_list_active_filters');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, 'catalog/listing/category.tpl');
}
/* {block 'product_list_top'} */
class Block_32521976060007e0196ed92_38238854 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_top' => 
  array (
    0 => 'Block_32521976060007e0196ed92_38238854',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_subTemplateRender('module:stfacetedsearch/views/templates/listing/center_column.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_103715150960007e019700e8_41077501 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_active_filters' => 
  array (
    0 => 'Block_103715150960007e019700e8_41077501',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_list_active_filters'} */
}
