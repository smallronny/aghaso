<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb7470609_84061646',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2b46613555e79851fd9998214b15b712a3ae9bde' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb7470609_84061646 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_smarty_tpl->tpl_vars['button_rule']->value)) {?>
    <?php $_smarty_tpl->_assignInScope('button_rule', "display_button_cart");
}
if (!isset($_smarty_tpl->tpl_vars['button_icon']->value)) {?>
    <?php $_smarty_tpl->_assignInScope('button_icon', false);
}?>

<?php if (($_smarty_tpl->tpl_vars['additional_product']->value->has_attributes == 0 || (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['combinations_add_to_cart']) && ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['combinations_add_to_cart'] == 1))) && $_smarty_tpl->tpl_vars['additional_product']->value->available_for_order && !isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value) && ($_smarty_tpl->tpl_vars['additional_product']->value->quantity > 0 || ($_smarty_tpl->tpl_vars['additional_product']->value->allow_oosp && $_smarty_tpl->tpl_vars['additional_product']->value->quantity <= 0)) && $_smarty_tpl->tpl_vars['additional_product']->value->minimal_quantity <= 1 && $_smarty_tpl->tpl_vars['additional_product']->value->customizable == false && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
    <?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_button']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_button'] == '1') {?>
        <?php if ($_smarty_tpl->tpl_vars['button_rule']->value == 'display_button_cart' || $_smarty_tpl->tpl_vars['button_rule']->value == 'display_config') {?>
            <div class="buttons">
                <?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['ajax_put_cart']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['ajax_put_cart']) {?>
                    <a class="lapo-add-to-cart lapo-add-to-cart-stats btn btn-primary add-to-cart" data-button-action="add-to-cart" href="<?php ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['lineven']->value['prestashop']['is_ssl']), ENT_QUOTES, 'UTF-8');
$_prefixVariable3 = ob_get_clean();
ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');
$_prefixVariable4=ob_get_clean();
ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');
$_prefixVariable5=ob_get_clean();
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['link']->getPageLink('cart',$_prefixVariable3,NULL,"add=1&amp;id_product=".$_prefixVariable4."&amp;id_product_attribute=".$_prefixVariable5."&amp;token=".((string)$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['static_token']),false),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','mod'=>'additionalproductsorder'),$_smarty_tpl ) );?>
" data-id-association="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_association), ENT_QUOTES, 'UTF-8');?>
" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
" data-id-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
" data-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
">
                        <span><?php if ($_smarty_tpl->tpl_vars['button_icon']->value) {?><i class="material-icons">add_shopping_cart</i><?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','mod'=>'additionalproductsorder'),$_smarty_tpl ) );
}?></span>
                    </a>
                <?php } else { ?>
                    <a class="lapo-add-to-cart lapo-add-to-cart-stats btn btn-primary" href="<?php ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['lineven']->value['prestashop']['is_ssl']), ENT_QUOTES, 'UTF-8');
$_prefixVariable6 = ob_get_clean();
ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');
$_prefixVariable7=ob_get_clean();
ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');
$_prefixVariable8=ob_get_clean();
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['link']->getPageLink('cart',$_prefixVariable6,NULL,"add=1&amp;id_product=".$_prefixVariable7."&amp;id_product_attribute=".$_prefixVariable8."&amp;token=".((string)$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['static_token']),false),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" data-id-association="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_association), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','mod'=>'additionalproductsorder'),$_smarty_tpl ) );?>
" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
" data-id-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
" data-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
">
                        <span><?php if ($_smarty_tpl->tpl_vars['button_icon']->value) {?><i class="material-icons">add_shopping_cart</i><?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','mod'=>'additionalproductsorder'),$_smarty_tpl ) );
}?></span>
                    </a>
                <?php }?>
            </div>
        <?php }?>
    <?php } else { ?>
        <?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_checkbox']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_checkbox']) {?>
            <?php if ($_smarty_tpl->tpl_vars['button_rule']->value == 'display_checkbox_cart' || $_smarty_tpl->tpl_vars['button_rule']->value == 'display_config') {?>
                <div class="lapo-cart-checkbox">
                    <input id="lapo-add-to-cart-checkbox-<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
-<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
" name="lapo-add-to-cart-checkbox[]" class="checkbox lapo-add-to-cart-checkbox" type="checkbox" <?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_checkbox_icon']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_checkbox_icon']) {?>style="display:none;"<?php }?>
                           data-add-to-cart-url="<?php ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['lineven']->value['prestashop']['is_ssl']), ENT_QUOTES, 'UTF-8');
$_prefixVariable9 = ob_get_clean();
ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');
$_prefixVariable10=ob_get_clean();
ob_start();
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');
$_prefixVariable11=ob_get_clean();
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['link']->getPageLink('cart',$_prefixVariable9,NULL,"add=1&amp;id_product=".$_prefixVariable10."&amp;id_product_attribute=".$_prefixVariable11."&amp;token=".((string)$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['static_token']),false),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                           data-id-association="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_association), ENT_QUOTES, 'UTF-8');?>
"
                           data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
"
                           data-id-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
"
                           data-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
">
                    <?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_checkbox_icon']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_cart_checkbox_icon']) {?>
                        <a class="lapo-add-to-cart lapo-add-to-cart-button-check btn btn-primary" href="javascript:void(0)" rel="nofollow" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','mod'=>'additionalproductsorder'),$_smarty_tpl ) );?>
" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
" data-id-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
" data-product-attribute="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['id_product_attribute']->value), ENT_QUOTES, 'UTF-8');?>
">
                            <span><i class="material-icons">add_shopping_cart</i></span>
                        </a>
                    <?php }?>
                </div>
            <?php }?>
        <?php } else { ?>
            <div class="buttons"></div>
        <?php }?>
    <?php }
} else { ?>
    <?php if ($_smarty_tpl->tpl_vars['button_rule']->value == 'display_config' || $_smarty_tpl->tpl_vars['button_rule']->value == 'display_button_cart') {?>
        <div class="buttons">
            <a class="lapo-view btn btn-info" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->product_link,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View','mod'=>'additionalproductsorder'),$_smarty_tpl ) );?>
" data-id-association="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_association), ENT_QUOTES, 'UTF-8');?>
" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
">
			<span>
				<?php if (($_smarty_tpl->tpl_vars['additional_product']->value->has_attributes == 1 && isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['combinations_add_to_cart']) && !$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['combinations_add_to_cart']) || $_smarty_tpl->tpl_vars['additional_product']->value->customizable) {?>
                    <?php if ($_smarty_tpl->tpl_vars['button_icon']->value) {?><i class="material-icons">remove_red_eye</i><?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Customize','mod'=>'additionalproductsorder'),$_smarty_tpl ) );
}?>
                <?php } else { ?>
                    <?php if ($_smarty_tpl->tpl_vars['button_icon']->value) {?><i class="material-icons">remove_red_eye</i><?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'More','mod'=>'additionalproductsorder'),$_smarty_tpl ) );
}?>
                <?php }?>
			</span>
            </a>
        </div>
    <?php }
}
}
}
