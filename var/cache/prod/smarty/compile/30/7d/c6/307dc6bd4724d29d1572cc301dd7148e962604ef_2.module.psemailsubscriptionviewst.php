<?php
/* Smarty version 3.1.33, created on 2021-01-20 19:15:14
  from 'module:psemailsubscriptionviewst' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6008c792053736_29223163',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:psemailsubscriptionviewst',
      1 => 1606843479,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6008c792053736_29223163 (Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="ps-emailsubscription-block">
<h5>Suscríbete</h5>
<div class="col-md content-subscription">
<p class="text-info-subs"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Quieres más información? Suscríbete.','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
 <p>
    <form action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'index','params'=>array('fc'=>'module','module'=>'iqitemailsubscriptionconf','controller'=>'subscription')),$_smarty_tpl ) );?>
"

          method="post">

                <div class="input-group newsletter-input-group ">

                    <input

                            name="email"

                            type="email"

                            value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"

                            class="form-control input-subscription"

                            placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Ingresar Correo','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
"

                            aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your email address','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
"

                    >

                    <button

                            class="btn btn-primary btn-subscribe btn-iconic"

                            name="submitNewsletter"

                            type="submit"

                            aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
">

                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enviar','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
</button>

                    

                </div>

        <?php if (isset($_smarty_tpl->tpl_vars['id_module']->value)) {?>

            <div class="mt-2 text-muted"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayGDPRConsent','id_module'=>$_smarty_tpl->tpl_vars['id_module']->value),$_smarty_tpl ) );?>
</div>

        <?php }?>

                <input type="hidden" name="action" value="0">

    </form>
<p class="text-info-subs"> <span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Al enviar acepto los términos y condiciones.','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
</span> <p>

</div>
<div class="payment-method">
<ul class="d-flex">

<li>
<div class="t-payment"><img src="/img/cms/iconos/visa-pay.png" alt="Visa">
<div></div>
</div>
</li>
<li>
<div class="t-payment"><img src="/img/cms/iconos/mastercard.png" alt="Masterd Card"></div>
</li>
<li>
<div class="t-payment"><img src="/img/cms/iconos/american-express.png" alt="American Express"></div>
</li>
<li>
<div class="t-payment"><img src="/img/cms/iconos/paypal.png" alt="Paypal"></div>
</li>
</ul>
<div class="text-center mt-4 ComplaintBookFooter">
<a href="/content/12-libro-de-reclamaciones"><img src="/img/cms/libro-de-reclamaciones-aghaso-white.png" alt="Libro de Reclamaciones"></a>
</div>

</div>
</div>



<?php }
}
