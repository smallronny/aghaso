<?php
/* Smarty version 3.1.33, created on 2021-01-20 19:15:13
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6008c791a28214_49660171',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6186a6e0fe95a809282462480c770ae14b2749b0' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6008c791a28214_49660171 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('show_on', Configuration::get('ST_FAC_SEARCH_SHOW_ON'));
$_smarty_tpl->_assignInScope('loading_effect', Configuration::get('ST_FAC_SEARCH_LOADING_EFFECT'));?>
<div class="feds_offcanvas feds_show_on_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_on']->value, ENT_QUOTES, 'UTF-8');?>
 feds_show_on_x">
	<div class="feds_offcanvas_background"></div>
	<div id="feds_offcanvas_search_filters" class="feds_offcanvas_content">
		<?php if ($_smarty_tpl->tpl_vars['show_on']->value == 3 && isset($_smarty_tpl->tpl_vars['listing']->value['rendered_facets'])) {?>
			<?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_facets'];?>

		<?php }?>
		<div class="feds_offcanvas_btn stfeds_flex_container">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clear_all_link']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reset','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
" class="feds_link stfeds_flex_child"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reset','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</a>
			<a href="javascript:;" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Done','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
" class="feds_offcanvas_done feds_offcanvas_guan stfeds_flex_child"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Done','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</a>
		</div>
	</div>
	<a href="javascript:;" class="feds_offcanvas_times feds_offcanvas_guan" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Close','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
">&times;</a>
</div>
<div id="feds_overlay" class="feds_overlay <?php if ($_smarty_tpl->tpl_vars['loading_effect']->value == 2) {?> feds_overlay_tr <?php } else { ?> feds_overlay_center stfeds_flex_container stfeds_flex_center <?php }?> feds_overlay_hide <?php if ($_smarty_tpl->tpl_vars['loading_effect']->value != 2) {?> feds_overlay_click <?php }?>"><i class="feds_overlay_loader feds-spin5 feds_animate-spin"></i></div><?php }
}
