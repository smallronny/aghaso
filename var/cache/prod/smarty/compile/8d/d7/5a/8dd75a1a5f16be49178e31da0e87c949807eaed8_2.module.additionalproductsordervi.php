<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb74c7f64_61201687',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8dd75a1a5f16be49178e31da0e87c949807eaed8' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb74c7f64_61201687 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['partners_reviews']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['partners_reviews'] == true) {?>
    <?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['partners_reviews_module'] == 'homecomments' && isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['partners_reviews_module_instance']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['partners_reviews_module_instance'] != null) {?>
        <div class="reviews">
                        <?php echo $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['partners_reviews_module_instance']->partnerDisplayAverageRate($_smarty_tpl->tpl_vars['additional_product']->value->id,'all',$_smarty_tpl->tpl_vars['additional_product']->value);?>

                    </div>
    <?php } else { ?>
        <?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['partners_reviews_module'] == 'productcomments') {?>
            <div class="hook-reviews">
                <div class="lapo-product-list-reviews-ps_productcomments_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
 product-list-reviews" data-comment-grade="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_average_grade), ENT_QUOTES, 'UTF-8');?>
"  data-comment-nb="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_nb), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="grade-stars small-stars"></div>
                    <div class="comments-nb"></div>
                    <?php if ($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_nb != 0) {?>
                        <div itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating" itemscope>
                            <meta itemprop="reviewCount" content="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_nb), ENT_QUOTES, 'UTF-8');?>
" />
                            <meta itemprop="ratingValue" content="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_average_grade), ENT_QUOTES, 'UTF-8');?>
" />
                        </div>
                        <?php echo '<script'; ?>
>
                            if (typeof jQuery == 'function') {
                                AdditionalProductsOrder.ProductsListComments.displayPrestashopProductCommentsForClassic(<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_average_grade), ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_nb), ENT_QUOTES, 'UTF-8');?>
);
                            } else {
                                document.addEventListener("DOMContentLoaded", function() {
                                    window.setTimeout(function() { AdditionalProductsOrder.ProductsListComments.displayPrestashopProductCommentsForClassic(<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_average_grade), ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->prestashop_product_comment_nb), ENT_QUOTES, 'UTF-8');?>
); }, 80);
                                });
                            }
                        <?php echo '</script'; ?>
>
                    <?php }?>
                </div>
            </div>
        <?php } else { ?>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductListReviews','product'=>$_smarty_tpl->tpl_vars['additional_product']->value->product_lazy_array),$_smarty_tpl ) );?>

        <?php }?>
    <?php }
}
}
}
