<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb73f17b8_32277867',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2c8e9ee012e0f11cb4713b86e444d34d9407ef1c' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:additionalproductsorder/views/templates/hook/_partials/buttons.tpl' => 2,
    'module:additionalproductsorder/views/templates/hook/_partials/image.tpl' => 1,
    'module:additionalproductsorder/views/templates/hook/_partials/name.tpl' => 1,
    'module:additionalproductsorder/views/templates/hook/_partials/options.tpl' => 1,
    'module:additionalproductsorder/views/templates/hook/_partials/attributes.tpl' => 1,
    'module:additionalproductsorder/views/templates/hook/_partials/reviews.tpl' => 1,
    'module:additionalproductsorder/views/templates/hook/_partials/description.tpl' => 1,
    'module:additionalproductsorder/views/templates/hook/_partials/price.tpl' => 1,
    'module:additionalproductsorder/views/templates/hook/_partials/tooltip.tpl' => 1,
  ),
),false)) {
function content_6000ceb73f17b8_32277867 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10975225346000ceb73e5082_24276643', "lapo_header");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9175231086000ceb73e5f57_04528317', "lapo_title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1976907986000ceb73e8180_31211251', "lapo_content_header");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18897111036000ceb73e8d94_38988761', "lapo_product");
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_766557096000ceb73f0376_26237923', "lapo_content_footer");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3143091106000ceb73f0f06_31961501', "lapo_footer");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, 'module:additionalproductsorder/views/templates/hook/_partials/layout.tpl');
}
/* {block "lapo_header"} */
class Block_10975225346000ceb73e5082_24276643 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_header' => 
  array (
    0 => 'Block_10975225346000ceb73e5082_24276643',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<div id="products" class="card list-a">
<?php
}
}
/* {/block "lapo_header"} */
/* {block "lapo_title"} */
class Block_9175231086000ceb73e5f57_04528317 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_title' => 
  array (
    0 => 'Block_9175231086000ceb73e5f57_04528317',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<div class="card-block">
		<div class="h5 header-title"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['section_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
	</div>
<?php
}
}
/* {/block "lapo_title"} */
/* {block "lapo_content_header"} */
class Block_1976907986000ceb73e8180_31211251 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_header' => 
  array (
    0 => 'Block_1976907986000ceb73e8180_31211251',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<ul class="apo-products">
<?php
}
}
/* {/block "lapo_content_header"} */
/* {block "lapo_product"} */
class Block_18897111036000ceb73e8d94_38988761 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_product' => 
  array (
    0 => 'Block_18897111036000ceb73e8d94_38988761',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<li class="apo-product">
		<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/buttons.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('position'=>1,'button_rule'=>"display_checkbox_cart"), 0, false);
?>
		<div class="product-container">
			<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
			<div class="container-description">
				<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/name.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('truncate'=>38), 0, false);
?>
				<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/options.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
				<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/attributes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
				<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/reviews.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('class_name'=>"list"), 0, false);
?>
				<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/description.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('truncate'=>55), 0, false);
?>
				<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
			</div>
			<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/buttons.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('position'=>2,'button_rule'=>"display_button_cart"), 0, true);
?>
		</div>
		<br style="clear:both;" />
		<?php $_smarty_tpl->_subTemplateRender("module:additionalproductsorder/views/templates/hook/_partials/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	</li>
<?php
}
}
/* {/block "lapo_product"} */
/* {block "lapo_content_footer"} */
class Block_766557096000ceb73f0376_26237923 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_footer' => 
  array (
    0 => 'Block_766557096000ceb73f0376_26237923',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	</ul>
<?php
}
}
/* {/block "lapo_content_footer"} */
/* {block "lapo_footer"} */
class Block_3143091106000ceb73f0f06_31961501 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_footer' => 
  array (
    0 => 'Block_3143091106000ceb73f0f06_31961501',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	</div>
<?php
}
}
/* {/block "lapo_footer"} */
}
