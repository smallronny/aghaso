<?php
/* Smarty version 3.1.33, created on 2021-01-13 16:13:06
  from '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/list-contact.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff62625ef681_20596203',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24673acbfb22d4be0c824c1be0ee62675b65b8d8' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/list-contact.tpl',
      1 => 1600098673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fff62625ef681_20596203 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['ETS_CFU_ENABLE_TMCE']->value) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        var ad = '';
        var iso = 'en';
        var file_not_found = '';
        var pathCSS = '<?php echo @constant('_THEME_CSS_DIR_');?>
';
    <?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_PS_JS_DIR_']->value,'html','UTF-8' ));?>
tiny_mce/tiny_mce.js"><?php echo '</script'; ?>
>
    <?php if ($_smarty_tpl->tpl_vars['is_ps15']->value) {?>
        <?php echo '<script'; ?>
 src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_PS_JS_DIR_']->value,'html','UTF-8' ));?>
tinymce.inc.js"><?php echo '</script'; ?>
>
    <?php } else { ?>
        <?php echo '<script'; ?>
 src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_PS_JS_DIR_']->value,'html','UTF-8' ));?>
admin/tinymce.inc.js"><?php echo '</script'; ?>
>
    <?php }
}
if (isset($_smarty_tpl->tpl_vars['okimport']->value) && $_smarty_tpl->tpl_vars['okimport']->value) {?>
    <div class="bootstrap">
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact form imported successfully.','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

        </div>
    </div>
<?php }
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'contactFormUltimateTopBlock'),$_smarty_tpl ) );?>

<?php echo '<script'; ?>
 type="text/javascript">
    var text_update_position = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Successful update','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
';
    <?php if (isset($_smarty_tpl->tpl_vars['is_ps15']->value) && $_smarty_tpl->tpl_vars['is_ps15']->value) {?>
    $(document).on('click', '.dropdown-toggle', function () {
        $(this).closest('.btn-group').toggleClass('open');
    });
    <?php }
echo '</script'; ?>
>
<div class="cfu-content-block">
    <form id="form-contact" class="form-horizontal clearfix products-catalog"
          action="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
" method="post">
        <input id="submitFilterContact" type="hidden" value="0" name="submitFilterContact"/>
        <input type="hidden" value="1" name="page"/>
        <input type="hidden" value="50" name="selected_pagination"/>
        <div class="panel col-lg-12">
            <div class="panel-heading">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact forms','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                <span class="badge"><?php echo intval(count($_smarty_tpl->tpl_vars['ets_cfu_contacts']->value));?>
</span>
                <span class="panel-heading-action">
                    <a id="desc-contactform-new" class="list-toolbar-btn" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add new','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
"
                       href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuAddContact=1">
                        <span title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add new','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
" class="label-tooltip" data-placement="top"
                              data-html="true" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add new','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
"
                              data-toggle="tooltip" title="">
                            <i class="process-icon-new"></i>
                        </span>
                    </a>
                 </span>
            </div>
            <div class="table-responsive-row clearfix">
                <table id="table-contact" class="table contact">
                    <thead>
                    <tr class="nodrag nodrop">
                        <th class="fixed-width-xs text-center ctf_id">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ID','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'id_contact' && $_smarty_tpl->tpl_vars['sort_type']->value == 'desc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=id_contact&sort_type=desc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-down"></i></a>
                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'id_contact' && $_smarty_tpl->tpl_vars['sort_type']->value == 'asc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=id_contact&sort_type=asc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_title">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Title','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'title' && $_smarty_tpl->tpl_vars['sort_type']->value == 'desc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=title&sort_type=desc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-down"></i></a>
                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'title' && $_smarty_tpl->tpl_vars['sort_type']->value == 'asc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=title&sort_type=asc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <?php if (!isset($_smarty_tpl->tpl_vars['showShortcodeHook']->value) || (isset($_smarty_tpl->tpl_vars['showShortcodeHook']->value) && $_smarty_tpl->tpl_vars['showShortcodeHook']->value)) {?>
                            <th class="ctf_shortcode">
                                    <span class="title_box">
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Short code','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    </span>
                            </th>
                        <?php }?>
                        <th class="ct_form_url">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Form URL','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                </span>
                        </th>
                        <th class="ct_form_views">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Views','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                </span>
                        </th>
                        <th class="ctf_sort">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sort order','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'position' && $_smarty_tpl->tpl_vars['sort_type']->value == 'desc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=position&sort_type=desc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-down"></i></a>
                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'position' && $_smarty_tpl->tpl_vars['sort_type']->value == 'asc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=position&sort_type=asc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_message">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Save message','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'save_message' && $_smarty_tpl->tpl_vars['sort_type']->value == 'asc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=save_message&sort_type=desc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-down"></i></a>
                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'save_message' && $_smarty_tpl->tpl_vars['sort_type']->value == 'desc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=save_message&sort_type=asc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_active">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Active','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'active' && $_smarty_tpl->tpl_vars['sort_type']->value == 'desc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=active&sort_type=desc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-down"></i></a>
                                    <a <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'active' && $_smarty_tpl->tpl_vars['sort_type']->value == 'asc') {?> class="active"<?php }?> href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
&sort=active&sort_type=asc<?php echo $_smarty_tpl->tpl_vars['filter_params']->value;?>
"><i
                                                class="icon-caret-up"></i></a>
                                </span>
                        </th>
                        <th class="ctf_action">
                                <span class="title_box">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Action','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                </span>
                        </th>
                    </tr>
                    <tr class="nodrag nodrop">
                        <th class="fixed-width-xs text-center ctf_id">
                                <span class="title_box">
                                    <input class="form-control" name="id_contact" style="width:25px"
                                           value="<?php if (isset($_smarty_tpl->tpl_vars['values_submit']->value['id_contact'])) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['values_submit']->value['id_contact'],'html','UTF-8' ));
}?>"/>
                                </span>
                        </th>
                        <th class="ctf_title">
                                <span class="title_box">
                                    <input class="form-control" name="contact_title" style="width:150px"
                                           value="<?php if (isset($_smarty_tpl->tpl_vars['values_submit']->value['contact_title'])) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['values_submit']->value['contact_title'],'html','UTF-8' ));
}?>"/>
                                </span>
                        </th>
                        <?php if (!isset($_smarty_tpl->tpl_vars['showShortcodeHook']->value) || (isset($_smarty_tpl->tpl_vars['showShortcodeHook']->value) && $_smarty_tpl->tpl_vars['showShortcodeHook']->value)) {?>
                            <th class="">

                            </th>
                        <?php }?>
                        <th class="ct_form_url">
                        </th>
                        <th class="">

                        </th>
                        <th>
                        </th>
                        <th class="ctf_message">
                                <span class="title_box">
                                    <select class="form-control" name="save_message">
                                        <option value="">---</option>
                                        <option value="1" <?php if (isset($_smarty_tpl->tpl_vars['values_submit']->value['save_message']) && $_smarty_tpl->tpl_vars['values_submit']->value['hook'] == 1) {?> selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</option>
                                        <option value="0" <?php if (isset($_smarty_tpl->tpl_vars['values_submit']->value['save_message']) && $_smarty_tpl->tpl_vars['values_submit']->value['hook'] == 0) {?> selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</option>
                                    </select>
                                </span>
                        </th>
                        <th class="ctf_active">
                            <select class="form-control" name="active_contact">
                                <option value="">---</option>
                                <option value="1" <?php if (isset($_smarty_tpl->tpl_vars['values_submit']->value['active_contact']) && $_smarty_tpl->tpl_vars['values_submit']->value['active_contact'] == 1) {?> selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</option>
                                <option value="0" <?php if (isset($_smarty_tpl->tpl_vars['values_submit']->value['active_contact']) && $_smarty_tpl->tpl_vars['values_submit']->value['active_contact'] == 0) {?> selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</option>
                            </select>
                        </th>
                        <th class="ctf_action">
                                <span class="pull-right">
                                    <button id="submitFilterButtonContact" class="btn btn-default"
                                            name="submitFilterButtonContact" type="submit">
                                    <i class="icon-search"></i>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    </button>
                                    <?php if (isset($_smarty_tpl->tpl_vars['filter']->value) && $_smarty_tpl->tpl_vars['filter']->value) {?>
                                        <a class="btn btn-warning"
                                           href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateContactForm',true),'html','UTF-8' ));?>
">
                                            <i class="icon-eraser"></i>
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reset','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                        </a>
                                    <?php }?>
                                </span>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="list-contactform">

                    <?php if ($_smarty_tpl->tpl_vars['ets_cfu_contacts']->value) {?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ets_cfu_contacts']->value, 'contact');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['contact']->value) {
?>
                            <tr id="formcontact_<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
">
                                <td class="ctf_id"><?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
</td>
                                <td class="ctf_title"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact']->value['title'],'html','UTF-8' ));?>
</td>
                                <?php if (!isset($_smarty_tpl->tpl_vars['showShortcodeHook']->value) || (isset($_smarty_tpl->tpl_vars['showShortcodeHook']->value) && $_smarty_tpl->tpl_vars['showShortcodeHook']->value)) {?>
                                    <td class="ctf_shortcode">
                                        <div class="short-code">
                                            <input title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click to copy','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
" class="ctf-short-code"
                                                   type="text" value='[contact-form-7 id="<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"]'/>
                                            <span class="text-copy"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Copied','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</span>
                                        </div>
                                    </td>
                                <?php }?>
                                <td class="ct_form_url">
                                    <?php if ($_smarty_tpl->tpl_vars['contact']->value['enable_form_page']) {?>
                                        <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact']->value['link'],'html','UTF-8' ));?>
"
                                           target="_blank"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['contact']->value['link'],'html','UTF-8' ));?>
</a>
                                    <?php } else { ?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Form page is disabled','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                    <?php }?>
                                </td>
                                <td class="ct_view text-center">
                                    <?php echo intval($_smarty_tpl->tpl_vars['contact']->value['count_views']);?>

                                                                    </td>
                                <td class="ctf_sort text-center <?php if ($_smarty_tpl->tpl_vars['sort']->value == 'position' && $_smarty_tpl->tpl_vars['sort_type']->value == 'asc') {?>pointer dragHandle center<?php }?>">
                                    <div class="dragGroup">
                                        <span class="positions"><?php echo intval(($_smarty_tpl->tpl_vars['contact']->value['position']+1));?>
</span>
                                    </div>
                                </td>
                                <td class="text-center ctf_message">
                                    <?php if ($_smarty_tpl->tpl_vars['contact']->value['save_message']) {?>
                                        <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click to disable','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
"
                                           href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuSaveMessageUpdate=0&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
">
                                            <i class="material-icons action-enabled">check</i>
                                        </a>
                                        <?php if ($_smarty_tpl->tpl_vars['contact']->value['count_message']) {?>
                                            <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View messages','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
"
                                               href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateMessage'),'html','UTF-8' ));?>
&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"
                                               class="">
                                                (<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['count_message']);?>
)
                                            </a>
                                        <?php }?>
                                    <?php } else { ?>
                                        <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click to enable','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
"
                                           href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuSaveMessageUpdate=1&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
">
                                            <i class="material-icons action-disabled"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'clear','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</i>
                                        </a>
                                        <?php if ($_smarty_tpl->tpl_vars['contact']->value['count_message']) {?>
                                            <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateMessage'),'html','UTF-8' ));?>
&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"
                                               class="">
                                                (<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['count_message']);?>
)
                                            </a>
                                        <?php }?>
                                    <?php }?>
                                </td>
                                <td class="text-center ctf_active">
                                    <?php if ($_smarty_tpl->tpl_vars['contact']->value['active']) {?>
                                        <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click to disable','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
"
                                        href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuActiveUpdate=0&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
">
                                            <i class="material-icons action-enabled">check</i>
                                        </a>
                                    <?php } else { ?>
                                        <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click to enable','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
"
                                        href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuActiveUpdate=1&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
">
                                            <i class="material-icons action-disabled"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'clear','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</i>
                                        </a>
                                    <?php }?>
                                </td>
                                <td class="text-center ctf_action">
                                    <div class="btn-group-action">
                                        <div class="btn-group">
                                            <a class="btn tooltip-link product-edit" title=""
                                               href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuEditContact=1&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"
                                               title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
">
                                                <i class="material-icons">mode_edit</i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                            </a>
                                            <a class="btn btn-link dropdown-toggle dropdown-toggle-split product-edit"
                                               aria-expanded="false" aria-haspopup="true" data-toggle="dropdown"> <i
                                                        class="icon-caret-down"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right"
                                                 style="position: absolute; transform: translate3d(-164px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                <?php if ($_smarty_tpl->tpl_vars['contact']->value['enable_form_page']) {?>
                                                    <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( Ets_CfUltimate::getLinkContactForm(intval($_smarty_tpl->tpl_vars['contact']->value['id_contact'])),'html','UTF-8' ));?>
"
                                                       class="dropdown-item product-edit" target="_blank"
                                                       >
                                                        <i class="material-icons">remove_red_eye</i>
                                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View form','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                                    </a>
                                                <?php }?>
                                                <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateMessage'),'html','UTF-8' ));?>
&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"
                                                   class="dropdown-item product-edit"
                                                   >
                                                    <i class="icon icon-comments fa fa-comments"></i>
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Messages','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                                </a>
                                                <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminContactFormUltimateStatistics'),'html','UTF-8' ));?>
&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"
                                                   class="dropdown-item product-edit"
                                                   >
                                                    <i class="icon icon-line-chart"></i>
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Statistics','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                                </a>
                                                <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuDuplicateContact=1&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"
                                                   class="dropdown-item message-duplidate product-edit"
                                                   >
                                                    <i class="icon icon-copy"></i>
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Duplicate','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                                </a>
                                                <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_module']->value,'html','UTF-8' ));?>
&etsCfuDeleteContact=1&id_contact=<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
"
                                                   class="dropdown-item message-delete product-edit"
                                                   >
                                                    <i class="material-icons">delete</i>
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete form','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>

                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="10">
                                <p class="alert alert-warning"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No contact forms available','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</p>
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
                <?php echo $_smarty_tpl->tpl_vars['ets_cfu_pagination_text']->value;?>

            </div>
        </div>
    </form>
    <?php echo '<script'; ?>
 type="text/javascript">
        $(document).ready(function () {
            if ($("table .datepicker").length > 0) {
                $("table .datepicker").datepicker({
                    prevText: '',
                    nextText: '',
                    dateFormat: 'yy-mm-dd'
                });
            }
        });
    <?php echo '</script'; ?>
>
</div>
<div class="ctf-popup-wapper-admin">
    <div class="fuc"></div>
    <div class="ctf-popup-tablecell">
        <div class="ctf-popup-content">
            <div class="ctf_close_popup"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'close','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</div>
            <div id="form-contact-preview">

            </div>
        </div>
    </div>
</div><?php }
}
