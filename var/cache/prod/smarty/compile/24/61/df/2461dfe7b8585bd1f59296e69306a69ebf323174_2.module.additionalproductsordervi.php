<?php
/* Smarty version 3.1.33, created on 2021-01-20 19:15:13
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6008c791e365a6_79447086',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2461dfe7b8585bd1f59296e69306a69ebf323174' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_6008c791e365a6_79447086 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4614006326008c791e2f418_93795812', "lapo_header");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20263530936008c791e30480_20032773', "lapo_title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9378660836008c791e325b4_75773323', "lapo_content_header");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19567156286008c791e333c7_66678042', "lapo_product");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8569345786008c791e35103_70519698', "lapo_content_footer");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12308102606008c791e35d45_99318229', "lapo_footer");
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'module:additionalproductsorder/views/templates/hook/_partials/layout_theme.tpl');
}
/* {block "lapo_header"} */
class Block_4614006326008c791e2f418_93795812 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_header' => 
  array (
    0 => 'Block_4614006326008c791e2f418_93795812',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<div class="theme">
<?php
}
}
/* {/block "lapo_header"} */
/* {block "lapo_title"} */
class Block_20263530936008c791e30480_20032773 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_title' => 
  array (
    0 => 'Block_20263530936008c791e30480_20032773',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<div class="card-block">
		<div class="h1 header-title"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['section_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
	</div>
	<hr class="header-hr"/>
<?php
}
}
/* {/block "lapo_title"} */
/* {block "lapo_content_header"} */
class Block_9378660836008c791e325b4_75773323 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_header' => 
  array (
    0 => 'Block_9378660836008c791e325b4_75773323',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<section id="products">
		<div class="products">
<?php
}
}
/* {/block "lapo_content_header"} */
/* {block "lapo_product"} */
class Block_19567156286008c791e333c7_66678042 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_product' => 
  array (
    0 => 'Block_19567156286008c791e333c7_66678042',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['additional_product']->value,'position'=>$_smarty_tpl->tpl_vars['position']->value), 0, false);
}
}
/* {/block "lapo_product"} */
/* {block "lapo_content_footer"} */
class Block_8569345786008c791e35103_70519698 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_footer' => 
  array (
    0 => 'Block_8569345786008c791e35103_70519698',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

		</div>
	</section>
<?php
}
}
/* {/block "lapo_content_footer"} */
/* {block "lapo_footer"} */
class Block_12308102606008c791e35d45_99318229 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_footer' => 
  array (
    0 => 'Block_12308102606008c791e35d45_99318229',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	</div>
<?php
}
}
/* {/block "lapo_footer"} */
}
