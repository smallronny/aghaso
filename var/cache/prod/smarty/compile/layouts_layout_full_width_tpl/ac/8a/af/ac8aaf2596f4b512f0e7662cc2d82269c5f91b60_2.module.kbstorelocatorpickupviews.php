<?php
/* Smarty version 3.1.33, created on 2021-01-13 15:31:15
  from 'module:kbstorelocatorpickupviews' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff5893ec1367_94944685',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ac8aaf2596f4b512f0e7662cc2d82269c5f91b60' => 
    array (
      0 => 'module:kbstorelocatorpickupviews',
      1 => 1606511013,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fff5893ec1367_94944685 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12652687425fff5893e6c259_67993349', 'content');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'content'} */
class Block_12652687425fff5893e6c259_67993349 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_12652687425fff5893e6c259_67993349',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="kb-clearfix"></div>
<div class="d-none">
    <h1 class="page-heading"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Our Stores','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</h1>
</div>

<div class="velo-search-container page-content card card-block">
    <div class="velo-store-locator">
        <form method="post" id="searchStoreForm" action>
            <?php if (isset($_smarty_tpl->tpl_vars['is_enabled_store_logo']->value) && $_smarty_tpl->tpl_vars['is_enabled_store_logo']->value == 1) {?>
                <div class="velo-store-banners">
                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logo_image']->value, ENT_QUOTES, 'UTF-8');?>
" style="width: 100%;max-height:300px;">
                </div>
            <?php }?>

            <div class="velo-store-filters">
                <div class="velo-search-bar velo-field-inline">
                    <label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Find Stores Near','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
:</label>
                    <input type="text" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enter Street Address or City & State','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
" id="velo_address_search"/>
                </div>
                <div class="velo-search-distance velo-field-inline">
                    <label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Distance','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
:</label>
                    <select id="velo_within_distance">
                        <option value="5">5 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="10">10 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="15">15 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option selected="" value="25">25 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="50">50 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="100">100 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="250">250 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="500">500 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="1000">1000 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['distance_unit']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
                        <option value="0"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No Limit','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</option>
                    </select>
                </div>
                <div class="velo-search-limit velo-field-inline">
                    <label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Results','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</label>
                    <select id="velo_limit" >
                        <option value="25" selected="selected">25</option>
                        <option value="50">50</option>
                        <option value="75">75</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div class="velo-search-button velo-field-inline">
                    <button type="button" id="searchStoreBtn"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</button>
                </div>
                <div class="velo-search-button velo-field-inline">
                    <button type="button" id="resetStoreBtn"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reset','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</button>
                </div>
            </div>
        </form>
        <!--Filters-->
        <div class="velo-store-map velo-pickup-store-map">
            <div id="map"></div>
        </div>
        <div class="velo-location-list velo-pickup-location-list">
        <div class="header-store-locator">
            <h3 class="page-heading titlestore"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Find a Store','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</h3>
            <img src="/img/cms/iconos/Line_peq_blog_amano.png" alt="">
        </div>
        <div>
            <ul>
                <?php if (!empty($_smarty_tpl->tpl_vars['available_stores']->value)) {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['available_stores']->value, 'store', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['store']->value) {
?>
                        <?php $_smarty_tpl->_assignInScope('random', mt_rand(132310,20323221));?>
                        <li id="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['random']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" >
                            
                            <table style="width: 100%;">
                                <tr class="row">
                                    <input type="hidden" id="velo-add-longitude" name="velo-add-longitude" value=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['store']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
>
                    <input type="hidden" id="velo-add-latitude" name="velo-add-longitude" value=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['store']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
>

                      <?php if ($_smarty_tpl->tpl_vars['is_enabled_store_image']->value) {?> 
                                    <td class="col-md-6">
                                        <div id="kb-store-image">
                            <?php if (!empty($_smarty_tpl->tpl_vars['store']->value['image'])) {
echo $_smarty_tpl->tpl_vars['store']->value['image'];
}?>
                        </div>
                                        <div></div>
                                    </td>
                                    <?php }?>
                                    <td class="col-md-6">
                                        <div class="velo_add_name"> <span class="velo_store_icon" style="display:none;"><img class="velo-phone-icon" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store_icon']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/> </span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
                            <div class="velo-add-address"> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['address1'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php if (!empty($_smarty_tpl->tpl_vars['store']->value['address2'])) {?> 
                                <br/><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['address2'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            <?php if (!empty($_smarty_tpl->tpl_vars['store']->value['state'])) {?> 
                                <br/><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['state'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            <?php if (!empty($_smarty_tpl->tpl_vars['store']->value['postcode'])) {?> 
                                <br/><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['postcode'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            <?php if (!empty($_smarty_tpl->tpl_vars['store']->value['city'])) {?> 
                                <br/><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['city'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            </div>
                                    </td>
                                  
                                </tr>
                            </table>

                            <div style="display: none" class="velo-w3-address-country"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['country'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
                            <div class="velo-show-more"><strong><a href="javascript:void(0);" id="button-show-more-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['store']->value['id_store'], ENT_QUOTES, 'UTF-8');?>
" class="button-show-more" ><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Show More','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a></strong></div>
                            <div class="extra_content" style="display:none;">
                        <?php if (!empty($_smarty_tpl->tpl_vars['store']->value['hours'])) {?>
                                <div  class="kb-store-hours">
                                <span class="velo_add_clock"><img class="velo-phone-icon" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['clock_icon']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Store Timing','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</span>
                                    <table class="mp-openday-list" style="display: table;">
                                    <tbody>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['store']->value['hours'], 'time', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['time']->value) {
?>

                                    <?php if (isset($_smarty_tpl->tpl_vars['time']->value['hours'])) {?>
                                            <tr>
                                        <td class="mp-openday-list-title"><strong><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['time']->value['day'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</td>
                                        <td class="mp-openday-list-value"><?php if ($_smarty_tpl->tpl_vars['time']->value['hours'] == '') {?> <span style="color: red"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Closed','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</span><?php } else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['time']->value['hours'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?></td>
                                        </tr>
                                    <?php }?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </tbody></table>
                            </div>
                        <?php }?>
                        <?php if (!empty($_smarty_tpl->tpl_vars['store']->value['email'])) {?>
                            <span class="velo_add_number"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['email'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['display_phone']->value && !empty($_smarty_tpl->tpl_vars['store']->value['phone'])) {?>
                            <span class="velo_add_number"><img class="velo-phone-icon" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['phone_icon']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['phone'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
                            <?php }?>
                                                <?php if ($_smarty_tpl->tpl_vars['is_enabled_website_link']->value) {?>
                                <div class="kb-store-url">
                                    <span class="velo_add_number"><img class="velo-web-icon" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['web_icon']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/> <a href='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['index_page_link']->value, ENT_QUOTES, 'UTF-8');?>
'>
                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['index_page_link']->value, ENT_QUOTES, 'UTF-8');?>

                                    </a></span>

                            </div>
                        <?php }?>
                            </div>
                                                <div class="velo_add_distance"></div>
                        
                        <button onclick="focus_and_popup(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['random']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)" class="velo-directions-button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View Map','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
 <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                                                <?php if ($_smarty_tpl->tpl_vars['is_enabled_direction_link']->value) {?>
                                <a  target="_blank" class="velo-directions-link" href="http://google.com/maps/dir/?api=1&amp;destination=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['latitude'],'url' )), ENT_QUOTES, 'UTF-8');?>
%2C<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store']->value['longitude'],'url' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Get Direction','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
 <i class="material-icons">directions</i></a>
                                <?php }?>
                                                <input type="hidden" name="kb_av_store_details" value='<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['av_store_detail']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
'>
                        
                    </li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php } else { ?>
            <?php }?>
        </ul>
        </div>
    </div>
</div>
</div>
<div id="ComingSoonStore"></div>
<input type="hidden" class="page_controller" value='stores'>
<?php echo '<script'; ?>
 >
    //changes by tarun
    <?php if ($_smarty_tpl->tpl_vars['is_enabled_show_stores']->value == 1) {?>
        var kb_all_stores = <?php echo $_smarty_tpl->tpl_vars['kb_all_stores']->value;?>
;
    <?php } else { ?>
        var kb_all_stores = '';
    <?php }?>    
    //changes over 
    var kb_not_found = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'not found','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
    var show_more = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Show More','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
    var hide = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Hide','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
    var lang_iso = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lang_iso']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var kb_store_url = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['kb_store_url']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var locator_icon = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['locator_icon']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var default_latitude = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['default_latitude']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
    var default_longitude = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['default_longitude']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
    var zoom_level = <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoom_level']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
    var current_location_icon = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['current_location_icon']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var locations = '';

    <?php if (!empty($_smarty_tpl->tpl_vars['selected_store']->value)) {?>
    locations = [
        ["<div class='velo-popup'><div class='gm_name'><?php if (isset($_smarty_tpl->tpl_vars['selected_store']->value['name'][$_smarty_tpl->tpl_vars['id_lang']->value])) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['name'][$_smarty_tpl->tpl_vars['id_lang']->value],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?></div><div class='gm_address'><?php if (isset($_smarty_tpl->tpl_vars['selected_store']->value['address1'][$_smarty_tpl->tpl_vars['id_lang']->value])) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['address1'][$_smarty_tpl->tpl_vars['id_lang']->value],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_store']->value['address1'], ENT_QUOTES, 'UTF-8');
}?><br/><?php if (isset($_smarty_tpl->tpl_vars['selected_store']->value['address2'][$_smarty_tpl->tpl_vars['id_lang']->value])) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['address2'][$_smarty_tpl->tpl_vars['id_lang']->value],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_store']->value['address2'], ENT_QUOTES, 'UTF-8');
}?></div><div class='gm_location'><?php if (!empty($_smarty_tpl->tpl_vars['selected_store']->value['state'])) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['state'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,<?php }
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['country'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div><?php if ($_smarty_tpl->tpl_vars['display_phone']->value && !empty($_smarty_tpl->tpl_vars['selected_store']->value['phone'])) {?><div class='gm_phone'><img class='velo-phone-icon' src='<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['phone_icon']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
'><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['phone'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div><?php }
if ($_smarty_tpl->tpl_vars['is_enabled_website_link']->value) {?><div class='gm_website'><a href='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['index_page_link']->value, ENT_QUOTES, 'UTF-8');?>
'><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['index_page_link']->value, ENT_QUOTES, 'UTF-8');?>
</a></div><?php }?></div>", <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['latitude'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['selected_store']->value['longitude'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
],
    ];
    <?php }?>
    var default_content = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'location','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
    function initialize() {
        var myOptions = {
            center: new google.maps.LatLng(25.6141693, -80.5509604),
            zoom: zoom_level,

        };
        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        <?php if ($_smarty_tpl->tpl_vars['is_enabled_show_stores']->value == 1) {?>
            locations = kb_all_stores;
        <?php }?>
        setMarkers(map, locations)

    }
    
    function setMarkers(map, locations) {
//     
    if (locations.length == 0) {
        var lat = parseFloat(default_latitude);
        var long = parseFloat(default_longitude);

        latlngset = new google.maps.LatLng(lat, long);
        var icon = {
            url: locator_icon, // url
            scaledSize: new google.maps.Size(40, 40), // scaled size
        };
        var marker = new google.maps.Marker({
            map: map, position: latlngset, icon: icon, animation: google.maps.Animation.DROP
        });
        map.setCenter(marker.getPosition())
        var content = default_content;
        var infowindow = new google.maps.InfoWindow()

        google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
            return function (results, status) {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };

        })(marker, content, infowindow));
    } else {

        var marker, i

        for (i = 0; i < locations.length; i++)
        {

            var loan = locations[i][0]
            var lat = locations[i][1]
            var long = locations[i][2]

            latlngset = new google.maps.LatLng(lat, long);
            var icon = {
                url: locator_icon, // url
                scaledSize: new google.maps.Size(40, 40), // scaled size
            };
            var marker = new google.maps.Marker({
                map: map, title: loan, position: latlngset, icon: icon, animation: google.maps.Animation.DROP
            });
            map.setCenter(marker.getPosition());


            var content = loan;

            var infowindow = new google.maps.InfoWindow()

            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));
        }
    }
    google.maps.event.addListener(marker, 'mouseover', function (e) {
              e.tb.target.parentElement.removeAttribute('title')
           });
}
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['map_api']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&callback=initialize">
<?php echo '</script'; ?>
>
<style>
    .velo-location-list ul li{
        background-image:url("<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['locator_icon']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"); 
        background-size: 30px;
    }
    .velo-directions-link {
        clear:none !important;
    }
</style>
<?php
}
}
/* {/block 'content'} */
}
