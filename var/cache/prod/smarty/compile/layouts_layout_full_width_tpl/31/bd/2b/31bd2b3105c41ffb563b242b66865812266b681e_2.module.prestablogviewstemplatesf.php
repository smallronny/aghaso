<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:28:28
  from 'module:prestablogviewstemplatesf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007f3c3d5072_33449344',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '31bd2b3105c41ffb563b242b66865812266b681e' => 
    array (
      0 => 'module:prestablogviewstemplatesf',
      1 => 1599962465,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007f3c3d5072_33449344 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<!-- Module Presta Blog START PAGE -->


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_131839797660007f3c3b15b8_71386988', 'head_seo');
?>


	  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_73933771060007f3c3c96c0_58295048', 'content');
?>




<!-- /Module Presta Blog END PAGE -->
<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout_blog']->value);
}
/* {block 'head_seo_keywords'} */
class Block_101730435360007f3c3b3e53_26331067 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['meta']['keywords'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}
}
/* {/block 'head_seo_keywords'} */
/* {block 'head_hreflang'} */
class Block_120498162060007f3c3b7fb3_10734476 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['urls']->value['alternative_langs'], 'pageUrl', false, 'code');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['code']->value => $_smarty_tpl->tpl_vars['pageUrl']->value) {
?>
            <link rel="alternate" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pageUrl']->value, ENT_QUOTES, 'UTF-8');?>
" hreflang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
">
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  <?php
}
}
/* {/block 'head_hreflang'} */
/* {block 'head_seo'} */
class Block_131839797660007f3c3b15b8_71386988 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_seo' => 
  array (
    0 => 'Block_131839797660007f3c3b15b8_71386988',
  ),
  'head_seo_keywords' => 
  array (
    0 => 'Block_101730435360007f3c3b3e53_26331067',
  ),
  'head_hreflang' => 
  array (
    0 => 'Block_120498162060007f3c3b7fb3_10734476',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <title><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</title>
  <meta name="description" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_description']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
  <meta name="keywords" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_101730435360007f3c3b3e53_26331067', 'head_seo_keywords', $this->tplIndex);
?>
">
  <?php if ($_smarty_tpl->tpl_vars['page']->value['meta']['robots'] !== 'index') {?>
    <meta name="robots" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['meta']['robots'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_120498162060007f3c3b7fb3_10734476', 'head_hreflang', $this->tplIndex);
?>

  <?php if ($_smarty_tpl->tpl_vars['page']->value['canonical']) {?>
    <link rel="canonical" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['canonical'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>
  <?php if (isset($_smarty_tpl->tpl_vars['Pagination']->value['NombreTotalPages']) && $_smarty_tpl->tpl_vars['Pagination']->value['NombreTotalPages'] > 1) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Pagination']->value['PremieresPages'], 'value_page', false, 'key_page');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key_page']->value => $_smarty_tpl->tpl_vars['value_page']->value) {
?>
      <?php if (($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] == $_smarty_tpl->tpl_vars['key_page']->value) || (!$_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] && $_smarty_tpl->tpl_vars['key_page']->value == 1)) {?>

      <?php } else { ?>
        <?php if ($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] == ($_smarty_tpl->tpl_vars['key_page']->value-1)) {?>
          <link rel="next" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'start'=>$_smarty_tpl->tpl_vars['value_page']->value,'p'=>$_smarty_tpl->tpl_vars['key_page']->value,'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
          <?php } elseif ($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] == $_smarty_tpl->tpl_vars['key_page']->value) {?>
          <link rel="prev" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" >
        <?php } elseif ($_smarty_tpl->tpl_vars['Pagination']->value['PageCourante'] == ($_smarty_tpl->tpl_vars['key_page']->value+1)) {?>
          <link rel="prev" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('categorie'=>$_smarty_tpl->tpl_vars['prestablog_categorie_link_rewrite']->value,'start'=>$_smarty_tpl->tpl_vars['value_page']->value,'p'=>$_smarty_tpl->tpl_vars['key_page']->value,'c'=>$_smarty_tpl->tpl_vars['prestablog_categorie']->value,'m'=>$_smarty_tpl->tpl_vars['prestablog_month']->value,'y'=>$_smarty_tpl->tpl_vars['prestablog_year']->value),$_smarty_tpl ) );
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>
      <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      <?php }?>


<?php
}
}
/* {/block 'head_seo'} */
/* {block 'content'} */
class Block_73933771060007f3c3c96c0_58295048 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_73933771060007f3c3c96c0_58295048',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


		<?php if (isset($_smarty_tpl->tpl_vars['tpl_filtre_cat']->value) && $_smarty_tpl->tpl_vars['tpl_filtre_cat']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_filtre_cat']->value),$_smarty_tpl ) );
}?>
		<?php if (isset($_smarty_tpl->tpl_vars['tpl_menu_cat']->value) && $_smarty_tpl->tpl_vars['tpl_menu_cat']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_menu_cat']->value),$_smarty_tpl ) );
}?>

		<?php if (isset($_smarty_tpl->tpl_vars['tpl_unique']->value) && $_smarty_tpl->tpl_vars['tpl_unique']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_unique']->value),$_smarty_tpl ) );
}?>
		<?php if (isset($_smarty_tpl->tpl_vars['tpl_comment']->value) && $_smarty_tpl->tpl_vars['tpl_comment']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_comment']->value),$_smarty_tpl ) );
}?>
		<?php if (isset($_smarty_tpl->tpl_vars['tpl_comment_fb']->value) && $_smarty_tpl->tpl_vars['tpl_comment_fb']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_comment_fb']->value),$_smarty_tpl ) );
}?>

		<?php if (isset($_smarty_tpl->tpl_vars['tpl_slide']->value) && $_smarty_tpl->tpl_vars['tpl_slide']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_slide']->value),$_smarty_tpl ) );
}?>
    <?php if (isset($_smarty_tpl->tpl_vars['tpl_cat']->value) && $_smarty_tpl->tpl_vars['tpl_cat']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_cat']->value),$_smarty_tpl ) );
}?>
		<?php if (isset($_smarty_tpl->tpl_vars['tpl_aut']->value) && $_smarty_tpl->tpl_vars['tpl_aut']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_aut']->value),$_smarty_tpl ) );
}?>
		<?php if (isset($_smarty_tpl->tpl_vars['tpl_all']->value) && $_smarty_tpl->tpl_vars['tpl_all']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['tpl_all']->value),$_smarty_tpl ) );
}?>

    <?php
}
}
/* {/block 'content'} */
}
