<?php
/* Smarty version 3.1.33, created on 2021-01-20 19:15:13
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6008c791e529a9_76395516',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0113901a7d1aa36e503c1df65a6aa14c5d67762f' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6008c791e529a9_76395516 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['is_active'] == true) {?>
    <?php if (!$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['is_refresh']) {?>
        <section class="lineven-additionalproductsorder">
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['sections']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['sections']) {?>
        <?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['title'], ENT_QUOTES, 'UTF-8');
$_prefixVariable1 = ob_get_clean();
$_smarty_tpl->_assignInScope('section_title', $_prefixVariable1);?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1166899266008c791e402b4_18418982', "lapo_before_sections");
?>

            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['sections'], 'section', false, 'section_key', 'section', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['section_key']->value => $_smarty_tpl->tpl_vars['section']->value) {
?>
                <?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['is_separate_results']) {?>
                    <?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['section']->value['title'], ENT_QUOTES, 'UTF-8');
$_prefixVariable2 = ob_get_clean();
$_smarty_tpl->_assignInScope('section_title', $_prefixVariable2);?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['section']->value['products'] && count($_smarty_tpl->tpl_vars['section']->value['products'])) {?>
                    <section class="lineven-additionalproductsorder-section">
                        <div class="apo-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['hook_class_name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10665153886008c791e479f1_24596024', "lapo_header");
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2917698666008c791e4a301_60462017', "lapo_title");
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17723376386008c791e4af57_91529730', "lapo_content_header");
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18049128416008c791e4bba0_91729162', "lapo_content");
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11000663966008c791e4f0c0_12702553', "lapo_content_footer");
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21472734746008c791e4fbf2_63778089', "lapo_footer");
?>

                        </div>
                    </section>
                <?php }?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20808728716008c791e50d87_94610829', "lapo_after_sections");
?>

    <?php }?>
    <?php if (!$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['is_refresh']) {?>
        </section>
    <?php }
}
}
/* {block "lapo_before_sections"} */
class Block_1166899266008c791e402b4_18418982 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_before_sections' => 
  array (
    0 => 'Block_1166899266008c791e402b4_18418982',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_before_sections"} */
/* {block "lapo_header"} */
class Block_10665153886008c791e479f1_24596024 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_header' => 
  array (
    0 => 'Block_10665153886008c791e479f1_24596024',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <div class="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['template_class_name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                            <?php
}
}
/* {/block "lapo_header"} */
/* {block "lapo_title"} */
class Block_2917698666008c791e4a301_60462017 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_title' => 
  array (
    0 => 'Block_2917698666008c791e4a301_60462017',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_title"} */
/* {block "lapo_content_header"} */
class Block_17723376386008c791e4af57_91529730 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_header' => 
  array (
    0 => 'Block_17723376386008c791e4af57_91529730',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_content_header"} */
/* {block "lapo_product"} */
class Block_11793437076008c791e4dca0_39284972 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_product"} */
/* {block "lapo_content"} */
class Block_18049128416008c791e4bba0_91729162 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content' => 
  array (
    0 => 'Block_18049128416008c791e4bba0_91729162',
  ),
  'lapo_product' => 
  array (
    0 => 'Block_11793437076008c791e4dca0_39284972',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['section']->value['products'], 'additional_product', false, 'position', 'additional_product', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['position']->value => $_smarty_tpl->tpl_vars['additional_product']->value) {
?>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11793437076008c791e4dca0_39284972', "lapo_product", $this->tplIndex);
?>

                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            <?php
}
}
/* {/block "lapo_content"} */
/* {block "lapo_content_footer"} */
class Block_11000663966008c791e4f0c0_12702553 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_footer' => 
  array (
    0 => 'Block_11000663966008c791e4f0c0_12702553',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_content_footer"} */
/* {block "lapo_footer"} */
class Block_21472734746008c791e4fbf2_63778089 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_footer' => 
  array (
    0 => 'Block_21472734746008c791e4fbf2_63778089',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                </div>
                            <?php
}
}
/* {/block "lapo_footer"} */
/* {block "lapo_after_sections"} */
class Block_20808728716008c791e50d87_94610829 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_after_sections' => 
  array (
    0 => 'Block_20808728716008c791e50d87_94610829',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_after_sections"} */
}
