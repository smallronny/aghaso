<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:23:13
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007e01954538_42559238',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '39df4192b5bbde53adc9288b2be23918c4ae8b5f' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007e01954538_42559238 (Smarty_Internal_Template $_smarty_tpl) {
if (($_smarty_tpl->tpl_vars['with_inputs']->value && ($_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight')) || (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['tooltips']) && $_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['tooltips']) || (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['snap']) && $_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['snap'])) {?>
<div class="st-range-top st-range-bar <?php if ($_smarty_tpl->tpl_vars['with_inputs']->value && ($_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight')) {?> with_inputs <?php } elseif (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['tooltips']) && $_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['tooltips']) {?> space_for_tooltips <?php }?>">
                  <?php if ($_smarty_tpl->tpl_vars['with_inputs']->value && ($_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight')) {?>
                  <input class="st_lower_input form-control" />
                  <?php if (!isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['vertical']) || !$_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['vertical']) {?><div class="value-split">-</div><input class="st_upper_input form-control" /><?php }?>
                  <?php } elseif (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['snap']) && $_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['snap']) {?>
                  <span class="value-lower"></span>
                  <?php if ($_smarty_tpl->tpl_vars['facet']->value['properties']['multi_selection'] || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight') {?><span class="value-split">-</span>
                  <span class="value-upper"></span><?php }?>
                  <?php }?>
                </div>
                <?php }?>
                <div class="st_range_inner">
                <div class="st-range" 
                data-jiazhong="<?php if ($_smarty_tpl->tpl_vars['facet']->value['type'] != 'price' && $_smarty_tpl->tpl_vars['facet']->value['type'] != 'weight') {?>rangeslider<?php } else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['type'], ENT_QUOTES, 'UTF-8');
}?>" 
                data-url="<?php if ($_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight') {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['filters'][0]['nextEncodedFacetsURL'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['url'], ENT_QUOTES, 'UTF-8');
}?>" 
                data-min="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['min'], ENT_QUOTES, 'UTF-8');?>
" 
                data-max="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['max'], ENT_QUOTES, 'UTF-8');?>
" 
                data-lower="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['lower'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['lower'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['min'], ENT_QUOTES, 'UTF-8');
}?>" 
                <?php if ($_smarty_tpl->tpl_vars['facet']->value['properties']['multi_selection'] || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight') {?> data-upper="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['upper'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['upper'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['max'], ENT_QUOTES, 'UTF-8');
}?>" <?php }?>
                data-nolower="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['nolower'])) {?>1<?php } else { ?>0<?php }?>"
                data-values="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['values'])) {
echo htmlspecialchars(implode('#',$_smarty_tpl->tpl_vars['facet']->value['properties']['values']), ENT_QUOTES, 'UTF-8');
}?>" 
                data-prefix="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['prefix'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['prefix'], ENT_QUOTES, 'UTF-8');
}?>" 
                data-suffix="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['suffix'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['suffix'], ENT_QUOTES, 'UTF-8');
}?>" 
                data-slider-label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['label'], ENT_QUOTES, 'UTF-8');?>
" 
                data-slider-unit="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['unit'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['unit'], ENT_QUOTES, 'UTF-8');
}?>" 
                data-pips="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['pips'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['pips'], ENT_QUOTES, 'UTF-8');
}?>"
                data-vertical="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['vertical'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['vertical'], ENT_QUOTES, 'UTF-8');
}?>"
                data-tooltips="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['tooltips']) && $_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['tooltips']) {?>1<?php }?>"
                data-numerical="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['numerical']) && $_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['numerical']) {?>1<?php }?>"
                data-step="<?php if (isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['numerical_step'])) {
echo htmlspecialchars((int)$_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['numerical_step'], ENT_QUOTES, 'UTF-8');
} else { ?>1<?php }?>"></div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['with_inputs']->value && ($_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight') && isset($_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['vertical']) && $_smarty_tpl->tpl_vars['facet']->value['properties']['facet_item']['vertical']) {?>
                <div class="st-range-bottom st-range-bar <?php if ($_smarty_tpl->tpl_vars['with_inputs']->value && ($_smarty_tpl->tpl_vars['facet']->value['type'] == 'price' || $_smarty_tpl->tpl_vars['facet']->value['type'] == 'weight')) {?> with_inputs <?php }?>">
                  <input class="st_upper_input form-control" />
                </div>
                <?php }
}
}
