<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb7517eb0_80751098',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '20f9db37667771fb7c34b781bf4229d01bc49a63' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb7517eb0_80751098 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_price']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_price'] == '1' && $_smarty_tpl->tpl_vars['additional_product']->value->show_price && !isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value) && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
    <?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['price_display'] >= 0 && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['price_display'] <= 2) {?>
        <?php $_smarty_tpl->_assignInScope('has_discount', false);?>
        <?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_reduction']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['display_reduction'] == 1 && isset($_smarty_tpl->tpl_vars['additional_product']->value->specific_price) && $_smarty_tpl->tpl_vars['additional_product']->value->specific_price && isset($_smarty_tpl->tpl_vars['additional_product']->value->specific_price['reduction']) && $_smarty_tpl->tpl_vars['additional_product']->value->specific_price['reduction'] > 0) {?>
            <?php $_smarty_tpl->_assignInScope('has_discount', true);?>
        <?php }?>
        <div class="price product-price">
            <span class="price-reduction price">
                <?php if ($_smarty_tpl->tpl_vars['has_discount']->value) {?>
                    <span class="old-price product-price regular-price">
                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_price_without_reduction']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                    </span>
                <?php }?>
            </span>
            <span class="current-price<?php if ($_smarty_tpl->tpl_vars['has_discount']->value) {?> has-discount<?php }?>">
                <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_price']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                <?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['tax_enabled'] && ((isset($_smarty_tpl->tpl_vars['configuration']->value['display_taxes_label']) && $_smarty_tpl->tpl_vars['configuration']->value['display_taxes_label'] == 1) || !isset($_smarty_tpl->tpl_vars['configuration']->value['display_taxes_label']))) {?>
                    <?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['price_display'] == 1) {?> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'tax excl.','mod'=>'additionalproductsorder'),$_smarty_tpl ) );
} else { ?> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'tax incl.','mod'=>'additionalproductsorder'),$_smarty_tpl ) );
}?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['has_discount']->value && $_smarty_tpl->tpl_vars['additional_product']->value->specific_price['reduction_type'] == 'percentage') {?>
                    <span class="discount discount-percentage">-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['additional_product']->value->specific_price['reduction']*intval(100), ENT_QUOTES, 'UTF-8');?>
%</span>
                <?php }?>
            </span>
            <?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['price_display'] == 2) {?>
                <span id="pretaxe-price"><span id="pretaxe-price-display"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_price_tax_excluded']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'tax excl.','mod'=>'additionalproductsorder'),$_smarty_tpl ) );?>
</span>
            <?php }?>
        </div>
    <?php }
}
}
}
