<?php
/* Smarty version 3.1.33, created on 2021-01-13 15:33:00
  from '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/admin/_configure/helpers/view/view_custom.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff58fc76b090_55809537',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a837dc478a99ef32300824dcf99aa39063690f0b' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/admin/_configure/helpers/view/view_custom.tpl',
      1 => 1599623794,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fff58fc76b090_55809537 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="steps_to_configure_google_API" class="panel steps_to_config_google_API">
<span class="vss-google-link_google_API">
    <h3><a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click here to get Google map API key','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a></h3> </span>

    <div class="steps_panel-heading">
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEPS TO CONFIGURE Google API:','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>

    </div>
    <div class="">
        <div class="panel-group kbstorelocatorpickup_step_accordion" id="kbstorelocatorpickup_dropbox_accordion">
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse1"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 1','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step1.png' style="width: 100%"/>                 </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 2','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step2.png' style="width: 100%"/>                 </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 3','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step3.png' style="width: 100%"/>                 </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse45"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 4 & 5','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse45" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step4_5.png' style="width: 100%"/>                 </div>
            </div>
        </div>        
                
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 6','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step6.png' style="width: 100%"/>                 </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse7"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 7','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step7.png' style="width: 100%"/>                 </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse8"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 8','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse8" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step8.png' style="width: 100%"/>                 </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse9"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STEP 9','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a>
            </div>
            <div id="dropbox_step_collapse9" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='<?php echo $_smarty_tpl->tpl_vars['mod_dir']->value;?>
kbstorelocatorpickup/views/img/admin/Step9.png' style="width: 100%"style="width: 100%"/>                 </div>
            </div>
        </div>
    </div>
    </div>
</div>
                

<?php }
}
