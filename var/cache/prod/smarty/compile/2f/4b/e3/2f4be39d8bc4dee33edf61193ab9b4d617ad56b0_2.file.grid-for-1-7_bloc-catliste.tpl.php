<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:28:28
  from '/home/renatonunez/public_html/tienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-catliste.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007f3c6b93d5_38987999',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2f4be39d8bc4dee33edf61193ab9b4d617ad56b0' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-catliste.tpl',
      1 => 1599962465,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007f3c6b93d5_38987999 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<div class="block-categories">
	<h4 class="title_block">
		<?php if ($_smarty_tpl->tpl_vars['prestablog_categorie_courante']->value->id) {?>
			<?php if ($_smarty_tpl->tpl_vars['prestablog_categorie_parent']->value->id == 0 && $_smarty_tpl->tpl_vars['prestablog_categorie_courante']->value->id != 0) {?>
				<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );?>
" class="link_block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Blog','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a>&nbsp;>
			<?php } elseif ($_smarty_tpl->tpl_vars['prestablog_categorie_parent']->value->id > 0) {?>
				<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('c'=>$_smarty_tpl->tpl_vars['prestablog_categorie_parent']->value->id,'titre'=>$_smarty_tpl->tpl_vars['prestablog_categorie_parent']->value->link_rewrite),$_smarty_tpl ) );?>
" class="link_block"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_categorie_parent']->value->title,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a>&nbsp;>
			<?php }?>
			<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_categorie_courante']->value->title,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

		<?php } else { ?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Blog categories','mod'=>'prestablog'),$_smarty_tpl ) );?>

		<?php }?>
	</h4>
	<div class="block_content" id="prestablog_catliste">
		<?php if (sizeof($_smarty_tpl->tpl_vars['ListeBlocCatNews']->value)) {?>
			<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_tree']) {?>
				<ul class="prestablogtree <?php if ($_smarty_tpl->tpl_vars['isDhtml']->value) {?>dhtml<?php }?> category-sub-menu">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ListeBlocCatNews']->value, 'Item', false, NULL, 'myLoop', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Item']->value) {
?>
					<li data-depth="0">

							<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('c'=>$_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'],'titre'=>$_smarty_tpl->tpl_vars['Item']->value['link_rewrite']),$_smarty_tpl ) );?>
" class="link_block">
								<?php if (isset($_smarty_tpl->tpl_vars['Item']->value['image_presente']) && $_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_showthumb']) {?><img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
c/adminth_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie']), ENT_QUOTES, 'UTF-8');?>
.jpg?<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['md5pic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['link_rewrite'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="catblog_img lastlisteimg" /><?php }?>
								<strong class="catblog_title"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</strong>
								<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_shownbnews'] && $_smarty_tpl->tpl_vars['Item']->value['nombre_news_recursif'] > 0) {?>&nbsp;<span class="catblog_nb_news">(<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['Item']->value['nombre_news_recursif']), ENT_QUOTES, 'UTF-8');?>
)</span><?php }?>
							</a>
							<div class="navbar-toggler collapse-icons collapsed" data-toggle="collapse" data-target="#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'], ENT_QUOTES, 'UTF-8');?>
"><i class="material-icons add"></i><i class="material-icons remove"></i></div>

							<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'], ENT_QUOTES, 'UTF-8');?>
" class="collapse">

							<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_rss']) {?><a target="_blank" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('rss'=>$_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie']),$_smarty_tpl ) );?>
"data-depth="1" class="link_block"><img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_dir']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
/img/rss.png" alt="Rss feed" align="absmiddle" /></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_showintro']) {?>
							<a class="catblog_desc" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('c'=>$_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'],'titre'=>$_smarty_tpl->tpl_vars['Item']->value['link_rewrite']),$_smarty_tpl ) );?>
" data-depth="1" class="link_block"><br /><span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['description_crop'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span></a><?php }?>
						<?php if (count($_smarty_tpl->tpl_vars['Item']->value['children']) > 0) {?>
							<?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['tree_branch_path']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('node'=>$_smarty_tpl->tpl_vars['Item']->value['children']), 0, true);
?>
						<?php }?>

						</div>

					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</ul>
			<?php } else { ?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ListeBlocCatNews']->value, 'Item', false, NULL, 'myLoop', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Item']->value) {
?>

						<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('c'=>$_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'],'titre'=>$_smarty_tpl->tpl_vars['Item']->value['link_rewrite']),$_smarty_tpl ) );?>
" class="link_block">
							<?php if (isset($_smarty_tpl->tpl_vars['Item']->value['image_presente']) && $_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_showthumb']) {?><img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
c/adminth_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie']), ENT_QUOTES, 'UTF-8');?>
.jpg?<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['md5pic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['link_rewrite'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="lastlisteimg" /><?php }?>
							<strong><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</strong>
							<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_shownbnews'] && $_smarty_tpl->tpl_vars['Item']->value['nombre_news_recursif'] > 0) {?>&nbsp;<span>(<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['Item']->value['nombre_news_recursif']), ENT_QUOTES, 'UTF-8');?>
)</span><?php }?>
						</a>
													<div class="navbar-toggler collapse-icons collapsed" data-toggle="collapse" data-target="#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'], ENT_QUOTES, 'UTF-8');?>
"><i class="material-icons add"></i><i class="material-icons remove"></i></div>
<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'], ENT_QUOTES, 'UTF-8');?>
" class="collapse">
						<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_rss']) {?><a target="_blank" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('rss'=>$_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie']),$_smarty_tpl ) );?>
" class="link_block"><img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_dir']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
/img/rss.png" alt="Rss feed" align="absmiddle" /></a><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_showintro']) {?>
						<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('c'=>$_smarty_tpl->tpl_vars['Item']->value['id_prestablog_categorie'],'titre'=>$_smarty_tpl->tpl_vars['Item']->value['link_rewrite']),$_smarty_tpl ) );?>
" class="link_block"><br /><span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['description_crop'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span></a><?php }?>
					</div>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			<?php }?>
		<?php } else { ?>
			<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No subcategories','mod'=>'prestablog'),$_smarty_tpl ) );?>
</p>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_showall']) {?><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );?>
" class="btn-primary btn_link"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'See all','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a><?php }?>
	</div>
</div>
<!-- /Module Presta Blog -->

<?php }
}
