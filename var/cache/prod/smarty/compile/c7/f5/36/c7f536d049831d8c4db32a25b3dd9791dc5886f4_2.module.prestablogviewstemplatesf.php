<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:28:26
  from 'module:prestablogviewstemplatesf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007f3aa28ac4_06303200',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7f536d049831d8c4db32a25b3dd9791dc5886f4' => 
    array (
      0 => 'module:prestablogviewstemplatesf',
      1 => 1602302335,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007f3aa28ac4_06303200 (Smarty_Internal_Template $_smarty_tpl) {
?>


<!-- Module Presta Blog -->

    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displaySlider'),$_smarty_tpl ) );?>


<h1>

    <?php if (isset($_smarty_tpl->tpl_vars['prestablog_title_h1']->value)) {?>

		<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_title_h1']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
<br>

    <?php }?>

	

    <?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_catnews_shownbnews'] == 1) {?>



    <span><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['NbNews']->value), ENT_QUOTES, 'UTF-8');?>


    <?php if ($_smarty_tpl->tpl_vars['NbNews']->value <> 1) {?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'articles','mod'=>'prestablog'),$_smarty_tpl ) );?>


    <?php } else { ?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'article','mod'=>'prestablog'),$_smarty_tpl ) );?>


    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value)) {?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'in the category','mod'=>'prestablog'),$_smarty_tpl ) );?>
&nbsp;<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->title,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>


    <?php }?>

    </span>

  <?php }?>

</h1>

<?php if (sizeof($_smarty_tpl->tpl_vars['news']->value)) {?>

    <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['prestablog_pagination']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

 <?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_article_page'] == 1) {?>

    <ul id="blog_list_1-7" class="one_article">

        <?php } elseif ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_article_page'] == 2) {?>

        <ul id="blog_list_1-7" class="two_article">

        <?php } elseif ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_article_page'] == 3) {?>

        <ul id="blog_list_1-7" class="three_article">

          <?php }?>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['news']->value, 'news_item', false, NULL, 'NewsName', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['news_item']->value) {
?>

        <li class="blog-grid">

            <div class="block_cont">

                <div class="block_top">

                <?php if (isset($_smarty_tpl->tpl_vars['news_item']->value['image_presente'])) {?>

                    <a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('id'=>$_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news'],'seo'=>$_smarty_tpl->tpl_vars['news_item']->value['link_rewrite'],'titre'=>$_smarty_tpl->tpl_vars['news_item']->value['title']),$_smarty_tpl ) );?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['news_item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">

                      <span class="date_blog-cat date_blog_aghaso"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'','mod'=>'prestablog'),$_smarty_tpl ) );?>

                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>$_smarty_tpl->tpl_vars['news_item']->value['date'],'full'=>0),$_smarty_tpl ) );?>

                        </span>

                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
thumb_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news']), ENT_QUOTES, 'UTF-8');?>
.jpg?<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['md5pic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['news_item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />

                    </a>

                <?php }?>

                </div>

                <div class="block_bas">

                    <h3>

                        <a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('id'=>$_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news'],'seo'=>$_smarty_tpl->tpl_vars['news_item']->value['link_rewrite'],'titre'=>$_smarty_tpl->tpl_vars['news_item']->value['title']),$_smarty_tpl ) );?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['news_item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['news_item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a>

                    <br /><!--span class="date_blog-cat"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Published :','mod'=>'prestablog'),$_smarty_tpl ) );?>


                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>$_smarty_tpl->tpl_vars['news_item']->value['date'],'full'=>0),$_smarty_tpl ) );?>


                <?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_author_actif']) {?>

                <?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_author_cate_actif']) {?>



                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['news_item']->value['authors'], 'author', false, 'key', 'current', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['author']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['total'];
?>

                                <?php if ($_smarty_tpl->tpl_vars['key']->value == "firstname") {?>

                                    <?php $_smarty_tpl->_assignInScope('firstname', $_smarty_tpl->tpl_vars['author']->value);?>

                                <?php }?>

                                <?php if ($_smarty_tpl->tpl_vars['key']->value == "pseudo") {?>

                                    <?php $_smarty_tpl->_assignInScope('pseudo', $_smarty_tpl->tpl_vars['author']->value);?>

                                <?php }?>



                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                            <?php if ($_smarty_tpl->tpl_vars['firstname']->value) {?>

                                                    -

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'By','mod'=>'prestablog'),$_smarty_tpl ) );?>


<?php }?>

                    <a href="<?php ob_start();
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['firstname']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
$_prefixVariable1 = ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('au'=>$_smarty_tpl->tpl_vars['news_item']->value['author_id'],'titre'=>$_prefixVariable1),$_smarty_tpl ) );?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['firstname']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a>





                <?php }?>

                <?php }?>



                            <?php if (sizeof($_smarty_tpl->tpl_vars['news_item']->value['categories'])) {?> | <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Categories :','mod'=>'prestablog'),$_smarty_tpl ) );?>


                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['news_item']->value['categories'], 'categorie', false, 'key', 'current', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['categorie']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['total'];
?>

                                    <a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('c'=>$_smarty_tpl->tpl_vars['key']->value,'titre'=>$_smarty_tpl->tpl_vars['categorie']->value['link_rewrite']),$_smarty_tpl ) );?>
" class="categorie_blog"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['categorie']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a>

                                    <?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_current']->value['last'] : null)) {?>,<?php }?>

                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                            <?php }?></span-->





<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_rating_actif']) {?>

                             <div class="star_content">

<?php
$_smarty_tpl->tpl_vars['__smarty_section_i'] = new Smarty_Variable(array());
if (true) {
for ($__section_i_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index'] = 0; $__section_i_0_iteration <= 5; $__section_i_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index']++){
?>

    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_i']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index'] : null) < $_smarty_tpl->tpl_vars['news_item']->value['average_rating']) {?>

                    <div class="material-icons checked">star</div>

                        <?php } elseif ($_smarty_tpl->tpl_vars['news_item']->value['average_rating'] == 5) {?>

                        <div class="material-icons checked">star</div>

    <?php } else { ?>

        <div class="material-icons">star</div>

    <?php }?>

<?php
}
}
?>

</div>

<?php }?>



                    </h3>

                     <?php if ($_smarty_tpl->tpl_vars['news_item']->value['paragraph_crop'] != '') {?>

                         <p class="prestablog_desc">

                                <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['news_item']->value['paragraph_crop'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>


                        </p>

                     <?php }?>

                </div>

                   

                          <div class="prestablog_more">

                        <a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('id'=>$_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news'],'seo'=>$_smarty_tpl->tpl_vars['news_item']->value['link_rewrite'],'titre'=>$_smarty_tpl->tpl_vars['news_item']->value['title']),$_smarty_tpl ) );?>
" class="blog_link"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Read more','mod'=>'prestablog'),$_smarty_tpl ) );?>
<i class="fa fa-chevron-right green pl-2 ico-read" aria-hidden="true"></i></a>

                            <?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_comment_actif'] == 1) {?>

                                <a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('id'=>$_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news'],'seo'=>$_smarty_tpl->tpl_vars['news_item']->value['link_rewrite'],'titre'=>$_smarty_tpl->tpl_vars['news_item']->value['title']),$_smarty_tpl ) );?>
#comment" class="comments"><i class="material-icons">comment</i>  <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news_item']->value['count_comments']), ENT_QUOTES, 'UTF-8');?>
</a>

                            <?php }?>

							<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_read_actif']) {?>

									<span><i class="material-icons">remove_red_eye</i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['news_item']->value['read'], ENT_QUOTES, 'UTF-8');?>
</span>

							<?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_commentfb_actif'] == 1) {?>

                                <a

                                    href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('id'=>$_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news'],'seo'=>$_smarty_tpl->tpl_vars['news_item']->value['link_rewrite'],'titre'=>$_smarty_tpl->tpl_vars['news_item']->value['title']),$_smarty_tpl ) );?>
#comment"

                                    id="showcomments<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news']), ENT_QUOTES, 'UTF-8');?>
"

                                    class="comments"

                                    data-commentsurl="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('id'=>$_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news'],'seo'=>$_smarty_tpl->tpl_vars['news_item']->value['link_rewrite'],'titre'=>$_smarty_tpl->tpl_vars['news_item']->value['title']),$_smarty_tpl ) );?>
"

                                    data-commentsidnews="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news_item']->value['id_prestablog_news']), ENT_QUOTES, 'UTF-8');?>
"

                                    ><i class="material-icons">comment</i> <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news_item']->value['count_comments']), ENT_QUOTES, 'UTF-8');?>


                                </a>

                            <?php }?>

                 </div>

                    

              </div>

        </li>

    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

    </ul>

    <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['prestablog_pagination']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php } else { ?>

    <p class="warning"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Empty','mod'=>'prestablog'),$_smarty_tpl ) );?>
</p>



<?php }?>

<!-- /Module Presta Blog -->

<?php }
}
