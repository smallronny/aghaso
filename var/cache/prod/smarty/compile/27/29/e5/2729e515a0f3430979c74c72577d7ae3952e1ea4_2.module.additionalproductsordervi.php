<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb7428d06_67686655',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2729e515a0f3430979c74c72577d7ae3952e1ea4' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb7428d06_67686655 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['is_active'] == true) {?>
	<?php if (!$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['is_refresh']) {?>
		<section class="lineven-additionalproductsorder">
    <?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['sections']) && $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['sections']) {?>
			<?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['title'], ENT_QUOTES, 'UTF-8');
$_prefixVariable1 = ob_get_clean();
$_smarty_tpl->_assignInScope('section_title', $_prefixVariable1);?>
			<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12818348946000ceb740e172_05136416', "lapo_before_sections");
?>

			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['sections'], 'section', false, 'section_key', 'section', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['section_key']->value => $_smarty_tpl->tpl_vars['section']->value) {
?>
				<?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['is_separate_results']) {?>
					<?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['section']->value['title'], ENT_QUOTES, 'UTF-8');
$_prefixVariable2 = ob_get_clean();
$_smarty_tpl->_assignInScope('section_title', $_prefixVariable2);?>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['section']->value['products'] && count($_smarty_tpl->tpl_vars['section']->value['products'])) {?>
					<section class="lineven-additionalproductsorder-section">
						<div class="apo-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['hook_class_name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
							<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5898688776000ceb7415350_93045707', "lapo_header");
?>

								<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15781027846000ceb7417536_71758807', "lapo_title");
?>

								<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15416889616000ceb7418060_99626547', "lapo_content_header");
?>

								<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19612968766000ceb7418f12_09334989', "lapo_content");
?>

								<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10092581706000ceb7424fc4_98080599', "lapo_content_footer");
?>

							<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3193369676000ceb7425c61_15750269', "lapo_footer");
?>

						</div>
					</section>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3333764676000ceb7426cd4_03025180', "lapo_after_sections");
?>

		<?php }?>
	<?php if (!$_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['is_refresh']) {?>
		</section>
    <?php }
}
}
/* {block "lapo_before_sections"} */
class Block_12818348946000ceb740e172_05136416 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_before_sections' => 
  array (
    0 => 'Block_12818348946000ceb740e172_05136416',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_before_sections"} */
/* {block "lapo_header"} */
class Block_5898688776000ceb7415350_93045707 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_header' => 
  array (
    0 => 'Block_5898688776000ceb7415350_93045707',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

								<div class="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['template_class_name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
							<?php
}
}
/* {/block "lapo_header"} */
/* {block "lapo_title"} */
class Block_15781027846000ceb7417536_71758807 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_title' => 
  array (
    0 => 'Block_15781027846000ceb7417536_71758807',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_title"} */
/* {block "lapo_content_header"} */
class Block_15416889616000ceb7418060_99626547 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_header' => 
  array (
    0 => 'Block_15416889616000ceb7418060_99626547',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_content_header"} */
/* {block "lapo_product"} */
class Block_3963948866000ceb7423a04_06653839 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_product"} */
/* {block "lapo_content"} */
class Block_19612968766000ceb7418f12_09334989 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content' => 
  array (
    0 => 'Block_19612968766000ceb7418f12_09334989',
  ),
  'lapo_product' => 
  array (
    0 => 'Block_3963948866000ceb7423a04_06653839',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['section']->value['products'], 'additional_product', false, NULL, 'additional_product', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['additional_product']->value) {
?>
										<?php $_smarty_tpl->_assignInScope('id_product_attribute', '');?>
										<?php $_smarty_tpl->_assignInScope('product_attribute_designation', '');?>
										<?php if ($_smarty_tpl->tpl_vars['additional_product']->value->has_attributes) {?>
											<?php $_smarty_tpl->_assignInScope('id_product_attribute', $_smarty_tpl->tpl_vars['additional_product']->value->id_product_attribute);?>
											<?php $_smarty_tpl->_assignInScope('product_attribute_designation', $_smarty_tpl->tpl_vars['additional_product']->value->attribute_designation);?>
										<?php }?>
										<?php $_smarty_tpl->_assignInScope('product_price', $_smarty_tpl->tpl_vars['additional_product']->value->price);?>
										<?php $_smarty_tpl->_assignInScope('product_price_tax_excluded', $_smarty_tpl->tpl_vars['additional_product']->value->price_tax_excluded);?>
										<?php $_smarty_tpl->_assignInScope('product_price_without_reduction', $_smarty_tpl->tpl_vars['additional_product']->value->price_without_reduction);?>
										<?php if ($_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['price_display'] == 1) {?>
											<?php $_smarty_tpl->_assignInScope('product_price', $_smarty_tpl->tpl_vars['additional_product']->value->price_tax_excluded);?>
											<?php $_smarty_tpl->_assignInScope('product_price_without_reduction', $_smarty_tpl->tpl_vars['additional_product']->value->price_without_reduction_tax_excluded);?>
										<?php }?>
										<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3963948866000ceb7423a04_06653839', "lapo_product", $this->tplIndex);
?>

									<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
								<?php
}
}
/* {/block "lapo_content"} */
/* {block "lapo_content_footer"} */
class Block_10092581706000ceb7424fc4_98080599 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_content_footer' => 
  array (
    0 => 'Block_10092581706000ceb7424fc4_98080599',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_content_footer"} */
/* {block "lapo_footer"} */
class Block_3193369676000ceb7425c61_15750269 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_footer' => 
  array (
    0 => 'Block_3193369676000ceb7425c61_15750269',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

								</div>
							<?php
}
}
/* {/block "lapo_footer"} */
/* {block "lapo_after_sections"} */
class Block_3333764676000ceb7426cd4_03025180 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'lapo_after_sections' => 
  array (
    0 => 'Block_3333764676000ceb7426cd4_03025180',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "lapo_after_sections"} */
}
