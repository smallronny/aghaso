<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:23:13
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007e01e4b8a6_65001354',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f6239e7143ceacd990510abfa0750ae867346298' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007e01e4b8a6_65001354 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['listing']->value['rendered_facets'])) {?>
	<?php $_smarty_tpl->_assignInScope('show_on', Configuration::get('ST_FAC_SEARCH_SHOW_ON'));?>
	<div id="feds_search_filters" class="feds_show_on_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_on']->value, ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['show_on']->value != 2) {?> feds_show_on_x <?php }?>">
  	<?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_facets'];?>

	</div>
<?php }
}
}
