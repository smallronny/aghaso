<?php
/* Smarty version 3.1.33, created on 2021-01-13 15:33:00
  from '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/admin/_configure/helpers/form/Form_custom.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff58fc7ab1d9_67727498',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7c189631f3710b1d6fd89e1f13b46a5dea8c945f' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/admin/_configure/helpers/form/Form_custom.tpl',
      1 => 1599623794,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fff58fc7ab1d9_67727498 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17376137305fff58fc79d719_09667912', 'defaultForm');
?>

    
<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'helpers/form/form.tpl');
}
/* {block 'defaultForm'} */
class Block_17376137305fff58fc79d719_09667912 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'defaultForm' => 
  array (
    0 => 'Block_17376137305fff58fc79d719_09667912',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <?php echo $_smarty_tpl->tpl_vars['kb_tabs']->value;?>
     <?php echo $_smarty_tpl->tpl_vars['form']->value;?>
         <?php echo $_smarty_tpl->tpl_vars['view']->value;?>
     <?php echo '<script'; ?>
 type="text/javascript">
        var validate_css_length = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Length of CSS should be less than 10000','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
        var kb_numeric = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be numeric.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
        var kb_positive = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be positive.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            velovalidation.setErrorLanguage({
            alphanumeric: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be alphanumeric.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            digit_pass: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password should contain atleast 1 digit.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            empty_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be empty.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            number_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You can enter only numbers.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",            
            positive_number: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Number should be greater than 0.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            minchar_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be less than # character(s).','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            invalid_date: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Invalid date format.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            valid_amount: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be numeric.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            valid_decimal: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field can have only upto two decimal values.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_size: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Size cannot be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            specialchar_size: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Size should not have special characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_bar: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Barcode cannot be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            positive_amount: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be positive.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_color: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Color could not be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            invalid_color: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Color is not valid.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            specialchar: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Special characters are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            script: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Script tags are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            style: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Style tags are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            iframe: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Iframe tags are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
              not_image: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Uploaded file is not an image','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            image_size: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Uploaded file size must be less than #.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            html_tags: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should not contain HTML tags.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            number_pos: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You can enter only positive numbers.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
});
    <?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'defaultForm'} */
}
