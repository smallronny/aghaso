<?php
/* Smarty version 3.1.33, created on 2021-01-14 12:28:28
  from '/home/renatonunez/public_html/tienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-lastliste.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60007f3c5508e9_13942816',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd91c1d8c7b15f2f5e9a398c214aa4e5a6b48c068' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-lastliste.tpl',
      1 => 1599962465,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60007f3c5508e9_13942816 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<div class="block-categories">
	<h4 class="title_block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last blog articles','mod'=>'prestablog'),$_smarty_tpl ) );?>
</h4>
	<div class="block_content" id="prestablog_lastliste">
		<?php if ($_smarty_tpl->tpl_vars['ListeBlocLastNews']->value) {?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ListeBlocLastNews']->value, 'Item', false, NULL, 'myLoop', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Item']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['total'];
?>
				<p>
					<?php if (isset($_smarty_tpl->tpl_vars['Item']->value['link_for_unique'])) {?><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('id'=>$_smarty_tpl->tpl_vars['Item']->value['id_prestablog_news'],'seo'=>$_smarty_tpl->tpl_vars['Item']->value['link_rewrite'],'titre'=>$_smarty_tpl->tpl_vars['Item']->value['title']),$_smarty_tpl ) );?>
" class="link_block"><?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['Item']->value['image_presente']) && $_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_lastnews_showthumb']) {?>
							<img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
adminth_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['Item']->value['id_prestablog_news']), ENT_QUOTES, 'UTF-8');?>
.jpg?<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['md5pic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="lastlisteimg" />
						<?php }?>
						<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

						<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_lastnews_showintro']) {?><br /><span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Item']->value['paragraph_crop'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span><?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['Item']->value['link_for_unique'])) {?></a><?php }?>
				</p>
				<?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['last'] : null)) {
}?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		<?php } else { ?>
			<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No news','mod'=>'prestablog'),$_smarty_tpl ) );?>
</p>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_lastnews_showall']) {?><div class="clearblog"></div><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );?>
" class="btn-primary btn_link"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'See all','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a><?php }?>
	</div>
</div>
<!-- /Module Presta Blog -->
<?php }
}
