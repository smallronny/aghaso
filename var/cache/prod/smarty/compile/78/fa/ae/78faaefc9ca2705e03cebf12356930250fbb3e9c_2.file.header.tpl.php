<?php
/* Smarty version 3.1.33, created on 2020-10-08 22:10:44
  from '/home/desarrollo1webti/public_html/aghaso/modules/psproductcountdownpro/views/templates/hook/header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f7fd4b4e0d8e0_54101921',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78faaefc9ca2705e03cebf12356930250fbb3e9c' => 
    array (
      0 => '/home/desarrollo1webti/public_html/aghaso/modules/psproductcountdownpro/views/templates/hook/header.tpl',
      1 => 1601583596,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f7fd4b4e0d8e0_54101921 (Smarty_Internal_Template $_smarty_tpl) {
?><style type="text/css">
    <?php if ($_smarty_tpl->tpl_vars['pspc_custom_css']->value) {?>
        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_custom_css']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

    <?php }?>
</style>

<?php echo '<script'; ?>
 type="text/javascript">
    var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'days','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
',
        'hours': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'hours','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
',
        'minutes': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'min.','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
',
        'seconds': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'sec.','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
'
    };
    var pspc_labels_lang_1 = {
        'days': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'day','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
',
        'hours': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'hour','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
',
        'minutes': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'min.','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
',
        'seconds': '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'sec.','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
'
    };
    var pspc_offer_txt = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Offer ends in:','mod'=>'psproductcountdownpro'),$_smarty_tpl ) );?>
";
    var pspc_theme = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_theme']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var pspc_psv = <?php echo htmlspecialchars(floatval($_smarty_tpl->tpl_vars['psv']->value), ENT_QUOTES, 'UTF-8');?>
;
    var pspc_hide_after_end = <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['pspc_hide_after_end']->value), ENT_QUOTES, 'UTF-8');?>
;
    var pspc_hide_expired = <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['pspc_hide_expired']->value), ENT_QUOTES, 'UTF-8');?>
;
    var pspc_highlight = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_highlight']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var pspc_position_product = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_position_product']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var pspc_position_list = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_position_list']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var pspc_adjust_positions = <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['pspc_adjust_positions']->value), ENT_QUOTES, 'UTF-8');?>
;
    var pspc_promo_side = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pspc_promo_side']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
    var pspc_token = "<?php echo htmlspecialchars(Tools::getToken(false), ENT_QUOTES, 'UTF-8');?>
";
    <?php if ($_smarty_tpl->tpl_vars['pspc_custom_js']->value) {?>
    function pspc_callbackBeforeDisplay($pspc_container, $pspc) {
        try {
            <?php echo $_smarty_tpl->tpl_vars['pspc_custom_js']->value;?>
 /* JS code */
        } catch (e) {
            console.log(e.name + ":" + e.message + "\n" + e.stack);
        }
    }
    <?php }
echo '</script'; ?>
><?php }
}
