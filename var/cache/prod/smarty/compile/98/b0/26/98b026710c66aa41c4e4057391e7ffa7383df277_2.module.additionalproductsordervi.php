<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb7496337_00036430',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98b026710c66aa41c4e4057391e7ffa7383df277' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb7496337_00036430 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_smarty_tpl->tpl_vars['truncate']->value)) {?>
	<?php $_smarty_tpl->_assignInScope('truncate', 50);
}?>

<div class="product-name">
	<a class="lapo-view" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->product_link,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->name,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
	   data-id-association="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_association), ENT_QUOTES, 'UTF-8');?>
" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
">
		<span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['additional_product']->value->name),'html','UTF-8' )),$_smarty_tpl->tpl_vars['truncate']->value,'...' )), ENT_QUOTES, 'UTF-8');?>
</span>
	</a>
</div><?php }
}
