<?php
/* Smarty version 3.1.33, created on 2021-01-13 15:33:00
  from '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/admin/kb_tabs.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff58fc78f159_60430820',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3560ada4ed5ebd29a0ab26e042846dc2966f3935' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/admin/kb_tabs.tpl',
      1 => 1599623794,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fff58fc78f159_60430820 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="kb_custom_tabs kb_custom_panel">
    <span>
        <a class="kb_custom_tab <?php if ($_smarty_tpl->tpl_vars['selected_nav']->value == 'config') {?>kb_active<?php }?>" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'General Settings','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
" id="kbsl_config_link" href="<?php echo $_smarty_tpl->tpl_vars['admin_sl_setting_controller']->value;?>
">            <i class="icon-gear"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'General Settings','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>

        </a>
    </span>

    <span>
        <a class="kb_custom_tab <?php if ($_smarty_tpl->tpl_vars['selected_nav']->value == 'slpickupsetting') {?>kb_active<?php }?>" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Pickup Time Settings','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
" id="kbsl_pickup_time" href="<?php echo $_smarty_tpl->tpl_vars['admin_sl_pickup_time_controller']->value;?>
">            <i class="icon-clock-o"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Pickup Time Settings','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>

        </a>
    </span>
    <span>
        <a class="kb_custom_tab <?php if ($_smarty_tpl->tpl_vars['selected_nav']->value == 'slemailnotification') {?>kb_active<?php }?>" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email Notification','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
" id="kbsl_email_notification" href="<?php echo $_smarty_tpl->tpl_vars['admin_sl_email_controller']->value;?>
">            <i class="icon-envelope"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email Notification','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>

        </a>
    </span>
    <span>
        <a class="kb_custom_tab <?php if ($_smarty_tpl->tpl_vars['selected_nav']->value == 'slstoreimport') {?>kb_active<?php }?>" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Import Store Contacts','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
" id="kbsl_importer" href="<?php echo $_smarty_tpl->tpl_vars['admin_sl_importer_controller']->value;?>
">            <i class="icon-upload"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Import Store Contact','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>

        </a>
    </span>
    <span>
        <a class="kb_custom_tab <?php if ($_smarty_tpl->tpl_vars['selected_nav']->value == 'sldeliveryslip') {?>kb_active<?php }?>" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Download Delivery Slip','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
" id="kbsl_setting_link" href="<?php echo $_smarty_tpl->tpl_vars['admin_sl_delivery_slip_controller']->value;?>
">            <i class="icon-download"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Download Delivery Slip','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>

        </a>
    </span>
</div>
        
        <?php echo '<script'; ?>
>
            var admin_pickup_url = "<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['admin_sl_pickup_time_controller']->value,'quotes','UTF-8' ));?>
";
            var kb_placeholder_text = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select some options.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var no_results_text = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No results match','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var kb_numeric = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be numeric.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var kb_date_format_invalid = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Format is invalid.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var kb_positive = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be positive.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var check_for_all = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Kindly check for all languages.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var no_select = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please select placement','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var empty_field = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be empty','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var maximum_days_less_error = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Maximum days cannot be less or equal to days gap.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var csv_file_empty = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please upload csv file.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var download_sample_label = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Download Sample','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
             var currentText = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Now','mod'=>'kbstorelocatorpickup','js'=>1),$_smarty_tpl ) );?>
';
            var closeText = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Done','mod'=>'kbstorelocatorpickup','js'=>1),$_smarty_tpl ) );?>
';
            var timeOnlyTitle = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Choose Time','mod'=>'kbstorelocatorpickup','js'=>1),$_smarty_tpl ) );?>
';
            var timeText = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Time','mod'=>'kbstorelocatorpickup','js'=>1),$_smarty_tpl ) );?>
';
            var hourText = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Hour','mod'=>'kbstorelocatorpickup','js'=>1),$_smarty_tpl ) );?>
';
            var minuteText = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Minute','mod'=>'kbstorelocatorpickup','js'=>1),$_smarty_tpl ) );?>
';
            var available_store_empty = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select atleast one store','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var enable_store_not_selected = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select store which is selected for enabled store','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var empty_field = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be empty','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var please_check_kb_fields = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Fields cannot be blank.Please check for all the languages in the field.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            var invalid_from_date = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'From date should be less than To date.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
";
            velovalidation.setErrorLanguage({
            alphanumeric: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be alphanumeric.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            digit_pass: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password should contain atleast 1 digit.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            empty_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be empty.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            number_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You can enter only numbers.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",            
            positive_number: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Number should be greater than 0.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            minchar_field: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field cannot be less than # character(s).','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            invalid_date: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Invalid date format.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            valid_amount: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be numeric.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            valid_decimal: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field can have only upto two decimal values.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_size: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Size cannot be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            specialchar_size: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Size should not have special characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_bar: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Barcode cannot be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            positive_amount: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should be positive.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            maxchar_color: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Color could not be greater than # characters.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            invalid_color: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Color is not valid.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            specialchar: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Special characters are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            script: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Script tags are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            style: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Style tags are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            iframe: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Iframe tags are not allowed.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
             not_image: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Uploaded file is not an image','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            image_size: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Uploaded file size must be less than #.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            html_tags: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Field should not contain HTML tags.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
            number_pos: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You can enter only positive numbers.','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
",
});
        <?php echo '</script'; ?>
>
        
<?php }
}
