<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:07:35
  from 'module:additionalproductsordervi' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000ceb74883f9_23531693',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '96a31400434010c292c4d3357f93124e8eebff97' => 
    array (
      0 => 'module:additionalproductsordervi',
      1 => 1601782278,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6000ceb74883f9_23531693 (Smarty_Internal_Template $_smarty_tpl) {
?>
<a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->product_link,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
	title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->name,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
	class="lapo-view product-image"
    data-id-association="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id_association), ENT_QUOTES, 'UTF-8');?>
" data-id-product="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['additional_product']->value->id), ENT_QUOTES, 'UTF-8');?>
"
	>
	<img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->image_link,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
		 width="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['image_width'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
		 height="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lineven']->value['apo']['hook']['datas']['image_height'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
		 alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['additional_product']->value->name,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
</a><?php }
}
