<?php
/* Smarty version 3.1.33, created on 2021-01-13 20:14:31
  from 'module:iqithtmlandbannersviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff9af74c06d1_89655094',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a1f82456bbbd860eb2f9337cfcd5bfd7ddc900f7' => 
    array (
      0 => 'module:iqithtmlandbannersviewste',
      1 => 1598938146,
      2 => 'module',
    ),
    'b74ca73a91234e272faf0555bce247fa03c55481' => 
    array (
      0 => 'module:iqithtmlandbannersviewste',
      1 => 1602523943,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5fff9af74c06d1_89655094 (Smarty_Internal_Template $_smarty_tpl) {
?>

      



    <div id="iqithtmlandbanners-block-5"  class="d-inline-block">

        <div class="rte-content d-inline-block">

            <div class="row">
<div class="col-md">
<div>
<ul class="nav navbar-nav d-inline-flex" style="list-style: none;">
<li class="nav-item">
<ul class="list-inline-mb-0">
<li class="list-inline-item"><a href="/promociones"> Promociones </a></li>
<li class="list-inline-item d-none"><a href="/content/7-simulador-de-ahorro" title=""> Simulador de Ahorro </a></li>
<li class="list-inline-item"><a href="/novedades" title="">Novedades </a></li>
<li class="list-inline-item"><a href="/content/6-contacto" title="">Contacto </a></li>
</ul>
</li>
</ul>
</div>
</div>
<div class="col-md-3 col-12 redes-nav-content">
<ul class="d-flex justify-content-center">
<li>
<div class="redes-nav"><a class="social-inner" href="https://www.facebook.com/aghasoperu/"><img src="/img/cms/iconos/facebook-top.png" alt="Facebook" /></a>
<div></div>
</div>
</li>
<li>
<div class="redes-nav"><a class="social-inner" href="https://www.linkedin.com/company/aghasoperu/"><img src="/img/cms/iconos/linkedin-top.png" alt="Linkedin" /></a></div>
</li>
<li>
<div class="redes-nav"><a class="social-inner" href="https://www.instagram.com/aghasoperu/"><img src="/img/cms/iconos/insta-top.png" alt="Instagram" /></a></div>
</li>
<li>
<div class="redes-nav"><a class="social-inner" href="https://wa.me/51977611634?" target="_blank" rel="noreferrer noopener"><img src="/img/cms/iconos/whast-top.png" alt="WhatsApp" /></a></div>
</li>
</ul>
</div>
</div>

        </div>

    </div>






  
<?php }
}
