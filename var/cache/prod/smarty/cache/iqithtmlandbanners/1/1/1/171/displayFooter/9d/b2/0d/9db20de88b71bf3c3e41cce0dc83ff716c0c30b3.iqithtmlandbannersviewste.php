<?php
/* Smarty version 3.1.33, created on 2021-01-13 20:14:31
  from 'module:iqithtmlandbannersviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fff9af75954d2_59798339',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a1f82456bbbd860eb2f9337cfcd5bfd7ddc900f7' => 
    array (
      0 => 'module:iqithtmlandbannersviewste',
      1 => 1598938146,
      2 => 'module',
    ),
    'b74ca73a91234e272faf0555bce247fa03c55481' => 
    array (
      0 => 'module:iqithtmlandbannersviewste',
      1 => 1602523943,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5fff9af75954d2_59798339 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>

      



    <div id="iqithtmlandbanners-block-4"  class="col col-md block block-toggle block-iqithtmlandbanners-html js-block-toggle">

        <h5 class="block-title first"><span> footer</span></h5>

        <div class="block-content rte-content  block block-toggle block-iqithtmlandbanners-html js-block-toggle">

            <div class="container">
<div class="row footer-center">
<div class="col col-md block block-toggle js-block-toggle">
<h5 class="block-title"><span>Enlaces</span></h5>
<div class="block-content">
<ul class="nav-list">
<li><a href="/">Inicio</a></li>
<li><a href="/content/4-nosotros">Nosotros</a></li>
<li><a href="/module/kbstorelocatorpickup/stores">Nuestras Tiendas</a></li>
<li><a href="/novedades">Novedades</a></li>
<li><a href="/content/6-contacto">Contacto</a></li>
<li><a href="/promociones">Promociones</a></li>
<li class="d-none"><a href="/content/7-simulador-de-ahorro">Simulador de ahorro</a></li>
<li><a href="/content/17-preguntas-frecuentes">Preguntas Frecuentes</a></li>
</ul>
</div>
</div>
<div class="col col-md block block-toggle js-block-toggle">
<h5 class="block-title"><span>Producto</span></h5>
<div class="block-content">
<ul class="nav-list">
<li><a href="/10-linea-para-cocinas">Línea de cocinas</a></li>
<li><a href="/11-linea-de-lavanderia">Línea de lavandería</a></li>
<li><a href="/12-linea-de-climatizacion">Línea de climatización</a></li>
<li><a href="/13-linea-de-refrigeracion">Línea de refrigeración</a></li>
</ul>
</div>
<div class="pl-0 col col-md block block-toggle js-block-toggle">
<h5 class="block-title "><span>Inmobiliaria</span></h5>
<div class="block-content">
<ul class="nav-list">
<li><a href="/content/16-inmobiliarias">Proyectos inmobiliarios</a></li>
<li class="d-none"><a href="/content/15-soluciones-de-construccion-de-instalacion-de-gas">Soluciones de construcción de instalación de gas</a></li>
</ul>
</div>
</div>
</div>
<div class="col col col-md block block-toggle js-block-toggle">
<h5 class="block-title"><span>Servicios</span></h5>
<div class="block-content">
<ul class="nav-list">
<li><a href="/content/18-legales-de-promociones">Legales de promociones</a></li>
<li><a href="/content/9-servicio-y-soporte">Servicio y soporte</a></li>
<li><a href="/content/3-terminos-y-condiciones">Términos y condiciones</a></li>
<li><a href="/content/11-politicas-y-privacidad">Política y privacidad</a></li>
<li><a href="/content/12-libro-de-reclamaciones">Libro de reclamaciones</a></li>
<li><a href="/content/13-protocolos-de-seguridad">Protocolos de seguridad</a></li>
<li><a href="/content/14-politica-de-devoluciones">Política de devoluciones</a></li>
<li><a href="/content/10-financiamiento">Financiamiento</a></li>
</ul>
</div>
</div>
<div class="col col col-md block block-toggle js-block-toggle">
<h5 class="block-title">Contáctanos</h5>
<div class="block-content">
<ul class="nav-list">
<li class="contact">
<p><i class="fa fa-envelope-o mr-1"></i><a href="mailto:consultas@aghaso.com">consultas@aghaso.com</a></p>
</li>
<li class="contact">
<p><i class="fa fa-phone mr-1"></i><a href="tel:080071171">0-800-711-71</a></p>
</li>
<li class="contact">
<p><i class="fa fa-map-marker mr-1"></i>Oficina<br />Jirón Gaspar Hernández Nº 964 Urb. Lima Industrial de Lima</p>
</li>
</ul>
<div class="py-3"><a href="#more-store" id="showStore" class="link-store btn btn-store text-white">Ver Tiendas <i class="fa fa-plus green pl-2"></i></a></div>
</div>
</div>
</div>
</div>

        </div>

    </div>






  
<?php }
}
