<?php
/* Smarty version 3.1.33, created on 2021-01-14 11:24:11
  from 'module:iqitpopupviewstemplatesho' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000702b2ae5e4_01964407',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b166f7c8dc620ec07e6ab5438f4221ad26ff2d3e' => 
    array (
      0 => 'module:iqitpopupviewstemplatesho',
      1 => 1600290751,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6000702b2ae5e4_01964407 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>


<div id="iqitpopup">

    <div class="iqitpopup-close ">

        <div class="iqit-close-checkbox d-none">

<span class="custom-checkbox">

    <input type="checkbox" name="iqitpopup-checkbox" id="iqitpopup-checkbox" checked/>

    <span><i class="fa fa-check checkbox-checked"></i></span>

    <label for="iqitpopup-checkbox">No volver a mostrar</label>

</span>



        </div>

        <div class="iqit-close-popup"> 

        <span class="cross d-flex" title="Cerrar"></span>

        </div>

    </div>





    <div class="iqitpopup-content"><div class="row">
<div class="col-md-6 col-6">
<div class="img-welcome"><img src="/img/cms/bg-pop-welcome.jpg" alt="" width="350" /></div>
</div>
<div class="col-md-6 col-6">
<div class="text-center p-5 mt-4">
<div class="pb-3"><img src="/img/cms/iconos/icon-suscribete.png" alt="" width="70" /></div>
<h1 class="green h1title">Bienvenido</h1>
<p class="blue pt-2">Nos gustaría presentarte promociones y descuentos pensados para ti</p>
</div>
</div>
</div></div>

    
        <div class="iqitpopup-newsletter-form">

            <div class="row">

                <div class="col-12">

                    <form action="//tienda.aghaso.com/?fc=module&module=iqitemailsubscriptionconf&controller=subscription" method="post" class="form-inline justify-content-center">

                        <input class="inputNew form-control grey newsletter-input mr-3" type="text" name="email" size="18"

                               placeholder="Ingresar correo" value=""/>

                        <button type="submit" name="submitNewsletter"

                                class="btn btn-default button button-medium iqit-btn-newsletter">

                            <span>Suscribete</span>

                        </button>

                        <input type="hidden" name="action" value="0"/>

                        <div class="mt-3 text-muted"> </div>

                    </form>

                </div>

            </div>

        </div>

    
</div> <!-- #layer_cart -->

<div id="iqitpopup-overlay"></div><?php }
}
