<?php
/* Smarty version 3.1.33, created on 2021-01-14 18:48:25
  from 'module:iqitsocialloginviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6000d8494695c0_77789431',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '61c5997c2183ea5ba1b537d10008fd4f75175c34' => 
    array (
      0 => 'module:iqitsocialloginviewstempl',
      1 => 1598938145,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6000d8494695c0_77789431 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?><div class="iqitsociallogin iqitsociallogin-checkout iqitsociallogin-colors-native pb-3 pt-1">
        <span class="text-muted pr-1">Or connect with social account:</span>

            <a                 onclick="iqitSocialPopup('//tienda.aghaso.com/module/iqitsociallogin/authenticate?provider=facebook&page=authentication')"
                       class="btn btn-secondary btn-iqitsociallogin btn-facebook btn-sm mt-1 mb-1">
            <i class="fa fa-facebook-square" aria-hidden="true"></i>
            Facebook
        </a>
    
    
    
    </div>

<script type="text/javascript">
    
    function iqitSocialPopup(url) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var left = ((width / 2) - (960 / 2)) + dualScreenLeft;
        var top = ((height / 2) - (600 / 2)) + dualScreenTop;
        var newWindow = window.open(url, '_blank', 'scrollbars=yes,top=' + top + ',left=' + left + ',width=960,height=600');
        if (window.focus) {
            newWindow.focus();
        }
    }
    
</script>




<?php }
}
