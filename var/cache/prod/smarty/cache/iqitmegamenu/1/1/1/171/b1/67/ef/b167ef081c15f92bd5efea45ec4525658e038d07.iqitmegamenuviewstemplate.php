<?php
/* Smarty version 3.1.33, created on 2021-01-15 10:01:50
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6001ae5e6804b5_33286367',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '797404135c3d6163c184d5946c377ac2bc91c4d2' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1604340983,
      2 => 'module',
    ),
    '470d5c96fd175e37e89afd5cc78d331c9756e29d' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1598938145,
      2 => 'module',
    ),
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1603068055,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6001ae5e6804b5_33286367 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home/renatonunez/public_html/tienda/var/cache/prod/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_8092360515fff58941edc13_31013364',
  ),
  'categories_links' => 
  array (
    'compiled_filepath' => '/home/renatonunez/public_html/tienda/var/cache/prod/smarty/compile/47/0d/5c/470d5c96fd175e37e89afd5cc78d331c9756e29d_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => '470d5c96fd175e37e89afd5cc78d331c9756e29d',
    'call_name' => 'smarty_template_function_categories_links_20460862385fff589416ee29_05563426',
  ),
));
?>
	<div id="iqitmegamenu-wrapper" class="iqitmegamenu-wrapper iqitmegamenu-all">

		<div class="container container-iqitmegamenu">

		<div id="iqitmegamenu-horizontal" class="iqitmegamenu  clearfix" role="navigation">



				
				

				<nav id="cbp-hrmenu" class="cbp-hrmenu cbp-horizontal cbp-hrsub-narrow">

					<ul>

						
						<li id="cbp-hrmenu-tab-1" class="cbp-hrmenu-tab cbp-hrmenu-tab-1 ">

	<a href="https://tienda.aghaso.com/" class="nav-link" >




								<span class="cbp-tab-title">


								Inicio</span>

								
						</a>

							
						</li>

						
						<li id="cbp-hrmenu-tab-2" class="cbp-hrmenu-tab cbp-hrmenu-tab-2  cbp-has-submeu">

	<a href="/2-productos" class="nav-link" >




								<span class="cbp-tab-title">


								Productos <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>

								
						</a>

							
							<div class="cbp-hrsub col-12">

								<div class="cbp-hrsub-inner">

									<div class="container iqitmegamenu-submenu-container">

									


										
											
												




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-12 cbp-menu-column cbp-menu-element menu-element-id-9 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <!-- LINEA CATEGORIAS - MEGAMENU-->
<div class="row">
<div class="col-md-3 p-0">
<div class="card text-white"><img loading="lazy" src="/img/cms/linea-cocina-menu-aghaso.jpg" class="card-img" alt="Linea de Cocina Aghaso" />
<div class="card-img-menu-cocina">
<div class="cont-category-menu">
<div class="cont-category-img"><img loading="lazy" src="/img/cms/iconos/icono-ahorro.png" /></div>
<div class="cont-text-category mb-3">
<p class="text-category m-0">Línea <br />de cocinas</p>
</div>
<div>
<p><a class="btn btn-trans" href="/10-linea-para-cocinas"> Ver Productos</a></p>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-3 p-0">
<div class="card text-white"><img loading="lazy" src="/img/cms/linea-lavanderia-menu-aghaso.jpg" class="card-img" alt="Linea de lavandería Aghaso" />
<div class="card-img-menu">
<div class="cont-category-menu">
<div class="cont-category-img"><img loading="lazy" src="/img/cms/iconos/lavanderia-icon.png" /></div>
<div class="cont-text-category mb-3">
<p class="text-category m-0">Línea de <br />Lavandería</p>
</div>
<div>
<p><a class="btn btn-trans" href="/11-linea-de-lavanderia"> Ver Productos</a></p>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-3 p-0">
<div class="card text-white"><img loading="lazy" src="/img/cms/linea-climatizacion-menu-aghaso.jpg" class="card-img" alt="Linea de climatización Aghaso" />
<div class="card-img-menu">
<div class="cont-category-menu">
<div class="cont-category-img"><img loading="lazy" src="/img/cms/iconos/climatizacion-icon.png" /></div>
<div class="cont-text-category mb-3">
<p class="text-category m-0">Línea de<br /> Climatización</p>
</div>
<div>
<p><a class="btn btn-trans" href="/12-linea-de-climatizacion"> Ver Productos</a></p>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-3 p-0">
<div class="card text-white"><img loading="lazy" src="/img/cms/linea-refrigeracion-menu-aghaso.jpg" class="card-img" alt="Linea de Refrigeración Aghaso" />
<div class="card-img-menu">
<div class="cont-category-menu">
<div class="cont-category-img"><img loading="lazy" src="/img/cms/iconos/refrigeracion-icon.png" /></div>
<div class="cont-text-category mb-3">
<p class="text-category m-0">Línea de <br />Refrigeración</p>
</div>
<div>
<p><a class="btn btn-trans" href="/13-linea-de-refrigeracion"> Ver Productos</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END LINEA CATEGORIAS - MEGAMENU-->
                    
                
            

            
            </div>    </div>
                            
                </div>

											
										


									
										</div>

								</div>

							</div>

							
						</li>

						
						<li id="cbp-hrmenu-tab-3" class="cbp-hrmenu-tab cbp-hrmenu-tab-3  cbp-has-submeu">

	<a href="https://tienda.aghaso.com/content/16-inmobiliarias" class="nav-link" >




								<span class="cbp-tab-title">


								Inmobiliarias <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>

								
						</a>

							
							<div class="cbp-hrsub col-12">

								<div class="cbp-hrsub-inner">

									<div class="container iqitmegamenu-submenu-container">

									


										
											
												




<div class="row menu_row menu-element  first_rows menu-element-id-2">
                

                                                




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 cbp-empty-column">
        <div class="cbp-menu-column-inner">
                        
                
                
            

            
            </div>    </div>
                                    




    <div class="col-6 cbp-menu-column cbp-menu-element menu-element-id-3 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <!-- INMOBILIARIA MEGAMENU-->
<div class="row">
<div class="col-md-12 col-12 p-0">
<div class="card text-white"><img loading="lazy" src="/img/cms/aconmpañamos-aghaso.jpg" class="card-img" alt="Proyectos Inmobiliarios" />
<div class="card-img-inmob-menu">
<div class="cont-simulador-ahorro-menu">
<div class="simulador-ahorro-icon"><img loading="lazy" src="/img/cms/svg/icon-check-aghaso.svg" /></div>
<div class="cont-text-simulador-ahorro mb-3">
<p class="text-simulador-ahorro m-0">Acompañamos tus <br />proyectos inmobiliarios,<br /> Cocinas, Lavadoras<br /> Termas y mucho más</p>
</div>
<div>
<p><a class="btn btn-trans" href="/content/16-inmobiliarias"> Ver más</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END INMOBILIARIA MEGAMENU-->
                    
                
            

            
            </div>    </div>
                            
                </div>

											
										


									
										</div>

								</div>

							</div>

							
						</li>

						
						<li id="cbp-hrmenu-tab-4" class="cbp-hrmenu-tab cbp-hrmenu-tab-4 ">

	<a href="https://tienda.aghaso.com/content/4-nosotros" class="nav-link" >




								<span class="cbp-tab-title">


								Nosotros</span>

								
						</a>

							
						</li>

						
						<li id="cbp-hrmenu-tab-5" class="cbp-hrmenu-tab cbp-hrmenu-tab-5 ">

	<a href="https://tienda.aghaso.com/content/9-servicio-y-soporte" class="nav-link" >




								<span class="cbp-tab-title">


								Servicio y Soporte</span>

								
						</a>

							
						</li>

						
						<li id="cbp-hrmenu-tab-6" class="cbp-hrmenu-tab cbp-hrmenu-tab-6 ">

	<a href="/module/kbstorelocatorpickup/stores" class="nav-link" >




								<span class="cbp-tab-title">


								Nuestras Tiendas</span>

								
						</a>

							
						</li>

						
						<li id="cbp-hrmenu-tab-7" class="cbp-hrmenu-tab cbp-hrmenu-tab-7 ">

	<a href="https://tienda.aghaso.com/content/10-financiamiento" class="nav-link" >




								<span class="cbp-tab-title">


								Financiamiento</span>

								
						</a>

							
						</li>

						
					</ul>

				</nav>

		</div>

		</div>

		<div id="sticky-cart-wrapper"></div>

	</div>



<div id="_desktop_iqitmegamenu-mobile">

	<ul id="iqitmegamenu-mobile">

		
  


<ul class="closeMenuMobile" style="
    border-bottom: 2px solid #f3f3f3;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-2">
    <a href="https://tienda.aghaso.com/" >

                <img class="logo img-fluid"

                     src="/img/aghaso-logo-1601437151.jpg"  srcset="/img/cms/aghaso-logo.png 2x"
                     alt="Aghaso">

            </a>
    </div>
    <div class="col-4">
        <a class="close" aria-label="Close" >
          <span><img src="/img/cms/svg/ico-closed.svg"></span>
        </a>
    </div>
</li>
    </ul>
    







	

	<li><a  href="https://tienda.aghaso.com/" >Inicio</a></li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://tienda.aghaso.com/" >Todos nuestros productos</a>

	<ul><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://tienda.aghaso.com/10-linea-de-cocinas" >Línea de cocinas</a>

	<ul><li><a  href="https://tienda.aghaso.com/23-cocinas" >Cocinas</a></li><li><a  href="https://tienda.aghaso.com/24-encimeras" >Encimeras</a></li><li><a  href="https://tienda.aghaso.com/25-hornos" >Hornos</a></li><li><a  href="https://tienda.aghaso.com/27-campana" >Campana</a></li></ul>
</li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://tienda.aghaso.com/11-linea-de-lavanderia" >Línea de lavandería</a>

	<ul><li><a  href="https://tienda.aghaso.com/28-secadoras" >Secadoras</a></li><li><a  href="https://tienda.aghaso.com/29-lavadoras" >Lavadoras</a></li><li><a  href="https://tienda.aghaso.com/30-centro-de-lavado" >Centro de lavado</a></li></ul>
</li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://tienda.aghaso.com/12-linea-de-climatizacion" >Línea de climatización</a>

	<ul><li><a  href="https://tienda.aghaso.com/31-termas" >Termas</a></li></ul>
</li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://tienda.aghaso.com/13-linea-de-refrigeracion" >Línea de refrigeración</a>

	<ul><li><a  href="https://tienda.aghaso.com/26-refrigeradoras" >Refrigeradoras</a></li></ul>
</li></ul>
</li><li><a  href="https://tienda.aghaso.com/content/4-nosotros" >Nosotros</a></li><li><a  href="https://tienda.aghaso.com/content/10-financiamiento" >Financiamiento</a></li><li><a  href="https://tienda.aghaso.com/content/9-servicio-y-soporte" >Servicio y Soporte</a></li><li><a  href="https://tienda.aghaso.com/content/6-contacto" >Contacto</a></li>




	</ul>

</div>

<?php }
}
