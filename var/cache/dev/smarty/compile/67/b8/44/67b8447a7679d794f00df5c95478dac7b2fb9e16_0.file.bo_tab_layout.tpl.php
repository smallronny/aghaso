<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:36:57
  from '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/bo_tab_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf30e9403df0_03786014',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '67b8447a7679d794f00df5c95478dac7b2fb9e16' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/bo_tab_layout.tpl',
      1 => 1601444637,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf30e9403df0_03786014 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="tabbable row">
<div class="st_sidebar col-xs-12 col-lg-2"><ul class="nav nav-tabs">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['bo_tabs']->value, 'tab');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->value) {
?>
	<li class="nav-item"><a href="javascript:;" title="<?php echo $_smarty_tpl->tpl_vars['tab']->value['name'];?>
" data-fieldset="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['tab']->value['name'];?>
</a></li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</ul></div>
<div id="st_tab" class="col-xs-12 col-lg-10 tab-content">
<?php echo $_smarty_tpl->tpl_vars['bo_tab_content']->value;?>
</div>
</div><?php }
}
