<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:36:57
  from '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/_configure/helpers/form/form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf30e932a7d1_84793931',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '11e61017c18a5e2a8af9534750ecceef6b7bb892' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/_configure/helpers/form/form.tpl',
      1 => 1601444637,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf30e932a7d1_84793931 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7683825935fbf30e922c7f4_36315741', "field");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "helpers/form/form.tpl");
}
/* {block "field"} */
class Block_7683825935fbf30e922c7f4_36315741 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'field' => 
  array (
    0 => 'Block_7683825935fbf30e922c7f4_36315741',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<?php if ($_smarty_tpl->tpl_vars['input']->value['type'] == 'dropdownlistgroup') {?>
		<div class="col-lg-<?php if (isset($_smarty_tpl->tpl_vars['input']->value['col'])) {
echo intval($_smarty_tpl->tpl_vars['input']->value['col']);
} else { ?>9<?php }?> <?php if (!isset($_smarty_tpl->tpl_vars['input']->value['label'])) {?>col-lg-offset-3<?php }?> fontello_wrap">
			<div class="row">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values']['medias'], 'media');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['media']->value) {
?>
					<div class="col-xs-4 col-sm-3">
						<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="<?php if ($_smarty_tpl->tpl_vars['media']->value == 'fw') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'If this is set and this module is hooked to the displayFullWidthXXX hooks, then this module will be displayed in full screen.','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'xxl') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Desktops (>1400px)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'xl') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Desktops (>1200px)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'lg') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Desktops (>992px)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'md') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Tablets (>768px)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'sm') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Phones (>544px)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'xs') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Phones (<544px)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
}?>"><?php if ($_smarty_tpl->tpl_vars['media']->value == 'fw') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Full screen','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'xxl') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Extra large devices','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'xl') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Large devices','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'lg') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Medium devices','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'md') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Small devices','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'sm') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Extra small devices','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['media']->value == 'xs') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Extremely small devices','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
}?></label>
						<select name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['media']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_<?php echo $_smarty_tpl->tpl_vars['media']->value;?>
" class="fixed-width-md">
						<?php if ($_smarty_tpl->tpl_vars['media']->value == 'fw') {?><option value="0" <?php if (!$_smarty_tpl->tpl_vars['fields_value']->value[(($_smarty_tpl->tpl_vars['input']->value['name']).("_")).($_smarty_tpl->tpl_vars['media']->value)]) {?> selected="selected" <?php }?>></option><?php }?>
            			<?php
$_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['input']->value['values']['maximum']+1 - (1) : 1-($_smarty_tpl->tpl_vars['input']->value['values']['maximum'])+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration === 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration === $_smarty_tpl->tpl_vars['foo']->total;?>
	                        <option value="<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['fields_value']->value[(($_smarty_tpl->tpl_vars['input']->value['name']).("_")).($_smarty_tpl->tpl_vars['media']->value)] == $_smarty_tpl->tpl_vars['foo']->value) {?> selected="selected" <?php }?>><?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
</option>
	                    <?php }
}
?>
            			</select>
					</div>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['input']->value['desc']) && !empty($_smarty_tpl->tpl_vars['input']->value['desc'])) {?>
				<p class="help-block">
					<?php if (is_array($_smarty_tpl->tpl_vars['input']->value['desc'])) {?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['desc'], 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
							<?php if (is_array($_smarty_tpl->tpl_vars['p']->value)) {?>
								<span id="<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value['text'];?>
</span><br />
							<?php } else { ?>
								<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
<br />
							<?php }?>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					<?php } else { ?>
						<?php echo $_smarty_tpl->tpl_vars['input']->value['desc'];?>

					<?php }?>
				</p>
			<?php }?>
		</div>
	<?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'pc_images') {?>
		<div class="col-lg-9">
			<input name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" id="pc_images" type="hidden" value="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['values']['str'])) {
echo $_smarty_tpl->tpl_vars['input']->value['values']['str'];
}?>" />
			<ul class="pc_images_list clearfix">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values']['data'], 'img');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['img']->value) {
?>
					<li>
						<img src="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
">
						<a href="javascript:;" class="pc_image_item" data-image="<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Remove','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</a>
					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>
		</div>
    <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'go_back_to_list') {?>
		<div class="col-lg-9">
			<a class="btn btn-default btn-block fixed-width-md" href="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Back to list','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
"><i class="icon-arrow-left"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Back to list','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</a>
		</div>
	<?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'show_bg_patterns') {?>
		<div class="col-lg-9">
			<?php
$_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['input']->value['size']+1 - (1) : 1-($_smarty_tpl->tpl_vars['input']->value['size'])+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration === 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration === $_smarty_tpl->tpl_vars['foo']->total;?>
				<div class="parttern_wrap" style="background:url(<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];
echo $_smarty_tpl->tpl_vars['foo']->value;?>
.png);"><span><?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
</span></div>
			<?php }
}
?>
			<div><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Pattern credits:','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
<a href="http://subtlepatterns.com" target="_blank">subtlepatterns.com</a></div>
		</div>
	<?php } else { ?>
		<?php 
$_smarty_tpl->inheritance->callParent($_smarty_tpl, $this, '{$smarty.block.parent}');
?>

	<?php }
}
}
/* {/block "field"} */
}
