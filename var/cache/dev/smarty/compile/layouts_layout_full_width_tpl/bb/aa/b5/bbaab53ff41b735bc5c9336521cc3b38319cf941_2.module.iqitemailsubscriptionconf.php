<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:29:28
  from 'module:iqitemailsubscriptionconf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf2f282676b5_79214572',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bbaab53ff41b735bc5c9336521cc3b38319cf941' => 
    array (
      0 => 'module:iqitemailsubscriptionconf',
      1 => 1598938145,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf2f282676b5_79214572 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /home/renatonunez/public_html/tienda/modules/iqitemailsubscriptionconf/views/templates/front/subscription_execution.tpl -->


 
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19110505125fbf2f2821fe96_34176911', "page_content");
?>


<!-- end /home/renatonunez/public_html/tienda/modules/iqitemailsubscriptionconf/views/templates/front/subscription_execution.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block "page_content"} */
class Block_19110505125fbf2f2821fe96_34176911 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_19110505125fbf2f2821fe96_34176911',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<h1 class="h1 page-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Newsletter subscription','mod'=>'iqitemailsubscriptionconf'),$_smarty_tpl ) );?>
</span></h1>
	<?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['widget_block'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['widget_block'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'smartyWidgetBlock'))) {
throw new SmartyException('block tag \'widget_block\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('widget_block', array('name'=>"ps_emailsubscription"));
$_block_repeat=true;
echo $_block_plugin1->smartyWidgetBlock(array('name'=>"ps_emailsubscription"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>

		<?php if ($_smarty_tpl->tpl_vars['msg']->value) {?>
			<p class="alert <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>alert-danger<?php } else { ?>alert-success<?php }?>">
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>

			</p>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['conditions']->value) {?>
			<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['conditions']->value, ENT_QUOTES, 'UTF-8');?>
</p>
		<?php }?>

	<?php $_block_repeat=false;
echo $_block_plugin1->smartyWidgetBlock(array('name'=>"ps_emailsubscription"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
/* {/block "page_content"} */
}
