<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:19:23
  from '/home/renatonunez/public_html/tienda/themes/warehousechild/templates/customer/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf2ccb7daca0_40693022',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9801dbc6b3062c218e2d471b60f15dbda6408d8f' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/themes/warehousechild/templates/customer/page.tpl',
      1 => 1599157620,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/notifications.tpl' => 1,
    'file:customer/_partials/my-account-links.tpl' => 1,
  ),
),false)) {
function content_5fbf2ccb7daca0_40693022 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19265969575fbf2ccb751c01_23336992', 'page_header_container');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16001644015fbf2ccb757252_70943235', 'notifications');
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17883954495fbf2ccb759cc3_22533979', 'page_content_container');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5148248165fbf2ccb7d39c2_89056183', 'page_footer');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_header_container'} */
class Block_19265969575fbf2ccb751c01_23336992 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_header_container' => 
  array (
    0 => 'Block_19265969575fbf2ccb751c01_23336992',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <header class="page-header">
      <h1 class="h1 page-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your account','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>
</span></h1>
    </header>
<?php
}
}
/* {/block 'page_header_container'} */
/* {block 'notifications'} */
class Block_16001644015fbf2ccb757252_70943235 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_16001644015fbf2ccb757252_70943235',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'notifications'} */
/* {block 'display_customer_account'} */
class Block_102530395fbf2ccb7a4310_95238088 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayCustomerAccount'),$_smarty_tpl ) );?>

          <?php
}
}
/* {/block 'display_customer_account'} */
/* {block 'my_account_side_links'} */
class Block_5380579205fbf2ccb75ba09_93244282 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?>
      <div class="my-account-side-links col-sm-3">
          <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="identity-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['identity'], ENT_QUOTES, 'UTF-8');?>
">
        <span class="link-item">
          <i class="fa fa-user fa-fw" aria-hidden="true"></i>
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Information','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

        </span>
          </a>

          <?php if (count($_smarty_tpl->tpl_vars['customer']->value['addresses'])) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="addresses-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['addresses'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Addresses','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php } else { ?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="address-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['address'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add first address','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['history'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-history fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Order history and details','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="order-slips-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order_slip'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-file-o fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Credit slips','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['configuration']->value['voucher_enabled'] && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="discounts-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['discount'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-tags fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Vouchers','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['configuration']->value['return_enabled'] && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="returns-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order_follow'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-undo fa-fw"" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Merchandise returns','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_102530395fbf2ccb7a4310_95238088', 'display_customer_account', $this->tplIndex);
?>


          <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'index','params'=>array('mylogout'=>'')),$_smarty_tpl ) );?>
">
          <span class="link-item">
            <i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign out','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

          </span>
          </a>
      </div>
      <?php }?>
    <?php
}
}
/* {/block 'my_account_side_links'} */
/* {block 'page_title'} */
class Block_20335387495fbf2ccb7baee0_65283314 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <h2><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h2>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'customer_notifications'} */
class Block_18305836225fbf2ccb7c2033_85113322 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'customer_notifications'} */
/* {block 'page_content_top'} */
class Block_4394240755fbf2ccb7c04a9_48081387 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18305836225fbf2ccb7c2033_85113322', 'customer_notifications', $this->tplIndex);
?>

      <?php
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_11101725065fbf2ccb7cf1f6_40152692 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

       <!-- Page content -->
      <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_17883954495fbf2ccb759cc3_22533979 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_17883954495fbf2ccb759cc3_22533979',
  ),
  'my_account_side_links' => 
  array (
    0 => 'Block_5380579205fbf2ccb75ba09_93244282',
  ),
  'display_customer_account' => 
  array (
    0 => 'Block_102530395fbf2ccb7a4310_95238088',
  ),
  'page_title' => 
  array (
    0 => 'Block_20335387495fbf2ccb7baee0_65283314',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_4394240755fbf2ccb7c04a9_48081387',
  ),
  'customer_notifications' => 
  array (
    0 => 'Block_18305836225fbf2ccb7c2033_85113322',
  ),
  'page_content' => 
  array (
    0 => 'Block_11101725065fbf2ccb7cf1f6_40152692',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <section id="content" class="page-content my-account-page-content-wrapper">
    <div class="row">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5380579205fbf2ccb75ba09_93244282', 'my_account_side_links', $this->tplIndex);
?>



    <div class="my-account-page-content <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?>col-sm-9<?php } else { ?>col<?php }?>">

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20335387495fbf2ccb7baee0_65283314', 'page_title', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4394240755fbf2ccb7c04a9_48081387', 'page_content_top', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11101725065fbf2ccb7cf1f6_40152692', 'page_content', $this->tplIndex);
?>

    </div>

    </div>
  </section>

<?php
}
}
/* {/block 'page_content_container'} */
/* {block 'my_account_links'} */
class Block_20691257855fbf2ccb7d57e5_82866232 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_subTemplateRender('file:customer/_partials/my-account-links.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <?php
}
}
/* {/block 'my_account_links'} */
/* {block 'page_footer'} */
class Block_5148248165fbf2ccb7d39c2_89056183 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_footer' => 
  array (
    0 => 'Block_5148248165fbf2ccb7d39c2_89056183',
  ),
  'my_account_links' => 
  array (
    0 => 'Block_20691257855fbf2ccb7d57e5_82866232',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20691257855fbf2ccb7d57e5_82866232', 'my_account_links', $this->tplIndex);
?>

<?php
}
}
/* {/block 'page_footer'} */
}
