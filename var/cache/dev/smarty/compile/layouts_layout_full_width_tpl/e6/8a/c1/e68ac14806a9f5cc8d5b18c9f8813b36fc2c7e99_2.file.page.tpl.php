<?php
/* Smarty version 3.1.33, created on 2020-11-02 16:49:12
  from '/home/desarrollo1webti/public_html/aghaso/themes/warehouse/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fa07ed82ce603_74376735',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e68ac14806a9f5cc8d5b18c9f8813b36fc2c7e99' => 
    array (
      0 => '/home/desarrollo1webti/public_html/aghaso/themes/warehouse/templates/page.tpl',
      1 => 1598938146,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fa07ed82ce603_74376735 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7371902465fa07ed82c87c3_76896658', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_13007448255fa07ed82c9615_22014092 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
            <h1 class="h1 page-title"><span><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</span></h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_21009042445fa07ed82c8d59_91467192 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13007448255fa07ed82c9615_22014092', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_10080347565fa07ed82cbbd0_04796623 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_6057352435fa07ed82cc5f1_44119891 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_12368294365fa07ed82cb691_02309835 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10080347565fa07ed82cbbd0_04796623', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6057352435fa07ed82cc5f1_44119891', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_7238063065fa07ed82cd791_49203674 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_506823605fa07ed82cd262_55039317 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7238063065fa07ed82cd791_49203674', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_7371902465fa07ed82c87c3_76896658 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_7371902465fa07ed82c87c3_76896658',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_21009042445fa07ed82c8d59_91467192',
  ),
  'page_title' => 
  array (
    0 => 'Block_13007448255fa07ed82c9615_22014092',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_12368294365fa07ed82cb691_02309835',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_10080347565fa07ed82cbbd0_04796623',
  ),
  'page_content' => 
  array (
    0 => 'Block_6057352435fa07ed82cc5f1_44119891',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_506823605fa07ed82cd262_55039317',
  ),
  'page_footer' => 
  array (
    0 => 'Block_7238063065fa07ed82cd791_49203674',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21009042445fa07ed82c8d59_91467192', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12368294365fa07ed82cb691_02309835', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_506823605fa07ed82cd262_55039317', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
