<?php
/* Smarty version 3.1.33, created on 2020-11-25 22:17:19
  from '/home/renatonunez/public_html/tienda/themes/warehouse/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf1e3f804998_30055625',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c5ea242136db99bf4e191bee48f92f49d2300ad0' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/themes/warehouse/templates/page.tpl',
      1 => 1598938146,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf1e3f804998_30055625 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10418288445fbf1e3f7ea7c9_45015756', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_4184745245fbf1e3f7eebe8_44363198 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
            <h1 class="h1 page-title"><span><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</span></h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_2565434215fbf1e3f7ec554_12614580 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4184745245fbf1e3f7eebe8_44363198', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_20597081985fbf1e3f7f7047_35769901 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_11851392015fbf1e3f7f9ba9_41801999 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_12474398125fbf1e3f7f4f73_28540750 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20597081985fbf1e3f7f7047_35769901', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11851392015fbf1e3f7f9ba9_41801999', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_15670985845fbf1e3f800157_91022524 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_16306883275fbf1e3f7fe3e1_19936168 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15670985845fbf1e3f800157_91022524', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_10418288445fbf1e3f7ea7c9_45015756 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10418288445fbf1e3f7ea7c9_45015756',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_2565434215fbf1e3f7ec554_12614580',
  ),
  'page_title' => 
  array (
    0 => 'Block_4184745245fbf1e3f7eebe8_44363198',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_12474398125fbf1e3f7f4f73_28540750',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_20597081985fbf1e3f7f7047_35769901',
  ),
  'page_content' => 
  array (
    0 => 'Block_11851392015fbf1e3f7f9ba9_41801999',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_16306883275fbf1e3f7fe3e1_19936168',
  ),
  'page_footer' => 
  array (
    0 => 'Block_15670985845fbf1e3f800157_91022524',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2565434215fbf1e3f7ec554_12614580', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12474398125fbf1e3f7f4f73_28540750', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16306883275fbf1e3f7fe3e1_19936168', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
