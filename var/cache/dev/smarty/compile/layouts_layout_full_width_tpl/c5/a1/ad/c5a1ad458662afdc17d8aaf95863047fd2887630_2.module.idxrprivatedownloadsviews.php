<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:19:23
  from 'module:idxrprivatedownloadsviews' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf2ccb7438b8_51991271',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c5a1ad458662afdc17d8aaf95863047fd2887630' => 
    array (
      0 => 'module:idxrprivatedownloadsviews',
      1 => 1603262492,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf2ccb7438b8_51991271 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /home/renatonunez/public_html/tienda/modules/idxrprivatedownloads/views/templates/front/downloads17.tpl -->
    
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9310091975fbf2ccb6ea949_18153198', 'page_content_container');
?>
<!-- end /home/renatonunez/public_html/tienda/modules/idxrprivatedownloads/views/templates/front/downloads17.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'customer/page.tpl');
}
/* {block 'page_content_container'} */
class Block_9310091975fbf2ccb6ea949_18153198 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_9310091975fbf2ccb6ea949_18153198',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
<h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your files','mod'=>'idxrprivatedownloads'),$_smarty_tpl ) );?>
</h2>
        <div class='tabs'> 
        <ul class="nav nav-tabs" >
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sections']->value, 'section', false, NULL, 'tab', array (
  'index' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['section']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tab']->value['index']++;
?>
                <li class="nav-item" ><a class="nav-link <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tab']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tab']->value['index'] : null) === 0) {?>active<?php }?>" href="#privatedown<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['section']->value['id_section'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"  data-toggle="tab"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['section']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a></li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
        <div id="tab-content" class="tab-content">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sections']->value, 'section', false, NULL, 'sc', array (
  'index' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['section']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_sc']->value['index']++;
?>
                <div  class="tab-pane fade <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_sc']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_sc']->value['index'] : null) === 0) {?>active in<?php }?>" id="privatedown<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['section']->value['id_section'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="list-group">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['section']->value['files'], 'file');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
?>
                            <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
?section=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value['section_id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&file=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value['id_file'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="list-group-item" target="_blank" >
                                <p><img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value['icon'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/> <b><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
. </b><i><?php if ($_smarty_tpl->tpl_vars['file']->value['description']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value['description'], ENT_QUOTES, 'UTF-8');
}?></i> <i class="icon icon-download icon-3x pull-right transparent"></i></p>
                            </a>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </div>
    <?php
}
}
/* {/block 'page_content_container'} */
}
