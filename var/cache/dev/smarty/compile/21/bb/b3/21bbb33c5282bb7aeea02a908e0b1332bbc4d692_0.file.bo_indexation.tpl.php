<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:36:56
  from '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/bo_indexation.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf30e8d5bdf7_52799546',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21bbb33c5282bb7aeea02a908e0b1332bbc4d692' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/bo_indexation.tpl',
      1 => 1601444637,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf30e8d5bdf7_52799546 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel" id="fieldset_<?php echo $_smarty_tpl->tpl_vars['identi']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['identi']->value;?>
">
	<h3><i class="icon-cogs"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Indexes and caches','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
</h3>
	<div id="indexing-warning" class="alert alert-warning" style="display: none">
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Indexing is in progress. Please do not leave this page','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>

	</div>
	<div class="row">
		<p>
			<a class="ajaxcall-recurcive btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['price_indexer_url']->value;?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Index all missing prices','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
</a>
			<a class="ajaxcall-recurcive btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['full_price_indexer_url']->value;?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rebuild entire price index','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
</a>
			<a class="ajaxcall btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['attribute_indexer_url']->value;?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Build attributes and features indexes','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
</a>
			<a class="ajaxcall btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['clear_cache_url']->value;?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Clear cache','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
</a>
		</p>
	</div>
</div>


<?php echo '<script'; ?>
 type="text/javascript">
	<?php if (isset($_smarty_tpl->tpl_vars['PS_LAYERED_INDEXED']->value)) {?>var PS_LAYERED_INDEXED = <?php echo $_smarty_tpl->tpl_vars['PS_LAYERED_INDEXED']->value;?>
;<?php }?>
	var token = '<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
';
	var id_lang = <?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
;
	var base_folder = '<?php echo $_smarty_tpl->tpl_vars['base_folder']->value;?>
';
	var translations = new Object();

	translations.in_progress = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'(in progress)','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.url_indexation_finished = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'URL indexing finished','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.attribute_indexation_finished = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Attribute indexing finished','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.url_indexation_failed = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'URL indexing failed','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.attribute_indexation_failed = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Attribute indexing failed','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.price_indexation_finished = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Price indexing finished','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.price_indexation_failed = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Price indexing failed','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.price_indexation_in_progress = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'(in progress, current product id %s)','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.loading = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Loading...','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.delete_all_filters_templates = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You selected -All categories-: all existing filter templates will be deleted. Is it OK?','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
	translations.no_selected_categories = '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You must select at least one category','js'=>1,'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
';
<?php echo '</script'; ?>
><?php }
}
