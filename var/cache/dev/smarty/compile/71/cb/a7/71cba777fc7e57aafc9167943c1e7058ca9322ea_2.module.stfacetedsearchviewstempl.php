<?php
/* Smarty version 3.1.33, created on 2020-11-25 22:14:18
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf1d8ae18ba8_92472108',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '71cba777fc7e57aafc9167943c1e7058ca9322ea' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf1d8ae18ba8_92472108 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!-- begin /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/front/catalog/active-filters.tpl --><?php if (count($_smarty_tpl->tpl_vars['activeFilters']->value)) {?>
<section id="js-active-search-filters" class="active_filters_block feds_active_filters feds_active_filters_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_active_filters']->value, ENT_QUOTES, 'UTF-8');?>
">
    <ul>
      <?php if ($_smarty_tpl->tpl_vars['show_active_filters']->value) {?>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['activeFilters']->value, 'filter');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['filter']->value) {
?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12242914835fbf1d8ad97717_66314588', 'active_filters_item');
?>

      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      <?php }?>
      <li class="filter-block">
        <a class="feds_link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clear_all_link']->value, ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['show_active_filters']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Clear all','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'(x)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );
} else { ?><i class="feds-cw"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reset','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );
}?></a>
      </li>
    </ul>
</section>
<?php }?>
<!-- end /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/front/catalog/active-filters.tpl --><?php }
/* {block 'active_filters_item'} */
class Block_12242914835fbf1d8ad97717_66314588 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'active_filters_item' => 
  array (
    0 => 'Block_12242914835fbf1d8ad97717_66314588',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <li class="filter-block">
            <a class="feds_link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['nextEncodedFacetsURL'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['label'], ENT_QUOTES, 'UTF-8');
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'(x)','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</a>
          </li>
        <?php
}
}
/* {/block 'active_filters_item'} */
}
