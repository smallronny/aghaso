<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:38:17
  from '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/bo_filters.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf3139c2eba9_81890857',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '00ba6b78413a9f0a1a73f417cf3e59b1dee7c667' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/admin/bo_filters.tpl',
      1 => 1601444637,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./bo_filter.tpl' => 3,
  ),
),false)) {
function content_5fbf3139c2eba9_81890857 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="filter_panel">
  <header class="clearfix">
    <span class="badge pull-right">
      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total filter blocks: %s','sprintf'=>array($_smarty_tpl->tpl_vars['total_filters']->value),'d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>

    </span>
  </header>
  <section class="filter_list">
    <div class="row">
      <div class="col-xs-4">
        <h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Available filter blocks','d'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</h3>
        <ul class="list-unstyled sortable disabled_filters">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['quanbu']->value, 'filter', false, 'filterId');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['filterId']->value => $_smarty_tpl->tpl_vars['filter']->value) {
?>
          <?php if (!isset($_smarty_tpl->tpl_vars['filter']->value['filter_type'])) {?>
            <?php $_smarty_tpl->_subTemplateRender('file:./bo_filter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('filter'=>$_smarty_tpl->tpl_vars['filter']->value), 0, true);
?>
          <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 
        </ul>   
      </div>
      <div class="col-xs-8">
        <h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enabled filter blocks','d'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</h3>
        <ul class="list-unstyled sortable enabled_filters">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['quanbu']->value, 'filter', false, 'filterId');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['filterId']->value => $_smarty_tpl->tpl_vars['filter']->value) {
?>
          <?php if (isset($_smarty_tpl->tpl_vars['filter']->value['filter_type']) && $_smarty_tpl->tpl_vars['filter']->value['zhuangtai'] == 1) {?>
            <?php $_smarty_tpl->_subTemplateRender('file:./bo_filter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('filter'=>$_smarty_tpl->tpl_vars['filter']->value), 0, true);
?>
          <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>     
        <li class="filtered filtered_show_more filter_list_item"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Items below will be hidden by default','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</li>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['quanbu']->value, 'filter', false, 'filterId');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['filterId']->value => $_smarty_tpl->tpl_vars['filter']->value) {
?>
          <?php if (isset($_smarty_tpl->tpl_vars['filter']->value['filter_type']) && $_smarty_tpl->tpl_vars['filter']->value['zhuangtai'] == 2) {?>
            <?php $_smarty_tpl->_subTemplateRender('file:./bo_filter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('filter'=>$_smarty_tpl->tpl_vars['filter']->value), 0, true);
?>
          <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>    
        </ul>   
      </div>
    </div>
  </section>
</section>

<?php echo '<script'; ?>
 type="text/javascript">
  var translations = new Array();
  translations['no_selected_categories'] = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You must select at least one category','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
";
  translations['no_selected_filters'] = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You must select at least one filter','d'=>'Modules.Facetedsearch.Admin'),$_smarty_tpl ) );?>
";
<?php echo '</script'; ?>
><?php }
}
