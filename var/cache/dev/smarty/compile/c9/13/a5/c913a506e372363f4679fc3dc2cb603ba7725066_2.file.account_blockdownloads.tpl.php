<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:16:44
  from '/home/renatonunez/public_html/tienda/modules/idxrprivatedownloads/views/templates/front/account_blockdownloads.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf2c2cc3a974_61928552',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c913a506e372363f4679fc3dc2cb603ba7725066' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/idxrprivatedownloads/views/templates/front/account_blockdownloads.tpl',
      1 => 1603262492,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf2c2cc3a974_61928552 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['es17']->value) {?>
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="idxfacturae" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('idxrprivatedownloads','downloads'),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Go to my files','mod'=>'idxrprivatedownloads'),$_smarty_tpl ) );?>
">
  <span class="link-item">
    <i class="material-icons">file_download</i>
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Go to my files','mod'=>'idxrprivatedownloads'),$_smarty_tpl ) );?>

  </span>
</a>    
<?php } else { ?>
<li>
    <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Go to my files','mod'=>'idxrprivatedownloads'),$_smarty_tpl ) );?>
" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('idxrprivatedownloads','downloads'),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
        <i class="icon-download"></i>
        <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Go to my files','mod'=>'idxrprivatedownloads'),$_smarty_tpl ) );?>
</span>
    </a>
</li>
<?php }
}
}
