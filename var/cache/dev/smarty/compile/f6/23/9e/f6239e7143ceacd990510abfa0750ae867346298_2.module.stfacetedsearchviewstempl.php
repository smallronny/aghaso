<?php
/* Smarty version 3.1.33, created on 2020-11-25 22:14:25
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf1d913a9806_70122928',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f6239e7143ceacd990510abfa0750ae867346298' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf1d913a9806_70122928 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/hook/stfacetedsearch.tpl --><?php if (isset($_smarty_tpl->tpl_vars['listing']->value['rendered_facets'])) {?>
	<?php $_smarty_tpl->_assignInScope('show_on', Configuration::get('ST_FAC_SEARCH_SHOW_ON'));?>
	<div id="feds_search_filters" class="feds_show_on_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_on']->value, ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['show_on']->value != 2) {?> feds_show_on_x <?php }?>">
  	<?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_facets'];?>

	</div>
<?php }?>
<!-- end /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/hook/stfacetedsearch.tpl --><?php }
}
