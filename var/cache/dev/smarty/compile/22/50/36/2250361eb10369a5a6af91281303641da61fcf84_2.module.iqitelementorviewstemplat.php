<?php
/* Smarty version 3.1.33, created on 2020-11-25 22:17:20
  from 'module:iqitelementorviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf1e403adb39_76512129',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2250361eb10369a5a6af91281303641da61fcf84' => 
    array (
      0 => 'module:iqitelementorviewstemplat',
      1 => 1598938145,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf1e403adb39_76512129 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/renatonunez/public_html/tienda/modules/iqitelementor/views/templates/hook/generated_content_cms.tpl -->

<?php if ($_smarty_tpl->tpl_vars['options']->value['elementor']) {?>
    <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

<?php } else { ?>
    <div class="rte-content"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</div>
<?php }?>

<!-- end /home/renatonunez/public_html/tienda/modules/iqitelementor/views/templates/hook/generated_content_cms.tpl --><?php }
}
