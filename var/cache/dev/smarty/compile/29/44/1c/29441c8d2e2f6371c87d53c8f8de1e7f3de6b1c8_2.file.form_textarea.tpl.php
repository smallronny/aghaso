<?php
/* Smarty version 3.1.33, created on 2020-11-25 22:17:20
  from '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/form_textarea.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf1e405de345_45047544',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '29441c8d2e2f6371c87d53c8f8de1e7f3de6b1c8' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/form_textarea.tpl',
      1 => 1600098673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf1e405de345_45047544 (Smarty_Internal_Template $_smarty_tpl) {
?><span class="ets_cfu_form-control-wrap <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['html_class']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
    <textarea <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['atts']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
if ($_smarty_tpl->tpl_vars['item']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['key']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" <?php }
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> ><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['value']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</textarea> 
    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['validation_error']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

    <?php if (isset($_smarty_tpl->tpl_vars['atts']->value['maxlength']) && $_smarty_tpl->tpl_vars['atts']->value['maxlength']) {?>
        <span class="number_max"><span class="index_type">0</span>/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['atts']->value['maxlength'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
    <?php }?>
</span>
<?php }
}
