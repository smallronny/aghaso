<?php
/* Smarty version 3.1.33, created on 2020-11-25 23:19:09
  from '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/form_select.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf2cbd579cf7_95499937',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8c634497c68d82c22f12e5ef0f5112a932327e57' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/ets_cfultimate/views/templates/hook/form_select.tpl',
      1 => 1600098673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf2cbd579cf7_95499937 (Smarty_Internal_Template $_smarty_tpl) {
?><span class="ets_cfu_form-control-wrap <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['html_class']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
    <select <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['atts']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?> <?php if ($_smarty_tpl->tpl_vars['item']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['key']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"<?php }?> <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> >
        <?php echo $_smarty_tpl->tpl_vars['html']->value;?>

    </select>
    <?php if (isset($_smarty_tpl->tpl_vars['atts']->value['multiple']) && $_smarty_tpl->tpl_vars['atts']->value['multiple']) {?>
    <?php } else { ?>
        <span class="select_arrow"></span>
    <?php }?>
    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['validation_error']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

</span><?php }
}
