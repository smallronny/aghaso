<?php
/* Smarty version 3.1.33, created on 2020-11-25 22:14:23
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf1d8f026300_64610571',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b4b938717295ba075e1ecd4a66b8c91d8334d4e2' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbf1d8f026300_64610571 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/listing/center_column.tpl --><?php if (isset($_smarty_tpl->tpl_vars['listing']->value['rendered_facets']) && $_smarty_tpl->tpl_vars['listing']->value['rendered_facets']) {
$_smarty_tpl->_assignInScope('show_on', Configuration::get('ST_FAC_SEARCH_SHOW_ON'));
$_smarty_tpl->_assignInScope('show_on_mobile', Configuration::get('ST_FAC_SEARCH_SHOW_ON_MOBILE'));?>
<a href="javascript:;" class="feds_offcanvas_tri feds_offcanvas_tri_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_on']->value, ENT_QUOTES, 'UTF-8');?>
 feds_offcanvas_tri_mobile_<?php if ($_smarty_tpl->tpl_vars['show_on_mobile']->value) {?>1<?php }?> <?php if ($_smarty_tpl->tpl_vars['show_on_mobile']->value == 2) {?> feds_offcanvas_tri_tablet <?php }?> "><i class="feds-sliders"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Filters','mod'=>'stfacetedsearch'),$_smarty_tpl ) );?>
</a>
<?php if ($_smarty_tpl->tpl_vars['show_on']->value) {?>
<div class="feds_horizontal_wrap">
<?php if ($_smarty_tpl->tpl_vars['show_on']->value == 1 || $_smarty_tpl->tpl_vars['show_on']->value == 2) {?>
<div class="feds_horizontal feds_horizontal_<?php if ($_smarty_tpl->tpl_vars['show_on']->value == 2) {?>dropdown<?php } else { ?>list<?php }?>">
	<div id="feds_search_filters" class="feds_show_on_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_on']->value, ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['show_on']->value != 2) {?> feds_show_on_x <?php }?>">
	<?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_facets'];?>

	</div>
</div>
<?php }?>
</div>
<?php }
}?><!-- end /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/listing/center_column.tpl --><?php }
}
