<?php
/* Smarty version 3.1.33, created on 2020-11-25 22:14:18
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbf1d8aeea073_53758438',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0e2ed7d1e2abc6b0a82714afad00d8c3881c002' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1601444637,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:stfacetedsearch/views/templates/listing/center_column.tpl' => 1,
  ),
),false)) {
function content_5fbf1d8aeea073_53758438 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/listing/category.tpl -->
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6550048925fbf1d8aebef74_94181259', 'product_list_top');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14732402085fbf1d8aee81d7_83815605', 'product_list_active_filters');
?>
<!-- end /home/renatonunez/public_html/tienda/modules/stfacetedsearch/views/templates/listing/category.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'catalog/listing/category.tpl');
}
/* {block 'product_list_top'} */
class Block_6550048925fbf1d8aebef74_94181259 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_top' => 
  array (
    0 => 'Block_6550048925fbf1d8aebef74_94181259',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_subTemplateRender('module:stfacetedsearch/views/templates/listing/center_column.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_14732402085fbf1d8aee81d7_83815605 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_active_filters' => 
  array (
    0 => 'Block_14732402085fbf1d8aee81d7_83815605',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_list_active_filters'} */
}
