<?php
/* Smarty version 3.1.33, created on 2020-11-26 09:28:04
  from '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/hook/search_marker_result.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fbfbb74055446_95375873',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '844aac1e18b1113cd00a579c31c43ce024677a2e' => 
    array (
      0 => '/home/renatonunez/public_html/tienda/modules/kbstorelocatorpickup/views/templates/hook/search_marker_result.tpl',
      1 => 1604346316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbfbb74055446_95375873 (Smarty_Internal_Template $_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['available_store']->value)) {?>
    <div class="velo-popup">
        
        
                            
                            <table style="
    width: 100%;
">
                                <tr>
                                    <td style="padding-left:0px;">
                                        <div class="velo_add_name"> <span class="velo_store_icon" style="display:none;"><img class="velo-phone-icon" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['store_icon']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/> </span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['available_store']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
                            <div class="velo-add-address"> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['available_store']->value['address1'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php if (!empty($_smarty_tpl->tpl_vars['available_store']->value['address2'])) {?> 
                                <br/><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['available_store']->value['address2'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            <?php if (!empty($_smarty_tpl->tpl_vars['available_store']->value['state'])) {?> 
                                <br/><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['available_store']->value['state'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            </div>
                                    </td>
                                    <?php if ($_smarty_tpl->tpl_vars['is_enabled_store_image']->value) {?> 
                                    <td>
                                        <div id="kb-store-image">
                                        <?php if (!empty($_smarty_tpl->tpl_vars['available_store']->value['image'])) {
echo $_smarty_tpl->tpl_vars['available_store']->value['image'];
}?>
                        </div>
                                        <div></div>
                                    </td>
                                    <?php }?>
                                </tr>
                            </table>

                            <div style="display: none" class="velo-w3-address-country"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['available_store']->value['country'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
                            <div class="velo-show-more"><strong><a href="javascript:void(0);" id="button-show-more-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['available_store']->value['id_store'], ENT_QUOTES, 'UTF-8');?>
" class="button-show-more" ><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Show More','mod'=>'kbstorelocatorpickup'),$_smarty_tpl ) );?>
</a></strong></div>
                            <div class="extra_content" style="display:none;">
                        <?php if (!empty($_smarty_tpl->tpl_vars['available_store']->value['email'])) {?>
                                <span class="velo_add_number"><i class="icon-envelope"></i> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['available_store']->value['email'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['display_phone']->value && !empty($_smarty_tpl->tpl_vars['available_store']->value['phone'])) {?>
                            <span class="velo_add_number"><i class="icon-phone"></i><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['available_store']->value['phone'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
                            <?php }?>
                                                        <?php if ($_smarty_tpl->tpl_vars['is_enabled_website_link']->value) {?>
                                <div class="kb-store-url">
                                    <span class="velo_add_number"><img class="velo-web-icon" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['web_icon']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/> <a href='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['index_page_link']->value, ENT_QUOTES, 'UTF-8');?>
'>
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['index_page_link']->value, ENT_QUOTES, 'UTF-8');?>

                                    </a></span>

                                </div>
                            <?php }?>
                            </div>
                                                <div class="velo_add_distance"></div>

                        
                    
 </div>
<?php }?>

<?php }
}
