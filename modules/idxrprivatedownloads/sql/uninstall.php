<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innovadeluxe SL
* @copyright 2017 Innovadeluxe SL

* @license   INNOVADELUXE
*/

$sql = array();
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_files';
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_files_lang';
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_sections';
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_sections_lang';
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_customers';
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_customer_groups';
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_section_access';
$sql[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'idxrprivatedownloads_file_access';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
