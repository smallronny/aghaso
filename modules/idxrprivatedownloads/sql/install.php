<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innovadeluxe SL
* @copyright 2017 Innovadeluxe SL

* @license   INNOVADELUXE
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_files (
    id_file int(11) NOT NULL AUTO_INCREMENT,
    location varchar(255) NOT NULL,
    section_id int(11) NOT NULL,
    position int(5),
    icon varchar(255) NOT NULL,
    id_shop int(11) NOT NULL,
    PRIMARY KEY  (id_file)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_files_lang (
    id_file_lang int(11) NOT NULL AUTO_INCREMENT,
    id_file int(11) NOT NULL,
    id_lang varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    description varchar(255) NULL,
    PRIMARY KEY  (id_file_lang)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_sections (
    id_section int(11) NOT NULL AUTO_INCREMENT,
    position int(5),
    id_shop int(11) NOT NULL,
    PRIMARY KEY  (id_section)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_sections_lang (
    id_section_lang int(11) NOT NULL AUTO_INCREMENT,
    id_section int(11) NOT NULL,
    id_lang int(11) NOT NULL,
    name varchar(255) NOT NULL,
    PRIMARY KEY  (id_section_lang)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_customers (
    id_customer int(11) NOT NULL,
    active tinyint(1) NOT NULL,
    id_shop int(11) NOT NULL,
    PRIMARY KEY  (id_customer)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_customer_groups (
    id_group int(11) NOT NULL,
    active tinyint(1) NOT NULL,
    id_shop int(11) NOT NULL,
    PRIMARY KEY  (id_group)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_section_access (
    id_access int(11) NOT NULL AUTO_INCREMENT,
    is_group tinyint(1) NOT NULL,
    id_cg int(11) NOT NULL,
    id_section int(11) NOT NULL,
    id_shop int(11) NOT NULL,
    UNIQUE (is_group, id_cg, id_section, id_shop),
    PRIMARY KEY  (id_access)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'idxrprivatedownloads_file_access (
    id_access int(11) NOT NULL AUTO_INCREMENT,
    is_group tinyint(1) NOT NULL,
    id_cg int(11) NOT NULL,
    id_file int(11) NOT NULL,.
    id_shop int(11) NOT NULL,
    UNIQUE (is_group, id_cg, id_file, id_shop),
    PRIMARY KEY  (id_access)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
