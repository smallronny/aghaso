<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innovadeluxe SL
 * @copyright 2017 Innovadeluxe SL

 * @license   INNOVADELUXE
 */

include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('idxrprivatedownloads.php');

$module = new IdxrPrivateDownloads();

if (!Tools::isSubmit('secure_key') || Tools::getValue('secure_key') != $module->secure_key || !Tools::getValue('action')) {
    echo 'Wrong secure key '.Tools::getValue('secure_key').' '.$module->secure_key;
    die(1);
}
if (Tools::getValue('action') == 'delete' && Tools::getValue('section')) {
    $section_id = Tools::getValue('section');
    $module->delSection($section_id);
}

if (Tools::getValue('action') == 'deletefile' && Tools::getValue('file')) {
    $file_id = Tools::getValue('file');
    $module->delFile($file_id);
}

if (Tools::getValue('action') == 'order' && Tools::getValue('section')) {
    $section_order = json_decode(Tools::getValue('section'));
    for ($i = 0; $i < count($section_order); $i++) {
        $data = array(
            'position' => $i
        );
        Db::getInstance()->update('idxrprivatedownloads_sections', $data, 'id_section = ' . (int) $section_order[$i]);
    }
}

if (Tools::getValue('action') == 'orderfiles' && Tools::getValue('file')) {
    $file_order = json_decode(Tools::getValue('file'));
    for ($i = 0; $i < count($file_order); $i++) {
        $data = array(
            'position' => $i
        );
        Db::getInstance()->update('idxrprivatedownloads_files', $data, 'id_file = ' . (int) $file_order[$i]);
    }
}

if (Tools::getValue('action') == 'exampleFile') {
    $module->generateTemplate();
}
