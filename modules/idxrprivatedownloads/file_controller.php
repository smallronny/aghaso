<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innovadeluxe SL
 * @copyright 2017 Innovadeluxe SL

 * @license   INNOVADELUXE
 */

include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('idxrprivatedownloads.php');

$module = new IdxrPrivateDownloads();

$context = Context::getContext();
if (!$context->customer->isLogged()) {
    exit('File not allowed please log in');
}

$section_id = Tools::getValue('section');
$file = (int)Tools::getValue('file');

if (!$module->existFile($file)) {
     exit('File not exist');
}

if (!$module->validFileCustomer($file, $context->customer->id)) {
    exit('File not allowed');
}

$file_info = $module->getFile($file, $context->language->id)[0];

$url = _PS_ROOT_DIR_ . '/upload/' . $file_info['location'] .'_'.$context->language->id;

$mime = mime_content_type($url);
$ext = $file_info['icon'];

$name = preg_replace("/[^A-Za-z0-9 ]/", '', $file_info['name']);
$name = ucwords(Tools::strtolower($name));
$name  = preg_replace('/([\s])\1+/', '_', $name);
$name  =str_replace(" ", "_", $name);
$name = urldecode($name). '.' .$ext;

// Fetch and serve
if ($url) {
    $size= filesize($url);
    
    // Generate the server headers
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) {
        header('Content-Type: "' . $mime . '"');
        header('Content-Disposition: attachment; filename="' . $name . '"');
        header('Expires: 0');
        header('Content-Length: '.$size);
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header("Content-Transfer-Encoding: binary");
        header('Pragma: public');
    } else {
        header('Content-Type: "' . $mime . '"');
        header('Content-Disposition: attachment; filename="' . $name . '"');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        header('Content-Length: '.$size);
        header('Pragma: no-cache');
    }
    
    readfile($url);
    exit;
}

// Not found
exit('File not found');
