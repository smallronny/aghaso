<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innovadeluxe SL
 * @copyright 2017 Innovadeluxe SL

 * @license   INNOVADELUXE
 */

if (!defined('_PS_VERSION_')) {
    return false;
}

if (!class_exists('InnovaTools_2_0_0')) {
    require_once(_PS_ROOT_DIR_ . '/modules/idxrprivatedownloads/libraries/innovatools_2_0_0.php');
}

include_once _PS_ROOT_DIR_ . '/modules/idxrprivatedownloads/libraries/simplexlsx.class.php';
include_once _PS_ROOT_DIR_ . '/modules/idxrprivatedownloads/libraries/xlsxwriter.class.php';

class IdxrPrivateDownloads extends Module
{

    public function __construct()
    {
        $this->name = 'idxrprivatedownloads';
        $this->tab = 'content_management';
        $this->version = '1.2.6';
        $this->author_address = '0x899FC2b81CbbB0326d695248838e80102D2B4c53';
        $this->innovatabs = "";
        $this->doclink = $this->name."/doc/readme_en.pdf";
        $this->author = 'innovadeluxe';
        $this->bootstrap = true;
        $this->secure_key = Tools::encrypt($this->name);
        $this->es15 = version_compare(_PS_VERSION_, '1.6.0.0', '<');
        $this->es17 = version_compare(_PS_VERSION_, '1.7.0.0', '>=');
        $this->module_key = '4fe80989aadd86f0f2b3c39c3bcabb6b';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        parent::__construct();

        $this->displayName = $this->l('Private Downloads');
        $this->description = $this->l('Create a new customer section with downloads');
    }

    public function install()
    {
        include(dirname(__FILE__) . '/sql/install.php');
        return parent::install() && $this->registerHook('displayCustomerAccount');
    }

    public function uninstall()
    {
        include(dirname(__FILE__) . '/sql/uninstall.php');
        return parent::uninstall();
    }

    public function getContent()
    {
        $output = $this->innovaTitle();
        $output .= $this->postProcess() . $this->renderForm();
        return $output;
    }

    public function postProcess()
    {
        if (Tools::getValue('addSection')) {
            return $this->renderFormAdd();
        }
        if (Tools::getValue('submitConfigurationFile')) {
            $id_section = Tools::getValue('id_section');
            if (isset($_FILES['file'])) {
                if ($_FILES['file']['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    return $this->displayError($this->l('The file must be an excel'));
                }
                
                $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $target_file = _PS_ROOT_DIR_ . '/modules/idxrprivatedownloads/import.' .$extension;

                move_uploaded_file($_FILES['file']['tmp_name'], $target_file);
                $xlsx = new SimpleXLSX($target_file);
                $files = array();
                foreach ($xlsx->sheets() as $key => $sheet) {
                    foreach ($xlsx->rows($key) as $k => $r) {
                        if ($k == 0) {
                            continue; // skip first row
                        }
                        $counter = 0;
                        $file = array();
                        foreach ($r as $value) {
                            $item = (int) floor($counter/4);
                            $step = $counter%4;
                            switch ($step) {
                                case 0:
                                    $file[$item]['language'] = $value;
                                    break;
                                case 1:
                                    $file[$item]['url'] = $value;
                                    break;
                                case 2:
                                    $file[$item]['title'] = $value;
                                    break;
                                case 3:
                                    $file[$item]['description'] = $value;
                                    break;
                                default:
                                    break;
                            }
                            $counter ++;
                        }
                        $files[] = $file;
                    }
                }
                
                $this->importFiles($files, $id_section);
                
                return $this->displayConfirmation($this->l('The settings have been updated.'));
            } else {
                return $this->displayError($this->l('The file was not update properly'));
            }
        }
//        if (Tools::getValue('editFile')) {
//            $id_file = Tools::getValue('fileid');
//            if ($this->existFile($id_file)) {
//                return $this->renderFormAddFile($id_file);
//            }
//        }
//        if (Tools::getValue('editFileAccess')) {
//            $id_file = Tools::getValue('fileid');
//            if ($this->existFile($id_file)) {
//                return $this->renderFormAccessFile($id_file);
//            }
//        }
        if (Tools::getValue('addCustomer')) {
            return $this->renderFormAddCustomer();
        }
        if (Tools::isSubmit('submitSection')) {
            $id_section = Tools::getValue('id_section');
            $languages = Language::getLanguages(false);
            $title = array();
            foreach ($languages as $lang) {
                $title[(int) $lang['id_lang']] = Tools::getValue('title_' . (int) $lang['id_lang']);
            }
            $this->addSection($title, $id_section);
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }
//        if (Tools::getValue('editSection')) {
//            $id_section = Tools::getValue('sectionid');
//            return $this->renderFormAdd($id_section);
//        }
        if (Tools::isSubmit('submitFile')) {
            $languages = Language::getLanguages(false);
            $id_section = (int) Tools::getValue('id_section');
            $id_file = (int) Tools::getValue('id_file');
            $id_shop = (int) $this->context->shop->id;
            foreach ($_FILES as $file) {
                if ($file['error'] == UPLOAD_ERR_INI_SIZE) {
                    return $this->displayError($this->l('The uploaded file exceeds the prestashop Upload quota (Advanced Parameters -> Administration) or upload_max_filesize directive in php.ini'));
                }
            }
            if ($id_file) {
                foreach ($languages as $lang) {
                    $lang_data = array(
                        'name' => pSQL(Tools::getValue('title_' . (int) $lang['id_lang'])),
                        'description' => pSQL(Tools::getValue('description_' . (int) $lang['id_lang'])),
                    );
                    $where = 'id_file = ' . (int)$id_file . ' and id_lang = ' . (int) $lang['id_lang'];
                    Db::getInstance()->update('idxrprivatedownloads_files_lang', $lang_data, $where);
                }
                foreach ($languages as $lang) {
                    if (isset($_FILES['file_' . $lang['id_lang']]) &&
                                isset($_FILES['file_' . $lang['id_lang']]['tmp_name']) &&
                                !empty($_FILES['file_' . $lang['id_lang']]['tmp_name'])) {
                        $old_extension = Db::getInstance()->getValue('Select icon from ' . _DB_PREFIX_ . 'idxrprivatedownloads_files where id_file = ' . (int)$id_file);
                        $old_location = Db::getInstance()->getValue('Select location from ' . _DB_PREFIX_ . 'idxrprivatedownloads_files where id_file = ' . (int)$id_file);
                        $extension = pathinfo($_FILES['file_' . $lang['id_lang']]['name'], PATHINFO_EXTENSION);
                        
                        $data = array(
                            'icon' => $extension,
                        );
                        $where = 'id_file = ' . (int)$id_file . ' and id_shop = ' . (int)$id_shop;
                        Db::getInstance()->update('idxrprivatedownloads_files', $data, $where);
                        $old_file =  _PS_ROOT_DIR_ . '/upload/' . $old_location . '_' . (int) $lang['id_lang'];
                        $target_file = _PS_ROOT_DIR_ . '/upload/' . $old_location . '_' . (int) $lang['id_lang'];
                        if (file_exists($old_file)) {
                            unlink($old_file);
                        }
                        move_uploaded_file($_FILES['file_' . $lang['id_lang']]['tmp_name'], $target_file);
                    }
                }
            } else {
                $i = 0;
                $file_id = false;
                $filled_lang = false;
                $shaname = '';
                foreach ($languages as $lang) {
                    if (isset($_FILES['file_' . $lang['id_lang']]) &&
                            isset($_FILES['file_' . $lang['id_lang']]['tmp_name']) &&
                            !empty($_FILES['file_' . $lang['id_lang']]['tmp_name'])) {
                        if (!$filled_lang) {
                            $filled_lang = $lang;
                        }
                        $extension = pathinfo($_FILES['file_' . $lang['id_lang']]['name'], PATHINFO_EXTENSION);
                        if ($i == 0) {
                            $shaname = sha1_file($_FILES['file_' . $lang['id_lang']]['tmp_name']);
                            $data = array(
                                'location' => $shaname,
                                'section_id' => $id_section,
                                'icon' => $extension,
                                'id_shop' => $id_shop
                            );
                            Db::getInstance()->insert('idxrprivatedownloads_files', $data);
                            $file_id = Db::getInstance()->Insert_ID();
                        }
                        
                        //$target_file = _PS_ROOT_DIR_ . '/modules/idxrprivatedownloads/files/' . $file_id . '_' . (int) $lang['id_lang'] . '.' . $extension;
                        $target_file = _PS_ROOT_DIR_ . '/upload/' . $shaname . '_' . (int) $lang['id_lang'];
                        move_uploaded_file($_FILES['file_' . $lang['id_lang']]['tmp_name'], $target_file);
                        $lang_data = array(
                            'id_file' => $file_id,
                            'id_lang' => (int) $lang['id_lang'],
                            'name' => pSQL(Tools::getValue('title_' . (int) $lang['id_lang'])),
                            'description' => pSQL(Tools::getValue('description_' . (int) $lang['id_lang'])),
                        );
                        Db::getInstance()->insert('idxrprivatedownloads_files_lang', $lang_data);
                    } else {
                        if (!$filled_lang) {
                            foreach ($languages as $otherlang) {
                                if (isset($_FILES['file_' . $otherlang['id_lang']]) &&
                                    isset($_FILES['file_' . $otherlang['id_lang']]['tmp_name']) &&
                                    !empty($_FILES['file_' . $otherlang['id_lang']]['tmp_name'])) {
                                    $filled_lang = $otherlang['id_lang'];
                                    break;
                                }
                            }
                        }
                        $extension = pathinfo($_FILES['file_' . $filled_lang['id_lang']]['name'], PATHINFO_EXTENSION);
                        if ($i == 0) {
                            $shaname = sha1_file($_FILES['file_' . $filled_lang['id_lang']]['tmp_name']);
                            $data = array(
                                'location' => $shaname,
                                'section_id' => $id_section,
                                'icon' => $extension,
                                'id_shop' => $id_shop
                            );
                            Db::getInstance()->insert('idxrprivatedownloads_files', $data);
                            $file_id = Db::getInstance()->Insert_ID();
                        }
                        
//                        $source_file = _PS_ROOT_DIR_ . '/modules/idxrprivatedownloads/files/' . $file_id . '_' . (int) $filled_lang['id_lang'] . '.' . $extension;
//                        $target_file = _PS_ROOT_DIR_ . '/modules/idxrprivatedownloads/files/' . $file_id . '_' . (int) $lang['id_lang'] . '.' . $extension;
                        
                        $source_file = _PS_ROOT_DIR_ . '/upload/' . $shaname . '_' . (int) $filled_lang['id_lang'];
                        $target_file = _PS_ROOT_DIR_ . '/upload/' . $shaname . '_' . (int) $lang['id_lang'];
                        copy($source_file, $target_file);
                        $lang_data = array(
                            'id_file' => $file_id,
                            'id_lang' => (int) $lang['id_lang'],
                            'name' => pSQL(Tools::getValue('title_' . (int) $lang['id_lang'])),
                            'description' => pSQL(Tools::getValue('description_' . (int) $lang['id_lang'])),
                        );
                        Db::getInstance()->insert('idxrprivatedownloads_files_lang', $lang_data);
                    }
                    $i++;
                }
            }
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }
        if (Tools::isSubmit('submitCustomer')) {
            if (Tools::getValue('customer')) {
                $customers_email = $this->extractEmailAddress(Tools::getValue('customer'));
                foreach ($customers_email as $email) {
                    $customer = Customer::getCustomersByEmail($email);
                    if ($customer) {
                        $this->addCustomer($customer[0]['id_customer']);
                    }
                }
            }
            $id_customer = Tools::getValue('id_customer');
            if ($id_customer != -1) {
                $this->addCustomer($id_customer);
            }
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }
        if (Tools::isSubmit('submitCustomerGroup')) {
            $id_group = Tools::getValue('id_group');
            if ($id_group != -1) {
                $this->addCustomerGroup($id_group);
            }
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }
        if (Tools::isSubmit('deleteidxrprivatedownloads_customers')) {
            $id_customer = Tools::getValue('id');
            $this->deleteCustomer($id_customer);
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }
        if (Tools::isSubmit('deleteidxrprivatedownloads_customer_groups')) {
            $id_customer_group = Tools::getValue('id');
            $this->deleteCustomerGroup($id_customer_group);
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }
        if (Tools::getValue('addcas')) {//customer access to section
            $customers_email = $this->extractEmailAddress(Tools::getValue('customer_section_access'));
            $id_section = Tools::getValue('sectionid');
            foreach ($customers_email as $email) {
                $customer = Customer::getCustomersByEmail($email);
                if ($customer) {
                    $this->grantaccessSection($customer[0]['id_customer'], $id_section, false);
                }
            }
        }
        if (Tools::getValue('addgas')) {//customer group access to section
            $id_group = Tools::getValue('id_group');
            $id_section = Tools::getValue('sectionid');
            $this->grantaccessSection($id_group, $id_section, true);
        }
        if (Tools::getValue('delas')) {//remove access to section
            $is_group = Tools::getValue('ig');
            $id = Tools::getValue('id');
            $id_section = Tools::getValue('sectionid');
            $this->revokeaccessSection($id_section, $is_group, $id);
        }
        
        if (Tools::getValue('addcaf')) {//customer access to file
            $customers_email = $this->extractEmailAddress(Tools::getValue('customer_section_access'));
            $id_file = Tools::getValue('fileid');
            foreach ($customers_email as $email) {
                $customer = Customer::getCustomersByEmail($email);
                if ($customer) {
                    $this->grantaccessFile($customer[0]['id_customer'], $id_file, false);
                }
            }
        }
        if (Tools::getValue('addgaf')) {//customer group access to file
            $id_group = Tools::getValue('id_group');
            $id_file = Tools::getValue('fileid');
            $this->grantaccessFile($id_group, $id_file, true);
        }
        if (Tools::getValue('delaf')) {//remove access to file
            $is_group = Tools::getValue('ig');
            $id = Tools::getValue('id');
            $id_file = Tools::getValue('fileid');
            $this->revokeaccessFile($id_file, $is_group, $id);
        }
    }

    public function renderForm()
    {
        return InnovaTools_2_0_0::adminTabWrap($this);
    }

    public function helpGenerateForm()
    {
        $sections = $this->getSections();
        $this->context->controller->addCSS($this->_path . 'views/css/back.css', 'all');
        $this->context->controller->addJqueryUI('ui.sortable');
        Media::addJsDef(
            array(
                'ajax_link' =>  $this->context->shop->physical_uri . $this->context->shop->virtual_uri . 'modules/' . $this->name . '/ajax_back.php?secure_key=' . $this->secure_key,
                'confirm_text' => $this->l('Are you sure to delete the section and all the files attached?'),
                'confirm_text2' => $this->l('Are you sure to delete the files?'),
                'msgSuccess' => $this->l('The settings have been updated.'),
                'template_link' => $this->context->shop->physical_uri . $this->context->shop->virtual_uri . 'modules/' . $this->name . '/template.xlsx',
            )
        );
        $this->context->controller->addJS($this->_path . 'views/js/admin.js');
        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'sections' => $sections,
                //'ajax_link' => $this->context->shop->physical_uri . $this->context->shop->virtual_uri . 'modules/' . $this->name . '/ajax_back.php?secure_key=' . $this->secure_key
            )
        );

        return $this->renderCustomerList() . $this->renderCustomerGroupList() . $this->display(__FILE__, '/views/templates/admin/back-form.tpl');
    }

    public function renderCustomerList()
    {
        $customers = $this->getAllowedCustomers();
        $select = array();
        if ($customers) {
            foreach ($customers as $key => $value) {
                $select[$key]['id'] = $value['id_customer'];
                $select[$key]['name'] = $value['firstname'];
                $select[$key]['surname'] = $value['lastname'];
                $select[$key]['email'] = $value['email'];
            }
        }
        $fields_list = array(
            'id' => array('title' => $this->l('id'), 'id' => 'id', 'align' => 'left', 'width' => 30),
            'name' => array('title' => $this->l('name'), 'name' => 'name', 'align' => 'left', 'width' => 30),
            'surname' => array('title' => $this->l('surname'), 'surname' => 'surname', 'align' => 'left', 'width' => 30),
            'email' => array('title' => $this->l('email'), 'email' => 'email', 'align' => 'left', 'width' => 30),
        );
        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('delete');
        $helper->identifier = 'id';
        $helper->show_toolbar = false;
        $helper->title = $this->l('Allowed Customers');
        $helper->table = 'idxrprivatedownloads_customers';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        return $helper->generateList($select, $fields_list);
    }

    public function renderCustomerGroupList()
    {
        $customer_groups = $this->getAllowedCustomerGroups();
        $select = array();
        if ($customer_groups) {
            foreach ($customer_groups as $key => $value) {
                $select[$key]['id'] = $value['id_group'];
                $select[$key]['name'] = $value['name'];
            }
        }
        $fields_list = array(
            'id' => array('title' => $this->l('id'), 'id' => 'id', 'align' => 'left', 'width' => 30),
            'name' => array('title' => $this->l('name'), 'name' => 'name', 'align' => 'left', 'width' => 30),
        );
        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('delete');
        $helper->identifier = 'id';
        $helper->show_toolbar = false;
        $helper->title = $this->l('Allowed Customer Groups');
        $helper->table = 'idxrprivatedownloads_customer_groups';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        return $helper->generateList($select, $fields_list);
    }
    
    public function renderFormEdit()
    {
        $id_section = Tools::getValue('sectionid');
        return $this->renderFormAdd($id_section);
    }
    
    public function renderFormAdd($id_section = false)
    {
        if ($id_section) {
            $section = $this->getSection($id_section);
        }
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => ($id_section)?$this->l('Update Section'):$this->l('Add a new Section'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Section title'),
                        'lang' => true,
                        'name' => 'title',
                        'desc' => $this->l('Set the title for this section'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        
        if ($id_section) {
            $fields_form['form']['input'][] = array(
                'type' => 'hidden',
                'name' => 'id_section',
                'value' => (int)$id_section,
            );
        }
        
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSection';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(($id_section)?$section:false),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }
    
    public function renderFormEditFile()
    {
        $file_id = Tools::getValue('fileid');
        return $this->renderFormAddFile($file_id);
    }

    public function renderFormAddFile($file_id = false)
    {
        $file_info = $this->getFile($file_id);
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => ($file_id)?$this->l('Update file'):$this->l('Add a new File'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'file_lang',
                        'label' => ($file_id)?$this->l('Update file (left empty for left actual file)'):$this->l('Select a file'),
                        'name' => 'file',
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('File title'),
                        'lang' => true,
                        'name' => 'title',
                        'desc' => $this->l('Set the title for this file'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('File description'),
                        'lang' => true,
                        'name' => 'description',
                        'desc' => $this->l('Set a little description for this file'),
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_section',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        
        if ($file_id) {
            $fields_form['form']['input'][] = array(
                'type' => 'hidden',
                'name' => 'id_file',
            );
        }
        
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitFile';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(false, $file_info),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }
    
    public function renderFormFileAccess()
    {
        $id_file = Tools::getValue('fileid');
        $file = $this->getFile($id_file, $this->context->language->id)[0];
        $section = $this->getSection($file['section_id'], $this->context->language->id);
        $global_access = $this->getAllowedCustomers();
        $customer_groups = Group::getGroups($this->context->language->id);
        $default = array('name' => $this->l('Select a customer group'), 'id_group' => -1);
        array_unshift($customer_groups, $default);
        $file_access = $this->getFileAccess($id_file);
        $section_access = $this->getSectionAccess($file['section_id']);
        
        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'file' => $file,
                'section' => $section[0],
                'customer_groups' => $customer_groups,
                'faccess' => $file_access,
                'saccess' => $section_access,
                'global' => $global_access,
            )
        );

        return $this->display(__FILE__, '/views/templates/admin/back-accessfile-form.tpl');
    }
    
    public function renderFormSectionAccess()
    {
        $id_section = Tools::getValue('sectionid');
        $section = $this->getSection($id_section, $this->context->language->id);
        $global_access = $this->getAllowedCustomers();
        $customer_groups = Group::getGroups($this->context->language->id);
        $default = array('name' => $this->l('Select a customer group'), 'id_group' => -1);
        array_unshift($customer_groups, $default);
        $section_access = $this->getSectionAccess($id_section);
            
        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'section' => $section[0],
                'customer_groups' => $customer_groups,
                'saccess' => $section_access,
                'global' => $global_access,
            )
        );

        return $this->display(__FILE__, '/views/templates/admin/back-accesssection-form.tpl');
    }
    
    public function renderFormImportFiles()
    {
        $sections = $this->getSections();
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Update a config file'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'lang' => false,
                        'label' => $this->l('Section'),
                        'name' => 'id_section',
                        'desc' => $this->l('Enter the section where will be imported the files.'),
                        'options' => array(
                            'query' => $sections,
                            'id' => 'id_section',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Select a file'),
                        'name' => 'file',
                        'lang' => true,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitConfigurationFile';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(false),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form)).$this->renderImportFilesHelpBlock();
    }
    
    public function renderImportFilesHelpBlock()
    {
        return $this->display(__FILE__, 'views/templates/admin/import-helpblock.tpl');
    }

    public function renderFormAddCustomer()
    {
        $customers = Customer::getCustomers();
        $customer_simple = array();
        foreach ($customers as &$customer) {
            $customer['name'] = $customer['firstname'] . ' ' . $customer['lastname'] . ' - ' . $customer['email'];
            $customer_simple[] = $customer['name'];
        }
        $default = array('name' => $this->l('Select a customer'), 'id_customer' => -1);
        array_unshift($customers, $default);
        
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Allow a customer'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    /*
                    array(
                        'type' => 'select',
                        'lang' => false,
                        'label' => $this->l('Customer'),
                        'name' => 'id_customer',
                        'desc' => $this->l('Select the customer to allow.'),
                        'options' => array(
                            'query' => $customers,
                            'id' => 'id_customer',
                            'name' => 'name'
                        )
                    ),
                     */
                    array(
                        'type' => 'text',
                        'label' => $this->l('Customer name or email'),
                        'name' => 'customer',
                        'class' => 'input_customer_ac',
                        'desc' => $this->l('Insert customer name or email'),
                    ),
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCustomer';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        $this->context->controller->addJqueryPlugin('autocomplete');
        
        Media::addJsDef(
            array(
                'customers' => $customer_simple,
            )
        );
        
        return $helper->generateForm(array($fields_form)) . $this->renderFormAddCustomerGroup();
    }
    
    public function renderFormAddCustomerGroup()
    {
        $customer_groups = Group::getGroups($this->context->language->id);

        $default = array('name' => $this->l('Select a customer group'), 'id_group' => -1);
        array_unshift($customer_groups, $default);
        
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Allow a customer group'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'lang' => false,
                        'label' => $this->l('Customer Group'),
                        'name' => 'id_group',
                        'desc' => $this->l('Select the customer group to allow.'),
                        'options' => array(
                            'query' => $customer_groups,
                            'id' => 'id_group',
                            'name' => 'name'
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCustomerGroup';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm(array($fields_form));
    }

    public function renderFormAddFileWoutSection()
    {
        $sections = $this->getSections();
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Add a new File'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'lang' => false,
                        'label' => $this->l('Section'),
                        'name' => 'id_section',
                        'desc' => $this->l('Enter the section where will be the file.'),
                        'options' => array(
                            'query' => $sections,
                            'id' => 'id_section',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'file_lang',
                        'label' => $this->l('Select a file'),
                        'name' => 'file',
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('File title'),
                        'lang' => true,
                        'name' => 'title',
                        'desc' => $this->l('Set the title for this file'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('File description'),
                        'lang' => true,
                        'name' => 'description',
                        'desc' => $this->l('Set a little description for this file'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitFile';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues($section = false, $file = false)
    {
        $languages = Language::getLanguages(false);

        $fields = array();
        foreach ($languages as $lang) {
            $fields['file'][$lang['id_lang']] = Tools::getValue('file_' . (int) $lang['id_lang']);
            $fields['title'][$lang['id_lang']] = Tools::getValue('title_' . (int) $lang['id_lang']);
            $fields['description'][$lang['id_lang']] = Tools::getValue('description_' . (int) $lang['id_lang']);
        }
        $fields['id_section'] = Tools::getValue('sectionid');
        $fields['id_customer'] = Tools::getValue('id_customer');
        $fields['customer'] = '';
        $fields['id_group'] = Tools::getValue('id_group');
        if ($section) {
            foreach ($section as $lsect) {
                $fields['title'][$lsect['id_lang']] = $lsect['name'];
            }
        }
        if ($file) {
            foreach ($file as $fi) {
                $fields['id_file'] = $fi['id_file'];
                $fields['id_section'] = $fi['section_id'];
                $fields['title'][$fi['id_lang']] = $fi['name'];
                $fields['description'][$fi['id_lang']] = $fi['description'];
            }
        }
        return $fields;
    }

    public function hookDisplayCustomerAccount()
    {
        $this->context->smarty->assign(array(
              
                'es17' => $this->es17,
               
            ));
        $id_customer = $this->context->customer->id;
        if ($this->validCustomer($id_customer)) {
            if ($this->es17) {
                return $this->display(__FILE__, 'views/templates/front/account_blockdownloads.tpl');
            } else {
                return $this->display(__FILE__, '/views/templates/front/account_blockdownloads.tpl', $this->getCacheId());
            }
        }
    }

// hook new 
public function hookRightColumn($params){
       $this->context->smarty->assign(array(
              
                'es17' => $this->es17,
               
            ));
        $id_customer = $this->context->customer->id;
        if ($this->validCustomer($id_customer)) {
            if ($this->es17) {
                return $this->display(__FILE__, 'views/templates/front/account_blockdownloads.tpl');
            } else {
                return $this->display(__FILE__, '/views/templates/front/account_blockdownloads.tpl', $this->getCacheId());
            }
        }
    }

    public function getSections($id_customer = false)
    {
        $id_shop = (int) $this->context->shop->id;
        $id_lang = (int) $this->context->language->id;
        $sections_sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_sections sect '
                . 'inner join ' . _DB_PREFIX_ . 'idxrprivatedownloads_sections_lang sectl on sectl.id_section = sect.id_section '
                . 'where sect.id_shop = ' . $id_shop . ' and sectl.id_lang = ' . (int) $id_lang . ' order by position ASC;';
        $sections = Db::getInstance()->executeS($sections_sql);
        
        foreach ($sections as $key => &$section) {
            $valid = true;
            if ($id_customer) {
                $valid = $this->validSectionCustomer($section['id_section'], $id_customer);
            }
            if (!$valid) {
                unset($sections[$key]);
                continue;
            }
            $files_sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_files file '
                    . 'inner join ' . _DB_PREFIX_ . 'idxrprivatedownloads_files_lang filel on file.id_file = filel.id_file '
                    . 'where file.section_id = ' . (int) $section['id_section'] . ' and filel.id_lang = ' . (int) $id_lang . ' and file.id_shop = ' . (int) $id_shop . ' order by file.position ASC;';
            $files = Db::getInstance()->executeS($files_sql);
            foreach ($files as &$file) {
                $valid = true;
                if ($id_customer) {
                    $valid = $this->validFileCustomer($file['id_file'], $id_customer);
                }
                if (!$valid) {
                    continue;
                }
                $file['location'] = $file['id_file'] . '_' . $file['id_lang'] . '.' . $file['icon'];
                $file['icon'] = $this->getIconByFiletype($file['icon']);
            }
            $section['files'] = $files;
        }
        return $sections;
    }
    
    public function getSection($id_section, $id_lang = false)
    {
        $id_shop = (int) $this->context->shop->id;
        $sections_sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_sections sect '
                . 'inner join ' . _DB_PREFIX_ . 'idxrprivatedownloads_sections_lang sectl on sectl.id_section = sect.id_section '
                . 'where sect.id_shop = ' . $id_shop . ' and sect.id_section = ' . (int)$id_section . ' ';
        if ($id_lang) {
            $sections_sql .= 'and sectl.id_lang = ' . (int)$id_lang . ' ';
        }
        $sections_sql .= 'order by position ASC;';
        return Db::getInstance()->executeS($sections_sql);
    }
    
    public function getSectionFiles($id_section)
    {
        $files_sql = 'Select id_file from ' . _DB_PREFIX_ . 'idxrprivatedownloads_files where section_id = ' . (int)$id_section . ' order by position ASC;';
        return Db::getInstance()->executeS($files_sql);
    }
    
    public function getSectionAccess($id_section)
    {
        $access_sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_section_access where id_section = ' . (int)$id_section . ' and id_shop = ' . (int)$this->context->shop->id;
        $access = Db::getInstance()->executeS($access_sql);
        if ($access) {
            $return = array();
            foreach ($access as $row) {
                if ($row['is_group']) {
                    $group = new Group($row['id_cg'], $this->context->language->id);
                    $gr = array(
                        'is_group' => true,
                        'name' => $group->name,
                        'id' => $group->id,
                    );
                    $return[] = $gr;
                } else {
                    $customer = new Customer($row['id_cg']);
                    $cust = array(
                        'is_group' => false,
                        'name' => $customer->firstname . ' ' . $customer->lastname. ' ('.$customer->email.')',
                        'id' => $customer->id
                    );
                    $return[] = $cust;
                }
            }
            return $return;
        } else {
            return false;
        }
    }
    
    public function getFile($id_file, $id_lang = false)
    {
        $id_shop = (int) $this->context->shop->id;
        $files_sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_files file '
                    . 'inner join ' . _DB_PREFIX_ . 'idxrprivatedownloads_files_lang filel on file.id_file = filel.id_file '
                    . 'where file.id_file = ' . (int) $id_file . ' and file.id_shop = ' . (int) $id_shop;
        if ($id_lang) {
            $files_sql .= ' and filel.id_lang = '.(int)$id_lang;
        }
        return Db::getInstance()->executeS($files_sql);
    }
    
    public function getFileAccess($id_file)
    {
        $access_sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_file_access where id_file = ' . (int)$id_file . ' and id_shop = ' . (int)$this->context->shop->id;
        $access = Db::getInstance()->executeS($access_sql);
        if ($access) {
            $return = array();
            foreach ($access as $row) {
                if ($row['is_group']) {
                    $group = new Group($row['id_cg'], $this->context->language->id);
                    $gr = array(
                        'is_group' => true,
                        'name' => $group->name,
                        'id' => $group->id,
                    );
                    $return[] = $gr;
                } else {
                    $customer = new Customer($row['id_cg']);
                    $cust = array(
                        'is_group' => false,
                        'name' => $customer->firstname . ' ' . $customer->lastname. ' ('.$customer->email.')',
                        'id' => $customer->id
                    );
                    $return[] = $cust;
                }
            }
            return $return;
        } else {
            return false;
        }
    }
        
    public function existFile($id_file)
    {
        $exist_q = 'Select id_file from ' . _DB_PREFIX_ . 'idxrprivatedownloads_files where id_file = ' . (int) $id_file;
        return Db::getInstance()->getValue($exist_q);
    }
    
    public function getAllowedCustomers()
    {
        $id_shop = (int) $this->context->shop->id;
        $sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_customers idxcus '
                . 'inner join ' . _DB_PREFIX_ . 'customer cus on idxcus.id_customer = cus.id_customer '
                . 'where idxcus.id_shop = ' . $id_shop . ' and idxcus.active = 1';
        return Db::getInstance()->executeS($sql);
    }

    public function getAllowedCustomerGroups()
    {
        $id_shop = (int) $this->context->shop->id;
        $sql = 'Select * from ' . _DB_PREFIX_ . 'idxrprivatedownloads_customer_groups idxcusg '
                . 'inner join ' . _DB_PREFIX_ . 'group_lang cusg on idxcusg.id_group = cusg.id_group '
                . 'where idxcusg.id_shop = ' . $id_shop . ' and idxcusg.active = 1 and cusg.id_lang = ' . (int)$this->context->language->id;
        return Db::getInstance()->executeS($sql);
    }

    public function validCustomer($id_customer)
    {
        $allowed_global = $this->validGlobalCustomer($id_customer);
        $allowed_especific = $this->validEspecificCustomer($id_customer);
        $allowed = $allowed_global || $allowed_especific;
        $categories = $this->getSections($id_customer);
        return($allowed && $categories);
    }
    
    public function validGlobalCustomer($id_customer)
    {
        $id_shop = (int) $this->context->shop->id;
        $check_sql = 'Select id_customer from ' . _DB_PREFIX_ . 'idxrprivatedownloads_customers where id_customer = ' . (int) $id_customer . ' and id_shop = ' . (int) $id_shop . ' and active = 1';
        $allowed_global = Db::getInstance()->getValue($check_sql);
        $groups = Customer::getGroupsStatic($id_customer);
        $groups_string = implode(',', $groups);
        $checkg_sql = 'Select id_group from ' . _DB_PREFIX_ . 'idxrprivatedownloads_customer_groups where id_group in (' . pSQL($groups_string) . ') and id_shop = ' . (int) $id_shop . ' and active = 1';
        $allowed_global_group = Db::getInstance()->getValue($checkg_sql);
        return ($allowed_global || $allowed_global_group);
    }
    
    public function validEspecificCustomer($id_customer)
    {
        $id_shop = (int) $this->context->shop->id;
        $groups = Customer::getGroupsStatic($id_customer);
        $groups_condition = ' in ('.implode(',', $groups).')';
        
        $query1 = 'select id_access from ' . _DB_PREFIX_ . 'idxrprivatedownloads_section_access '
            . 'where ((is_group = 0 and id_cg = '.(int)$id_customer.') || (is_group = 1 and id_cg ' . pSQL($groups_condition) . ') && id_shop = ' . (int)$id_shop . ');';
        $r1 = Db::getInstance()->executeS($query1);
        
        $query2 = 'select id_access from ' . _DB_PREFIX_ . 'idxrprivatedownloads_file_access '
            . 'where ((is_group = 0 and id_cg = '.(int)$id_customer.') || (is_group = 1 and id_cg ' . pSQL($groups_condition) . ') && id_shop = ' . (int)$id_shop . ');';
        $r2 = Db::getInstance()->executeS($query2);
        
        if ($r1 || $r2) {
            return true;
        } else {
            return false;
        }
    }
    
    public function validSectionCustomer($id_section, $id_customer)
    {
        $id_shop = (int) $this->context->shop->id;
        $groups = Customer::getGroupsStatic($id_customer);

        $groups_condition = ' in ('.implode(',', $groups).')';
        
        $rules_q = 'select id_access from ' . _DB_PREFIX_ . 'idxrprivatedownloads_section_access '
            . 'where (id_shop = ' . (int)$id_shop . ' && id_section = ' . (int)$id_section . ' && ((is_group = 0 and id_cg = '.(int)$id_customer.') || (is_group = 1 and id_cg ' . pSQL($groups_condition) . ')));';

        $rules = Db::getInstance()->executeS($rules_q);
        
        if ($rules) {
            return true;
        }
        
        $norules_q = 'select id_access from ' . _DB_PREFIX_ . 'idxrprivatedownloads_section_access '
            . 'where (id_shop = ' . (int)$id_shop . ' && id_section = ' . (int)$id_section . ');';
        $norules = Db::getInstance()->executeS($norules_q);
        
        $global_access = $this->validGlobalCustomer($id_customer);
        
        if (!$norules && $global_access) {
            return true;
        } else {
            return false;
        }
    }
    
    public function validFileCustomer($id_file, $id_customer)
    {
        $id_shop = (int) $this->context->shop->id;
        $groups = Customer::getGroupsStatic($id_customer);
        $groups_condition = ' in ('.implode(',', $groups).')';
        
        $rules_q = 'select id_access from ' . _DB_PREFIX_ . 'idxrprivatedownloads_file_access '
            . 'where (id_shop = ' . (int)$id_shop . ' && id_file = ' . (int)$id_file . ' && (is_group = 0 and id_cg = '.(int)$id_customer.') || (is_group = 1 and id_cg ' . pSQL($groups_condition) . '));';
        
        $rules = Db::getInstance()->executeS($rules_q);
        if ($rules) {
            return true;
        }
        
        $file_info = $this->getFile($id_file)[0];
        $valid_section = $this->validSectionCustomer($file_info['section_id'], $id_customer);
        
        $norules_q = 'select id_access from ' . _DB_PREFIX_ . 'idxrprivatedownloads_file_access '
            . 'where (id_shop = ' . (int)$id_shop . ' && id_file = ' . (int)$id_file . ');';
        $norules = Db::getInstance()->executeS($norules_q);
                
        if (!$norules && $valid_section) {
            return true;
        } else {
            return false;
        }
    }

    public function addCustomer($id_customer)
    {
        $id_shop = (int) $this->context->shop->id;
        $this->deleteCustomer($id_customer);
        $add_sql = 'Insert into ' . _DB_PREFIX_ . 'idxrprivatedownloads_customers values (' . (int) $id_customer . ',1,' . (int) $id_shop . ');';
        return Db::getInstance()->execute($add_sql);
    }

    public function addCustomerGroup($id_group)
    {
        $id_shop = (int) $this->context->shop->id;
        $this->deleteCustomerGroup($id_group);
        $add_sql = 'Insert into ' . _DB_PREFIX_ . 'idxrprivatedownloads_customer_groups values (' . (int) $id_group . ',1,' . (int) $id_shop . ');';
        return Db::getInstance()->execute($add_sql);
    }

    public function deleteCustomer($id_customer)
    {
        $id_shop = (int) $this->context->shop->id;
        $delete_sql = 'Delete from ' . _DB_PREFIX_ . 'idxrprivatedownloads_customers where id_customer = ' . (int) $id_customer . ' and id_shop = ' . (int) $id_shop . ';';
        return Db::getInstance()->execute($delete_sql);
    }

    public function deleteCustomerGroup($id_group)
    {
        $id_shop = (int) $this->context->shop->id;
        $delete_sql = 'Delete from ' . _DB_PREFIX_ . 'idxrprivatedownloads_customer_groups where id_group = ' . (int) $id_group . ' and id_shop = ' . (int) $id_shop . ';';
        return Db::getInstance()->execute($delete_sql);
    }

    public function addSection($title, $id_section = false)
    {
        if ($id_section) {
            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                $data = array(
                    'name' => isset($title[(int) $lang['id_lang']]) ? pSQL($title[(int) $lang['id_lang']]) : ''
                );
                $where = 'id_section = ' . (int)$id_section . ' and id_lang = ' . (int)$lang['id_lang'];
                Db::getInstance()->update('idxrprivatedownloads_sections_lang', $data, $where);
            }
            return true;
        }
        $section = array(
            'position' => 0,
            'id_shop' => $this->context->shop->id
        );
        Db::getInstance()->insert('idxrprivatedownloads_sections', $section);
        $section_id = Db::getInstance()->Insert_ID();
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $data = array(
                'id_section' => (int)$section_id,
                'id_lang' => (int) $lang['id_lang'],
                'name' => isset($title[(int) $lang['id_lang']]) ? pSQL($title[(int) $lang['id_lang']]) : ''
            );
            Db::getInstance()->insert('idxrprivatedownloads_sections_lang', $data);
        }
    }

    public function delSection($id_section)
    {
        $where = 'id_section = ' . (int) $id_section;
        $section_files = $this->getSectionFiles($id_section);
        Db::getInstance()->delete('idxrprivatedownloads_sections', $where);
        Db::getInstance()->delete('idxrprivatedownloads_sections_lang', $where);
        foreach ($section_files as $id_file) {
            $this->delFile($id_file['id_file']);
        }
    }
    
    public function delFile($id_file)
    {
        $files = $this->getFile($id_file);
        foreach ($files as $file_data) {
            $file = _PS_ROOT_DIR_ . '/upload/' . $file_data['location'] . '_' . (int) $file_data['id_lang'];
            if (file_exists($file)) {
                unlink($file);
            }
        }
        $where = 'id_file = ' . (int) $id_file;
        Db::getInstance()->delete('idxrprivatedownloads_files', $where);
        Db::getInstance()->delete('idxrprivatedownloads_files_lang', $where);
    }
    
    private function grantaccessSection($id, $section, $isgroup = false)
    {
        $id_shop = (int) $this->context->shop->id;
        $exist_sql = 'Select id_access from ' . _DB_PREFIX_ .'idxrprivatedownloads_section_access '
            . 'where is_group = ' . (int)$isgroup . ' and  id_cg = ' . (int)$id . ' and id_section = ' . (int)$section . ' and id_shop = '. (int)$id_shop;
        $exist = Db::getInstance()->getValue($exist_sql);
        if (!$exist) {
            $data = array(
                'is_group' => (int)$isgroup,
                'id_cg' => (int)$id,
                'id_section' => (int)$section,
                'id_shop' => (int)$id_shop
            );
            Db::getInstance()->insert('idxrprivatedownloads_section_access', $data);
        }
    }
    
    private function revokeaccessSection($id_section, $is_group, $id)
    {
        $id_shop = (int) $this->context->shop->id;
        $del_sql = 'DELETE FROM ' . _DB_PREFIX_ . 'idxrprivatedownloads_section_access '
            . 'WHERE is_group = ' . (int)$is_group . ' and  id_cg = ' . (int)$id . ' and id_section = ' . (int)$id_section . ' and id_shop = '. (int)$id_shop;
        Db::getInstance()->execute($del_sql);
        $files = $this->getSectionFiles($id_section);
        foreach ($files as $file) {
            $this->revokeaccessFile($file['id_file'], $is_group, $id);
        }
    }
    
    private function grantaccessFile($id, $file, $isgroup = false)
    {
        $id_shop = (int) $this->context->shop->id;
        $exist_sql = 'Select id_access from ' . _DB_PREFIX_ .'idxrprivatedownloads_file_access '
            . 'where is_group = ' . (int)$isgroup . ' and  id_cg = ' . (int)$id . ' and id_file = ' . (int)$file . ' and id_shop = '. (int)$id_shop;
        $exist = Db::getInstance()->getValue($exist_sql);
        $file_info = $this->getFile($file, $this->context->language->id);
        if (!$exist && $file_info) {
            $data = array(
                'is_group' => (int)$isgroup,
                'id_cg' => (int)$id,
                'id_file' => (int)$file,
                'id_shop' => (int)$id_shop
            );
            Db::getInstance()->insert('idxrprivatedownloads_file_access', $data);
            $this->grantaccessSection($id, $file_info[0]['section_id'], $isgroup);
        }
    }
    
    private function revokeaccessFile($id_file, $is_group, $id)
    {
        $id_shop = (int) $this->context->shop->id;
        $del_sql = 'DELETE FROM ' . _DB_PREFIX_ . 'idxrprivatedownloads_file_access '
            . 'WHERE is_group = ' . (int)$is_group . ' and  id_cg = ' . (int)$id . ' and id_file = ' . (int)$id_file . ' and id_shop = '. (int)$id_shop;
        return Db::getInstance()->execute($del_sql);
    }

    public function getIconByFiletype($extension)
    {
        $extension_map = array(
            'excel' => array('xls', 'xlsx', 'xlsm', 'xlsb'),
            'image' => array('jpg', 'png', 'jpeg', 'bmp', 'tiff', 'tif', 'gif', 'dds'),
            'pdf' => array('pdf', 'ppt', 'pps', 'pptx'),
            'word' => array('odt', 'txt', 'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'rtf'),
            'zip' => array('zip', 'rar', '7z', 'gzip', 'tar', 'gz')
        );

        $icon_image = 'empty';

        foreach ($extension_map as $icon => $extensions) {
            if (in_array($extension, $extensions)) {
                $icon_image = $icon;
            }
        }

        return $this->context->shop->physical_uri . $this->context->shop->virtual_uri . 'modules/' . $this->name . '/views/img/icons/' . $icon_image . '.png';
    }

    public function extractEmailAddress($string)
    {
        $emails = array();
        foreach (preg_split('/\s/', $string) as $token) {
            $email = filter_var(filter_var($token, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
            if ($email !== false) {
                $emails[] = $email;
            }
        }
        return $emails;
    }
    
    public function importFiles($files, $id_section)
    {
        $id_shop = (int) $this->context->shop->id;
        foreach ($files as $file) {
            $extension = pathinfo($file[0]['url'], PATHINFO_EXTENSION);
            $shaname = sha1($file[0]['url']);
            $data = array(
                'location' => $shaname,
                'section_id' => $id_section,
                'icon' => $extension,
                'id_shop' => $id_shop
            );
            Db::getInstance()->insert('idxrprivatedownloads_files', $data);
            $file_id = Db::getInstance()->Insert_ID();
            foreach ($file as $file_lang) {
                $id_lang = Language::getIdByIso($file_lang['language']);
                $target_file = _PS_ROOT_DIR_ . '/upload/' . $shaname . '_' . (int) $id_lang;
                file_put_contents($target_file, fopen($file_lang['url'], 'r'));
                $lang_data = array(
                    'id_file' => $file_id,
                    'id_lang' => (int) $id_lang,
                    'name' => pSQL($file_lang['title']),
                    'description' => pSQL($file_lang['description']),
                );
                Db::getInstance()->insert('idxrprivatedownloads_files_lang', $lang_data);
            }
        }
    }
    
    public function generateTemplate()
    {
        
        $header = array();
        $line = array();
        
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $header[] = 'Idioma';
            $header[] = 'Fichero '.$lang['iso_code'];
            $header[] = 'Titulo '.$lang['iso_code'];
            $header[] = 'Descripcion '.$lang['iso_code'];
            
            $line[] = $lang['iso_code'];
            $line[] = 'http://filesource.com/file.doc';
            $line[] = 'file title';
            $line[] = 'file description';
        }
        
        $data = array(
            $header,
            $line
        );

        $writer = new XLSXWriter();
        $writer->writeSheet($data);
        $writer->writeToFile('template.xlsx');
    }

    public function setInnovaTabs()
    {
        $isoLinks = InnovaTools_2_0_0::getIsoLinks($this);
        $this->innovatabs = array();
        $this->innovatabs [] = array(
                "title" => $this->l('Configuration'),
                "icon" => "wrench",
                "link" => "helpGenerateForm",
                "type" => "tab",
                "show" => "both",
                "active" => (
                    Tools::getValue('sectionid') ||
                    Tools::getValue('editSection') ||
                    Tools::getValue('editFile') ||
                    Tools::getValue('editSectionAccess') ||
                    Tools::getValue('editFileAccess') ||
                    Tools::getValue('addcas') ||
                    Tools::getValue('addgas') ||
                    Tools::getValue('delas') ||
                    Tools::getValue('addcaf') ||
                    Tools::getValue('addgaf') ||
                    Tools::getValue('delaf'))?false:true,
            );
        $this->innovatabs [] = array(
                "title" => $this->l('Add an user'),
                "icon" => "user",
                "link" => "renderFormAddCustomer",
                "type" => "tab",
                "show" => "both",
            );
        $this->innovatabs [] = array(
                "title" => $this->l('Create a section'),
                "icon" => "plus",
                "link" => "renderFormAdd",
                "type" => "tab",
                "show" => "both",
            );
        if (Tools::getValue('editSection')) {
            $this->innovatabs [] = array(
                "title" => $this->l('Edit a section'),
                "icon" => "pencil",
                "link" => "renderFormEdit",
                "type" => "tab",
                "show" => "both",
                "active" => true,
            );
        }
        if (Tools::getValue('editSectionAccess') || Tools::getValue('addcas') || Tools::getValue('addgas') || Tools::getValue('delas')) {
            $this->innovatabs [] = array(
                "title" => $this->l('Edit section access'),
                "icon" => "lock",
                "link" => "renderFormSectionAccess",
                "type" => "tab",
                "show" => "both",
                "active" => true,
            );
        }
        $this->innovatabs [] = array(
                "title" => $this->l('Add a file'),
                "icon" => "upload",
                "link" => (Tools::getValue('sectionid'))?"renderFormAddFile":"renderFormAddFileWoutSection",
                "type" => "tab",
                "show" => "both",
                "active" => (
                    Tools::getValue('sectionid') &&
                    !Tools::getValue('editSection') &&
                    !Tools::getValue('editSectionAccess') &&
                    !Tools::getValue('addcas') &&
                    !Tools::getValue('addgas') &&
                    !Tools::getValue('delas'))?true:false,
            );
        
        if (Tools::getValue('editFile')) {
            $this->innovatabs [] = array(
                "title" => $this->l('Edit a file'),
                "icon" => "pencil",
                "link" => "renderFormEditFile",
                "type" => "tab",
                "show" => "both",
                "active" => true,
            );
        }
        
        if (Tools::getValue('editFileAccess') || Tools::getValue('addcaf') || Tools::getValue('addgaf') || Tools::getValue('delaf')) {
            $this->innovatabs [] = array(
                "title" => $this->l('Edit file access'),
                "icon" => "lock",
                "link" => "renderFormFileAccess",
                "type" => "tab",
                "show" => "both",
                "active" => true,
            );
        }
        
        $this->innovatabs [] = array(
                "title" => $this->l('Upload a configuration'),
                "icon" => "upload",
                "link" => "renderFormImportFiles",
                "type" => "tab",
                "show" => "both",
            );
        $this->innovatabs [] = array(
                "title" => $this->l('Documentation'),
                "icon" => "file",
                "link" => $this->doclink,
                "type" => "doc",
                "show" => "both",
            );
        $this->innovatabs [] = array(
                "title" => $this->l('Support'),
                "icon" => "life-saver",
                "link" => $isoLinks["support"],
                "type" => "url",
                "show" => "whmcs",
            );
            /*
        $this->innovatabs [] = array(
                "title" => $this->l('Opinion'),
                "icon" => "comments",
                "link" => $isoLinks["ratings"],
                "type" => "url",
                "show" => "both",
            );
            */
            /*
        $this->innovatabs [] = array(
                "title" => $this->l('Certified Agency'),
                "icon" => "trophy",
                "link" => $isoLinks["certified"],
                "type" => "url",
                "show" => "addons",
            );
            */
        $this->innovatabs [] = array(
                "title" => $this->l('Our Modules'),
                "icon" => "cubes",
                "link" => $isoLinks["ourmodules"],
                "type" => "url",
                "show" => "both",
            );
    }

    public function innovaTitle()
    {
        //tabs version
        $innovaTabs = "";
        if (method_exists(get_class($this), "setInnovaTabs")) {
            $innovaTabs=$this->setInnovaTabs();
        }
        $this->smarty->assign(array(
            "module_dir" => $this->_path,
            "module_name" => $this->displayName,
            "module_description" => $this->description,
            "isoLinks" => InnovaTools_2_0_0::getIsoLinks($this),
            "isAddons" => InnovaTools_2_0_0::isAddons($this),
            "tabs" => InnovaTools_2_0_0::getVersionTabs($this),
        ));

        $this->context->controller->addCSS(($this->_path)."views/css/backinnova.css", "all");
        return $this->display(__FILE__, "views/templates/admin/innova-title.tpl");
    }
}
