<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innovadeluxe SL
* @copyright 2017 Innovadeluxe SL

* @license   INNOVADELUXE
*/

class IdxrprivatedownloadsDownloadsModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $display_column_left = false;

    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
        //include_once($this->module->getLocalPath().'blockwishlist.php');
    }

    /**
     * See ModuleFrontController initContent
     */
    public function initContent()
    {
        parent::initContent();

        if (!$this->context->customer->isLogged() && $this->php_self != 'authentication' && $this->php_self != 'password') {
            if ($this->module->es17) {
                $this->setTemplate('module:idxrprivatedownloads/views/templates/front/notlogged17.tpl');
            } else {
                $this->setTemplate('notlogged.tpl');
            }
        } else {
            if ($this->module->validCustomer($this->context->customer->id)) {
                if($sections = $this->module->getSections($this->context->customer->id)){
                    foreach($sections as $j => $section){
                        if(count($section['files']) > 0){
                            foreach($section['files'] as $index => $file){
                                if (!$this->module->validFileCustomer((int)$file['id_file'], $this->context->customer->id) || !$this->module->existFile((int)$file['id_file'])) {
                                    unset($section['files'][$index]);
                                    $sections[$j]['files'] = array_values($section['files']);
                                }
                            }
                            if(count($section['files']) == 0){
                                unset($sections[$j]);
                                $sections = array_values($sections);
                            }
                        }

                    }
                }

                $this->context->controller->addCSS($this->module->getLocalPath() . 'views/css/front.css', 'all');
                $this->context->smarty->assign(array(
                    'sections' => $sections,
                    'es17' => $this->module->es17,
                    'base_dir_ssl_17' => __PS_BASE_URI__,
                    'file_url' => $this->context->shop->physical_uri . $this->context->shop->virtual_uri . 'modules/' . $this->module->name . '/file_controller.php'
                ));
                if ($this->module->es17) {
                    $this->setTemplate('module:idxrprivatedownloads/views/templates/front/downloads17.tpl');
                } else {
                    $this->setTemplate('downloads.tpl');
                }
            } else {
                if ($this->module->es17) {
                    $this->setTemplate('module:idxrprivatedownloads/views/templates/front/notlogged17.tpl');
                } else {
                    $this->setTemplate('notlogged.tpl');
                }
            }
        }
    }
}
