/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2017 Innova Deluxe SL

* @license   INNOVADELUXE
*/

$(document).ready(function () {

    var $mySections = $("#sections");				
    $mySections.sortable({
        opacity: 0.6,
        cursor: "move",
        update: function() {
            var data = $(this).sortable('toArray', {attribute: "data-item"});
            $.post(ajax_link, { action: "order", section: JSON.stringify(data)}).done(function( data ) {
                showSuccessMessage("" + msgSuccess + "");
            });
        }
    });

    $('.list-group').each(function() {
        $(this).sortable({
            opacity: 0.6,
            cursor: "move",
            update: function() {
                var data = $(this).sortable('toArray', {attribute: "data-file"});
                $.post(ajax_link, { action: "orderfiles", file: JSON.stringify(data)}).done(function( data ) {
                    showSuccessMessage("" + msgSuccess + "");
                });
            }
        });
    });

    $('.delete_section').on('click', function() {
        var section_id = $(this).attr('id');
        section_id = section_id.replace('delete_section_','');
        if(confirm(confirm_text)){            
            $.post(ajax_link, { action: "delete", section: section_id }).done(function( data ) {
                $('#section_'+section_id).remove();
                showSuccessMessage("" + msgSuccess + "");
            });
        }
    });
    
    $('.delete_file').on('click', function() {
        var section_id = $(this).attr('id');
        file_id = section_id.replace('delete_file_','');
        
        if(confirm(confirm_text2)){            
            $.post(ajax_link, { action: "deletefile", file: file_id }).done(function( data ) {
                $('#file_'+file_id).remove();
                if (($("#id_file").length > 0)){
                    form_file_id =  $("#id_file").val();
                    if(form_file_id == file_id){
                        $("#id_file").closest('div[class^="panel"]').remove();
                    }
                }
                showSuccessMessage("" + msgSuccess + "");
            });
        }
    });
    
    $('#example_file_button').on('click', function() {
        var doc_link = ajax_link + '&action=exampleFile';
        $.ajax({
            url: doc_link,
            type: 'POST',
            success: function() {
                window.location = template_link;
            }
        });
    });
    
    if(typeof customers != "undefined"){
        $('.input_customer_ac').autocomplete(customers);
    }
});