{**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2017 Innova Deluxe SL
* @license   INNOVADELUXE
*}

{if $es17}
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="idxfacturae" href="{$link->getModuleLink('idxrprivatedownloads','downloads')|escape:'htmlall':'UTF-8'}" title="{l s='Go to my files' mod='idxrprivatedownloads'}">
  <span class="link-item">
    <i class="fa fa-download" aria-hidden="true"></i>
    {l s='Autocad Files' mod='idxrprivatedownloads'}
  </span>
</a>    
{else}
<li>
    <a title="{l s='Go to my files' mod='idxrprivatedownloads'}" href="{$link->getModuleLink('idxrprivatedownloads','downloads')|escape:'htmlall':'UTF-8'}">
        <i class="fa fa-download" aria-hidden="true"></i>
        <span>{l s='Autocad Files' mod='idxrprivatedownloads'}</span>
    </a>
</li>
{/if}