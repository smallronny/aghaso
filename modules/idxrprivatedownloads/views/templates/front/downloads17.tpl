{**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2017 Innova Deluxe SL
* @license   INNOVADELUXE
*}

    {extends file='customer/page.tpl'}
    {block name='page_content_container'}<h2 class="title-zone-privated">{l s='Diseños de nuestros productos en Autocad' mod='idxrprivatedownloads'}</h2>
        <div class='tabs'> 
        <ul class="nav nav-tabs" >
            {foreach from=$sections item=section name=tab}
                <li class="nav-item" ><a class="nav-link {if $smarty.foreach.tab.index === 0}active{/if}" href="#privatedown{$section.id_section|escape:'htmlall':'UTF-8'}"  data-toggle="tab">{$section.name|escape:'htmlall':'UTF-8'}</a></li>
            {/foreach}
        </ul>
        <div id="tab-content" class="tab-content">
            {foreach from=$sections item=section name=sc}
                <div  class="tab-pane fade {if $smarty.foreach.sc.index === 0}active in{/if}" id="privatedown{$section.id_section|escape:'htmlall':'UTF-8'}">
                    <div class="list-group">
                        {foreach from=$section.files item=file}
                            <a href="{$file_url|escape:'htmlall':'UTF-8'}?section={$file.section_id|escape:'htmlall':'UTF-8'}&file={$file.id_file|escape:'htmlall':'UTF-8'}" class="list-group-item" target="_blank" >
                                <p><img src="{$file.icon|escape:'htmlall':'UTF-8'}"/> <b>{$file.name|escape:'htmlall':'UTF-8'}. </b><i>{if $file.description}{$file.description}{/if}</i> <i class="icon icon-download icon-3x pull-right transparent"></i></p>
                            </a>
                        {/foreach}
                    </div>
                </div>
            {/foreach}
            </div>
        </div>
    {/block}