{**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2017 Innova Deluxe SL
* @license   INNOVADELUXE
*}

{extends file='customer/page.tpl'}
{block name='page_content_container'}
<h1>{l s='Privated zone' mod='idxrprivatedownloads'}</h1>
<p>
    {l s='This page is only for registered users, please go to ' mod='idxrprivatedownloads'} 
    <a class="border_bottom" href="{$link->getPageLink('my-account.php', true)}">{l s='authentication page' mod='idxrprivatedownloads'}</a> 
    {l s=' and login' mod='idxrprivatedownloads'} 
</p>
{/block}