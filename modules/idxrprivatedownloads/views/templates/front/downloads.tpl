{**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2017 Innova Deluxe SL
* @license   INNOVADELUXE
*}

    <h2>{l s='Your files' mod='idxrprivatedownloads'}</h2>
    <ul class="nav nav-tabs">
        {foreach from=$sections item=section name=tab}
            <li {if $smarty.foreach.tab.index == 0}class='active'{/if}><a href="#{$section.id_section|escape:'htmlall':'UTF-8'}" role="tab" data-toggle="tab">{$section.name|escape:'htmlall':'UTF-8'}</a></li>
            {/foreach}
    </ul>
    <div class="tab-content">
        {foreach from=$sections item=section name=sc}
            <div role="tabpanel" class="tab-pane fade {if $smarty.foreach.sc.index == 0}active in{/if}" id="{$section.id_section|escape:'htmlall':'UTF-8'}">
                <div class="list-group">
                    {foreach from=$section.files item=file}
                        <a href="{$file_url|escape:'htmlall':'UTF-8'}?section={$file.section_id|escape:'htmlall':'UTF-8'}&file={$file.id_file|escape:'htmlall':'UTF-8'}" class="list-group-item" target="_blank" >

                            <p><img src="{$file.icon|escape:'htmlall':'UTF-8'}"/> <b>{$file.name|escape:'htmlall':'UTF-8'}. </b><i>{if $file.description}{$file.description}{/if}</i> <i class="icon icon-download icon-3x pull-right transparent"></i></p>

                        </a>
                    {/foreach}
                </div>
            </div>
        {/foreach}
    </div>

