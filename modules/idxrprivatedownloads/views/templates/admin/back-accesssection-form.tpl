{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2018 Innova Deluxe SL
* @license   INNOVADELUXE
*}

<div class="panel">
    <h3>
        <i class="icon-list-ul"></i> {l s='Access granted for the section' mod='idxrprivatedownloads'} {$section.name}
    </h3>
    <div id="sectionAccessContent" class="container">

        <div class="form-wrapper">

            <form action="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&addcas=1&sectionid={$section.id_section|escape:'htmlall':'UTF-8'}" method="POST">
                <div class="form-group">
                    <label class="control-label col-lg-2">
                        {l s='Customer name or email' mod='idxrprivatedownloads'}
                    </label>
                    <div class="col-lg-9">
                        <input name="customer_section_access" id="customer_section_access" value="" class="input_customer_ac ac_input" autocomplete="off" type="text">
                        <p class="help-block">
                            {l s='Insert customer name or email' mod='idxrprivatedownloads'}
                        </p>
                    </div>
                    <div class="col-lg-1">
                        <button type="submit" class="btn btn-success">
                            <i class="icon-save"></i>
                        </button>
                    </div>
                </div>
            </form>                
                        
            <form action="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&addgas=1&sectionid={$section.id_section|escape:'htmlall':'UTF-8'}" method="POST">
                <div class="form-group">
                    <label class="control-label col-lg-2">
                        {l s='Customer Group' mod='idxrprivatedownloads'}
                    </label>
                    <div class="col-lg-9">
                        <select name="id_group" class=" fixed-width-xl" id="id_group">
                            {foreach from=$customer_groups item='option'}
                            <option value="{$option.id_group}">{$option.name}</option>
                            {/foreach}
                        </select>
                        <p class="help-block">
                            {l s='Select the customer group to allow.' mod='idxrprivatedownloads'}
                        </p>
                    </div>
                    <div class="col-lg-1">
                        <button type="submit" class="btn btn-success" id=''>
                            <i class="icon-save"></i>
                        </button>
                    </div>
                </div>
            </form>
                        
        </div>



        <div class="col-lg-6">
            <div class="panel">
                <h3>
                    <i class="icon-lock"></i> {l s='Access granted for this section' mod='idxrprivatedownloads'}
                </h3>
                {if $saccess}
                    {foreach from=$saccess item=access}
                        <div class="row">
                            <div class="col-lg-2">
                                {if $access.is_group}
                                    {l s='Group' mod='idxrprivatedownloads'}
                                {else}
                                    {l s='Customer' mod='idxrprivatedownloads'}
                                {/if}
                            </div>
                            <div class="col-lg-9">
                                {$access.name}           
                            </div>
                            <div class="col-lg-1 text-right">
                                <a href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&delas=1&sectionid={$section.id_section|escape:'htmlall':'UTF-8'}&ig={$access.is_group}&id={$access.id}">
                                    <button type="button" class="btn btn-danger">
                                        <i class="icon-remove"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    {/foreach}
                {else}
                    {l s='Access not granted' mod='idxrprivatedownloads'}
                {/if}
            </div>
        </div>
        <div class="col-lg-6">            
            <div class="panel">
                <h3>
                    <i class="icon-lock"></i> {l s='Access granted by global configuration' mod='idxrprivatedownloads'}
                </h3>
                {if $global}
                    {foreach from=$global item=gaccess}
                        <div class="row">
                            {$gaccess.firstname} {$gaccess.lastname} ({$gaccess.email})
                        </div>
                    {/foreach}
                {else}
                    {l s='Access not granted' mod='idxrprivatedownloads'}
                {/if}
            </div>
        </div> 
    </div>
</div>
