{**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2017 Innova Deluxe SL
* @license   INNOVADELUXE
*}

<div class="panel">
    <h3>
        <i class="icon-list-ul"></i> {l s='Section list' mod='idxrprivatedownloads'}
	<span class="panel-heading-action">
            <a class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&addSection=1">
                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Add new section' mod='idxrprivatedownloads'}" data-html="true">
                    <i class="process-icon-new "></i>
                </span>
            </a>
	</span>
    </h3>
    <div id="sectionsContent">
        <div id="sections">
        {foreach from=$sections item=section}
            <div id="section_{$section.id_section|escape:'htmlall':'UTF-8'}" class="panel" data-item="{$section.id_section|escape:'htmlall':'UTF-8'}">
                <div class="row">
                    <div class="col-lg-1">
                        <span><i class="icon-arrows"></i></span>
                    </div>
                    <div class="col-lg-10">
                        <h3>{$section.name|escape:'htmlall':'UTF-8'} ({$section.files|count} {l s='files in this section' mod='idxrprivatedownloads'})</h3>
                        <ul class="list-group" id="files_{$section.id_section|escape:'htmlall':'UTF-8'}">                                                    
                        {foreach from=$section.files item=file}
                            <li class="list-group-item col-lg-12" id="li_file_{$file.id_file|escape:'htmlall':'UTF-8'}" data-file="{$file.id_file|escape:'htmlall':'UTF-8'}">
                                <div class="col-lg-1">
                                    <span><i class="icon-arrows"></i></span>
                                    <img src="{$file.icon|escape:'htmlall':'UTF-8'}"/>
                                </div>
                                <div class="col-lg-9">
                                    
                                    <h4 class="list-group-item-heading">{$file.name|escape:'htmlall':'UTF-8'} </h4>
                                    <p class="list-group-item-text">{if $file.description|escape:'htmlall':'UTF-8'}({$file.description|escape:'htmlall':'UTF-8'}){/if}</p>
                                    
                                </div>
                                <div class="col-lg-2">
                                    <a href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&editFileAccess=1&fileid={$file.id_file|escape:'htmlall':'UTF-8'}">
                                        <button type="button" class="btn access_file" id='access_file_{$file.id_file|escape:'htmlall':'UTF-8'}'>
                                            <i class="icon-lock"></i>
                                        </button>
                                    </a>
                                    <a href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&editFile=1&fileid={$file.id_file|escape:'htmlall':'UTF-8'}">
                                        <button type="button" class="btn btn-default edit_section" id='edit_file_{$file.id_file|escape:'htmlall':'UTF-8'}'>
                                            <i class="icon-pencil"></i>
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger delete_file" id='delete_file_{$file.id_file|escape:'htmlall':'UTF-8'}'>
                                        <i class="icon-remove"></i>
                                    </button>
                                </div>
                            </li>
                        {/foreach}
                        </ul>          
                        <a href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&addFile=1&sectionid={$section.id_section|escape:'htmlall':'UTF-8'}">
                            <button type="button" class="btn btn-success add_file pull-right spacing" id='add_file_{$section.id_section|escape:'htmlall':'UTF-8'}'><i class="icon-plus"></i>&nbsp;{l s='Add a file to this section' mod='idxrprivatedownloads'}</button>
                        </a>
                    </div>
                    <div class="col-lg-1 text-right">
                        <a href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&editSectionAccess=1&sectionid={$section.id_section|escape:'htmlall':'UTF-8'}">
                            <button type="button" class="btn access_section" id='access_section_{$section.id_section|escape:'htmlall':'UTF-8'}'>
                                <i class="icon-lock"></i>
                            </button>
                        </a>
                        <a href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=idxrprivatedownloads&editSection=1&sectionid={$section.id_section|escape:'htmlall':'UTF-8'}">
                            <button type="button" class="btn btn-default edit_section" id='edit_section_{$section.id_section|escape:'htmlall':'UTF-8'}'>
                                <i class="icon-pencil"></i>
                            </button>
                        </a>
                        <button type="button" class="btn btn-danger delete_section" id='delete_section_{$section.id_section|escape:'htmlall':'UTF-8'}'>
                            <i class="icon-remove"></i></button>
                    </div>
                </div>
            </div>
        {/foreach}
        </div>
    </div>
</div>
