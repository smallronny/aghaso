{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2018 Innova Deluxe SL
* @license   INNOVADELUXE
*}

<div class="panel">
    
    <div class="panel-heading">
        <i class="icon-life-ring"></i> {l s='Help about how import documents' mod='idxrprivatedownloads'}
    </div>

    <div class='panel-body'>
        <div class="alert alert-info">
        <p>Para poder subir archivos, sobre todo si está en diferentes idiomas de la siguiente forma:</p>
        <br />
        
        <b>Elementos de la tabla</b>
        <hr />
        
        <ol>
        <li><b>IDIOMA :</b> código ISO ( p.ej: es ) </li>
        <li><b>ARCHIVO URL :</b> ruta del archivo ( p.ej: /mitiendadeejemplo.com/mis_archivos/miarchivo.pdf )</li>
        <li><b>TÍTULO :</b> un título/nombre para el elemento a mostrar ( p.ej: Documentación )</li>
        <li><b>DESCRIPCIÓN :</b> una decripción para indicar la referncia del elemnto ( p.ej: Documentación técinca del producto )</li>

        </ol>

        <table class="table table-bordered table-striped">

        <tr>

        <th>Idioma</th>
        <th>Archivo URL</th>
        <th>Título</th>
        <th>Descripción</th>

        <th>Idioma</th>
        <th>Archivo URL</th>
        <th>Título</th>
        <th>Descripción</th>

       

        </tr>

        <tr>

        <td>es</td>
        <td>/mitiendadeejemplo.com/mis_archivos/mi-archivo.pdf</td>
        <td>Mi archivo</td>
        <td>Mi archivo de descarga</td>

        <td>en</td>
        <td>/mitiendadeejemplo.com/mis_archivos/mi-archivo-ingles.pdf</td>
        <td>My file</td>
        <td>My download file</td>

        

        </tr>
        <tr>

        <td>es</td>
        <td>/mitiendadeejemplo.com/mis_archivos/mi-documento.pdf</td>
        <td>Mi documento</td>
        <td>Mi documento de descarga</td>

        <td>en</td>
        <td>/mitiendadeejemplo.com/mis_archivos/mi-documento-ingles.pdf</td>
        <td>My document</td>
        <td>My download document</td>

        

        </tr>



        </table>


        </div>
        <div class="alert alert-warning"><p>Los códigos ISO tienen que ser iguales que en su plantilla</div>

    </div>

    <div class="panel-footer">
        <button class="btn btn-default pull-right" id='example_file_button' >
            <i class="process-icon-download"></i>{l s='Download template for fill' mod='idxrprivatedownloads'}
        </button>
    </div>

</div>