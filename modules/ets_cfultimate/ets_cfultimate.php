<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

header('Access-Control-Allow-Origin: *');

if (!defined('_ETS_MODULE_')) {
    define('_ETS_MODULE_', 'ets_cfultimate');
}
if (!defined('_PS_VERSION_') || !defined('_ETS_MODULE_'))
    exit;
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Form_Tag.php');
require_once(dirname(__FILE__) . '/classes/function.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Contact.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Contact_Form.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Form_Tag_Manager.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Contact_Message.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Submission.php');
require_once(dirname(__FILE__) . '/classes/mail.php');
require_once(dirname(__FILE__) . '/classes/pipe.php');
require_once(dirname(__FILE__) . '/classes/integration.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Recaptcha.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Validation.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Link.php');
require_once(dirname(__FILE__) . '/classes/ETS_CFU_Browser.php');
require_once(dirname(__FILE__) . '/classes/AdminModuleAdapterController.php');
require_once(dirname(__FILE__) . '/defines.php');
include_once(dirname(__FILE__) . '/cache/temp');

class Ets_cfultimate extends Module
{
    public $_html;
    public $_errors;
    public $_ps17;

    public $rmHookShortcode =false;

    public static $translation;

    public function __construct()
    {
        $this->name = 'ets_cfultimate';
        $this->tab = 'front_office_features';
        $this->version = '1.0.8';
        $this->author = 'ETS-Soft';
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        $this->module_key = '4bfac04a2e06bdebd1fb9c03ce95409b';
        if (version_compare(_PS_VERSION_, '1.7', '>='))
            $this->_ps17 = true;
        parent::__construct();
        $this->displayName = $this->l('Contact Form Ultimate');
        $this->description = $this->l('Visual drag and drop contact form builder for Prestashop. Create any kind of contact form you want.');
        $this->shortlink = 'https://mf.short-link.org/';
        //ajax.
        if(Tools::getValue('configure')==$this->name && Tools::isSubmit('othermodules'))
        {
            $this->displayRecommendedModules();
        }
        if (Tools::getValue('action') == 'etsCfuGetCountMessageContactForm') {
            die(Tools::jsonEncode(array(
                'count' => $this->getCountUnreadMessage(),
            )));
        }
        self::$translation = array(
            'email_is_black_list' => $this->l('This email address is in blacklist'),
        );

    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function install()
    {
        return parent::install()
            && $this->_registerHook()
            && $this->installDb()
            && $this->_installTabs()
            && $this->initContact()
            && $this->createTemplateMail();
    }

    /**
     * @return int
     * @throws PrestaShopDatabaseException
     */
    public function getCountUnreadMessage()
    {
        return (int)Db::getInstance()->getValue('
            SELECT COUNT(DISTINCT  m.id_contact_message)
            FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message m
            INNER JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_message_shop ms ON (m.id_contact_message=ms.id_contact_message)
            lEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact c on (m.id_contact=c.id_contact)
            WHERE m.readed = 0 AND ms.id_shop=' . (int)Context::getContext()->shop->id
        );
    }

    /**
     * @return string
     */
    public function getBaseLink()
    {
        return (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') ? 'https://' : 'http://') . $this->context->shop->domain . $this->context->shop->getBaseURI();
    }

    /**
     * @param $string
     * @return string
     */
    static public function getEmailToString($string)
    {
        $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
        preg_match_all($pattern, $string, $matches);
        return isset($matches[0][0]) ? $matches[0][0] : '';
    }

    public function smartyBackOffice($args = array())
    {
        $this->smarty->assign($args);
        return $this->display(__FILE__, 'defines.tpl');
    }

    public static function checkEmailBlackLists($email)
    {
        $listipemails = explode("\n", Configuration::get('ETS_CFU_IP_BLACK_LIST'));
        if ( count($listipemails) ){
            for( $i=0; $i<count($listipemails); $i++){
                if( ( $emailCheck = trim($listipemails[$i])) && trim($email) == $emailCheck){
                    return self::$translation['email_is_black_list'];
                }
            }
        }
        return false;
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getContent()
    {
        if (!$this->active) {
            $this->_html .= $this->displayWarning($this->l('You have to enable Contact Form Ultimate module to configure its features'));
            return $this->_html;
        }
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            $this->context->controller->addJqueryUI('ui.widget');
        else
            $this->context->controller->addJqueryPlugin('widget');
        $this->context->controller->addJqueryPlugin('tagify');

        if (Tools::isSubmit('etsCfuExportContactForm'))
            $this->generateArchive();
        if (Tools::isSubmit('etsCfuGetFormElementAjax')) {
            die(Tools::jsonEncode(
                array(
                    'form_html' => $this->replace_all_form_tags(Tools::getValue('short_code')),
                )
            ));
        }
        if (Tools::isSubmit('contactFormUltimateDefault')) {
            if ((int)Tools::getValue('contactFormUltimateDefault') == 1 && Tools::getValue('id_contact')) {
                Configuration::updateValue('ETS_CONTACT_FORM_ULTIMATE_DEFAULT', Tools::getValue('id_contact'));
            } elseif ((int)Tools::getValue('contactFormUltimateDefault') == 0 && Configuration::get('ETS_CONTACT_FORM_ULTIMATE_DEFAULT') == Tools::getValue('id_contact')) {
                Configuration::updateValue('ETS_CONTACT_FORM_ULTIMATE_DEFAULT', 0);
            }
        } elseif (Tools::isSubmit('etsCfuSaveMessageUpdate') && $id_contact = Tools::getValue('id_contact')) {
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'ets_cfu_contact SET save_message="' . (int)Tools::getValue('etsCfuSaveMessageUpdate') . '" WHERE id_contact=' . (int)$id_contact);
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateContactForm', true));
        } elseif (Tools::isSubmit('etsCfuSubmitSaveContact') || Tools::isSubmit('etsCfuSubmitSaveAndStayContact')) {
            $this->_html .= $this->saveContactFrom();
        } elseif (Tools::isSubmit('etsCfuDuplicateContact') && $id_contact = Tools::getValue('id_contact')) {
            $contact = new ETS_CFU_Contact($id_contact);
            $languages = Language::getLanguages(false);
            $identity = Tools::passwdGen(2, 'NUMERIC');
            foreach ($languages as $language) {
                $contact->title[$language['id_lang']] = $contact->title[$language['id_lang']] . ' [' . $this->l('duplicated') . ']';
                $contact->title_alias[$language['id_lang']] = $contact->title_alias[$language['id_lang']] . '-' . $identity;
            }
            $contact->position = (int)Db::getInstance()->getValue('SELECT count(*) FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_shop where id_shop=' . (int)$this->context->shop->id);
            if ($contact->add()) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateContactForm', true) . '&conf=19');
            }
        } elseif (Tools::isSubmit('etsCfuDeleteContact') && $id_contact = Tools::getValue('id_contact')) {
            $contact = new ETS_CFU_Contact($id_contact);
            $contact->delete();
            Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_shop WHERE id_contact=' . (int)$id_contact);
            $contacts = Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'ets_cfu_contact c ,' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs  WHERE c.id_contact=cs.id_contact AND  id_shop=' . (int)$this->context->shop->id . ' order by c.position asc');
            $messages = Db::getInstance()->getValue('SELECT attachments FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message WHERE id_contact="' . (int)$id_contact . '"');
            if ($messages) {
                foreach ($messages as $message) {
                    if ($message['attachments']) {
                        foreach (explode(',', $message['attachments']) as $attachment) {
                            @unlink(dirname(__FILE__) . '/views/img/etscfu_upload/' . $attachment);
                        }
                    }
                }
            }
            if ($contacts) {
                foreach ($contacts as $key => $contact) {
                    Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'ets_cfu_contact set position="' . (int)$key . '" WHERE id_contact=' . (int)$contact['id_contact']);
                }
            }
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateContactForm', true) . '&conf=1');
        } elseif (Tools::isSubmit('etsCfuPreview') && $id_contact = Tools::getValue('id_contact')) {
            $contact = new ETS_CFU_Contact($id_contact, $this->context->language->id);
            die(Tools::jsonEncode(array(
                'form_html' => $this->replace_all_form_tags($contact->short_code),
                'contact' => $contact,
            )));
        } elseif (Tools::isSubmit('etsCfuActiveUpdate') && $id_contact = Tools::getValue('id_contact')) {
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'ets_cfu_contact SET active="' . (int)Tools::getValue('etsCfuActiveUpdate') . '" WHERE id_contact=' . (int)$id_contact);
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateContactForm', true));
        }
        if (Tools::isSubmit('etsCfuEditContact') || Tools::isSubmit('etsCfuAddContact')) {
            $assign = array(
                'link' => $this->context->link,
                'link_basic' => $this->getBaseLink(),
                'mod_dr_ctf' => $this->_path,
                'ETS_CFU_ENABLE_TMCE' => Configuration::get('ETS_CFU_ENABLE_TMCE'),
                'languages' => $this->context->controller->getLanguages(),
                'defaultFormLanguage' => $this->context->employee->id_lang,
                'img_dir' => '../modules/' . $this->name . '/views/img/',
                'max_upload_file' => Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE'),
            );

            if (Configuration::get('ETS_CFU_ENABLE_RECAPTCHA')) {
                $assign['re_captcha_v3'] = Configuration::get('ETS_CFU_RECAPTCHA_TYPE') != 'v2';
            }

            $this->context->smarty->assign(
                array(
                    'showShortcodeHook' => Configuration::get('ETS_CFU_ENABLE_HOOK_SHORTCODE')
                )
            );

            $this->smarty->assign($assign);
            $this->_html .= $this->renderAddContactForm();
            $this->_html .= $this->display(__FILE__, 'url.tpl');
            $this->_html .= $this->display(__FILE__, 'textarea.tpl');
            $this->_html .= $this->display(__FILE__, 'text.tpl');
            $this->_html .= $this->display(__FILE__, 'telephone.tpl');
            $this->_html .= $this->display(__FILE__, 'submit.tpl');
            $this->_html .= $this->display(__FILE__, 'select.tpl');
            $this->_html .= $this->display(__FILE__, 'radio.tpl');
            $this->_html .= $this->display(__FILE__, 'quiz.tpl');
            $this->_html .= $this->display(__FILE__, 'number.tpl');
            $this->_html .= $this->display(__FILE__, 'email.tpl');
            $this->_html .= $this->display(__FILE__, 'checkbox.tpl');
            $this->_html .= $this->display(__FILE__, 'captcha.tpl');
            $this->_html .= $this->display(__FILE__, 'recaptcha.tpl');
            $this->_html .= $this->display(__FILE__, 'acceptance.tpl');
            $this->_html .= $this->display(__FILE__, 'date.tpl');
            $this->_html .= $this->display(__FILE__, 'file.tpl');
            $this->_html .= $this->display(__FILE__, 'html.tpl');
            $this->_html .= $this->display(__FILE__, 'password.tpl');
            $this->_html .= $this->display(__FILE__, 'referrence.tpl');
        } else {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateDashboard', true));
        }
        $this->smarty->assign(array(
            'html_content' => $this->_html,
            'etsCfuOkImport' => Tools::getValue('etsCfuOkImport'),
            '_PS_JS_DIR_' => _PS_JS_DIR_,
            'ETS_CFU_ENABLE_TMCE' => Configuration::get('ETS_CFU_ENABLE_TMCE'),
        ));
        return $this->display(__FILE__, 'admin.tpl');
    }

    /**
     * @return void
     * @throws PrestaShopDatabaseException
     */
    private function generateArchive()
    {
        $zip = new ZipArchive();
        $cacheDir = dirname(__FILE__) . '/cache/';
        $zip_file_name = 'contact_form_ultimate' . date('dmYHis') . '.zip';
        if ($zip->open($cacheDir . $zip_file_name, ZipArchive::OVERWRITE | ZipArchive::CREATE) === true) {
            if (!$zip->addFromString('Data-Info.xml', $this->renderDataInfo())) {
                $this->_errors[] = $this->l('Cannot create Contact-Info.xml');
            }
            if (!$zip->addFromString('Contact-Info.xml', $this->renderContactFormData())) {
                $this->_errors[] = $this->l('Cannot create Contact-Info.xml');
            }
            $zip->close();
            if (!is_file($cacheDir . $zip_file_name)) {
                $this->_errors[] = $this->l(sprintf('Could not create %1s', $cacheDir . $zip_file_name));
            }
            if (!$this->_errors) {
                if (ob_get_length() > 0) {
                    ob_end_clean();
                }
                ob_start();
                header('Pragma: public');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: public');
                header('Content-Description: File Transfer');
                header('Content-type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $zip_file_name . '"');
                header('Content-Transfer-Encoding: binary');
                ob_end_flush();
                readfile($cacheDir . $zip_file_name);
                @unlink($cacheDir . $zip_file_name);
                exit;
            }
        }
        {
            echo $this->l('An error occurred during the archive generation');
            die;
        }
    }

    /**
     * @return string
     */
    private function renderDataInfo()
    {
        $xml_output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $xml_output .= '<entity_profile>' . "\n";
        $xml_output .= '<version>' . $this->version . '</version>';
        $xml_output .= '</entity_profile>' . "\n";
        return $xml_output;
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    private function renderContactFormData()
    {
        $xml_output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $xml_output .= '<entity_profile>' . "\n";
        $contacts = Db::getInstance()->executeS('SELECT c.* FROM ' . _DB_PREFIX_ . 'ets_cfu_contact c, ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs WHERE c.id_contact=cs.id_contact AND cs.id_shop="' . (int)$this->context->shop->id . '"');
        if ($contacts) {
            foreach ($contacts as $contact) {
                $xml_output .= '<contactfrom id="' . (int)$contact['id_contact'] . '">';
                foreach ($contact as $key => $value) {
                    if ($key != 'id_contact') {
                        $xml_output .= '<' . $key . '><![CDATA[' . $value . ']]></' . $key . '>' . "\n";
                    }
                }
                $contactLanguages = Db::getInstance()->executeS('SELECT cl.*,l.iso_code FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_lang cl,' . _DB_PREFIX_ . 'lang l WHERE cl.id_lang=l.id_lang AND cl.id_contact=' . (int)$contact['id_contact']);
                if ($contactLanguages) {
                    foreach ($contactLanguages as $datalanguage) {
                        $xml_output .= '<datalanguage iso_code="' . $datalanguage['iso_code'] . '"' . ($datalanguage['id_lang'] == Configuration::get('PS_LANG_DEFAULT') ? ' default="1"' : '') . ' >' . "\n";
                        foreach ($datalanguage as $key => $value)
                            if ($key != 'id_contact' && $key != 'id_lang' && $key != 'iso_code')
                                $xml_output .= '<' . $key . '><![CDATA[' . $value . ']]></' . $key . '>' . "\n";
                        $xml_output .= '</datalanguage>' . "\n";
                    }
                }
                $xml_output .= '</contactfrom>';
            }
        }
        $xml_output .= '</entity_profile>' . "\n";
        return $xml_output;
    }

    /**
     * @param $form
     * @return array|mixed|string|string[]|null
     */
    public function replace_all_form_tags($form)
    {
        $manager = ETS_CFU_Form_Tag_Manager::get_instance();
        $manager->set_instance();
        if (ets_cfu_autop_or_not()) {
            $form = $manager->normalize($form);
            $form = ets_cfu_autop($form);
        }
        $form = $manager->replace_all($form);
        $this->scanned_form_tags = $manager->get_scanned_tags();
        return $form;
    }

    /**
     * @return string
     * @throws PrestaShopException
     */
    public function saveContactFrom()
    {
        $errors = array();
        $languages = Language::getLanguages(false);
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $configs = Ets_cfudefines::getInstance($this)->getFields('contact')['form']['input'];
        if ($configs) {
            foreach ($configs as $config) {
                $key = $config['name'];
                if (trim(Tools::getValue('thank_you_page')) == 'thank_page_url' && ($key == 'thank_you_alias' || $key == 'thank_you_message' || $key == 'thank_you_page_title')){
                    continue;
                }

                if (isset($config['lang']) && $config['lang']) {
                    $languages = Language::getLanguages(false);
                    if ($key == 'thank_you_url'){
                        if (trim(Tools::getValue('thank_you_page')) == 'thank_page_url'){
                            if (! Tools::getValue($key . '_' . $id_lang_default)){
                                $errors[] = $config['label'] . ' ' . $this->l('is required');
                            }elseif (!Validate::isAbsoluteUrl(trim(Tools::getValue($key . '_' . $id_lang_default)))){
                                $errors[] = $config['label'] . ' ' . $this->l('is invalid');
                            }
                        }
                        continue;
                    }
                    if (trim(Tools::getValue('thank_you_page')) == 'thank_page_default' && ($key == 'thank_you_message' || $key == 'thank_you_page_title')){
                        if (!trim(Tools::getValue($key . '_' . $id_lang_default))){
                            $errors[] = $config['label'] .' '. $this->l('thank you page') .' '. $this->l('is required');
                        }
                        continue;
                    }
                    
                    if (isset($config['required']) && $config['required'] && $config['type'] != 'switch' && trim(Tools::getValue($key . '_' . $id_lang_default) == '')) {
                        $errors[] = $config['label'] . ' ' . $this->l('is required');
                    } elseif (isset($config['validate']) && method_exists('Validate', $config['validate'])) {
                        $validate = $config['validate'];
                        if (!Validate::$validate(trim(Tools::getValue($key . '_' . $id_lang_default))))
                            $errors[] = $config['label'] . ' ' . $this->l('is invalid');
                        else {
                            if ($languages) {
                                foreach ($languages as $lang) {
                                    if (Tools::getValue($key . '_' . $lang['id_lang']) && !Validate::$validate(trim(Tools::getValue($key . '_' . $lang['id_lang']))))
                                        $errors[] = $config['label'] . ' ' . $lang['iso_code'] . ' ' . $this->l('is invalid');
                                }
                            }
                        }
                        unset($validate);
                    }
                } else {
                    if (isset($config['required']) && $config['required'] && isset($config['type']) && $config['type'] == 'file') {
                        if ($this->$key == '' && !isset($_FILES[$key]['size']))
                            $errors[] = $config['label'] . ' ' . $this->l('is required');
                        elseif (isset($_FILES[$key]['size'])) {
                            $fileSize = round((int)$_FILES[$key]['size'] / (1024 * 1024));
                            if ($fileSize > 100)
                                $errors[] = $config['label'] . ' ' . $this->l('Upload file cannot be larger than 100MB');
                        }
                    } else {
                        if (isset($config['required']) && $config['required'] && $config['type'] != 'switch' && trim(Tools::getValue($key) == '')) {
                            $errors[] = $config['label'] . ' ' . $this->l('is required');
                        } elseif (!is_array(Tools::getValue($key)) && isset($config['validate']) && method_exists('Validate', $config['validate'])) {
                            $validate = $config['validate'];
                            if (!Validate::$validate(trim(Tools::getValue($key))))
                                $errors[] = $config['label'] . ' ' . $this->l('is invalid');
                            unset($validate);
                        }
                    }
                }
            }
        }
        if (Tools::getValue('enable_form_page')) {
            foreach ($languages as $language) {
                if (($title_alias = Tools::getValue('title_alias_' . $language['id_lang'])) && !Validate::isLinkRewrite($title_alias)) {
                    $errors[] = $this->l('Title alias') . ' (' . $language['iso_code'] . ') is invalid.';
                } elseif ($title_alias && ($alias = ETS_CFU_Contact::getIdContactByAlias($title_alias, (int)$language['id_lang'])) && $alias != Tools::getValue('id_contact')) {
                    $errors[] = $this->l('Title alias') . ' (' . $language['iso_code'] . ') is exists.';
                }
            }
        }

        if (trim(Tools::getValue('thank_you_page')) == 'thank_page_default') {
            foreach ($languages as $language) {
                $thank_you_alias = Tools::getValue('thank_you_alias_' . $language['id_lang']);
                if ($thank_you_alias && !Validate::isLinkRewrite($thank_you_alias)) {
                    $errors[] = $this->l('Thank page alias') . ' (' . $language['iso_code'] . ') is invalid.';
                }
            }
        }


        if (!$errors) {
            if ($id_contact = Tools::getValue('id_contact')) {
                $contact = new ETS_CFU_Contact($id_contact);
            } else {
                $contact = new ETS_CFU_Contact();
                $contact->position = (int)Db::getInstance()->getValue('SELECT count(*) FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_shop where id_shop=' . (int)$this->context->shop->id);
            }
            $contact->id_employee = (int)$this->context->employee->id;
            $contact->id_employee = $this->context->employee->id;
            if ($configs) {
                foreach ($configs as $config) {
                    $key = $config['name'];
                    if (trim(Tools::getValue('thank_you_page')) == 'thank_page_url' && ($key == 'thank_you_page_title' || $key == 'thank_you_message')){
                        continue;
                    }
                    if (trim(Tools::getValue('thank_you_page')) == 'thank_page_default' && $key == 'thank_you_url'){
                        continue;
                    }
                    if (isset($config['lang']) && $config['lang']) {
                        $valules = array();
                        foreach ($languages as $lang) {
                            if ($config['type'] == 'switch')
                                $valules[$lang['id_lang']] = (int)trim(Tools::getValue($key . '_' . $lang['id_lang'])) ? 1 : 0;
                            else
                                $valules[$lang['id_lang']] = trim(Tools::getValue($key . '_' . $lang['id_lang'])) ? trim(Tools::getValue($key . '_' . $lang['id_lang'])) : trim(Tools::getValue($key . '_' . $id_lang_default));
                        }
                        $contact->$key = $valules;
                    } elseif ($config['type'] == 'switch') {
                        $contact->$key = (int)Tools::getValue($key) ? 1 : 0;
                    } elseif ($config['type'] == 'file') {
                        //Upload file
                        if (isset($_FILES[$key]['tmp_name']) && isset($_FILES[$key]['name']) && $_FILES[$key]['name']) {
                            $salt = Tools::substr(sha1(microtime()), 0, 10);
                            $type = Tools::strtolower(Tools::substr(strrchr($_FILES[$key]['name'], '.'), 1));
                            $imageName = @file_exists(dirname(__FILE__) . '/../views/img/upload/' . Tools::strtolower($_FILES[$key]['name'])) || Tools::strtolower($_FILES[$key]['name']) == $contact->$key ? $salt . '-' . Tools::strtolower($_FILES[$key]['name']) : Tools::strtolower($_FILES[$key]['name']);
                            $fileName = dirname(__FILE__) . '/../views/img/upload/' . $imageName;
                            if (file_exists($fileName)) {
                                $errors[] = $config['label'] . ' ' . $this->l('File name already exists. Try to rename the file and upload again');
                            } else {
                                $imagesize = @getimagesize($_FILES[$key]['tmp_name']);
                                if (!$errors && isset($_FILES[$key]) &&
                                    !empty($_FILES[$key]['tmp_name']) &&
                                    !empty($imagesize) &&
                                    in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
                                ) {
                                    $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                                    if ($error = ImageManager::validateUpload($_FILES[$key]))
                                        $errors[] = $error;
                                    elseif (!$temp_name || !move_uploaded_file($_FILES[$key]['tmp_name'], $temp_name))
                                        $errors[] = $this->l('Cannot upload file');
                                    elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))
                                        $errors[] = $this->l('An error occurred during the image upload process.');
                                    if (isset($temp_name))
                                        @unlink($temp_name);
                                    if (!$errors) {
                                        if ($contact->$key != '') {
                                            $oldImage = dirname(__FILE__) . '/../views/img/upload/' . $contact->$key;
                                            if (file_exists($oldImage))
                                                @unlink($oldImage);
                                        }
                                        $contact->$key = $imageName;
                                    }
                                }
                            }
                        }
                        //End upload file
                    } elseif ($config['type'] == 'categories' && isset($config['tree']['use_checkbox']) && $config['tree']['use_checkbox'])
                        $contact->$key = implode(',', Tools::getValue($key));
                    elseif ($config['type'] == 'group'){
                        $contact->$key = implode(',', Tools::getValue($key));
                    }elseif ($config['type'] == 'checkbox') {
                        $values = array();
                        foreach ($config['values']['query'] as $value) {
                            if (Tools::getValue($key . '_' . $value['id'])) {
                                $values[] = Tools::getValue($key . '_' . $value['id']);
                            }
                        }
                        $contact->$key = implode(',', $values);
                    } elseif ($config['type'] == 'select' && isset($config['multiple']) && $config['multiple']) {
                        $contact->$key = implode(',', Tools::getValue($key));
                    } else
                        $contact->$key = trim(Tools::getValue($key));
                }
                $valules_title_alias = array();
                $valules_title_thank = array();
                foreach ($languages as $lang) {
                    if (!Tools::getValue('title_alias_' . $lang['id_lang']) && !Tools::getValue('title_alias_' . $id_lang_default)) {
                        $valules_title_alias[$lang['id_lang']] = trim(Tools::getValue('title_' . $lang['id_lang'])) ? Tools::link_rewrite(trim(Tools::getValue('title_' . $lang['id_lang']))) : Tools::link_rewrite(trim(Tools::getValue('title_' . $id_lang_default)), true);
                    } else
                        $valules_title_alias[$lang['id_lang']] = trim(Tools::getValue('title_alias_' . $lang['id_lang'])) ? trim(Tools::getValue('title_alias_' . $lang['id_lang'])) : trim(Tools::getValue('title_alias_' . $id_lang_default));

                    if (!Tools::getValue('thank_you_alias_' . $lang['id_lang']) && !Tools::getValue('thank_you_alias_' . $id_lang_default)) {
                        $valules_title_thank[$lang['id_lang']] = trim(Tools::getValue('thank_you_page_title_' . $lang['id_lang'])) ? Tools::link_rewrite(trim(Tools::getValue('thank_you_page_title_' . $lang['id_lang']))) : Tools::link_rewrite(trim(Tools::getValue('thank_you_page_title_' . $id_lang_default)), true);
                        $checkAliasExit = $this->checkAliasExit($valules_title_thank[$lang['id_lang']], (int)$lang['id_lang'],$contact->id ? $contact->id : false);
                        if ($checkAliasExit){
                            $valules_title_thank[$lang['id_lang']] = $valules_title_thank[$lang['id_lang']].'-'.($contact->id ? $contact->id : (int)$this->getMaxId()+1);
                        }
                    } else{

                        $valules_title_thank[$lang['id_lang']] = trim(Tools::getValue('thank_you_alias_' . $lang['id_lang'])) ? trim(Tools::getValue('thank_you_alias_' . $lang['id_lang'])) : trim(Tools::getValue('thank_you_alias_' . $id_lang_default));
                        $checkAliasExit = $this->checkAliasExit($valules_title_thank[$lang['id_lang']], (int)$lang['id_lang'],$contact->id ? $contact->id : false);
                        if ( $checkAliasExit){
                            $valules_title_thank[$lang['id_lang']] = $valules_title_thank[$lang['id_lang']].'-'.($contact->id ? $contact->id : (int)$this->getMaxId()+1);
                        }
                    }
                }
                $contact->title_alias = $valules_title_alias;
                $contact->thank_you_alias = $valules_title_thank;
            }
        }

        if (!count($errors)) {
            if ($contact->id && $contact->update()) {
                Tools::redirectAdmin($this->getRedirect(array('id_contact' => $contact->id, 'conf' => 4)));
            } elseif (!$contact->id && $contact->add()) {
                Tools::redirectAdmin($this->getRedirect(array('id_contact' => $contact->id, 'conf' => 3)));
            } else
                $errors[] = $this->l('Unknown error happens');
        }
        if ($errors)
            return $this->displayError($errors);
    }

    public function getRedirect($args = array())
    {
        return $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . (isset($args['conf']) ? '&conf=' . $args['conf'] : '') . '&etsCfuEditContact=1' . (!empty($args['id_contact']) ? '&id_contact=' . (int)$args['id_contact'] : '') . '&current_tab=' . Tools::getValue('current_tab'). '&current_tab_email=' . Tools::getValue('current_tab_email');
    }

    /**
     * @param bool $token
     * @return string
     * @throws PrestaShopException
     */
    public function getAdminLink($token = true)
    {
        return $this->context->link->getAdminLink('AdminModules', $token) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
    }

    /**
     * @return string
     * @throws PrestaShopException
     */
    public function renderAddContactForm()
    {
        $contact_fields = Ets_cfudefines::getInstance($this)->getFields('contact');
        if (Tools::isSubmit('id_contact')) {
            $contact_fields['form']['input'][] = array(
                'type' => 'hidden',
                'name' => 'id_contact'
            );
            $contact_fields['form']['legend']['new'] = $this->getAdminLink() . '&etsCfuAddContact=1';
        }

        if ($this->rmHookShortcode){
            foreach($this->contact_fields['form']['input'] as $key => $value){
                if ($value['name'] == 'hook'){
                    unset($this->contact_fields['form']['input'][$key]);
                }
            }
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'etsCfuSubmitSaveContact';
        $helper->currentIndex = $this->getAdminLink(false) . (Tools::isSubmit('etsCfuEditContact') && Tools::getValue('id_contact') ? '&etsCfuEditContact=1&id_contact=' . (int)Tools::getValue('id_contact') : '&etsCfuAddContact=1');
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $assign = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'language' => array(
                'id_lang' => $language->id,
                'iso_code' => $language->iso_code
            ),
            'fields_value' => $this->getAddContactFieldsValues($contact_fields),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => '../modules/' . $this->name . '/views/img/',
            'page' => 'contact',
            'link_basic' => $this->getBaseLink(),
            'name_controller' => 'edit_contact_form',
            'ps15' => version_compare(_PS_VERSION_, '1.6', '<') ? true : false,
            'google_captcha' => $this->context->link->getAdminLink('AdminContactFormUltimateIntegration') . '&current_tab=google',
            'inputs' => Ets_cfudefines::getInstance($this)->getFields('inputs'),
        );
        $helper->tpl_vars = $assign;
        $helper->override_folder = '/';

        return $helper->generateForm(array($contact_fields));
    }

    /**
     * @return array
     */
    public function getAddContactFieldsValues($contact_fields)
    {
        $fields = array();
        $languages = $this->context->controller->getLanguages();
        $inputs = $contact_fields['form']['input'];
        if (!$inputs)
            return $fields;
        if ($id_contact = (int)Tools::getValue('id_contact')) {
            $contact = new ETS_CFU_Contact($id_contact);
            foreach ($inputs as $input) {
                $key = $input['name'];
                if (isset($input['lang']) && $input['lang']) {
                    foreach ($languages as $l) {
                        $lang_values = $contact->{$key};
                        $fields[$key][$l['id_lang']] = Tools::getValue($key . '_' . $l['id_lang'], isset($lang_values[$l['id_lang']]) ? $lang_values[$l['id_lang']] : '');
                    }
                } elseif ($input['name'] == 'id_contact') {
                    $fields['id_contact'] = Tools::getValue('id_contact');
                    $fields['link_contact'] = $contact->enable_form_page ? Ets_CfUltimate::getLinkContactForm(Tools::getValue('id_contact')) : '';
                } elseif ($input['type'] == 'checkbox') {
                    if (($values = Tools::getValue($key, explode(',', $contact->{$key})))) {
                        foreach ($values as $value) {
                            $fields[$key . '_' . $value] = 1;
                        }
                    }
                } elseif ($input['type'] == 'select' && isset($input['multiple']) && $input['multiple']) {
                    $fields[$key . '[]'] = Tools::getValue($key, explode(',', $contact->{$key}));
                } elseif (!isset($input['tree']) && $input['type'] != 'checkbox')
                    $fields[$key] = Tools::getValue($key, $contact->{$key});
                $this->multiValue($fields, $key, $input);
            }
        } else {
            foreach ($inputs as $input) {
                $key = $input['name'];
                if (isset($input['lang']) && $input['lang']) {
                    foreach ($languages as $l) {
                        $fields[$key][$l['id_lang']] = isset($input['default']) && $input['default'] ? Tools::getValue($key . '_' . $l['id_lang'], $input['default']) : Tools::getValue($key . '_' . $l['id_lang']);
                    }
                } elseif ($input['name'] == 'id_contact') {
                    $fields['id_contact'] = Tools::getValue('id_contact');
                } elseif ($input['type'] == 'checkbox') {
                    if (($values = isset($input['default']) && $input['default'] ? Tools::getValue($key, explode(',', $input['default'])) : Tools::getValue($key))) {
                        foreach ($values as $value) {
                            $fields[$key . '_' . $value] = 1;
                        }
                    }
                } elseif ($input['type'] == 'select' && isset($input['multiple']) && $input['multiple']) {
                    $fields[$key . '[]'] = Tools::getValue($key);
                } elseif (!isset($input['tree']) && $input['type'] != 'checkbox')
                    $fields[$key] = isset($input['default']) && $input['default'] ? Tools::getValue($key, $input['default']) : Tools::getValue($key);
                $this->multiValue($fields, $key, $input);
            }
        }
        return $fields;
    }

    public function multiValue(&$fields, $key, $input)
    {
        if (!(isset($fields[$key])))
            return false;
        if (isset($input['multi']) && $input['multi']) {
            $fields['multi_' . $key] = $fields[$key] ? ets_cfu_mailbox_list($fields[$key]) : array();
        }
    }

    /**
     * @param $id_contact_form
     * @param int $id_lang
     * @return string
     */
    static public function getLinkContactForm($id_contact_form, $id_lang = 0,$controller = 'contact')
    {
        $context = Context::getContext();
        $id_lang = $id_lang ? $id_lang : $context->language->id;
        $contact_form = new ETS_CFU_Contact($id_contact_form, $id_lang);
        $blogLink = new ETS_CFU_Link();
        $params = array();
        //if ($controller == 'contact')
            $params['id_contact'] = $id_contact_form;


        if (Configuration::get('PS_REWRITING_SETTINGS') ) {
            $url = $blogLink->getBaseLinkFriendly(null, null) . $blogLink->getLangLinkFriendly($id_lang, null, null);
            if ($controller == 'contact' && $contact_form->id && $contact_form->title_alias){
                $url .= (($subAlias = Configuration::get('ETS_CFU_CONTACT_ALIAS', Context::getContext()->language->id)) ? $subAlias : 'contact-form') . '/' . (Configuration::get('ETS_CFU_REMOVE_ID') ? '' : (int)$contact_form->id . '-') . $contact_form->title_alias . (Configuration::get('ETS_CFU_URL_SUFFIX') ? '.html' : '');
            }elseif ($controller == 'thank'){
                $url .= (($subAlias = Configuration::get('ETS_CFU_CONTACT_ALIAS', Context::getContext()->language->id)) ? $subAlias : 'contact-form') . '/thank/'. (Configuration::get('ETS_CFU_REMOVE_ID') ? '' : (int)$contact_form->id . '-') . $contact_form->thank_you_alias . (Configuration::get('ETS_CFU_URL_SUFFIX') ? '.html' : '');
            }
            return $url;
        }
        return $context->link->getModuleLink('ets_cfultimate', $controller,$params);
    }

    public function initContact()
    {
        if (class_exists('ETS_CFU_Contact')) {

            $group = Group::getGroups($this->context->language->id, true);
            $total_group = count($group);
            $group_temp = array();
            for( $i = 0; $i< $total_group; $i++){
                $group_temp[] = $group[$i]['id_group'];
            }
            $str_update = implode(',',$group_temp);

            $contact = new ETS_CFU_Contact();
            $languages = Language::getLanguages(false);
            $contact_fields = Ets_cfudefines::getInstance($this)->getFields('contact');
            if (($inputs = $contact_fields['form']['input'])) {
                foreach ($inputs as $input) {
                    if (isset($input['lang']) && $input['lang']) {
                        $values = array();
                        foreach ($languages as $l) {
                            $values[$l['id_lang']] = isset($input['default']) ? $input['default'] : '';
                        }
                        $contact->{$input['name']} = $values;
                    } else {
                        $contact->{$input['name']} = isset($input['default']) ? $input['default'] : '';
                    }
                }
            }
            $html_sc = $this->getHTML(array('type' => 'sc'));
            $html_msg = $this->getHTML(array('type' => 'msg'));
            foreach ($languages as $l) {
                $contact->title[$l['id_lang']] = 'Sample contact form';
                $contact->title_alias[$l['id_lang']] = 'sample-contact-form';
                $contact->short_code[$l['id_lang']] = $html_sc;
                $contact->message_body[$l['id_lang']] = $html_msg;
                $contact->message_body2[$l['id_lang']] = $html_msg;

                $contact->thank_you_page_title[$l['id_lang']] = 'Sample thank page';
                $contact->thank_you_alias[$l['id_lang']] = 'sample-thank-page';
                $contact->thank_you_message[$l['id_lang']] = "<p>Thank you for contacting us.</p> <p>This message is to confirm that you have successfully submitted the contact form.</p> <p>We'll get back to you shortly.</p>";
                $contact->message_email_black_list[$l['id_lang']] = 'Your email is blocked. Contact webmaster for more info.';

            }
            $contact->render_form = $this->getHTML(array('type' => 'rf'));
            $contact->email_to = Configuration::get('PS_SHOP_NAME') . ' <' . Configuration::get('PS_SHOP_EMAIL') . '>';
            $contact->email_to2 = '[text-652]<[email-668]>';
            $contact->email_from = '[text-652]<[email-668]>';
            $contact->email_from2 = Configuration::get('PS_SHOP_NAME') . ' <' . Configuration::get('PS_SHOP_EMAIL') . '>';
            $contact->additional_headers = '[text-652]<[email-668]>';
            $contact->additional_headers2 = Configuration::get('PS_SHOP_NAME') . ' <' . Configuration::get('PS_SHOP_EMAIL') . '>';
            $contact->id_employee = Context::getContext()->employee->id;
            $contact->group_access = $str_update;
            return $contact->add();
        }
        return true;
    }

    public function getHTML($args = array())
    {
        $args['languages'] = Language::getLanguages(false);
        $args['id_lang_default'] = $this->context->language->id;
        $args['icon_link'] = '../modules/ets_cfultimate/views/img/';
        $this->smarty->assign($args);
        return $this->display(__FILE__, 'init_contact.tpl');
    }

    /**
     * @return bool
     */
    public function _registerHook()
    {
        foreach (Ets_cfudefines::$_hooks as $hook) {
            $this->registerHook($hook);
        }
        return true;
    }

    /**
     * @return bool
     */
    public function installDb()
    {
        $res = Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ets_cfu_contact` (
          `id_contact` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `email_to` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `bcc` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `email_from` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `exclude_lines` int(11) NOT NULL,
          `use_html_content` int(11) NOT NULL,
          `use_email2` int(11) NOT NULL,
          `email_to2` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `bcc2` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `email_from2` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `additional_headers` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `additional_headers2` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `exclude_lines2` int(11) NOT NULL,
          `use_html_content2` int(11) NOT NULL,
          `id_employee` int(1) NOT NULL,
          `save_message` int(1) NOT NULL,
          `save_attachments` INT(1) NOT NULL,
          `star_message` INT(1) NOT NULL,
          `open_form_by_button` INT (1),
          `file_attachments2` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `file_attachments` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `render_form` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `hook` VARCHAR(222),
          `group_access` VARCHAR(255),
          `only_customer` INT(1) NOT NULL,
          `thank_you_active` INT(1) NOT NULL,
          `thank_you_page` VARCHAR(255) CHARACTER SET utf8 NOT NULL,
          `active` INT(1),
          `enable_form_page` INT(1),
          `position` INT(11),
          `date_add` date NOT NULL,
          `date_upd` date NOT NULL,
          PRIMARY KEY (`id_contact`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');
        $res &= Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ets_cfu_contact_lang` (
          `id_contact` int(11) NOT NULL,
          `id_lang` int(11) NOT NULL,
          `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `title_alias` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `meta_title` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `meta_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `button_label` VARCHAR(222),
          `short_code` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `template_mail` text NOT NULL,
          `subject` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_body` text CHARACTER SET utf8 COLLATE utf8_general_ci	,
          `subject2` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_body2` text CHARACTER SET utf8 COLLATE utf8_general_ci	,
          `message_mail_sent_ok` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_mail_sent_ng` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_validation_error` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_spam` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_accept_terms` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_invalid_required` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_invalid_too_long` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_invalid_too_short` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_date_too_early` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL, 
          `message_invalid_date` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_date_too_late` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_upload_failed` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_upload_file_type_invalid` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_upload_file_too_large` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_quiz_answer_not_correct` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_invalid_email` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_invalid_url` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_invalid_tel` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `additional_settings` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_upload_failed_php_error` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_invalid_number` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_number_too_small` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_number_too_large` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_captcha_not_match` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_ip_black_list` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `message_email_black_list` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `thank_you_page_title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          `thank_you_message` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `thank_you_alias` VARCHAR(100) CHARACTER SET utf8 NOT NULL,
          `thank_you_url` text CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL
        )ENGINE=InnoDB DEFAULT CHARSET=latin1');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ets_cfu_contact_shop` (
          `id_contact` int(11) NOT NULL,
          `id_shop` int(11) NOT NULL
        )ENGINE=InnoDB DEFAULT CHARSET=latin1');
        $res &= Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ets_cfu_contact_message`(
          `id_contact_message` int(11) unsigned NOT NULL AUTO_INCREMENT ,
          `id_contact` int(11) NOT NULL,
          `id_customer` INT (11) NOT NULL,
          `replied` INT(1) NOT NULL,
          `readed` INT(1) NOT NULL,
          `special` INT(1) NOT NULL,
          `subject` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `sender` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `body` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `recipient` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `attachments` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `reply_to` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
          `date_add` DATETIME NOT NULL,
          `date_upd` DATETIME NOT NULL,
          PRIMARY KEY (`id_contact_message`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ets_cfu_contact_message_shop` (
          `id_contact_message` int(11) NOT NULL,
          `id_shop` int(11) NOT NULL
        )ENGINE=InnoDB DEFAULT CHARSET=latin1');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ets_cfu_message_reply`(
            `id_ets_cfu_message_reply` INT(11) unsigned NOT NULL AUTO_INCREMENT ,
            `id_contact_message` INT(11) NOT NULL,
            `id_employee` INT(11) NOT NULL,
            `content` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
            `reply_to` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
            `subject` text CHARACTER SET utf8 COLLATE utf8_general_ci	 NOT NULL,
            `date_add` date NOT NULL,
            `date_upd` date NOT NULL,
            PRIMARY KEY (`id_ets_cfu_message_reply`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ets_cfu_log`(
            `ip` varchar(50) DEFAULT NULL,
            `id_contact` INT(11) NOT NULL,
            `browser` varchar(70) DEFAULT NULL,
            `id_customer` INT (11) DEFAULT NULL,
            `datetime_added` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $res &= $this->_installDbConfig();
        return $res;
    }

    /**
     * @return bool
     */
    public function _installDbConfig()
    {
        $fields_config = Ets_cfudefines::getInstance($this)->getFields('config');
        $inputs = $fields_config['form']['input'];
        $languages = Language::getLanguages(false);
        if ($inputs) {
            foreach ($inputs as $input) {
                if (isset($input['default'])) {
                    $key = $input['name'];
                    if (isset($input['lang']) && $input['lang']) {
                        $vals = array();
                        foreach ($languages as $language) {
                            $vals[$language['id_lang']] = $input['default'];
                        }
                        Configuration::updateValue($key, $vals, true);
                    } else {
                        Configuration::updateValue($key, $input['default']);
                    }
                }
            }
        }
        $fields_config = Ets_cfudefines::getInstance($this)->getFields('email');
        $inputs = $fields_config['form']['input'];
        $languages = Language::getLanguages(false);
        if ($inputs) {
            foreach ($inputs as $input) {
                if (isset($input['default'])) {
                    $key = $input['name'];
                    if (isset($input['lang']) && $input['lang']) {
                        $vals = array();
                        foreach ($languages as $language) {
                            $vals[$language['id_lang']] = $input['default'];
                        }
                        Configuration::updateValue($key, $vals, true);
                    } else {
                        Configuration::updateValue($key, $input['default']);
                    }
                }
            }
        }
        return true;
    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function _installTabs()
    {
        $languages = Language::getLanguages(false);
        $tab = new Tab();
        $tab->class_name = 'AdminContactFormUltimate';
        $tab->module = $this->name;
        $tab->id_parent = 0;
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Contact');
        }
        $tab->save();
        if ($tab->id) {
            $this->addTabs(Ets_cfudefines::getInstance($this)->getFields('tabs'), $languages, $tab->id);
        }
        return true;
    }

    public function addTabs($tabs, $languages, $parent_id)
    {
        if (!$tabs) {
            return;
        }
        foreach ($tabs as $t) {
            $tab = new Tab();
            $tab->class_name = $t['class_name'];
            $tab->module = $this->name;
            $tab->id_parent = $parent_id;
            $tab->icon = $t['icon'];
            foreach ($languages as $lang) {
                $tab->name[$lang['id_lang']] = $t['tab_name'];
            }
            $tab->save();
            if ($tab->id && isset($t['children']) && $t['children']) {
                $this->addTabs($t['children'], $languages, $tab->id);
            }
        }
    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function _uninstallTabs()
    {
        if (($tabId = (int)Tab::getIdFromClassName('AdminContactFormUltimate'))) {
            $tab = new Tab($tabId);
            if ($tab) {
                $tab->delete();
            }
            $this->deleteTabs(Ets_cfudefines::getInstance($this)->getFields('tabs'));
        }
        return true;
    }

    public function deleteTabs($tabs)
    {
        if (!$tabs) {
            return;
        }
        foreach ($tabs as $t) {
            if (($tabId = (int)Tab::getIdFromClassName($t['class_name']))) {
                $tab = new Tab($tabId);
                if ($tab) {
                    $tab->delete();
                }
                if (isset($t['children']) && $t['children']) {
                    $this->deleteTabs($t['children']);
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function createTemplateMail()
    {
        $languages = Language::getLanguages(false);
        foreach ($languages as $language) {
            if (!file_exists(dirname(__FILE__) . '/mails/' . $language['iso_code'])) {
                mkdir(dirname(__FILE__) . '/mails/' . $language['iso_code'], 0755, true);
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_form_ultimate.html', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_form_ultimate.html');
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_form_ultimate.txt', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_form_ultimate.txt');
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_reply_form_ultimate.html', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_reply_form_ultimate.html');
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_reply_form_ultimate.txt', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_reply_form_ultimate.txt');
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_form_ultimate_plain.txt', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_form_ultimate_plain.txt');
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_form_ultimate_plain.html', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_form_ultimate_plain.html');
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_reply_form_ultimate_plain.txt', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_reply_form_ultimate_plain.txt');
                Tools::copy(dirname(__FILE__) . '/mails/en/contact_reply_form_ultimate_plain.html', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/contact_reply_form_ultimate_plain.html');
                Tools::copy(dirname(__FILE__) . '/mails/en/index.php', dirname(__FILE__) . '/mails/' . $language['iso_code'] . '/index.php');
            }
        }
        return true;
    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallDb() && $this->_uninstallTabs() && $this->clearLogInstall();
    }

    /**
     * @return bool
     */
    public function uninstallDb()
    {
        $res = Db::getInstance()->execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ets_cfu_contact');
        $res &= Db::getInstance()->execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ets_cfu_contact_lang');
        $res &= Db::getInstance()->execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ets_cfu_contact_shop');
        $res &= Db::getInstance()->execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ets_cfu_contact_message');
        $res &= Db::getInstance()->execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ets_cfu_contact_message_shop');
        $res &= Db::getInstance()->execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ets_cfu_message_reply');
        $res &= Db::getInstance()->execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'ets_cfu_log');
        $res &= $this->_unInstallDbConfig();
        return $res;
    }

    /**
     * @return bool
     */
    public function _unInstallDbConfig()
    {
        if (($fields_config = Ets_cfudefines::getInstance($this)->getFields('config')) && ($inputs = $fields_config['form']['input'])) {
            foreach ($inputs as $input) {
                $key = $input['name'];
                Configuration::deleteByName($key);
            }
        }
        if (($fields_config = Ets_cfudefines::getInstance($this)->getFields('email')) && ($inputs = $fields_config['form']['input'])) {
            foreach ($inputs as $input) {
                $key = $input['name'];
                Configuration::deleteByName($key);
            }
        }
        if (($fields_config = Ets_cfudefines::getInstance($this)->getFields('ip_black_list')) && ($inputs = $fields_config['form']['input'])) {
            foreach ($inputs as $input) {
                $key = $input['name'];
                Configuration::deleteByName($key);
            }
        }
        foreach (glob(dirname(__file__) . '/views/img/etscfu_upload/*.*') as $filename) {
            if ($filename != dirname(__file__) . '/views/img/etscfu_upload/index.php')
                @unlink($filename);
        }
        return true;
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayHeader()
    {
        if(!(int)Configuration::get('ETS_CFU_ENABLE_HOOK_SHORTCODE') && ($controller = Dispatcher::getInstance()->getController($this->context->shop->id)) && $controller !='contact'){
            $this->rmHookShortcode = true;
        }
        if($this->rmHookShortcode){
            return '';
        }
        $this->context->controller->addJS($this->_path . 'views/js/date.js');
        $this->context->controller->addCSS($this->_path . 'views/css/date.css', 'all');
        $this->context->controller->addJS($this->_path . 'views/js/scripts.js');
        $this->context->controller->addCSS($this->_path . 'views/css/style.css', 'all');
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            $this->context->controller->addCSS($this->_path . 'views/css/style15.css', 'all');
        if (version_compare(_PS_VERSION_, '1.7', '<') && version_compare(_PS_VERSION_, '1.5', '>'))
            $this->context->controller->addCSS($this->_path . 'views/css/style16.css', 'all');
        if (Configuration::get('ETS_CFU_ENABLE_TMCE')) {
            $this->context->controller->addCSS($this->_path . 'views/css/skin.min.css', 'all');
            $this->context->controller->addJS($this->_path . 'views/js/tinymce/tinymce.min.js');
        }
        $assign = array(
            'url_basic' => $this->getBaseLink(),
            'link_contact_ets' => $this->context->link->getModuleLink('ets_cfultimate', 'contact'),
        );

        if (Configuration::get('ETS_CFU_ENABLE_RECAPTCHA')) {
            $assign['rc_enabled'] = true;
            $assign['rc_v3'] = Configuration::get('ETS_CFU_RECAPTCHA_TYPE') != 'v2';
            $assign['rc_key'] = Configuration::get('ETS_CFU_RECAPTCHA_TYPE') == 'v2' ? Configuration::get('ETS_CFU_SITE_KEY') : Configuration::get('ETS_CFU_SITE_KEY_V3');
            $this->context->controller->addJS($this->_path . 'views/js/recaptcha.js');
        }
        $assign['iso_code'] = $this->context->language->iso_code;

        if (! $this->context->customer->logged || !getOrderReferrence() ){
            $assign['hidden_referrence'] = true;
        }
        $this->smarty->assign($assign);

        return $this->display(__FILE__, 'header.tpl') . $this->getContactFormByHook('header');
    }

    /**
     * @param $hook
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function getContactFormByHook($hook)
    {

        $contacts = Db::getInstance()->executeS('
            SELECT c.id_contact FROM ' . _DB_PREFIX_ . 'ets_cfu_contact c
            INNER JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs ON (c.id_contact= cs.id_contact)
            LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_lang cl on (c.id_contact= cl.id_contact AND cl.id_lang="' . (int)$this->context->language->id . '")
            WHERE c.active=1 AND c.hook like "%' . pSQL($hook) . '%" AND cs.id_shop="' . (int)$this->context->shop->id . '";
        ');

        if ($contacts) {
            $form_html = '';
            foreach ($contacts as $contact) {
                $form_html .= $this->hookDisplayContactFormUltimate($contact);
            }
            return $form_html;
        }
        return '';
    }

    /**
     * @param $params
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayContactFormUltimate($params)
    {
        if ($this->rmHookShortcode){
            return '';
        }
        $id = isset($params['id']) ? $params['id'] : $params['id_contact'];
        $contact = new ETS_CFU_Contact($id);

        $customer_status = $this->context->customer->isLogged(true);
        $customer_group = $this->context->customer->getGroups();
        $group_access = $contact->id ? explode(',',$contact->group_access) : array();

        $status_access = false;
        if ( $total =  count($customer_group) ){
            for ($i=0; $i< $total; $i++){
                if ( in_array($customer_group[$i],$group_access)){
                    $status_access = true;
                    break;
                }
            }
        }

        if ($contact->id && $contact->active && $status_access && (! $contact->only_customer || $customer_status) ) {
            $contact_form = $this->ets_cfu_contact_form($id);
            return $this->form_html($contact_form, true);
        }

        return '';
    }

    /**
     * @param $id_contact
     * @return array|false|mysqli_result|PDOStatement|resource|null
     * @throws PrestaShopDatabaseException
     */
    public function existContact($id_contact)
    {
        return Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'ets_cfu_contact c, ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs WHERE c.id_contact="' . (int)$id_contact . '" AND c.id_contact=cs.id_contact AND cs.id_shop="' . (int)$this->context->shop->id . '"');
    }

    /**
     * @param $id
     * @return bool|ETS_CFU_Contact_Form
     */
    public function ets_cfu_contact_form($id)
    {
        return ETS_CFU_Contact_Form::get_instance($id);
    }

    /**
     * @param $contact_form
     * @param bool $displayHook
     * @return string
     */
    public function form_html($contact_form, $displayHook = false)
    {
        $contact_form->unit_tag = ETS_CFU_Contact_Form::get_unit_tag($contact_form->id);
        $contact_form->form_unit_tag = str_replace('wpcfu-f'.(int)$contact_form->id.'-o','',$contact_form->unit_tag);
        $this->smarty->assign(array(
            'contact_form' => $contact_form,
            'link' => $this->context->link,
            'open_form_by_button' => $contact_form->open_form_by_button && $displayHook,
            'form_elements' => $contact_form->form_elements(),
            'displayHook' => $displayHook,
        ));

        return $this->display(__FILE__, 'contact-form.tpl');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderFormConfig()
    {
        $config_fields = Ets_cfudefines::getInstance($this)->getFields('config');
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'etsCfuBtnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminContactFormUltimateIntegration', false);
        $helper->token = Tools::getAdminTokenLite('AdminContactFormUltimateIntegration');
        $helper->tpl_vars = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'fields_value' => $this->getConfigFieldsValues($config_fields),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path . 'views/img/',
            'page' => 'integration',
            'name_controller' => 'integration',
            'link_basic' => $this->getBaseLink(),
            'ps15' => version_compare(_PS_VERSION_, '1.6', '<') ? true : false,
        );
        $helper->module = $this;
        return $helper->generateForm(array($config_fields));
    }

    /**
     * @param $config_fields
     * @return array
     */
    public function getConfigFieldsValues($config_fields)
    {
        if (!$config_fields) {
            $config_fields = Ets_cfudefines::getInstance($this)->getFields('config');
        }
        $inputs = $config_fields['form']['input'];
        $languages = Language::getLanguages(false);
        $fields = array();
        if ($inputs) {
            foreach ($inputs as $input) {
                $key = $input['name'];
                if (isset($input['lang']) && $input['lang']) {
                    foreach ($languages as $language) {
                        $fields[$key][$language['id_lang']] = Tools::getValue($key . '_' . $language['id_lang'], Configuration::get($key, $language['id_lang']));
                    }
                } else
                    $fields[$key] = Tools::getValue($key, Configuration::get($key));
            }
        }
        return $fields;
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderFormEmail()
    {
        $email_fields = Ets_cfudefines::getInstance($this)->getFields('email');
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'etsCfuBtnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminContactFormUltimateEmail', false);
        $helper->token = Tools::getAdminTokenLite('AdminContactFormUltimateEmail');
        $helper->tpl_vars = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'fields_value' => $this->getEmailFieldsValues($email_fields),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path . 'views/img/',
            'page' => 'email',
            'name_controller' => 'email',
            'link_basic' => $this->getBaseLink(),
            'ps15' => version_compare(_PS_VERSION_, '1.6', '<') ? true : false,
        );
        $helper->module = $this;
        return $helper->generateForm(array($email_fields));
    }

    /**
     * @param $email_fields
     * @return array
     */
    public function getEmailFieldsValues($email_fields)
    {
        if (!$email_fields) {
            $email_fields = Ets_cfudefines::getInstance($this)->getFields('email');
        }
        $inputs = $email_fields['form']['input'];
        $languages = Language::getLanguages(false);
        $fields = array();
        if ($inputs) {
            foreach ($inputs as $input) {
                $key = $input['name'];
                if (isset($input['lang']) && $input['lang']) {
                    foreach ($languages as $language) {
                        $fields[$key][$language['id_lang']] = Tools::getValue($key . '_' . $language['id_lang'], Configuration::get($key, $language['id_lang']));
                    }
                } else
                    $fields[$key] = Tools::getValue($key, Configuration::get($key));
            }
        }
        return $fields;
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderFormIpBlackList()
    {
        $ip_black_list = Ets_cfudefines::getInstance($this)->getFields('ip_black_list');
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'etsCfuBtnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminContactFormUltimateIpBlacklist', false);
        $helper->token = Tools::getValue('token', Tools::getAdminTokenLite('AdminContactFormUltimateIpBlacklist'));
        $helper->tpl_vars = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'fields_value' => $this->getIpBlackListFieldsValues($ip_black_list),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path . 'views/img/',
            'page' => 'ip_black_list',
            'name_controller' => 'ip_black_list',
            'link_basic' => $this->getBaseLink(),
            'ps15' => version_compare(_PS_VERSION_, '1.6', '<') ? true : false,
        );
        $helper->module = $this;
        return $helper->generateForm(array($ip_black_list));
    }

    /**
     * @param $ip_black_list
     * @return array
     */
    public function getIpBlackListFieldsValues($ip_black_list)
    {
        if (!$ip_black_list) {
            $ip_black_list = Ets_cfudefines::getInstance($this)->getFields('ip_black_list');
        }
        $inputs = $ip_black_list['form']['input'];
        $languages = Language::getLanguages(false);
        $fields = array();
        if ($inputs) {
            foreach ($inputs as $input) {
                $key = $input['name'];
                if (isset($input['lang']) && $input['lang']) {
                    foreach ($languages as $language) {
                        $fields[$key][$language['id_lang']] = Tools::getValue($key . '_' . $language['id_lang'], Configuration::get($key, $language['id_lang']));
                    }
                } else
                    $fields[$key] = Tools::getValue($key, Configuration::get($key));
            }
        }
        return $fields;
    }

    /**
     * @return void
     */
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addCSS(array(
            $this->_path . '/views/css/contact_form7_admin_all.css',
        ), 'all');
        $controller = Tools::getValue('controller');
        $is_loaded = (strpos($controller, 'AdminContactFormUltimate') !== FALSE);
        if ((Tools::getValue('configure') == $this->name && $controller == 'AdminModules') || $is_loaded) {
            $this->context->controller->addCSS($this->_path . '/views/css/contact_form7_admin.css', 'all');
            $this->context->controller->addCSS($this->_path . '/views/css/other.css', 'all');
            if (version_compare(_PS_VERSION_, '1.7', '<') && version_compare(_PS_VERSION_, '1.5', '>')) {
                $this->context->controller->addCSS($this->_path . '/views/css/contact_form7_admin16.css', 'all');
            }
            if (version_compare(_PS_VERSION_, '1.6', '<')) {
                $this->context->controller->addCSS($this->_path . '/views/css/contact_form7_admin15.css', 'all');
            }
            if (method_exists($this->context->controller, 'addJquery')) {
                $this->context->controller->addJquery();
                $this->context->controller->addJqueryUI('ui.sortable');
            }
        }
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookContactFormUltimateTopBlock()
    {
        $intro = true;
        $localIps = array(
            '127.0.0.1',
            '::1'
        );
		$baseURL = Tools::strtolower(self::getBaseModLink());
		if(!Tools::isSubmit('intro') && (in_array(Tools::getRemoteAddr(), $localIps) || preg_match('/^.*(localhost|demo|test|dev|:\d+).*$/i', $baseURL)))
		    $intro = false;
        $languages = Language::getLanguages(false);
        $this->smarty->assign(
            array(
                'controller' => Tools::getValue('controller'),
                'link' => $this->context->link,
                'ets_cfu_js_dir_path' => $this->_path . 'views/js/',
                'ets_cfu_default_lang' => Configuration::get('PS_LANG_DEFAULT'),
                'ets_cfu_is_updating' => Tools::getValue('id_contact') ? 1 : 0,
                'count_messages' => $this->getCountUnreadMessage(),
                'languages' => $languages,
                'other_modules_link' => isset($this->refs) ? $this->refs.$this->context->language->iso_code : $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name.'&othermodules=1',
                'intro' => $intro,
                'refsLink' => isset($this->refs) ? $this->refs.$this->context->language->iso_code : false,
            )
        );
        return $this->display(__FILE__, 'block-top.tpl');
    }
    public static function getBaseModLink()
    {
        $context = Context::getContext();
        return (Configuration::get('PS_SSL_ENABLED_EVERYWHERE')?'https://':'http://').$context->shop->domain.$context->shop->getBaseURI();
    }

    /**
     * @param $params
     * @return array
     */
    public function hookModuleRoutes($params)
    {
        $contactAlias = (Configuration::get('ETS_CFU_CONTACT_ALIAS', $this->context->language->id) ? Configuration::get('ETS_CFU_CONTACT_ALIAS', $this->context->language->id) : 'contact-form');
        if (!$contactAlias)
            return array();
        $removeId = Configuration::get('ETS_CFU_REMOVE_ID');
        $keywords = array(
            'url_alias' => array('regexp' => '[_a-zA-Z0-9-]+', 'param' => 'url_alias'),
        );

        if (!$removeId) {
            $keywords['id_contact'] = array('regexp' => '[0-9]+', 'param' => 'id_contact');
        }

        $routes = array(
            'ets_cfultimate_contact' => array(
                'controller' => 'contact',
                'rule' => $contactAlias,
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ets_cfultimate',
                ),
            ),
            'ets_cfultimate_contact_single' => array(
                'controller' => 'contact',
                'rule' => $contactAlias . '/' . (!$removeId ? '{id_contact}-' : '') . '{url_alias}' . (Configuration::get('ETS_CFU_URL_SUFFIX') ? '.html' : ''),
                'keywords' => $keywords,
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ets_cfultimate',
                ),
            ),
            'ets_cfultimate_contact_thank' => array(
                'controller' => 'thank',
                'rule' => $contactAlias . '/thank/' . (!$removeId ? '{id_contact}-' : '') . '{url_alias}' . (Configuration::get('ETS_CFU_URL_SUFFIX') ? '.html' : ''),
                'keywords' => $keywords,
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ets_cfultimate',
                ),
            ),
        );

        return $routes;
    }
    public function displayRecommendedModules()
    {
        $cacheDir = dirname(__file__) . '/../../cache/'.$this->name.'/';
        $cacheFile = $cacheDir.'module-list.xml';
        $cacheLifeTime = 24;
        $cacheTime = (int)Configuration::getGlobalValue('ETS_MOD_CACHE_'.$this->name);
        $profileLinks = array(
            'en' => 'https://addons.prestashop.com/en/207_ets-soft',
            'fr' => 'https://addons.prestashop.com/fr/207_ets-soft',
            'it' => 'https://addons.prestashop.com/it/207_ets-soft',
            'es' => 'https://addons.prestashop.com/es/207_ets-soft',
        );
        if(!is_dir($cacheDir))
        {
            @mkdir($cacheDir, 0755,true);
            if ( @file_exists(dirname(__file__).'/index.php')){
                @copy(dirname(__file__).'/index.php', $cacheDir.'index.php');
            }
        }
        if(!file_exists($cacheFile) || !$cacheTime || time()-$cacheTime > $cacheLifeTime * 60 * 60)
        {
            if(file_exists($cacheFile))
                @unlink($cacheFile);
            if($xml = self::file_get_contents($this->shortlink.'ml.xml'))
            {
                $xmlData = @simplexml_load_string($xml);
                if($xmlData && (!isset($xmlData->enable_cache) || (int)$xmlData->enable_cache))
                {
                    @file_put_contents($cacheFile,$xml);
                    Configuration::updateGlobalValue('ETS_MOD_CACHE_'.$this->name,time());
                }
            }
        }
        else
            $xml = Tools::file_get_contents($cacheFile);
        $modules = array();
        $categories = array();
        $categories[] = array('id'=>0,'title' => $this->l('All categories'));
        $enabled = true;
        $iso = Tools::strtolower($this->context->language->iso_code);
        $moduleName = $this->displayName;
        $contactUrl = '';
        if($xml && ($xmlData = @simplexml_load_string($xml)))
        {
            if(isset($xmlData->modules->item) && $xmlData->modules->item)
            {
                foreach($xmlData->modules->item as $arg)
                {
                    if($arg)
                    {
                        if(isset($arg->module_id) && (string)$arg->module_id==$this->name && isset($arg->{'title'.($iso=='en' ? '' : '_'.$iso)}) && (string)$arg->{'title'.($iso=='en' ? '' : '_'.$iso)})
                            $moduleName = (string)$arg->{'title'.($iso=='en' ? '' : '_'.$iso)};
                        if(isset($arg->module_id) && (string)$arg->module_id==$this->name && isset($arg->contact_url) && (string)$arg->contact_url)
                            $contactUrl = $iso!='en' ? str_replace('/en/','/'.$iso.'/',(string)$arg->contact_url) : (string)$arg->contact_url;
                        $temp = array();
                        foreach($arg as $key=>$val)
                        {
                            if($key=='price' || $key=='download')
                                $temp[$key] = (int)$val;
                            elseif($key=='rating')
                            {
                                $rating = (float)$val;
                                if($rating > 0)
                                {
                                    $ratingInt = (int)$rating;
                                    $ratingDec = $rating-$ratingInt;
                                    $startClass = $ratingDec >= 0.5 ? ceil($rating) : ($ratingDec > 0 ? $ratingInt.'5' : $ratingInt);
                                    $temp['ratingClass'] = 'mod-start-'.$startClass;
                                }
                                else
                                    $temp['ratingClass'] = '';
                            }
                            elseif($key=='rating_count')
                                $temp[$key] = (int)$val;
                            else
                                $temp[$key] = (string)strip_tags($val);
                        }
                        if($iso)
                        {
                            if(isset($temp['link_'.$iso]) && isset($temp['link_'.$iso]))
                                $temp['link'] = $temp['link_'.$iso];
                            if(isset($temp['title_'.$iso]) && isset($temp['title_'.$iso]))
                                $temp['title'] = $temp['title_'.$iso];
                            if(isset($temp['desc_'.$iso]) && isset($temp['desc_'.$iso]))
                                $temp['desc'] = $temp['desc_'.$iso];
                        }
                        $modules[] = $temp;
                    }
                }
            }
            if(isset($xmlData->categories->item) && $xmlData->categories->item)
            {
                foreach($xmlData->categories->item as $arg)
                {
                    if($arg)
                    {
                        $temp = array();
                        foreach($arg as $key=>$val)
                        {
                            $temp[$key] = (string)strip_tags($val);
                        }
                        if(isset($temp['title_'.$iso]) && $temp['title_'.$iso])
                                $temp['title'] = $temp['title_'.$iso];
                        $categories[] = $temp;
                    }
                }
            }
        }
        if(isset($xmlData->{'intro_'.$iso}))
            $intro = $xmlData->{'intro_'.$iso};
        else
            $intro = isset($xmlData->intro_en) ? $xmlData->intro_en : false;
        $this->smarty->assign(array(
            'modules' => $modules,
            'enabled' => $enabled,
            'module_name' => $moduleName,
            'categories' => $categories,
            'img_dir' => $this->_path . 'views/img/',
            'intro' => $intro,
            'shortlink' => $this->shortlink,
            'ets_profile_url' => isset($profileLinks[$iso]) ? $profileLinks[$iso] : $profileLinks['en'],
            'trans' => array(
                'txt_must_have' => $this->l('Must-Have'),
                'txt_downloads' => $this->l('Downloads!'),
                'txt_view_all' => $this->l('View all our modules'),
                'txt_fav' => $this->l('Prestashop\'s favourite'),
                'txt_elected' => $this->l('Elected by merchants'),
                'txt_superhero' => $this->l('Superhero Seller'),
                'txt_partner' => $this->l('Module Partner Creator'),
                'txt_contact' => $this->l('Contact us'),
                'txt_close' => $this->l('Close'),
            ),
            'contactUrl' => $contactUrl,
         ));
         echo $this->display(__FILE__, 'module-list.tpl');
         die;
    }
    public static function file_get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 60)
    {
        if ($stream_context == null && preg_match('/^https?:\/\//', $url)) {
            $stream_context = stream_context_create(array(
                "http" => array(
                    "timeout" => $curl_timeout,
                    "max_redirects" => 101,
                    "header" => 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
                ),
                "ssl"=>array(
                    "allow_self_signed"=>true,
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ));
        }
        if (function_exists('curl_init')) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => html_entity_decode($url),
                CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => $curl_timeout,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_FOLLOWLOCATION => true,
            ));
            $content = curl_exec($curl);
            curl_close($curl);
            return $content;
        } elseif (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url)) {
            return Tools::file_get_contents($url, $use_include_path, $stream_context);
        } else {
            return false;
        }
    }

    /**
     * @param $params
     */
    public function hookActionOutputHTMLBefore($params)
    {
        if (isset($params['html']) && $params['html']) {
            $params['html'] = $this->doShortCode($params['html']);
        }
    }

    /**
     * @param $str
     * @return string|string[]|null
     */
    public function doShortCode($str)
    {
        return preg_replace_callback('~\[contact\-form\-7 id="(\d+)"\]~', array($this, 'replace'), $str);//[social-locker ]
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayHome()
    {
        return $this->getContactFormByHook('home');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayNav2()
    {
        return $this->getContactFormByHook('nav_top');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayProductAdditionalInfo()
    {
        return $this->getContactFormByHook('product_info');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayFooterProduct()
    {
        return $this->getContactFormByHook('product_footer');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayNav()
    {
        return $this->getContactFormByHook('nav_top');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayTop()
    {
        return $this->getContactFormByHook('displayTop');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayLeftColumn()
    {
        return $this->getContactFormByHook('left_column');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayFooter()
    {
        return $this->getContactFormByHook('footer_page');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayRightColumn()
    {
        return $this->getContactFormByHook('right_column');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayAfterProductThumbs()
    {
        return $this->getContactFormByHook('product_thumbs');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayRightColumnProduct()
    {
        return $this->getContactFormByHook('product_right');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayLeftColumnProduct()
    {
        return $this->getContactFormByHook('product_left');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function displayShoppingCartFooter()
    {
        return $this->getContactFormByHook('checkout_page');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayCustomerAccountForm()
    {
        return $this->getContactFormByHook('register_page');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayCustomerLoginFormAfter()
    {
        return $this->getContactFormByHook('login_page');
    }

    /**
     * @param $str
     * @return string|string[]|null
     */
    public function replaceDefaultContactForm($str)
    {
        return preg_replace('~<section class="contact-form">.*</section>~', 'abc', $str);
    }

    /**
     * @param $matches
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function replace($matches)
    {
        if (is_array($matches) && count($matches) == 2) {
            if ($this->rmHookShortcode){
                return $matches[0];
            }
            $form = $this->hookDisplayContactFormUltimate(array(
                'id' => (int)$matches[1]
            ));
            if ($form)
                return $form;
            else
                return $this->display(__FILE__, 'no-form-contact.tpl');
        }
    }

    /**
     * @return void
     */
    public function setMetas($id_contact,$thank_page = false)
    {
        if (!$id_contact || trim(Tools::getValue('module')) != $this->name) {
            return;
        }
        $meta = array();
        $contact = new ETS_CFU_Contact($id_contact, $this->context->language->id);;
        $meta['meta_title'] = $contact->meta_title ? $contact->meta_title : ($thank_page ? $contact->thank_you_page_title : $contact->title);
        $meta['meta_description'] = $contact->meta_description;
        $meta['meta_keywords'] = $contact->meta_keyword;
        if (version_compare(_PS_VERSION_, '1.7.0', '>=')) {
            $body_classes = array(
                'lang-' . $this->context->language->iso_code => true,
                'lang-rtl' => (bool)$this->context->language->is_rtl,
                'country-' . $this->context->country->iso_code => true,
            );
            $page = array(
                'title' => $meta['meta_title'],
                'canonical' => '',
                'meta' => array(
                    'title' => $meta['meta_title'],
                    'description' => $meta['meta_description'],
                    'keywords' => $meta['meta_keywords'],
                    'robots' => 'index',
                ),
                'page_name' => 'ets_cft_page',
                'body_classes' => $body_classes,
                'admin_notifications' => array(),
            );
            $this->context->smarty->assign(array('page' => $page));
        } else {
            $this->context->smarty->assign($meta);
        }
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_text_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type, 'wpcfu-text');
        if (in_array($tag->basetype, array('email', 'url', 'tel', 'password'))) {
            $class .= ' wpcfu-validates-as-' . $tag->basetype;
        }
        if ($validation_error) {
            $class .= ' wpcfu-not-valid';
        }
        $class .= ' form-control';
        $atts = array();

        $atts['size'] = $tag->get_size_option('40');
        $atts['maxlength'] = $tag->get_maxlength_option();
        $atts['minlength'] = $tag->get_minlength_option();
        if ($atts['maxlength'] && $atts['minlength'] && $atts['maxlength'] < $atts['minlength']) {
            unset($atts['maxlength'], $atts['minlength']);
        }
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['autocomplete'] = $tag->get_option('autocomplete', '[-0-9a-zA-Z]+', true);
        if ($tag->has_option('readonly')) {
            $atts['readonly'] = 'readonly';
        }
        if ($tag->is_required()) {
            $atts['aria-required'] = 'true';
        }
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $value = (string)reset($tag->values);
        if ($tag->has_option('placeholder') || $tag->has_option('watermark')) {
            $atts['placeholder'] = $value;
            $value = '';
        }

        if ($tag->basetype != 'password') {
            if ($tag->has_option('default')) {
                $option = $tag->get_first_match_option('/^default:user_(.+)+$/');
                if (is_array($option) && isset($option[0]))
                    $value = $option[0];
            }

            $value = $tag->get_default_option($value);
            $value = ets_cfu_get_hangover($tag->name, $value);
            if ($tag->has_option('use_current_url'))
                $value = $this->getFileCacheByUrl();

            if ($tag->has_option('read_only'))
                $atts['readonly'] = 'true';
        }

        $atts['value'] = $value;
        if (ets_cfu_support_html5()) {
            $atts['type'] = $tag->basetype;
        } else {
            $atts['type'] = 'text';
        }
        $atts['name'] = $tag->name;

        $this->smarty->assign(array(
            'html_class' => ets_cfu_sanitize_html_class($tag->name),
            'atts' => $atts,
            'validation_error' => $validation_error,
            'show_hide_password' => ($tag->basetype == 'password')
        ));
        return $this->display(__FILE__, 'form_text.tpl');
    }

    /**
     * @return bool|string
     */
    public function getFileCacheByUrl()
    {
        $url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
            $url = 'https://' . $url;
        } else
            $url = 'http://' . $url;
        if (strpos($url, '#') !== FALSE) {
            $url = Tools::substr($url, 0, strpos($url, '#'));
        }
        return $url;
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_textarea_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        if ($validation_error) {
            $class .= ' wpcfu-not-valid';
        }
        $class .= ' form-control' . ($tag->has_option('rte') && Configuration::get('ETS_CFU_ENABLE_TMCE') ? ' autoload_rte_cfu' : '');
        $atts = array();
        $atts['cols'] = $tag->get_cols_option('40');
        $atts['rows'] = $tag->get_rows_option('10');
        $atts['maxlength'] = $tag->get_maxlength_option();
        $atts['minlength'] = $tag->get_minlength_option();
        if ($atts['maxlength'] && $atts['minlength'] && $atts['maxlength'] < $atts['minlength'] || $tag->has_option('rte')) {
            unset($atts['maxlength'], $atts['minlength']);
        }
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['autocomplete'] = $tag->get_option('autocomplete', '[-0-9a-zA-Z]+', true);
        if ($tag->has_option('readonly')) {
            $atts['readonly'] = 'readonly';
        }
        if ($tag->is_required()) {
            $atts['aria-required'] = 'true';
        }
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $value = empty($tag->content)
            ? (string)reset($tag->values)
            : $tag->content;
        if ($tag->has_option('placeholder') || $tag->has_option('watermark')) {
            $atts['placeholder'] = $value;
            $value = '';
        }
        $value = $tag->get_default_option($value);
        $value = ets_cfu_get_hangover($tag->name, $value);
        $atts['name'] = $tag->name;
        $this->smarty->assign(array(
            'html_class' => ets_cfu_sanitize_html_class($tag->name),
            'atts' => $atts,
            'value' => esc_cfu_textarea($value),
            'tiny_mce_enabled' => (int)Configuration::get('ETS_CFU_ENABLE_TMCE'),
            'validation_error' => $validation_error,
        ));
        return $this->display(__FILE__, 'form_textarea.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_captcha_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type, 'wpcfu-text');
        if (in_array($tag->basetype, array('email', 'url', 'tel'))) {
            $class .= ' wpcfu-validates-as-' . $tag->basetype;
        }
        if ($validation_error) {
            $class .= ' wpcfu-not-valid';
        }
        $class .= ' form-control';
        $atts = array();
        $atts['size'] = $tag->get_size_option('40');
        $atts['maxlength'] = $tag->get_maxlength_option();
        $atts['minlength'] = $tag->get_minlength_option();
        if ($atts['maxlength'] && $atts['minlength'] && $atts['maxlength'] < $atts['minlength']) {
            unset($atts['maxlength'], $atts['minlength']);
        }
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['autocomplete'] = $tag->get_option('autocomplete', '[-0-9a-zA-Z]+', true);
        if ($tag->has_option('readonly')) {
            $atts['readonly'] = 'readonly';
        }
        if ($tag->is_required()) {
            $atts['aria-required'] = 'true';
        }
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $atts['type'] = 'captcha';
        $atts['name'] = $tag->name;
        $rand = md5(rand());
        $theme = $tag->get_option('theme', '(basic|complex|colorful)', true);
        $this->smarty->assign(array(
            'link_captcha_image' => Context::getContext()->link->getModuleLink('ets_cfultimate', 'captcha', array('captcha_name' => $tag->name, 'rand' => $rand, 'theme' => $theme), true),
            'html_class' => ets_cfu_sanitize_html_class($tag->name),
            'atts' => $atts,
            'url_base' => $this->getBaseLink(),
            'rand' => $rand,
            'validation_error' => $validation_error
        ));
        return $this->display(__FILE__, 'form_captcha.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_quiz_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        if ($validation_error) {
            $class .= ' wpcfu-not-valid';
        }
        $class .= ' form-control';
        $atts = array();
        $atts['size'] = $tag->get_size_option('40');
        $atts['maxlength'] = $tag->get_maxlength_option();
        $atts['minlength'] = $tag->get_minlength_option();
        if ($atts['maxlength'] && $atts['minlength'] && $atts['maxlength'] < $atts['minlength']) {
            unset($atts['maxlength'], $atts['minlength']);
        }
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['autocomplete'] = 'off';
        $atts['aria-required'] = 'true';
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $pipes = $tag->pipes;
        if ($pipes instanceof ETS_CFU_Pipes && !$pipes->zero()) {
            $pipe = $pipes->random_pipe();
            $question = $pipe->before;
            $answer = $pipe->after;
        } else {
            // default quiz
            $question = '1+1=?';
            $answer = '2';
        }
        $answer = ets_cfu_canonicalize($answer);
        $atts['type'] = 'text';
        $atts['name'] = $tag->name;
        $this->smarty->assign(
            array(
                'html_class' => ets_cfu_sanitize_html_class($tag->name),
                'question' => $question,
                'atts' => $atts,
                'tag_name' => $tag->name,
                'answer' => ets_cfu_hash($answer, 'ets_cfu_quiz'),
                'validation_error' => $validation_error,
            )
        );
        return $this->display(__FILE__, 'form_quiz.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_number_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        $class .= ' wpcfu-validates-as-number';
        if ($validation_error) {
            $class .= ' wpcfu-not-valid';
        }
        $class .= ' form-control';
        $atts = array();
        $atts['maxlength'] = $tag->get_maxlength_option();
        $atts['minlength'] = $tag->get_minlength_option();
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['min'] = $tag->get_option('min', 'signed_int', true);
        $atts['max'] = $tag->get_option('max', 'signed_int', true);
        $atts['step'] = $tag->get_option('step', 'int', true);
        $atts['tagtype'] = $tag->get_option('tagtype');

        if ($tag->has_option('readonly')) {
            $atts['readonly'] = 'readonly';
        }
        if ($tag->is_required()) {
            $atts['aria-required'] = 'true';
        }
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $value = (string)reset($tag->values);
        if ($tag->has_option('placeholder') || $tag->has_option('watermark')) {
            $atts['placeholder'] = $value;
            $value = '';
        }
        $value = $tag->get_default_option($value);
        $value = ets_cfu_get_hangover($tag->name, $value);
        $atts['value'] = $value;

        if (ets_cfu_support_html5()) {
            $atts['type'] = $tag->basetype;
        } else {
            $atts['type'] = 'text';
        }
        $atts['name'] = $tag->name;
        //$atts['class'] .= isset($atts['type']) && $atts['type']=='number' ? ' ':'';
        $this->smarty->assign(
            array(
                'html_class' => ets_cfu_sanitize_html_class($tag->name),
                'atts' => $atts,
                'validation_error' => $validation_error,
            )
        );
        return $this->display(__FILE__, 'form_number.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_hidden_form_tag_handler($tag)
    {
        $atts = array();
        $class = ets_cfu_form_controls_class($tag->type);
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $value = (string)reset($tag->values);
        $value = $tag->get_default_option($value);
        $atts['value'] = $value;
        $atts['type'] = 'hidden';
        $atts['name'] = $tag->name;
        $this->smarty->assign(
            array(
                'atts' => $atts,
            )
        );
        return $this->display(__FILE__, 'form_hidden.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_file_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        if ($validation_error) {
            $class .= ' wpcfu-not-valid';
        }
        $class .= ' form-control filestyle';
        $atts = array();
        $atts['size'] = $tag->get_size_option('40');
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['accept'] = ets_cfu_acceptable_filetypes(
            $tag->get_option('filetypes'), 'attr');

        if ($tag->is_required()) {
            $atts['aria-required'] = 'true';
        }
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $atts['type'] = 'file';
        $atts['name'] = $tag->name;
        $atts['data-icon'] = 'false';
        $atts['data-placeholder'] = $this->l('No file chosen');
        $this->smarty->assign(
            array(
                'html_class' => ets_cfu_sanitize_html_class($tag->name),
                'atts' => $atts,
                'validation_error' => $validation_error,
                'type_file' => $tag->get_option('filetypes') ? (implode(' | ', explode('|',implode(',', $tag->get_option('filetypes'))) )) : '',
                'limit_zie' => $tag->get_option('limit') ? implode(',', $tag->get_option('limit')) : '',
            )
        );

        return $this->display(__FILE__, 'form_file.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_select_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        if ($validation_error) {
            $class .= ' wpcfu-not-valid';
        }
        $class .= ' form-control';
        $atts = array();
        $mod_referrence = $tag->has_option('mod_referrence');
        if ( $mod_referrence && (! $this->context->customer->logged || !getOrderReferrence())){
            return '';
        }
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        if ($tag->is_required()) {
            $atts['aria-required'] = 'true';
        }
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $multiple = $tag->has_option('multiple');
        $include_blank = $tag->has_option('include_blank');
        $first_as_label = $tag->has_option('first_as_label');
        if ($tag->has_option('size')) {
            $size = $tag->get_option('size', 'int', true);

            if ($size) {
                $atts['size'] = $size;
            } elseif ($multiple) {
                $atts['size'] = 4;
            } else {
                $atts['size'] = 1;
            }
        }
        $values = $tag->values;
        $labels = $tag->labels;
        if ($data = (array)$tag->get_data_option()) {
            $values = array_merge($values, array_values($data));
            $labels = array_merge($labels, array_values($data));
        }
        $defaults = array();
        $default_choice = $tag->get_default_option(null, 'multiple=1');
        foreach ($default_choice as $value) {
            $key = array_search($value, $values, true);

            if (false !== $key) {
                $defaults[] = (int)$key + 1;
            }
        }
        if ($matches = $tag->get_first_match_option('/^default:([0-9_]+)$/')) {
            $defaults = array_merge($defaults, explode('_', $matches[1]));
        }
        $defaults = array_unique($defaults);
        $shifted = false;
        if (!$multiple) {
            if ($include_blank || empty($values)) {
                array_unshift($labels, '---');
                array_unshift($values, '');
                $shifted = true;
            } elseif ($first_as_label) {
                $values[0] = '';
            }
        }
        $html = '';
        $hangover = ets_cfu_get_hangover($tag->name);
        if ( $mod_referrence && $this->context->customer->logged && $orders = getOrderReferrence()){
            $labels = array();
            $orders = array_merge(array('select_reference'=>$this->l('Select reference')),$orders);
            $values = $orders;
        }
        foreach ($values as $key => $value) {
            $selected = false;
            if ($hangover) {
                if ($multiple) {
                    $selected = in_array($value, (array)$hangover, true);
                } else {
                    $selected = ($hangover === $value);
                }
            } else {
                if (!$shifted && in_array((int)$key + 1, (array)$defaults)) {
                    $selected = true;
                } elseif ($shifted && in_array((int)$key, (array)$defaults)) {
                    $selected = true;
                }
            }
            $item_atts = array(
                'value' => ($mod_referrence && trim($key) === 'select_reference') ? ' ' : $value,
                'selected' => $selected ? 'selected' : '',
            );

            $label = isset($labels[$key]) ? $labels[$key] : $value;
            $this->smarty->assign(
                array(
                    'item_atts' => $item_atts,
                    'label' => $label,
                )
            );
            $html .= $this->display(__FILE__, 'option.tpl');
        }
        if ($multiple) {
            $atts['multiple'] = 'multiple';
        }
        $atts['name'] = $tag->name . ($multiple ? '[]' : '');
        $this->smarty->assign(
            array(
                'html_class' => ets_cfu_sanitize_html_class($tag->name),
                'atts' => $atts,
                'html' => $html,
                'validation_error' => $validation_error,
            )
        );
        return $this->display(__FILE__, 'form_select.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_submit_form_tag_handler($tag)
    {
        $class = ets_cfu_form_controls_class($tag->type);
        $atts = array();
        $atts['class'] = $tag->get_class_option($class) . ' btn btn-primary ';
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $value = isset($tag->values[0]) ? $tag->values[0] : '';
        if (empty($value)) {
            $value = 'Send';
        }
        $atts['type'] = 'submit';
        $atts['value'] = $value;
        $this->smarty->assign(
            array(
                'atts' => $atts
            )
        );
        return $this->display(__FILE__, 'form_submit.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_html_form_tag_handler($tag)
    {
        $class = ets_cfu_form_controls_class($tag->type);
        $atts = array();
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['type'] = 'html';
        $atts['name'] = $tag->name;
        $content = empty($tag->content) ? (string)reset($tag->values) : $tag->content;
        $content = trim($content);
        $this->smarty->assign(
            array(
                'html_class' => ets_cfu_sanitize_html_class($tag->name),
                'atts' => $atts,
                'content' => $content,
            )
        );

        return $this->display(__FILE__, 'form_html.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_recaptcha_form_tag_handler($tag)
    {
        $atts = array();
        $recaptcha = ETS_CFU_Recaptcha::get_instance();
        $atts['data-sitekey'] = $recaptcha->get_sitekey();
        $atts['data-type'] = $tag->get_option('type', '(audio|image)', true);
        $atts['data-size'] = $tag->get_option(
            'size', '(compact|normal|invisible)', true);
        $atts['data-theme'] = $tag->get_option('theme', '(dark|light)', true);
        $atts['data-badge'] = $tag->get_option(
            'badge', '(bottomright|bottomleft|inline)', true);
        $atts['data-tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['data-callback'] = $tag->get_option('callback', '', true);
        $atts['data-expired-callback'] =
            $tag->get_option('expired_callback', '', true);
        $atts['class'] = $tag->get_class_option(
            ets_cfu_form_controls_class($tag->type, 'g-recaptcha'));
        $atts['id'] = $tag->get_id_option();
        $this->smarty->assign(
            array(
                'atts' => $atts,
                'html' => ets_cfu_recaptcha_noscript(array('sitekey' => $atts['data-sitekey'])),
                'v3' => Configuration::get('ETS_CFU_RECAPTCHA_TYPE') != 'v2',
            )
        );
        return $this->display(__FILE__, 'form_recaptcha.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_date_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        $class .= ' ets_cfu-validates-as-date';
        if ($validation_error) {
            $class .= ' ets_cfu-not-valid';
        }
        $class .= ' form-control';
        if ($tag->has_option('time')) {
            $class .= ' datetimepicker';
        } else {
            $class .= ' datepicker';
        }
        $atts = array();
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['min'] = $tag->get_date_option('min');
        $atts['max'] = $tag->get_date_option('max');
        $atts['step'] = $tag->get_option('step', 'int', true);
        if ($tag->has_option('readonly')) {
            $atts['readonly'] = 'readonly';
        }
        if ($tag->is_required()) {
            $atts['aria-required'] = 'true';
        }
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $value = (string)reset($tag->values);
        if ($tag->has_option('placeholder') || $tag->has_option('watermark')) {
            $atts['placeholder'] = $value;
            $value = '';
        }
        $value = $tag->get_default_option($value);
        $value = ets_cfu_get_hangover($tag->name, $value);
        $atts['value'] = $value;
        $atts['type'] = 'text';
        $atts['name'] = $tag->name;
        $atts['autocomplete'] = 'off';
        $this->smarty->assign(
            array(
                'html_class' => ets_cfu_sanitize_html_class($tag->name),
                'atts' => $atts,
                'validation_error' => $validation_error,
            )
        );
        return $this->display(__FILE__, 'form_date.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_count_form_tag_handler($tag)
    {
        $targets = ets_cfu_scan_form_tags(array('name' => $tag->name));
        $maxlength = $minlength = null;
        while ($targets) {
            $target = array_shift($targets);

            if ('count' != $target->type) {
                $maxlength = $target->get_maxlength_option();
                $minlength = $target->get_minlength_option();
                break;
            }
        }
        if ($maxlength && $minlength && $maxlength < $minlength) {
            $maxlength = $minlength = null;
        }
        if ($tag->has_option('down')) {
            $value = (int)$maxlength;
            $class = 'ets_cfu-character-count down';
        } else {
            $value = '0';
            $class = 'ets_cfu-character-count up';
        }
        $atts = array();
        $atts['id'] = $tag->get_id_option();
        $atts['class'] = $tag->get_class_option($class);
        $atts['data-target-name'] = $tag->name;
        $atts['data-starting-value'] = $value;
        $atts['data-current-value'] = $value;
        $atts['data-maximum-value'] = $maxlength;
        $atts['data-minimum-value'] = $minlength;
        $this->smarty->assign(
            array(
                'atts' => $atts,
                'value' => $value,
            )
        );
        return $this->display(__FILE__, 'form_count.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_checkbox_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        if ($validation_error) {
            $class .= ' ets_cfu-not-valid';
        }
        $label_first = $tag->has_option('label_first');
        $each_a_line = $tag->has_option('each_a_line');

        $use_label_element = $tag->has_option('use_label_element');
        $exclusive = $tag->has_option('exclusive');
        //$free_text = $tag->has_option( 'free_text' );
        $multiple = false;
        if ('checkbox' == $tag->basetype) {
            $multiple = !$exclusive;
        } else { // radio
            $exclusive = false;
        }
        if ($exclusive) {
            $class .= ' ets_cfu-exclusive-checkbox';
        }
        $atts = array();
        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $tabindex = $tag->get_option('tabindex', 'signed_int', true);
        if (false !== $tabindex) {
            $tabindex = (int)$tabindex;
        }
        $html = '';
        $count = 0;
        $values = (array)$tag->values;
        $labels = (array)$tag->labels;
        $defaults = array();
        $default_choice = $tag->get_default_option(null, 'multiple=1');
        foreach ($default_choice as $value) {
            $key = array_search($value, $values, true);

            if (false !== $key) {
                $defaults[] = (int)$key + 1;
            }
        }
        if ($matches = $tag->get_first_match_option('/^default:([0-9_]+)$/')) {
            $defaults = array_merge($defaults, explode('_', $matches[1]));
        }
        $defaults = array_unique($defaults);
        $hangover = ets_cfu_get_hangover($tag->name, $multiple ? array() : '');
        foreach ($values as $key => $value) {
            $class = 'ets_cfu-list-item';
            $checked = false;
            if ($hangover) {
                if ($multiple) {
                    $checked = in_array($value, (array)$hangover, true);
                } else {
                    $checked = ($hangover === $value);
                }
            } else {
                $checked = in_array($key + 1, (array)$defaults);
            }

            if (isset($labels[$key])) {
                $label = $labels[$key];
            } else {
                $label = $value;
            }
            $item_atts = array(
                'type' => $tag->basetype,
                'name' => $tag->name . ($multiple ? '[]' : ''),
                'value' => $value,
                'checked' => $checked ? 'checked' : '',
                'tabindex' => false !== $tabindex ? $tabindex : '',
                'id' => $tag->name . '_' . $value,
            );
            if (false !== $tabindex && 0 < $tabindex) {
                $tabindex += 1;
            }
            $count += 1;
            if (1 == $count) {
                $class .= ' first';
            }
            $this->smarty->assign(
                array(
                    'class' => $class,
                    'label' => $label,
                    'label_first' => $label_first,
                    'label_for' => $tag->name . '_' . $value,
                    'use_label_element' => $use_label_element,
                    'item_atts' => $item_atts,
                    'values' => $values,
                    'count' => $count,
                    'each_a_line' => $each_a_line
                )
            );
            $html .= $this->display(__FILE__, 'item_checkbox.tpl');
        }
        $this->smarty->assign(
            array(
                'html_class' => ets_cfu_sanitize_html_class($tag->name),
                'atts' => $atts,
                'html' => $html,
                'validation_error' => $validation_error,
            )
        );
        return $this->display(__FILE__, 'form_checkbox.tpl');
    }

    /**
     * @param $tag
     * @return string
     */
    public function ets_cfu_acceptance_form_tag_handler($tag)
    {
        $validation_error = false;
        $class = ets_cfu_form_controls_class($tag->type);
        if ($validation_error) {
            $class .= ' ets_cfu-not-valid';
        }
        if ($tag->has_option('invert')) {
            $class .= ' invert';
        }
        if ($tag->has_option('optional')) {
            $class .= ' optional';
        }
        $atts = array(
            'class' => trim($class),
        );
        $item_atts = array();
        $item_atts['type'] = 'checkbox';
        $item_atts['name'] = $tag->name;
        $item_atts['value'] = '1';
        $item_atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $item_atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        if ($tag->has_option('default:on')) {
            $item_atts['checked'] = 'checked';
        }
        $item_atts['class'] = $tag->get_class_option();
        $item_atts['id'] = $tag->get_id_option();
        $content = empty($tag->content)
            ? (string)reset($tag->values)
            : $tag->content;
        $content = trim($content);
        $this->smarty->assign(array(
            'html_class' => ets_cfu_sanitize_html_class($tag->name),
            'atts' => $atts,
            'item_atts' => $item_atts,
            'content' => $content,
            'validation_error' => $validation_error,
        ));
        return $this->display(__FILE__, 'form_acceptance.tpl');
    }

    /**
     * @param bool $active
     * @param string $filters
     * @param int $start
     * @param int $limit
     * @param bool $count
     * @param string $order_by
     * @param string $order_way
     * @return array|false|int|mysqli_result|PDOStatement|resource|null
     * @throws PrestaShopDatabaseException
     */
    public function getContacts($active = false, $filters = '', $start = 0, $limit = 0, $count = false, $order_by = 'position', $order_way = 'asc')
    {
        $contacts = Db::getInstance()->executeS('
        SELECT c.*,cl.*,cs.*,e.firstname,e.lastname FROM ' . _DB_PREFIX_ . 'ets_cfu_contact c
        INNER JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs ON (c.id_contact =cs.id_contact)
        LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_lang cl on (c.id_contact=cl.id_contact AND cl.id_lang="' . (int)$this->context->language->id . '")
        LEFT JOIN ' . _DB_PREFIX_ . 'employee e on(c.id_employee= e.id_employee)
        WHERE cs.id_shop ="' . (int)Context::getContext()->shop->id . '"' . (string)$filters . ($active ? ' AND c.active=1' : '') . ' GROUP BY c.id_contact ORDER BY ' . pSQL($order_by) . ' ' . pSQL($order_way) . ' ' . ($limit ? 'LIMIT ' . (int)$start . ',' . (int)$limit : ''));
        if ($count)
            return Count($contacts);
        else
            return $contacts;
    }

    /**
     * @param string $filters
     * @param int $start
     * @param int $limit
     * @param bool $count
     * @param string $orderby
     * @return array|false|int|mysqli_result|PDOStatement|resource|null
     * @throws PrestaShopDatabaseException
     */
    public function getMessages($filters = '', $start = 0, $limit = 0, $count = false, $order_by = '')
    {
        $messages = Db::getInstance()->executeS('
            SELECT m.*,cl.title,IF(r.id_ets_cfu_message_reply IS NULL,0,1) AS replied FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message m
            INNER JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_message_shop ms ON (m.id_contact_message=ms.id_contact_message)
            lEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_lang cl on (m.id_contact=cl.id_contact AND cl.id_lang="' . (int)$this->context->language->id . '")
            LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_message_reply r ON (r.id_contact_message=m.id_contact_message)
            WHERE ms.id_shop="' . (int)Context::getContext()->shop->id . '"' . (string)$filters . ' GROUP BY m.id_contact_message ' . ($order_by ? 'ORDER BY ' . pSQL($order_by) : '') . ',replied' . ($limit ? ' LIMIT ' . (int)$start . ',' . (int)$limit : '') . '');
        if ($count)
            return count($messages);
        if ($messages) {
            foreach ($messages as &$message) {
                $message['attachments'] = $message['attachments'] ? explode(',', $message['attachments']) : array();
            }
        }
        return $messages;
    }

    /**
     * @param $reply
     * @return string
     */
    public function displayReplyMessage($reply)
    {
        $this->smarty->assign(
            array(
                'reply' => $reply,
                'countReply' => (int)Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'ets_cfu_message_reply WHERE id_contact_message="' . (int)$reply->id_contact_message . '"')
            )
        );
        return $this->display(__FILE__, 'reply.tpl');
    }

    /**
     * @param bool $zipfile
     * @throws PrestaShopException
     */
    public function processImport($zipfile = false)
    {
        if (!$zipfile) {
            $savePath = dirname(__FILE__) . '/cache/';
            if (@file_exists($savePath . 'contactformultimate.data.zip'))
                @unlink($savePath . 'contactformultimate.data.zip');
            $uploader = new Uploader('contactformdata');
            $uploader->setCheckFileSize(false);
            $uploader->setAcceptTypes(array('zip'));
            $uploader->setSavePath($savePath);
            $file = $uploader->process('contactformultimate.data.zip');
            if ($file[0]['error'] === 0) {
                if (!Tools::ZipTest($savePath . 'contactformultimate.data.zip'))
                    $this->_errors[] = $this->l('Zip file seems to be broken');
            } else {
                $this->_errors[] = $file[0]['error'];
            }
            $extractUrl = $savePath . 'contactformultimate.data.zip';
        } else
            $extractUrl = $zipfile;
        if (!@file_exists($extractUrl))
            $this->_errors[] = $this->l('Zip file doesn\'t exist');
        if (!$this->_errors) {
            $zip = new ZipArchive();
            if ($zip->open($extractUrl) === true) {
                if ($zip->locateName('Contact-Info.xml') === false) {
                    $this->_errors[] = $this->l('Import file is invalid');
                    if ($extractUrl)
                        @unlink(dirname(__FILE__) . '/cache/contactformultimate.data.zip');
                }
                $zip->close();
            } else
                $this->_errors[] = $this->l('Cannot open zip file. It might be broken or damaged');
        }
        if (!$this->_errors) {
            if (!Tools::ZipExtract($extractUrl, dirname(__FILE__) . '/views/'))
                $this->_errors[] = $this->l('Cannot extract zip data');
            if (!@file_exists(dirname(__FILE__) . '/views/Contact-Info.xml'))
                $this->_errors[] = $this->l('Import file is invalid');
        }
        if (!$this->_errors) {

            if (@file_exists(dirname(__FILE__) . '/views/Contact-Info.xml')) {
                $this->importXmlTbl(@simplexml_load_file(dirname(__FILE__) . '/views/Contact-Info.xml'));
                @unlink(dirname(__FILE__) . '/views/Contact-Info.xml');
                @unlink(dirname(__FILE__) . '/views/Data-Info.xml');
            }
            if ($extractUrl) {
                @unlink(dirname(__FILE__) . '/cache/contactformultimate.data.zip');
            }
        }
        if (!$this->_errors) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateContactForm') . '&conf=99');
        }

    }

    /**
     * @param $xml
     * @throws PrestaShopDatabaseException
     */
    public function importXmlTbl($xml)
    {
        $languages = Language::getLanguages(false);
        if ($xml && isset($xml->contactfrom)) {
            if (Tools::getValue('importdeletebefore')) {
                Db::getInstance()->execute("DELETE FROM " . _DB_PREFIX_ . "ets_cfu_contact WHERE id_contact IN (SELECT id_contact FROM " . _DB_PREFIX_ . "ets_cfu_contact_shop WHERE id_shop=" . (int)$this->context->shop->id . ")");
                Db::getInstance()->execute("DELETE FROM " . _DB_PREFIX_ . "ets_cfu_contact_lang WHERE id_contact IN (SELECT id_contact FROM " . _DB_PREFIX_ . "ets_cfu_contact_shop WHERE id_shop=" . (int)$this->context->shop->id . ")");
                Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_shop WHERE id_shop="' . (int)$this->context->shop->id . '"');
            }
            foreach ($xml->contactfrom as $dataContact) {
                $id_contact = (int)$dataContact['id'];
                if (Tools::getValue('importoverride') && $this->existContact($id_contact))
                    $contact = new ETS_CFU_Contact($id_contact);
                else {
                    $contact = new ETS_CFU_Contact();
                    $contact->position = Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_shop where id_shop=' . (int)$this->context->shop->id);
                }
                $contact_fields = Ets_cfudefines::getInstance($this)->getFields('contact');
                $configs = $contact_fields['form']['input'];
                if ($configs) {
                    foreach ($configs as $config) {
                        $key = $config['name'];
                        if (!isset($config['lang']) || !$config['lang'] && $key != 'postion')
                            $contact->$key = $dataContact->$key;
                        if ($key == 'id_employee')
                            $contact->id_employee = (int)$this->context->employee->id;
                    }
                }
                if (isset($dataContact->datalanguage) && $dataContact->datalanguage) {
                    $language_xml_default = null;
                    foreach ($dataContact->datalanguage as $language_xml) {
                        if (isset($language_xml['default']) && (int)$language_xml['default']) {
                            $language_xml_default = $language_xml;
                            break;
                        }
                    }
                    $list_language_xml = array();
                    foreach ($dataContact->datalanguage as $language_xml) {
                        $iso_code = (string)$language_xml['iso_code'];
                        $id_lang = Language::getIdByIso($iso_code);
                        $list_language_xml[] = $id_lang;
                        if ($id_lang) {
                            foreach ($configs as $config) {
                                $key = $config['name'];
                                if (isset($config['lang']) && $config['lang']) {
                                    $temp = $contact->$key;
                                    $temp[$id_lang] = (string)$language_xml->$key;
                                    if (!$temp[$id_lang]) {
                                        if (isset($language_xml_default) && $language_xml_default && isset($language_xml_default->$key) && $language_xml_default->$key) {
                                            $temp[$id_lang] = (string)$language_xml_default->$key;
                                        }
                                    }
                                    $contact->$key = $temp;
                                }
                            }
                        }
                    }
                    foreach ($languages as $language) {
                        if (!in_array($language['id_lang'], $list_language_xml)) {
                            foreach ($configs as $config) {
                                $key = $config['name'];
                                if (isset($config['lang']) && $config['lang']) {
                                    $temp = $contact->$key;
                                    if (isset($language_xml_default) && $language_xml_default && isset($language_xml_default->$key) && $language_xml_default->$key) {
                                        $temp[$language['id_lang']] = $language_xml_default->$key;
                                    }
                                    $contact->$key = $temp;
                                }
                            }
                        }
                    }
                }
                $contact->save();
            }
        }
    }

    /**
     * @return string
     * @throws PrestaShopException
     */
    public function hookDisplayBackOfficeFooter()
    {
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            return '';
        $this->smarty->assign(
            array(
                'link_ajax' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name,
            )
        );
        return $this->display(__FILE__, 'admin_footer.tpl');
    }

    /**
     * @return string
     */
    public function getDevice()
    {
        return ($userAgent = new ETS_CFU_Browser()) ? $userAgent->getBrowser() . ' ' . $userAgent->getVersion() . ' ' . $userAgent->getPlatform() : $this->l('Unknown');
    }

    /**
     * @return array|null
     */
    public function getOverrides()
    {
        if (!$this->_ps17) {
            if (!is_dir($this->getLocalPath() . 'override')) {
                return null;
            }
            $result = array();
            foreach (Tools::scandir($this->getLocalPath() . 'override', 'php', '', true) as $file) {
                $class = basename($file, '.php');
                if (PrestaShopAutoload::getInstance()->getClassPath($class . 'Core') || Module::getModuleIdByName($class)) {
                    $result[] = $class;
                }
            }
            return $result;
        } else
            return parent::getOverrides();
    }

    /**
     * @param string $classname
     * @return bool
     * @throws ReflectionException
     */
    public function addOverride($classname)
    {
        $_errors = array();
        $orig_path = $path = PrestaShopAutoload::getInstance()->getClassPath($classname . 'Core');
        if (!$path) {
            $path = 'modules' . DIRECTORY_SEPARATOR . $classname . DIRECTORY_SEPARATOR . $classname . '.php';
        }
        $path_override = $this->getLocalPath() . 'override' . DIRECTORY_SEPARATOR . $path;
        if (!@file_exists($path_override)) {
            return true;
        } else {
            @file_put_contents($path_override, preg_replace('#(\r\n|\r)#ism', "\n", Tools::file_get_contents($path_override)));
        }
        $pattern_escape_com = '#(^\s*?\/\/.*?\n|\/\*(?!\n\s+\* module:.*?\* date:.*?\* version:.*?\*\/).*?\*\/)#ism';
        if ($file = PrestaShopAutoload::getInstance()->getClassPath($classname)) {
            $override_path = _PS_ROOT_DIR_ . '/' . $file;

            if ((!@file_exists($override_path) && !is_writable(dirname($override_path))) || (@file_exists($override_path) && !is_writable($override_path))) {
                $_errors[] = sprintf($this->l('file (%s) is not writable'), $override_path);
            }
            do {
                $uniq = uniqid();
            } while (@class_exists($classname . 'OverrideOriginal_remove', false));

            $override_file = file($override_path);
            $override_file = array_diff($override_file, array("\n"));
            $this->execEval(preg_replace(array('#^\s*<\?(?:php)?#', '#class\s+' . $classname . '\s+extends\s+([a-z0-9_]+)(\s+implements\s+([a-z0-9_]+))?#i'), array(' ', 'class ' . $classname . 'OverrideOriginal' . $uniq), implode('', $override_file)));
            $override_class = new ReflectionClass($classname . 'OverrideOriginal' . $uniq);

            $module_file = file($path_override);
            $module_file = array_diff($module_file, array("\n"));
            $this->execEval(preg_replace(array('#^\s*<\?(?:php)?#', '#class\s+' . $classname . '(\s+extends\s+([a-z0-9_]+)(\s+implements\s+([a-z0-9_]+))?)?#i'), array(' ', 'class ' . $classname . 'Override' . $uniq), implode('', $module_file)));
            $module_class = new ReflectionClass($classname . 'Override' . $uniq);

            foreach ($module_class->getMethods() as $method) {
                if ($override_class->hasMethod($method->getName())) {
                    $method_override = $override_class->getMethod($method->getName());
                    if (preg_match('/module: (.*)/ism', $override_file[$method_override->getStartLine() - 5], $name) && preg_match('/date: (.*)/ism', $override_file[$method_override->getStartLine() - 4], $date) && preg_match('/version: ([0-9.]+)/ism', $override_file[$method_override->getStartLine() - 3], $version)) {
                        $_errors[] = sprintf($this->l('The method %1$s in the class %2$s is already overridden by the module %3$s version %4$s at %5$s.'), $method->getName(), $classname, $name[1], $version[1], $date[1]);
                    } else {
                        $_errors[] = sprintf($this->l('The method %1$s in the class %2$s is already overridden.'), $method->getName(), $classname);
                    }
                }
                $module_file = preg_replace('/((:?public|private|protected)\s+(static\s+)?function\s+(?:\b' . $method->getName() . '\b))/ism', "/*\n    * module: " . $this->name . "\n    * date: " . date('Y-m-d H:i:s') . "\n    * version: " . $this->version . "\n    */\n    $1", $module_file);
                if ($module_file === null) {
                    $_errors[] = sprintf($this->l('Failed to override method %1$s in class %2$s.'), $method->getName(), $classname);
                }
            }
            if (!$_errors) {
                $copy_from = array_slice($module_file, $module_class->getStartLine() + 1, $module_class->getEndLine() - $module_class->getStartLine() - 2);
                array_splice($override_file, $override_class->getEndLine() - 1, 0, $copy_from);
                $code = implode('', $override_file);

                @file_put_contents($override_path, preg_replace($pattern_escape_com, '', $code));
            }
        } else {
            $override_src = $path_override;
            $override_dest = _PS_ROOT_DIR_ . DIRECTORY_SEPARATOR . 'override' . DIRECTORY_SEPARATOR . $path;
            $dir_name = dirname($override_dest);
            if (!$orig_path && !is_dir($dir_name)) {
                $oldumask = umask(0000);
                @mkdir($dir_name, 0777);
                umask($oldumask);
            }
            if (!is_writable($dir_name)) {
                $_errors[] = sprintf($this->l('directory (%s) is not writable'), $dir_name);
            }
            $module_file = file($override_src);
            $module_file = array_diff($module_file, array("\n"));
            if ($orig_path) {
                do {
                    $uniq = uniqid();
                } while (@class_exists($classname . 'OverrideOriginal_remove', false));
                $this->execEval(preg_replace(array('#^\s*<\?(?:php)?#', '#class\s+' . $classname . '(\s+extends\s+([a-z0-9_]+)(\s+implements\s+([a-z0-9_]+))?)?#i'), array(' ', 'class ' . $classname . 'Override' . $uniq), implode('', $module_file)));
                $module_class = new ReflectionClass($classname . 'Override' . $uniq);

                foreach ($module_class->getMethods() as $method) {
                    $module_file = preg_replace('/((:?public|private|protected)\s+(static\s+)?function\s+(?:\b' . $method->getName() . '\b))/ism', "/*\n    * module: " . $this->name . "\n    * date: " . date('Y-m-d H:i:s') . "\n    * version: " . $this->version . "\n    */\n    $1", $module_file);
                    if ($module_file === null) {
                        $_errors[] = sprintf($this->l('Failed to override method %1$s in class %2$s.'), $method->getName(), $classname);
                    }
                }
            }
            if (!$_errors) {
                @file_put_contents($override_dest, preg_replace($pattern_escape_com, '', $module_file));
                Tools::generateIndex();
            }
        }
        if ($_errors)
            $this->logInstall($classname, $_errors);
        return true;
    }

    /**
     * @param $php_code
     */
    public function execEval($php_code)
    {
        if (function_exists('ets_cfu_excelVal')) {
            return call_user_func('ets_cfu_excelVal', $php_code);
        } else {
            $temp = @tempnam($this->getLocalPath() . 'cache', 'execEval');
            $handle = fopen($temp, "w+");
            fwrite($handle, "<?php\n" . $php_code);
            fclose($handle);
            include $temp;
            @unlink($temp);
        }
    }

    /**
     * @param string $classname
     * @return bool
     */
    public function removeOverride($classname)
    {
        if ($this->isLogInstall($classname))
            return true;
        $orig_path = $path = PrestaShopAutoload::getInstance()->getClassPath($classname . 'Core');
        if ($orig_path && !$file = PrestaShopAutoload::getInstance()->getClassPath($classname))
            return true;
        elseif (!$orig_path && Module::getModuleIdByName($classname))
            $path = 'modules' . DIRECTORY_SEPARATOR . $classname . DIRECTORY_SEPARATOR . $classname . '.php';
        $override_path = $orig_path ? _PS_ROOT_DIR_ . '/' . $file : _PS_OVERRIDE_DIR_ . $path;
        if (!@is_file($override_path) || !is_writable($override_path))
            return true;
        return parent::removeOverride($classname);
    }

    public $log_file = 'install.log';

    /**
     * @param $classname
     * @param $_errors
     */
    public function logInstall($classname, $_errors)
    {
        $log_file = $this->getLocalPath() . $this->log_file;
        $data = array();
        if (@file_exists($log_file))
            $data = (array)Tools::jsonDecode(Tools::file_get_contents($log_file));
        $data[$classname] = $_errors;
        @file_put_contents($log_file, Tools::jsonEncode($data));
    }

    /**
     * @param $classname
     * @return bool
     */
    public function isLogInstall($classname)
    {
        $log_file = $this->getLocalPath() . $this->log_file;
        if (!@file_exists($log_file))
            return false;
        $cached = (array)Tools::jsonDecode(Tools::file_get_contents($log_file));
        if ($cached && !empty($cached[$classname]))
            return true;
        return false;
    }

    /**
     * @return bool
     */
    public function clearLogInstall()
    {
        $log_file = $this->getLocalPath() . $this->log_file;
        if (@file_exists($log_file))
            @unlink($log_file);
        return true;
    }

    /* QUANG DEV */
    public function getOrderReferrence(){
        $orders = [];

        if (!isset($this->customer_thread['id_order'])) {
            $customer_orders = Order::getCustomerOrders($this->context->customer->id);

            foreach ($customer_orders as $customer_order) {
                $myOrder = new Order((int)$customer_order['id_order']);

                if (Validate::isLoadedObject($myOrder)) {
                    $orders[$customer_order['id_order']] = $customer_order;
                    $orders[$customer_order['id_order']]['products'] = $myOrder->getProducts();
                }
            }
        } elseif ((int)$this->customer_thread['id_order'] > 0) {
            $myOrder = new Order($this->customer_thread['id_order']);

            if (Validate::isLoadedObject($myOrder)) {
                $orders[$myOrder->id] = $this->context->controller->objectPresenter->present($myOrder);
                $orders[$myOrder->id]['id_order'] = $myOrder->id;
                $orders[$myOrder->id]['products'] = $myOrder->getProducts();
            }
        }

        if ($this->customer_thread['id_product']) {
            $id_order = isset($this->customer_thread['id_order']) ?
                (int)$this->customer_thread['id_order'] :
                0;

            $orders[$id_order]['products'][(int)$this->customer_thread['id_product']] = $this->context->controller->objectPresenter->present(
                new Product((int)$this->customer_thread['id_product'])
            );
        }

        $res = array();
        if ( $orders ){
            foreach ( $orders as $order ){
                $res[] = $order['reference'];
            }
        }

        return $res;
    }

    public function getMaxId()
    {
        $req = 'SELECT max(`id_contact`) as maxid
			FROM `'._DB_PREFIX_.'ets_cfu_contact` tbl';
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);
        return isset($row['maxid']) ? (int)$row['maxid'] : 0;
    }

    public function checkAliasExit($alias,$id_lang,$id_contact= false){
        if (!$id_lang) {
            $id_lang = $this->context->language->id;
        }

        return Db::getInstance()->getValue("
            SELECT c.id_contact FROM " . _DB_PREFIX_ . "ets_cfu_contact c
            LEFT JOIN " . _DB_PREFIX_ . "ets_cfu_contact_lang cl ON (c.id_contact = cl.id_contact AND cl.id_lang = " . (int)$id_lang . ")
            INNER JOIN " . _DB_PREFIX_ . "ets_cfu_contact_shop cs ON (c.id_contact = cs.id_contact AND cs.id_shop = " . (int)$this->context->shop->id . ")
            WHERE cl.`thank_you_alias` LIKE '%" . pSQL($alias) . "%' ".($id_contact ? " AND c.`id_contact` != ".(int)$id_contact." ":"")."
            GROUP BY c.id_contact
        ");
    }

    public static function checkVersionPs($ver,$compare = '>='){
        if (version_compare(_PS_VERSION_,$ver,$compare)){
            return true;
        }
        return false;
    }
}    
