<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_') || !defined('_ETS_MODULE_'))
    exit;

class Ets_cfudefines
{
    public static $_hooks = array(
        'actionOutputHTMLBefore',
        'contactFormUltimateTopBlock',
        'displayBackOfficeHeader',
        'displayContactFormUltimate',
        'displayHeader',
        'displayHome',
        'moduleRoutes',
        'displayNav2',
        'displayNav',
        'displayTop',
        'displayLeftColumn',
        'displayFooter',
        'displayRightColumn',
        'displayProductAdditionalInfo',
        'displayFooterProduct',
        'displayAfterProductThumbs',
        'displayRightColumnProduct',
        'displayLeftColumnProduct',
        'displayShoppingCartFooter',
        'displayCustomerAccountForm',
        'displayCustomerLoginFormAfter',
        'displayBackOfficeFooter'
    );
    public static $instance;
    public $_config_fields;
    public $_email_fields;
    public $_ip_black_list;
    public $contact_fields;
    public $_inputs = array();
    public $_tabs = array();
    public $module;
    public $smarty;
    public $groups;

    public function __construct($module = null)
    {
        if (!(is_object($module)) || !$module) {
            $module = Module::getInstanceByName(_ETS_MODULE_);
        }
        $this->module = $module;
        $this->context = Context::getContext();
        if (is_object($this->context->smarty)) {
            $this->smarty = $this->context->smarty;
        }
        $this->groups = Group::getGroups($this->context->language->id, true);
    }

    public static function getInstance($module = null)
    {
        if (!(isset(self::$instance))) {
            self::$instance = new Ets_cfudefines($module);
        }
        return self::$instance;
    }

    public function getFields($fields)
    {
        switch ($fields) {
            case 'config':
                if (!(isset($this->_config_fields)) || !$this->_config_fields) {
                    $this->_config_fields = array(
                        'form' => array(
                            'legend' => array(
                                'title' => $this->l('Integration'),
                                'icon' => 'icon-cogs'
                            ),
                            'id_form' => 'ets_cfu_module_form_integration',
                            'input' => array(
                                array(
                                    'type' => 'switch',
                                    'name' => 'ETS_CFU_ENABLE_RECAPTCHA',
                                    'label' => $this->l('Enable reCAPTCHA'),
                                    'values' => array(
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_RECAPTCHA_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_RECAPTCHA_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 0,
                                    'form_group_class' => 'form_group_contact google',
                                ),
                                array(
                                    'type' => 'radio',
                                    'label' => $this->l('ReCaptcha type'),
                                    'name' => 'ETS_CFU_RECAPTCHA_TYPE',
                                    'required' => true,
                                    'form_group_class' => 'form_group_contact google google2',
                                    'values' => array(
                                        array(
                                            'id' => 'id_recaptcha_v2',
                                            'value' => 'v2',
                                            'label' => $this->l('reCaptcha v2'),
                                        ),
                                        array(
                                            'id' => 'id_recaptcha_v3',
                                            'value' => 'v3',
                                            'label' => $this->l('reCaptcha v3'),
                                        ),
                                    ),
                                    'default' => 'v2'
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Site Key'),
                                    'name' => 'ETS_CFU_SITE_KEY',
                                    'required' => true,
                                    'form_group_class' => 'form_group_contact google google2 capv2',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Secret Key'),
                                    'name' => 'ETS_CFU_SECRET_KEY',
                                    'required' => true,
                                    'form_group_class' => 'form_group_contact google google2 capv2',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Site Key v3'),
                                    'name' => 'ETS_CFU_SITE_KEY_V3',
                                    'required' => true,
                                    'form_group_class' => 'form_group_contact google google3 capv3',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Secret Key v3'),
                                    'name' => 'ETS_CFU_SECRET_KEY_V3',
                                    'required' => true,
                                    'form_group_class' => 'form_group_contact google google3 capv3',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Form Title'),
                                    'name' => 'ETS_CFU_CONTACT_ALIAS',
                                    'required' => true,
                                    'lang' => true,
                                    'validate' => 'isLinkRewrite',
                                    'default' => 'contact',
                                    'form_group_class' => 'form_group_contact other_setting',
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'ETS_CFU_URL_SUFFIX',
                                    'label' => $this->l('Use URL suffix'),
                                    'values' => array(
                                        array(
                                            'id' => 'ETS_CFU_URL_SUFFIX_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'ETS_CFU_URL_SUFFIX_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 0,
                                    'form_group_class' => 'form_group_contact other_setting',
                                    'desc' => $this->l('Add ".html" to the end of form page URL. Set this to "Yes" if your product pages are ended with ".html". Set this to "No", if product pages are NOT ended with ".html"'),
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'ETS_CFU_REMOVE_ID',
                                    'label' => $this->l('Remove ID from contact form URL'),
                                    'values' => array(
                                        array(
                                            'id' => 'ETS_CFU_REMOVE_ID_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'ETS_CFU_REMOVE_ID_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 1,
                                    'form_group_class' => 'form_group_contact other_setting',
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'ETS_CFU_ENABLE_TMCE',
                                    'label' => $this->l('Enable TinyMCE editor'),
                                    'values' => array(
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_TMCE_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_TMCE_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 1,
                                    'form_group_class' => 'form_group_contact other_setting',
                                    'desc' => $this->l('Set this to "Yes" will allow you to enable rich text editor for textarea fields when compiling contact forms'),
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'ETS_CFU_ENABLE_HOOK_SHORTCODE',
                                    'label' => $this->l('Enable Shortcode & Contact form in Prestashop hook'),
                                    'values' => array(
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_HOOK_SHORTCODE_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_HOOK_SHORTCODE_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 0,
                                    'form_group_class' => 'form_group_contact other_setting',
                                ),
                                array(
                                    'type' => 'text',
                                    'name' => 'ETS_CFU_NUMBER_MESSAGE',
                                    'label' => $this->l('Number of messages displayed per message page in back office'),
                                    'default' => 20,
                                    'form_group_class' => 'form_group_contact other_setting',
                                    'required' => true,
                                    'validate' => 'isUnsignedInt',
                                ),

                            ),
                            'submit' => array(
                                'title' => $this->l('Save'),
                            )
                        ),
                    );
                }
                return $this->_config_fields;
            case 'email':
                if (!(isset($this->_email_fields)) || !$this->_email_fields) {
                    $this->_email_fields = array(
                        'form' => array(
                            'legend' => array(
                                'title' => $this->l('Email template'),
                                'icon' => 'icon-file-text-o'
                            ),
                            'id_form' => 'ets_cfu_module_form_email_template',
                            'input' => array(
                                array(
                                    'type' => 'switch',
                                    'name' => 'ETS_CFU_ENABLE_TEMPLATE',
                                    'label' => $this->l('Enable email template'),
                                    'values' => array(
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_TEMPLATE_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'ETS_CFU_ENABLE_TEMPLATE_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 1,
                                    'form_group_class' => 'template',
                                    'desc' => $this->l('Disable this option if you would like to send simple email without HTML/CSS styles'),
                                ),
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Mail to admin'),
                                    'name' => 'ETS_CFU_EMAIL_TEMPLATE_ADMIN',
                                    'lang' => true,
                                    'required' => true,
                                    'autoload_rte' => true,
                                    'default' => $this->display('mail_template.tpl'),
                                    'form_group_class' => 'template template2',
                                    'desc' => $this->l('Available shortcodes:') . '<span>{shop_name}</span>,<span>{shop_logo}</span>,<span>{message_content}</span>,<span>{shop_url}</span>',
                                ),
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Auto responder'),
                                    'name' => 'ETS_CFU_EMAIL_TEMPLATE_CUSTOMER',
                                    'lang' => true,
                                    'required' => true,
                                    'autoload_rte' => true,
                                    'form_group_class' => 'template template2',
                                    'default' => $this->display('mail_template2.tpl'),
                                    'desc' => $this->l('Available short codes:') . '<span>{shop_name}</span>,<span>{shop_logo}</span>,<span>{message_content}</span>,<span>{shop_url}</span>',
                                ),
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Reply email template'),
                                    'name' => 'ETS_CFU_EMAIL_REPLY_TEMPLATE',
                                    'lang' => true,
                                    'required' => true,
                                    'autoload_rte' => true,
                                    'form_group_class' => 'template template2',
                                    'default' => $this->display('mail_template_reply.tpl'),
                                    'desc' => $this->l('Available short codes:') . '<span>{shop_name}</span>,<span>{shop_logo}</span>,<span>{message_content}</span>,<span>{shop_url}</span>',
                                ),
                            ),
                            'submit' => array(
                                'title' => $this->l('Save'),
                            )
                        ),
                    );
                }
                return $this->_email_fields;
            case 'ip_black_list':
                if (!(isset($this->_ip_black_list)) || !$this->_ip_black_list) {
                    $this->_ip_black_list = array(
                        'form' => array(
                            'legend' => array(
                                'title' => $this->l('IP')." & ".$this->l('Email blacklist'),
                                'icon' => 'icon-user-times'
                            ),
                            'id_form' => 'ets_cfu_module_form_ip_black_list',
                            'input' => array(
                                array(
                                    'type' => 'textarea',
                                    'name' => 'ETS_CFU_IP_BLACK_LIST',
                                    'label' => $this->l('IP blacklist (IPs to block)'),
                                    'desc' => $this->l('Enter exact IP or IP pattern using "*", each IP/IP pattern on a line. For example: 69.89.31.226, 69.89.31.*, *.226, etc.'),
                                    'form_group_class' => 'form_group_contact black_list',
                                ),
                                array(
                                    'type' => 'textarea',
                                    'name' => 'ETS_CFU_EMAIL_BLACK_LIST',
                                    'label' => $this->l('Email blacklist (emails to block)'),
                                    'desc' => $this->l('Enter exact email address or email pattern using "*", each email/email pattern on a line. For example: example@mail.ru,*@mail.ru, *@qq.com, etc.'),
                                    'form_group_class' => 'form_group_contact black_list',
                                ),
                            ),
                            'submit' => array(
                                'title' => $this->l('Save'),
                            )
                        )
                    );
                }
                return $this->_ip_black_list;
            case 'contact':
                if (!(isset($this->contact_fields)) || !$this->contact_fields) {
                    $this->contact_fields = array(
                        'form' => array(
                            'legend' => array(
                                'title' => Tools::getValue('id_contact') ? $this->l('Edit contact form') : $this->l('Add contact form'),
                                'icon' => Tools::getValue('id_contact') ? 'icon-pencil-square-o' : 'icon-pencil-square-o'
                            ),
                            'input' => array(
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Form editor'),
                                    'name' => 'short_code',
                                    'lang' => true,
                                    'id' => 'wpcfu-form',
                                    'class' => 'wpcfu-form ',//hide
                                    'form_group_class' => 'form_group_contact form short_code',
                                ),
                                array(
                                    'label' => $this->l('Render form'),
                                    'type' => 'textarea',
                                    'name' => 'render_form',
                                    'class' => 'hide',
                                    'form_group_class' => 'form_group_contact form short_code',
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'save_message',
                                    'label' => $this->l('Save messages'),
                                    'values' => array(
                                        array(
                                            'id' => 'save_message_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'save_message_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 1,
                                    'form_group_class' => 'form_group_contact general_settings',
                                    'desc' => $this->l('Save customer messages to "Messages" tab.'),
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'save_attachments',
                                    'label' => $this->l('Save attachments'),
                                    'values' => array(
                                        array(
                                            'id' => 'save_attachments_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'save_attachments_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'desc' => $this->l('Save attached files on your server, you can download the files in "Messages" tab. Enable this option is useful but it will take some of your hosting disk space to store the files. You can set this to "No" if it is not necessary for saving files on server because the files will be also sent to your email inbox'),
                                    'default' => 1,
                                    'form_group_class' => 'form_group_contact general_settings general_settings4',
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'star_message',
                                    'label' => $this->l('Mark messages from this contact form as "Star message"'),
                                    'values' => array(
                                        array(
                                            'id' => 'star_message_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'star_message_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 0,
                                    'form_group_class' => 'form_group_contact general_settings general_settings4',
                                    'desc' => $this->l('Highlight messages sent from this contact form in the "Messages" tab by a yellow star'),
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'open_form_by_button',
                                    'label' => $this->l('Open form by button'),
                                    'values' => array(
                                        array(
                                            'id' => 'open_form_by_button_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'open_form_by_button_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'form_group_class' => 'form_group_contact general_settings',
                                    'desc' => $this->l('Display a button (the form is hidden initially), when customer click on the button, it will open the form via a popup'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Button label'),
                                    'name' => 'button_label',
                                    'lang' => true,
                                    'default' => $this->l('Open contact form'),
                                    'form_group_class' => 'form_group_contact general_settings general_settings2',
                                ),
                                array(
                                    'type' => 'switch',
                                    'name' => 'enable_form_page',
                                    'label' => $this->l('Enable separate form page'),
                                    'values' => array(
                                        array(
                                            'id' => 'enable_form_page_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'enable_form_page_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 1,
                                    'form_group_class' => 'form_group_contact seo',
                                    'desc' => $this->l('Besides displaying the form using short code, custom hook and default Prestashop hooks, you can also create a specific web page to display the form'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Form title'),
                                    'name' => 'title',
                                    'required' => true,
                                    'lang' => true,
                                    'class' => 'title_form',
                                    'form_group_class' => 'form_group_contact seo seo3',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Contact form alias'),
                                    'name' => 'title_alias',
                                    'lang' => true,
                                    'class' => 'alias_form',
                                    'form_group_class' => 'form_group_contact seo seo3',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Meta title'),
                                    'name' => 'meta_title',
                                    'lang' => true,
                                    'form_group_class' => 'form_group_contact seo seo3',
                                ),
                                array(
                                    'type' => 'tags',
                                    'label' => $this->l('Meta keywords'),
                                    'name' => 'meta_keyword',
                                    'lang' => true,
                                    'form_group_class' => 'form_group_contact seo seo3',
                                ),
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Meta description'),
                                    'name' => 'meta_description',
                                    'lang' => true,
                                    'form_group_class' => 'form_group_contact seo seo3',
                                ),
                                array(
                                    'type' => 'checkbox',
                                    'name' => 'hook',
                                    'label' => $this->l('Available display position (default Prestashop hooks)'),
                                    'values' => array(
                                        'query' => array(
                                            array(
                                                'name' => $this->l('Header - top navigation'),
                                                'val' => 'nav_top',
                                                'id' => 'nav_top',
                                            ),
                                            array(
                                                'name' => $this->l('Header - main header'),
                                                'val' => 'header',
                                                'id' => 'header',
                                            ),
                                            array(
                                                'name' => $this->l('Top'),
                                                'val' => 'displayTop',
                                                'id' => 'displayTop',
                                            ),
                                            array(
                                                'name' => $this->l('Home'),
                                                'val' => 'home',
                                                'id' => 'home',
                                            ),
                                            array(
                                                'name' => $this->l('Left column'),
                                                'val' => 'left_column',
                                                'id' => 'left_column',
                                            ),
                                            array(
                                                'name' => $this->l('Right column'),
                                                'val' => 'right_column',
                                                'id' => 'right_column',
                                            ),
                                            array(
                                                'name' => $this->l('Footer'),
                                                'val' => 'footer_page',
                                                'id' => 'footer_page',
                                            ),
                                            array(
                                                'name' => $this->l('Product page - below product images'),
                                                'val' => 'product_thumbs',
                                                'id' => 'product_thumbs',
                                            ),
                                            array(
                                                'name' => $this->l('Product page - Footer'),
                                                'val' => 'product_footer',
                                                'id' => 'product_footer',
                                            ),
                                            array(
                                                'name' => $this->l('Checkout page'),
                                                'val' => 'checkout_page',
                                                'id' => 'checkout_page',
                                            ),
                                            array(
                                                'name' => $this->l('Login page'),
                                                'val' => 'login_page',
                                                'id' => 'login_page',
                                            ),
                                        ),
                                        'id' => 'id',
                                        'name' => 'name'
                                    ),
                                    'desc' => $this->l('Besides using short code, custom hook and a separated page to display the contact form, you can also display contact form on default Prestashop pre-defined hooks'),
                                    'form_group_class' => 'form_group_contact general_settings form_hook '.(!Configuration::get('ETS_CFU_ENABLE_HOOK_SHORTCODE')?'hidden':'')
                                ),
                                array(
                                    'type' => 'group',
                                    'label' => $this->l('Give access to customer group'),
                                    'name' => 'group_access',
                                    'values' => $this->groups,
                                    'col' => '6',
                                    'is_id'=> (int)Tools::getValue('id_contact') ? true : false,
                                    'custommer_access' => ETS_CFU_Contact::getGroupAccessById((int)Tools::getValue('id_contact')),
                                    'form_group_class' => 'form_group_contact general_settings form_hook'
                                ),
                                array(
                                    'type' => 'switch',
                                    'label' => $this->l('Only display when customer logged in'),
                                    'name' => 'only_customer',
                                    'values' => array(
                                        array(
                                            'id' => 'active_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'active_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 0,
                                    'form_group_class' => 'form_group_contact general_settings',
                                ),
                                array(
                                    'type' => 'switch',
                                    'label' => $this->l('Activate contact form'),
                                    'name' => 'active',
                                    'values' => array(
                                        array(
                                            'id' => 'active_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'active_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 1,
                                    'form_group_class' => 'form_group_contact general_settings',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('To'),
                                    'name' => 'email_to',
                                    'multi' => true,
                                    'show_btn_add' => true,
                                    'class' => 'hide',
                                    'required' => true,
                                    'form_group_class' => 'form_group_contact mail mail1',
                                    'default' => Configuration::get('PS_SHOP_NAME') . ' <' . Configuration::get('PS_SHOP_EMAIL') . '>',
                                    'desc' => $this->l('Enter email addresses of people who will receive this email'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Bcc'),
                                    'name' => 'bcc',
                                    'multi' => true,
                                    'show_btn_add' => true,
                                    'class' => 'hide',
                                    'form_group_class' => 'form_group_contact mail mail1',
                                    'desc' => $this->l('When you place email addresses in the BCC field of a message, those addresses are invisible to the recipients of the email.'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('From'),
                                    'name' => 'email_from',
                                    'multi' => true,
                                    'show_btn_add' => false,
                                    'class' => 'hide',
                                    'mail_tag' => true,
                                    'form_group_class' => 'form_group_contact mail mail1',
                                    'desc' => $this->l('This should be an authorized email address. Normally it is your shop SMTP email (if your website is enabled with SMTP) or an email associated with your website domain name (if your website uses default Mail() function to send emails. Leave blank to get default setting)'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Subject'),
                                    'name' => 'subject',
                                    'lang' => true,
                                    'required' => true,
                                    'validate' => 'isMailSubject',
                                    'form_group_class' => 'form_group_contact mail mail1',
                                    'default' => $this->l('Your email subject'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Reply to'),
                                    'name' => 'additional_headers',
                                    'multi' => true,
                                    'show_btn_add' => false,
                                    'class' => 'hide',
                                    'mail_tag' => true,
                                    'form_group_class' => 'form_group_contact mail mail1',
                                ),
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Message body'),
                                    'name' => 'message_body',
                                    'lang' => true,
                                    'autoload_rte' => true,
                                    'form_group_class' => 'form_group_contact mail mail1 message_body',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('File attachments'),
                                    'name' => 'file_attachments',
                                    'file_attachment' => 'true',
                                    'class' => 'hide file_attachment',
                                    'form_group_class' => 'form_group_contact mail mail1 attach',
                                    'desc' => $this->l('*Note: You need to enter respective mail-tags for the file form-tags used in the "Form editor" into this field in order to receive the files via email as well as "Messages" tab. See more details about mail-tag in the documentation of this module.'),
                                ),
                                array(
                                    'type' => 'switch',
                                    'label' => $this->l('Enable auto responder'),
                                    'name' => 'use_email2',
                                    'values' => array(
                                        array(
                                            'id' => 'use_email2_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'use_email2_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'desc' => $this->l('Auto responder is an additional email sent to anyone you want when customer submits a contact form. It\'s often used to send a confirmation email to customer when they successfully submit a contact form'),
                                    'form_group_class' => 'form_group_contact mail mail2',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('To'),
                                    'name' => 'email_to2',
                                    'multi' => true,
                                    'show_btn_add' => true,
                                    'mail_tag' => true,
                                    'class' => 'hide',
                                    'form_group_class' => 'form_group_contact mail mail2',
                                    'desc' => $this->l('Enter email addresses of people who will receive this email'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Bcc'),
                                    'name' => 'bcc2',
                                    'multi' => true,
                                    'show_btn_add' => true,
                                    'class' => 'hide',
                                    'form_group_class' => 'form_group_contact mail mail2',
                                    'desc' => $this->l('When you place email addresses in the BCC field of a message, those addresses are invisible to the recipients of the email.'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('From'),
                                    'name' => 'email_from2',
                                    'multi' => true,
                                    'show_btn_add' => false,
                                    'class' => 'hide',
                                    'form_group_class' => 'form_group_contact mail mail2',
                                    'desc' => $this->l('This should be an authorized email address. Normally it is your shop SMTP email (if your website is enabled with SMTP) or an email associated with your website domain name (if your website uses default Mail() function to send emails. Leave blank to get default setting)'),
                                    'default' => Configuration::get('PS_SHOP_NAME') . ' <' . Configuration::get('PS_SHOP_EMAIL') . '>',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Subject'),
                                    'name' => 'subject2',
                                    'lang' => true,
                                    'required' => true,
                                    'validate' => 'isMailSubject',
                                    'form_group_class' => 'form_group_contact mail mail2',
                                    'default' => $this->l('Your email has been sent'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Reply to'),
                                    'name' => 'additional_headers2',
                                    'multi' => true,
                                    'show_btn_add' => false,
                                    'class' => 'hide',
                                    'form_group_class' => 'form_group_contact mail mail2',
                                    'default' => Configuration::get('PS_SHOP_NAME') . ' <' . Configuration::get('PS_SHOP_EMAIL') . '>',
                                ),
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Message body'),
                                    'name' => 'message_body2',
                                    'lang' => true,
                                    'autoload_rte' => true,
                                    'form_group_class' => 'form_group_contact mail mail2 message_body',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('File attachments'),
                                    'name' => 'file_attachments2',
                                    'file_attachment' => 'true',
                                    'class' => 'hide file_attachment',
                                    'form_group_class' => 'form_group_contact mail mail2 attach',
                                    'desc' => $this->l('*Note: You need to enter respective mail-tags for the file form-tags used in the "Form editor" into this field in order to receive the files via email. See more details about mail-tag in the documentation of this module.'),
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Notification message when contact form was sent successfully'),
                                    'name' => 'message_mail_sent_ok',
                                    'lang' => true,
                                    'default' => $this->l('Thank you for your message. It has been sent.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Notification message when contact form failed to send'),
                                    'name' => 'message_mail_sent_ng',
                                    'lang' => true,
                                    'default' => $this->l('There was an error while trying to send your message. Please try again later.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Validation errors occurred'),
                                    'name' => 'message_validation_error',
                                    'lang' => true,
                                    'default' => $this->l('One or more fields have an error. Please check and try again.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Submission was referred as spam'),
                                    'name' => 'message_spam',
                                    'lang' => true,
                                    'default' => $this->l('There was an error while trying to send your message. Please try again later.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('There are terms that the sender must accept'),
                                    'name' => 'message_accept_terms',
                                    'lang' => true,
                                    'default' => $this->l('You must accept the terms and conditions before sending your message.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('There is a field that the sender must fill in'),
                                    'name' => 'message_invalid_required',
                                    'lang' => true,
                                    'default' => $this->l('The field is required.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('There is a field with input value that is longer than the maximum allowed length'),
                                    'name' => 'message_invalid_too_long',
                                    'lang' => true,
                                    'default' => $this->l('The field is too long.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('There is a field with input value that is shorter than the minimum allowed length'),
                                    'name' => 'message_invalid_too_short',
                                    'lang' => true,
                                    'default' => $this->l('The field is too short.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Date format that the sender entered is invalid'),
                                    'name' => 'message_invalid_date',
                                    'lang' => true,
                                    'default' => $this->l('The date format is incorrect.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('The date sender entered is earlier than minimum limit'),
                                    'name' => 'message_date_too_early',
                                    'lang' => true,
                                    'default' => $this->l('The date is before the earliest one allowed.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('The date sender entered is later than maximum limit'),
                                    'name' => 'message_date_too_late',
                                    'lang' => true,
                                    'default' => $this->l('The date is after the latest one allowed.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Uploading a file failed due to some unknown reasons'),
                                    'name' => 'message_upload_failed',
                                    'lang' => true,
                                    'default' => $this->l('There was an unknown error while uploading the file.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Invalid upload file type'),
                                    'name' => 'message_upload_file_type_invalid',
                                    'lang' => true,
                                    'default' => $this->l('You are not allowed to upload files of this type.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Sender does not enter the correct answer to the quiz'),
                                    'name' => 'message_quiz_answer_not_correct',
                                    'lang' => true,
                                    'default' => $this->l('The answer to the quiz is incorrect.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Uploaded file is too large'),
                                    'name' => 'message_upload_file_too_large',
                                    'lang' => true,
                                    'default' => $this->l('The file is too big.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Uploading a file failed due to PHP error'),
                                    'name' => 'message_upload_failed_php_error',
                                    'lang' => true,
                                    'default' => $this->l('There was an error while uploading the file.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Number format that the sender entered is invalid'),
                                    'name' => 'message_invalid_number',
                                    'lang' => true,
                                    'default' => $this->l('The number format is invalid.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('The number sender entered is smaller than minimum limit'),
                                    'name' => 'message_number_too_small',
                                    'lang' => true,
                                    'default' => $this->l('The number is smaller than the minimum allowed.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('The number sender entered is larger than maximum limit'),
                                    'name' => 'message_number_too_large',
                                    'lang' => true,
                                    'default' => $this->l('The number is larger than the maximum allowed'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Email address that the sender entered is invalid'),
                                    'name' => 'message_invalid_email',
                                    'lang' => true,
                                    'default' => $this->l('The e-mail address entered is invalid.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('URL that the sender entered is invalid'),
                                    'name' => 'message_invalid_url',
                                    'lang' => true,
                                    'default' => $this->l('The URL is invalid.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Telephone number that the sender entered is invalid'),
                                    'name' => 'message_invalid_tel',
                                    'lang' => true,
                                    'default' => $this->l('The telephone number is invalid.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Message IP is in blacklist'),
                                    'name' => 'message_ip_black_list',
                                    'lang' => true,
                                    'default' => $this->l('You are not allowed to submit this form. Please contact webmaster for more information.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Message Email is in blacklist'),
                                    'name' => 'message_email_black_list',
                                    'lang' => true,
                                    'default' => $this->l('Your email is blocked. Contact webmaster for more info.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Captcha entered is invalid'),
                                    'name' => 'message_captcha_not_match',
                                    'lang' => true,
                                    'default' => $this->l('Your entered code is incorrect.'),
                                    'form_group_class' => 'form_group_contact message',
                                ),
                                /*Quang ADD*/
                                array(
                                    'type' => 'switch',
                                    'label' => $this->l('Display "Thank you" page after form submission'),
                                    'name' => 'thank_you_active',
                                    'values' => array(
                                        array(
                                            'id' => 'active_on',
                                            'value' => 1,
                                            'label' => $this->l('Yes')
                                        ),
                                        array(
                                            'id' => 'active_off',
                                            'value' => 0,
                                            'label' => $this->l('No')
                                        )
                                    ),
                                    'default' => 0,
                                    'form_group_class' => 'form_group_contact thank_you thank_you_active',
                                ),
                                array(
                                    'label' => $this->l('"Thank you" page'),
                                    'type' => 'select',
                                    'name' => 'thank_you_page',
                                    'options' => array(
                                        'query'=>array(
                                            array(
                                                'name' => $this->l('Default page'),
                                                'thank_page' => 'thank_page_default',
                                            ),
                                            array(
                                                'name' => $this->l('Custom URL'),
                                                'thank_page' => 'thank_page_url',
                                            ),
                                        ),
                                        'id' => 'thank_page',
                                        'name' => 'name',
                                    ),
                                    'default' => 'thank_page_default',
                                    'form_group_class' => 'form_group_contact thank_you thank_you_page',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Title'),
                                    'name' => 'thank_you_page_title',
                                    'lang' => true,
                                    'required' => true,
                                    'class' => 'title_tk_page',
                                    'form_group_class' => 'form_group_contact thank_you thank_you_message',
                                    'default' => $this->l('Thanks for submitting the form')
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => 'Page alias',
                                    'lang' => true,
                                    'name' => 'thank_you_alias',
                                    'class' => 'alias_tk_page',
                                    'form_group_class' => 'form_group_contact thank_you thank_you_message',
                                    'default' => $this->l('thanks-for-submitting-the-form')
                                ),
                                array(
                                    'type' => 'textarea',
                                    'label' => $this->l('Content'),
                                    'name' => 'thank_you_message',
                                    'class' => 'rte',
                                    'lang' => true,
                                    'required' => true,
                                    'autoload_rte' => true,
                                    'default' => $this->l('Thank you for contacting us. This message is to confirm that you have successfully submitted the contact form. We\'ll get back to you shortly.'),
                                    'form_group_class' => 'form_group_contact thank_you thank_you_message',
                                ),
                                array(
                                    'type' => 'text',
                                    'label' => $this->l('Custom URL'),
                                    'name' => 'thank_you_url',
                                    'lang' => true,
                                    'required' => true,
                                    'placeholder'=>$this->l('https://example.com/thank-you.html'),
                                    'default' => '',
                                    'desc' => $this->l('Customer will be redirected to this URL after submitting the form successfully'),
                                    'form_group_class' => 'form_group_contact thank_you thank_you_url',
                                ),

                            ),
                            'submit' => array(
                                'title' => $this->l('Save'),
                            ),
                            'buttons' => array(
                                array(
                                    'id' => 'backListContact',
                                    'href' => defined('_PS_ADMIN_DIR_') ? 'index.php?controller=AdminContactFormUltimateContactForm&token=' . Tools::getAdminTokenLite('AdminContactFormUltimateContactForm') : '#',
                                    'icon' => 'process-icon-cancel',
                                    'class' => 'pull-left',
                                    'title' => $this->l('Cancel'),
                                )
                            )
                        ),
                    );
                }
                return $this->contact_fields;
            case 'inputs':
                if (!(isset($this->_inputs)) || !$this->_inputs) {
                    $this->_inputs = array(
                        'tex' => array(
                            'id' => 'text',
                            'label' => $this->l('Text'),
                        ),
                        'tea' => array(
                            'id' => 'textarea',
                            'label' => $this->l('Textarea'),
                        ),
                        'ema' => array(
                            'id' => 'email',
                            'label' => $this->l('Email'),
                        ),
                        'pas' => array(
                            'id' => 'password',
                            'label' => $this->l('Password'),
                        ),
                        'tel' => array(
                            'id' => 'tel',
                            'label' => $this->l('Phone'),
                        ),
                        'url' => array(
                            'id' => 'url',
                            'label' => $this->l('URL'),
                        ),
                        'num' => array(
                            'id' => 'number',
                            'label' => $this->l('Number'),
                        ),
                        'dat' => array(
                            'id' => 'date',
                            'label' => $this->l('Date'),
                        ),
                        'fil' => array(
                            'id' => 'file',
                            'label' => $this->l('File'),
                        ),
                        'ref' => array(
                            'id' => 'referrence',
                            'label' => $this->l('Order reference'),
                        ),
                        'men' => array(
                            'id' => 'menu',
                            'label' => $this->l('Dropdown selections'),
                        ),
                        'che' => array(
                            'id' => 'checkbox',
                            'label' => $this->l('Checkboxes'),
                        ),
                        'rad' => array(
                            'id' => 'radio',
                            'label' => $this->l('Radio buttons'),
                        ),
                        'htm' => array(
                            'id' => 'html',
                            'label' => $this->l('HTML'),
                        ),
                        'qui' => array(
                            'id' => 'quiz',
                            'label' => $this->l('Quiz'),
                        ),
                        'acc' => array(
                            'id' => 'acceptance',
                            'label' => $this->l('Acceptance'),
                        ),
                        'rec' => array(
                            'id' => 'recaptcha',
                            'label' => $this->l('ReCaptcha'),
                            'enabled' => (int)Configuration::get('ETS_CFU_ENABLE_RECAPTCHA')
                        ),
                        'cap' => array(
                            'id' => 'captcha',
                            'label' => $this->l('Captcha'),
                        ),
                        'sub' => array(
                            'id' => 'submit',
                            'label' => $this->l('Submit'),
                        ),
                    );
                }
                return $this->_inputs;
            case 'tabs' :
                if (!(isset($this->_tabs)) || !$this->_tabs) {
                    $this->_tabs = array(
                        array(
                            'class_name' => 'AdminContactFormUltimateDashboard',
                            'tab_name' => $this->l('Contact dashboard'),
                            'icon' => 'icon icon-home'
                        ),
                        array(
                            'class_name' => 'AdminContactFormUltimateContactForm',
                            'tab_name' => $this->l('Contact forms'),
                            'icon' => 'icon icon-envelope-o'
                        ),
                        array(
                            'class_name' => 'AdminContactFormUltimateMessage',
                            'tab_name' => $this->l('Messages'),
                            'icon' => 'icon icon-comments',
                        ),
                        array(
                            'class_name' => 'AdminContactFormUltimateStatistics',
                            'tab_name' => $this->l('Statistics'),
                            'icon' => 'icon icon-line-chart',
                        ),
                        array(
                            'class_name' => 'AdminContactFormUltimateIpBlacklist',
                            'tab_name' => $this->l('IP and Email blacklist'),
                            'icon' => 'icon icon-user-times',
                        ),
                        array(
                            'class_name' => 'AdminContactFormUltimateSetting',
                            'tab_name' => $this->l('Setting'),
                            'icon' => 'icon icon-cog',
                            'children' => array(
                                array(
                                    'class_name' => 'AdminContactFormUltimateEmail',
                                    'tab_name' => $this->l('Email templates'),
                                    'icon' => 'icon icon-file-text-o',
                                ),
                                array(
                                    'class_name' => 'AdminContactFormUltimateImportExport',
                                    'tab_name' => $this->l('Import/Export'),
                                    'icon' => 'icon icon-exchange',
                                ),
                                array(
                                    'class_name' => 'AdminContactFormUltimateIntegration',
                                    'tab_name' => $this->l('Integration'),
                                    'icon' => 'icon icon-cogs',
                                ),
                            )
                        ),
                    );
                }
                return $this->_tabs;
        }
    }

    public function l($string)
    {
        return Translate::getModuleTranslation(_ETS_MODULE_, $string, pathinfo(__FILE__, PATHINFO_FILENAME));
    }

    public function display($template)
    {
        if (!$this->module)
            return;
        return $this->module->display($this->module->getLocalPath(), $template);
    }

    public function getBaseLink()
    {
        return (Configuration::get('PS_SSL_ENABLED_EVERYWHERE') ? 'https://' : 'http://') . $this->context->shop->domain . $this->context->shop->getBaseURI();
    }

}