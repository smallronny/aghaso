{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
* 
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs, please contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{extends file="helpers/form/form.tpl"}
{block name="fieldset"}
{assign var="current_tab" value=""}
{if isset($smarty.request.current_tab) && $smarty.request.current_tab}
{assign var="current_tab" value=$smarty.request.current_tab}
{/if}
{assign var="current_tab_email" value=""}
{if isset($smarty.request.current_tab_email) && $smarty.request.current_tab_email}
{assign var="current_tab_email" value=$smarty.request.current_tab_email}
{/if}
<input type="hidden" name="current_tab" value="{$current_tab|escape:'html':'utf-8'}" />
<input type="hidden" name="current_tab_email" value="{$current_tab_email|escape:'html':'utf-8'}" />
{$smarty.block.parent}
{/block}
{block name="label"}
	{if $ps15}
        {if $input.name=='ETS_CFU_ENABLE_RECAPTCHA'}
            <div class="ets_form_tab_header">
                <span class="active" data-tab="other_setting">{l s='Global settings' mod='ets_cfultimate'}</span>
                <span class="" data-tab="google">{l s='reCAPTCHA' mod='ets_cfultimate'}</span>
                <span class="" data-tab="black_list">{l s='IP & Email blacklist' mod='ets_cfultimate'}</span>
            </div>
            <div class="form-group-wapper">
            <div class="form-group form_group_contact google">
                <div class="col-lg-3">&nbsp;</div>
                <div class="col-lg-9 ">
                    <p class="alert alert-info">
                        <a target="_blank" href="https://www.google.com/recaptcha/intro/index.html">{l s='Google reCAPTCHA ' mod='ets_cfultimate'}</a>{l s='is a free service to protect your website from spam and abuse' mod='ets_cfultimate'}<br />
                        {l s='To use reCAPTCHA, you need to install an API key pair' mod='ets_cfultimate'}<br />

                    </p>
                </div>
            </div>
        {/if}
        {if $input.name=='title'}
        <div class="ets_form_tab_header">
                <span class="active" data-tab="form">{l s='Form' mod='ets_cfultimate'}</span>
                <span class="" data-tab="mail">{l s='Mail' mod='ets_cfultimate'}</span>
                <span class="" data-tab="message">{l s='Notifications' mod='ets_cfultimate'}</span>
                <span class="" data-tab="seo">{l s='Seo' mod='ets_cfultimate'}</span>
                <span class="" data-tab="general_settings">{l s='Settings' mod='ets_cfultimate'}</span>
        </div>
        <div class="form-group-wapper">
        {/if}
        {if $input.name=='email_to2'}
            <div class="form-group form_group_contact mail mail2">
                <div class="col-lg-3">&nbsp;</div>
                <div class="col-lg-9">
                    <p class="alert alert-info">
                        {l s='You can edit the mail template here. For details, see' mod='ets_cfultimate'} <a target="_blank" href="{$link_basic|escape:'html':'UTF-8'}/modules/ets_cfultimate/help/index.html#!/create">{l s='Create your first contact form' mod='ets_cfultimate'}</a>.<br />
                        {l s='In the following fields, you can use these mail-tags such as' mod='ets_cfultimate'}:
                        <span class="ets_cfu_tag_shortcode">
                            <span class="mailtag code unused">[your-name]</span>
                            <span class="mailtag code used">[your-email]</span>
                            <span class="mailtag code used">[your-subject]</span>
                            <span class="mailtag code used">[your-message]</span>
                        </span>
                    </p>
                </div>
            </div>
        {/if}
        {if $input.name=='message_mail_sent_ok'}
            <div class="form-group form_group_contact message">
                <div class="col-lg-3">&nbsp;</div>
                <div class="col-lg-9">
                    <p class="alert alert-info">
                        {l s='You can edit messages used in various situations here.' mod='ets_cfultimate'}
                    </p>
                </div>
            </div>
        {/if}
        {if $input.name=='title' && isset($fields_value['id_contact']) && $fields_value['id_contact']}
            <div class="form-group form_group_contact form"> 
                <div class="col-lg-3"></div>
                  <div class="col-lg-9">
                       <p class="alert alert-info">
                        {if isset($fields_value['id_contact']) && $fields_value['id_contact'] && $fields_value['link_contact']}
                        {l s='Form URL:' mod='ets_cfultimate'} <a target="_blank" href="{$fields_value['link_contact']|escape:'html':'UTF-8'}">{$fields_value['link_contact']|escape:'html':'UTF-8'}</a><br />
                        {/if}
                        {if !isset($showShortcodeHook) || (isset($showShortcodeHook)  && $showShortcodeHook)}
                            {l s='Contact form shortcode: ' mod='ets_cfultimate'}<span title="{l s='Click to copy' mod='ets_cfultimate'}" style="position: relative;display: inline-block; vertical-align: middle;"><input type="text" class="ctf-short-code" value='[contact-form-7 id="{$fields_value['id_contact']|intval}"]'/><span class="text-copy">{l s='Copied' mod='ets_cfultimate'}</span></span><br/>
                            {l s='Copy the shortcode above, paste into anywhere on your product description, CMS page content, tpl files, etc. in order to display this contact form' mod='ets_cfultimate'}
                            <br/>
                            {l s='Besides using shortcode to display the contact form, you can also display the contact form using a custom hook. Copy this custom hook' mod='ets_cfultimate'}
                            <span title="{l s='Click to copy' mod='ets_cfultimate'}" style="position: relative;display: inline-block; vertical-align: middle;">
                            <input style="width: 305px ! important;" class="ctf-short-code" type="text" value='{literal}{hook h="displayContactFormUltimate" id="{/literal}{$fields_value.id_contact|intval}{literal}"}{/literal}' /><span class="text-copy">{l s='Copied' mod='ets_cfultimate'}</span></span>
                            {l s=', place into your template .tpl files where you want to display the contact form' mod='ets_cfultimate'}
                        {/if}
                       </p>
                  </div>
             </div>
        {/if}
        <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
    {/if}
    {$smarty.block.parent}
{/block}
{block name="field"}
    {$smarty.block.parent}
    {if $ps15}
        </div>
        {if $input.name=='ETS_CFU_NUMBER_MESSAGE'}
            <div class="form-group form_group_contact export_import">
                <div class="ctf_export_form_content">            
                    <div class="ctf_export_option">
                        <div class="export_title">{l s='Export contact forms' mod='ets_cfultimate'}</div>
                        <p>{l s='Export form configurations of all contact forms of the current shop that you are viewing' mod='ets_cfultimate'}</p>
                        <a target="_blank" href="{$link->getAdminlink('AdminModules',true)|escape:'html':'UTF-8'}&configure=ets_cfultimate&tab_module=front_office_features&module_name=ets_cfultimate&exportContactForm=1" class="btn btn-default mm_export_menu">
                            <i class="fa fa-download"></i>{l s='Export contact forms' mod='ets_cfultimate'}
                        </a>
                    </div>                       
                    <div class="ctf_import_option">
                        <div class="export_title">{l s='Import contact forms' mod='ets_cfultimate'}</div>
                        <p>{l s='Import contact forms to the current shop that you are viewing for quick configuration. This is useful when you need to migrate contact forms between websites' mod='ets_cfultimate'}</p>
                            <div class="ctf_import_option_updata">
                                <label for="contactformdata">{l s='Data file' mod='ets_cfultimate'}</label>
                                <input type="file" name="contactformdata" id="contactformdata" />
                            </div>
                            <div class="cft_import_option_clean">
                                <input type="checkbox" name="importdeletebefore" id="importdeletebefore" value="1" />
                                <label for="importdeletebefore">{l s='Delete all contact forms before importing' mod='ets_cfultimate'}</label>
                            </div>
                            <div class="cft_import_option_clean">
                                <input type="checkbox" name="importoverride" id="importoverride" value="1" />
                                <label for="importoverride">{l s='Override all forms with the same IDs' mod='ets_cfultimate'}</label>
                            </div>
                            <div class="cft_import_option_button">
                                <input type="hidden" value="1" name="importContactform" />
                                <div class="cft_import_contact_submit">
                                    <i class="fa fa-compress"></i>
                                    <input type="submit" class="btn btn-default cft_import_menu" name="cft_import_contact_submit" value="{l s='Import contact forms' mod='ets_cfultimate'}" />
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        {/if}
        {if $input.name=='message_captcha_not_match' || $input.name=='ETS_CFU_NUMBER_MESSAGE'}
            </div>
        {/if}
    {/if}
{/block}
{block name="input_row"}
{if $input.name=='ETS_CFU_ENABLE_RECAPTCHA'}
    <div class="ets_form_tab_header">
        <span class="{if $current_tab == 'other_setting'  || !$current_tab}active{/if}" data-tab="other_setting">{l s='Global settings' mod='ets_cfultimate'}</span>
        <span class="{if $current_tab == 'google'}active{/if}" data-tab="google">{l s='reCAPTCHA' mod='ets_cfultimate'}</span>
    </div>
    <div class="form-group-wapper">
    <div class="form-group form_group_contact google">
        <div class="col-lg-3">&nbsp;</div>
        <div class="col-lg-9 ">
            <p class="alert alert-info">
                <a target="_blank" href="https://www.google.com/recaptcha/intro/index.html">{l s='Google reCAPTCHA ' mod='ets_cfultimate'}</a>{l s='is a free service to protect your website from spam and abuse' mod='ets_cfultimate'}<br />
                {l s='To use reCAPTCHA, you need to install an API key pair' mod='ets_cfultimate'}<br />
            </p>
        </div>
    </div>
{/if}
{if $input.name=='short_code'}
    <div class="ets_form_tab_header">
        <span class="{if $current_tab == 'seo' || !$current_tab}active{/if}" data-tab="seo">{l s='Info' mod='ets_cfultimate'}</span>
        <span class="{if $current_tab == 'form'}active{/if}" data-tab="form">{l s='Form' mod='ets_cfultimate'}</span>
        <span class="{if $current_tab == 'mail'}active{/if}" data-tab="mail">{l s='Mail' mod='ets_cfultimate'}</span>
        <span class="{if $current_tab == 'message'}active{/if}" data-tab="message">{l s='Notifications' mod='ets_cfultimate'}</span>
        <span class="{if $current_tab == 'thank_you'}active{/if}" data-tab="thank_you">{l s='Thank you page' mod='ets_cfultimate'}</span>
        <span class="{if $current_tab == 'general_settings'}active{/if}" data-tab="general_settings">{l s='Settings' mod='ets_cfultimate'}</span>
    </div>
    <div class="form-group-wapper">
{/if}
{if $input.name=='email_to'}
    <div class="form-group form_group_contact mail menu">
        <ul class="ets_cfu_mail_menu">
            <li class="ets_cfu_item mail1 {if $current_tab_email == 'mail1' || !$current_tab_email}active{/if}" data-tab="mail1">
                <span>{l s='Email to admin' mod='ets_cfultimate'}</span>
            </li>
            <li class="ets_cfu_item mail2 {if $current_tab_email == 'mail2'}active{/if}" data-tab="mail2">
                <span>{l s='Auto responder' mod='ets_cfultimate'}</span>
            </li>
        </ul>
    </div>
    <div class="ets_cfu_block_short_code form_group_contact mail hide">
        <h3 class="ets_cfu_title">{l s='Available mail-tags' mod='ets_cfultimate'}</h3>
        <p class="ets_cfu_desc">{l s='Copy mail-tags below and paste into any configuration fields of the "Email to admin" and "Auto responder" to get form input value.' tags=['<br>','<span class="ets_cfu_example">'] mod='ets_cfultimate'}</p>
        <ul class="ets_cfu_block_ul" data-title="{l s='Click to copy' mod='ets_cfultimate'}"></ul>
    </div>
{/if}
{if $input.name=='message_mail_sent_ok'}
    <div class="form-group form_group_contact message">
        <div class="col-lg-3">&nbsp;</div>
        <div class="col-lg-9">
            <p class="alert alert-info">
                {l s='You can edit notification messages used in various situations here.' mod='ets_cfultimate'}
            </p>
        </div>
    </div>
{/if}
{if $input.name == 'enable_form_page' && isset($fields_value['id_contact']) && $fields_value['id_contact']}
    <div class="form-group form_group_contact seo">
        <div class="col-lg-3"></div>
        <div class="col-lg-9">
            <p class="alert alert-info">
            {if isset($fields_value['id_contact']) && $fields_value['id_contact'] && $fields_value['link_contact']}
            {l s ='Form URL:' mod='ets_cfultimate'} <a target="_blank" href="{$fields_value['link_contact']|escape:'html':'UTF-8'}">{$fields_value['link_contact']|escape:'html':'UTF-8'}</a><br />
            {/if}
            {if !isset($showShortcodeHook) || (isset($showShortcodeHook)  && $showShortcodeHook)}
                {l s='Contact form shortcode: ' mod='ets_cfultimate'}<span title="{l s='Click to copy' mod='ets_cfultimate'}" style="position: relative;display: inline-block; vertical-align: middle;">
                    <input type="text" class="ctf-short-code" value='[contact-form-7 id="{$fields_value['id_contact']|intval}"]'/><span class="text-copy">{l s='Copied' mod='ets_cfultimate'}</span></span><br/>
                {l s='Copy the shortcode above, paste into anywhere on your product description, CMS page content, tpl files, etc. in order to display this contact form' mod='ets_cfultimate'}
                <br />
                {l s='Besides using shortcode to display the contact form, you can also display the contact form using a custom hook. Copy this custom hook' mod='ets_cfultimate'}
                <span title="{l s='Click to copy' mod='ets_cfultimate'}" style="position: relative;display: inline-block; vertical-align: middle;">
                <input style="width: 305px ! important;" class="ctf-short-code" type="text" value='{literal}{hook h="displayContactFormUltimate" id="{/literal}{$fields_value.id_contact|intval}{literal}"}{/literal}' /><span class="text-copy">{l s='Copied' mod='ets_cfultimate'}</span></span>
                {l s=', place into your template .tpl files where you want to display the contact form' mod='ets_cfultimate'}
            {/if}
            </p>
        </div>
    </div>
{/if}
{if $input.name=='short_code'}
    <div class="form-group form_group_contact form ets_cfu_add_contact" data-multi-lang="{$languages|count > 1|intval}" data-default-lang="{$defaultFormLanguage|intval}" style="display: none;">
        <div class="ets_cfu_add_form_contact">
            {assign var="is_render_form" value=(isset($fields_value.render_form) && $fields_value.render_form)}
            <div class="ets_cfu_add_form">
                {if $is_render_form}{$fields_value.render_form nofilter}{/if}
            </div>
            <div class="ets_cfu_form_empty"{if $is_render_form} style="display: none;"{/if}>
                <h4 class="ets_cfu_form_title">{l s='Contact form is blank' mod='ets_cfultimate'}</h4>
                <a class="ets_cfu_add_input first_item" href="javascript:void(0)">{l s='Add new input field/row to create your contact form' mod='ets_cfultimate'}</a>
            </div>
            <div class="ets_cfu_action">
               <button class="ets_cfu_add_input btn" name="ets_cfu_add_input btn"><i class="icon-plus-circle"></i>&nbsp;{l s='Add input field' mod='ets_cfultimate'}</button>
               <button class="ets_cfu_add_row btn" name="ets_cfu_add_row btn"><i class="icon-bars"></i>&nbsp;{l s='Add row' mod='ets_cfultimate'}</button>
            </div>
        </div>
        <div class="ets_cfu_form_popup">
            <div class="ets_cfu_table">
                <div class="ets_cfu_table_cell">
                    <div class="ets_cfu_wrapper">
                        <span class="ets_cfu_close_popup" title="{l s='Close' mod='ets_cfultimate'}">{l s='Close' mod='ets_cfultimate'}</span>
                        <div class="ets_cfu_form_load"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ets_cfu_form_group">
            <div class="ets_cfu_row_group">
                {include file="./rows.tpl"}
            </div>
            <div class="ets_cfu_input_group">
                {include file="./inputs.tpl"}
            </div>
        </div>
    </div>
{/if}
{$smarty.block.parent}
{if $input.name=='thank_you_url' || $input.name=='ETS_CFU_NUMBER_MESSAGE'}
    </div>
{/if}
{/block}

{block name="input"}
{if $input.type == 'switch'}
    {if isset($input.values) && $input.values}
    <span class="switch prestashop-switch fixed-width-lg">
        {foreach $input.values as $value}
        <input type="radio" name="{$input.name|escape:'html':'UTF-8'}"{if $value.value == 1} id="{$input.name|escape:'html':'UTF-8'}_on"{else} id="{$input.name|escape:'html':'UTF-8'}_off"{/if} value="{$value.value|escape:'html':'UTF-8'}"{if $fields_value[$input.name] == $value.value} checked="checked"{/if}{if (isset($input.disabled) && $input.disabled) or (isset($value.disabled) && $value.disabled)} disabled="disabled"{/if}/>
        {strip}<label {if $value.value == 1} for="{$input.name|escape:'html':'UTF-8'}_on"{else} for="{$input.name|escape:'html':'UTF-8'}_off"{/if}>
            {if $value.value == 1}
                {l s='Yes' d='Admin.Global' mod='ets_cfultimate'}
            {else}
                {l s='No' d='Admin.Global' mod='ets_cfultimate'}
            {/if}
        </label>{/strip}
        {/foreach}
        <a class="slide-button btn"></a>
    </span>
    {/if}
{elseif isset($input.multi) && $input.multi}
    <ul class="ets_cfu_ul{if isset($input.mail_tag) && $input.mail_tag} mail-tag{/if} {$input.name|lower|escape:'UTF-8'} form-group" data-ul="{$input.name|lower|escape:'UTF-8'}">
        {assign var="ik" value=0}
        {assign var="key" value='multi_'|cat:$input.name}
        {assign var="end" value=-1}
        {if isset($fields_value.$key) && $fields_value.$key}
            {assign var="end" value=$fields_value.$key|count - 1}
            {foreach from=$fields_value.$key item='email'}
                {include file="./email.tpl" element = $email}
                {assign var="ik" value=$ik+1}
            {/foreach}
        {/if}
        {if (isset($input.show_btn_add) && $input.show_btn_add && $ik >= $end) || (empty($fields_value.$key) && $ik > $end)}{include file="./email.tpl"}{/if}
    </ul>
    {$smarty.block.parent}
{elseif $input.type == 'group'}
    {if count($input.values) && isset($input.values)}
    <div class="row">
        <div class="col-lg-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="fixed-width-xs">
                            <span class="title_box">
                                <input type="checkbox" name="checkme" id="checkme" onclick="checkDelBoxes(this.form, '{$input.name|escape:'html':'UTF-8'}[]', this.checked)" />
                            </span>
                        </th>
                        <th class="fixed-width-xs"><span class="title_box">{l s='ID' d='Admin.Global' mod='ets_cfultimate'}</span></th>
                        <th>
                            <span class="title_box">
                                {l s='Group name' mod='ets_cfultimate'}
                            </span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                {foreach $input.values as $key => $group}
                    <tr>
                        <td>
                            {assign var=id_checkbox value=$group['id_group']}
                            <input type="checkbox" name="{$input.name|escape:'html':'UTF-8'}[]" class="groupBox" id="{$id_checkbox}" value="{$group['id_group']}" {if ! $input.is_id}checked="checked"{else}{if in_array($group['id_group'], $input.custommer_access)}checked="checked"{/if}{/if} />
                        </td>
                        <td>{$group['id_group']|escape:'html':'UTF-8'}</td>
                        <td>
                            <label for="{$id_checkbox|escape:'html':'UTF-8'}">{$group['name']|escape:'html':'UTF-8'}</label>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
    {else}
    <p>
        {l s='No group created' mod='ets_cfultimate'}
    </p>
    {/if}
{elseif $input.type == 'textarea' && $ps15}
    {if isset($input.lang) AND $input.lang && $languages|count > 1}
        <div class="translatable translatable-field">
            {foreach $languages as $language}
                <div class="lang_{$language.id_lang|intval}" id="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}" style="display:{if $language.id_lang == $defaultFormLanguage}block{else}none{/if}; float: left;">
                    <textarea cols="{$input.cols|escape:'html':'UTF-8'}" rows="{$input.rows|escape:'html':'UTF-8'}" name="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}" class="{if isset($input.autoload_rte) && $input.autoload_rte}rte autoload_rte{/if} {if isset($input.class)}{$input.class|escape:'html':'UTF-8'}{/if}" >{$fields_value[$input.name][$language.id_lang]|escape:'htmlall':'UTF-8'}</textarea>
                </div>
            {/foreach}
        </div>
    {else}
        <textarea name="{$input.name|escape:'html':'UTF-8'}" id="{if isset($input.id)}{$input.id|escape:'html':'UTF-8'}{else}{$input.name|escape:'html':'UTF-8'}{/if}" cols="{$input.cols|escape:'html':'UTF-8'}" rows="{$input.rows|escape:'html':'UTF-8'}" {if isset($input.autoload_rte) && $input.autoload_rte}class="rte autoload_rte {if isset($input.class)}{$input.class|escape:'html':'UTF-8'}{/if}"{/if}>{$fields_value[$input.name]|escape:'htmlall':'UTF-8'}</textarea>
    {/if}
{else}
    {$smarty.block.parent}
{/if}
{if $input.name=='title_alias'&& isset($fields_value['id_contact']) && $fields_value['id_contact']}
    <div class="col-lg-9">
        {if $languages|count > 1}
            {foreach from=$languages item='language'}
                <div class="translatable-field lang-{$language.id_lang|intval}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
                    <i class="ets_cfu_page_url">{l s='Form page url:' mod='ets_cfultimate'}</i>&nbsp;<a class="ets_cfu_page_url" target="_blank" href="{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval,$language['id_lang']|intval)}">{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval,$language['id_lang']|intval)}</a>
                </div>
            {/foreach}
        {else}
            <i class="ets_cfu_page_url">{l s='Form page url:' mod='ets_cfultimate'}</i>&nbsp;<a class="ets_cfu_page_url" target="_blank" href="{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval)}">{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval)}</a>
        {/if}
    </div>
{/if}
{if $input.name=='thank_you_alias'&& isset($fields_value['id_contact']) && $fields_value['id_contact']}
    <div class="col-lg-9">
        {if $languages|count > 1}
            {foreach from=$languages item='language'}
                <div class="translatable-field lang-{$language.id_lang|intval}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
                    <i class="ets_cfu_page_url">{l s='Form page url:' mod='ets_cfultimate'}</i>&nbsp;<a class="ets_cfu_page_url" target="_blank" href="{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval,$language['id_lang']|intval,'thank')}">{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval,$language['id_lang']|intval,'thank')}</a>
                </div>
            {/foreach}
        {else}
            <i class="ets_cfu_page_url">{l s='Form page url:' mod='ets_cfultimate'}</i>&nbsp;<a class="ets_cfu_page_url" target="_blank" href="{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval,0,'thank')}">{ets_cfultimate::getLinkContactForm($fields_value['id_contact']|intval,0,'thank')}</a>
        {/if}
    </div>
{/if}
{/block}

{block name="legend"}
<div class="panel-heading">
	{if isset($field.image) && isset($field.title)}<img src="{$field.image|escape:'html':'UTF-8'}" alt="{$field.title|escape:'html':'UTF-8'}" />{/if}
	{if isset($field.icon)}<i class="{$field.icon|escape:'html':'UTF-8'}"></i>{/if}
	{if isset($field.title)}{$field.title|escape:'html':'UTF-8'}{/if}
    {if isset($field.new) && $field.new && (!isset($smarty.get.etsCfuEditContact) && !isset($smarty.get.etsCfuAddContact))}
        <span class="panel-heading-action">
            <a id="desc-contactform-new" class="list-toolbar-btn" href="{$field.new|escape:'html':'UTF-8'}" title="{l s='Add new' mod='ets_cfultimate'}">
                <span class="label-tooltip" data-toggle="tooltip" data-original-title="{l s='Add new' mod='ets_cfultimate'}" data-html="true" data-placement="top" title="{l s='Add new' mod='ets_cfultimate'}">
                    <i class="process-icon-new"></i>
                </span>
            </a>
        </span>
    {/if}
</div>
{/block}

{block name="description"}
	{if isset($input.desc) && !empty($input.desc)}
		<p class="help-block">
			{if is_array($input.desc)}
				{foreach $input.desc as $p}
					{if is_array($p)}
						<span id="{$p.id|escape:'html':'UTF-8'}">{$p.text|escape:'html':'UTF-8'}</span>
					{else}
						{$p|escape:'html':'UTF-8'}
					{/if}
				{/foreach}
			{else}
				{$input.desc nofilter}
			{/if}
		</p>
	{/if}
{/block}