{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{hook h='contactFormUltimateTopBlock'}
<div class="ets_cfu_errors">
    {if isset($errors) && $errors}{$errors nofilter}{/if}
</div>
{assign var='controller' value = $smarty.get.controller}
{assign var="wrapTab" value=($controller == 'AdminContactFormUltimateEmail' || $controller == 'AdminContactFormUltimateImportExport' || $controller == 'AdminContactFormUltimateIntegration')}
{if $wrapTab}
<div class="ets_cfu_wrapper">
    <div class="ets_cfu_group_wrapper">
        <h3 class="ets_cfu_title">{l s='Settings' mod='ets_cfultimate'}</h3>
        <ul class="ets_cfu_menu_left">
            <li{if $controller=='AdminContactFormUltimateEmail'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateEmail',true)|escape:'html':'UTF-8'}"><i class="icon icon-file-text-o"> </i> {l s='Email templates' mod='ets_cfultimate'}</a></li>
            <li{if $controller=='AdminContactFormUltimateImportExport'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateImportExport',true)|escape:'html':'UTF-8'}"><i class="icon icon-exchange"> </i> {l s='Import/Export' mod='ets_cfultimate'}</a></li>
            <li{if $controller=='AdminContactFormUltimateIntegration'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateIntegration',true)|escape:'html':'UTF-8'}"><i class="icon icon-cogs"> </i> {l s='Integration' mod='ets_cfultimate'}</a></li>
        </ul>
        <div class="ets_cfu_setting_wrapper">{/if}
            <div class="cfu-content-block" style="display: none;">
                {$form_config nofilter}
            </div>
            {if $wrapTab}
        </div>
    </div>
</div>{/if}