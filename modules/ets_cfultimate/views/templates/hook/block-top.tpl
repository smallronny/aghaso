{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
<script type="text/javascript">
    var ets_cfu_default_lang = {$ets_cfu_default_lang|intval};
    var ets_cfu_is_updating = {$ets_cfu_is_updating|intval};
    var PS_ALLOW_ACCENTED_CHARS_URL = true;
    var detele_confirm = "{l s='Do you want to delete?' mod='ets_cfultimate'}";
    var ets_cfu_msg_email_required = "{l s='Email is required.' mod='ets_cfultimate'}";
    var ets_cfu_msg_email_invalid = "{l s='Email %s is invalid.' mod='ets_cfultimate'}";
    var ets_cfu_msg_email_exist = "{l s='This email is exist. Please enter another email address.' mod='ets_cfultimate'}";
    var ets_cfu_label_delete = "{l s='Delete' mod='ets_cfultimate'}";
    var ets_cfu_copy_msg = "{l s='Copied' mod='ets_cfultimate'}";
    var ets_cfu_delete_msg = "{l s='Do you want to delete this item?' mod='ets_cfultimate'}";
    var ets_cfu_btn_back_label = "{l s='Back' mod='ets_cfultimate'}";
    var ets_cfu_btn_close_label = "{l s='Close' mod='ets_cfultimate'}";
    var ets_cfu_add_input_field = "{l s='Add input field:' mod='ets_cfultimate'}";
    var ets_cfu_edit_input_field = "{l s='Edit input field:' mod='ets_cfultimate'}";
    var ets_cfu_add_row_title = "{l s='Add row' mod='ets_cfultimate'}";
    var ets_cfu_edit_row_title = "{l s='Edit row' mod='ets_cfultimate'}";
    var ets_cfu_languages = {$languages|json_encode};
</script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}contact_form7_admin.js"></script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}other.js"></script>
<div class="cfu-top-menu">
    <ul>
        <li{if $controller=='AdminContactFormUltimateDashboard'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateDashboard',true)|escape:'html':'UTF-8'}"><i class="icon icon-desktop"> </i> {l s='Dashboard' mod='ets_cfultimate'}</a></li>
        <li{if $controller=='AdminContactFormUltimateContactForm' || isset($smarty.request.etsCfuEditContact) || isset($smarty.request.etsCfuAddContact)} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateContactForm',true)|escape:'html':'UTF-8'}"><i class="icon icon-envelope-o"> </i> {l s='Contact forms' mod='ets_cfultimate'}</a></li>
        <li{if $controller=='AdminContactFormUltimateMessage'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateMessage',true)|escape:'html':'UTF-8'}"><i class="icon icon-comments"> </i> {l s='Messages' mod='ets_cfultimate'}&nbsp;<span class="count_messages {if !$count_messages}hide{/if}">{$count_messages|intval}</span></a></li>
        <li{if $controller=='AdminContactFormUltimateStatistics'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateStatistics',true)|escape:'html':'UTF-8'}"><i class="icon icon-area-chart"> </i> {l s='Statistics' mod='ets_cfultimate'}</a></li>
        <li{if $controller=='AdminContactFormUltimateIpBlacklist'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateIpBlacklist', true)|escape:'html':'UTF-8'}"><i class="icon icon-user-times"> </i> {l s='IP & Email blacklist' mod='ets_cfultimate'}</a></li>
        <li{if $controller=='AdminContactFormUltimateEmail' ||  $controller=='AdminContactFormUltimateImportExport' || $controller=='AdminContactFormUltimateIntegration'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateEmail',true)|escape:'html':'UTF-8'}"><i class="icon icon-cog"> </i> {l s='Settings' mod='ets_cfultimate'}</a></li>
        <li{if $controller=='AdminContactFormUltimateHelp'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateHelp',true)|escape:'html':'UTF-8'}"><i class="icon icon-question-circle"> </i> {l s='Help' mod='ets_cfultimate'}</a></li>
        {if isset($intro) && $intro}
        <li class="li_othermodules ">
            <a class="{if isset($refsLink) && $refsLink}refs_othermodules{else}link_othermodules{/if}" href="{$other_modules_link|escape:'html':'UTF-8'}" {if isset($refsLink) && $refsLink}target="_blank" {/if}>
                <span class="tab-title">{l s='Other modules' mod='ets_cfultimate'}</span>
                <span class="tab-sub-title">{l s='Made by ETS-Soft' mod='ets_cfultimate'}</span>
            </a></li>
        {/if}
    </ul>
</div>
<div class="cfu-top-menu-height"></div>
