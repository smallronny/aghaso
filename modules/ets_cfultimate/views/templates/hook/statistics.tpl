{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
<script type="text/javascript">
    var text_add_to_black_list = '{l s='Add IP address to blacklist successful' js='1' mod='ets_cfultimate' }';
    var detele_log = '{l s='If you clear "View log", view chart will be reset. Do you want to do that?' js='1' mod='ets_cfultimate' }';
    var ets_cfu_x_days = '{l s='Day' mod='ets_cfultimate'}';
    var ets_cfu_x_months = '{l s='Month' mod='ets_cfultimate'}';
    var ets_cfu_x_years = '{l s='Year' mod='ets_cfultimate'}';
    var ets_cfu_y_label = '{l s='Count' mod='ets_cfultimate'}';
    var ets_cfu_lc_title = '{l s='Statistics' mod='ets_cfultimate'}';
    var ets_cfu_line_chart = '{$ets_cfu_line_chart|json_encode}';
    var ets_cfu_lc_labels = '{$ets_cfu_lc_labels|json_encode}';
    var ets_cfu_y_max = {$y_max_value|intval};
</script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}chart.js"></script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}common.js"></script>
<script type="text/javascript" src="{$ets_cfu_js_dir_path|escape:'quotes':'UTF-8'}statistics.js"></script>
{hook h='contactFormUltimateTopBlock'}
<div class="cfu-content-block">
    <div class="panel statics_form">
        <div class="panel-heading">
            <i class="icon icon-line-chart fa fa-line-chart"></i> {l s='Statistics' mod='ets_cfultimate'}
        </div>
        <div class="form-wrapper">
            <div class="ets_form_tab_header">
                <span {if $cfu_tab_ets=='chart'}class="active"{/if} data-tab="chart">{l s='Chart' mod='ets_cfultimate'}</span>
                <span {if $cfu_tab_ets=='view-log'}class="active"{/if}  data-tab="view-log">{l s='Views log' mod='ets_cfultimate'}</span>
            </div>
            <div class="form-group-wapper">
                <div class="ets_cfu_admin_statistic form-group form_group_contact chart">
                    <div class="ets_cfu_admin_chart">
                        <div class="ets_cfu_line_chart">
                            <canvas id="ets_cfu_line_chart" style="width:100%; height: 500px;"></canvas>
                        </div>
                    </div>
                    <div class="ets_cfu_admin_filter">
                        <form id="ets_cfu_admin_filter_chart" class="defaultForm form-horizontal"
                              action="{$action|escape:'quotes'}" enctype="multipart/form-data" method="POST">
                            <div class="ets_cfu_admin_filter_chart_settings">
                                <div class="ets_cfu_admin_filter_cotactform">
                                    <label>{l s='Contact form' mod='ets_cfultimate'}</label>
                                    <select id="ets_cfu_id_contact" name="id_contact" class="form-control">
                                        <option value=""{if !$sl_contact} selected="selected"{/if}>{l s='All contact form' mod='ets_cfultimate'}</option>
                                        {foreach from=$ets_cfu_contacts item=contact}
                                            <option value="{$contact.id_contact|intval}" {if $sl_contact == $contact.id_contact} selected="selected"{/if}>{$contact.title|escape:'html':'utf-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="ets_cfu_admin_filter_date">
                                    <label>{l s='Month' mod='ets_cfultimate'}</label>
                                    <select id="ets_cfu_months" name="ets_cfu_months" class="form-control">
                                        <option value="" {if !$sl_month} selected="selected"{/if}>{l s='All' mod='ets_cfultimate'}</option>
                                        {foreach from=$ets_cfu_months key=k item=month}
                                            <option value="{$k|intval}"{if $sl_month == $k} selected="selected"{/if}>{l s=$month mod='ets_cfultimate'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="ets_cfu_admin_filter_date">
                                    <label>{l s='Year' mod='ets_cfultimate'}</label>
                                    <select id="ets_cfu_years" name="ets_cfu_years" class="form-control">
                                        <option value="" {if !$sl_year} selected="selected"{/if}>{l s='All' mod='ets_cfultimate'}</option>
                                        {foreach from=$ets_cfu_years item=year}
                                            <option value="{$year|intval}" {if $sl_year == $year} selected="selected"{/if}>{$year|intval}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="ets_cfu_admin_filter_button">
                                    <button name="etsCfuSubmitFilterChart" class="btn btn-default"
                                            type="submit">{l s='Filter' mod='ets_cfultimate'}</button>
                                    {if $ets_cfu_show_reset}
                                        <a href="{$action|escape:'quotes'}"
                                           class="btn btn-default">{l s='Reset' mod='ets_cfultimate'}</a>
                                    {/if}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="ets_cfu_admin_log form-group form_group_contact view-log">
                    {if $ets_cfu_logs}
                        <table id="table-log" class="table log">
                            <thead>
                            <tr class="nodrag nodrop">
                                <th>{l s='IP address' mod='ets_cfultimate'}</th>
                                <th>{l s='Browser' mod='ets_cfultimate'}</th>
                                <th>{l s='Customer' mod='ets_cfultimate'}</th>
                                <th>{l s='Contact form' mod='ets_cfultimate'}</th>
                                <th>{l s='Date' mod='ets_cfultimate'}</th>
                                <th>{l s='Action' mod='ets_cfultimate'}</th>
                            </tr>
                            </thead>
                            <tbody id="list-ets_cfu_logs">
                            {foreach from=$ets_cfu_logs item='log'}
                                <tr>
                                    <td>{$log.ip|escape:'html':'UTF-8'}</td>
                                    <td>
                                        <span class="browser-icon {$log.class|escape:'html':'UTF-8'}"></span> {$log.browser|escape:'html':'UTF-8'}
                                    </td>
                                    <td>{if $log.id_customer}<a
                                            href="{$link->getAdminLink('AdminCustomers')|escape:'html':'UTF-8'}"
                                            >{$log.firstname|escape:'html':'UTF-8'}
                                            &nbsp;{$log.lastname|escape:'html':'UTF-8'}</a>{else}--{/if}</td>
                                    <td>
                                        {if $log.enable_form_page}
                                        <a href="{Ets_CfUltimate::getLinkContactForm($log.id_contact|intval)|escape:'html':'UTF-8'}"
                                           class="dropdown-item product-edit" target="_blank"
                                           >
                                            {/if}
                                            {$log.title|escape:'html':'UTF-8'}
                                            {if $log.enable_form_page}
                                        </a>
                                        {/if}
                                    </td>
                                    <td>{$log.datetime_added|escape:'html':'UTF-8'}</td>
                                    <td class="statitics_form_action">
                                        <a class="btn btn-default view_location"
                                           href="https://www.infobyip.com/ip-{$log.ip|escape:'html':'UTF-8'}.html"
                                           target="_blank">{l s='View location' mod='ets_cfultimate'}</a>
                                        {if !$log.black_list}
                                            <a class="btn btn-default etsCfuAddToBlackList " data-ip="{$log.ip|escape:'html':'UTF-8'}" href="{$action|escape:'quotes'}&etsCfuAddToBlackList={$log.ip|escape:'html':'UTF-8'}">{l s='Add to blacklist' mod='ets_cfultimate'}</a>
                                        {else}
                                            <span><i class="icon icon-user-times"></i></span>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <form action="{$action|escape:'quotes'}" enctype="multipart/form-data" method="POST">
                            <input type="hidden" value="1" name="etsCfuClearLogSubmit"/>
                            <div class="ets_pagination">
                                {$ets_cfu_pagination_text nofilter}
                            </div>
                            <button class="clear-log btn btn-default" type="submit"
                                    name="etsCfuClearLogSubmit">{l s='Clear all view logs' mod='ets_cfultimate'}</button>
                        </form>
                    {else}
                        {l s='No views log' mod='ets_cfultimate'}
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>