{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
<div class="ets_cfu_btn_input">
    <span class="settings_icon" title="{l s='Setting' mod='ets_cfultimate'}"><i class="fa fa-cog"></i></span>
    <div class="settings_icon_content">
        <span class="ets_cfu_btn_edit_input" title="{l s='Edit' mod='ets_cfultimate'}"><i class="fa fa-pencil-square-o"></i>&nbsp;{l s='Edit' mod='ets_cfultimate'}</span>
        <span class="ets_cfu_btn_copy_input" title="{l s='Duplicate' mod='ets_cfultimate'}"><i class="fa fa-files-o"></i>&nbsp;{l s='Duplicate' mod='ets_cfultimate'}</span>
        <span class="ets_cfu_btn_delete_input" title="{l s='Delete' mod='ets_cfultimate'}"><i class="fa fa-trash-o"></i>&nbsp;{l s='Delete' mod='ets_cfultimate'}</span>
    </div>
</div>