{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{hook h='contactFormUltimateTopBlock'}
<div class="ets_cfu_errors">
    {if isset($errors) && $errors}{$errors nofilter}{/if}
</div>
{assign var='controller' value = $smarty.get.controller}
{assign var="wrapTab" value=($controller == 'AdminContactFormUltimateEmail' || $controller == 'AdminContactFormUltimateImportExport' || $controller == 'AdminContactFormUltimateIntegration')}
<div class="ets_cfu_wrapper">
    <div class="ets_cfu_group_wrapper">
        <h3 class="ets_cfu_title">{l s='Settings' mod='ets_cfultimate'}</h3>
        <ul class="ets_cfu_menu_left">
            <li{if $controller=='AdminContactFormUltimateEmail'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateEmail',true)|escape:'html':'UTF-8'}"><i class="icon icon-file-text-o"> </i> {l s='Email templates' mod='ets_cfultimate'}</a></li>
            <li{if $controller=='AdminContactFormUltimateImportExport'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateImportExport',true)|escape:'html':'UTF-8'}"><i class="icon icon-exchange"> </i> {l s='Import/Export' mod='ets_cfultimate'}</a></li>
            <li{if $controller=='AdminContactFormUltimateIntegration'} class="active"{/if}><a href="{$link->getAdminLink('AdminContactFormUltimateIntegration',true)|escape:'html':'UTF-8'}"><i class="icon icon-cogs"> </i> {l s='Integration' mod='ets_cfultimate'}</a></li>
        </ul>
        <div class="ets_cfu_setting_wrapper">
            <div class="cfu-content-block">
                <form id="module_form" class="defaultForm form-horizontal" novalidate="" enctype="multipart/form-data" method="post"
                      action="">
                    <div id="fieldset_0" class="panel">
                        <div class="panel-heading"><i class="icon-exchange"></i>&nbsp;
                            {l s='Import/export' mod='ets_cfultimate'}</div>
                        <div class="form-wrapper">

                                    <div class="form-group export_import">
                                        <div class="ctf_export_form_content">
                                            <div class="ctf_export_option">
                                                <div class="export_title">{l s='Export contact forms' mod='ets_cfultimate'}</div>
                                                <p>{l s='Export form configurations of all contact forms of the current shop that you are viewing' mod='ets_cfultimate'}</p>
                                                <a href="{$link->getAdminlink('AdminModules',true)|escape:'html':'UTF-8'}&configure=ets_cfultimate&tab_module=front_office_features&module_name=ets_cfultimate&etsCfuExportContactForm=1"
                                                   class="btn btn-default mm_export_menu">
                                                    <i class="fa fa-download"></i>{l s='Export contact forms' mod='ets_cfultimate'}
                                                </a>
                                            </div>
                                            <div class="ctf_import_option">
                                                <div class="export_title">{l s='Import contact forms' mod='ets_cfultimate'}</div>
                                                <p>{l s='Import contact forms to the current shop that you are viewing for quick configuration. This is useful when you want to migrate contact forms between websites' mod='ets_cfultimate'}</p>
                                                <div class="ctf_import_option_updata">
                                                    <label for="contactformdata">{l s='Data file' mod='ets_cfultimate'}</label>
                                                    <input type="file" name="contactformdata" id="contactformdata"/>
                                                </div>
                                                <div class="cft_import_option_clean">
                                                    <input type="checkbox" name="importdeletebefore" id="importdeletebefore" value="1"/>
                                                    <label for="importdeletebefore">{l s='Delete all contact forms before importing' mod='ets_cfultimate'}</label>
                                                </div>
                                                <div class="cft_import_option_clean">
                                                    <input type="checkbox" name="importoverride" id="importoverride" value="1"/>
                                                    <label for="importoverride">{l s='Override all forms with the same IDs' mod='ets_cfultimate'}</label>
                                                </div>
                                                <div class="cft_import_option_button">
                                                    <input type="hidden" value="1" name="importContactform"/>
                                                    <div class="etsCfuImportContactSubmit">
                                                        <i class="fa fa-compress"></i>
                                                        <input type="submit" class="btn btn-default cft_import_menu"
                                                               name="etsCfuImportContactSubmit"
                                                               value="{l s='Import contact forms' mod='ets_cfultimate'}"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>