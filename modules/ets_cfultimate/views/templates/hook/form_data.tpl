{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{assign var="form_data" value=array('label', 'short_code', 'values', 'desc', 'content')}
<div class="ets_cfu_form_data">
    {foreach $languages as $language}
        {foreach from=$form_data item='type'}
            {if !in_array($type, $excludes)}<span class="ets_cfu_{$type|escape:'html':'utf-8'}_{if $is_multi_lang}{$language.id_lang|intval}{else}{$defaultFormLanguage|intval}{/if}"{if  $type != $default || ($is_multi_lang && $language.id_lang != $defaultFormLanguage)} style="display:none;"{/if}></span>{/if}
        {/foreach}
    {/foreach}
</div>