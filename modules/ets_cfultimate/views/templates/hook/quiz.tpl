{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{assign var="is_multi_lang" value=($languages|count > 1)}
<div id="tag-generator-panel-quiz" class="hidden">
    <form action="" class="tag-generator-panel bootstrap" data-id="quiz">
        <div class="control-box">
            <fieldset>
                <table class="form-table">
                    <tbody>
                    {include file="./input_label.tpl" input_type='quiz'}
                    <tr>
                        <th scope="row"><label for="tag-generator-panel-quiz-name">{l s='Name' mod='ets_cfultimate'}</label></th>
                        <td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-quiz-name"/>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">{l s='Questions and answers' mod='ets_cfultimate'}</th>
                        <td>
                            <fieldset>
                                {include "./values.tpl" element='quiz' input='textarea'}
                                <label for="tag-generator-panel-quiz-values"><span class="description">{l s='One pipe-separated question-answer pair (e.g. The capital of Brazil?|Rio)' mod='ets_cfultimate'}.</span></label>
                            </fieldset>
                        </td>
                    </tr>
                    {include file="./desc.tpl" input_type='quiz'}
                    <tr>
                        <th scope="row">
                            <label for="tag-generator-panel-quiz-id">{l s='Id attribute' mod='ets_cfultimate'}</label>
                        </th>
                        <td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-quiz-id"/></td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="tag-generator-panel-quiz-class">{l s='Class attribute' mod='ets_cfultimate'}</label>
                        </th>
                        <td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-quiz-class"/></td>
                    </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
        <div class="insert-box">
            <div class="ets_cfu_input_quiz ets_cfu_input"
                 data-type="quiz"
                 data-name=""
                 data-id=""
                 data-class="">
                <div class="ets_cfu_input_generator">
                    <img src="{$img_dir|cat:'ip_quiz.png'|escape:'html':'utf-8'}" alt="{l s='Input quiz' mod='ets_cfultimate'}" />
                    {include file="./form_data.tpl" excludes = array('content') default = 'label'}
                    <p class="ets_cfu_help_block">{l s='Quiz' mod='ets_cfultimate'}</p>
                </div>
                {include file="./buttons.tpl"}
            </div>
            {include file="./input_tag.tpl" input_type='quiz'}
        </div>
    </form>
</div>