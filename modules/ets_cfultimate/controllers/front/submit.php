<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

class Ets_CfUltimateSubmitModuleFrontController extends ModuleFrontController
{
    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        if ($id = (int)Tools::getValue('_ets_cfu_container_post')) {
            $item = ets_cfu_contact_form($id);
            $result = $item->submit();
            $unit_tag = Tools::getValue('_ets_cfu_unit_tag');
            $response = array(
                'into' => '#' . ets_cfu_sanitize_unit_tag($unit_tag),
                'status' => $result['status'],
                'message' => $result['message'],
            );
            if ('validation_failed' == $result['status']) {
                $invalid_fields = array();
                foreach ((array)$result['invalid_fields'] as $name => $field) {
                    $invalid_fields[] = array(
                        'into' => 'span.ets_cfu_form-control-wrap.' . ets_cfu_sanitize_html_class($name),
                        'message' => $field['reason'],
                        'idref' => $field['idref'],
                    );
                }

                $response['invalidFields'] = $invalid_fields;
            }
            die(
                Tools::jsonEncode($response)
            );
        }
    }
}