<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
    exit;

class AdminContactFormUltimateImportExportController extends ModuleAdminController
{
    public $_html;

    public function __construct()
    {
        parent::__construct();
        $this->bootstrap = true;
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function renderList()
    {
        $errors = array();
        if (Tools::isSubmit('etsCfuImportContactSubmit')) {
            $this->module->processImport();
            $errors = $this->module->getErrors();
        }
        $this->context->smarty->assign(array(
            'controller' => Tools::getValue('controller'),
            'link' => $this->context->link,
            'errors' => $errors?$this->module->displayError($errors) : false,
        ));
        $this->_html .= $this->module->display($this->module->getLocalPath(), 'form_import.tpl');
        return $this->_html;
    }
}