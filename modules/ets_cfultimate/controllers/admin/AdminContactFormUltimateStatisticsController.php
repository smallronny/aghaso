<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

require_once(_PS_MODULE_DIR_ . 'ets_cfultimate/classes/ETS_CFU_Pagination.php');
if (!defined('_PS_VERSION_'))
    exit;

class AdminContactFormUltimateStatisticsController extends AdminModuleAdapterController
{
    public function __construct()
    {
        parent::__construct();
        $this->charts = array(
            'mes' => array(
                'label' => $this->l('Messages'),
            ),
            'vie' => array(
                'label' => $this->l('Views'),
            ),
            'rep' => array(
                'label' => $this->l('Replies'),
            ),
            'use' => array(
                'label' => $this->l('Users'),
            ),
        );
        if ($ip = Tools::getValue('etsCfuAddToBlackList'))
        {
            $black_list = explode("\n", Configuration::get('ETS_CFU_IP_BLACK_LIST'));
            $black_list[] = $ip;
            Configuration::updateValue('ETS_CFU_IP_BLACK_LIST', implode("\n", $black_list));
            if (Tools::isSubmit('etsCfuAjax')) {
                die(Tools::jsonEncode(array(
                    'ok' => true,
                )));
            }
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateStatistics') . '&cfu_tab_ets=view-log');
        }
        if (Tools::isSubmit('etsCfuClearLogSubmit'))
        {
            Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'ets_cfu_log WHERE id_contact IN (SELECT id_contact FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_shop WHERE id_shop=' . (int)$this->context->shop->id . ')');
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateStatistics') . '&cfu_tab_ets=view-log&conf=1');
        }
    }
    public function renderList()
    {
        $assigns = $this->getCharts();
        $contacts = Db::getInstance()->executeS('
            SELECT * FROM ' . _DB_PREFIX_ . 'ets_cfu_contact c, ' . _DB_PREFIX_ . 'ets_cfu_contact_lang cl 
            WHERE c.id_contact=cl.id_contact AND cl.id_lang=' . (int)$this->context->language->id
        );
        $sql = "
            SELECT * FROM " . _DB_PREFIX_ . "ets_cfu_log l 
            INNER JOIN " . _DB_PREFIX_ . "ets_cfu_contact c ON (l.id_contact=c.id_contact)
            LEFT JOIN " . _DB_PREFIX_ . "ets_cfu_contact_lang cl ON (c.id_contact=cl.id_contact AND cl.id_lang='" . (int)$this->context->language->id . "')
            GROUP BY l.ip,l.id_contact,l.datetime_added
        ";
        $total = count(Db::getInstance()->executeS($sql));
        $limit = 20;
        $page = Tools::getValue('page', 1);
        if ($page <= 0)
            $page = 1;
        $start = ($page - 1) * $limit;
        $pagination = new ETS_CFU_Pagination();
        $pagination->url = $this->context->link->getAdminLink('AdminContactFormUltimateStatistics') . '&cfu_tab_ets=view-log&page=_page_';
        $pagination->limit = $limit;
        $pagination->page = $page;
        $pagination->total = $total;
        $sql = "
            SELECT * FROM " . _DB_PREFIX_ . "ets_cfu_log l 
            INNER JOIN " . _DB_PREFIX_ . "ets_cfu_contact c ON (l.id_contact=c.id_contact)
            LEFT JOIN " . _DB_PREFIX_ . "ets_cfu_contact_lang cl ON (c.id_contact=cl.id_contact AND cl.id_lang='" . (int)$this->context->language->id . "')
            LEFT JOIN " . _DB_PREFIX_ . "customer cu ON (l.id_customer=cu.id_customer)
            GROUP BY l.ip,l.id_contact,l.datetime_added ORDER BY l.datetime_added DESC LIMIT " . (int)$start . ", " . (int)$limit;
        if (($logs = Db::getInstance()->executeS($sql)))
        {
            $black_list = explode("\n", Configuration::get('ETS_CFU_IP_BLACK_LIST'));
            foreach ($logs as &$log) {
                if (in_array($log['ip'], $black_list))
                    $log['black_list'] = true;
                else
                    $log['black_list'] = false;
                $browser = explode(' ', $log['browser']);
                if (isset($browser[0]))
                    $log['class'] = Tools::strtolower($browser[0]);
                else
                    $log['class'] = 'default';
            }
        }
        $assigns = array_merge($assigns, array(
            'action' => $this->context->link->getAdminLink('AdminContactFormUltimateStatistics'),
            'ets_cfu_js_dir_path' => $this->module->getPathUri() . 'views/js/',
            'ets_cfu_logs' => $logs,
            'cfu_tab_ets' => Tools::getValue('cfu_tab_ets', 'chart'),
            'ets_cfu_pagination_text' => $pagination->render(),
            'ets_cfu_show_reset' => Tools::isSubmit('etsCfuSubmitFilterChart'),
            'ets_cfu_contacts' => $contacts,
        ));
        $this->context->smarty->assign($assigns);
        return $this->module->display($this->module->getLocalPath(), 'statistics.tpl');
    }
}