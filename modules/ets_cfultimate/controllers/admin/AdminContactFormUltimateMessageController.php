<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
    exit;
require_once(_PS_MODULE_DIR_ . 'ets_cfultimate/classes/ETS_CFU_Contact_Reply.php');
require_once(_PS_MODULE_DIR_ . 'ets_cfultimate/classes/ETS_CFU_Pagination.php');

class AdminContactFormUltimateMessageController extends ModuleAdminController
{
    public $is170 =false;
    public function __construct()
    {
        parent::__construct();
        $this->bootstrap = true;
        if (Ets_cfultimate::checkVersionPs('1.7.0','>=')){
            $this->is170 = true;
        }
    }

    public function initContent()
    {
        parent::initContent();
        if (Tools::isSubmit('etsCfuSubmitSpecialActionMessage') && $id_message = Tools::getValue('id_contact_message')) {
            $messages = array();
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'ets_cfu_contact_message SET special="' . (int)Tools::getValue('etsCfuSubmitSpecialActionMessage') . '" WHERE id_contact_message ="' . (int)$id_message . '"');
            $messages[$id_message] = $this->displayRowMessage($id_message);
            die(
            Tools::jsonEncode(
                array(
                    'ok' => true,
                    'messages' => $messages,
                )
            )
            );
        }
        if (Tools::isSubmit('etsCfuSubmitBulkActionMessage')) {
            $bulk_action_message = Tools::getValue('bulk_action_message');
            if ($bulk_action_message && Tools::getValue('etsCfuMessageReaded')) {
                if ($bulk_action_message == 'mark_as_read') {
                    $messages = array();
                    foreach (Tools::getValue('etsCfuMessageReaded') as $key => $value) {
                        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'ets_cfu_contact_message SET readed=1 WHERE id_contact_message ="' . (int)$key . '"');
                        unset($value);
                        $messages[$key] = $this->displayRowMessage($key);
                    }
                    die(
                    Tools::jsonEncode(
                        array(
                            'ok' => true,
                            'messages' => $messages,
                            'count_messages' => $this->module->getCountUnreadMessage(),
                        )
                    )
                    );

                } elseif ($bulk_action_message == 'mark_as_unread') {
                    $messages = array();
                    foreach (Tools::getValue('etsCfuMessageReaded') as $key => $value) {
                        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'ets_cfu_contact_message SET readed=0 WHERE id_contact_message ="' . (int)$key . '"');
                        unset($value);
                        $messages[$key] = $this->displayRowMessage($key);
                    }
                    die(
                    Tools::jsonEncode(
                        array(
                            'ok' => true,
                            'messages' => $messages,
                            'count_messages' => $this->module->getCountUnreadMessage(),
                        )
                    )
                    );

                } elseif ($bulk_action_message == 'delete_selected') {
                    foreach (Tools::getValue('etsCfuMessageReaded') as $key => $value) {
                        $attachments = Db::getInstance()->getValue('SELECT attachments FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message WHERE id_contact_message="' . (int)$key . '"');
                        if ($attachments) {
                            foreach (explode(',', $attachments) as $attachment) {
                                @unlink(dirname(__FILE__) . '/../../views/img/etscfu_upload/' . $attachment);
                            }
                        }
                        Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message WHERE id_contact_message ="' . (int)$key . '"');
                        unset($value);
                    }
                    die(
                    Tools::jsonEncode(
                        array(
                            'ok' => true,
                            'url_reload' => $this->context->link->getAdminLink('AdminContactFormUltimateMessage', true) . '&conf=1'
                        )
                    )
                    );
                }
            }
        }
        //reply message.
        if (Tools::isSubmit('etsCfuSubmitReplyMessage') && $id_message = Tools::getValue('id_message')) {
            $reply = new ETS_CFU_Contact_Reply();
            $errors = array();
            $context = Context::getContext();
            $template = Configuration::get('ETS_CFU_ENABLE_TEMPLATE') ? 'contact_reply_form_ultimate' : 'contact_reply_form_ultimate_plain';
            $configuration = Configuration::getMultiple(
                array(
                    'PS_SHOP_EMAIL',
                    'PS_SHOP_NAME',
                    'PS_MAIL_TYPE',
                ),
                null,
                null, $context->shop->id
            );
            if( !Tools::getValue('reply_to')){
                $errors[] = $this->module->l('Reply to is required');
            }
            if( !Tools::getValue('reply_subject')){
                $errors[] = $this->module->l('Subject is required');
            }
            if (!Tools::getValue('message_reply')) {
                $errors[] = $this->module->l('Message is required');
            }
            if (!Ets_CfUltimate::getEmailToString(Tools::getValue('reply_to'))) {
                $errors[] = $this->module->l('Email is not validate');
            }
            if (!$errors){
                $mail_temp = $this->module->getLocalPath() . 'mails/'.$context->language->iso_code.'/'.$template;
                $msg = $this->module->l('Error - The following e-mail template is missing: %s');
                if (!file_exists(($file = $mail_temp.'.txt')) && ($configuration['PS_MAIL_TYPE'] == Mail::TYPE_BOTH || $configuration['PS_MAIL_TYPE'] == Mail::TYPE_TEXT)) {
                    $errors[] = sprintf($msg, $file);
                } else if (!file_exists(($file = $mail_temp.'.html')) && ($configuration['PS_MAIL_TYPE'] == Mail::TYPE_BOTH || $configuration['PS_MAIL_TYPE'] == Mail::TYPE_HTML)) {
                    $errors[] = sprintf($msg, $file);
                }
            }

            if ($errors) {
                die(Tools::jsonEncode(array(
                    'error' => $this->module->displayError($errors),
                )));
            } else {
                $template_email = Configuration::get('ETS_CFU_EMAIL_REPLY_TEMPLATE', Context::getContext()->language->id);
                $template_vars = array(
                    '{message_content}' => Configuration::get('ETS_CFU_ENABLE_TEMPLATE') ? str_replace(
                        array('{message_content}', '%7Bshop_url%7D', '%7Bshop_logo%7D'),
                        array(Tools::getValue('message_reply'), '{shop_url}', '{shop_logo}'),
                        $template_email
                    ) : Tools::getValue('message_reply'),
                );
                $toEmail = Ets_CfUltimate::getEmailToString(Tools::getValue('reply_to'));
                $toName = str_replace(array('<', '>', $toEmail), '', Tools::getValue('reply_to'));
                $fromEmail = Ets_CfUltimate::getEmailToString(Tools::getValue('from_reply'));
                $fromName = str_replace(array('<', '>', $fromEmail), '', Tools::getValue('from_reply'));
                $replyTo = Ets_CfUltimate::getEmailToString(Tools::getValue('reply_to_reply'));
                $replyToName = trim(str_replace(array('<', '>', $replyTo), '', Tools::getValue('reply_to_reply')));
                if (Mail::Send(
                    Context::getContext()->language->id,
                    $template,
                    Tools::getValue('reply_subject'),
                    $template_vars,
                    $toEmail,
                    $toName ? $toName : null,
                    $fromEmail ? $fromEmail : null,
                    $fromName ? $fromName : null,
                    null,
                    null,
                    $this->module->getLocalPath() . 'mails/',
                    null,
                    Context::getContext()->shop->id,
                    $replyTo ? $replyTo : null,
                    $replyToName ? $replyToName : null
                )) {
                    $reply->id_contact_message = $id_message;
                    $reply->content = Tools::getValue('message_reply');
                    $reply->id_employee = $this->context->employee->id;
                    $reply->reply_to = Tools::getValue('reply_to');
                    $reply->subject = Tools::getValue('reply_subject');
                    $reply->add();
                    die(Tools::jsonEncode(array(
                        'success' => $this->module->l('Your message has been successfully sent'),
                        'message_reply' => Tools::getValue('message_reply'),
                        'id_message' => $id_message,
                        'reply' => $this->module->displayReplyMessage($reply),
                    )));
                } else {
                    $errors[] = $this->module->l('An error occurred while sending the message');
                    die(Tools::jsonEncode(array(
                        'error' => $this->module->displayError($errors),
                    )));
                }
            }
        }
        /*end reply msg*/

        if (Tools::isSubmit('etsCfuDeleteMessage') && $id_message = Tools::getValue('id_message')) {
            $attachments = Db::getInstance()->getValue('SELECT attachments FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message WHERE id_contact_message="' . (int)$id_message . '"');
            if ($attachments) {
                foreach (explode(',', $attachments) as $attachment) {
                    @unlink(dirname(__FILE__) . '/../../views/img/etscfu_upload/' . $attachment);
                }
            }
            Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message WHERE id_contact_message="' . (int)$id_message . '"');
            Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message_shop where id_contact_message=' . (int)$id_message);
            Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'ets_cfu_message_reply WHERE id_contact_message=' . (int)$id_message);
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminContactFormUltimateMessage', true) . '&conf=1');
        }
        if (Tools::isSubmit('etsCfuAjax') && Tools::isSubmit('etsCfuViewMessage') && $id_message = Tools::getValue('id_message'))
        {
            $message = $this->getMessageById($id_message);
            if ($message['reply_to'] && Ets_CfUltimate::getEmailToString($message['reply_to'])) {
                $message['reply_to_check'] = true;
            } else
                $message['reply_to_check'] = false;
            if ($message)
            {
                $replies = $this->getRepliesByIdMessage($id_message);
                $this->context->smarty->assign(array(
                    'message' => $message,
                    'replies' => $replies,
                    'is170' => $this->is170
                ));
                $message_html = $this->module->display(_PS_MODULE_DIR_ . $this->module->name . DIRECTORY_SEPARATOR . $this->module->name . '.php', 'message.tpl');

                if (!Validate::isCleanHtml($message_html, true)) {
                    die(Tools::jsonEncode(array(
                        'errors' => $this->module->l('Message invalid.'),
                    )));
                }
                $messages = array();
                if (!Tools::getValue('etsCfuMessageReaded'))
                {
                    Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'ets_cfu_contact_message SET readed="1" WHERE id_contact_message ="' . (int)$id_message . '"');
                    $messages[$id_message] = $this->displayRowMessage($id_message);
                }
                die(Tools::jsonEncode(array(
                    'message_html' => $message_html,
                    'messages' => $messages,
                    'count_messages' => $this->module->getCountUnreadMessage(),
                )));
            }
        }
    }

    public function displayRowMessage($message)
    {
        if (!is_array($message))
            $message = $this->getMessageById($message);
        $this->context->smarty->assign(
            array(
                'message' => $message,
            )
        );
        return $this->module->display($this->module->getLocalPath(), 'row-message.tpl');
    }

    public function getMessageById($id_message)
    {
        $message = Db::getInstance()->getRow('
        SELECT m.*,cl.title,c.save_attachments ,c.email_to, CONCAT(cu.firstname," ",cu.lastname) as customer_name ,log.ip 
        FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message m
        INNER JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_message_shop ms ON (m.id_contact_message=ms.id_contact_message)
        LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact c ON (c.id_contact=m.id_contact)
        lEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_lang cl on (c.id_contact=cl.id_contact AND cl.id_lang="' . (int)$this->context->language->id . '")
        LEFT JOIN ' . _DB_PREFIX_ . 'customer cu ON (m.id_customer=cu.id_customer) 
        LEFT JOIN '._DB_PREFIX_.'ets_cfu_log log ON (log.id_contact = m.id_contact) 
        WHERE ms.id_shop="' . (int)Context::getContext()->shop->id . '" AND m.id_contact_message=' . (int)$id_message);
        if (trim($message['attachments']))
            $message['attachments'] = explode(',', trim($message['attachments']));
        else
            $message['attachments'] = '';
        $message['replies'] = $this->getRepliesByIdMessage($message['id_contact_message']);
        $message['from_reply'] = Configuration::get('PS_SHOP_NAME') . ' <' . (Configuration::get('PS_MAIL_METHOD') == 2 ? Configuration::get('PS_MAIL_USER') : Configuration::get('PS_SHOP_EMAIL')) . '>';
        $message['reply'] = Configuration::get('PS_SHOP_NAME') . ' <' . Configuration::get('PS_SHOP_EMAIL') . '>';
        $message['email_to'] = explode(',',trim($message['email_to']))[0];
        return $message;
    }

    public function getRepliesByIdMessage($id_message)
    {
        $replies = Db::getInstance()->executeS('
       SELECT * FROM ' . _DB_PREFIX_ . 'ets_cfu_message_reply 
       WHERE id_contact_message="' . (int)$id_message . '"'
        );
        return $replies;
    }

    public function renderList()
    {
        if (Tools::isSubmit('etsCfuViewMessage') && $id_message = Tools::getValue('id_message')) {
            $message = $this->getMessageById($id_message);
            if ($message['reply_to'] && Ets_CfUltimate::getEmailToString($message['reply_to'])) {
                $message['reply_to_check'] = True;
            } else
                $message['reply_to_check'] = false;
            if ($message) {
                $replies = $this->getRepliesByIdMessage($id_message);
                $this->context->smarty->assign(
                    array(
                        'message' => $message,
                        'replies' => $replies,
                        'base_url' => $this->module->getBaseLink(),
                        'is170' => $this->is170
                    )
                );
                return $this->module->display(_PS_MODULE_DIR_ . $this->module->name . DIRECTORY_SEPARATOR . $this->module->name . '.php', 'message.tpl');
            }
        } else {
            $filter = '';
            $url_extra = '';
            $values_submit = array();
            if (Tools::getValue('id_contact')) {
                $filter .= ' AND m.id_contact="' . (int)Tools::getValue('id_contact') . '"';
                $url_extra .= '&id_contact=' . (int)Tools::getValue('id_contact');
                $values_submit['id_contact'] = Tools::getValue('id_contact');
            }
            if (Tools::getValue('id_contact_message')) {
                $filter .= ' AND m.id_contact_message="' . (int)Tools::getValue('id_contact_message') . '"';
                $url_extra .= '&id_contact_message=' . (int)Tools::getValue('id_contact_message');
                $values_submit['id_contact_message'] = (int)Tools::getValue('id_contact_message');
            }
            if (Tools::getValue('subject')) {
                $filter .= ' AND m.subject like "%' . Tools::getValue('subject') . '%"';
                $url_extra .= '&subject=' . Tools::getValue('subject');
                $values_submit['subject'] = Tools::getValue('subject');
            }
            if (Tools::getValue('sender')) {
                $filter .= ' AND m.sender like "%' . Tools::getValue('sender') . '%"';
                $url_extra .= '&sender=' . Tools::getValue('sender');
                $values_submit['sender'] = Tools::getValue('sender');
            }
            if (Tools::getValue('messageFilter_dateadd_from')) {
                $filter .= ' AND m.date_add >="' . Tools::getValue('messageFilter_dateadd_from') . '"';
                $url_extra .= '&messageFilter_dateadd_from=' . Tools::getValue('messageFilter_dateadd_from');
                $values_submit['messageFilter_dateadd_from'] = Tools::getValue('messageFilter_dateadd_from');
            }
            if (Tools::getValue('messageFilter_dateadd_to')) {
                $filter .= ' AND m.date_add <= "' . Tools::getValue('messageFilter_dateadd_to') . '"';
                $url_extra .= '&messageFilter_dateadd_to=' . Tools::getValue('messageFilter_dateadd_to');
                $values_submit['messageFilter_dateadd_to'] = Tools::getValue('messageFilter_dateadd_to');
            }
            if (Tools::getValue('messageFilter_replied') != '') {
                if (Tools::getValue('messageFilter_replied') == 0)
                    $filter .= ' AND m.id_contact_message NOT IN (SELECT id_contact_message FROM ' . _DB_PREFIX_ . 'ets_cfu_message_reply)';
                else
                    $filter .= ' AND m.id_contact_message IN (SELECT id_contact_message FROM ' . _DB_PREFIX_ . 'ets_cfu_message_reply)';
                $url_extra .= '&messageFilter_replied=' . Tools::getValue('messageFilter_replied');
                $values_submit['messageFilter_replied'] = Tools::getValue('messageFilter_replied');
            }
            if (Tools::getValue('messageFilter_message') != '') {
                $filter .= ' AND m.body like "%' . pSQL(Tools::getValue('messageFilter_message')) . '%"';
                $url_extra .= '&messageFilter_message=' . Tools::getValue('messageFilter_message');
                $values_submit['messageFilter_message'] = Tools::getValue('messageFilter_message');
            }
            $url_extra_no_order = $url_extra;
            if (Tools::getValue('OrderBy') != '' && Tools::getValue('OrderBy') != 'm.id_contact_message') {
                $orderBy = Tools::getValue('OrderBy') . ' ' . Tools::getValue('OrderWay', 'DESC') . ',m.id_contact_message DESC';
                $url_extra .= '&OrderBy=' . Tools::getValue('OrderBy') . '&OrderWay=' . Tools::getValue('OrderWay', 'DESC');
            } else {
                $orderBy = Tools::getValue('OrderBy', 'm.id_contact_message') . ' ' . Tools::getValue('OrderWay', 'DESC');
            }
            $totalMessage = $this->module->getMessages($filter, 0, 0, true, $orderBy);
            $limit = Configuration::get('ETS_CFU_NUMBER_MESSAGE') ? Configuration::get('ETS_CFU_NUMBER_MESSAGE') : 20;
            $page = Tools::getValue('page', 1);
            $start = ($page - 1) * $limit;
            $pagination = new ETS_CFU_Pagination();
            $pagination->url = $this->context->link->getAdminLink('AdminContactFormUltimateMessage', true) . $url_extra . '&page=_page_';
            $pagination->limit = $limit;
            $pagination->page = $page;
            $pagination->total = $totalMessage;
            if (Tools::isSubmit('etsCfuSubmitExportButtonMessage')) {
                ob_get_clean();
                ob_start();
                $messages = $this->module->getMessages($filter);
                $csv = "Subject\tFrom\tContact Form\tMessage\tDate" . "\r\n";
                foreach ($messages as $row) {
                    $message = array();
                    $message[] = $row['subject'];
                    $message[] = $row['sender'];
                    $message[] = $row['title'];
                    $message[] = str_replace("\n", '', strip_tags($row['body']));
                    $message[] = $row['date_add'];
                    $csv .= join("\t", $message) . "\r\n";
                }
                $csv = chr(255) . chr(254) . mb_convert_encoding($csv, "UTF-16LE", "UTF-8");
                header("Content-type: application/x-msdownload");
                header("Content-disposition: csv; filename=" . date("Y-m-d") .
                    "_message_list.csv; size=" . Tools::strlen($csv));
                echo $csv;
                exit();
            }
            $messages = $this->module->getMessages($filter, $start, $limit, false, $orderBy);
            if ($messages) {
                foreach ($messages as &$message) {
                    $message['replies'] = $this->getRepliesByIdMessage($message['id_contact_message']);
                    $message['row_message'] = $this->displayRowMessage($message);
                }

            }
            $contacts = $this->module->getContacts();
            $this->context->smarty->assign(
                array(
                    'messages' => $messages,
                    'ets_cfu_contacts' => $contacts,
                    'url_module' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->module->name . '&tab_module=' . $this->module->tab . '&module_name=' . $this->module->name,
                    'link' => $this->context->link,
                    'base_url' => $this->module->getBaseLink(),
                    'filter' => $filter,
                    'is_ps15' => version_compare(_PS_VERSION_, '1.6', '<') ? true : false,
                    'ets_cfu_pagination_text' => $pagination->render(),
                    'values_submit' => $values_submit,
                    'url_full' => $this->context->link->getAdminLink('AdminContactFormUltimateMessage', true) . $url_extra_no_order . '&page=' . Tools::getValue('page', 1),
                    'orderBy' => Tools::getValue('OrderBy', 'm.id_contact_message'),
                    'orderWay' => Tools::getValue('OrderWay', 'DESC'),
                )
            );
            return $this->module->display($this->module->getLocalPath(), 'list-message.tpl');
        }
    }
}