<!-- Preferred Delivery Time block -->
<div class="tab-pane" id="preferred_pickup">
    <h4 class="visible-print">{l s='Pickup at Store' mod='kbstorelocatorpickup'} <span class="badge"></span></h4>
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label">{l s='Customer\'s Preferred Pickup Store and Date' mod='kbstorelocatorpickup'}</label>
            <hr style='border-top: 1px solid #a0d0eb !important;margin-top: 4px !important;margin-bottom: 4px !important;' />
            <div class="col-lg-12">
                <div class="col-lg-3">
                    <p class="form-control-static" style="font-weight: bold;">{l s='Pickup Store' mod='kbstorelocatorpickup'}:</p>
                </div>
                <div class="col-lg-9">
                    <p class="form-control-static">
                        {if !empty($store->name)}
                        {*{if isset($store->name[$id_lang]) && !empty($store->name[$id_lang])}
                    {$store->name[$id_lang]|escape:'htmlall':'UTF-8'},
                {else}*}
                    {$store->name|escape:'htmlall':'UTF-8'},
                {/if}
                {if !empty($store->address1)}
                    {*{if isset($store->address1[$id_lang])}
                        {if !empty($store->address1[$id_lang])}
                            {$store->address1[$id_lang]|escape:'htmlall':'UTF-8'},
                        {/if}
                    {else}*}
                        {$store->address1|escape:'htmlall':'UTF-8'},
                   {* {/if}*}
                {/if}
                {if !empty($store->address2)}
                    {*{if isset($store->address2[$id_lang])}
                        {if !empty($store->address2[$id_lang])}
                            {$store->address2[$id_lang]|escape:'htmlall':'UTF-8'},
                        {/if}
                    {else}*}
                        {$store->address2|escape:'htmlall':'UTF-8'},
                   {* {/if}*}
                {/if}
                {if !empty($store->postcode)}{$store->postcode|escape:'htmlall':'UTF-8'},{/if}
                {if !empty($store->city)}{$store->city|escape:'htmlall':'UTF-8'},{/if}
                {if !empty($store->id_state)}{State::getNameById($store->id_state)},{/if}
            {if !empty($store->id_country)}{Country::getNameById($id_lang, $store->id_country)}{/if}
                    </p>
                </div>
            </div>
                
            <div class="col-lg-12">
                <div class="col-lg-3">
                    <p class="form-control-static" style="font-weight: bold;">{l s='Preferred Pickup Date' mod='kbstorelocatorpickup'}:</p>
                </div>
                <div class="col-lg-9">
                    <p class="form-control-static">{$pickup_time|escape:'htmlall':'UTF-8'}</p>
                </div>
            </div>
                
        </div>
    </div>
</div>

{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2018 Knowband
* @license   see file: LICENSE.txt
*
* Description
*
* Admin tpl file
*}