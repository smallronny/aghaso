{if isset($search_store_result)}
    <ul>
        {if !empty($available_stores)}
            {foreach $available_stores as $store}
                {assign var=random value=132310|mt_rand:20323221}
                <li id="{$random|escape:'htmlall':'UTF-8'}" >
{*                    changes done by tarun*}
                    <input type="hidden" id="velo-add-longitude" name="velo-add-longitude" value="{$store['longitude']|escape:'htmlall':'UTF-8'}">
                    <input type="hidden" id="velo-add-latitude" name="velo-add-longitude" value="{$store['latitude']|escape:'htmlall':'UTF-8'}">
{*                    changes over        *}
                            <table style="
    width: 100%;
">
                                <tr>
                                       {if $is_enabled_store_image} 
                                    <td>
                                        <div id="kb-store-image">
                                        {if !empty($store['image'])}{$store['image'] nofilter}{*escape not required*}{/if}
                        </div>
                                        <div></div>
                                    </td>
                    {/if}
                                    <td style="padding-left:0px;">
                                        <div class="velo_add_name"> <span class="velo_store_icon" style="display:none;"><img class="velo-phone-icon" src="{$store_icon|escape:'htmlall':'UTF-8'}"/> </span>{$store['name']|escape:'htmlall':'UTF-8'}</div>
                            <div class="velo-add-address"> {$store['address1']|escape:'htmlall':'UTF-8'}
                    {if !empty($store['address2'])} 
                        <br/>{$store['address2']|escape:'htmlall':'UTF-8'}
                    {/if}
                    {if !empty($store['state'])} 
                        <br/>{$store['state']|escape:'htmlall':'UTF-8'}
                    {/if}
                    {if !empty($store['city'])} 
                        <br/>{$store['city']|escape:'htmlall':'UTF-8'}
                    {/if}
                    {if !empty($store['postcode'])} 
                        <br/>{$store['postcode']|escape:'htmlall':'UTF-8'}
                    {/if}
                            </div>
                                    </td>
                             
                                </tr>
                            </table>

                            <div style="display: none" class="velo-w3-address-country">{$store['country']|escape:'htmlall':'UTF-8'}</div>
                            <div class="velo-show-more"><strong><a href="javascript:void(0);" id="button-show-more-{$store['id_store']}" class="button-show-more" >{l s='Show More' mod='kbstorelocatorpickup'}</a></strong></div>
                            <div class="extra_content" style="display:none;">
                        {if !empty($store['hours'])}
                                <div  class="kb-store-hours">
                                <span class="velo_add_clock"><img class="velo-phone-icon" src="{$clock_icon|escape:'htmlall':'UTF-8'}"/>  {l s='Store Timing' mod='kbstorelocatorpickup'}</span>
                                    <table class="mp-openday-list" style="display: table;">
                                    <tbody>
                                {foreach $store['hours'] as $key => $time}

                                    {if isset($time['hours'])}
                                            <tr>
                                        <td class="mp-openday-list-title"><strong>{$time['day']|escape:'htmlall':'UTF-8'}</td>
                                        <td class="mp-openday-list-value">{if $time['hours'] == ''} <span style="color: red">{l s='Closed' mod='kbstorelocatorpickup'}</span>{else}{$time['hours']|escape:'htmlall':'UTF-8'}{/if}</td>
                                        </tr>
                    {/if}
                                {/foreach}
                                        </tbody></table>
                    </div>
                        {/if}
                {if !empty($store['email'])}
                            <span class="velo_add_number"><i class="material-icons">mail_outline</i> {$store['email']|escape:'htmlall':'UTF-8'}</span>
                        {/if}
                        {if $display_phone && !empty($store['phone'])}
                            <span class="velo_add_number"><img class="velo-phone-icon" src="{$phone_icon|escape:'htmlall':'UTF-8'}"/> {$store['phone']|escape:'htmlall':'UTF-8'}</span>
                            {/if}
                            {* changes by rishabh jain *}
                            {if $is_enabled_website_link}
                                <div class="kb-store-url">
                                    <span class="velo_add_number"><img class="velo-web-icon" src="{$web_icon|escape:'htmlall':'UTF-8'}"/> <a href='{$index_page_link}'>
                                        {$index_page_link}
                                    </a></span>

                                </div>
                            {/if}
                    </div>
                        {* changes over *}
                        <div class="velo_add_distance"></div>
                    {if $store['store_distance'] != ''}
                    <div class="velo_add_distance"><img class="velo-distance-icon" src="{$distance_icon|escape:'quotes':'UTF-8'}"/> {$store['store_distance']|escape:'htmlall':'UTF-8'} {$distance_unit|escape:'htmlall':'UTF-8'}</div>
                    {/if}
                    {if isset($filter_pickup) && $filter_pickup && isset($controller) && $controller == 'order'}
                        <a class="velo-store-select-link {if $default_store == $store['id_store']}store-selected{/if}" data-key="{$store['id_store']|escape:'htmlall':'UTF-8'}" href="#">{if $default_store == $store['id_store']}{l s='Selected' mod='kbstorelocatorpickup'} <i class="icon-check"></i>{else}{l s='Select' mod='kbstorelocatorpickup'}{/if}</a>
                    {/if}
                    
                    <button onclick="focus_and_popup({$random|escape:'htmlall':'UTF-8'})" class="velo-directions-button">{l s='Locate Store' mod='kbstorelocatorpickup'} <i class="material-icons">chevron_right</i></button>
                        {* changes by rishabh jain *}
                        {if $is_enabled_direction_link}
                                <a  target="_blank" class="velo-directions-link" href="http://google.com/maps/dir/?api=1&amp;destination={$store['latitude']|escape:'url'}%2C{$store['longitude']|escape:'url'}">{l s='Get Direction' mod='kbstorelocatorpickup'} <i class="material-icons">directions</i></a>
                                {/if}
                        {* changes over*}
                        
                <input type="hidden" name="kb_av_store_details" value='{$av_store_detail|escape:'htmlall':'UTF-8'}'>
            </li>
        {/foreach}
    {else}
        <li>
            <div style="height: 280px;">
                <b>{l s='No result found' mod='kbstorelocatorpickup'}</b>
            </div>
        </li>
    {/if}
</ul>
{/if}

{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2018 Knowband
* @license   see file: LICENSE.txt
*
* Description
*
* Admin tpl file
*}