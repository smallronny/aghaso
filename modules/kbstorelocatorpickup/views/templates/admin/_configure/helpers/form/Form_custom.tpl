{extends file='helpers/form/form.tpl'}

{block name='defaultForm'}

    {$kb_tabs} {*Variable contains html content, escape not required*}
    {$form} {*Variable contains html content, escape not required*}
    {*//changes by tarun*}
    {$view nofilter} {*Variable contains html content, escape not required*}
    <script type="text/javascript">
        var validate_css_length = "{l s='Length of CSS should be less than 10000' mod='kbstorelocatorpickup'}";
        var kb_numeric = "{l s='Field should be numeric.' mod='kbstorelocatorpickup'}";
        var kb_positive = "{l s='Field should be positive.' mod='kbstorelocatorpickup'}";
            velovalidation.setErrorLanguage({
            alphanumeric: "{l s='Field should be alphanumeric.' mod='kbstorelocatorpickup'}",
            digit_pass: "{l s='Password should contain atleast 1 digit.' mod='kbstorelocatorpickup'}",
            empty_field: "{l s='Field cannot be empty.' mod='kbstorelocatorpickup'}",
            number_field: "{l s='You can enter only numbers.' mod='kbstorelocatorpickup'}",            
            positive_number: "{l s='Number should be greater than 0.' mod='kbstorelocatorpickup'}",
            maxchar_field: "{l s='Field cannot be greater than # characters.' mod='kbstorelocatorpickup'}",
            minchar_field: "{l s='Field cannot be less than # character(s).' mod='kbstorelocatorpickup'}",
            invalid_date: "{l s='Invalid date format.' mod='kbstorelocatorpickup'}",
            valid_amount: "{l s='Field should be numeric.' mod='kbstorelocatorpickup'}",
            valid_decimal: "{l s='Field can have only upto two decimal values.' mod='kbstorelocatorpickup'}",
            maxchar_size: "{l s='Size cannot be greater than # characters.' mod='kbstorelocatorpickup'}",
            specialchar_size: "{l s='Size should not have special characters.' mod='kbstorelocatorpickup'}",
            maxchar_bar: "{l s='Barcode cannot be greater than # characters.' mod='kbstorelocatorpickup'}",
            positive_amount: "{l s='Field should be positive.' mod='kbstorelocatorpickup'}",
            maxchar_color: "{l s='Color could not be greater than # characters.' mod='kbstorelocatorpickup'}",
            invalid_color: "{l s='Color is not valid.' mod='kbstorelocatorpickup'}",
            specialchar: "{l s='Special characters are not allowed.' mod='kbstorelocatorpickup'}",
            script: "{l s='Script tags are not allowed.' mod='kbstorelocatorpickup'}",
            style: "{l s='Style tags are not allowed.' mod='kbstorelocatorpickup'}",
            iframe: "{l s='Iframe tags are not allowed.' mod='kbstorelocatorpickup'}",
              not_image: "{l s='Uploaded file is not an image' mod='kbstorelocatorpickup'}",
            image_size: "{l s='Uploaded file size must be less than #.' mod='kbstorelocatorpickup'}",
            html_tags: "{l s='Field should not contain HTML tags.' mod='kbstorelocatorpickup'}",
            number_pos: "{l s='You can enter only positive numbers.' mod='kbstorelocatorpickup'}",
});
    </script>
{/block}
    {*
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
    * versions in the future. If you wish to customize PrestaShop for your
    * needs please refer tohttp://www.prestashop.com for more information.
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * @category  PrestaShop Module
    * @author    knowband.com <support@knowband.com>
    * @copyright 2015 Knowband
    * @license   see file: LICENSE.txt
    *
    * Description
    *
    * Admin tpl file
    *}

