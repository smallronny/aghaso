{*
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
    * versions in the future. If you wish to customize PrestaShop for your
    * needs please refer tohttp://www.prestashop.com for more information.
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * @category  PrestaShop Module
    * @author    knowband.com <support@knowband.com>
    * @copyright 2015 Knowband
    * @license   see file: LICENSE.txt
    *
    * Description
    *
    * Admin tpl file
*}

{*//added by tarun*}
<div id="steps_to_configure_google_API" class="panel steps_to_config_google_API">
<span class="vss-google-link_google_API">
    <h3><a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">{l s='Click here to get Google map API key' mod='kbstorelocatorpickup'}</a></h3> {*Variable contains a URL, escape not required*}
</span>

    <div class="steps_panel-heading">
        {l s='STEPS TO CONFIGURE Google API:' mod='kbstorelocatorpickup'}
    </div>
    <div class="">
        <div class="panel-group kbstorelocatorpickup_step_accordion" id="kbstorelocatorpickup_dropbox_accordion">
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse1">{l s='STEP 1' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step1.png' style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse2">{l s='STEP 2' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step2.png' style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse3">{l s='STEP 3' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step3.png' style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse45">{l s='STEP 4 & 5' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse45" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step4_5.png' style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>        
                
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickup_dropbox_accordion" href="#dropbox_step_collapse6">{l s='STEP 6' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step6.png' style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse7">{l s='STEP 7' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step7.png' style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse8">{l s='STEP 8' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse8" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step8.png' style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="steps_panel-heading">
                <a data-toggle="collapse" data-parent="#kbstorelocatorpickupv3_dropbox_accordion" href="#dropbox_step_collapse9">{l s='STEP 9' mod='kbstorelocatorpickup'}</a>
            </div>
            <div id="dropbox_step_collapse9" class="panel-collapse collapse">
                <div class="panel-body">
                    <img src='{$mod_dir nofilter}{*Variable contains html content, escape not required*}kbstorelocatorpickup/views/img/admin/Step9.png' style="width: 100%"style="width: 100%"/> {*Variable contains a URL, escape not required*}
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
                

