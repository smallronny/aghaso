<div class="kb_custom_tabs kb_custom_panel">
    <span>
        <a class="kb_custom_tab {if $selected_nav == 'config'}kb_active{/if}" title="{l s='General Settings' mod='kbstorelocatorpickup'}" id="kbsl_config_link" href="{$admin_sl_setting_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-gear"></i>
            {l s='General Settings' mod='kbstorelocatorpickup'}
        </a>
    </span>

    <span>
        <a class="kb_custom_tab {if $selected_nav == 'slpickupsetting'}kb_active{/if}" title="{l s='Pickup Time Settings' mod='kbstorelocatorpickup'}" id="kbsl_pickup_time" href="{$admin_sl_pickup_time_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-clock-o"></i>
            {l s='Pickup Time Settings' mod='kbstorelocatorpickup'}
        </a>
    </span>
    <span>
        <a class="kb_custom_tab {if $selected_nav == 'slemailnotification'}kb_active{/if}" title="{l s='Email Notification' mod='kbstorelocatorpickup'}" id="kbsl_email_notification" href="{$admin_sl_email_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-envelope"></i>
            {l s='Email Notification' mod='kbstorelocatorpickup'}
        </a>
    </span>
    <span>
        <a class="kb_custom_tab {if $selected_nav == 'slstoreimport'}kb_active{/if}" title="{l s='Import Store Contacts' mod='kbstorelocatorpickup'}" id="kbsl_importer" href="{$admin_sl_importer_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-upload"></i>
            {l s='Import Store Contact' mod='kbstorelocatorpickup'}
        </a>
    </span>
    <span>
        <a class="kb_custom_tab {if $selected_nav == 'sldeliveryslip'}kb_active{/if}" title="{l s='Download Delivery Slip' mod='kbstorelocatorpickup'}" id="kbsl_setting_link" href="{$admin_sl_delivery_slip_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-download"></i>
            {l s='Download Delivery Slip' mod='kbstorelocatorpickup'}
        </a>
    </span>
</div>
        
        <script>
            var admin_pickup_url = "{$admin_sl_pickup_time_controller|escape:'quotes':'UTF-8'}";
            var kb_placeholder_text = "{l s='Select some options.' mod='kbstorelocatorpickup'}";
            var no_results_text = "{l s='No results match' mod='kbstorelocatorpickup'}";
            var kb_numeric = "{l s='Field should be numeric.' mod='kbstorelocatorpickup'}";
            var kb_date_format_invalid = "{l s='Format is invalid.' mod='kbstorelocatorpickup'}";
            var kb_positive = "{l s='Field should be positive.' mod='kbstorelocatorpickup'}";
            var check_for_all = "{l s='Kindly check for all languages.' mod='kbstorelocatorpickup'}";
            var no_select = "{l s='Please select placement' mod='kbstorelocatorpickup'}";
            var empty_field = "{l s='Field cannot be empty' mod='kbstorelocatorpickup'}";
            var maximum_days_less_error = "{l s='Maximum days cannot be less or equal to days gap.' mod='kbstorelocatorpickup'}";
            var csv_file_empty = "{l s='Please upload csv file.' mod='kbstorelocatorpickup'}";
            var download_sample_label = "{l s='Download Sample' mod='kbstorelocatorpickup'}";
             var currentText = '{l s='Now'  mod='kbstorelocatorpickup' js=1}';
            var closeText = '{l s='Done'  mod='kbstorelocatorpickup' js=1}';
            var timeOnlyTitle = '{l s='Choose Time'  mod='kbstorelocatorpickup' js=1}';
            var timeText = '{l s='Time' mod='kbstorelocatorpickup' js=1}';
            var hourText = '{l s='Hour' mod='kbstorelocatorpickup' js=1}';
            var minuteText = '{l s='Minute' mod='kbstorelocatorpickup' js=1}';
            var available_store_empty = "{l s='Select atleast one store' mod='kbstorelocatorpickup'}";
            var enable_store_not_selected = "{l s='Select store which is selected for enabled store' mod='kbstorelocatorpickup'}";
            var empty_field = "{l s='Field cannot be empty' mod='kbstorelocatorpickup'}";
            var please_check_kb_fields = "{l s='Fields cannot be blank.Please check for all the languages in the field.' mod='kbstorelocatorpickup'}";
            var invalid_from_date = "{l s='From date should be less than To date.' mod='kbstorelocatorpickup'}";
            velovalidation.setErrorLanguage({
            alphanumeric: "{l s='Field should be alphanumeric.' mod='kbstorelocatorpickup'}",
            digit_pass: "{l s='Password should contain atleast 1 digit.' mod='kbstorelocatorpickup'}",
            empty_field: "{l s='Field cannot be empty.' mod='kbstorelocatorpickup'}",
            number_field: "{l s='You can enter only numbers.' mod='kbstorelocatorpickup'}",            
            positive_number: "{l s='Number should be greater than 0.' mod='kbstorelocatorpickup'}",
            maxchar_field: "{l s='Field cannot be greater than # characters.' mod='kbstorelocatorpickup'}",
            minchar_field: "{l s='Field cannot be less than # character(s).' mod='kbstorelocatorpickup'}",
            invalid_date: "{l s='Invalid date format.' mod='kbstorelocatorpickup'}",
            valid_amount: "{l s='Field should be numeric.' mod='kbstorelocatorpickup'}",
            valid_decimal: "{l s='Field can have only upto two decimal values.' mod='kbstorelocatorpickup'}",
            maxchar_size: "{l s='Size cannot be greater than # characters.' mod='kbstorelocatorpickup'}",
            specialchar_size: "{l s='Size should not have special characters.' mod='kbstorelocatorpickup'}",
            maxchar_bar: "{l s='Barcode cannot be greater than # characters.' mod='kbstorelocatorpickup'}",
            positive_amount: "{l s='Field should be positive.' mod='kbstorelocatorpickup'}",
            maxchar_color: "{l s='Color could not be greater than # characters.' mod='kbstorelocatorpickup'}",
            invalid_color: "{l s='Color is not valid.' mod='kbstorelocatorpickup'}",
            specialchar: "{l s='Special characters are not allowed.' mod='kbstorelocatorpickup'}",
            script: "{l s='Script tags are not allowed.' mod='kbstorelocatorpickup'}",
            style: "{l s='Style tags are not allowed.' mod='kbstorelocatorpickup'}",
            iframe: "{l s='Iframe tags are not allowed.' mod='kbstorelocatorpickup'}",
             not_image: "{l s='Uploaded file is not an image' mod='kbstorelocatorpickup'}",
            image_size: "{l s='Uploaded file size must be less than #.' mod='kbstorelocatorpickup'}",
            html_tags: "{l s='Field should not contain HTML tags.' mod='kbstorelocatorpickup'}",
            number_pos: "{l s='You can enter only positive numbers.' mod='kbstorelocatorpickup'}",
});
        </script>
        
{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2018 Knowband
* @license   see file: LICENSE.txt
*
* Description
*
* Admin tpl file
*}