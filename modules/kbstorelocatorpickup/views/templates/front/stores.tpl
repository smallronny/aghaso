{*{capture name=path}{l s='Our stores' mod='kbstorelocatorpickup'}{/capture}*}

{extends file=$layout}
{block name = 'content'}
<div class="kb-clearfix"></div>
<div class="d-none">
    <h1 class="page-heading">{l s='Our Stores' mod='kbstorelocatorpickup'}</h1>
</div>

<div class="velo-search-container page-content card card-block">
    <div class="velo-store-locator">
        <form method="post" id="searchStoreForm" action>
            {if isset($is_enabled_store_logo) && $is_enabled_store_logo == 1}
                <div class="velo-store-banners">
                    <img src="{$logo_image}" style="width: 100%;max-height:300px;">
                </div>
            {/if}

            <div class="velo-store-filters">
                <div class="velo-search-bar velo-field-inline">
                    <label>{l s='Find Stores Near' mod='kbstorelocatorpickup'}:</label>
                    <input type="text" placeholder="{l s='Enter Street Address or City & State' mod='kbstorelocatorpickup'}" id="velo_address_search"/>
                </div>
                <div class="velo-search-distance velo-field-inline">
                    <label>{l s='Distance' mod='kbstorelocatorpickup'}:</label>
                    <select id="velo_within_distance">
                        <option value="5">5 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="10">10 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="15">15 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option selected="" value="25">25 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="50">50 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="100">100 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="250">250 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="500">500 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="1000">1000 {$distance_unit|escape:'htmlall':'UTF-8'}</option>
                        <option value="0">{l s='No Limit' mod='kbstorelocatorpickup'}</option>
                    </select>
                </div>
                <div class="velo-search-limit velo-field-inline">
                    <label>{l s='Results' mod='kbstorelocatorpickup'}</label>
                    <select id="velo_limit" >
                        <option value="25" selected="selected">25</option>
                        <option value="50">50</option>
                        <option value="75">75</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div class="velo-search-button velo-field-inline">
                    <button type="button" id="searchStoreBtn">{l s='Search' mod='kbstorelocatorpickup'}</button>
                </div>
                <div class="velo-search-button velo-field-inline">
                    <button type="button" id="resetStoreBtn">{l s='Reset' mod='kbstorelocatorpickup'}</button>
                </div>
            </div>
        </form>
        <!--Filters-->
        <div class="velo-store-map velo-pickup-store-map">
            <div id="map"></div>
        </div>
        <div class="velo-location-list velo-pickup-location-list">
        <div class="header-store-locator">
            <h3 class="page-heading titlestore">{l s='Find a Store' mod='kbstorelocatorpickup'}</h3>
        </div>
        <div>
            <ul>
                {if !empty($available_stores)}
                    {foreach $available_stores as $key => $store}
                        {assign var=random value=132310|mt_rand:20323221}
                        <li id="{$random|escape:'htmlall':'UTF-8'}" >
                            
                            <div style="width: 100%;">
                                <div class="row">
                                    <input type="hidden" id="velo-add-longitude" name="velo-add-longitude" value={$store['longitude']}>
                    <input type="hidden" id="velo-add-latitude" name="velo-add-longitude" value={$store['latitude']}>

                                    <div class="col-md-4">
                      {if $is_enabled_store_image} 
                                        <div id="kb-store-image">
                            {if !empty($store['image'])}{$store['image'] nofilter}{*escape not required*}{/if}
                        </div>
                                        <div></div>
                                    {/if}
                                    </div>
                                    <div class="col-md-8">
                                        <div class="velo_add_name"> <span class="velo_store_icon" style="display:none;"><img class="velo-phone-icon" src="{$store_icon|escape:'htmlall':'UTF-8'}"/> </span>{$store['name']|escape:'htmlall':'UTF-8'}</div>
                            <div class="velo-add-address"> {$store['address1']|escape:'htmlall':'UTF-8'}
                            {if !empty($store['address2'])} 
                                <br/>{$store['address2']|escape:'htmlall':'UTF-8'}
                            {/if}
                            {if !empty($store['state'])} 
                                <br/>{$store['state']|escape:'htmlall':'UTF-8'}
                            {/if}
{*                            changes by tarun*}
                            {if !empty($store['postcode'])} 
                                <br/>{$store['postcode']|escape:'htmlall':'UTF-8'}
                            {/if}
                            {if !empty($store['city'])} 
                                <br/>{$store['city']|escape:'htmlall':'UTF-8'}
                            {/if}

                             {if !empty($store['email'])}
                            <span class="velo_add_number"><i class="fa fa-envelope-o" aria-hidden="true"></i> {$store['email']|escape:'htmlall':'UTF-8'}</span>
                        {/if}
                        {if $display_phone && !empty($store['phone'])}
                            <span class="velo_add_number"><img class="velo-phone-icon" src="{$phone_icon|escape:'htmlall':'UTF-8'}"/> {$store['phone']|escape:'htmlall':'UTF-8'}</span>
                            {/if}
{*                            changes over*}
                            </div>
                                    </div>
                                    
                                  
                                </div>
                            </div>

                            <div style="display: none" class="velo-w3-address-country">{$store['country']|escape:'htmlall':'UTF-8'}</div>
                            <div class="velo-show-more"><strong><a href="javascript:void(0);" id="button-show-more-{$store['id_store']}" class="button-show-more" >{l s='Show More' mod='kbstorelocatorpickup'}</a></strong></div>
                            <div class="extra_content" style="display:none;">
                        {if !empty($store['hours'])}
                                <div  class="kb-store-hours">
                                <span class="velo_add_clock"><img class="velo-phone-icon" src="{$clock_icon|escape:'htmlall':'UTF-8'}"/>{l s='Store Timing' mod='kbstorelocatorpickup'}</span>
                                    <table class="mp-openday-list" style="display: table;">
                                    <tbody>
                                {foreach $store['hours'] as $key => $time}

                                    {if isset($time['hours'])}
                                            <tr>
                                        <td class="mp-openday-list-title"><strong>{$time['day']|escape:'htmlall':'UTF-8'}</td>
                                        <td class="mp-openday-list-value">{if $time['hours'] == ''} <span style="color: red">{l s='Closed' mod='kbstorelocatorpickup'}</span>{else}{$time['hours']|escape:'htmlall':'UTF-8'}{/if}</td>
                                        </tr>
                                    {/if}
                                {/foreach}
                                        </tbody></table>
                            </div>
                        {/if}
                       
                        {* changes by rishabh jain *}
                        {if $is_enabled_website_link}
                                <div class="kb-store-url">
                                    <span class="velo_add_number"><img class="velo-web-icon" src="{$web_icon|escape:'htmlall':'UTF-8'}"/> <a href='{$index_page_link}'>
                                    {$index_page_link}
                                    </a></span>

                            </div>
                        {/if}
                            </div>
                        {* changes over *}
                        <div class="velo_add_distance"></div>
                        
                        <button onclick="focus_and_popup({$random|escape:'htmlall':'UTF-8'})" class="velo-directions-button">{l s='View Map' mod='kbstorelocatorpickup'} <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                        {* changes by rishabh jain *}
                        {if $is_enabled_direction_link}
                                <a  target="_blank" class="velo-directions-link" href="http://google.com/maps/dir/?api=1&amp;destination={$store['latitude']|escape:'url'}%2C{$store['longitude']|escape:'url'}">{l s='Get Direction' mod='kbstorelocatorpickup'} <i class="material-icons">directions</i></a>
                                {/if}
                        {* changes over*}
                        <input type="hidden" name="kb_av_store_details" value='{$av_store_detail|escape:'htmlall':'UTF-8'}'>
                        
                    </li>
                {/foreach}
            {else}
            {/if}
        </ul>
        </div>
    </div>
</div>
</div>
<div id="ComingSoonStore"></div>
<input type="hidden" class="page_controller" value='stores'>
<script >
    //changes by tarun
    {if $is_enabled_show_stores eq 1}
        var kb_all_stores = {$kb_all_stores nofilter}{*Variable contains html content, escape not required*};
    {else}
        var kb_all_stores = '';
    {/if}    
    //changes over 
    var kb_not_found = "{l s='not found' mod='kbstorelocatorpickup'}";
    var show_more = "{l s='Show More' mod='kbstorelocatorpickup'}";
    var hide = "{l s='Hide' mod='kbstorelocatorpickup'}";
    var lang_iso = "{$lang_iso|escape:'htmlall':'UTF-8'}";
    var kb_store_url = "{$kb_store_url|escape:'quotes':'UTF-8'}";
    var locator_icon = "{$locator_icon|escape:'quotes':'UTF-8'}";
    var default_latitude = '{$default_latitude|escape:'htmlall':'UTF-8'}';
    var default_longitude = '{$default_longitude|escape:'htmlall':'UTF-8'}';
    var zoom_level = {$zoom_level|escape:'htmlall':'UTF-8'};
    var current_location_icon = "{$current_location_icon|escape:'quotes':'UTF-8'}";
    var locations = '';

    {if !empty($selected_store)}
    locations = [
        ["<div class='velo-popup'><div class='gm_name'>{if isset($selected_store['name'][$id_lang])}{$selected_store['name'][$id_lang]|escape:'htmlall':'UTF-8'}{else}{$selected_store['name']|escape:'htmlall':'UTF-8'}{/if}</div><div class='gm_address'>{if isset($selected_store['address1'][$id_lang])}{$selected_store['address1'][$id_lang]|escape:'htmlall':'UTF-8'}{else}{$selected_store['address1']}{/if}<br/>{if isset($selected_store['address2'][$id_lang])}{$selected_store['address2'][$id_lang]|escape:'htmlall':'UTF-8'}{else}{$selected_store['address2']}{/if}</div><div class='gm_location'>{if !empty($selected_store['state'])}{$selected_store['state']|escape:'htmlall':'UTF-8'},{/if}{$selected_store['country']|escape:'htmlall':'UTF-8'}</div>{if $display_phone && !empty($selected_store['phone'])}<div class='gm_phone'><img class='velo-phone-icon' src='{$phone_icon|escape:'quotes':'UTF-8'}'>{$selected_store['phone']|escape:'htmlall':'UTF-8'}</div>{/if}{if $is_enabled_website_link}<div class='gm_website'><a href='{$index_page_link}'>{$index_page_link}</a></div>{/if}</div>", {$selected_store['latitude']|escape:'htmlall':'UTF-8'}, {$selected_store['longitude']|escape:'htmlall':'UTF-8'}],
    ];
    {/if}
    var default_content = "{l s='location' mod='kbstorelocatorpickup'}";
    function initialize() {
        var myOptions = {
            center: new google.maps.LatLng(25.6141693, -80.5509604),
            zoom: zoom_level,

        };
        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        {if $is_enabled_show_stores eq 1}
            locations = kb_all_stores;
        {/if}
        setMarkers(map, locations)

    }
    
    function setMarkers(map, locations) {
//     
    if (locations.length == 0) {
        var lat = parseFloat(default_latitude);
        var long = parseFloat(default_longitude);

        latlngset = new google.maps.LatLng(lat, long);
        var icon = {
            url: locator_icon, // url
            scaledSize: new google.maps.Size(40, 40), // scaled size
        };
        var marker = new google.maps.Marker({
            map: map, position: latlngset, icon: icon, animation: google.maps.Animation.DROP
        });
        map.setCenter(marker.getPosition())
        var content = default_content;
        var infowindow = new google.maps.InfoWindow()

        google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
            return function (results, status) {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };

        })(marker, content, infowindow));
    } else {

        var marker, i

        for (i = 0; i < locations.length; i++)
        {

            var loan = locations[i][0]
            var lat = locations[i][1]
            var long = locations[i][2]

            latlngset = new google.maps.LatLng(lat, long);
            var icon = {
                url: locator_icon, // url
                scaledSize: new google.maps.Size(40, 40), // scaled size
            };
            var marker = new google.maps.Marker({
                map: map, title: loan, position: latlngset, icon: icon, animation: google.maps.Animation.DROP
            });
            map.setCenter(marker.getPosition());


            var content = loan;

            var infowindow = new google.maps.InfoWindow()

            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));
        }
    }
    google.maps.event.addListener(marker, 'mouseover', function (e) {
              e.tb.target.parentElement.removeAttribute('title')
           });
}
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={$map_api|escape:'htmlall':'UTF-8'}&callback=initialize">
</script>
<style>
    .velo-location-list ul li{
        background-image:url("{$locator_icon|escape:'quotes':'UTF-8'}"); 
        background-size: 30px;
    }
    .velo-directions-link {
        clear:none !important;
    }
</style>
{/block}

{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2018 Knowband
* @license   see file: LICENSE.txt
*
* Description
*
* Admin tpl file
*}