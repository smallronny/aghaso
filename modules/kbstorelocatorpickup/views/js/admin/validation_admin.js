/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

$(document).ready(function() {
    $('#delivery_form_submit_btn').click(function(){
        var error = false;
        $(".error_message").remove();
        $('input[name="date_to"]').closest('.input-group').removeClass('error_field');

        var date_from = $('input[name="date_from"]').val().trim();
        var date_to = $('input[name="date_to"]').val().trim();
        var from = new Date(date_from);
        var to = new Date(date_to);

        if ((from > to) && date_from !='' && date_to!='')
        {
            error = true;
            $('input[name="date_to"]').closest('.input-group').addClass('error_field');
            $('input[name="date_to"]').closest('.input-group').after('<span class="error_message">'+invalid_from_date+'</span>');
        }
        if (error) {
            $('html, body').animate({
                scrollTop: $(".error_message").offset().top-200
            }, 1000);
            return false;
        }

        if (error) {
           return false;
       }
        $("#delivery_form_submit_btn").attr('disabled', 'disabled');
        $('#delivery_form').submit();
    });


    $('input[name="field_separator"]').on('keypress', function(e){
         if (e.which == 13) e.preventDefault();
    });

    $('select[name="delivery_slip_store"]').closest('.form-group').hide();

    $('select[name="delivery_slip_carrier"]').change(function(){
        if ($(this).val() == $('input[name="pickup_carrier_id"]').val()) {
            $('select[name="delivery_slip_store"]').closest('.form-group').show();
        } else {
            $('select[name="delivery_slip_store"]').closest('.form-group').hide();
        }
    }).change();

    $('button[name="generalsettingsubmitkbstorelocatorpickup"]').click(function(){
    var is_error2 = 0;
        var error = false;
        $(".error_message").remove();
        $(".error_message1").remove();
        $('input[name="kbstorelocator[api_key]"]').removeClass('error_field');
        $('select[name="kbstorelocator[default_store]"]').removeClass('error_field');
        $('input[name="map_marker"]').closest('.form-group').find('.input-group').removeClass('error_field');
        // changes by rishabh jain
        $('input[name="banner_image"]').closest('.form-group').find('.input-group').removeClass('error_field');
        // changes over
        $('#multiple_select_avilable_store_chosen').removeClass('error_field');
        $('input[name^="header_menu_text_"]').removeClass('error_field');
        var api_key_empty = velovalidation.checkMandatory($('input[name="kbstorelocator[api_key]"]'));
        if (api_key_empty != true) {
            error = true;
            $('input[name="kbstorelocator[api_key]"]').addClass('error_field');
            $('input[name="kbstorelocator[api_key]"]').after('<span class="error_message">' + api_key_empty + '</span>');
        }

        if ($('input[name="map_marker"]').prop('files').length) {
            validate_image = velovalidation.checkImage($('input[name="map_marker"]'));
            if (validate_image != true) {
                error = true;
                $('input[name="map_marker"]').closest('.form-group').find('.input-group').addClass('error_field');
                $('input[name="map_marker"]').closest('.form-group').after('<span class="error_message1">' + validate_image + '</span>');
            }
        }
        // changes by rishabh jain
        if ($('input[name="banner_image"]').prop('files').length) {
            validate_image = velovalidation.checkImage($('input[name="banner_image"]'));
            if (validate_image != true) {
                error = true;
                $('input[name="banner_image"]').closest('.form-group').find('.input-group').addClass('error_field');
                $('input[name="banner_image"]').closest('.form-group').after('<span class="error_message1">' + validate_image + '</span>');
            }
        }
        // changes over

        if ($('select[name="kbstorelocator[avilable_store][]"]').val() == null) {
             error = true;
            $('#multiple_select_avilable_store_chosen').addClass('error_field');
            $('select[name="kbstorelocator[avilable_store][]"]').closest('.col-lg-9').append('<p class="error_message">'+available_store_empty+'</p>');
        } else {
            var is_enabled_store = false;
            $.each($('select[name="kbstorelocator[avilable_store][]"]').val(), function(e, i){
                if (i == $('select[name="kbstorelocator[default_store]"]').val()) {
                    is_enabled_store = true;
                }
            });
            if (!is_enabled_store) {
                 error = true;
                $('select[name="kbstorelocator[default_store]"]').addClass('error_field');
                $('select[name="kbstorelocator[default_store]"]').after('<p class="error_message">'+enable_store_not_selected+'</p>');
            }
        }

        if ($("input[name='kbstorelocator[header_menu]']:checked").val() == 1) {
            $('input[name^="header_menu_text_"]').each(function () {
                    var header_mand = velovalidation.checkMandatory($(this));
                    if (header_mand != true) {
                        error = true;
                        if (is_error2 < 1) {
                            $(this).parents('.col-lg-9').last().append('<p class="error_message">' + header_mand + ' ' + check_for_all + '</p>');
                            is_error2++;
                        }
                        $(this).addClass('error_field');
                    }
            });
        }

        if (error) {
            $('html, body').animate({
                scrollTop: $(".error_message").offset().top-200
            }, 1000);
            return false;
        }

        if (error) {
           return false;
       }
        $("button[name='generalsettingsubmitkbstorelocatorpickup']").attr('disabled', 'disabled');
        $('#general_configuration_form').submit();

    });
    var is_error = 0;
    $('button[name="submitpickuptimesetting"]').click(function () {
        var error = false;
        $(".error_message").remove();
//        $('input[name^="format_"]').removeClass('error_field');
        $('input[name="kbslpickuptime[days_gap]"]').removeClass('error_field');
        $('input[name="kbslpickuptime[maximum_days]"]').removeClass('error_field');
        $('input[name="kbslpickuptime[format]"]').removeClass('error_field');
        $('input[name="kbslpickuptime[hours_gap]"]').closest('.input-group').removeClass('error_field');


        var day_gap_error = velovalidation.checkMandatory($('input[name="kbslpickuptime[days_gap]"]'));
        if (day_gap_error != true) {
            error = true;
            $('input[name="kbslpickuptime[days_gap]"]').addClass('error_field');
            $('input[name="kbslpickuptime[days_gap]"]').after('<span class="error_message">'+day_gap_error+'</span>');
        } else {
            var day_gap_num = velovalidation.isNumeric($('input[name="kbslpickuptime[days_gap]"]'));
            if (day_gap_num != true) {
                error = true;
                $('input[name="kbslpickuptime[days_gap]"]').addClass('error_field');
                $('input[name="kbslpickuptime[days_gap]"]').after('<span class="error_message">'+day_gap_num+'</span>');
            }
        }

        var maximum_days_error = velovalidation.checkMandatory($('input[name="kbslpickuptime[maximum_days]"]'));
        if (maximum_days_error != true) {
            error = true;
            $('input[name="kbslpickuptime[maximum_days]"]').addClass('error_field');
            $('input[name="kbslpickuptime[maximum_days]"]').after('<span class="error_message">'+day_gap_error+'</span>');
        } else {
            var maximum_days_num = velovalidation.isNumeric($('input[name="kbslpickuptime[maximum_days]"]'));
            if (maximum_days_num != true) {
                error = true;
                $('input[name="kbslpickuptime[maximum_days]"]').addClass('error_field');
                $('input[name="kbslpickuptime[maximum_days]"]').after('<span class="error_message">'+maximum_days_num+'</span>');
            } else {
                var day_gap = parseInt($('input[name="kbslpickuptime[days_gap]"]').val().trim());
                var maximum_days = parseInt($('input[name="kbslpickuptime[maximum_days]"]').val().trim());
                if (maximum_days <= day_gap) {
                    error = true;
                    $('input[name="kbslpickuptime[maximum_days]"]').addClass('error_field');
                    $('input[name="kbslpickuptime[maximum_days]"]').after('<span class="error_message">'+maximum_days_less_error+'</span>');
                }
            }
        }

        if ($('input[name="kbslpickuptime[time_slot]"]:checked').val() == '1') {
            var hours_gap_error = velovalidation.checkMandatory($('input[name="kbslpickuptime[hours_gap]"]'));
            if (hours_gap_error != true) {
                error = true;
                $('input[name="kbslpickuptime[hours_gap]"]').closest('.input-group').addClass('error_field');
                $('input[name="kbslpickuptime[hours_gap]"]').closest('.input-group').after('<span class="error_message">' + hours_gap_error + '</span>');
            } else {
                var hours_gap_num = velovalidation.isNumeric($('input[name="kbslpickuptime[hours_gap]"]'));
                if (hours_gap_num != true) {
                    error = true;
                    $('input[name="kbslpickuptime[hours_gap]"]').closest('.input-group').addClass('error_field');
                    $('input[name="kbslpickuptime[hours_gap]"]').closest('.input-group').after('<span class="error_message">' + hours_gap_num + '</span>');
                }
            }
        }

        var label_error = velovalidation.checkMandatory($('input[name="kbslpickuptime[format]"]'));
        if (label_error != true) {
            error = true;
            $('input[name="kbslpickuptime[format]"]').after('<span class="error_message">' + label_error + '</span>');
            $('input[name="kbslpickuptime[format]"]').addClass('error_field');
        }

        if (error) {
            $('html, body').animate({
                scrollTop: $(".error_message").offset().top-200
            }, 1000);
            return false;
        }

        if (error) {
            return false;
        } else {
            var date_format = $('input[name="kbslpickuptime[format]"]').val().trim();
            date_format = date_format.replace('%y', 'Y');
            date_format = date_format.replace('%m', 'm');
            date_format = date_format.replace('%d', 'd');
            date_format = date_format.replace('%t24', 'H:i');
            date_format = date_format.replace('%t12', 'h:i A');
            if (date_format != '' && (typeof admin_pickup_url != 'undefined')) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    url: admin_pickup_url + '&validateAdminDateFormart=1',
                    data: {dateformat: date_format},
                    success: function (rec) {
                        if (!rec) {
                            error = true;
                            $('input[name="kbslpickuptime[format]"]').addClass('error_field');
                            $('input[name="kbslpickuptime[format]"]').after('<span class="error_message">'+kb_date_format_invalid+'</span>');
                            return false;
                        } else {
                             $("button[name='submitpickuptimesetting']").attr('disabled', 'disabled');
                            $('#configuration_form').submit();
                        }
                    }
                });
            }
            return false;
        }

    });


   $('button[name="submitslimporter"]').click(function(){
        var error = false;
        $(".error_message").remove();
        $('input[name="field_separator"]').removeClass('error_field');
        $('input[name="csv_file"]').closest('.form-group').find('.input-group').removeClass('error_field');

        var separator_err = velovalidation.checkMandatory($('input[name="field_separator"]'));
        if (separator_err != true) {
            error = true;
            $('input[name="field_separator"]').addClass('error_field');
            $('input[name="field_separator"]').after('<span class="error_message">'+separator_err+'</span>');
        }

        if (!$('input[name="csv_file"]').prop('files').length) {
             error = true;
            $('input[name="csv_file"]').closest('.form-group').find('.input-group').addClass('error_field');
            $('input[name="csv_file"]').closest('.form-group').after('<span class="error_message">'+csv_file_empty+'</span>');
        }

        if (error) {
            return false;
        }
        $("button[name='submitslimporter']").attr('disabled', 'disabled');
        $('#configuration_form').submit();
   });

   $('button[name="submitAddkb_pickup_at_store_email_template"]').click(function(){
       var error = false;
         $(".error_message").remove();
         $('input[name^="subject_"]').removeClass('error_field');;
        $('[name^="email_template"]').removeClass('error_field');
        var is_error = 0;
       $('input[name^="subject_"]').each(function () {
            var subject_error = velovalidation.checkMandatory($(this));
            if (subject_error != true) {
                error = true;
                if (is_error < 1) {
                    $(this).parents('.col-lg-9').last().append('<span class="error_message">' + subject_error + ' ' + check_for_all + '</span>');
                    is_error++;
                }
                $(this).addClass('error_field');
            }
        });

        var first_err4 = 0;
        $("[name^=email_template_]").each(function () {
            $(this).parent().find('.mce-tinymce').removeClass('error_field');
            var email_err1 = checkTinyMCERequired(tinyMCE.get($(this).attr("id")).getContent());
            if (email_err1 != true) {
                $(this).parent().find('.mce-tinymce').addClass('error_field');
                if (first_err4 == 0) {
                    $('<p class="error_message">' + please_check_kb_fields + '</p>').insertBefore($('textarea[name^="email_template_"]').closest('.form-group').parent().find('.help-block'));
                }
                first_err4 = 1;
                error = true;
            }
        });

         if (error) {
            $('html, body').animate({
                scrollTop: $(".error_message").offset().top - 200
            }, 1000);
            return false;
        }


        /*Knowband button validation start*/
        $("button[name='submitAddkb_pickup_at_store_email_template']").attr('disabled', 'disabled');
        $('#kb_pickup_at_store_email_template_form').submit();


   });

});

function checkTinyMCERequired(val) {
    var new_str = str_replace_all(val, '<p>', '');
    new_str = str_replace_all(new_str, '</p>', '');
    new_str = new_str.trim();
    var return_val = true;
    if (new_str == '') {
        return_val = empty_field;
    }
    return return_val;
}

function str_replace_all(string, str_find, str_replace){
    try{
        return string.replace(new RegExp(str_find, "gi"), str_replace);
    } catch(ex){
        return string;
    }
}