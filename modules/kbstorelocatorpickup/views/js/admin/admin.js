/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */




$(document).ready(function() {
    if (typeof kb_placeholder_text != 'undefined') {
        $("#multiple-select-avilable_store").chosen({
            placeholder_text: kb_placeholder_text,
            no_results_text: no_results_text,
        });
    }

    $('input[name="date_from"],input[name="date_to"]').datepicker({
        beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        'z-index': 1031
                    });
                }, 0);
            },
            prevText: '',
            nextText: '',
            dateFormat: 'yy-mm-dd',
            // Define a custom regional settings in order to use PrestaShop translation tools
            currentText: currentText,
            closeText: closeText,
            ampm: false,
            amNames: ['AM', 'A'],
            pmNames: ['PM', 'P'],
            timeFormat: 'hh:mm:ss tt',
            timeSuffix: '',
            timeOnlyTitle: timeOnlyTitle,
            timeText: timeText,
            hourText: hourText,
            minuteText: minuteText,
    });

    $('input[name="date_from"],input[name="date_to"]').attr('readonly', true);


    if (typeof download_sample_label != 'undefined') {
        $('input[name="field_separator"]').closest('.col-lg-3').append("<div><button class='btn btn-info pull-left' name='kbdownloadsample'><i class='icon-arrow-circle-o-down'></i> "+download_sample_label+"</button></div>");
    }


    $('input[name="map_marker"]').on('change', function() {
        $("input[name='map_marker']").find('.input-group').removeClass('error_field');
        var imgPath = $(this)[0].value;
        $('.error_message1').remove();
        var image_holder = $("#kbslmarker");
        if (($("input[name='map_marker']").prop("files").length)) {
            var validate_image = velovalidation.checkImage($(this), 2097152, 'kb');
            if (validate_image != true) {
                $('input[name="filename"]').val('');
                showErrorMessage(validate_image);
                $("input[name='map_marker']").closest('.form-group').find('.input-group').addClass('error_field');
                $('input[name="map_marker"]').closest('.form-group').after('<span class="error_message1">' + validate_image + '</span>');
            } else {
                $("input[name='map_marker']").parents('.form-group').removeClass('error_field');
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#kbslmarker').attr('src', e.target.result);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
            }
        }
    });
    // changes  by rishabh jain
    $('input[name="banner_image"]').on('change', function () {
        $("input[name='banner_image']").find('.input-group').removeClass('error_field');
        var imgPath = $(this)[0].value;
        $('.error_message1').remove();
        var image_holder = $("#kbslstorelogo");
        if (($("input[name='banner_image']").prop("files").length)) {
            var validate_image = velovalidation.checkImage($(this), 2097152, 'kb');
            if (validate_image != true) {
                $('input[name="filename"]').val('');
                showErrorMessage(validate_image);
                $("input[name='banner_image']").closest('.form-group').find('.input-group').addClass('error_field');
                $('input[name="banner_image"]').closest('.form-group').after('<span class="error_message1">' + validate_image + '</span>');
            } else {
                $("input[name='banner_image']").parents('.form-group').removeClass('error_field');
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#kbslstorelogo').attr('src', e.target.result);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
            }
        }
    });
    // changes over
    if ($("input[name='kbslpickuptime[time_slot]']:checked").val() == 1) {
        $("input[name='kbslpickuptime[hours_gap]']").closest('.form-group').show();
    } else if ($("input[name='kbslpickuptime[time_slot]']:checked").val() == 0) {
        $("input[name='kbslpickuptime[hours_gap]']").closest('.form-group').hide();
    }

    $("input[name='kbslpickuptime[time_slot]']").click(function () {
        if ($("input[name='kbslpickuptime[time_slot]']:checked").val() == 1) {
            $("input[name='kbslpickuptime[hours_gap]']").closest('.form-group').show();
        } else {
            $("input[name='kbslpickuptime[hours_gap]']").closest('.form-group').hide();
        }
    });
    // changes by rishabh jain
    if ($("input[name='kbstorelocator[display_store_banner]']:checked").val() == 1) {
        $("input[name='banner_image']").parent().parent().parent().parent().show();
    } else if ($("input[name='kbstorelocator[display_store_banner]']:checked").val() == 0) {
        $("input[name='banner_image']").parent().parent().parent().parent().hide();
    }

    $("input[name='kbstorelocator[display_store_banner]']").click(function () {
        if ($("input[name='kbstorelocator[display_store_banner]']:checked").val() == 1) {
            $("input[name='banner_image']").parent().parent().parent().parent().show();
        } else {
            $("input[name='banner_image']").parent().parent().parent().parent().hide();
        }
    });

// changes over
    if ($("input[name='kbstorelocator[header_menu]']:checked").val() == 1) {
        $("input[name^='header_menu_text_']").parents('.form-group').show();
    }
    if ($("input[name='kbstorelocator[header_menu]']:checked").val() == 0) {
        $("input[name^='header_menu_text_']").parents('.form-group').hide();
    }

    $("input[name='kbstorelocator[header_menu]']").click(function () {
        if ($("input[name='kbstorelocator[header_menu]']:checked").val() == 1) {
            $("input[name^='header_menu_text_']").parents('.form-group').show();
        } else {
            $("input[name^='header_menu_text_']").parents('.form-group').hide();
        }
    });


});
