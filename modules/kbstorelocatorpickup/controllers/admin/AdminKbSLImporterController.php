<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

//include_once(_PS_MODULE_DIR_.'kbproductfield/classes/KbProductCustomFields.php');

class AdminKbSLImporterController extends ModuleAdminController
{
    
    public function __construct()
    {
        $this->context = Context::getContext();
        $this->bootstrap = true;
        $this->className = 'Configuration';
        $this->table = 'configuration';

        parent::__construct();
        $this->toolbar_title = $this->module->l('Import Store Contact', 'AdminKbSLImporterController');
    }
    
    
    /**
     * Prestashop Default Function in AdminController.
     * Assign smarty variables for all default views, list and form, then call other init functions
     */
    public function initContent()
    {
//        changes by tarun      
        $this->warnings[] = $this->l('Before uploading file please check the format of the file. It should be same as the sample.csv');
        $this->warnings[] = $this->l('Before uploading file please check the read and write permissions of upload.csv files in views/uploads folder of the module');
        //changes over
        if (isset($this->context->cookie->kb_redirect_error)) {
            $this->errors[] = $this->context->cookie->kb_redirect_error;
            unset($this->context->cookie->kb_redirect_error);
        }

        if (isset($this->context->cookie->kb_redirect_success)) {
            $this->confirmations[] = $this->context->cookie->kb_redirect_success;
            unset($this->context->cookie->kb_redirect_success);
        }
        
        $this->context->smarty->assign('selected_nav', 'slstoreimport');
        $this->context->smarty->assign(
            'admin_sl_setting_controller',
            $this->context->link->getAdminLink('AdminModules', true)
            .'&configure='.urlencode($this->module->name)
            .'&tab_module='.$this->module->tab
            .'&module_name='.urlencode($this->module->name)
        );
        $this->context->smarty->assign(
            'admin_sl_delivery_slip_controller',
            $this->context->link->getAdminLink('AdminKbSLDeliverySlip', true)
        );
        $this->context->smarty->assign(
            'admin_sl_email_controller',
            $this->context->link->getAdminLink('AdminKbSLEmailNotification', true)
        );
        $this->context->smarty->assign(
            'admin_sl_pickup_time_controller',
            $this->context->link->getAdminLink('AdminKbSLTimeSetting', true)
        );
        $this->context->smarty->assign(
            'admin_sl_importer_controller',
            $this->context->link->getAdminLink('AdminKbSLImporter', true)
        );
        $kb_tabs = $this->context->smarty->fetch(
            _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/kb_tabs.tpl'
        );
        $this->content .= $kb_tabs;
        
        $this->content .= $this->kbImportSettingForm();
        
        
        parent::initContent();
    }
    
    /*
     * function to create import csv form
     */
    protected function kbImportSettingForm()
    {
        $formFields = array(
            array(
                'type' => 'text',
                'label' => $this->module->l('Field Separator', 'AdminKbSLImporterController'),
                'name' => 'field_separator',
                'required' => true,
                'col' => 3,
                'desc' => $this->module->l('Enter the separator which is used in your CSV file that separates each field or column', 'AdminKbSLImporterController').'<br>E.g: 1;Joe;32',
            ),
            array(
                'type' => 'file',
                'label' => $this->module->l('CSV File', 'AdminKbSLImporterController'),
                'required'=> true,
                'name' => 'csv_file',
                'required' => true,
                'desc' => $this->module->l('Upload CSV file to import Store Contacts', 'AdminKbSLImporterController'),
            ),
        );

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->module->l('Import Store Contacts', 'AdminKbSLImporterController'),
                    'icon' => 'icon-upload'
                ),
                'input' => $formFields,
                'submit' => array(
                    'class' => 'btn btn-default pull-right',
                    'title' => $this->module->l('Save', 'AdminKbSLImporterController')
                )
            )
        );
 
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->submit_action = 'submitslimporter';
        $helper->default_form_language = $this->context->language->id;
        $helper->fields_value['field_separator'] = ',';
        $helper->fields_value['csv_file'] = '';

        return $helper->generateForm(array($fields_form));
    }
    
    
    public function postProcess()
    {
        if (Tools::isSubmit('kbdownloadsample')) {
            $this->downloadKbSampleCSV();
        }
        if (Tools::isSubmit('submitslimporter')) {
            return $this->processImportKbCSV();
        }
        return parent::postProcess();
    }
    
    /**
     * Function used to import CSV File
     */
    protected function processImportKbCSV()
    {
//        d($_FILES);
        $csv_mimetypes = array(
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'text/anytext',
            'application/octet-stream',
            'application/txt',
        );
        if ($_FILES['csv_file']['size'] == 0) {
            $this->errors[] = $this->module->l('File is empty', 'AdminKbSLImporterController');
        } elseif (!in_array($_FILES['csv_file']['type'], $csv_mimetypes)) {
            $this->errors[] = $this->module->l('File is not supported', 'AdminKbSLImporterController');
        } elseif ($_FILES['csv_file']['error'] != 0) {
            $this->errors[] = $this->module->l('File is not valid', 'AdminKbSLImporterController');
        }
        $existing_message = '';
        if (is_array($this->errors) && empty($this->errors)) {
            if ($_FILES['csv_file']['error'] == 0) {
                $path = _PS_MODULE_DIR_ . $this->module->name . '/views/upload/upload.csv';
                $is_upload = move_uploaded_file(
                    $_FILES['csv_file']['tmp_name'],
                    $path
                );
                chmod(_PS_MODULE_DIR_ . $this->module->name . '/views/upload/upload.csv', 0777);
                if ($is_upload) {
                    $csv_array = $this->readCSVtoArray($path, Tools::getValue('field_separator'));
                    //changes by tarun to skip the array of the translated text from the csv file
                    if (is_array($csv_array) && !empty($csv_array)) {
                        $file_process = array();
//                        Tools::dieObject($csv_array, true);
                        foreach ($csv_array as $key => $array) {
                            if (is_array($array) && !empty($array) && $key != 0) {
                                if (version_compare(_PS_VERSION_, '1.7.3.0', '>=')) {
                                    $hours = array();
                                    $address1 = array();
                                    $address2 = array();
                                    $name = array();
                                    $note = array();
                                    $file_process[] = $array;
                                    $postcode = trim($array['pincode']);
                                    $city = trim($array['city']);
                                    $country_iso = trim($array['country_iso_code']);
                                    $id_country = Country::getByIso($country_iso);
                                    $state_iso = trim($array['state_iso_code']);
                                    $id_state = 0;
                                    if (!empty($id_country)) {
                                        $id_state = State::getIdByIso($state_iso, $id_country);
                                    } else {
                                        $id_state = State::getIdByIso($state_iso);
                                    }
                                    $latitude = number_format((float) trim($array['latitude']), 8);
                                    $longitude = number_format((float) trim($array['longitude']), 8);
                                    $phone = trim($array['phone']);
                                    $fax = trim($array['fax']);
                                    $email = trim($array['email']);
                                    $active = trim($array['active']);
                                    $image = trim($array['image']);
                                    $language = Language::getLanguages(false);
                                    $error = false;
                                    foreach ($language as $lang) {
                                        $data_name = trim($array['name_'.$lang['iso_code']]);
                                        $data_address1 = trim($array['address1_'.$lang['iso_code']]);
                                        if (Tools::isEmpty($data_name) || Tools::isEmpty($data_address1)) {
                                            $error = true;
                                            $pos = $key+1;
                                            $msg = $this->module->l('Required Columns cannot be empty for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif (!empty($data_name) && !Validate::isGenericName($data_name)) {
                                            $error = true;
                                            $pos = $key+1;
                                            $msg = $this->module->l('name is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        }
                                        $note[$lang['id_lang']] = trim($array['note_'.$lang['iso_code']]);
                                        $name[$lang['id_lang']] = $data_name;
                                        $address1[$lang['id_lang']] = $data_address1;
                                        $address2[$lang['id_lang']] = trim($array['address2_'.$lang['iso_code']]);
                                        $monday = trim($array['monday_'.$lang['iso_code']]);
                                        $tuesday = trim($array['tuesday_'.$lang['iso_code']]);
                                        $wednesday = trim($array['wednesday_'.$lang['iso_code']]);
                                        $thrusday = trim($array['thrusday_'.$lang['iso_code']]);
                                        $friday = trim($array['friday_'.$lang['iso_code']]);
                                        $saturday = trim($array['saturday_'.$lang['iso_code']]);
                                        $sunday = trim($array['sunday_'.$lang['iso_code']]);
                                        $hours[$lang['id_lang']] = json_encode(array(
                                            '0' => array($monday),
                                            '1' => array($tuesday),
                                            '2' => array($wednesday),
                                            '3' => array($thrusday),
                                            '4' => array($friday),
                                            '5' => array($saturday),
                                            '6' => array($sunday)
                                        ));
                                    }
                                    $country = new Country((int) $id_country);
                                    if (Tools::isEmpty($city) || Tools::isEmpty($id_country) || ($latitude == '') || ($longitude == '')) {
                                        $error = true;
                                        $pos = $key+1;
                                        $msg = $this->module->l('Required Columns cannot be empty for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif ($id_country && $country && !(int) $country->contains_states && $id_state) {
                                        $error = true;
                                        $pos = $key+1;
                                        $msg = $this->module->l('id_country cannot be empty or is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif ($country->zip_code_format && !$country->checkZipCode($postcode)) {
                                        $error = true;
                                        $pos = $key + 1;
                                        $msg = $this->module->l('Format of the Zipcode is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif (empty($postcode) && $country->need_zip_code) {
                                        $error = true;
                                        $pos = $key + 1;
                                        $msg = $this->module->l('Zipcode cannot be empty for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif ($postcode && !Validate::isPostCode($postcode)) {
                                        $error = true;
                                        $pos = $key + 1;
                                        $msg = $this->module->l('Zipcode is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif ($email && !Validate::isEmail($email)) {
                                        $error = true;
                                        $pos = $key + 1;
                                        $msg = $this->module->l('Email is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif (!empty($phone) && !validate::isPhoneNumber($phone)) {
                                        $error = true;
                                        $pos = $key + 1;
                                        $msg = $this->module->l('Phone number is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif (!empty($latitude) && !validate::isCoordinate($latitude)) {
                                        $error = true;
                                        $pos = $key + 1;
                                        $msg = $this->module->l('Latitude is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    } elseif (!empty($longitude) && !validate::isCoordinate($longitude)) {
                                        $error = true;
                                        $pos = $key + 1;
                                        $msg = $this->module->l('Longitude is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                        $this->context->cookie->__set('kb_redirect_error', $msg);
                                    }
                                    if (!$error) {
                                        $kbstore = new Store();
                                        $kbstore->name = $name;
                                        $kbstore->id_country = $id_country;
                                        $kbstore->id_state = $id_state;
                                        $kbstore->address1 = $address1;
                                        $kbstore->address2 = $address2;
                                        $kbstore->postcode = $postcode;
                                        $kbstore->city = $city;
                                        $kbstore->latitude = $latitude;
                                        $kbstore->longitude = $longitude;
                                        $kbstore->hours = $hours;
                                        $kbstore->phone = $phone;
                                        $kbstore->fax = $fax;
                                        $kbstore->note = $note;
                                        $kbstore->email = $email;
                                        $kbstore->active = $active;
                                        if ($kbstore->add()) {
                                            $id_store = $kbstore->id;
                                            $kbstore->id_store = $id_store;
                                            $kbstore->store_name = $name;
                                            Kbstorelocatorpickup::kbStoreAddressMapping((array) $kbstore);
                                            if (!empty($image)) {
                                                $img_path = _PS_STORE_IMG_DIR_ . $id_store . '.jpg';
                                                $img_medium_path = _PS_STORE_IMG_DIR_ . $id_store . '-' . ImageType::getFormatedName('medium') . '.jpg';
                                                $data = Tools::file_get_contents($image);
                                                $file = file_put_contents($img_path, $data);
                                                if ($file) {
                                                    copy($img_path, $img_medium_path);
                                                }
                                                $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');
                                                if ($file) {
                                                    if (($id_store = (int) $id_store) && file_exists(_PS_STORE_IMG_DIR_ . $id_store . '.jpg')) {
                                                        $images_types = ImageType::getImagesTypes('stores');
                                                        foreach ($images_types as $image_type) {
                                                            ImageManager::resize(
                                                                _PS_STORE_IMG_DIR_ . $id_store . '.jpg',
                                                                _PS_STORE_IMG_DIR_ . $id_store . '-' . Tools::stripslashes($image_type['name']) . '.jpg',
                                                                (int) $image_type['width'],
                                                                (int) $image_type['height']
                                                            );
                                                            if ($generate_hight_dpi_images) {
                                                                ImageManager::resize(
                                                                    _PS_STORE_IMG_DIR_ . $id_store . '.jpg',
                                                                    _PS_STORE_IMG_DIR_ . $id_store . '-' . Tools::stripslashes($image_type['name']) . '2x.jpg',
                                                                    (int) $image_type['width'] * 2,
                                                                    (int) $image_type['height'] * 2
                                                                );
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbSLImporter', true));
                                    }
                                } else {
                                    if ($key == 1) {
                                        continue;
                                    }
                                    //changes over
                                    if (!isset($array[0]) || !isset($array[1]) || !isset($array[2]) || !isset($array[3])) {
                                        continue;
                                    } else {
                                        $file_process[] = $array;
                                        $name = trim($array[0]);
                                        $address1 = trim($array[1]);
                                        $address2 = trim($array[2]);
                                        $postcode = trim($array[3]);
                                        $city = trim($array[4]);
                                        $id_country = trim($array[5]);
                                        $id_state = trim($array[6]);
                                        $latitude = number_format((float) trim($array[7]), 8);
                                        $longitude = number_format((float) trim($array[8]), 8);
                                        $phone = trim($array[9]);
                                        $fax = trim($array[10]);
                                        $email = trim($array[11]);
                                        $note = trim($array[12]);
                                        $active = trim($array[13]);
                                        $image = trim($array[14]);
                                        $monday = trim($array[15]);
                                        $tuesday = trim($array[16]);
                                        $wednesday = trim($array[17]);
                                        $thrusday = trim($array[18]);
                                        $friday = trim($array[19]);
                                        $saturday = trim($array[20]);
                                        $sunday = trim($array[21]);

                                        $country = new Country((int) $id_country);
                                        $error = false;
                                        if (($name == '') || ($address1 == '') || ($city == '') || ($id_country == '') || ($id_state == '') || ($latitude == '') || ($longitude == '')) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Required Columns cannot be empty for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif (!empty($name) && !Validate::isName($name)) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('name is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif ($id_country && $country && !(int) $country->contains_states && $id_state) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('id_country cannot be empty or is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif ($country->zip_code_format && !$country->checkZipCode($postcode)) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Format of the Zipcode is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif (empty($postcode) && $country->need_zip_code) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Zipcode cannot be empty for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif ($postcode && !Validate::isPostCode($postcode)) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Zipcode is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif ($email && !Validate::isEmail($email)) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Email is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif (!empty($phone) && !validate::isPhoneNumber($phone)) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Phone number is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif (!empty($latitude) && !validate::isCoordinate($latitude)) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Latitude is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        } elseif (!empty($longitude) && !validate::isCoordinate($longitude)) {
                                            $error = true;
                                            $pos = $key + 1;
                                            $msg = $this->module->l('Longitude is not valid for the corresponding row:', 'AdminKbSLImporterController') . $pos;
                                            $this->context->cookie->__set('kb_redirect_error', $msg);
                                        }
                                        if (!$error) {
                                            $hours = array(
                                                '0' => array($monday),
                                                '1' => array($tuesday),
                                                '2' => array($wednesday),
                                                '3' => array($thrusday),
                                                '4' => array($friday),
                                                '5' => array($saturday),
                                                '6' => array($sunday)
                                            );
                                            $kbstore = new Store();
                                            $kbstore->name = $name;
                                            $kbstore->id_country = $id_country;
                                            $kbstore->id_state = $id_state;
                                            $kbstore->address1 = $address1;
                                            $kbstore->address2 = $address2;
                                            $kbstore->postcode = $postcode;
                                            $kbstore->city = $city;
                                            $kbstore->latitude = $latitude;
                                            $kbstore->longitude = $longitude;
                                            $kbstore->hours = json_encode($hours);
                                            $kbstore->phone = $phone;
                                            $kbstore->fax = $fax;
                                            $kbstore->note = $note;
                                            $kbstore->email = $email;
                                            $kbstore->active = $active;
                                            if ($kbstore->add()) {
                                                $id_store = $kbstore->id;
                                                $kbstore->id_store = $id_store;
                                                Kbstorelocatorpickup::kbStoreAddressMapping((array) $kbstore);

                                                if (!empty($image)) {
                                                    $img_path = _PS_STORE_IMG_DIR_ . $id_store . '.jpg';
                                                    $img_medium_path = _PS_STORE_IMG_DIR_ . $id_store . '-' . ImageType::getFormatedName('medium') . '.jpg';
                                                    $data = Tools::file_get_contents($image);
                                                    $file = file_put_contents($img_path, $data);
                                                    if ($file) {
                                                        copy($img_path, $img_medium_path);
                                                    }
                                                    $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');
                                                    if ($file) {
                                                        if (($id_store = (int) $id_store) && file_exists(_PS_STORE_IMG_DIR_ . $id_store . '.jpg')) {
                                                            $images_types = ImageType::getImagesTypes('stores');
                                                            foreach ($images_types as $image_type) {
                                                                ImageManager::resize(
                                                                    _PS_STORE_IMG_DIR_ . $id_store . '.jpg',
                                                                    _PS_STORE_IMG_DIR_ . $id_store . '-' . Tools::stripslashes($image_type['name']) . '.jpg',
                                                                    (int) $image_type['width'],
                                                                    (int) $image_type['height']
                                                                );
                                                                if ($generate_hight_dpi_images) {
                                                                    ImageManager::resize(
                                                                        _PS_STORE_IMG_DIR_ . $id_store . '.jpg',
                                                                        _PS_STORE_IMG_DIR_ . $id_store . '-' . Tools::stripslashes($image_type['name']) . '2x.jpg',
                                                                        (int) $image_type['width'] * 2,
                                                                        (int) $image_type['height'] * 2
                                                                    );
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbSLImporter', true));
                                        }
                                    }
                                }
                            }
                        }
                        //changes by tarun
                        if (is_array($file_process) && empty($file_process)) {
                            $this->context->cookie->__set('kb_redirect_error', $this->module->l('Not able to process file. Columns cannot be empty or check file separator.', 'AdminKbSLImporterController'));
                        } else {
                            $this->context->cookie->__set('kb_redirect_success', $this->module->l('File is successfully imported.', 'AdminKbSLImporterController') . ' ' . $existing_message);
                        }
                        //changes over
                        Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbSLImporter', true));
                    } else {
                        $this->errors[] = $this->module->l('Unable to read file', 'AdminKbSLImporterController');
                    }
                }
            }
        }
    }
    
    
    /**
     * Function used to convert CSV into Array
     */
    public function readCSVtoArray($csv_file, $delemietr)
    {
        $file_handle = fopen($csv_file, 'r');
        $line_of_text = array();
        if (version_compare(_PS_VERSION_, '1.7.3.0', '>=')) {
            $header = fgetcsv($file_handle, '100000', $delemietr);
//            Tools::dieObject($header, false);
            while (!feof($file_handle)) {
                $data = fgetcsv($file_handle, '100000', $delemietr);
                if (is_array($data)) {
                    $line_of_text[] = array_combine($header, $data);
                }
            }
        } else {
            while (!feof($file_handle)) {
                $data = fgetcsv($file_handle, '100000', $delemietr);
                if (is_array($data)) {
                    $line_of_text[] = $data;
                }
            }
        }
        fclose($file_handle);
        return $line_of_text;
    }

    /*
     * Function to download Sample CSV
     */
    public function downloadKbSampleCSV()
    {
        $filename = "Sample.csv";
        $file = fopen('php://output', 'w');
        header("Content-Transfer-Encoding: Binary");
        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename=' . basename($filename));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        ob_clean();
        if (version_compare(_PS_VERSION_, '1.7.3.0', '>=')) {
            //create sample csv file
            $languages = Language::getLanguages(false);
            
            $data = array(
                'pincode' => '99501',
                'city' => 'Alaska',
                'country_iso_code' => 'US',
                'state_iso_code' => 'AK',
                'latitude' => '25.765005',
                'longitude' => '-80.243797',
                'phone' => '',
                'fax' => '',
                'email' => 'samplestore@xyz.com',
                'active' => '1',
                'image' => 'http://yourlinktotheimage.com/img1.jpg',
            );
            $data_translated = array(
                $this->module->l('pincode', 'AdminKbSLImporterController'),
                $this->module->l('city', 'AdminKbSLImporterController'),
                $this->module->l('country_iso_code', 'AdminKbSLImporterController'),
                $this->module->l('state_iso_code', 'AdminKbSLImporterController'),
                $this->module->l('latitude', 'AdminKbSLImporterController'),
                $this->module->l('longitude', 'AdminKbSLImporterController'),
                $this->module->l('phone', 'AdminKbSLImporterController'),
                $this->module->l('fax', 'AdminKbSLImporterController'),
                $this->module->l('email', 'AdminKbSLImporterController'),
                $this->module->l('active', 'AdminKbSLImporterController'),
                $this->module->l('image', 'AdminKbSLImporterController')
            );
            foreach ($languages as $lang) {
                $data['name_' . $lang['iso_code']] = $this->l('Sample Store');
                $data_translated[] = $this->module->l('name', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['address1_' . $lang['iso_code']] = $this->l('Test Street');
                $data_translated[] = $this->module->l('address1', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['address2_' . $lang['iso_code']] = '';
                $data_translated[] = $this->module->l('address2', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['note_' . $lang['iso_code']] = '';
                $data_translated[] = $this->module->l('note', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['monday_' . $lang['iso_code']] = '9:00 - 19:00';
                $data_translated[] = $this->module->l('monday', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['tuesday_' . $lang['iso_code']] = '9:00 - 19:00';
                $data_translated[] = $this->module->l('tuesday', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['wednesday_' . $lang['iso_code']] = '9:00 - 19:00';
                $data_translated[] = $this->module->l('wednesday', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['thrusday_' . $lang['iso_code']] = '9:00 - 19:00';
                $data_translated[] = $this->module->l('thursday', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['friday_' . $lang['iso_code']] = '9:00 - 19:00';
                $data_translated[] = $this->module->l('friday', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['saturday_' . $lang['iso_code']] = '10:00 - 17:00';
                $data_translated[] = $this->module->l('saturday', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
                $data['sunday_' . $lang['iso_code']] = '10:00 - 17:00';
                $data_translated[] = $this->module->l('sunday', 'AdminKbSLImporterController').'('. $lang['iso_code'] .')';
            }

            if (!empty($data) && (count($data) > 0)) {
                $data_key = array();
                $data_val = array();
                foreach ($data as $key => $d) {
                    $data_key[] = $key;
                    $data_val[] = $d;
                }
                
                fputcsv($file, $data_key);
                fputcsv($file, $data_translated);
                fputcsv($file, $data_val);
                fclose($file);
            }
        } else {
            $data_translated = array(
                $this->module->l('name', 'AdminKbSLImporterController'),
                $this->module->l('address1', 'AdminKbSLImporterController'),
                $this->module->l('address2', 'AdminKbSLImporterController'),
                $this->module->l('postcode', 'AdminKbSLImporterController'),
                $this->module->l('city', 'AdminKbSLImporterController'),
                $this->module->l('id_country', 'AdminKbSLImporterController'),
                $this->module->l('id_state', 'AdminKbSLImporterController'),
                $this->module->l('latitude', 'AdminKbSLImporterController'),
                $this->module->l('longitude', 'AdminKbSLImporterController'),
                $this->module->l('phone', 'AdminKbSLImporterController'),
                $this->module->l('fax', 'AdminKbSLImporterController'),
                $this->module->l('email', 'AdminKbSLImporterController'),
                $this->module->l('note', 'AdminKbSLImporterController'),
                $this->module->l('active', 'AdminKbSLImporterController'),
                $this->module->l('image', 'AdminKbSLImporterController'),
                $this->module->l('monday', 'AdminKbSLImporterController'),
                $this->module->l('tuesday', 'AdminKbSLImporterController'),
                $this->module->l('wednesday', 'AdminKbSLImporterController'),
                $this->module->l('thrusday', 'AdminKbSLImporterController'),
                $this->module->l('friday', 'AdminKbSLImporterController'),
                $this->module->l('saturday', 'AdminKbSLImporterController'),
                $this->module->l('sunday', 'AdminKbSLImporterController')
            );
                
                $data = array('name','address1','address2','postcode','city','id_country','id_state','latitude','longitude','phone','fax','email','note','active','image','monday','tuesday','wednesday','thrusday','friday','saturday','sunday');
                $sample_data = array('Sample Store','Test Street','','99501','Alaska','21','2','25.765005','-80.243797','','','samplestore@xyz.com','','1','http://yourstore.com/logo.png','9:00 - 19:00','9:00 - 19:00','9:00 - 19:00','9:00 - 19:00','9:00 - 19:00','','');
                fputcsv($file, $data);
                fputcsv($file, $data_translated);
                fputcsv($file, $sample_data);
        }
        fclose($file);
        die();
    }
    
    
    /**
     * Prestashop Default Function in AdminController.
     * Init context and dependencies, handles POST and GET
     */
    public function init()
    {
       
        parent::init();
    }
    
    /**
     * assign default action in toolbar_btn smarty var, if they are not set.
     * uses override to specifically add, modify or remove items
     *
     */
    public function initToolbar()
    {
        parent::initToolbar();
    }
    
    /*
     * Function for returning the absolute path of the module directory
     */
    protected function getKbModuleDir()
    {
        return _PS_MODULE_DIR_.$this->module->name.'/';
    }
    
    /*
     * Default function, used here to include JS/CSS files for the module.
     */
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->context->controller->addCSS($this->getKbModuleDir() . 'views/css/admin/kb_admin.css');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/kbcustomfield_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/velovalidation.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/validation_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/admin.js');
    }
    
    /**
     * Function used display toolbar in page header
     */
    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
    }
    
    /*
     * Function for returning the URL of PrestaShop Root Modules Directory
     */
    protected function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }
    
    /*
     * Function for checking SSL
     */
    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * Function for returning all the languages in the system
     */
    public function getAllLanguages()
    {
        return Language::getLanguages(false);
    }
}
