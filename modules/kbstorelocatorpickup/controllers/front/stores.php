<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 *
 *
 */

include_once(_PS_MODULE_DIR_ . '/kbstorelocatorpickup/kbstorelocatorpickup.php');

class KbStoreLocatorPickupStoresModuleFrontController extends ModuleFrontController
{

//    public $php_self = 'Kbstores';
    public $ssl = true;
    public $display_column_left = false;
    public $display_column_right = false;

    public function __construct()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::__construct();
    }

    /**
     * Initializes front controller: sets smarty variables,
     * class properties, redirects depending on context, etc.
     *
     * @throws PrestaShopException
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Sets default medias for this controller
     */
    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_PS_MODULE_DIR_ . $this->module->name . '/views/css/front/kb_front.css');
        $this->addCSS(_PS_MODULE_DIR_ . $this->module->name . '/views/css/front/jquery.mCustomScrollbar.css');
        $this->addJS(_PS_MODULE_DIR_ . $this->module->name . '/views/js/front/kb_front.js');
        $this->addJS(_PS_MODULE_DIR_ . $this->module->name . '/views/js/front/jquery.mCustomScrollbar.concat.min.js');
    }

    /**
     * Method that is executed after init() and checkAccess().
     * Used to process user input.
     *
     * @see Controller::run()
     */
    public function postProcess()
    {
        if (Tools::getValue('validatePickupDate')) {
            if ($this->validatePickupDate()) {
                echo 1;
            } else {
                echo 0;
            }
            die;
        }
        
        if (Tools::getValue('pickupStoreMapping')) {
            $preferred_date = Tools::getValue('preferred_date');
            $preferred_store = Tools::getValue('preferred_store');
            if (!empty($preferred_date) && !empty($preferred_store)) {
                $id_cart = $this->context->cart->id;
                $id_customer = $this->context->customer->id;
                $id_shop = $this->context->shop->id;
                $id_address = Db::getInstance()->getValue('SELECT id_address FROM ' . _DB_PREFIX_ . 'kb_pickup_store_address_mapping where id_store=' . (int) $preferred_store);
                $exist_data = Db::getInstance()->getValue('SELECT id_pickup FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_time WHERE id_cart=' . (int) $id_cart . ' AND id_customer=' . (int) $id_customer . ' AND id_shop=' . (int) $id_shop);
//                $query = 0;
                if (empty($exist_data)) {
                    $query = Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'kb_pickup_at_store_time set id_shop=' . (int) $id_shop . ',id_cart=' . (int) $id_cart . ',id_customer=' . (int) $id_customer . ',id_store=' . (int) $preferred_store . ',id_address_delivery=' . (int) $id_address . ',preferred_date="' . pSQL($preferred_date) . '",date_add=now(),date_upd=now()');
                } else {
                    $query = Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'kb_pickup_at_store_time  set id_shop=' . (int) $id_shop . ',id_store=' . (int) $preferred_store . ',id_address_delivery=' . (int) $id_address . ',preferred_date="' . pSQL($preferred_date) . '",date_upd=now() WHERE id_cart=' . (int) $id_cart . ' AND id_customer=' . (int) $id_customer . ' AND id_shop=' . (int) $id_shop);
                }
            }
            
            //Start : changes done on 12th august 2019 by vishal :  for save data in DB if data selection is disable
            
            if (empty($preferred_date) && !empty($preferred_store)) {
                $id_cart = $this->context->cart->id;
                $id_customer = $this->context->customer->id;
                $id_shop = $this->context->shop->id;
                $id_address = Db::getInstance()->getValue('SELECT id_address FROM ' . _DB_PREFIX_ . 'kb_pickup_store_address_mapping where id_store=' . (int) $preferred_store);
                $exist_data = Db::getInstance()->getValue('SELECT id_pickup FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_time WHERE id_cart=' . (int) $id_cart . ' AND id_customer=' . (int) $id_customer . ' AND id_shop=' . (int) $id_shop);
//                $query = 0;
                if (empty($exist_data)) {
                    $query = Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'kb_pickup_at_store_time set id_shop=' . (int) $id_shop . ',id_cart=' . (int) $id_cart . ',id_customer=' . (int) $id_customer . ',id_store=' . (int) $preferred_store . ',id_address_delivery=' . (int) $id_address . ',preferred_date="' . pSQL($preferred_date) . '",date_add=now(),date_upd=now()');
                } else {
                    $query = Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'kb_pickup_at_store_time  set id_shop=' . (int) $id_shop . ',id_store=' . (int) $preferred_store . ',id_address_delivery=' . (int) $id_address . ',preferred_date="' . pSQL($preferred_date) . '",date_upd=now() WHERE id_cart=' . (int) $id_cart . ' AND id_customer=' . (int) $id_customer . ' AND id_shop=' . (int) $id_shop);
                }
            }
            
            //End : changes done on 12th august 2019 by vishal :  for save data in DB if data selection is disable
            $data = array();
            if (Db::getInstance()->executeS('SELECT id_store from '. _DB_PREFIX_ .'store') && Db::getInstance()->executeS('SELECT id_store from '. _DB_PREFIX_ .'store_lang')) {
                $store_data = Db::getInstance()->executeS('SELECT CONCAT(sl.name, ", ", sl.address1, ", ", s.city, ", ", s.postcode) AS store_name, kp.preferred_date as preferred_date FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_time kp'
                        . ' JOIN ' . _DB_PREFIX_ . 'store_lang sl ON sl.id_store = kp.id_store' . ' JOIN ' . _DB_PREFIX_ . 'store s ON s.id_store = sl.id_store'
                        . ' WHERE id_cart=' . (int) $id_cart . ' AND id_customer=' . (int) $id_customer . ' AND id_shop=' . (int) $id_shop);
            } else {
                $store_data = Db::getInstance()->executeS('SELECT CONCAT(s.name, ", ", s.address1, ", ", s.city, ", ", s.postcode) AS store_name, kp.preferred_date as preferred_date FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_time kp'
                        . ' JOIN ' . _DB_PREFIX_ . 'store s'
                        . ' USING (id_store) WHERE id_cart=' . (int) $id_cart . ' AND id_customer=' . (int) $id_customer . ' AND id_shop=' . (int) $id_shop);
            }
            $data['store'] = $store_data;
            $data['status'] = true;
            echo json_encode($data);
            die;
        }
        if (Tools::getValue('searchStore')) {
            if (Tools::getIsset('reset') && Tools::getValue('reset') == 1) {
                $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
                $enabled_store = $storeLocatorConfig['avilable_store'];
                $available_stores = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                $default_store = $storeLocatorConfig['default_store'];
                $filter_pickup = Tools::getValue('filter_pickup');
                $updated_available_stores = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                $this->context->smarty->assign(
                    array(
                        'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                        'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                        'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                        'index_page_link' => $this->context->link->getPageLink('index'),
                    )
                );
                // changes by rishabh jain
                $available_stores = array();
                if ((int)$filter_pickup) {
                    $id_address_delivery = (int) $this->context->cart->id_address_delivery;
                    if ($id_address_delivery != 0) {
                        $address_obj = new Address($id_address_delivery);
                        $selected_add_country_id = $address_obj->id_country;
                    } else {
                        $selected_add_country_id = $this->context->country->id;
                    }
                    $is_enabled_country_restriction = 0;
                    if (isset($storeLocatorConfig['enable_country_restriction']) && $storeLocatorConfig['enable_country_restriction'] == 1) {
                        $is_enabled_country_restriction =  1;
                    }
                    // changes over
                    foreach ($updated_available_stores as $key => $store) {
                        if ($is_enabled_country_restriction) {
                            if ($selected_add_country_id == $store['id_country']) {
                            } else {
                                unset($updated_available_stores[$key]);
                            }
                        }
                    }
                    foreach ($updated_available_stores as $key => $store) {
                        $available_stores[] = $store;
                    }
                } else {
                    $available_stores = $updated_available_stores;
                }
                // changes over
                $near_by_store = array();
                $av_store_detail = array();
                $data = array();
                foreach ($available_stores as $key => $store) {
                    $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store);
                    $available_stores[$key]['store_distance'] = '';
                    $available_stores[$key]['hours'] = $workingHours;
                    $near_by_store[] = $available_stores[$key];
                    $this->context->smarty->assign('display_phone', $storeLocatorConfig['display_phone']);
                    $this->context->smarty->assign('filter_pickup', $filter_pickup);
                    $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png');
                    $this->context->smarty->assign('available_store', $available_stores[$key]);
                    $av_store_detail[$store['id_store']] = $workingHours;
                }
                $this->context->smarty->assign(
                    array(
                        'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                        'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                        'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                        'index_page_link' => $this->context->link->getPageLink('index'),
                        'distance_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/distance.png',
                        'phone_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png',
                        'web_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/web.png',
                        'clock_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/clock.png',
                        'store_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/store.png',
                    )
                );
                $this->context->smarty->assign('search_store_result', true);
                $this->context->smarty->assign('available_stores', $near_by_store);
                $this->context->smarty->assign('distance_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/distance.png');
                $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png');
                $this->context->smarty->assign('filter_pickup', $filter_pickup);
                $this->context->smarty->assign('distance_unit', $storeLocatorConfig['distance_unit']);
                $this->context->smarty->assign('display_phone', $storeLocatorConfig['display_phone']);
                $this->context->smarty->assign('controller', Tools::getValue('page_controller'));
                $this->context->smarty->assign(array(
                    'av_store_detail' => json_encode($av_store_detail),
                    'default_store' => $default_store,
                ));
                $data['body'] = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'kbstorelocatorpickup/views/templates/hook/search_store_result.tpl');
                echo json_encode($data);
                die();
            } else {
                if (Tools::getIsset('lat_val') && Tools::getIsset('lng_val')) {
                    $search_latitude = Tools::getValue('lat_val');
                    $search_longitude = Tools::getValue('lng_val');
                    $radius_val = Tools::getValue('radius_val');
                    $result_limit = Tools::getValue('result_limit');
                    $filter_pickup = Tools::getValue('filter_pickup');
                    $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
                    $enabled_store = $storeLocatorConfig['avilable_store'];
                    $updated_available_stores = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                    // changes by rishabh jain
                    $available_stores = array();
                    if ((int)$filter_pickup) {
                        $id_address_delivery = (int) $this->context->cart->id_address_delivery;
                        $address_obj = new Address($id_address_delivery);
                        $selected_add_country_id = $address_obj->id_country;

                        $is_enabled_country_restriction = 0;
                        if (isset($storeLocatorConfig['enable_country_restriction']) && $storeLocatorConfig['enable_country_restriction'] == 1) {
                            $is_enabled_country_restriction =  1;
                        }
                        // changes over
                        foreach ($updated_available_stores as $key => $store) {
                            if ($is_enabled_country_restriction) {
                                if ($selected_add_country_id == $store['id_country']) {
                                } else {
                                    unset($updated_available_stores[$key]);
                                }
                            }
                        }
                        foreach ($updated_available_stores as $key => $store) {
                            $available_stores[] = $store;
                        }
                    } else {
                        $available_stores = $updated_available_stores;
                    }
                    // changes over
                    $distance_unit = "M";
                    if ($storeLocatorConfig['distance_unit'] == 'km') {
                        $distance_unit = "K";
                    }
                    $near_by_store = array();
                    $marker_json_data = array();
                    $av_store_detail = array();
                    $default_store = $storeLocatorConfig['default_store'];
                    $this->context->smarty->assign(
                        array(
                            'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                            'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                            'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                            'index_page_link' => $this->context->link->getPageLink('index'),
                            'distance_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/distance.png',
                            'phone_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png',
                            'web_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/web.png',
                            'clock_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/clock.png',
                            'store_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/store.png',
                        )
                    );
                        $this->context->smarty->assign('distance_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/distance.png');
                        $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png');
                    foreach ($available_stores as $key => $store) {
                            $json_data = array();
                            $near_by_distance = Tools::ps_round($this->calculateDistance($search_latitude, $search_longitude, $store['latitude'], $store['longitude'], $distance_unit), 2);
                        if ($near_by_distance <= $radius_val || $radius_val == 0) {
                                $available_stores[$key]['store_distance'] = $near_by_distance;
                                $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store);
                                $available_stores[$key]['hours'] = $workingHours;
                                $near_by_store[] = $available_stores[$key];
                                $this->context->smarty->assign('display_phone', $storeLocatorConfig['display_phone']);
                                $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png');
                                $this->context->smarty->assign('filter_pickup', $filter_pickup);
                                $this->context->smarty->assign('available_store', $available_stores[$key]);
                        
                                $json_data[0] = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'kbstorelocatorpickup/views/templates/hook/search_marker_result.tpl');
                                $json_data[1] = $store['latitude'];
                                $json_data[2] = $store['longitude'];
                                $marker_json_data[] = $json_data;
                                $av_store_detail[$store['id_store']] = $workingHours;
                        }
                    }
                        $data = array();
//                p($available_stores);
                        $near_by_store = array_slice($near_by_store, 0, $result_limit);
                        $store_near_distance = array_column($near_by_store, 'store_distance');
                        array_multisort($store_near_distance, SORT_ASC, $near_by_store);
                        $this->context->smarty->assign(
                            array(
                            'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                            'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                            'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                            'index_page_link' => $this->context->link->getPageLink('index'),
                            )
                        );
                        $this->context->smarty->assign('search_store_result', true);
                        $this->context->smarty->assign('available_stores', $near_by_store);
                        $this->context->smarty->assign('distance_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/distance.png');
                        $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png');
                        $this->context->smarty->assign('distance_unit', $storeLocatorConfig['distance_unit']);
                        $this->context->smarty->assign('display_phone', $storeLocatorConfig['display_phone']);
                        $this->context->smarty->assign('controller', Tools::getValue('page_controller'));
                        $this->context->smarty->assign(array(
                        'av_store_detail' => json_encode($av_store_detail),
                        'default_store' => $default_store,
                        ));
                        $data['body'] = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'kbstorelocatorpickup/views/templates/hook/search_store_result.tpl');
                        $this->context->smarty->assign('search_marker_result', true);

                        $data['location_arr_json'] = json_encode($marker_json_data);
                        echo json_encode($data);
                        die();
                }
            }
        }
        parent::postProcess();
    }

    /*
     * This routine calculates the distance between two points (given the 
     * latitude/longitude of those points). It is being used to calculate
     * the distance between two locations using GeoDataSource(TM) Products
     */

    protected function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = Tools::strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /*
     * function to validate the pickup date
     */

    public function validatePickupDate()
    {
        if (!empty(Tools::getValue('date')) && !empty(Tools::getValue('id_store'))) {
            $id_store = Tools::getValue('id_store');
            $store = new Store($id_store);
//            if (isset($store->hours[$this->context->language->id])) {
//                $hours = Tools::jsonDecode($store->hours, true);
//            } else {
//                $hours = Tools::jsonDecode($store->hours, true);
//            }
            
            /* Start changes done by Vishal on 17th August 2019 : for PS version compatibilty   */
            
            if (version_compare(_PS_VERSION_, '1.7.4.0', '>=')) {
                if (isset($store->hours[$this->context->language->id])) {
                    $hours = Tools::jsonDecode($store->hours[$this->context->language->id], true);
                } else {
                    $hours = Tools::jsonDecode($store->hours[$this->context->language->id], true);
                }
            } else {
                if (isset($store->hours[$this->context->language->id])) {
                    $hours = Tools::jsonDecode($store->hours, true);
                } else {
                    $hours = Tools::jsonDecode($store->hours, true);
                }
            }
            
            /* End changes done by Vishal on 17th August 2019 : for PS version compatibilty   */
            
            $day = Tools::getValue('selectedDay');
            $selectedHours = Tools::getValue('selectedHour');
            $pickup_setting = Tools::jsonDecode(Configuration::get('KB_PICKUP_TIME_SETTINGS'), true);
            $time_slot = 0;
            if (!empty($pickup_setting)) {
                if ($pickup_setting['time_slot']) {
                    $time_slot = 1;
                }
            }
            $time_period = '';
            if ($day != '') {
                if ($day == 0) {
                    if (isset($hours[6][0])) {
                        $time_period = $hours[6][0];
                    }
                } elseif ($day == 1) {
                    $time_period = $hours[0][0];
                } else {
                    $time_period = $hours[$day - 1][0];
                }
            }
            if (!empty($time_period)) {
                if (!$time_slot) {
                    return true;
                }
                $time_period = explode('-', $time_period);
                $startHrs = (isset($time_period[0])) ? trim($time_period[0]) : '';
                $endHrs = (isset($time_period[1])) ? trim($time_period[1]) : '';
                if (empty($startHrs) && empty($endHrs)) {
                    return false;
                } elseif (empty($startHrs) || empty($endHrs)) {
                    if (!empty($startHrs)) {
                        $startHrs = date("H", strtotime($startHrs));
                        if ($selectedHours >= $startHrs) {
                            return true;
                        }
                    } elseif (!empty($endHrs)) {
                        $endHrs = date("H", strtotime($endHrs));
                        if ($startHrs <= $endHrs) {
                            return true;
                        }
                    }
                } else {
                    $startHrs = date("H", strtotime($startHrs));
                    $endHrs = date("H", strtotime($endHrs));
                    if ($selectedHours >= $startHrs && $startHrs <= $endHrs) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function initContent()
    {
        parent::initContent();
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_store_locator']) {
                $av_store_detail = array();
                $default_store = $storeLocatorConfig['default_store'];
                 $store = new Store($default_store);
                 //changes by tarun
                $selected_store = new Store($default_store);
                $selected_store = (array) $selected_store;
                    $selected_store['country'] = Country::getNameById($this->context->language->id, $selected_store['id_country']);
                    //changes over
                 $default_latitude = $store->latitude;
                    $default_longitude = $store->longitude;
                $enabled_store = $storeLocatorConfig['avilable_store'];
                $available_stores = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                foreach ($available_stores as $key => $store) {
                    $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store);
                    $available_stores[$key]['hours'] = $workingHours;
                    $av_store_detail[$store['id_store']] = $workingHours;
                }
                $time = time();
                $marker = $this->getModuleDirUrl() . $this->module->name . '/views/img/marker.png?time='.$time;
                $exist_file = _PS_MODULE_DIR_ . $this->module->name . '/views/img/user_marker.*';
                $match1 = glob($exist_file);
                if (count($match1) > 0) {
                    $ban = explode('/', $match1[0]);
                    $ban = end($ban);
                    $ban = trim($ban);
                    if (file_exists($match1[0])) {
                        $marker = $this->getModuleDirUrl() . $this->module->name . '/views/img/' . $ban.'?time='.$time;
                    }
                }
                // changes by rishabh jain
                $logo_image = $this->getModuleDirUrl() . $this->module->name . '/views/img/store_logo.jpg?time='.$time;
                $exist_file = _PS_MODULE_DIR_.$this->module->name. '/views/img/user_logo.*';

                $match1 = glob($exist_file);
                if (count($match1) > 0) {
                    $ban = explode('/', $match1[0]);
                    $ban = end($ban);
                    $ban = trim($ban);
                    $img_url = $this->getModuleDirUrl() . $this->module->name . '/views/img/' . $ban;
                    if (file_exists($match1[0])) {
                        $logo_image = $img_url.'?time='.$time;
                    }
                }
                $is_enabled_store_logo = 0;
                if (isset($storeLocatorConfig['display_store_banner']) && $storeLocatorConfig['display_store_banner'] == 1) {
                    $is_enabled_store_logo = 1;
                }
                // changes over
                // changes by rishabh jain
                $this->context->smarty->assign('is_enabled_website_link', $storeLocatorConfig['website_link']);
                $this->context->smarty->assign('is_enabled_direction_link', $storeLocatorConfig['get_direction_link']);
                $this->context->smarty->assign('index_page_link', $this->context->link->getPageLink('index'));
                // change over
                //changes by tarun to show all stores in the map
                if ($storeLocatorConfig['show_stores'] == 0) {
                    $this->context->smarty->assign('kb_all_stores', '');
                } else {
                    $radius_val = 0;
                    $filter_pickup = 1;
                    $available_stores1 = array();
                    $enabled_store = $storeLocatorConfig['avilable_store'];
                    $available_stores1 = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                    $distance_unit = "M";
                    if ($storeLocatorConfig['distance_unit'] == 'km') {
                        $distance_unit = "K";
                    }
                    $near_by_store = array();
                    $marker_json_data = array();
                    $av_store_detail = array();
                    $default_store = $storeLocatorConfig['default_store'];
                    $this->context->smarty->assign(
                        array(
                            'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                            'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                            'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                            'index_page_link' => $this->context->link->getPageLink('index'),
                            'distance_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/distance.png',
                            'phone_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png',
                            'web_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/web.png',
                            'clock_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/clock.png',
                            'store_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/store.png',
                        )
                    );
                    $this->context->smarty->assign('distance_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/distance.png');
                    $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png');
                    foreach ($available_stores1 as $key => $store1) {
                        $near_by_distance = Tools::ps_round($this->calculateDistance($default_latitude, $default_longitude, $store1['latitude'], $store1['longitude'], $distance_unit), 2);
                        if ($near_by_distance <= $radius_val || $radius_val == 0) {
                            $json_data = array();
                            $available_stores1[$key]['store_distance'] = $near_by_distance;
                            $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store1);
                            $available_stores1[$key]['hours'] = $workingHours;
                            $near_by_store[] = $available_stores1[$key];
                            $this->context->smarty->assign('display_phone', $storeLocatorConfig['display_phone']);
                            $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->module->name . '/views/img/front/call.png');
                            $this->context->smarty->assign('filter_pickup', $filter_pickup);
                            $this->context->smarty->assign('available_store', $available_stores1[$key]);

                            $json_data[] = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'kbstorelocatorpickup/views/templates/hook/search_marker_result.tpl');
                            $json_data[] = $store1['latitude'];
                            $json_data[] = $store1['longitude'];
                            $marker_json_data[] = $json_data;
                            $av_store_detail[$store1['id_store']] = $workingHours;
                        }
                    }
                    $this->context->smarty->assign('kb_all_stores', json_encode($marker_json_data));
                }
                //changes over
                $this->context->smarty->assign(
                    array(
                        'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                        'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                        //changes by tarun
                        'is_enabled_show_stores' => $storeLocatorConfig['show_stores'],
                        //changes over
                        'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                        'index_page_link' => $this->context->link->getPageLink('index'),
                        'web_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/web.png',
                        'clock_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/clock.png',
                        'store_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/store.png',
                        'logo_image' => $logo_image,
                        'is_enabled_store_logo' => $is_enabled_store_logo,
                        'kb_store_url' => $this->context->link->getModuleLink($this->module->name, 'stores'),
                        'map_api' => $storeLocatorConfig['api_key'],
                        'lang_iso' => $this->context->language->iso_code,
                        'zoom_level' => $storeLocatorConfig['zoom_level'],
                        'default_latitude' => $default_latitude,
                        'default_longitude' => $default_longitude,
                        'display_phone' => $storeLocatorConfig['display_phone'],
                        'default_store' => $default_store,
                        'distance_unit' => $storeLocatorConfig['distance_unit'],   //to be implemented
                        'distance_icon' => $this->getModuleDirUrl().$this->module->name.'/views/img/front/distance.png',
                        'phone_icon' => $this->getModuleDirUrl().$this->module->name.'/views/img/front/call.png',
                        'locator_icon' => $marker,
                        'current_location_icon' => $this->getModuleDirUrl() . $this->module->name . '/views/img/front/current_location.png',
                        'available_stores' => $available_stores,
                        'av_store_detail' => json_encode($av_store_detail),
                        'default_store_detail' => json_encode($av_store_detail[$default_store]),
                        'id_lang' => $this->context->language->id,
                        //changes by tarun
                        'selected_store' => $selected_store,
                        //changes over
                    )
                );
                
                $this->setTemplate('module:kbstorelocatorpickup/views/templates/front/stores.tpl');
            }
        }
    }

    /*
     * Function for getting the URL to PrestaShop Root Module Directory
     */

    private function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }

    /*
     * Function for checking SSL 
     */

    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }
}
