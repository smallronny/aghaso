<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 *
 *
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Kbstorelocatorpickup extends Module
{

    const MODEL_FILE = 'model.sql';

    public function __construct()
    {
        $this->name = 'kbstorelocatorpickup';
        $this->tab = 'front_office_features';
        $this->version = '1.0.9';
        $this->author = 'knowband';
        $this->need_instance = 1;
        $this->module_key = 'c84e6195faeab56015a91905c9191146';
        $this->author_address = '0x2C366b113bd378672D4Ee91B75dC727E857A54A6';
        $this->bootstrap = true;

        parent::__construct();
        $this->displayName = $this->l('Knowband Store Locator And Pickup');
        $this->description = $this->l('This module allows you to display your physical store locations using Google maps and gives you the ability to offer an Personal Pickup at Store delivery option.');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => '1.7.999.999');
    }

    /*
     * To install Database Table during install of the module
     */
    protected function installModel()
    {
        $installation_error = false;
        if (!file_exists(_PS_MODULE_DIR_ . $this->name . '/' . self::MODEL_FILE)) {
            $this->custom_errors[] = $this->l('Model installation file not found.');
            $installation_error = true;
        } elseif (!is_readable(_PS_MODULE_DIR_ . $this->name . '/' . self::MODEL_FILE)) {
            $this->custom_errors[] = $this->l('Model installation file is not readable.');
            $installation_error = true;
        } elseif (!$sql = Tools::file_get_contents(_PS_MODULE_DIR_ . $this->name . '/' . self::MODEL_FILE)) {
            $this->custom_errors[] = $this->l('Model installation file is empty.');
            $installation_error = true;
        }

        if (!$installation_error) {
            /*
             * Replace _PREFIX_ and ENGINE_TYPE with default Prestashop values
             */
            $sql = str_replace(
                array('_PREFIX_', 'ENGINE_TYPE'),
                array(_DB_PREFIX_, _MYSQL_ENGINE_),
                $sql
            );
            $sql = preg_split("/;\s*[\r\n]+/", trim($sql));
            foreach ($sql as $query) {
                if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(trim($query))) {
                    $installation_error = true;
                }
            }
        }
        
        $languages = Language::getLanguages(false);

        if (!$installation_error) {
            if (!$installation_error && !Configuration::get('KB_PICKUP_AT_STORE_MAIL_CHECK')) {
                //Insert Email Data
                if (Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(trim($sql[0]))) {
                    $content = $this->getEmailTemplateData();
                    foreach ($languages as $lng) {
                        Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                            'INSERT INTO '._DB_PREFIX_.'kb_pickup_at_store_email_template '
                            . 'set id_lang='.(int)$lng['id_lang']
                            .',iso_code="'.pSQL($lng['iso_code'])
                            .'",subject="'.pSQL($content['subject'])
                            .'",body="'.pSQL(Tools::htmlentitiesUTF8($content['body']))
                            .'",date_add=now(),date_upd=now()'
                        );
                    }
                    
                    $mail_dir = dirname(__FILE__) . '/mails/en';
                    $languages = Language::getLanguages(false);
                    $language_count = count($languages);
                    for ($i = 0; $i < $language_count; $i++) {
                        if ($languages[$i]['iso_code'] != 'en') {
                            $new_dir = dirname(__FILE__) . '/mails/' . $languages[$i]['iso_code'];
                            $this->copyfolder($mail_dir, $new_dir);
                        }
                    }

                    Configuration::updateValue('KB_PICKUP_AT_STORE_MAIL_CHECK', 1);
                } else {
                    $installation_error = true;
                    $this->custom_errors[] = $this->l('Email data is not installed.');
                }
            }
        }
        
        
        if ($installation_error) {
            return false;
        } else {
            return true;
        }
    }
    
    private function getEmailTemplateData()
    {
        $mail_content = $this->kbMailContent();
        
        $data = array(
            'subject' => $mail_content['subject'],
            'body' => $mail_content['body'],
        );
        
        return $data;
    }
    
    protected function kbMailContent()
    {
        $mail_content = array();
        $this->context->smarty->assign(
            array(
                'shop_logo'=> $this->context->link->getMediaLink(__PS_BASE_URI__ . 'img/logo.jpg'),
                'delivery_icon' => $this->context->link->getMediaLink(__PS_BASE_URI__ . 'modules/'.$this->name.'/views/img/preferred_icon.png'),
            )
        );
        $body =  $this->display(__FILE__, 'views/templates/admin/pickup_mail_template.tpl');
        $body = str_replace('[', '{', $body);
        $body = str_replace(']', '}', $body);
        $mail_content['subject'] = $this->l('You have received a new Order');
        $mail_content['body'] = $body;
        
        return $mail_content;
    }

    /*
     * Install function to install module
     */
    public function install()
    {
        /*
         * Create Database table and if there is some problem then display error message
         */
        if (!$this->installModel()) {
            $this->custom_errors[] = $this->l('Error occurred while installing/upgrading modal.');
            return false;
        }

        /*
         * Register various hook functions
         */
        if (!parent::install() ||
            !$this->registerHook('displayHeader') ||
            !$this->registerHook('displayBackOfficeHeader') ||
            !$this->registerHook('displayHome') ||
            !$this->registerHook('displayBeforeCarrier') ||
            !$this->registerHook('displayShoppingCartFooter') ||
            !$this->registerHook('displayOrderConfirmation') ||
            !$this->registerHook('displayOrderDetail') ||
            !$this->registerHook('displayPDFInvoice') ||
            !$this->registerHook('displayNav') ||
            !$this->registerHook('displayNav2') ||
            !$this->registerHook('displayAdminOrderContentShip') ||
            !$this->registerHook('displayAdminOrderTabShip') ||
            !$this->registerHook('actionValidateOrder') ||
            !$this->registerHook('actionOrderStatusPostUpdate') ||
            !$this->registerHook('actionObjectStoreDeleteAfter') ||
            !$this->registerHook('actionObjectStoreUpdateAfter') ||
            !$this->registerHook('actionObjectStoreAddAfter') ||
            !$this->registerHook('actionCarrierUpdate') ||
            !$this->registerHook('displayPaymentByBinaries') ||
            !$this->registerHook('actionDispatcher')) {
            return false;
        }

        $this->installKbModuleTabs();
        
        /*
         * create Pickup at store shipping on installation
         */
        if (!Configuration::get('KB_PICKUP_AT_STORE_SHIPPING')) {
            $pickup_id  = $this->kbCreatePickupShipping();
            Configuration::updateValue('KB_PICKUP_AT_STORE_SHIPPING', $pickup_id);
        }
        
        if (!Configuration::get('KB_PICKUP_TIME_SETTINGS')) {
            $setting = array();
            $setting['days_gap'] = 3;
            $setting['maximum_days'] = 30;
            $setting['format'] = "%y-%m-%d %t24";
//            $setting['cutoff_time'] = 0;
            $setting['time_slot'] = 0;
            $setting['hours_gap'] = 0;
            Configuration::updateValue('KB_PICKUP_TIME_SETTINGS', Tools::jsonEncode($setting));
            $general_setting = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
            if (!empty($general_setting)) {
                if ($general_setting['enable_pickup']) {
                    $kb_pickup = Configuration::get('KB_PICKUP_AT_STORE_SHIPPING');
                    $carrier = new Carrier($kb_pickup);
                    if (!empty($kb_pickup)) {
                        $carrier->active = 1;
                    } else {
                        $carrier->active = 0;
                    }
                    $carrier->update();
                }
            }
        }
        
        /* Start changes done by Vishal on 13th August 2019 : Updated the query ,add ss.address 1 and ss.address 2   */
        
        if (version_compare(_PS_VERSION_, '1.7.4.0', '>=')) {
            $ps_store = Db::getInstance()->executeS(
                'SELECT s.*,ss.name,ss.address1,ss.address2 FROM `' . _DB_PREFIX_ . 'store` s '
                . 'INNER JOIN ' . _DB_PREFIX_ . 'store_lang ss '
                . 'on (s.id_store=ss.id_store AND ss.id_lang='
                . (int) Context::getContext()->language->id . ') '
                . 'WHERE s.active=1 ORDER BY s.`id_store`'
            );
        } else {
            $ps_store = Store::getStores(Context::getContext()->language->id);
        }
        
        /* End changes done by Vishal on 13th August 2019 : Updated the query ,add ss.address 1 and ss.address 2   */
        
        foreach ($ps_store as $psStore) {
            self::kbStoreAddressMapping($psStore);
        }
        

        return true;
    }
    
    /*
     * function to create pickup at store carrier for the plugin
     */
    protected function kbCreatePickupShipping($active = 0)
    {
        $carrier = new Carrier();
        $carrier->name = $this->l('Pickup at store');
        $languages     = Language::getLanguages();
        foreach ($languages as $lang) {
            $carrier->delay[$lang['id_lang']] = $this->l('Pickup your package at selected store');
        }
        $carrier->active = $active;
        $carrier->deleted           = 0;
        $carrier->shipping_handling = 0;
        $carrier->shipping_external = 0;
        $carrier->range_behavior    = 0;
        $carrier->is_module         = 0;
        $carrier->is_free           = 1;
        $carrier->shipping_method   = Carrier::SHIPPING_METHOD_PRICE;
        if ($carrier->add()) {
            $id_carrier = $carrier->id;
            
            $carrier->setGroups($this->getCustomerGroups());
            $zones = Zone::getZones(false);
            foreach ($zones as $zone) {
                $carrier->addZone((int) $zone['id_zone']);
            }
            $definition    = ObjectModel::getDefinition($carrier);
            $shop_mapping = array(
                array(
                    $definition['primary'] => (int) $carrier->id,
                    'id_shop' => (int) $this->context->shop->id
                )
            );

            Db::getInstance()->insert(pSQL($definition['table']).'_shop', $shop_mapping, false, true, (int) Db::INSERT_IGNORE);
            $carrier->setTaxRulesGroup(0);
            
            $carrier_img = _PS_MODULE_DIR_.$this->name.'/views/img/front/carrier.png';
            $img_path = _PS_SHIP_IMG_DIR_.$id_carrier.'.jpg';
            if (file_exists($carrier_img)) {
                copy($carrier_img, $img_path);
                ImageManager::resize(
                    $carrier_img,
                    $img_path
                );
            }
            return $carrier->id;
        } else {
            return;
        }
    }
    
    /*
     * function to get customer groups
     * return array
     */
    protected function getCustomerGroups()
    {
        $groups = Group::getGroups(Context::getContext()->language->id);
        $arr    = array();
        foreach ($groups as $g) {
            $arr[] = $g['id_group'];
        }
        return $arr;
    }

    /*
     * Function to uninstall the module with 
     * unregister various hook and 
     * also delete the configuration setting     */

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->unregisterHook('displayOrderConfirmation') ||
            !$this->unregisterHook('displayOrderDetail') ||
            !$this->unregisterHook('displayPDFInvoice') ||
            !$this->unregisterHook('actionCarrierUpdate') ||
            !$this->unregisterHook('displayAdminOrderTabShip') ||
            !$this->unregisterHook('displayAdminOrderContentShip') ||
            !$this->unregisterHook('displayNav2') ||
            !$this->unregisterHook('actionValidateOrder') ||
            !$this->unregisterHook('actionOrderStatusPostUpdate') ||
            !$this->unregisterHook('displayShoppingCartFooter') ||
            !$this->unregisterHook('actionObjectStoreAddAfter') ||
            !$this->unregisterHook('actionObjectStoreUpdateAfter') ||
            !$this->unregisterHook('actionObjectStoreDeleteAfter') ||
            !$this->unregisterHook('displayHeader')) {
            return false;
        }

        $this->uninstallKbModuleTabs();
        
        if (Configuration::get('KB_PICKUP_AT_STORE_SHIPPING')) {
            $id = Configuration::get('KB_PICKUP_AT_STORE_SHIPPING');
            $carrier = new Carrier($id);
            $carrier->delete();
            Configuration::deleteByName('KB_PICKUP_AT_STORE_SHIPPING');
        }
        
        return true;
    }
    
    

    /*
     * Function to set the default configuration setting
     */

    private function getDefaultSettings()
    {
        $settings = array(
            'enable' => 0,
            'display_custom_message' => 1,
            'include_headerfooter' => 1,
            'email_notification' => 1,
            'website_link' => 1,
            'store_image' => 1,
            //changes by tarun
            'show_stores' => 0,
            //changes over
            'get_direction_link' => 1,
            'enable_country_restriction' => 1,
            'display_store_banner' => 1,
            'subject' => $this->l('Customer has contact you regarding unblocking the site'),
        );
        return $settings;
    }
    
    /*
     * Function to create admin tabs
     */
    protected function installKbModuleTabs()
    {
        $lang = Language::getLanguages();
        $tab = new Tab();
        $tab->class_name = 'AdminKbSLDeliverySlip';
        $tab->module = $this->name;
        $tab->active = 0;
        $tab->id_parent = 0;
        foreach ($lang as $l) {
            $tab->name[$l['id_lang']] = $this->l('Download Delivery Slip');
        }
        $tab->save();
        
        $tab1 = new Tab();
        $tab1->class_name = 'AdminKbSLTimeSetting';
        $tab1->module = $this->name;
        $tab1->active = 0;
        $tab1->id_parent = 0;
        foreach ($lang as $l) {
            $tab1->name[$l['id_lang']] = $this->l('Pickup Time Settings');
        }
        $tab1->save();
        
        $tab2 = new Tab();
        $tab2->class_name = 'AdminKbSLEmailNotification';
        $tab2->module = $this->name;
        $tab2->active = 0;
        $tab2->id_parent = 0;
        foreach ($lang as $l) {
            $tab2->name[$l['id_lang']] = $this->l('Email Notification');
        }
        $tab2->save();
        
        $tab3 = new Tab();
        $tab3->class_name = 'AdminKbSLImporter';
        $tab3->module = $this->name;
        $tab3->active = 0;
        $tab3->id_parent = 0;
        foreach ($lang as $l) {
            $tab3->name[$l['id_lang']] = $this->l('Import Store Contacts');
        }
        $tab3->save();

        return true;
    }
    
    /*
    * Function to remove admin tabs
    */
    protected function uninstallKbModuleTabs()
    {
        $parentTab12 = new Tab(Tab::getIdFromClassName('AdminKbSLGeneralSettings'));
        $parentTab12->delete();
        $parentTab = new Tab(Tab::getIdFromClassName('AdminKbSLDeliverySlip'));
        $parentTab->delete();
        
        $parentTab1 = new Tab(Tab::getIdFromClassName('AdminKbSLTimeSetting'));
        $parentTab1->delete();
        
        $parentTab2 = new Tab(Tab::getIdFromClassName('AdminKbSLEmailNotification'));
        $parentTab2->delete();
        
        $parentTab3 = new Tab(Tab::getIdFromClassName('AdminKbSLImporter'));
        $parentTab3->delete();

        return true;
    }
    
    /*
     * function to map address of Store with Adress table
     */
    public static function kbStoreAddressMapping($psStore)
    {
//        print_r($psStore);
//        die;
        $module = Module::getInstanceByName('kbstorelocatorpickup');
        $existing_record = Db::getInstance()->getRow('SELECT count(*) as count,id_address FROM '._DB_PREFIX_.'kb_pickup_store_address_mapping Where id_store='.(int)$psStore['id_store']);
        if ($existing_record['count'] == 0) {
            $address = new Address();
        } else {
            $address = new Address($existing_record['id_address']);
        }
        $address->id_country = $psStore['id_country'];
        $address->id_state =(!empty($psStore['id_state'])) ? $psStore['id_state']: 0;
        $address->country = Country::getNameById(Configuration::get('PS_LANG_DEFAULT'), $address->id_country);
        $address->alias = $module->l('Pickup at Store', 'kbstorelocatorpickup');
        $address->company = '';
        
//        $id_lang = Context::getContext()->language->id;
        
        /* Start changes done by Vishal on 13th August 2019 : Remove $id_lang index   */
        
//        if (version_compare(_PS_VERSION_, '1.7.3.0', '>=')) {
//            if (isset($psStore['address1'])) {
//                $psStore['address1'] = $psStore['address1'];
//            }
//            if (isset($psStore['address2'])) {
//                $psStore['address2'] = $psStore['address2'];
//            }
//            if (isset($psStore['name'])) {
//                $psStore['name'] = $psStore['name'];
//            }
//        }
        
        /* Start changes done by Vishal on 13th August 2019 : Remove $id_lang index   */
        
        /* Start changes done by Vishal on 31st August 2019 : For resolving issue : display no delivery address for pickup store   */
        
        $id_lang = Context::getContext()->language->id;
        if (version_compare(_PS_VERSION_, '1.7.3.0', '>=')) {
            if (isset($psStore['address1'][$id_lang]) && is_array($psStore['address1'])) {
                $psStore['address1'] = $psStore['address1'][$id_lang];
            }
            if (isset($psStore['address2'][$id_lang]) && is_array($psStore['address2'])) {
                $psStore['address2'] = $psStore['address2'][$id_lang];
            }
            if (isset($psStore['name'][$id_lang]) && is_array($psStore['name'])) {
                $psStore['name'] = $psStore['name'][$id_lang];
            }
        }
        
        /* End changes done by Vishal on 31st August 2019 : For resolving issue : display no delivery address for pickup store   */
        
        $name = explode(' ', $psStore['name']);
        $last = array_pop($name);
        $parts = array(implode(' ', $name), $last);
        if (empty($parts[0])) {
            $parts[0] = $parts[1];
        }
        if (!Validate::isName($parts[0])) {
            $parts[0] = 'xx';
        }
        if (!Validate::isName($parts[1])) {
            $parts[1] = 'xx';
        }
        $address->lastname = (!empty($parts[1])) ? $parts[1] : 'xx';
        $address->firstname =(!empty($parts[0])) ? $parts[0] : 'xx';
        $address->address1 = $psStore['address1'];
        $address->address2 = $psStore['address2'];
        $address->postcode = (!empty($psStore['postcode'])) ? $psStore['postcode'] : 0;
        $address->city = $psStore['city'];
        $address->phone = (!empty($psStore['phone'])) ? $psStore['phone'] : null;
        $address->deleted = 0;
//        Tools::dieObject($address, true);
        if (empty($address->address1)) {
             $address->address1 = 'xx';
        }
        if ($address->save()) {
            $id_address = $address->id;
            if ($existing_record['count'] == 0) {
                Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'kb_pickup_store_address_mapping set id_store=' . (int) $psStore['id_store'] . ',id_address=' . (int) $id_address);
            }
        }
    }

    /*
     * Function to create module configuration
     */
    public function getContent()
    {
//        $errors = array();
        /*
         * Function to submit the configuration setting values,
         * first by validating the form data and then save into the DB
         */
        if (Tools::isSubmit('generalsettingsubmit' . $this->name)) {
            $formvalues = Tools::getValue('kbstorelocator');
            $formvalues['api_key'] = trim($formvalues['api_key']);
            if (!empty($_FILES)) {
                if ($_FILES['map_marker']['error'] == 0 && $_FILES['map_marker']['name'] != '' && $_FILES['map_marker']['size'] > 0) {
                    $file_extension = pathinfo($_FILES['map_marker']['name'], PATHINFO_EXTENSION);
                    $path = _PS_MODULE_DIR_ . $this->name . '/views/img/user_marker.' . $file_extension;
                    $exist_image = glob(_PS_MODULE_DIR_ . $this->name . '/views/img/user_marker.*');
                    if (file_exists($exist_image[0])) {
                        unlink($exist_image[0]);
                    }
                    move_uploaded_file(
                        $_FILES['map_marker']['tmp_name'],
                        $path
                    );
                    chmod(_PS_MODULE_DIR_ . $this->name . '/views/img/user_marker.' . $file_extension, 0777);
                }
                // changes by rishabh jain
                if ($_FILES['banner_image']['error'] == 0 && $_FILES['banner_image']['name'] != '' && $_FILES['banner_image']['size'] > 0) {
                    $file_extension = pathinfo($_FILES['banner_image']['name'], PATHINFO_EXTENSION);
                    $path = _PS_MODULE_DIR_ . $this->name . '/views/img/user_logo.' . $file_extension;
                    $exist_image = glob(_PS_MODULE_DIR_ . $this->name . '/views/img/user_logo.*');
                    if (file_exists($exist_image[0])) {
                        unlink($exist_image[0]);
                    }
                    
                    move_uploaded_file(
                        $_FILES['banner_image']['tmp_name'],
                        $path
                    );
                    chmod(_PS_MODULE_DIR_ . $this->name . '/views/img/user_logo.' . $file_extension, 0777);
                }
                // changes over
            }
            
            $pickup_carrier = Configuration::get('KB_PICKUP_AT_STORE_SHIPPING');
            $carrier = new Carrier($pickup_carrier);
            if (empty($carrier->id)) {
                $pickup_id = $this->kbCreatePickupShipping(1);
                Configuration::updateValue('KB_PICKUP_AT_STORE_SHIPPING', $pickup_id);
            } else {
//                changes by tarun
                if ($formvalues['enable']) {
                    if (!$formvalues['enable_pickup']) {
                        $carrier->active = 0;
                    } else {
                        $carrier->active = 1;
                    }
                } else {
                    $carrier->active = 0;
                }
                $carrier->update();
            }
            $languages = Language::getLanguages(false);
            $header_menu = array();
            foreach ($languages as $lang) {
                $header_menu[$lang['id_lang']] = trim(Tools::getValue('header_menu_text_'.$lang['id_lang']));
            }
            $formvalues['header_menu_text'] = $header_menu;
            Configuration::updateValue('KB_STORE_LOCATOR_GENERAL_SETTING', Tools::jsonEncode($formvalues));
            $this->context->cookie->__set('kb_redirect_success', $this->l('General Setting successfully updated.'));
            Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
        }

        $output = '';
        if (isset($this->context->cookie->kb_redirect_success)) {
            $output .= $this->displayConfirmation($this->context->cookie->kb_redirect_success);
            unset($this->context->cookie->kb_redirect_success);
        }
        /*
         * Fetch configuration settings from the Database and convert them into array
         */
        $this->general_settings_values = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
//        $this->kb_block_custom_message = Tools::htmlentitiesDecodeUTF8(Tools::jsonDecode(Configuration::get('KB_BLOCK_USER_CUSTOM_MESSAGE'), true));
        /*
         * Persistence the configuration setting form data
         */
        $config_form_data = Tools::getValue('kbstorelocator');
//        d($config_form_data);
        $config_field_value = array(
            'kbstorelocator[enable]' =>  isset($config_form_data['enable']) ? $config_form_data['enable'] : $this->general_settings_values['enable'],
            'kbstorelocator[enable_store_locator]' =>  isset($config_form_data['enable_store_locator']) ? $config_form_data['enable_store_locator'] : $this->general_settings_values['enable_store_locator'],
            'kbstorelocator[enable_pickup]' =>  isset($config_form_data['enable_pickup']) ? $config_form_data['enable_pickup'] : $this->general_settings_values['enable_pickup'],
            'kbstorelocator[api_key]' =>  isset($config_form_data['api_key']) ? $config_form_data['api_key'] : $this->general_settings_values['api_key'],
            'kbstorelocator[zoom_level]' =>  isset($config_form_data['zoom_level']) ? $config_form_data['zoom_level'] : $this->general_settings_values['zoom_level'],
            'kbstorelocator[distance_unit]' =>  isset($config_form_data['distance_unit']) ? $config_form_data['distance_unit'] : $this->general_settings_values['distance_unit'],
            // changes by rishabh jain
            'kbstorelocator[website_link]' =>  isset($config_form_data['website_link']) ? $config_form_data['website_link'] : $this->general_settings_values['website_link'],
            'kbstorelocator[store_image]' =>  isset($config_form_data['store_image']) ? $config_form_data['store_image'] : $this->general_settings_values['store_image'],
            //changes by tarun
            'kbstorelocator[show_stores]' =>  isset($config_form_data['show_stores']) ? $config_form_data['show_stores'] : $this->general_settings_values['show_stores'],
            //changes over
            'kbstorelocator[get_direction_link]' =>  isset($config_form_data['get_direction_link']) ? $config_form_data['get_direction_link'] : $this->general_settings_values['get_direction_link'],
            'kbstorelocator[enable_country_restriction]' =>  isset($config_form_data['enable_country_restriction']) ? $config_form_data['enable_country_restriction'] : $this->general_settings_values['enable_country_restriction'],
            'kbstorelocator[display_store_banner]' =>  isset($config_form_data['display_store_banner']) ? $config_form_data['display_store_banner'] : $this->general_settings_values['display_store_banner'],
            // changes over
            'kbstorelocator[avilable_store][]' =>  isset($config_form_data['avilable_store']) ? $config_form_data['avilable_store'] : $this->general_settings_values['avilable_store'],
            'kbstorelocator[default_store]' =>  isset($config_form_data['default_store']) ? $config_form_data['default_store'] : $this->general_settings_values['default_store'],
            'kbstorelocator[homepage]' =>  isset($config_form_data['homepage']) ? $config_form_data['homepage'] : $this->general_settings_values['homepage'],
            'kbstorelocator[display_phone]' =>  isset($config_form_data['display_phone']) ? $config_form_data['display_phone'] : $this->general_settings_values['display_phone'],
            'kbstorelocator[header_menu]' =>  isset($config_form_data['header_menu']) ? $config_form_data['header_menu'] : $this->general_settings_values['header_menu'],
        );

        /*
         * loop to fetch all language with default language in an array
         */
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = ((int) ($language['id_lang'] == $this->context->language->id));
            $config_field_value['header_menu_text'][$language['id_lang']] = (!empty($config_form_data) && isset($config_form_data['header_menu_text'])) ? $config_form_data['header_menu_text'][$language['id_lang']] : $this->general_settings_values['header_menu_text'][$language['id_lang']];
        }
        /*
         * Create configuration setting form
         */
        $this->fields_form = $this->getConfigurationForm();
        /*
         * Create helper form for configuration setting form
         */
        $form = $this->getform(
            $this->fields_form,
            $languages,
            $this->l('Configuration'),
            false,
            $config_field_value,
            'general',
            'generalsetting'
        );
        $this->context->smarty->assign('form', $form);
        //changes by tarun
        $helper = new HelperView();
        $helper->module = $this;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->current = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->override_folder='helpers/';
        $helper->base_folder='view/';
        $helper->base_tpl = 'view_custom.tpl';
        
        if (Tools::getShopProtocol() == 'https://') {
            $mod_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $mod_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        $this->context->smarty->assign('mod_dir', $mod_dir);
        $this->context->smarty->assign('gr_protocol_name', Tools::getShopProtocol());
        $view = $helper->generateView();
        
        $this->context->smarty->assign('view', $view);
        //changes over
        $this->context->smarty->assign('form', $form);
        $this->context->smarty->assign('selected_nav', 'config');
        $this->context->smarty->assign(
            'admin_sl_setting_controller',
            $this->context->link->getAdminLink('AdminModules', true)
            . '&configure=' . urlencode($this->name) . '&tab_module=' . $this->tab
        );
        $this->context->smarty->assign(
            'admin_sl_email_controller',
            $this->context->link->getAdminLink('AdminKbSLEmailNotification', true)
        );
        $this->context->smarty->assign(
            'admin_sl_delivery_slip_controller',
            $this->context->link->getAdminLink('AdminKbSLDeliverySlip', true)
        );
        $this->context->smarty->assign(
            'admin_sl_pickup_time_controller',
            $this->context->link->getAdminLink('AdminKbSLTimeSetting', true)
        );
        $this->context->smarty->assign(
            'admin_sl_importer_controller',
            $this->context->link->getAdminLink('AdminKbSLImporter', true)
        );
        $this->context->smarty->assign('firstCall', false);
        $this->context->smarty->assign(
            'kb_tabs',
            $this->context->smarty->fetch(
                _PS_MODULE_DIR_ . $this->name . '/views/templates/admin/kb_tabs.tpl'
            )
        );

        //Loads JS and CSS
        $this->setKbMedia(0);

        /*
         * Generate form using Helper class
         */
        $helper = new Helper();
        $tpl = 'Form_custom.tpl';
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->override_folder = 'helpers/';
        $helper->base_folder = 'form/';
        
        $helper->base_tpl = 'view_custom.tpl';
        $helper->setTpl($tpl);
        $tpl = $helper->generate();
        $output .= $tpl;
        return $output;
    }

    /*
     * Function to create configuration setting form
     */
    private function getConfigurationForm()
    {
        $time = time();
        $marker_image = $this->getModuleDirUrl() . $this->name . '/views/img/marker.png?time='.$time;
        $exist_file = _PS_MODULE_DIR_.$this->name. '/views/img/user_marker.*';
        $match1 = glob($exist_file);
        if (count($match1) > 0) {
            $ban = explode('/', $match1[0]);
            $ban = end($ban);
            $ban = trim($ban);
            $img_url = $this->getModuleDirUrl() . $this->name . '/views/img/' . $ban;
            if (file_exists($match1[0])) {
                $marker_image = $img_url.'?time='.$time;
            }
        }
        $marker_url = "<img id='kbslmarker' class='img img-thumbnail'  src='".$marker_image."' width='35px;' height='35px;'>";
        // changes by rishabh jain
        $logo_image = $this->getModuleDirUrl() . $this->name . '/views/img/store_logo.jpg?time='.$time;
        $exist_file = _PS_MODULE_DIR_.$this->name. '/views/img/user_logo.*';
        
        $match1 = glob($exist_file);
        if (count($match1) > 0) {
            $ban = explode('/', $match1[0]);
            $ban = end($ban);
            $ban = trim($ban);
            $img_url = $this->getModuleDirUrl() . $this->name . '/views/img/' . $ban;
            if (file_exists($match1[0])) {
                $logo_image = $img_url.'?time='.$time;
            }
        }
        $logo_url = "<img id='kbslstorelogo' class='img img-thumbnail'  src='".$logo_image."'>";
        // changes over
        
        $zoom_level = array();
        for ($i = 0; $i < 21; $i++) {
            $zoom_level[] = array(
                'id' => $i,
                'name' => $i
            );
        }
        if (version_compare(_PS_VERSION_, '1.7.4.0', '>=')) {
            $available_store = Db::getInstance()->executeS(
                'SELECT s.id_store,ss.name FROM `' . _DB_PREFIX_ . 'store` s '
                . 'INNER JOIN ' . _DB_PREFIX_ . 'store_lang ss '
                . 'on (s.id_store=ss.id_store AND ss.id_lang='
                . (int) Context::getContext()->language->id . ') '
                . 'WHERE s.active=1 ORDER BY s.`id_store`'
            );
        } else {
            $available_store = Store::getStores(Context::getContext()->language->id);
        }
        
        
        $form = array(
            'form' => array(
                'id_form' => 'general_configuration_form',
                'legend' => array(
                    'title' => $this->l('General Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'label' => $this->l('Enable/Disable'),
                        'type' => 'switch',
                        'name' => 'kbstorelocator[enable]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable/Disable the plugin')
                    ),
                    array(
                        'label' => $this->l('Enable/Disable Store Locator'),
                        'type' => 'switch',
                        'name' => 'kbstorelocator[enable_store_locator]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable/Disable the Pickup at Store')
                    ),
                    array(
                        'label' => $this->l('Enable/Disable Pickup at Store'),
                        'type' => 'switch',
                        'name' => 'kbstorelocator[enable_pickup]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable/Disable the Pickup at Store')
                    ),
//                    changes by tarun
                    array(
                        'label' => $this->l('Google Map API Key'),
                        'type' => 'text',
                        'required' => true,
                        'name' => 'kbstorelocator[api_key]',
                        'hint' => $this->l('Enter the Google Map API key'),
                        'col'=> 5,
                        'desc' => $this->l('Click here to').' <a href="#steps_to_configure_google_API">'.$this->l('create Google Map API key').'</a>',
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Google Map Marker'),
                        'name' => 'map_marker',
                        'required' => true,
                        'image' => $marker_url ? $marker_url : false,
                        'desc' => $this->l('For the best view, upload 30 x 30 pixel size PNG image file.'),
                        'display_image' => true,
                        'hint' => $this->l('Upload image to set as Google Map Marker')
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Map Zoom Level'),
                        'name' => 'kbstorelocator[zoom_level]',
                        'required' => true,
                        'hint' => $this->l('Select the default zoom level view of map'),
                        'desc' => $this->l('Recommended Zoom Level: 13'),
                        'options' => array(
                            'query'=> $zoom_level,
                            'id' => 'id',
                            'name'=> 'name',
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Distance Unit'),
                        'name' => 'kbstorelocator[distance_unit]',
                        'required' => true,
                        'hint' => $this->l('Select the unit of distance measure'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 'mi',
                                    'name' => $this->l('Mile'),
                                ),
                                array(
                                    'id' => 'km',
                                    'name' => $this->l('Kilometer'),
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display in homepage'),
                        'required' => false,
                        'name' => 'kbstorelocator[homepage]',
                        'desc' => $this->l('This setting will display store locator on the homepage in frontend.'),
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Enable Stores'),
                        'name' => 'kbstorelocator[avilable_store][]',
                        'required' => true,
                        'multiple' => 'true',
                        'id' => 'multiple-select-avilable_store',
                        'hint' => $this->l('Select the stores to display in the Google Map'),
                        'options' => array(
                            'query' => $available_store,
                            'id' => 'id_store',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Default Store'),
                        'name' => 'kbstorelocator[default_store]',
                        'required' => true,
                        'hint' => $this->l('Display the default store in the Google Map'),
                        'options' => array(
                            'query' => $available_store,
                            'id' => 'id_store',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Phone number'),
                        'required' => false,
                        'name' => 'kbstorelocator[display_phone]',
                        'desc' => $this->l('Enable/Disable to display phone number of the store'),
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                    ),
                    // changes by rishabh jain
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display get direction Link'),
                        'name' => 'kbstorelocator[get_direction_link]',
                        'hint' => $this->l('This will add a get direction link in store listing.Click on same will show the direction to the selected store.'),
                        'values' => array(
                            array(
                                'value' => 1
                            ),
                            array(
                                'value' => 0
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Header Menu'),
                        'name' => 'kbstorelocator[header_menu]',
                        'hint' => $this->l('This will enable to display the Store Locator in Header.'),
                        'values' => array(
                            array(
                                'value' => 1
                            ),
                            array(
                                'value' => 0
                            )
                        ),
                    ),
                    // changes by rishabh jain
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Website link'),
                        'name' => 'kbstorelocator[website_link]',
                        'hint' => $this->l('This will enable to display the website link on the google maps store location.'),
                        'values' => array(
                            array(
                                'value' => 1
                            ),
                            array(
                                'value' => 0
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Store Image'),
                        'name' => 'kbstorelocator[store_image]',
                        'hint' => $this->l('This will display the store image on the stores listing.'),
                        'values' => array(
                            array(
                                'value' => 1
                            ),
                            array(
                                'value' => 0
                            )
                        ),
                    ),
                    //changes by tarun
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display All Stores in map'),
                        'name' => 'kbstorelocator[show_stores]',
                        'hint' => $this->l('This will display all the enabled stores in the map by default.'),
                        'values' => array(
                            array(
                                'value' => 1
                            ),
                            array(
                                'value' => 0
                            )
                        ),
                    ),
                    //changes over
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable Country Restriction'),
                        'name' => 'kbstorelocator[enable_country_restriction]',
                        'hint' => $this->l('This will restrict the customer to select the pickup of country other than the selectd delivery address.'),
                        'values' => array(
                            array(
                                'value' => 1
                            ),
                            array(
                                'value' => 0
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Store Banner on store page'),
                        'name' => 'kbstorelocator[display_store_banner]',
                        'hint' => $this->l('This will enable to display the store banner on the store locator page.'),
                        'values' => array(
                            array(
                                'value' => 1
                            ),
                            array(
                                'value' => 0
                            )
                        ),
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Banner Image'),
                        'name' => 'banner_image',
                        'required' => true,
                        'image' => $logo_url ? $logo_url : false,
                        'desc' => $this->l('For the best view, upload 30 x 30 pixel size PNG image file.'),
                        'display_image' => true,
                        'hint' => $this->l('Upload image to set as banner for front store page')
                    ),
                    //hanges over
                    array(
                        'type' => 'text',
                        'label' => $this->l('Header Menu Text'),
                        'name' => 'header_menu_text',
                        'hint' => $this->l('Enter the text which to be display in the Header Menu in frontend'),
                        'lang' => true,
                        'required' => true,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right form_general'
                ),
            ),
        );
        return $form;
    }
    
    public function hookdisplayPaymentByBinaries($params)
    {
        $id_cart = $this->context->cart->id;
        $id_customer = $this->context->customer->id;
        $id_carrier = $this->context->cart->id_carrier;
        if (!empty($id_cart) && !empty($id_customer)) {
            if ($id_carrier == Configuration::get('KB_PICKUP_AT_STORE_SHIPPING')) {
                $this->context->controller->addJS($this->_path . '/views/js/front/kb_front.js');
                $pickup = Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_time WHERE id_cart=' . (int) $id_cart . ' AND id_shop=' . (int) $this->context->shop->id . ' AND id_customer=' . (int) $id_customer);
                if (!empty($pickup)) {
                    $store = new Store($pickup['id_store'], Context::getContext()->language->id);
//                    print_r($id_lang);
//                    die;
                    $this->context->smarty->assign(array(
                        'store' => $store,
                        'id_lang' => $this->context->language->id,
                        'pickup_time' => $pickup['preferred_date'],
                    ));
//                    print_r($store);
//                    die;
                    return $this->display(__FILE__, 'views/templates/hook/display_order_confirmation.tpl');
                }
            }
        }
    }
    
    /*
     * hook function to map store with the Address if new store is created
     */
    public function hookActionObjectStoreAddAfter($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (Module::isInstalled($this->name) && !empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable']) {
                if (!empty($params) && isset($params['object'])) {
                    $store = (array) $params['object'];
                    if (version_compare(_PS_VERSION_, '1.7.3.0', '>=')) {
                        $id_lang = Context::getContext()->language->id;
                        if (isset($store['name'][$id_lang]) && !empty($store['name'][$id_lang])) {
                            $store['name'] = $store['name'];
                        }
                    }
                    if (isset($store['name']) && !empty($store['name'])) {
                        if (!Validate::isName($store['name'])) {
                            $store['name'] = 'xx xx';
                        }
                        $store['id_store'] = $store['id'];
                        self::kbStoreAddressMapping($store);
                    }
                }
            }
        }
    }
    
    /*
     * hook function to update the address if store is updated
     */
    public function hookActionObjectStoreUpdateAfter($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (Module::isInstalled($this->name) && !empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable']) {
//                d($params);
                if (!empty($params) && isset($params['object'])) {
                    $store = (array) $params['object'];
                    
                    if (version_compare(_PS_VERSION_, '1.7.3.0', '>=')) {
                        $id_lang = Context::getContext()->language->id;
                        if (isset($store['name'][$id_lang]) && !empty($store['name'][$id_lang])) {
                            $store['name'] = $store['name'][$id_lang];
                        }
                    }
                    
                    if (isset($store['name']) && !empty($store['name'])) {
                        if (!Validate::isName($store['name'])) {
                            //$store['name'] = 'xx xx';
                        }
                        $store['id_store'] = $store['id'];
                        self::kbStoreAddressMapping($store);
                    }
                }
            }
        }
    }
    
    /*
     * hook function to delete Address if the store is removed
     */
    public function hookActionObjectStoreDeleteAfter($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (Module::isInstalled($this->name) && !empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable']) {
                if (!empty($params) && isset($params['object'])) {
                    $id_store = $params['object']->id;
                    $existing_record = Db::getInstance()->getRow('SELECT count(*) as count,id_address FROM '._DB_PREFIX_.'kb_pickup_store_address_mapping Where id_store='.(int)$id_store);
                    if (!empty($existing_record)) {
                        $id_address = $existing_record['id_address'];
                        $addr = new Address($id_address);
                        if ($addr->delete()) {
                            Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'kb_pickup_store_address_mapping where id_store='.(int)$id_store);
                        }
                    }
                }
            }
        }
    }

    /*
     * hook function to execute the display header hook
     */
    public function hookDisplayHeader()
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_store_locator']) {
                if ($this->context->controller->php_self == 'stores') {
                    Tools::redirect($this->context->link->getModuleLink($this->name, 'stores', array(), (bool) Configuration::get('PS_SSL_ENABLED')));
                }
            }
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup']) {
                $this->setKbMedia(1);
            }
        }
    }
    
    /*
     * hook function to display the menu link in the header in frontend
     */
    public function hookDisplayNav2()
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_store_locator'] && $storeLocatorConfig['header_menu']) {
                $id_lang = $this->context->language->id;
                if (isset($storeLocatorConfig['header_menu_text'][$id_lang])) {
                    $menu_text = $storeLocatorConfig['header_menu_text'][$id_lang];
                    if (!empty($menu_text)) {
                        $this->context->smarty->assign(array(
                            'menu_text' => $menu_text,
                            'fc_link' => $this->context->link->getModuleLink($this->name, 'stores', array(), (bool) Configuration::get('PS_SSL_ENABLED')),
                        ));
                        return $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->name . '/views/templates/hook/header_menu.tpl');
                    }
                }
            }
        }
    }
    
    
    public function hookDisplayHome($params = null)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            $this->setKbMedia(1);
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_store_locator']) {
                if ($storeLocatorConfig['homepage']) {
                    $av_store_detail = array();
                    $default_store = $storeLocatorConfig['default_store'];
                    $store = new Store($default_store, $this->context->language->id);
                    $latitude = $store->latitude;
                    $longitude = $store->longitude;
                    $selected_store = new Store($default_store);
                    $selected_store = (array) $selected_store;
                    $selected_store['country'] = Country::getNameById($this->context->language->id, $selected_store['id_country']);
                    $default_latitude = $store->latitude;
                    $default_longitude = $store->longitude;
                    $enabled_store = $storeLocatorConfig['avilable_store'];
                    $available_stores = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                    foreach ($available_stores as $key => $store) {
                        $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store);
                        $available_stores[$key]['hours'] = $workingHours;
                        $av_store_detail[$store['id_store']] = $workingHours;
                    }
                    $time = time();
                    $marker = $this->getModuleDirUrl() . $this->name . '/views/img/marker.png?time='.$time;
                    $exist_file = _PS_MODULE_DIR_ . $this->name . '/views/img/user_marker.*';
                    $match1 = glob($exist_file);
                    if (count($match1) > 0) {
                        $ban = explode('/', $match1[0]);
                        $ban = end($ban);
                        $ban = trim($ban);
                        if (file_exists($match1[0])) {
                            $marker = $this->getModuleDirUrl() . $this->name . '/views/img/' . $ban.'?time='.$time;
                        }
                    }
                    // changes by rishabh jain
                    $link = new Link;
                    // changes over
                    //changes by tarun to show all stores in the map
                    if ($storeLocatorConfig['show_stores'] == 0) {
                        $this->context->smarty->assign('kb_all_stores', '');
                    } else {
                        $radius_val = 0;
                        $filter_pickup = 1;
                        $available_stores1 = array();
                        $enabled_store = $storeLocatorConfig['avilable_store'];
                        $available_stores1 = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                        $distance_unit = "M";
                        if ($storeLocatorConfig['distance_unit'] == 'km') {
                            $distance_unit = "K";
                        }
                        $near_by_store = array();
                        $marker_json_data = array();
                        $av_store_detail = array();
                        $default_store = $storeLocatorConfig['default_store'];
                        $this->context->smarty->assign(
                            array(
                                'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                                'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                                'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                                'index_page_link' => $this->context->link->getPageLink('index'),
                                'distance_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/distance.png',
                                'phone_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png',
                                'web_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/web.png',
                                'clock_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/clock.png',
                                'store_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/store.png',
                            )
                        );
                        $this->context->smarty->assign('distance_icon', $this->getModuleDirUrl() . $this->name . '/views/img/front/distance.png');
                        $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png');
                        foreach ($available_stores1 as $key => $store1) {
                            $near_by_distance = Tools::ps_round($this->calculateDistance($latitude, $longitude, $store1['latitude'], $store1['longitude'], $distance_unit), 2);
                            if ($near_by_distance <= $radius_val || $radius_val == 0) {
                                $json_data = array();
                                $available_stores1[$key]['store_distance'] = $near_by_distance;
                                $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store1);
                                $available_stores1[$key]['hours'] = $workingHours;
                                $near_by_store[] = $available_stores1[$key];
                                $this->context->smarty->assign('display_phone', $storeLocatorConfig['display_phone']);
                                $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png');
                                $this->context->smarty->assign('filter_pickup', $filter_pickup);
                                $this->context->smarty->assign('available_store', $available_stores1[$key]);

                                $json_data[] = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'kbstorelocatorpickup/views/templates/hook/search_marker_result.tpl');
                                $json_data[] = $store1['latitude'];
                                $json_data[] = $store1['longitude'];
                                $marker_json_data[] = $json_data;
                                $av_store_detail[$store1['id_store']] = $workingHours;
                            }
                        }
                        $this->context->smarty->assign('kb_all_stores', json_encode($marker_json_data));
                    }
                    //changes over
                    $this->context->smarty->assign(
                        array(
                            'kb_store_url' => $this->context->link->getModuleLink($this->name, 'stores'),
                            'map_api' => $storeLocatorConfig['api_key'],
                            // changes by rishabh jain
                            'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                            'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                            //changes by tarun
                            'is_enabled_show_stores' => $storeLocatorConfig['show_stores'],
                            //changes over
                            'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                            'index_page_link' => $link->getPageLink('index'),
                            // change over
                            'selected_store' => $selected_store,
                            'lang_iso' => $this->context->language->iso_code,
                            'zoom_level' => $storeLocatorConfig['zoom_level'],
                            'default_latitude' => $default_latitude,
                            'default_longitude' =>$default_longitude,
                            'display_phone' => $storeLocatorConfig['display_phone'],
                            'default_store' => $default_store,
                            'distance_unit' => $storeLocatorConfig['distance_unit'], //to be implemented
                            'distance_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/distance.png',
                            'phone_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png',
                            'web_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/web.png',
                            'clock_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/clock.png',
                            'store_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/store.png',
                            'up_arrow' => $this->getModuleDirUrl() . $this->name . '/views/img/front/up_arrow.png',
                            'down_arrow' => $this->getModuleDirUrl() . $this->name . '/views/img/front/down_arrow.png',
                            'locator_icon' => $marker,
                            'current_location_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/current_location.png',
                            'available_stores' => $available_stores,
                            'av_store_detail' => json_encode($av_store_detail),
                            'default_store_detail' => json_encode($av_store_detail[$default_store]),
                            'id_lang' => $this->context->language->id,
                            'spinner' => $this->getModuleDirUrl().$this->name.'/views/img/front/spinner.gif',
                        )
                    );
                    return $this->display(__FILE__, 'views/templates/front/home_stores.tpl');
                }
            }
        }
    }
    
    /*
     * hook function to execute the action dispatcher when page load
     */
    public function hookActionDispatcher($params = null)
    {
//        $controller = $params['controller_class'];
//        if ($controller == 'AdminCarriersController' || $controller == 'AdminCarrierWizardController') {
//            $pickup_carrier = Configuration::get('KB_PICKUP_AT_STORE_SHIPPING');
//            if (Tools::getIsset('id_carrier')) {
//                if (!empty($pickup_carrier) && $pickup_carrier == Tools::getValue('id_carrier')) {
//                    $this->context->cookie->kbcarrierredirect = 1;
//                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminCarriers'));
//                }
//            }
//            
//            if (isset($_REQUEST['submitBulkenableSelectioncarrier']) || Tools::getValue('submitBulkdeletecarrier')
//                == "") {
//                $carrier_boxes = Tools::getValue('carrierBox');
//                if (!empty($carrier_boxes)) {
//                    foreach ($carrier_boxes as $carrier_box) {
//                        if (!empty($pickup_carrier) && $pickup_carrier == $carrier_box) {
//                            $this->context->cookie->kbcarrierredirect = 1;
//                            Tools::redirectAdmin($this->context->link->getAdminLink('AdminCarriers'));
//                        }
//                    }
//                }
//            } else {
//                $id_carrier = (int) Tools::getValue('id_carrier', 0);
//                $carrier    = new Carrier($id_carrier);
//                if (!empty($pickup_carrier) && $pickup_carrier == $carrier->id_reference) {
//                    $this->context->cookie->kbcarrierredirect = 1;
//                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminCarriers'));
//                }
//            }
//        }
    }
    
    public function hookActionCarrierUpdate($params = null)
    {
        $pickup_carrier = Configuration::get('KB_PICKUP_AT_STORE_SHIPPING');
        if ($pickup_carrier == $params['id_carrier']) {
            Configuration::updateValue('KB_PICKUP_AT_STORE_SHIPPING', $params['carrier']->id);
        }
    }
    public function hookDisplayBackOfficeHeader()
    {
        $controller = $this->context->controller->controller_name;
        if (($controller == 'AdminCarriers'
            || $controller == 'AdminCarrierWizard')
            && isset($this->context->cookie->kbcarrierredirect)
            && $this->context->cookie->kbcarrierredirect) {
            $msg = $this->l('You do not have permission to edit, delete or update pickeup carriers.');
            $this->context->controller->errors[] = $msg;
            unset($this->context->cookie->kbcarrierredirect);
        }
    }
    
    /*
     * function to display the pickup at store with search store in Cart page
     */
    public function kbDisplayPickupStoreShipping($params, $storeLocatorConfig)
    {
        if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup']) {
//            $default_carrier = array();
//            var_dump($params);
//            die;
//            if (isset($params['cart']->delivery_option)) {
//                if (version_compare(_PS_VERSION_, '1.7.4.0', '>')) {
//                    $default_carrier = unserialize($params['cart']->delivery_option);
//                } else {
//                    $default_carrier = Tools::jsonDecode($params['cart']->delivery_option, true);
//                }
//            }
            
//            $available_shipping_list = $this->context->cart->getDeliveryOptionList();
            $current_selected_shipping = current($this->context->cart->getDeliveryOption(null, false, false));
//            $id_address = 
//            Tools::dieObject($this->context->cart->id_address_delivery, true);
//            
            $this->setKbMedia(1);
            $pickup_carrier_id = Configuration::get('KB_PICKUP_AT_STORE_SHIPPING');
            $time = time();
            $marker = $this->getModuleDirUrl() . $this->name . '/views/img/marker.png?time='.$time;
            $exist_file = _PS_MODULE_DIR_ . $this->name . '/views/img/user_marker.*';
            $match1 = glob($exist_file);
            if (count($match1) > 0) {
                $ban = explode('/', $match1[0]);
                $ban = end($ban);
                $ban = trim($ban);
                if (file_exists($match1[0])) {
                    $marker = $this->getModuleDirUrl() . $this->name . '/views/img/' . $ban.'?time='.$time;
                }
            }
            
            $default_store = $storeLocatorConfig['default_store'];
            $enabled_store = $storeLocatorConfig['avilable_store'];
            $available_stores = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
            $av_store_detail = array();
            // changes by rishabh jain
            $id_address_delivery = (int) $params['cart']->id_address_delivery;
            $address_obj = new Address($id_address_delivery);
            $selected_add_country_id = $address_obj->id_country;
            $is_enabled_country_restriction = 0;
            if (isset($storeLocatorConfig['enable_country_restriction']) && $storeLocatorConfig['enable_country_restriction'] == 1) {
                $is_enabled_country_restriction =  1;
            }
            $updated_available_stores = array();
            // changes over
            foreach ($available_stores as $key => $store) {
                if ($is_enabled_country_restriction) {
                    if ($selected_add_country_id == $store['id_country']) {
                        $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store);
                        $available_stores[$key]['hours'] = $workingHours;
                        $av_store_detail[$store['id_store']] = $workingHours;
                    } else {
                        unset($available_stores[$key]);
                    }
                } else {
                    $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store);
                    $available_stores[$key]['hours'] = $workingHours;
                    $av_store_detail[$store['id_store']] = $workingHours;
                }
            }
            foreach ($available_stores as $key => $store) {
                $updated_available_stores[] = $store;
            }
            // changes over
            $cart_pickup = Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_time WHERE id_cart=' . (int) $this->context->cart->id . ' AND id_customer=' . (int) $this->context->customer->id . ' AND id_shop=' . (int) $this->context->shop->id);
            
            $store = new Store($default_store, Context::getContext()->language->id);
            $latitude = $store->latitude;
            $longitude = $store->longitude;
            $selected_store = '';
            if (!empty($cart_pickup)) {
                $selected_store = new Store($cart_pickup['id_store'], Context::getContext()->language->id);
                $selected_store = (array) $selected_store;
                $selected_store['country'] = Country::getNameById($this->context->language->id, $selected_store['id_country']);
//                $selected_store['store'] = Country::getNameById($this->context->language->id, $selected_store['id_country']);
                $default_store = $cart_pickup['id_store'];
                $latitude = $selected_store['latitude'];
                $longitude = $selected_store['longitude'];
            } else {
                $selected_store = new Store($default_store, Context::getContext()->language->id);
                $selected_store = (array) $selected_store;
                $selected_store['country'] = Country::getNameById($this->context->language->id, $selected_store['id_country']);
            }
            $pickup_settings = Tools::jsonDecode(Configuration::get('KB_PICKUP_TIME_SETTINGS'), true);
            
            // changes by rishabh jain
            $is_enabled_date_selcetion = 1;
            if (isset($pickup_settings['enable_date_selection']) && $pickup_settings['enable_date_selection'] == 0) {
                $is_enabled_date_selcetion = 0;
            }
            $this->context->smarty->assign('is_enabled_date_selcetion', $is_enabled_date_selcetion);
            // changes over
            $date_format = $pickup_settings['format'];
            if (!empty($date_format)) {
                $date_format = str_replace('%y', 'yyyy', $date_format);
                $date_format = str_replace('%m', 'mm', $date_format);
                $date_format = str_replace('%d', 'dd', $date_format);
                if ($pickup_settings['time_slot']) {
                    $date_format = str_replace('%t24', 'hh:ii', $date_format);
                    $date_format = str_replace('%t12', 'HH:ii P', $date_format);
                } else {
                    $date_format = str_replace('%t24', '', $date_format);
                    $date_format = str_replace('%t12', '', $date_format);
                }
            }
            
            $hour_gap = time() +($pickup_settings['days_gap']*24*60*60) + $pickup_settings['hours_gap']*60*60;
            $hour_gap_date = date('j', $hour_gap);
            $hour_gap_hour = date('H', $hour_gap);
            $hour_gap_month = date('n', $hour_gap) - 1;
            if (Module::isInstalled('supercheckout') && Module::isEnabled('supercheckout')) {
                if (Tools::getIsset('controller') && Tools::getValue('controller') == 'supercheckout') {
                    $this->context->smarty->assign('kb_superck', true);
                }
            }
//code for checking that customer save date is greater than current start
            if (!empty($cart_pickup['preferred_date']) && $cart_pickup['preferred_date'] < date('Y-m-d H:i:s', $hour_gap)) {
                $cart_pickup['preferred_date'] = '';
            }
//code for checking that customer save date is greater than current start       

            $is_all_stores_disabled = 0;
            if (is_array($updated_available_stores) && count($updated_available_stores) == 0) {
                $is_all_stores_disabled = 1;
            }
            $default_selected_Store = 0;
            //changes by tarun
            if (is_array($updated_available_stores) && count($updated_available_stores) != 0) {
                //changes over
                foreach ($updated_available_stores as $store_key => $store_data) {
                    if ($store_data['id_store'] == $default_store) {
                        $default_selected_Store = $default_store;
                        break;
                    }
                }
            }
            // changes by rishabh jain
            if ($default_selected_Store == 0) {
                $updated_default_selected_store = '';
            } else {
                $updated_default_selected_store = $default_selected_Store;
            }
            // changes over
//            print_r($av_store_detail);
//            die;
            if (!empty($av_store_detail[$default_store])) {
                $default_store_detail = json_encode($av_store_detail[$default_store]);
            } else {
                foreach ($av_store_detail as $key => $value) {
                    $default_store_detail = json_encode($value);
                }
            }
            if (count($av_store_detail)==0) {
                $default_store_detail="";
            }
//            print_r($updated_available_stores);die;
           // $updated_available_stores = array_slice($updated_available_stores,1);
            //changes by tarun to show all stores in the map
            if ($storeLocatorConfig['show_stores'] == 0) {
                $this->context->smarty->assign('kb_all_stores', '');
            } else {
                $radius_val = 0;
                $filter_pickup = 1;
                $available_stores1 = array();
                $enabled_store = $storeLocatorConfig['avilable_store'];
                $available_stores1 = Kbstorelocatorpickup::kbAvailableStores($enabled_store);
                $distance_unit = "M";
                if ($storeLocatorConfig['distance_unit'] == 'km') {
                    $distance_unit = "K";
                }
                $near_by_store = array();
                $marker_json_data = array();
                $av_store_detail = array();
                $default_store = $storeLocatorConfig['default_store'];
                $this->context->smarty->assign(
                    array(
                        'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                        'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                        'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                        'index_page_link' => $this->context->link->getPageLink('index'),
                        'distance_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/distance.png',
                        'phone_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png',
                        'web_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/web.png',
                        'clock_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/clock.png',
                        'store_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/store.png',
                    )
                );
                $this->context->smarty->assign('distance_icon', $this->getModuleDirUrl() . $this->name . '/views/img/front/distance.png');
                $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png');
                foreach ($available_stores1 as $key => $store1) {
                    $near_by_distance = Tools::ps_round($this->calculateDistance($latitude, $longitude, $store1['latitude'], $store1['longitude'], $distance_unit), 2);
                    if ($near_by_distance <= $radius_val || $radius_val == 0) {
                        $json_data = array();
                        $available_stores1[$key]['store_distance'] = $near_by_distance;
                        $workingHours = Kbstorelocatorpickup::renderKbStoreWorkingHours($store1);
                        $available_stores1[$key]['hours'] = $workingHours;
                        $near_by_store[] = $available_stores1[$key];
                        $this->context->smarty->assign('display_phone', $storeLocatorConfig['display_phone']);
                        $this->context->smarty->assign('phone_icon', $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png');
                        $this->context->smarty->assign('filter_pickup', $filter_pickup);
                        $this->context->smarty->assign('available_store', $available_stores1[$key]);

                        $json_data[] = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'kbstorelocatorpickup/views/templates/hook/search_marker_result.tpl');
                        $json_data[] = $store1['latitude'];
                        $json_data[] = $store1['longitude'];
                        $marker_json_data[] = $json_data;
                        $av_store_detail[$store1['id_store']] = $workingHours;
                    }
                }
                $this->context->smarty->assign('kb_all_stores', json_encode($marker_json_data));
            }
            //changes over
            $this->context->smarty->assign(
                array(
                    // changes by rishabh jain
                    'is_enabled_website_link' => $storeLocatorConfig['website_link'],
                    'is_enabled_store_image' => $storeLocatorConfig['store_image'],
                    //changes by tarun
                    'is_enabled_show_stores' => $storeLocatorConfig['show_stores'],
                    //changes over
                    'is_enabled_direction_link' => $storeLocatorConfig['get_direction_link'],
                    'index_page_link' => $this->context->link->getPageLink('index'),
                    // change over
                    'current_selected_shipping' => $current_selected_shipping,
                    'kb_store_url' => $this->context->link->getModuleLink($this->name, 'stores'),
                    'map_api' => $storeLocatorConfig['api_key'],
                    'default_latitude' => $latitude,
                    'default_longitude' => $longitude,
                    'default_store' => $updated_default_selected_store,
                    'store' => $store,
                    'zoom_level' => $storeLocatorConfig['zoom_level'],
                    'distance_unit' => $storeLocatorConfig['distance_unit'], //to be implemented
                    'display_phone' => $storeLocatorConfig['display_phone'],
                    'available_stores' => $updated_available_stores,
                    'av_store_detail' => json_encode($av_store_detail),
                    // changes by rishabh jain
                    'is_all_stores_disabled' => $is_all_stores_disabled,
                    'default_store_detail' => $default_store_detail,
                    // changes over
                    'lang_iso' => $this->context->language->iso_code,
                    'cart_pickup' => $cart_pickup,
                    'selected_store' => $selected_store,
                    'id_lang' => $this->context->language->id,
                    //pickup_time
                    'current_date' => strtotime(date('d-m-Y')) * 1000,
                    'days_gap' => date('Y-m-d', strtotime('+' . $pickup_settings['days_gap'] . 'days')),
                    'maximum_days' => date('Y-m-d', strtotime('+' . $pickup_settings['maximum_days'] . 'days')),
                    'time_slot' => $pickup_settings['time_slot'],
                    'hours_gap' => $hour_gap,
                    'hour_gap_date' => $hour_gap_date,
                    'hour_gap_hour' => $hour_gap_hour,
                    'hour_gap_month' => $hour_gap_month,
                    'format' => $date_format,
                    'pickup_carrier_id' => $pickup_carrier_id,
                    'distance_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/distance.png',
                    'phone_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/call.png',
                    'web_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/web.png',
                    'clock_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/clock.png',
                    'store_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/store.png',
                    'locator_icon' => $marker,
                    'current_location_icon' => $this->getModuleDirUrl() . $this->name . '/views/img/front/current_location.png',
                    'spinner' => $this->getModuleDirUrl().$this->name.'/views/img/front/spinner.gif',
                //    'default_carrier' => $default_carrier,
                )
            );
            
            return $this->display(__FILE__, 'views/templates/hook/pickup_stores.tpl');
        }
    }
    
    public function hookDisplayBeforeCarrier($params)
    {
//        return '';
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        $pickup_settings = Tools::jsonDecode(Configuration::get('KB_PICKUP_TIME_SETTINGS'), true);
        if (!empty($storeLocatorConfig) && !empty($pickup_settings)) {
            return $this->kbDisplayPickupStoreShipping($params, $storeLocatorConfig);
        }
    }
    
    public function hookDisplayPDFInvoice($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup']) {
                $order = new Order($params['object']->id_order);
                if (!empty($order->id_cart)) {
                    $id_cart = $order->id_cart;
                    $data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'kb_pickup_at_store_time WHERE id_cart='.(int)$id_cart.' AND id_shop='.(int)$order->id_shop);
                    if (!empty($data)) {
                        $store = new Store($data['id_store'], Context::getContext()->language->id);
                        $this->context->smarty->assign(array(
                            'store' => $store,
                            'id_lang' => $this->context->language->id,
                            'pickup_time' => $data['preferred_date'],
                        ));
                        return $this->display(__FILE__, 'display_invoice.tpl');
                    }
                }
            }
        }
    }
    
    public function hookDisplayOrderConfirmation($params)
    {
//        Tools::dieObject($params);
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup'] && $params) {
                $this->context->controller->addJS($this->_path . '/views/js/front/kb_front.js');
                $id_cart = $params['order']->id_cart;
                $pickup = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'kb_pickup_at_store_time WHERE id_cart='.(int)$id_cart.' AND id_shop='.(int)$this->context->shop->id.' AND id_customer='.(int)$this->context->customer->id);
                
                if (!empty($pickup)) {
                    $store = new Store($pickup['id_store'], Context::getContext()->language->id);
                    $this->context->smarty->assign(array(
                        'store' => $store,
                        'id_lang' => $this->context->language->id,
                        'pickup_time' => $pickup['preferred_date'],
                    ));
                    return $this->display(__FILE__, 'views/templates/hook/display_order_confirmation.tpl');
                }
            }
        }
    }
    
    public function hookDisplayOrderDetail($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup'] && $params) {
                $id_cart = $params['order']->id_cart;
                $id_shop = $params['order']->id_shop;
                $id_customer = $params['order']->id_customer;
                $data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'kb_pickup_at_store_time WHERE id_shop='.(int)$id_shop.' AND id_cart='.(int)$id_cart.' AND id_customer='.(int)$id_customer);
                if (!empty($data)) {
                     $store = new Store($data['id_store'], Context::getContext()->language->id);
                    $this->context->smarty->assign(array(
                        'store' => $store,
                        'pickup_time' => $data['preferred_date'],
                        'id_lang' => $this->context->language->id,
                        'order_history' => true,
                    ));
                    return $this->display(__FILE__, 'views/templates/hook/display_order_confirmation.tpl');
                }
            }
        }
    }
    
    public function hookactionOrderStatusPostUpdate($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup']  && !empty($params)) {
                $id_cart = $params['cart']->id;
                $id_shop = $params['cart']->id_shop;
                $id_customer = $params['cart']->id_customer;
                $id_carrier = $params['cart']->id_carrier;
                $data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'kb_pickup_at_store_time where id_cart='.(int)$id_cart.' AND id_shop='.(int)$id_shop.' AND id_customer='.(int)$id_customer);
                if (!empty($data)) {
                    if ($id_carrier == Configuration::get('KB_PICKUP_AT_STORE_SHIPPING')) {
                        $order = new Order($params['id_order']);
                        $order->id_address_delivery = $data['id_address_delivery'];
                        $order->save();
                    }
                }
            }
        }
    }
    
    public function hookActionValidateOrder($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup']  && !empty($params)) {
                $id_cart = $params['order']->id_cart;
                $id_carrier = $params['order']->id_carrier;
                $id_shop = $params['order']->id_shop;
                $id_customer = $params['order']->id_customer;
                $id_lang = $this->context->language->id;
                $data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'kb_pickup_at_store_time where id_cart='.(int)$id_cart.' AND id_shop='.(int)$id_shop.' AND id_customer='.(int)$id_customer);
                if (!empty($data)) {
                    if ($id_carrier == Configuration::get('KB_PICKUP_AT_STORE_SHIPPING')) {
                        $order = new Order($params['order']->id);
                        $cart = new Cart($id_cart);
                        $cart->id_address_delivery = $data['id_address_delivery'];
                        $this->context->cart->id_address_delivery = $data['id_address_delivery'];
                        $cart->save();
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product set id_address_delivery='.$this->context->cart->id_address_delivery.' where id_cart='.(int)$id_cart);
                        $order->id_address_delivery = $data['id_address_delivery'];
                        $order->save();
//                        d($order);
                        if (Configuration::get('kb_pickup_email_enable')) {
                            $store = new Store($data['id_store']);
                            if (!empty($store->email) && Validate::isEmail($store->email)) {
                                $mail_template = Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_email_template WHERE id_lang=' . (int) $id_lang);
                                $template = array(
                                    '{template}' => $mail_template['body'],
                                    '{new_arrival_link}' => $this->context->link->getPageLink('new-products'),
                                    '{price_drop_link}' => $this->context->link->getPageLink('prices-drop'),
                                    '{best_sale_link}' => $this->context->link->getPageLink('best-sales'),
                                    '{first_name}' => $store->name,
                                    '{minimal_image}' => $this->context->link->getMediaLink(
                                        __PS_BASE_URI__ . 'modules/' . $this->name . '/views/img/minimal6.png'
                                    ),
                                    '{order_id}' => $params['order']->reference,
                                    '{order_time}' => $params['order']->date_add,
                                    '{payment_method}' => $params['order']->payment,
                                    '{preferred_date}' => $data['preferred_date'],
                                    '{SHOP_NAME}' => Configuration::get('PS_SHOP_NAME'),
                                 );

                                Mail::Send(
                                    $id_lang,
                                    'order_mail',
                                    $mail_template['subject'],
                                    $template,
                                    $store->email,
                                    null,
                                    Configuration::get('PS_SHOP_EMAIL'),
                                    Configuration::get('PS_SHOP_NAME'),
                                    null,
                                    null,
                                    _PS_MODULE_DIR_ . $this->name.'/mails/'
                                );
                            }
                        }
                    } else {
                        Db::getInstance()->execute(
                            'DELETE FROM '._DB_PREFIX_.'kb_pickup_at_store_time WHERE id_cart='.(int)$id_cart
                            .' AND id_shop='.(int)$id_shop.' AND id_customer='.(int)$id_customer
                        );
                    }
                }
            }
        }
    }
    
    public function hookDisplayAdminOrderTabShip($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup'] && $params) {
                $id_cart = $params['order']->id_cart;
                $id_shop = $params['order']->id_shop;
                $data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'kb_pickup_at_store_time WHERE id_shop='.(int)$id_shop.' AND id_cart='.(int)$id_cart);
                if (!empty($data)) {
                    return $this->display(__FILE__, 'views/templates/hook/preferred_pickup_tab.tpl');
                }
            }
        }
    }
    
    public function hookDisplayAdminOrderContentShip($params)
    {
        $storeLocatorConfig = Tools::jsonDecode(Configuration::get('KB_STORE_LOCATOR_GENERAL_SETTING'), true);
        if (!empty($storeLocatorConfig)) {
            if ($storeLocatorConfig['enable'] && $storeLocatorConfig['enable_pickup'] && $params) {
                $id_cart = $params['order']->id_cart;
                $id_shop = $params['order']->id_shop;
                $data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'kb_pickup_at_store_time WHERE id_shop='.(int)$id_shop.' AND id_cart='.(int)$id_cart);
                if (!empty($data)) {
                    $store = new Store($data['id_store'], Context::getContext()->language->id);
                    $this->context->smarty->assign(array(
                        'store' => $store,
                        'pickup_time' => $data['preferred_date'],
                        'id_lang' => $this->context->language->id,
                        'order_history' => true,
                    ));
                    return $this->display(__FILE__, 'views/templates/hook/preferred_pickup_tab_content.tpl');
                }
            }
        }
    }
    
    
    /*
     * function to return the store's working hours
     * return array
     */
    public static function renderKbStoreWorkingHours($store)
    {
        $module = Module::getInstanceByName('kbstorelocatorpickup');
        $hours = array();
        if ($store['hours']) {
            $hours = $store['hours'];
            if (is_array($hours)) {
                $hours = array_filter($hours);
            }
        }
        $data = array();
        $data[0]['day'] = $module->l('Monday', 'kbstorelocatorpickup');
        $data[1]['day'] = $module->l('Tuesday', 'kbstorelocatorpickup');
        $data[2]['day'] = $module->l('Wednesday', 'kbstorelocatorpickup');
        $data[3]['day'] = $module->l('Thursday', 'kbstorelocatorpickup');
        $data[4]['day'] = $module->l('Friday', 'kbstorelocatorpickup');
        $data[5]['day'] = $module->l('Saturday', 'kbstorelocatorpickup');
        $data[6]['day'] = $module->l('Sunday', 'kbstorelocatorpickup');
        
        if (!empty($hours)) {
            if (isset($hours[6][0])) {
                $data[6]['hours'] = $hours[6][0];
            }
            if (isset($hours[0][0])) {
                $data[0]['hours'] = $hours[0][0];
            }
            if (isset($hours[1][0])) {
                $data[1]['hours'] = $hours[1][0];
            }
            if (isset($hours[2][0])) {
                $data[2]['hours'] = $hours[2][0];
            }
            if (isset($hours[3][0])) {
                $data[3]['hours'] = $hours[3][0];
            }
            if (isset($hours[4][0])) {
                $data[4]['hours'] = $hours[4][0];
            }
            if (isset($hours[5][0])) {
                $data[5]['hours'] = $hours[5][0];
            }
        }
        
        return $data;
    }

    /*
     * function to get all the available stores
     * return array
     */
    public static function kbAvailableStores($enabled_store)
    {
        if (empty($enabled_store)) {
            return;
        }
        if (version_compare(_PS_VERSION_, '1.7.3.0', '<')) {
            $available_stores = Db::getInstance()->executeS(
                'SELECT s.* FROM `' . _DB_PREFIX_ . 'store` s '
                . 'INNER JOIN ' . _DB_PREFIX_ . 'store_shop ss '
                . 'on (s.id_store=ss.id_store AND ss.id_shop='
                . (int) Context::getContext()->shop->id . ') '
                . 'WHERE s.active=1 AND s.id_store IN ('
                . pSQL(implode(',', $enabled_store))
                . ') ORDER BY s.`id_store`'
            );
        } else {
            $available_stores = Db::getInstance()->executeS(
                'SELECT s.*,sl.* FROM `' . _DB_PREFIX_ . 'store` s '
                    . 'INNER JOIN ' . _DB_PREFIX_ . 'store_shop ss '
                    . 'on (s.id_store=ss.id_store AND ss.id_shop='
                    . (int) Context::getContext()->shop->id . ') '
                    . 'LEFT JOIN ' . _DB_PREFIX_ . 'store_lang sl '
                    . 'ON (sl.id_store = s.id_store AND sl.id_lang = '
                    . (int) Context::getContext()->language->id
                    . ') WHERE s.active=1 AND s.id_store IN ('
                    . pSQL(implode(',', $enabled_store))
                    . ') ORDER BY s.`id_store`'
            );
        }
        foreach ($available_stores as $key => $stores) {
            $available_stores[$key]['state'] = State::getNameById($stores['id_state']);
            $available_stores[$key]['country'] = Country::getNameById(Context::getContext()->language->id, $stores['id_country']);
            $available_stores[$key]['hours'] = Tools::jsonDecode($available_stores[$key]['hours'], true);
            $image = _PS_STORE_IMG_DIR_ . $stores['id_store'] . '.jpg';
            $image_url = ImageManager::thumbnail($image, 'store' . '_' . (int) $stores['id_store'] . '.jpg', 40, 'jpg', true, true);
            $available_stores[$key]['image'] = $image_url;
        }
//        Tools::dieObject($available_stores);

        return $available_stores;
    }

    /*
     * Load JS and CSS file
     */

    public function setKbMedia($frontend = 1)
    {
        $this->context->controller->addJS($this->_path . 'views/js/velovalidation.js');
        if ($frontend) {
            //datepicker
//            $iso_code = $this->context->language->iso_code;
//            d($iso_code);
            $this->context->controller->addJQuery();
            $this->context->controller->addCSS($this->_path . 'views/css/front/datetimepicker/bootstrap-datetimepicker.css');
            $this->context->controller->addCSS($this->_path . 'views/css/front/datetimepicker/bootstrap-datetimepicker.min.css');
//            $this->context->controller->addJS($this->_path . 'views/js/front/datetimepicker/locales/bootstrap-datetimepicker.en.js');
            $this->context->controller->addJS($this->_path . 'views/js/front/datetimepicker/bootstrap-datetimepicker.js');
            
            $this->context->controller->addCSS($this->_path . 'views/css/front/kb_front.css');
            $this->context->controller->addCSS($this->_path . 'views/css/front/jquery.mCustomScrollbar.css');
            $this->context->controller->addJS($this->_path . 'views/js/front/kb_front.js');
            $this->context->controller->addJS($this->_path . 'views/js/front/jquery.mCustomScrollbar.concat.min.js');
        } else {
            $this->context->controller->addCSS($this->_path . 'views/css/admin/kb_admin.css');
            $this->context->controller->addJS($this->_path . 'views/js/admin/admin.js');
            $this->context->controller->addJS($this->_path . 'views/js/admin/validation_admin.js');
        }
    }

    /*
     * Function to create Helper Form
     */

    public function getform($field_form, $languages, $title, $show_cancel_button, $field_value, $id, $action)
    {
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->fields_value = $field_value;
        $helper->name_controller = $this->name;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->default_form_language = $this->context->language->id;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->title = $title;
        if ($id == 'general') {
            $helper->show_toolbar = true;
        } else {
            $helper->show_toolbar = false;
        }
        $helper->table = $id;
        $helper->firstCall = true;
        $helper->toolbar_scroll = true;
        $helper->show_cancel_button = $show_cancel_button;
        $helper->submit_action = $action . 'submit' . $this->name;
        return $helper->generateForm(array('form' => $field_form));
    }

    /*
     * Function to get the URL upto module directory
     */

    private function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }

    /*
     * Function to get the URL of the store,
     * this function also checks if the store
     * is a secure store or not and returns the URL accordingly
     */

    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    //Function to copy the folder from source to destination
    public function copyfolder($source, $destination)
    {
        $directory = opendir($source);
        if (!Tools::file_exists_no_cache($destination)) {
            mkdir($destination);
        }
        while (($file = readdir($directory)) != false) {
            if (version_compare(_PS_VERSION_, '1.6.0.1', '<')) {
                copy($source . '/' . $file, $destination . '/' . $file);
            } else {
                Tools::copy($source . '/' . $file, $destination . '/' . $file);
            }
        }
        closedir($directory);
    }
    protected function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = Tools::strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
