CREATE TABLE IF NOT EXISTS `_PREFIX_kb_pickup_at_store_time` (
    `id_pickup` INT(11) unsigned NOT NULL AUTO_INCREMENT ,
    `id_shop` int(10) unsigned NOT NULL default '0',
    `id_cart` int(12) unsigned NOT NULL  DEFAULT '0',
    `id_customer` int(10) unsigned NOT NULL default '0',
    `id_store` int(10) unsigned NOT NULL,
    `id_address_delivery` int(10) unsigned NOT NULL default '0',
    `preferred_date` varchar(30) NULL default NULL,
    `preferred_time` varchar(30) NULL default NULL,
    `date_add` DATETIME NOT NULL ,
    `date_upd` DATETIME NULL ,
    PRIMARY KEY (`id_pickup`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `_PREFIX_kb_pickup_store_address_mapping` (
    `id_pickup_address` INT(11) unsigned NOT NULL AUTO_INCREMENT ,
    `id_shop` int(10) unsigned NOT NULL default '0',
    `id_store` int(10) unsigned NOT NULL,
    `id_address` int(10) unsigned NOT NULL,
    PRIMARY KEY (`id_pickup_address`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `_PREFIX_kb_pickup_at_store_email_template` (
    `id_template` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
    `id_lang` INT(11) UNSIGNED NOT NULL,
    `iso_code` varchar(11) NOT NULL,
    `subject` varchar(255) NOT NULL,
    `body` text NULL,
    `date_add` datetime NOT NULL,
    `date_upd` datetime NOT NULL,
    PRIMARY KEY (`id_template`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

