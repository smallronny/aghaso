<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{iqitwishlist}prestashop>actions_956d818758e07660a5885268e15c92a9'] = 'Producto eliminado de la lista de favoritos';
$_MODULE['<{iqitwishlist}prestashop>actions_6cba49b790757be59e258de6d28fa078'] = 'Producto añadido a la lista de favoritos';
$_MODULE['<{iqitwishlist}prestashop>iqitwishlist-account_641254d77e7a473aa5910574f3f9453c'] = 'Lista de favoritos';
$_MODULE['<{iqitwishlist}prestashop>iqitwishlist-account_e38004377b7618106370e94bef4b1dca'] = 'Comparte tu lista de  favoritos';
$_MODULE['<{iqitwishlist}prestashop>iqitwishlist-account_a588b1abc58b8b758c4b34b69b9e10bb'] = 'Copiado';
$_MODULE['<{iqitwishlist}prestashop>iqitwishlist-account_a612782072a3b8f151c5e2120ef20efd'] = 'Copiar al portapapeles';
$_MODULE['<{iqitwishlist}prestashop>iqitwishlist-account_9ae79c1fccd231ac7fbbf3235dbf6326'] = 'Mis Favoritos';
$_MODULE['<{iqitwishlist}prestashop>iqitwishlist-account_7dc1578cf6ff845d36dab7db19ef86c0'] = 'Su lista de favoritos está vacía';
$_MODULE['<{iqitwishlist}prestashop>iqitwishlist-account_42a36b27675834714767716f9c8d2cd7'] = 'Los clientes que compraron este producto también compraron:';
$_MODULE['<{iqitwishlist}prestashop>product-page_2d96bb66d8541a89620d3c158ceef42b'] = 'Agregar a favoritos';
$_MODULE['<{iqitwishlist}prestashop>display-modal_b945c0fcfb0fade1c1953c9061250fa2'] = 'Necesitas iniciar sesión o crear una cuenta';
$_MODULE['<{iqitwishlist}prestashop>display-modal_b0bc2c064f7ee5680201d7bf95fc5a6c'] = 'Guarda los productos en tu lista de deseos para comprarlos más tarde o compartirlos con tus amigos.';
$_MODULE['<{iqitwishlist}prestashop>display-modal_952998528c20798fbd22b49d505a29d5'] = '¿No tienes una cuenta? Cree una aquí.';
$_MODULE['<{iqitwishlist}prestashop>display-modal_6cba49b790757be59e258de6d28fa078'] = 'Producto añadido a la lista de favorito';
$_MODULE['<{iqitwishlist}prestashop>product-miniature_2d96bb66d8541a89620d3c158ceef42b'] = 'Agregar a favoritos';
$_MODULE['<{iqitwishlist}prestashop>display-header-buttons_641254d77e7a473aa5910574f3f9453c'] = 'Lista de favoritos';
$_MODULE['<{iqitwishlist}prestashop>display-header-buttons-mobile_641254d77e7a473aa5910574f3f9453c'] = 'Lista de favoritos';
$_MODULE['<{iqitwishlist}prestashop>display-nav_641254d77e7a473aa5910574f3f9453c'] = 'Lista de favoritos';
$_MODULE['<{iqitwishlist}prestashop>my-account_9ae79c1fccd231ac7fbbf3235dbf6326'] = 'Mis Favoritos';
