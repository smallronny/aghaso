{*

* 2017 IQIT-COMMERCE.COM

*

* NOTICE OF LICENSE

*

* This file is licenced under the Software License Agreement.

* With the purchase or the installation of the software in your application

* you accept the licence agreement

*

* @author    IQIT-COMMERCE.COM <support@iqit-commerce.com>

* @copyright 2017 IQIT-COMMERCE.COM

* @license   Commercial license (You can not resell or redistribute this software.)

*

*}



{block name='iqitcookielaw'}

<div id="iqitcookielaw" class="p-3">

    <div class="row justify-content-center p-4"> 

             <div class="col-md-8 col-12 align-self-center">

{$txt nofilter}

 </div>

          <div class="col-md-2 col-6 align-self-center text-center">



<button class="btn btn-block btn-white-trans-aghaso" id="iqitcookielaw-accept">{l s='Accept' mod='iqitcookielaw'}</button>

  </div>

          <div class="col-md-2 col-6 align-self-center text-center">

<a href="/content/11-politicas-y-privacidad" id="iqitcookielaw-read" class="btn-cook btn btn-block btn-link-read-cok">{l s='Ver más' mod='iqitcookielaw' }</a>

  </div>

           </div>

</div>

{/block}


