<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2017 Presta.Site
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_2_0($module)
{
    try {
        $module->installMetaPage();
        $module->installHooks();

        // add fields
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc_block`
             ADD `page` TINYINT(1) DEFAULT 0;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc_block_lang`
             ADD `link_rewrite` VARCHAR(255),
             ADD `meta_title` VARCHAR(255),
             ADD `meta_desc` VARCHAR(255),
             ADD `meta_keywords` VARCHAR(255),
             ADD `desc` TEXT;'
        );
    } catch (Exception $e) {
        // ignore
    }
    
    return true; // Return true if success.
}
