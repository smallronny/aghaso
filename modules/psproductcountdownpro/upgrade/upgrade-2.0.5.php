<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2017 Presta.Site
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_0_5($module)
{
    try {
        // get old data
        $timers = Db::getInstance()->executeS(
            'SELECT *
             FROM `'._DB_PREFIX_.'pspc`'
        );
        $blocks = Db::getInstance()->executeS(
            'SELECT *
             FROM `'._DB_PREFIX_.'pspc_block`'
        );

        // new tables
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_shop` (
                `id_pspc` INT(10) NOT NULL,
                `id_shop` INT(10) NOT NULL,
                `active` TINYINT(1) DEFAULT 1,
                UNIQUE (`id_pspc`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_block_shop` (
                `id_pspc_block` INT(10) NOT NULL,
                `id_shop` INT(10) NOT NULL,
                `active` TINYINT(1) DEFAULT 1,
                UNIQUE (`id_pspc_block`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );

        // drop obsolete columns
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc`
             DROP `active`,
             DROP `id_shop`;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc_block`
             DROP `active`,
             DROP `id_shop`;'
        );

        $index_name = Db::getInstance()->getValue(
            'SELECT DISTINCT INDEX_NAME
             FROM information_schema.STATISTICS 
             WHERE `TABLE_SCHEMA` = "'._DB_NAME_.'"
              AND `NON_UNIQUE` = 0 
              AND `TABLE_NAME` = "'._DB_PREFIX_.'pspc_relations";');
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc_relations`
             ADD `id_shop` INT(10),
             DROP INDEX `'.pSQL($index_name).'`,
			 ADD UNIQUE (`id_pspc`, `id_product`, `id_product_attribute`, `id_specific_price`, `id_shop`);'
        );

        // update old data
        foreach ($timers as $timer) {
            Db::getInstance()->execute(
                'INSERT INTO `'._DB_PREFIX_.'pspc_shop`
                 (`id_pspc`, `id_shop`, `active`)
                 VALUES
                 ('.(int)$timer['id_pspc'].', '.(int)$timer['id_shop'].', '.(int)$timer['active'].')'
            );
        }
        foreach ($blocks as $block) {
            Db::getInstance()->execute(
                'INSERT INTO `'._DB_PREFIX_.'pspc_block_shop`
                 (`id_pspc_block`, `id_shop`, `active`)
                 VALUES
                 ('.(int)$block['id_pspc_block'].', '.(int)$block['id_shop'].', '.(int)$block['active'].')'
            );
        }
    } catch (Exception $e) {
        // ignore
    }
    
    return true; // Return true if success.
}
