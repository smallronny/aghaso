<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2017 Presta.Site
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_1_6($module)
{
    try {
        // add fields
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc`
             ADD `action_start_qty` INT(6), 
             ADD `action_end_qty` INT(6);'
        );
    } catch (Exception $e) {
        // ignore
    }
    
    return true; // Return true if success.
}
