<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2017 Presta.Site
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_1_0($module)
{
    try {
        // new tables
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_group` (
                `id_pspc` INT(11) NOT NULL,
                `id_group` INT(11) NOT NULL,
                UNIQUE (`id_pspc`, `id_group`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );

        // add fields
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc`
             ADD `all_groups` TINYINT(1) DEFAULT 1;'
        );

        // add fields and alter indexes
        $index_name = Db::getInstance()->getValue(
            'SELECT DISTINCT INDEX_NAME
             FROM information_schema.STATISTICS 
             WHERE `TABLE_SCHEMA` = "'._DB_NAME_.'"
              AND `NON_UNIQUE` = 0 
              AND `TABLE_NAME` = "'._DB_PREFIX_.'pspc_relations";');
        Db::getInstance()->execute(
            'ALTER TABLE `' . _DB_PREFIX_ . 'pspc_relations`
             ADD `id_group` INT(11),
             DROP INDEX `'.pSQL($index_name).'`,
			 ADD UNIQUE (`id_pspc`, `id_product`, `id_product_attribute`, `id_specific_price`, `id_shop`, `id_group`);'
        );
    } catch (Exception $e) {
        // ignore
    }
    
    return true; // Return true if success.
}
