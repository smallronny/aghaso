<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2019 Presta.Site
 * @license   LICENSE.txt
 */

class PSPCUpgrade
{
    // Migrate module data from v1 to v2
    public static function migrateTo20($module)
    {
        if (!Configuration::get($module->settings_prefix.'UPDATED20') && self::checkTableExists('psproductcountdown')) {
            $product_timers =
                Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'psproductcountdown`');
            $category_timers = (self::checkTableExists('psproductcountdown_category')
                ? Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'psproductcountdown_category`')
                : array()
            );
            $manufacturer_timers = (self::checkTableExists('psproductcountdown_manufacturer')
                ? Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'psproductcountdown_manufacturer`')
                : array()
            );
            $timers = array_merge($product_timers, $category_timers, $manufacturer_timers);

            // relations between old timer ids and new ids
            $timer_update = array('product' => array(), 'category' => array(), 'manufacturer' => array());

            foreach ($timers as $timer) {
                $pspc = new PSPC();

                // common fields
                $pspc->from = $timer['from'];
                $pspc->to = $timer['to'];
                // if free version convert dates to utc
                if (!self::checkTableExists('psproductcountdown_category')) {
                    $tz = Configuration::get('PS_TIMEZONE');
                    $dt_to = new DateTime($timer['to'], new DateTimeZone($tz));
                    $dt_to->setTimezone(new DateTimeZone('UTC'));
                    $dt_from = new DateTime($timer['from'], new DateTimeZone($tz));
                    $dt_from->setTimezone(new DateTimeZone('UTC'));
                    $pspc->from = $dt_from->format('Y-m-d H:i:s');
                    $pspc->to = $dt_to->format('Y-m-d H:i:s');
                }
                $pspc->active = $timer['active'];
                $pspc->personal = (isset($timer['personal']) ? $timer['personal'] : 0);
                $pspc->personal_hours = (isset($timer['personal_hours']) ? $timer['personal_hours'] : 0);
                $pspc->hours_restart =
                    (isset($timer['personal_hours_restart']) && $pspc->personal ? $timer['personal_hours_restart'] : 0);
                $pspc->action_start = (isset($timer['action_start']) ? $timer['action_start'] : '');
                $pspc->action_start_done = (isset($timer['action_start_done']) ? $timer['action_start_done'] : 0);
                $pspc->action_end = (isset($timer['action_end']) ? $timer['action_end'] : '');
                $pspc->action_end_done = (isset($timer['action_end_done']) ? $timer['action_end_done'] : 0);

                // type-specific fields
                if (isset($timer['id_category_countdown'])) {
                    $type = 'category';
                    if (!$timer['id_category_countdown']) {
                        continue;
                    }
                    $id_countdown = $timer['id_category_countdown'];
                    $id_object = $timer['id_category'];
                    $lang_table = 'psproductcountdown_category_lang';
                    $id_key = 'id_category_countdown';
                } elseif (isset($timer['id_manufacturer_countdown'])) {
                    $type = 'manufacturer';
                    if (!$timer['id_manufacturer_countdown']) {
                        continue;
                    }
                    $id_countdown = $timer['id_manufacturer_countdown'];
                    $id_object = $timer['id_manufacturer'];
                    $lang_table = 'psproductcountdown_manufacturer_lang';
                    $id_key = 'id_manufacturer_countdown';
                } else {
                    $type = 'product';
                    if (isset($timer['id_countdown']) && $timer['id_countdown']) {
                        $id_countdown = $timer['id_countdown'];
                        $id_object = $timer['id_product'];
                        if (isset($timer['id_product_attribute']) && $timer['id_product_attribute']) {
                            $id_object .= '-'.$timer['id_product_attribute'];
                        }
                        $lang_table = 'psproductcountdown_lang';
                        $id_key = 'id_countdown';
                    } else {
                        continue;
                    }
                }

                // reduction
                if (self::checkTableExists('psproductcountdown_relations')) {
                    try {
                        $reduction_data = Db::getInstance()->getRow(
                            'SELECT * FROM `' . _DB_PREFIX_ . 'psproductcountdown_relations`
                             WHERE `id_countdown` = ' . (int)$id_countdown .' AND `type` = "'.pSQL($type).'"'
                        );
                        if (is_array($reduction_data) && count($reduction_data)) {
                            $pspc->reduction =
                                (isset($reduction_data['reduction']) ? $reduction_data['reduction']*100 : 0);
                            $pspc->reduction_type = (isset($reduction_data['reduction_type'])
                                    ? $reduction_data['reduction_type'] : 'percentage');
                            $pspc->reduction_tax =
                                (isset($reduction_data['reduction_tax']) ? $reduction_data['reduction_tax'] : 0);
                        }
                    } catch (Exception $e) {
                        // ignore, update from the free version
                    }
                }

                // name
                if (self::checkTableExists($lang_table)) {
                    try {
                        $name_data = Db::getInstance()->executeS(
                            'SELECT * FROM `' . _DB_PREFIX_ . pSQL($lang_table) . '`
                             WHERE `' . pSQL($id_key) . '` = ' . (int)$id_countdown
                        );
                        foreach ($name_data as $name) {
                            $pspc->name[$name['id_lang']] = $name['name'];
                        }
                    } catch (Exception $e) {
                        // ignore, update from the free version
                    }
                }

                $pspc->all_groups = true;

                // validate and save
                $errors = $pspc->validateAllFields();
                if (!(is_array($errors) && count($errors))) {
                    $pspc->save();

                    // objects
                    $pspc->setObjects(array($id_object), $type);

                    $timer_update[$type][$id_countdown] = $pspc->id;
                    // relations / specific prices
                    if (self::checkTableExists('psproductcountdown_relations')) {
                        try {
                            $reduction_data = Db::getInstance()->executeS(
                                'SELECT r.*, sp.`id_product_attribute`
                                 FROM `' . _DB_PREFIX_ . 'psproductcountdown_relations` r
                                 LEFT JOIN `' . _DB_PREFIX_ . 'specific_price` sp
                                  ON r.`id_specific_price` = sp.`id_specific_price`
                                 WHERE r.`id_countdown` = ' . (int)$id_countdown
                            );
                            foreach ($reduction_data as $row) {
                                Db::getInstance()->execute(
                                    'INSERT IGNORE
                                     INTO `' . _DB_PREFIX_ . 'pspc_relations` 
                                     (`id_pspc`, `id_product`, `id_product_attribute`, `id_specific_price`) 
                                     VALUES
                                     (' . (int)$pspc->id . ', ' . (int)$row['id_product'] . ',
                                      ' . (int)$row['id_product_attribute'] . ', ' . (int)$row['id_specific_price'].') '
                                );
                            }
                        } catch (Exception $e) {
                            // ignore, update from the free version
                        }
                    }
                }
            }

            if (self::checkTableExists('psproductcountdown_block')) {
                $blocks = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'psproductcountdown_block`');
                foreach ($blocks as $block) {
                    $pspc_block = new PSPCBlock();
                    $pspc_block->hook = (isset($block['hook']) ? $block['hook'] : '');
                    $pspc_block->product_count = (isset($block['product_count']) ? $block['product_count'] : 0);
                    $pspc_block->active = (isset($block['active']) ? $block['active'] : 0);
                    $pspc_block->sort = (isset($block['sort']) ? $block['sort'] : '');
                    $pspc_block->source_all = (isset($block['source_all']) ? $block['source_all'] : 0);
                    $pspc_block->source_specific_prices =
                        (isset($block['source_specific_prices']) ? $block['source_specific_prices'] : 0);
                    $pspc_block->source_pspc = ($block['source_product_timers']
                        && $block['source_category_timers'] && $block['source_manufacturer_timers']);

                    $errors = $pspc_block->validateAllFields();
                    if (!(is_array($errors) && count($errors))) {
                        $pspc_block->save();

                        try {
                            $sources = Db::getInstance()->executeS(
                                'SELECT *
                                 FROM `'._DB_PREFIX_.'psproductcountdown_block_source`
                                 WHERE `id_psproductcountdown_block` = '.(int)$block['id_psproductcountdown_block']
                            );
                            foreach ($sources as $source) {
                                if (isset($timer_update[$source['type']][$source['id_source_object']])) {
                                    $id_pspc = $timer_update[$source['type']][$source['id_source_object']];
                                    $pspc_block->addSource($id_pspc);
                                }
                            }
                        } catch (Exception $e) {
                            // ignore
                        }

                        try {
                            $name_data = Db::getInstance()->executeS(
                                'SELECT * FROM `'._DB_PREFIX_.'psproductcountdown_block_lang`
                                 WHERE `id_psproductcountdown_block` = '.(int)$block['id_psproductcountdown_block']
                            );
                            foreach ($name_data as $name) {
                                $pspc_block->title[$name['id_lang']] = $name['title'];
                            }
                        } catch (Exception $e) {
                            // ignore
                        }
                    }
                }
            }
        }

        $theme = Configuration::get($module->settings_prefix.'THEME');
        if (!file_exists(_PS_MODULE_DIR_ . $module->name . '/views/css/themes/'.$theme)) {
            Configuration::updateValue($module->settings_prefix.'THEME', '1-simple.css');
        }
        // reset font size
        Configuration::updateValue($module->settings_prefix.'BASE_FONT_SIZE', '1');
        Configuration::updateValue($module->settings_prefix.'BASE_FONT_SIZE_LIST', '1');
        Configuration::updateValue($module->settings_prefix.'BASE_FONT_SIZE_COL', '0.8');


        Configuration::updateValue($module->settings_prefix.'UPDATED20', 1);
    }

    public static function checkTableExists($table)
    {
        $result = Db::getInstance()->executeS('SHOW TABLES LIKE "'._DB_PREFIX_.pSQL($table).'"');

        if (is_array($result) && count($result)) {
            return true;
        }

        return false;
    }
}
