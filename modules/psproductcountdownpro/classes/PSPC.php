<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2017 Presta.Site
 * @license   LICENSE.txt
 */

class PSPC extends ObjectModel
{
    public $from;
    public $to;
    public $active;
    public $system_name;
    public $name;
    public $reduction;
    public $reduction_type;
    public $reduction_tax;
    public $personal;
    public $personal_hours;
    public $hours_restart;
    public $action_start;
    public $action_start_done;
    public $action_start_qty;
    public $action_end;
    public $action_end_done;
    public $action_end_qty;
    public $all_groups;

    public $from_tz;
    public $to_tz;
    public $to_time;
    public $to_date;

    public static $definition = array(
        'table' => 'pspc',
        'primary' => 'id_pspc',
        'multilang' => true,
        'fields' => array(
            // Classic fields
            'from' => array('type' => self::TYPE_DATE, 'validate' => 'isPhpDateFormat'),
            'to' => array('type' => self::TYPE_DATE, 'validate' => 'isPhpDateFormat'),
            'system_name' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
            'reduction' => array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'reduction_tax' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),
            'reduction_type' => array('type' => self::TYPE_STRING, 'validate' => 'isReductionType'),
            'personal' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'personal_hours' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'hours_restart' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'action_start' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'action_start_done' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'action_start_qty' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'action_end' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'action_end_done' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'action_end_qty' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'all_groups' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            // Lang fields
            'name' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);

        $this->active = $this->isActive();

        $this->loadTz();
    }

    public function isActive()
    {
        if (!$this->id) {
            return true;
        }

        $context = Context::getContext();
        $is_active = Db::getInstance()->getValue(
            'SELECT `active`
             FROM `'._DB_PREFIX_.'pspc_shop`
             WHERE `id_pspc` = '.(int)$this->id.'
              AND `id_shop` = '.(int)$context->shop->id
        );

        return $is_active;
    }

    public function loadTz()
    {
        $this->from_tz = ($this->from ? date('Y-m-d\TH:i:s\Z', strtotime($this->from)) : '');
        $this->to_tz = ($this->to ? date('Y-m-d\TH:i:s\Z', strtotime($this->to)) : '');
        $this->to_time = ($this->to ? strtotime($this->to.' UTC') * 1000 : 0);
        $this->to_date = ($this->to ? date('d/m/Y', strtotime($this->to.' UTC')) : '');
    }

    public function validateAllFields()
    {
        $errors = array();

        $valid = $this->validateFields(false, true);
        if ($valid !== true) {
            $errors[] = $valid . "\n";
        }
        $valid_lang = $this->validateFieldsLang(false, true);
        if ($valid_lang !== true) {
            $errors[] = $valid_lang . "\n";
        }

        if (!$this->personal && !$this->to) {
            $module = Module::getInstanceByName('psproductcountdownpro');
            $errors[] = $module->l('The "to" field is required.');
        }

        return $errors;
    }

    public function validateField($field, $value, $id_lang = null, $skip = array(), $human_errors = true)
    {
        return parent::validateField($field, $value, $id_lang, $skip, $human_errors);
    }

    public static function displayFieldName($field, $class = __CLASS__, $htmlentities = true, Context $context = null)
    {
        return '"'.parent::displayFieldName($field, $class, $htmlentities, $context).'"';
    }

    public function save($null_values = false, $auto_date = true, $update_specific_prices = true)
    {
        // Convert dates from TZ to normal format if necessary
        $dt_from = new DateTime($this->from, new DateTimeZone('UTC'));
        $dt_to = new DateTime($this->to, new DateTimeZone('UTC'));
        $this->from = $dt_from->format('Y-m-d H:i:s');
        $this->to = $dt_to->format('Y-m-d H:i:s');

        // Saving
        $saved = parent::save($null_values, $auto_date);

        if ($saved && $update_specific_prices) {
            // Save specific prices
            $this->saveSpecificPrices();
        }

        if ($saved) {
            // save shop data
            foreach (Shop::getContextListShopID() as $id_shop) {
                Db::getInstance()->execute(
                    'INSERT INTO `'._DB_PREFIX_.'pspc_shop`
                     (`id_pspc`, `id_shop`, `active`)
                     VALUES
                     ('.(int)$this->id.', '.(int)$id_shop.', '.(int)$this->active.')
                     ON DUPLICATE KEY UPDATE
                      `active` = '.(int)$this->active
                );
            }
        }

        $module = Module::getInstanceByName('psproductcountdownpro');
        $module->clearSmartyCache();

        return $saved;
    }

    public function saveSpecificPrices()
    {
        $products = $this->getRelatedProducts();

        // return if reduction == 0 and there is no previous specific prices to delete
        if ($this->reduction == 0 && !$this->getIdSpecificPriceRelated()) {
            return false;
        }

        if ($this->all_groups) {
            $groups = array(0);
        } else {
            $groups = $this->getGroups();
        }

        // Search specific prices not belonging to current groups
        $ids_specific_prices = Db::getInstance()->executeS(
            'SELECT `id_specific_price`
             FROM `'._DB_PREFIX_.'pspc_relations`
             WHERE `id_pspc` = ' . (int)$this->id.'
              '.($this->all_groups
                ? 'AND `id_group` != 0'
                : 'AND (`id_group` = 0 OR `id_group` IS NULL'.
                    (count($groups) ? 'OR `id_group` NOT IN ('.implode(',', array_map('intval', $groups)).')' : '').')')
        );
        // delete unnecessary specific prices and their relations
        foreach ($ids_specific_prices as $sp) {
            $id_specific_price = $sp['id_specific_price'];
            $sp = new SpecificPrice($id_specific_price);
            $sp->delete();
            Db::getInstance()->execute(
                'DELETE FROM `' . _DB_PREFIX_ . 'pspc_relations`
                 WHERE `id_specific_price` = ' . (int)$id_specific_price
            );
        }

        foreach ($groups as $id_group) {
            foreach ($products as $product) {
                $id_product = $product['id_product'];
                $id_product_attribute =
                    (isset($product['id_product_attribute']) ? $product['id_product_attribute'] : 0);

                foreach ($this->getShops() as $id_shop) {
                    $id_specific_price =
                        $this->getIdSpecificPriceRelated($id_product, $id_product_attribute, $id_shop, $id_group);
                    $specific_price = new SpecificPrice($id_specific_price);
                    if ($this->reduction == 0) {
                        if ($id_specific_price) {
                            $specific_price->delete();
                            Db::getInstance()->execute(
                                'DELETE FROM `' . _DB_PREFIX_ . 'pspc_relations`
                                 WHERE `id_specific_price` = ' . (int)$id_specific_price
                            );
                        }
                        continue;
                    }
                    $specific_price->id_cart = 0;
                    $specific_price->id_product = $product['id_product'];
                    $specific_price->id_product_attribute = $id_product_attribute;
                    $specific_price->id_currency = 0;
                    $specific_price->id_country = 0;
                    $specific_price->id_group = $id_group;
                    $specific_price->id_customer = 0;
                    $specific_price->price = -1;
                    $specific_price->from_quantity = 1;
                    $specific_price->reduction =
                        (float)($this->reduction_type == 'percentage' ? $this->reduction / 100 : $this->reduction);
                    $specific_price->reduction_tax = $this->reduction_tax;
                    $specific_price->reduction_type = $this->reduction_type;
                    $specific_price->id_shop = $id_shop;
                    // from to
                    if (!$this->personal) {
                        $dt_from = new DateTime($this->from, new DateTimeZone('UTC'));
                        $from = $dt_from->setTimezone(new DateTimeZone(Configuration::get('PS_TIMEZONE')))->format('Y-m-d H:i:s');
                        $dt_to = new DateTime($this->to, new DateTimeZone('UTC'));
                        $dt_to->modify('-1 second');
                        $to = $dt_to->setTimezone(new DateTimeZone(Configuration::get('PS_TIMEZONE')))->format('Y-m-d H:i:s');
                        $specific_price->from = $from;
                        $specific_price->to = $to;
                    } else {
                        $specific_price->from = '0000-00-00 00:00:00';
                        $specific_price->to = '0000-00-00 00:00:00';
                    }

                    if ($specific_price->validateFields(false)) {
                        $specific_price->save();
                        Db::getInstance()->execute(
                            'INSERT IGNORE
                             INTO `'._DB_PREFIX_.'pspc_relations` 
                             (`id_pspc`, `id_product`, `id_product_attribute`,
                              `id_specific_price`, `id_shop`, `id_group`) 
                             VALUES
                             ('.(int)$this->id.', '.(int)$specific_price->id_product.',
                              '.(int)$specific_price->id_product_attribute.', '.(int)$specific_price->id.',
                              '.(int)$id_shop.', '.(int)$id_group.') '
                        );
                    }
                }
            }
        }
    }

    public function getRelatedProducts()
    {
        $products = array();

        // By products
        $countdown_products = $this->getObjects('product');
        foreach ($countdown_products as $c_product) {
            $id = $c_product['id_object'];
            $ipa = $c_product['id_product_attribute'];
            $products[$id.'-'.$ipa] = array('id_product' => $id, 'id_product_attribute' => $ipa);
        }

        // By categories
        $countdown_categories = $this->getObjects('category', true);
        foreach ($countdown_categories as $id_category) {
            foreach (self::getProductsByIdCategory($id_category) as $product) {
                $products[$product['id_product'].'-0'] = array('id_product' => $product['id_product']);
            }
        }

        // By manufacturers
        $countdown_manufacturers = $this->getObjects('manufacturer', true);
        foreach ($countdown_manufacturers as $id_manufacturer) {
            foreach (self::getProductsByIdManufacturer($id_manufacturer) as $product) {
                $products[$product['id_product'].'-0'] = array('id_product' => $product['id_product']);
            }
        }

        return $products;
    }

    public static function getRelatedProductsIDs($id_pspc)
    {
        $results = array();

        // from products
        $products = self::getObjectsStatic($id_pspc, 'product', true);
        $products = self::filterInactiveProducts($products);
        $results = array_merge($results, $products);
        $results = array_unique($results);

        // from categories
        $categories = self::getObjectsStatic($id_pspc, 'category', true);
        foreach ($categories as $id_category) {
            $products = self::getProductsByIdCategory($id_category, null, null, true, true);
            $results = array_merge($results, $products);
            $results = array_unique($results);
        }

        // from manufacturers
        $manufacturers = self::getObjectsStatic($id_pspc, 'manufacturer', true);
        foreach ($manufacturers as $id_manufacturer) {
            $products = self::getProductsByIdManufacturer($id_manufacturer, null, null, true, true);
            $results = array_merge($results, $products);
            $results = array_unique($results);
        }

        return $results;
    }

    protected function getIdSpecificPriceRelated($id_product = null, $id_product_attribute = null, $id_shop = null, $id_group = null)
    {
        $id_specific_price_rel = Db::getInstance()->getValue(
            'SELECT `id_specific_price`
             FROM `'._DB_PREFIX_.'pspc_relations`
             WHERE `id_pspc` = '.(int)$this->id
            .($id_product ? ' AND `id_product` = '.(int)$id_product : '')
            .($id_product_attribute ? ' AND `id_product_attribute` = '.(int)$id_product_attribute : '')
            .($id_shop ? ' AND `id_shop` = '.(int)$id_shop : '')
            .($id_group ? ' AND `id_group` = '.(int)$id_group : '')
        );

        return $id_specific_price_rel;
    }

    public function delete()
    {
        $result = parent::delete();

        if ($result) {
            // delete related specific prices
            $start = 0;
            $limit = 50;
            while ($specific_prices = $this->getSpecificPrices($start, $limit)) {
                foreach ($specific_prices as $specific_price) {
                    $sp = new SpecificPrice($specific_price['id_specific_price']);
                    $sp->delete();
                }
                $start += $limit;
            }
            Db::getInstance()->execute(
                'DELETE
                 FROM `' . _DB_PREFIX_ . 'pspc_relations`
                 WHERE `id_pspc` = ' . (int)$this->id
            );

            Db::getInstance()->execute(
                'DELETE
                 FROM `' . _DB_PREFIX_ . 'pspc_shop`
                 WHERE `id_pspc` = ' . (int)$this->id
            );
        }

        return $result;
    }

    protected function getSpecificPrices($start, $limit)
    {
        $result = Db::getInstance()->executeS(
            'SELECT `id_specific_price`
             FROM `'._DB_PREFIX_.'pspc_relations`
             WHERE `id_pspc` = '.(int)$this->id.'
             LIMIT '.(int)$start.', '.(int)$limit
        );

        return $result;
    }

    public static function findPSPC($type, $id_object, $id_product_attribute = 0, $pspc_module = null, $skip_expired = true, $all_langs = false)
    {
        $pspc_module = ($pspc_module ? $pspc_module : Module::getInstanceByName('psproductcountdownpro'));
        $context = Context::getContext();

        if (is_array($id_object)) {
            $id_object_str = implode(',', array_map('intval', $id_object));
        } else {
            $id_object_str = (int)$id_object;
        }

        $id_pspc = Db::getInstance()->getValue(
            'SELECT pspco.`id_pspc`
             FROM `' . _DB_PREFIX_ . 'pspc_object` pspco
             LEFT JOIN `'._DB_PREFIX_.'pspc` pspc USING (`id_pspc`)
             LEFT JOIN `'._DB_PREFIX_.'pspc_shop` pspcs USING (`id_pspc`)
             LEFT JOIN `'._DB_PREFIX_.'pspc_group` pspcg USING (`id_pspc`)
             WHERE pspcs.`id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')
              AND pspco.`type` = "'.pSQL($type).'"
              AND pspco.`id_object` IN (' . pSQL($id_object_str) . ')
              AND pspco.`id_product_attribute` IN (0, ' . (int)$id_product_attribute.')
              AND (
               pspc.`all_groups` = 1
               ' . (is_object($context->customer)
                ? 'OR pspcg.`id_group` IN (' . implode(',', array_map('intval', $context->customer->getGroups())) . ')'
                : '') . '
              )
             ORDER BY pspco.`id_product_attribute` DESC, pspc.`to` ASC, pspco.`id_object` DESC'
        );

        if ($id_pspc) {
            $id_lang = ($all_langs ? null : $context->language->id);
            $pspc = new PSPC($id_pspc, $id_lang);

            $datetime_current = new DateTime('now', new DateTimeZone('UTC'));
            $datetime_from = new DateTime($pspc->from, new DateTimeZone('UTC'));
            $datetime_to = new DateTime($pspc->to, new DateTimeZone('UTC'));

            // Return false if countdown is expired or not started yet
            if ($skip_expired) {
                if ($datetime_from > $datetime_current ||
                    (($datetime_to < $datetime_current) && ($pspc_module->hide_expired || $pspc_module->hide_after_end))
                ) {
                    if (!($pspc->personal && $pspc->personal_hours)) {
                        return false;
                    }
                }
            }

            if (Validate::isLoadedObject($pspc)) {
                return $pspc;
            }
        }

        return null;
    }

    public function setObjects($objects, $type)
    {
        // delete old values
        Db::getInstance()->delete('pspc_object', '`id_pspc` = ' . (int)$this->id.' AND `type` = "'.pSQL($type).'"');

        // insert new values
        if (is_array($objects)) {
            foreach ($objects as $id_object) {
                $id_product_attribute = 0;
                if (strpos($id_object, '-') !== false) {
                    $ids = explode('-', $id_object);
                    $id_object = $ids[0];
                    $id_product_attribute = $ids[1];
                }

                Db::getInstance()->insert('pspc_object', array(
                    'id_object' => (int)$id_object,
                    'id_product_attribute' => (int)$id_product_attribute,
                    'id_pspc' => (int)$this->id,
                    'type' => $type,
                ));
            }
        }
    }

    public function getObjects($type, $ids_only = false)
    {
        if (!$this->id) {
            return array();
        }

        $objects = self::getObjectsStatic($this->id, $type, $ids_only);

        return $objects;
    }

    public static function getObjectsStatic($id_pspc, $type, $ids_only = false)
    {
        $objects = Db::getInstance()->executeS(
            'SELECT *
             FROM `'._DB_PREFIX_.'pspc_object`
             WHERE `id_pspc` = '.(int)$id_pspc.'
              AND `type` = "'.pSQL($type).'"'
        );

        if ($ids_only) {
            $tmp = array();
            foreach ($objects as $object) {
                $tmp[] = $object['id_object'];
            }
            $objects = $tmp;
        }

        return $objects;
    }

    public static function getObjectsCount($id_pspc, $type = '')
    {
        return Db::getInstance()->getValue(
            'SELECT COUNT(`id_pspc_object`)
             FROM `'._DB_PREFIX_.'pspc_object`
             WHERE `id_pspc` = '.(int)$id_pspc.'
              '.($type ? ' AND `type` = "'.pSQL($type).'" ' : '')
        );
    }

    public static function getProductsByIdCategory($id_category, $start = null, $limit = null, $only_visible = false, $ids_only = false)
    {
        $query = 'SELECT cp.`id_product` FROM `'._DB_PREFIX_.'category_product` cp ';
        if ($only_visible) {
            $query .=
                ' LEFT JOIN `'._DB_PREFIX_.'product_shop` ps 
                   ON cp.`id_product` = ps.`id_product` 
                    AND ps.`id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')';
        }
        $query .=
            ' WHERE cp.`id_category` = '.(int)$id_category.' '
            .($only_visible ? ' AND ps.`active` = 1 AND ps.`visibility` != "none" ' : '')
            .' ORDER BY cp.`id_product` ASC '
            .($start === null && $limit === null ? '' : ' LIMIT '.(int)$start.', '.(int)$limit);

        $results = Db::getInstance()->executeS($query);

        if ($ids_only) {
            $tmp = array();
            foreach ($results as $row) {
                $tmp[] = $row['id_product'];
            }
            $results = $tmp;
        }

        return $results;
    }

    public static function getProductsByIdManufacturer($id_manufacturer, $start = null, $limit = null, $only_visible = false, $ids_only = false)
    {
        $query = 'SELECT p.`id_product` FROM `'._DB_PREFIX_.'product` p ';

        if ($only_visible) {
            $query .=
                ' LEFT JOIN `'._DB_PREFIX_.'product_shop` ps 
                   ON p.`id_product` = ps.`id_product`
                    AND ps.`id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')';
        }

        $query .=
            ' WHERE p.`id_manufacturer` = '.(int)$id_manufacturer.' '
            .($only_visible ? ' AND ps.`active` = 1 AND ps.`visibility` != "none" ' : '')
            .' ORDER BY p.`id_product` ASC '
            .($start === null && $limit === null ? '' : ' LIMIT '.(int)$start.', '.(int)$limit);

        $results = Db::getInstance()->executeS($query);

        if ($ids_only) {
            $tmp = array();
            foreach ($results as $row) {
                $tmp[] = $row['id_product'];
            }
            $results = $tmp;
        }

        return $results;
    }

    public static function checkCountdownAppliedToProduct($id_pspc, $id_product)
    {
        // 1. If the countdown is applied directly to the product
        $as_product = Db::getInstance()->getValue(
            'SELECT `id_pspc`
             FROM `'._DB_PREFIX_.'pspc_object`
             WHERE `type` = "product"
              AND `id_pspc` = '.(int)$id_pspc.'
              AND `id_object` = '.(int)$id_product
        );
        if ($as_product) {
            return true;
        }

        // 2. If the countdown is applied to the product category
        $categories = self::getObjectsStatic($id_pspc, 'category', true);
        if (is_array($categories) && count($categories)) {
            $as_category = Db::getInstance()->getValue(
                'SELECT `id_product`
                 FROM `' . _DB_PREFIX_ . 'category_product`
                 WHERE `id_category` IN(' . implode(',', array_map('intval', $categories)) . ')
                  AND `id_product` = ' . (int)$id_product
            );
            if ($as_category) {
                return true;
            }
        }

        // 3. If the countdown is applied to the product manufacturer
        $manufacturers = self::getObjectsStatic($id_pspc, 'manufacturer', true);
        if (is_array($manufacturers) && count($manufacturers)) {
            $as_manufacturer = Db::getInstance()->getValue(
                'SELECT `id_product`
                 FROM `' . _DB_PREFIX_ . 'product`
                 WHERE `id_manufacturer` IN(' . implode(',', array_map('intval', $manufacturers)) . ')
                  AND `id_product` = ' . (int)$id_product
            );
            if ($as_manufacturer) {
                return true;
            }
        }

        return false;
    }

    public function restart()
    {
        $dt_current = new DateTime('now', new DateTimeZone('UTC'));
        $dt_from = new DateTime($this->from, new DateTimeZone('UTC'));
        $dt_to = new DateTime($this->to, new DateTimeZone('UTC'));
        $dt_to_tmp = new DateTime($this->to, new DateTimeZone('UTC'));
        $minutes_diff = (int)round(abs($dt_to->getTimestamp() - $dt_from->getTimestamp()) / 60);
        $minutes_restart = (int)round($this->hours_restart * 60 * 1);

        // if expired and should be restarted
        if ($dt_current > $dt_to && $this->hours_restart > 0 && !$this->personal) {
            // add X hours to the old end of the countdown:
            $dt_from_new = $dt_to_tmp->modify("+$minutes_restart minute");
            $dt_from_new_tmp = clone $dt_from_new;
            // if the countdown with new start time should be already started:
            if ($dt_from_new <= $dt_current) {
                $dt_to_new = $dt_from_new_tmp->modify("+$minutes_diff minute");
                $i = 0;
                // if even after restart the timer is already expired, max 1000 cycles
                while ($dt_to_new < $dt_current && $i < 1000) {
                    $dt_from_new = $dt_from_new->modify("+$minutes_restart minute");
                    $dt_from_new_tmp = clone $dt_from_new;
                    $dt_to_new = $dt_from_new_tmp->modify("+$minutes_diff minute");
                    $i++;
                }
                $this->from = $dt_from_new->format('Y-m-d H:i:s');
                $this->to = $dt_to_new->format('Y-m-d H:i:s');
                return $this->save();
            }
        }
    }

    public static function restartStatic($id_pspc)
    {
        $pspc = new PSPC($id_pspc);
        return $pspc->restart();
    }

    public static function getAll($active = true, $personal = false)
    {
        $timers = self::getCollection('PSPC');

        $pspc_list = Db::getInstance()->executeS(
            'SELECT `id_pspc`
             FROM `'._DB_PREFIX_.'pspc_shop` 
             WHERE `id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')'
        );
        $timers->where('id_pspc', 'IN', self::sqlArrayToList($pspc_list, 'id_pspc'));

        if ($active) {
            $pspc_list = Db::getInstance()->executeS(
                'SELECT `id_pspc`
                 FROM `'._DB_PREFIX_.'pspc_shop` 
                 WHERE `active` = 1
                  AND `id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')'
            );
            $timers->where('id_pspc', 'IN', self::sqlArrayToList($pspc_list, 'id_pspc'));
        }
        if ($personal) {
            $timers->where('personal', '=', 1);
        }

        return $timers;
    }

    public static function getBlockTimers($id_block)
    {
        $result = array();

        if ($id_block) {
            $timers = Db::getInstance()->executeS(
                'SELECT `id_pspc`
                 FROM `'._DB_PREFIX_.'pspc_block_source`
                 WHERE `id_pspc_block` = '.(int)$id_block
            );

            foreach ($timers as $timer) {
                $result[] = $timer['id_pspc'];
            }
        }

        return $result;
    }

    public static function getCollection($class)
    {
        $context = Context::getContext();
        if (class_exists('PrestaShopCollection')) {
            $collection = new PrestaShopCollection($class, $context->language->id);
        } else {
            $collection = new Collection($class, $context->language->id);
        }

        return $collection;
    }

    public function toggleSpecificPrices()
    {
        $specific_prices = Db::getInstance()->executeS(
            'SELECT `id_specific_price`, `id_shop`
             FROM `'._DB_PREFIX_.'pspc_relations`
             WHERE `id_pspc` = '.(int)$this->id
        );

        foreach ($specific_prices as $specific_price) {
            Db::getInstance()->execute(
                'UPDATE `' . _DB_PREFIX_ . 'specific_price`
                 SET `id_shop` = ' . ($this->active ? (int)$specific_price['id_shop'] : '99990') . '
                 WHERE `id_specific_price` = '.(int)$specific_price['id_specific_price']
            );
        }
    }

    /**
     * @param bool $active_only
     * @return array list of shop ids
     */
    public function getShops($active_only = true)
    {
        $result = array();

        $shops = Db::getInstance()->executeS(
            'SELECT `id_shop`
             FROM `'._DB_PREFIX_.'pspc_shop`
             WHERE `id_pspc` = '.(int)$this->id.
             ($active_only ? ' AND `active` = 1 ' : '')
        );

        foreach ($shops as $shop) {
            $result[] = $shop['id_shop'];
        }

        return $result;
    }

    public static function getShopsStatic($id_pspc, $active_only = true)
    {
        $pspc = new PSPC($id_pspc);

        return $pspc->getShops($active_only);
    }

    public static function sqlArrayToList($array, $column)
    {
        $result = array();

        foreach ($array as $item) {
            $result[] = $item[$column];
        }

        if (!count($result)) {
            $result = array(0);
        }

        return array_unique($result);
    }

    public function checkGroup($id_group)
    {
        if (!$this->id) {
            $last_groups = Configuration::get('PSPC_LAST_GROUPS');
            if ($last_groups) {
                $last_groups = explode(',', $last_groups);
            }
            if (is_array($last_groups) && in_array($id_group, $last_groups)) {
                return true;
            }

            return false;
        }

        return (bool)Db::getInstance()->getValue(
            'SELECT `id_pspc`
             FROM `'._DB_PREFIX_.'pspc_group`
             WHERE `id_pspc` = '.(int)$this->id.'
              AND `id_group` = '.(int)$id_group
        );
    }
    
    public function setGroups($groups)
    {
        if (!$this->id) {
            return false;
        }

        // clear groups for this countdown
        Db::getInstance()->execute(
            'DELETE FROM `'._DB_PREFIX_.'pspc_group`
             WHERE `id_pspc` = '.(int)$this->id
        );

        $last_groups = '';
        if (is_array($groups) && count($groups) && !$this->all_groups) {
            $last_groups = implode(',', $groups);
        }
        Configuration::updateValue('PSPC_LAST_GROUPS', $last_groups);

        // skip adding groups if all_groups
        if ($this->all_groups) {
            return true;
        }

        if (is_array($groups) && count($groups)) {
            foreach ($groups as $id_group) {
                Db::getInstance()->execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'pspc_group`
                     (`id_pspc`, `id_group`)
                     VALUES
                     ('.(int)$this->id.', '.(int)$id_group.')'
                );
            }
        }
    }

    public function getGroups()
    {
        $groups_raw = Db::getInstance()->executeS(
            'SELECT `id_group`
             FROM `'._DB_PREFIX_.'pspc_group`
             WHERE `id_pspc` = '.(int)$this->id
        );

        $groups = array();
        foreach ($groups_raw as $group_raw) {
            $groups[] = $group_raw['id_group'];
        }

        return $groups;
    }

    public static function filterInactiveProducts($ids, $id_shop = null)
    {
        if (!$ids) {
            return $ids;
        }

        $result = array();
        $context = Context::getContext();
        $id_shop = ($id_shop ? $id_shop : $context->shop->id);

        $raw_data = Db::getInstance()->executeS(
            'SELECT `id_product`
             FROM `'._DB_PREFIX_.'product_shop`
             WHERE `id_shop` = '.(int)$id_shop.'
              AND `active` = 1
              AND `id_product` IN ('.implode(',', array_map('intval', $ids)).')'
        );

        foreach ($raw_data as $product) {
            $result[] = $product['id_product'];
        }

        return $result;
    }

    public static function getGroupsTxt($id_pspc)
    {
        $context = Context::getContext();

        $groups_raw = Db::getInstance()->executeS(
            'SELECT gl.`name`
             FROM `'._DB_PREFIX_.'pspc_group` pg
             LEFT JOIN `'._DB_PREFIX_.'group_lang` gl ON pg.`id_group` = gl.`id_group`
              AND gl.`id_lang` = '.(int)$context->language->id.'
             WHERE pg.`id_pspc` = '.(int)$id_pspc
        );

        $groups = array();
        foreach ($groups_raw as $group_raw) {
            $groups[] = Tools::strtolower($group_raw['name']);
        }

        return implode(', ', $groups);
    }

    public static function searchByProductName($name)
    {
        $product_ids = Db::getInstance()->executeS(
            'SELECT DISTINCT `id_product`
             FROM `'._DB_PREFIX_.'product_lang`
             WHERE `name` LIKE "%'.pSQL($name).'%"'
        );

        return self::sqlArrayToList($product_ids, 'id_product');
    }

    public static function searchByCategoryName($name)
    {
        $product_ids = Db::getInstance()->executeS(
            'SELECT DISTINCT `id_category`
             FROM `'._DB_PREFIX_.'category_lang`
             WHERE `name` LIKE "%'.pSQL($name).'%"'
        );

        return self::sqlArrayToList($product_ids, 'id_category');
    }

    public static function searchByManufacturerName($name)
    {
        $product_ids = Db::getInstance()->executeS(
            'SELECT DISTINCT `id_manufacturer`
             FROM `'._DB_PREFIX_.'manufacturer`
             WHERE `name` LIKE "%'.pSQL($name).'%"'
        );

        return self::sqlArrayToList($product_ids, 'id_manufacturer');
    }

    public static function deleteAll()
    {
        $timers = self::getCollection('PSPC');

        foreach ($timers as $timer) {
            $timer->delete();
        }
    }
}
