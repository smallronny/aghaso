<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2019 Presta.Site
 * @license   LICENSE.txt
 */

class PSPCBlock extends ObjectModel
{
    public $hook;
    public $product_count;
    public $title;
    public $active;
    public $sort;
    public $source_all;
    public $source_specific_prices;
    public $source_pspc;

    public $page;
    public $desc;
    public $link_rewrite;
    public $meta_title;
    public $meta_desc;
    public $meta_keywords;

    public static $definition = array(
        'table' => 'pspc_block',
        'primary' => 'id_pspc_block',
        'multilang' => true,
        'fields' => array(
            // Classic fields
            'hook' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'product_count' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'sort' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false),
            'page' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            // Product sources
            'source_all' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'source_specific_prices' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'source_pspc' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            // Lang fields
            'title' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
            'link_rewrite' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isUrl'),
            'desc' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'meta_title' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
            'meta_desc' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
            'meta_keywords' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);

        $this->active = $this->isActive();
    }

    public function isActive()
    {
        if (!$this->id) {
            return true;
        }

        $context = Context::getContext();
        $is_active = Db::getInstance()->getValue(
            'SELECT `active`
             FROM `'._DB_PREFIX_.'pspc_block_shop`
             WHERE `id_pspc_block` = '.(int)$this->id.'
              AND `id_shop` = '.(int)$context->shop->id
        );

        return $is_active;
    }

    public function validateAllFields()
    {
        $errors = array();

        $valid = $this->validateFields(false, true);
        if ($valid !== true) {
            $errors[] = $valid . "\n";
        }
        $valid_lang = $this->validateFieldsLang(false, true);
        if ($valid_lang !== true) {
            $errors[] = $valid_lang . "\n";
        }

        return $errors;
    }

    public function validateField($field, $value, $id_lang = null, $skip = array(), $human_errors = true)
    {
        return parent::validateField($field, $value, $id_lang, $skip, $human_errors);
    }

    public static function displayFieldName($field, $class = __CLASS__, $htmlentities = true, Context $context = null)
    {
        return '"'.parent::displayFieldName($field, $class, $htmlentities, $context).'"';
    }

    public function delete()
    {
        $id_block = $this->id;
        $result = parent::delete();

        if ($result) {
            Db::getInstance()->execute(
                'DELETE FROM `' . _DB_PREFIX_ . 'pspc_block_source`
                 WHERE `id_pspc_block` = ' . (int)$id_block
            );
            Db::getInstance()->execute(
                'DELETE FROM `' . _DB_PREFIX_ . 'pspc_block_shop`
                 WHERE `id_pspc_block` = ' . (int)$id_block
            );
        }

        return $result;
    }

    public function save($null_values = false, $auto_date = true)
    {
        $result = parent::save($null_values, $auto_date);

        if ($result && $this->id) {
            // save shop data
            foreach (Shop::getContextListShopID() as $id_shop) {
                Db::getInstance()->execute(
                    'INSERT INTO `'._DB_PREFIX_.'pspc_block_shop`
                     (`id_pspc_block`, `id_shop`, `active`)
                     VALUES
                     ('.(int)$this->id.', '.(int)$id_shop.', '.(int)$this->active.')
                     ON DUPLICATE KEY UPDATE
                      `active` = '.(int)$this->active
                );
            }

            $module = Module::getInstanceByName('psproductcountdownpro');
            $module->clearSmartyCache();
            if ($this->hook != 'custom') {
                $module->registerHook($this->hook);
                if ($this->hook == 'displayHomeTabContent') {
                    $module->registerHook('displayHomeTab');
                }
            }
        }

        return $result;
    }

    public function getProductsFront($check_only = false, $count_only = false, $p = null, $n = null, $order_by = null, $order_way = null, $assemble = true)
    {
        $pspc_module = Module::getInstanceByName('psproductcountdownpro');
        $context = Context::getContext();
        $chosen_timers = $this->getSourceTimers();
        $this->createTmpTable();

        // todo maybe cache but anyway smarty cache is already working
        // Get products from timers
        if ($this->source_pspc || count($chosen_timers) || $this->source_all) {
            self::searchPSPCProducts($chosen_timers);
        }

        // Get products from specific prices
        if (($this->source_specific_prices || $this->source_all) && $pspc_module->activate_all_special) {
            self::searchSpecificPricesProducts();
        }

        $product_pool = $this->getProductPool();
        
        $this->dropTmpTable();

        if (!is_array($product_pool) || !count($product_pool)) {
            return array();
        }

        if ($check_only) {
            return $product_pool;
        }

        if ($count_only) {
            return count($product_pool);
        }

        $id_lang = $context->language->id;
        $interval =
            (Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20);

        // Query
        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $sql = '
            SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
                pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`,
                pl.`meta_keywords`, pl.`meta_title`, pl.`name`,
                m.`name` AS manufacturer_name, p.`id_manufacturer` as id_manufacturer,
                MAX(image_shop.`id_image`) id_image, il.`legend`,
                t.`rate`, pl.`meta_keywords`, pl.`meta_title`, pl.`meta_description`,
                DATEDIFF(p.`date_add`, DATE_SUB(NOW(),
                INTERVAL '.$interval.' DAY)) > 0 AS new
            FROM `'._DB_PREFIX_.'product` p
            '.Shop::addSqlAssociation('product', 'p', false).'
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
                ON p.`id_product` = pl.`id_product`
                AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
            LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
                Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
            LEFT JOIN `'._DB_PREFIX_.'image_lang` il
             ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
            LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`)
                AND tr.`id_country` = '.(int)Context::getContext()->country->id.'
                AND tr.`id_state` = 0
            LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
            '.Product::sqlStock('p');
        } elseif ($this->getPSVersion() == 1.6) {
            $sql = '
            SELECT
                p.id_product, IFNULL(product_attribute_shop.id_product_attribute,0) id_product_attribute,
                pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
                image_shop.`id_image` id_image, il.`legend`,
                p.`ean13`, p.`upc`, p.`reference`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order,
                IFNULL(stock.quantity, 0) as quantity, p.customizable,
                IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock,
                product_shop.`date_add` > "' . date('Y-m-d', strtotime('-' . $interval . ' DAY')) . '" as new,
                product_shop.`on_sale`, product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity
            FROM  `' . _DB_PREFIX_ . 'product` p
            ' . Shop::addSqlAssociation('product', 'p') . '
            LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_shop` product_attribute_shop
             ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1
              AND product_attribute_shop.id_shop=' . (int)$context->shop->id . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
             ON (product_attribute_shop.id_product_attribute=pa.id_product_attribute)
            LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl
             ON p.`id_product` = pl.`id_product`
              AND pl.`id_lang` = ' . (int)$id_lang . Shop::addSqlRestrictionOnLang('pl') . '
            LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop ON (image_shop.`id_product` = p.`id_product`
             AND image_shop.cover=1 AND image_shop.id_shop=' . (int)$context->shop->id . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image`
             AND il.`id_lang` = ' . (int)$id_lang . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl
                ON cl.`id_category` = product_shop.`id_category_default`
                AND cl.`id_lang` = ' . (int)$id_lang . Shop::addSqlRestrictionOnLang('cl') . Product::sqlStock('p', 0);
        } else { // 1.7
            $sql = '
            SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
                '.(Combination::isFeatureActive()?'product_attribute_shop.minimal_quantity
                 AS product_attribute_minimal_quantity,
                IFNULL(product_attribute_shop.id_product_attribute,0) id_product_attribute,':'').'
                pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`,
                pl.`meta_keywords`, pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`,
                m.`name` AS manufacturer_name, p.`id_manufacturer` as id_manufacturer,
                image_shop.`id_image` id_image, il.`legend`,
                t.`rate`, pl.`meta_keywords`, pl.`meta_title`, pl.`meta_description`,
                DATEDIFF(p.`date_add`, DATE_SUB("'.date('Y-m-d').' 00:00:00",
                INTERVAL '.(int) $interval.' DAY)) > 0 AS new'
            .' FROM  `'._DB_PREFIX_.'product` p
            '.Shop::addSqlAssociation('product', 'p', false);

            if (Combination::isFeatureActive()) {
                $sql .= '
                 LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
				    ON (p.`id_product` = product_attribute_shop.`id_product`
				     AND product_attribute_shop.`default_on` = 1
				     AND product_attribute_shop.id_shop='.(int)$context->shop->id.')';
            }

            $sql .= '
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
                ON p.`id_product` = pl.`id_product`
                AND pl.`id_lang` = '.(int)$context->language->id.Shop::addSqlRestrictionOnLang('pl').'
            LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
                ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1
                 AND image_shop.id_shop='.(int) $context->shop->id.')
            LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image`
             AND il.`id_lang` = '.(int)$context->language->id.')
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
            LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`)
                AND tr.`id_country` = '.(int) $context->country->id.'
                AND tr.`id_state` = 0
            LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
            '.Product::sqlStock('p', 0);
        }

        $sql .=
            ' WHERE
            p.`id_product` IN ('.implode(',', array_map('intval', $product_pool)).')
            AND product_shop.`active` = 1
            AND p.`visibility` != "none"';

        if (Group::isFeatureActive()) {
            $groups = FrontController::getCurrentCustomerGroups();
            $sql .= ' AND EXISTS(SELECT 1 FROM `'._DB_PREFIX_.'category_product` cp
				JOIN `'._DB_PREFIX_.'category_group` cg
				 ON (cp.id_category = cg.id_category AND cg.`id_group`
				  '.(count($groups) ? 'IN ('.implode(',', array_map('intval', $groups)).')' : '= 1').')
				WHERE cp.`id_product` = p.`id_product`)';
        }

        $sql .= ' GROUP BY p.`id_product` ';
        // Sorting
        if ($order_by) {
            $order_by  = Validate::isOrderBy($order_by)   ? Tools::strtolower($order_by)  : 'position';
            $order_way = Validate::isOrderWay($order_way) ? Tools::strtoupper($order_way) : 'ASC';

            $order_by_prefix = false;
            if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd') {
                $order_by_prefix = 'p';
            } elseif ($order_by == 'name') {
                $order_by_prefix = 'pl';
            } elseif ($order_by == 'manufacturer' || $order_by == 'manufacturer_name') {
                $order_by_prefix = 'm';
                $order_by = 'name';
            } elseif ($order_by == 'position') {
                $order_by_prefix = 'cp';
            } elseif ($order_by == 'price') {
                $order_by_prefix = 'p';
            }

            if ($order_by == 'position' && $this->getPSVersion() >= 1.7) {
                $sql .= ' ORDER BY FIELD(p.`id_product`, ' . implode(',', array_map('intval', $product_pool)) . ')';
            } else {
                $sql .= ' ORDER BY ' . (!empty($order_by_prefix) ? $order_by_prefix . '.' : '') . '`' . bqSQL($order_by)
                    . '` ' . pSQL($order_way);
            }
        } else {
            $sql .= ' ORDER BY FIELD(p.`id_product`, ' . implode(',', array_map('intval', $product_pool)) . ')';
        }

        if ($p !== null && $n !== null) {
            $sql .= ' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;
        } else {
            $sql .= ' LIMIT 0, ' . (int)$this->product_count;
        }

        $products = Db::getInstance()->executeS($sql);
        $products = Product::getProductsProperties($context->language->id, $products);

        if ($assemble && $this->getPSVersion() == 1.7) {
            $assembler = new ProductAssembler($context);
            $presenterFactory = new ProductPresenterFactory($context);
            $presentationSettings = $presenterFactory->getPresentationSettings();
            $presenter = new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
                new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                    $context->link
                ),
                $context->link,
                new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
                new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
                $context->getTranslator()
            );

            $products_for_template = array();

            foreach ($products as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $context->language
                );
            }

            $products = $products_for_template;
        }

        return $products;
    }

    public static function getBlocksFront($hook)
    {
        $blocks = self::getCollection('PSPCBlock');

        $blocks_ok = Db::getInstance()->executeS(
            'SELECT `id_pspc_block`
             FROM `'._DB_PREFIX_.'pspc_block_shop` 
             WHERE `active` = 1
              AND `id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')'
        );
        $blocks->where('id_pspc_block', 'IN', self::sqlArrayToList($blocks_ok, 'id_pspc_block'));

        $blocks->sqlWhere(
            '1
            '.(is_array($hook) ?
                ' AND `hook` IN ("'.implode('", "', array_map('pSQL', $hook)).'")' :
                ' AND `hook` = "'.pSQL($hook).'"')
        );

        return $blocks;
    }

    public static function getCollection($class)
    {
        $context = Context::getContext();
        if (class_exists('PrestaShopCollection')) {
            $collection = new PrestaShopCollection($class, $context->language->id);
        } else {
            $collection = new Collection($class, $context->language->id);
        }

        return $collection;
    }

    protected function getPSVersion($without_dots = false)
    {
        $ps_version = _PS_VERSION_;
        $ps_version = Tools::substr($ps_version, 0, 3);

        if ($without_dots) {
            $ps_version = str_replace('.', '', $ps_version);
        }

        return (float)$ps_version;
    }

    public function clearSources()
    {
        $this->source_all = false;
        $this->source_specific_prices = false;
        $this->source_pspc = false;
        Db::getInstance()->execute(
            'DELETE FROM `'._DB_PREFIX_.'pspc_block_source`
             WHERE `id_pspc_block` = '.(int)$this->id
        );
    }

    public function addSource($id_pspc)
    {
        Db::getInstance()->execute(
            'INSERT IGNORE INTO `'._DB_PREFIX_.'pspc_block_source`
             (`id_pspc_block`, `id_pspc`)
             VALUES
             ('.(int)$this->id.', '.(int)$id_pspc.')'
        );
    }

    public function searchSpecificPricesProducts($context = null)
    {
        if (!SpecificPrice::isFeatureActive()) {
            return array();
        }

        $context = ($context ? $context : Context::getContext());
        $id_shop = $context->shop->id;
        $id_currency = $context->currency->id;
        $id_group = $context->customer->id_default_group;
        $id_customer = $context->customer->id;
        $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        $ids = Address::getCountryAndState($id_address);
        $id_country = $ids['id_country'] ? (int)$ids['id_country'] : (int) Configuration::get('PS_COUNTRY_DEFAULT');

        $tz = Configuration::get('PS_TIMEZONE');
        $dt_current = new DateTime('now', new DateTimeZone($tz));
        $now = $dt_current->format('Y-m-d H:i:s');

        // search specific prices, ignore products already added from timers, limit for better performance
        $specific_prices = Db::getInstance()->executeS(
            'SELECT sp.`id_product`, sp.`to`
			 FROM `'._DB_PREFIX_.'specific_price` sp
			 LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON sp.`id_product` = ps.`id_product`
			 LEFT JOIN `'._DB_PREFIX_.'pspc_tmp` tmp ON sp.`id_product` = tmp.`id_product`
			 WHERE
			  ps.`active` = 1 AND ps.`visibility` != "none"
			  AND sp.`id_product` > 0
			  AND sp.`id_shop` IN (0, '.(int)$id_shop.')
			  AND sp.`id_currency` IN (0, '.(int)$id_currency.')
			  AND sp.`id_country` IN (0, '.(int)$id_country.')
              AND sp.`id_group` IN (0, '.(int)$id_group.')
              AND sp.`id_customer` IN (0, '.(int)$id_customer.')
              AND sp.`from_quantity` = 1
              AND sp.`reduction` > 0
              AND sp.`to` > "'.pSQL($now).'" AND sp.`from` <= "'.pSQL($now).'"
              AND sp.`id_specific_price` NOT IN (
                SELECT `id_specific_price` FROM `'._DB_PREFIX_.'pspc_relations`
              )
              AND tmp.`id_product` IS NULL '
               .($this->sort == 'ending_time' ? ' ORDER BY sp.`to` ASC ' : '')
               .($this->sort == 'discount' ? ' ORDER BY sp.`reduction` DESC ' : '') . ' 
               LIMIT 300'
        );

        $query = '';
        foreach ($specific_prices as $specific_price) {
            $tz = Configuration::get('PS_TIMEZONE');
            $dt_to = new DateTime($specific_price['to'], new DateTimeZone($tz));
            $dt_to->setTimezone(new DateTimeZone('UTC'))->modify('+1 second'); // because we add 1 second on sp creation
            $to = $dt_to->format('Y-m-d H:i:s');
            $id_product = $specific_price['id_product'];

            $price = Product::getPriceStatic($id_product, true, null, 6, null, false, false);
            $reduction = Product::getPriceStatic($id_product, true, null, 6, null, true);
            $percent = ($reduction * 100) / $price;
            $query .=
                '('.(int)$id_product.', 0, '.(float)$percent.', "'.pSQL($to).'"),';
        }
        $query = rtrim($query, ',');
        if ($query) {
            Db::getInstance()->execute('INSERT IGNORE INTO `' . _DB_PREFIX_ . 'pspc_tmp` VALUES ' . $query, false);
        }
    }

    public function searchPSPCProducts($ids_chosen_timers = array())
    {
        $pspcs = Db::getInstance()->executeS(
            'SELECT pspc.`id_pspc`, `to`
             FROM `'._DB_PREFIX_.'pspc` pspc
             LEFT JOIN `'._DB_PREFIX_.'pspc_shop` pspcs
              ON pspc.`id_pspc` = pspcs.`id_pspc`
             WHERE pspcs.`active` = 1
              AND pspcs.`id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')
              AND `from` <= UTC_TIMESTAMP() AND `to` > UTC_TIMESTAMP() '
              .($ids_chosen_timers ? ' AND pspc.`id_pspc` IN ('.implode(',', array_map('intval', $ids_chosen_timers)).') ' :'')
        );
        
        foreach ($pspcs as $pspc) {
            $query = '';
            $products = PSPC::getRelatedProductsIDs($pspc['id_pspc']);
            foreach ($products as $id_product) {
                $price = Product::getPriceStatic($id_product, true, null, 6, null, false, false);
                if ($price <= 0) {
                    continue;
                }
                $reduction = Product::getPriceStatic($id_product, true, null, 6, null, true);
                $percent = ($reduction * 100) / $price;
                $query .=
                    '('.(int)$id_product.', '.(int)$pspc['id_pspc'].', '.(float)$percent.', "'.pSQL($pspc['to']).'"),';
            }
            $query = rtrim($query, ',');
            if ($query) {
                Db::getInstance()->execute('INSERT IGNORE INTO `' . _DB_PREFIX_ . 'pspc_tmp` VALUES ' . $query, false);
            }
        }
    }

    public function getSourceTimers()
    {
        $result = array();

        $timers = Db::getInstance()->executeS(
            'SELECT `id_pspc`
             FROM `'._DB_PREFIX_.'pspc_block_source`
             WHERE `id_pspc_block` = '.(int)$this->id
        );

        foreach ($timers as $timer) {
            $result[] = $timer['id_pspc'];
        }

        return $result;
    }

    protected function getPSPCObjects($ids = array())
    {
        $query = '
            SELECT *
            FROM `'._DB_PREFIX_.'pspc_object` po
            LEFT JOIN `'._DB_PREFIX_.'pspc` p ON po.`id_pspc` = p.`id_pspc`
            LEFT JOIN `'._DB_PREFIX_.'pspc_shop` pspcs ON po.`id_pspc` = pspcs.`id_pspc`
            WHERE pspcs.`active` = 1
             AND pspcs.`id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')
             AND p.`personal` = 0 AND p.`to` > UTC_TIMESTAMP() AND p.`from` <= UTC_TIMESTAMP()'
            .($ids ? ' AND p.`id_pspc` IN ('.implode(',', array_map('intval', $ids)).')' : '')
            .($this->sort == 'ending_time' ? ' ORDER BY `to` ASC ' : '')
            .($this->sort == 'discount' ? ' ORDER BY `reduction` DESC ' : '')
            .($this->sort == 'random' ? ' ORDER BY RAND() ' : '')
            // there can't be more products than the limit but maybe there are any empty categories etc
            .' LIMIT 0, '.(int)($this->product_count + 50);

        return Db::getInstance()->executeS($query);
    }

    protected function checkIsProductVisible($id_product, $id_shop = null)
    {
        if (!$id_shop) {
            $context = Context::getContext();
            $id_shop = $context->shop->id;
        }

        $query =
            'SELECT `id_product` 
             FROM `'._DB_PREFIX_.'product_shop`
             WHERE `id_product` = '.(int)$id_product.'
              AND `id_shop` = '.(int)$id_shop.'
              AND `active` = 1
              AND `visibility` != "none"';

        return Db::getInstance()->getValue($query);
    }

    public function arrayMergeRecursiveDistinct(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = $this->arrayMergeRecursiveDistinct($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    protected function createTmpTable()
    {
        Db::getInstance()->execute(
            'CREATE TEMPORARY TABLE `' . _DB_PREFIX_ . 'pspc_tmp` (
                `id_product` INT UNSIGNED NOT NULL DEFAULT 0,
                `id_pspc` INT UNSIGNED NOT NULL DEFAULT 0,
                `reduction` FLOAT(20, 6),
                `to` DATETIME,
                UNIQUE(`id_product`)
             ) ENGINE=MEMORY',
            false
        );
    }

    protected function dropTmpTable()
    {
        Db::getInstance()->execute('DROP TEMPORARY TABLE `' . _DB_PREFIX_ . 'pspc_tmp`', false);
    }

    protected function getProductPool()
    {
        $order_by = '`id_product` DESC';
        if ($this->sort == 'ending_time') {
            $order_by = '`to` ASC';
        } elseif ($this->sort == 'discount') {
            $order_by = '`reduction` DESC';
        } elseif ($this->sort == 'random') {
            $order_by = 'RAND()';
        }

        $products = Db::getInstance()->executeS(
            'SELECT `id_product`
             FROM `'._DB_PREFIX_.'pspc_tmp`
             WHERE 1
             ORDER BY '.pSQL($order_by).'
             LIMIT 0, '.(int)$this->product_count
        );

        $result = array();
        foreach ($products as $product) {
            $result[] = $product['id_product'];
        }

        return $result;
    }

    public static function sqlArrayToList($array, $column)
    {
        $result = array();

        foreach ($array as $item) {
            $result[] = $item[$column];
        }

        if (!count($result)) {
            $result = array(0);
        }

        return $result;
    }

    /**
     * @param bool $active_only
     * @return array list of shop ids
     */
    public function getShops($active_only = true)
    {
        $result = array();

        $shops = Db::getInstance()->executeS(
            'SELECT `id_shop`
             FROM `'._DB_PREFIX_.'pspc_block_shop`
             WHERE `id_pspc_block` = '.(int)$this->id.
            ($active_only ? ' AND `active` = 1 ' : '')
        );

        foreach ($shops as $shop) {
            $result[] = $shop['id_shop'];
        }

        return $result;
    }

    public static function getShopsStatic($id_pspc_block, $active_only = true)
    {
        $pspcb = new PSPCBlock($id_pspc_block);

        return $pspcb->getShops($active_only);
    }

    public static function getBlockByLinkRewrite($link_rewrite, $id_lang = null)
    {
        $context = Context::getContext();
        if (!$id_lang) {
            $id_lang = $context->language->id;
        }

        // if link rewrite is ID:
        if (is_numeric($link_rewrite)) {
            $block = new PSPCBlock($link_rewrite, $id_lang);
            if (Validate::isLoadedObject($block)) {
                return $block;
            }
        }

        // else find block by real link rewrite
        $id_block = Db::getInstance()->getValue(
            'SELECT b.`id_pspc_block`
             FROM `'._DB_PREFIX_.'pspc_block` b
             LEFT JOIN `'._DB_PREFIX_.'pspc_block_lang` bl ON b.`id_pspc_block` = bl.`id_pspc_block`
             WHERE bl.`link_rewrite` = "'.pSQL($link_rewrite).'"
             AND bl.`id_lang` = '.(int)$id_lang
        );

        $block = new PSPCBlock($id_block, $id_lang);
        if (Validate::isLoadedObject($block)) {
            return $block;
        }

        return false;
    }

    public function getUrl()
    {
        $context = Context::getContext();
        $module = Module::getInstanceByName('psproductcountdownpro');

        $link_rewrite = $this->id;
        if (is_array($this->link_rewrite)) {
            if ($this->link_rewrite[$context->language->id]) {
                $link_rewrite = $this->link_rewrite[$context->language->id];
            }
        } elseif ($this->link_rewrite) {
            $link_rewrite = $this->link_rewrite;
        }

        return $context->link->getModuleLink($module->name, 'block', array('link_rewrite' => $link_rewrite));
    }
}
