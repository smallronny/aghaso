{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2018 Presta.Site
* @license   LICENSE.txt
*}

<div id="pspc-toolbar" class="ps{$psvd|intval}">
    <a id="pspc-new-block-btn" class="toolbar_btn pointer btn btn-success" href="{$form_url|escape:'quotes':'UTF-8'}" title="{l s='Add new block' mod='psproductcountdownpro'}">
        <i class="icon-plus"></i>
        {l s='New block' mod='psproductcountdownpro'}
    </a>
    <p>
        {l s='Here you can manage blocks of products in your Front Office. For example, you can add a block to your home page and display there chosen products with countdown timers.' mod='psproductcountdownpro'}
    </p>
</div>
