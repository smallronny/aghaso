{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2017 Presta.Site
* @license   LICENSE.txt
*}
<div id="pspc-instructions">
    {if $psv == 1.5}
        <br/><fieldset><legend>{l s='Additional instructions' mod='psproductcountdownpro'}</legend>
    {else}
        <div class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i> {l s='Additional instructions' mod='psproductcountdownpro'}
        </div>
    {/if}

        <h4>{l s='Custom hook' mod='psproductcountdownpro'}</h4>
        <p>
            {l s='Usually it is necessary only when you need to display a countdown in a non-standard place.' mod='psproductcountdownpro'}
            <br>
            {l s='You can use this custom hook to place a countdown anywhere in your template:' mod='psproductcountdownpro'}
            <b>{literal}<pre>{hook h='pspc' id_product='X'}</pre>{/literal}</b>
            ({l s='Replace X by some product ID' mod='psproductcountdownpro'})
        </p>

        <p>
            {l s='Here are examples for the most common cases:' mod='psproductcountdownpro'} <br>

            {if $psv <= 1.6}
                <ul>
                    <li>
                        {l s='In' mod='psproductcountdownpro'} <b>product.tpl</b> {l s='use' mod='psproductcountdownpro'}
                        {literal}<pre>{hook h='pspc' id_product=$product->id}</pre>{/literal}
                    </li>
                    <li>
                        {l s='In' mod='psproductcountdownpro'} <b>product-list.tpl</b> {l s='use' mod='psproductcountdownpro'}
                        {literal}<pre>{hook h='pspc' id_product=$product.id_product}</pre>{/literal}
                    </li>
                </ul>
            {else}
                <ul>
                    <li>
                        {l s='At the [1]product page[/1] you can use this code:' tags=['<b>'] mod='psproductcountdownpro'}
                        {literal}<pre>{hook h='pspc' id_product=$product.id_product}</pre>{/literal}
                        {l s='Product page main template:' mod='psproductcountdownpro'} /themes/{$pspc_ps_theme|escape:'html':'UTF-8'}/templates/catalog/product.tpl
                        <br>
                        {l s='Sub templates are located in this directory' mod='psproductcountdownpro'} /themes/{$pspc_ps_theme|escape:'html':'UTF-8'}/templates/catalog/_partials/
                        <br><br>
                    </li>
                    <li>
                        {l s='In the [1]product list[/1] you can use this code:' tags=['<b>'] mod='psproductcountdownpro'}
                        {literal}<pre>{hook h='pspc' id_product=$product.id_product}</pre>{/literal}
                        {l s='Product miniature template:' mod='psproductcountdownpro'} /themes/{$pspc_ps_theme|escape:'html':'UTF-8'}/templates/catalog/_partials/miniatures/product.tpl
                    </li>
                </ul>
            {/if}
        </p>

        <p>{l s='Simply paste that code to the necessary place in your template file.' mod='psproductcountdownpro'}</p>

        <hr>

        <h4>{l s='Additional countdown' mod='psproductcountdownpro'}</h4>
        <p>
            {l s='You can also use this HTML code to create a countdown independently of any product:' mod='psproductcountdownpro'}
            <b><pre>{'<div class="pspc-custom pspc-inactive">2018-12-31 23:59</div>'|escape:'html':'UTF-8'}</pre></b>
            ({l s='enter the desired date instead of default' mod='psproductcountdownpro'})
            <br> {l s='Simply paste that code to the needed place in your template file or CMS editor.' mod='psproductcountdownpro'}
        </p>

        <hr>

        <h4>{l s='Custom hook for a block of products' mod='psproductcountdownpro'}</h4>
        <p>
            {l s='You can use this custom hook to place a block of products anywhere in your template:' mod='psproductcountdownpro'}
            <b>{literal}<pre>{hook h='pspcBlock' id_block='X'}</pre>{/literal}</b>
            ({l s='Replace X by the necessary block ID' mod='psproductcountdownpro'})
        </p>

    {if $psv == 1.5}
        </fieldset><br/>
    {else}
        </div>
    {/if}
</div>
