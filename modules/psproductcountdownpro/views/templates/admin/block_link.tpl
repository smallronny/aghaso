{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2020 Presta.Site
* @license   LICENSE.txt
*}
<a target="_blank" href="{$url|escape:'html':'UTF-8'}"><i class="icon icon-link"></i> {l s='Link' mod='psproductcountdownpro'}</a>