{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2019 Presta.Site
* @license   LICENSE.txt
*}

<div class="{if !$pspc->id}add-countdown-wrp{else}edit-countdown-wrp{/if}">
    {if !$pspc->id}
    <button class="btn btn-primary add-pspc">
        <i class="icon-plus-square"></i>
        {l s='Add new countdown' mod='psproductcountdownpro'}
    </button>
    <button class="btn btn-default pull-right" id="pspc-delete-expired">
        <i class="icon icon-trash"></i>
        {l s='Delete expired timers' mod='psproductcountdownpro'}
    </button>
    {/if}

    <div class="countdown-form">
        <div class="row">
            <div class="col-lg-{if $pspc->id}11{else}12{/if}">
                <div class="form-group row pspc-items-form-group">
                    <label class="control-label col-lg-2">
                        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='Select products for using with this countdown' mod='psproductcountdownpro'}">
                            {l s='Items:' mod='psproductcountdownpro'} <sup>*</sup>
                        </span>
                    </label>
                    <div class="col-lg-10 col-xs-12 pspc-prodselects-wrp">
                        <button class="btn btn-default pspc-select-obj pspc-select-products" data-type="products"><i class="icon-plus"></i> {l s='Select products' mod='psproductcountdownpro'} <span class="badge badge-info pspc-number-products">{count($pspc_chosen_products)|intval}</span></button>
                        <button class="btn btn-default pspc-select-obj pspc-select-categories" data-type="categories"><i class="icon-plus"></i> {l s='Select categories' mod='psproductcountdownpro'} <span class="badge badge-info pspc-number-categories">{count($pspc_chosen_categories)|intval}</span></button>
                        <button class="btn btn-default pspc-select-obj pspc-select-manufacturers" data-type="manufacturers"><i class="icon-plus"></i> {l s='Select manufacturers' mod='psproductcountdownpro'} <span class="badge badge-info pspc-number-manufacturers">{count($pspc_chosen_manufacturers)|intval}</span></button>

                        <div class="pspc-select-wrp pspc-select-wrp-products">
                            <div class="row">
                                <div class="col-lg-6 pspc-col-filters">
                                    <span class="btn btn-default btn-xs pspc-toggle-category-filter"><i class="icon-filter"></i> {l s='Filter by category' mod='psproductcountdownpro'}</span>
                                    <span class="btn btn-default btn-xs pspc-toggle-manufacturer-filter"><i class="icon-filter"></i> {l s='Filter by manufacturer' mod='psproductcountdownpro'}</span>
                                    <label class="pspc-search-combinations-label" for="pspc-search-combinations-{$pspc->id|intval}"><input type="checkbox" class="pspc-search-combinations" id="pspc-search-combinations-{$pspc->id|intval}"> {l s='Search combinations' mod='psproductcountdownpro'}</label>
                                    <div class="pspc-manufacturer-wrp pspc-filter-wrp">
                                        <select class="pspc-manufacturer-select">
                                            <option value="">--</option>
                                            {foreach from=$pspc_manufacturers item='manufacturer'}
                                                <option value="{$manufacturer.id_manufacturer|intval}">{$manufacturer.name|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div class="pspc-category-wrp pspc-filter-wrp">
                                        {$product_category_tree nofilter} {*rendered html*}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" class="form-control pspc-product-search" placeholder="{l s='Search products' mod='psproductcountdownpro'}">
                                </div>
                                <div class="col-lg-6"><h4>{l s='Selected products:' mod='psproductcountdownpro'}</h4></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <select multiple class="pspc_prod_select">
                                        <option disabled value="" class="pspc-default-option">{l s='Search products or use filters to get results' mod='psproductcountdownpro'}</option>
                                    </select>
                                    <a href="#" class="btn btn-default btn-block pspc_multiple_select_add">
                                        {l s='Add' mod='psproductcountdownpro'} <i class="icon-arrow-right"></i>
                                    </a>
                                </div>
                                <div class="col-lg-6">
                                    <select multiple class="pspc_prod_selected" name="products[]">
                                        {foreach from=$pspc_chosen_products item='chosen_product'}
                                            {assign var='ref' value=$pspc_module->getProductReference($chosen_product.id_object, $chosen_product.id_product_attribute)}
                                            <option value="{$chosen_product.id_object|intval}{if $chosen_product.id_product_attribute}-{$chosen_product.id_product_attribute|intval}{/if}">#{$chosen_product.id_object|intval} {Product::getProductName($chosen_product.id_object, $chosen_product.id_product_attribute)|escape:'html':'UTF-8'} {if $ref}({l s='ref:' mod='psproductcountdownpro'} {$ref|escape:'html':'UTF-8'}){/if}</option>
                                        {/foreach}
                                    </select>
                                    <a href="#" class="btn btn-default btn-block pspc_multiple_select_del">
                                        {l s='Remove' mod='psproductcountdownpro'} <i class="icon-arrow-left"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="pspc-select-wrp pspc-select-wrp-categories">
                            {$select_category_tree nofilter} {*rendered html*}
                        </div>
                        <div class="pspc-select-wrp pspc-select-wrp-manufacturers">
                            <div class="form-group pspc-swap-wrp">
                                <div class="form-control-static row">
                                    <div class="col-xs-6">
                                        <h4>{l s='Available manufacturers:' mod='psproductcountdownpro'}</h4>
                                        <select size="8" class="pspc-swap-left" multiple>
                                            {foreach from=$pspc_manufacturers item='manufacturer'}
                                                {if !in_array($manufacturer.id_manufacturer, $pspc_chosen_manufacturers)}
                                                    <option value="{$manufacturer.id_manufacturer|intval}">{$manufacturer.name|escape:'html':'UTF-8'}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <a href="#" class="btn btn-default btn-block pspc-add-swap">{l s='Add' mod='psproductcountdownpro'} <i class="icon-arrow-right"></i></a>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4>{l s='Selected manufacturers:' mod='psproductcountdownpro'}</h4>
                                        <select size="8" class="pspc-swap-right" multiple name="manufacturers[]">
                                            {foreach from=$pspc_manufacturers item='manufacturer'}
                                                {if in_array($manufacturer.id_manufacturer, $pspc_chosen_manufacturers)}
                                                    <option value="{$manufacturer.id_manufacturer|intval}">{$manufacturer.name|escape:'html':'UTF-8'}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <a href="#" class="btn btn-default btn-block pspc-remove-swap"><i class="icon-arrow-left"></i> {l s='Remove' mod='psproductcountdownpro'}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group row">
                    <label class="control-label col-lg-2">
                        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='A text displayed alongside the countdown.' mod='psproductcountdownpro'}">
                            {l s='Promo text:' mod='psproductcountdownpro'}
                        </span>
                    </label>
                    <div class="col-lg-5">
                        {assign var='name_id' value="pspc-add-name`$pspc->id`"}
                        {$pspc_module->generateInput(['type' => 'text', 'lang' => true, 'name' => 'name', 'class' => 'pspc-add-name', 'id' => $name_id, 'values' => $pspc->name]) nofilter}
                    </div>
                </div>

                <div class="form-group row datepicker-row">
                    <label class="control-label col-lg-2">
                        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='Select time interval for this countdown.' mod='psproductcountdownpro'}">
                            {l s='Display:' mod='psproductcountdownpro'} <sup>*</sup>
                        </span>
                    </label>
                    <div class="col-lg-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{l s='from' mod='psproductcountdownpro'}</span>
                                    <input type="text" name="from" class="pspc-datepicker" value="{$pspc->from_tz|escape:'html':'UTF-8'}" style="text-align: center;">
                                    <span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{l s='to' mod='psproductcountdownpro'}</span>
                                    <input type="text" name="to" class="pspc-datepicker" value="{$pspc->to_tz|escape:'html':'UTF-8'}" style="text-align: center;">
                                    <span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row discount-row">
                    <label for="pspc_discount" class="control-label col-lg-2">
                        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='Create a specific price.' mod='psproductcountdownpro'}">
                            {l s='Apply a discount of:' mod='psproductcountdownpro'}
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <input type="text" class="form-control pspc_discount" name="reduction" value="{if floatval($pspc->reduction)}{$pspc->reduction * 1|escape:'html':'UTF-8'}{/if}">
                    </div>
                    <div class="col-lg-1">
                        <select class="pspc_discount_type" name="reduction_type">
                            <option value="percentage" {if $pspc->reduction_type == 'percentage'}selected{/if}>{l s='%' mod='psproductcountdownpro'}</option>
                            <option value="amount" {if $pspc->reduction_type == 'amount'}selected{/if}>{$pspc_default_currency->sign|escape:'html':'UTF-8'}</option>
                        </select>
                    </div>
                    <div class="col-lg-1">
                        <select class="pspc_reduction_tax_add" name="reduction_tax">
                            <option value="0" {if $pspc->reduction_tax == 0}selected{/if}>{l s='Tax excluded' mod='psproductcountdownpro'}</option>
                            <option value="1" {if $pspc->reduction_tax == 1}selected{/if}>{l s='Tax included' mod='psproductcountdownpro'}</option>
                        </select>
                    </div>
                </div>

                {if !$pspc->id}
                <div class="form-group row">
                    <div class="col-lg-2">
                        <button class="btn btn-default pspc-toggle-options">{l s='Options' mod='psproductcountdownpro'} <span class="pspc-toggle-sign">+</span><span class="pspc-toggle-sign" style="display: none;">-</span></button>
                    </div>
                </div>
                {/if}

                <div class="pspc-options">
                    <div class="form-group row pspc-options-row">
                        <label class="control-label col-lg-2">
                            <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='Not required, visible only for you.' mod='psproductcountdownpro'}">
                                {l s='Campaign name:' mod='psproductcountdownpro'}
                            </span>
                        </label>
                        <div class="col-lg-5">
                            <input type="text" name="system_name" value="{$pspc->system_name|escape:'html':'UTF-8'}">
                        </div>
                    </div>

                    <div class="form-group row pspc-options-row">
                        <label for="pspc_customer_personal" class="control-label col-lg-2">
                            <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='Display countdown for each customer for some specified time since first viewing of related product.' mod='psproductcountdownpro'}">
                                {l s='Customer personal countdown:' mod='psproductcountdownpro'}
                            </span>
                        </label>
                        <div class="col-lg-5">
                            <input type="checkbox" name="personal" class="pspc_customer_personal" {if $pspc->personal}checked{/if} value="1">
                        </div>
                    </div>

                    <div class="form-group row personal-row" style="display: none;">
                        <label class="control-label col-lg-2">
                            {l s='Display for:' mod='psproductcountdownpro'}
                        </label>
                        <div class="col-lg-1">
                            <div class="input-group">
                                <input type="text" name="personal_hours" value="{$pspc->personal_hours*1|escape:'html':'UTF-8'}"><span class="input-group-addon">{l s='hours' mod='psproductcountdownpro'}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row pspc-options-row">
                        <label class="control-label col-lg-2">
                            <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='Restart this countdown for the same duration when X hours passed after its end.' mod='psproductcountdownpro'}">
                                {l s='Restart after:' mod='psproductcountdownpro'}
                            </span>
                        </label>
                        <div class="col-lg-1">
                            <div class="input-group">
                                <input type="text" name="hours_restart" value="{if $pspc->hours_restart > 0}{$pspc->hours_restart*1|escape:'html':'UTF-8'}{/if}"><span class="input-group-addon">{l s='hours' mod='psproductcountdownpro'}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row pspc-options-row action-row">
                        <label for="pspc_action_start" class="control-label col-lg-2">
                            {l s='Action on countdown start:' mod='psproductcountdownpro'}
                        </label>
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <select name="action_start" class="pspc_action_start pspc_action_select">
                                        <option value="">--</option>
                                        {foreach from=$actions_start item="action" key="key"}
                                            <option value="{$key|escape:'quotes':'UTF-8'}" {if $pspc->action_start == $key}selected{/if}>{$action|escape:'html':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <input type="number"
                                           name="action_start_qty"
                                           class="pspc-action-addon pspc-action-addon-set_qty form-control"
                                           placeholder="{l s='Quantity' mod='psproductcountdownpro'}"
                                           value="{if $pspc->action_start_qty !== null}{$pspc->action_start_qty|intval}{/if}"
                                           {if $pspc->action_start == 'set_qty'}style="display: block;" {/if}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row pspc-options-row action-row">
                        <label for="pspc_action_end" class="control-label col-lg-2">
                            {l s='Action on countdown end:' mod='psproductcountdownpro'}
                        </label>
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <select name="action_end" class="pspc_action_end pspc_action_select">
                                        <option value="">--</option>
                                        {foreach from=$actions_end item="action" key="key"}
                                            <option value="{$key|escape:'quotes':'UTF-8'}" {if $pspc->action_end == $key}selected{/if}>{$action|escape:'html':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <input type="number"
                                           name="action_end_qty"
                                           class="pspc-action-addon pspc-action-addon-set_qty form-control"
                                           placeholder="{l s='Quantity' mod='psproductcountdownpro'}"
                                           value="{if $pspc->action_end_qty !== null}{$pspc->action_end_qty|intval}{/if}"
                                           {if $pspc->action_end == 'set_qty'}style="display: block;" {/if}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row pspc-options-row">
                        <label class="control-label col-lg-2">
                            <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="{l s='Display this countdown only for selected groups. Uncheck "All" to see the groups.' mod='psproductcountdownpro'}">
                                {l s='Customer groups:' mod='psproductcountdownpro'}
                            </span>
                        </label>
                        <div class="col-lg-5 pspc-cg-col">
                            <label class="pspc-cg-label">
                                {if Group::isFeatureActive()}
                                    <input type="checkbox" class="pspc-cg-all" name="all_groups" value="1" {if $pspc->all_groups}checked{/if}>
                                    {l s='All' mod='psproductcountdownpro'}
                                {else}
                                    <input type="checkbox" class="pspc-cg-all" checked disabled>
                                    <input type="hidden" name="all_groups" value="1">
                                    {l s='All' mod='psproductcountdownpro'}
                                    ({l s='groups are disabled' mod='psproductcountdownpro'}: <a target="_blank" href="{$pspc_perf_url|escape:'html':'UTF-8'}">{l s='settings' mod='psproductcountdownpro'}</a>)
                                {/if}
                            </label>
                            <div class="pspc-cg-wrp" {if $pspc->all_groups || !Group::isFeatureActive()}style="display: none;" {/if}>
                            {foreach from=$pspc_groups item='group'}
                                <label class="pspc-cg-label">
                                    <input type="checkbox" name="groups[]" value="{$group.id_group|intval}" {if $pspc->checkGroup($group.id_group)}checked{/if}>
                                    {$group.name|escape:'html':'UTF-8'}
                                </label>
                            {/foreach}
                            </div>
                        </div>
                    </div>
                </div>

                {if !$pspc->id}
                <div class="form-group row ">
                    <div class="col-lg-2">

                    </div>
                    <div class="col-lg-4 col-xs-12">
                        <div class="alert alert-danger add-pspc-error" style="display: none;"></div>
                        <div class="pspc-form-btns-add">
                            <button class="btn btn-primary btn-pspc-submit">{l s='Add' mod='psproductcountdownpro'}</button>
                        </div>
                    </div>
                </div>
                {else}
                    <div class="alert alert-danger add-pspc-error" style="display: none;"></div>
                {/if}
            </div>
            {if $pspc->id}
                <div class="col-lg-1">
                    <div class="pspc-form-btns-edit">
                        <button class="btn btn-primary pull-right btn-pspc-submit"><i class="icon-save"></i> {l s='Save' mod='psproductcountdownpro'}</button>
                        <br>
                        <button class="btn btn-default pspc-toggle-options">{l s='Options' mod='psproductcountdownpro'} <span class="pspc-toggle-sign">+</span><span class="pspc-toggle-sign" style="display: none;">-</span></button>
                        <input type="hidden" name="id_pspc" value="{$pspc->id|intval}">
                    </div>
                </div>
            {/if}
            <input type="hidden" name="action" value="saveCountdown">
            <input type="hidden" name="ajax" value="1">
        </div>

        <div class="close-countdown-form close-product-countdown-form">&#10006;</div>
    </div>
</div>
