{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2017 Presta.Site
* @license   LICENSE.txt
*}
<div id="pspc-rating">
    {if $psv == 1.5}
        <br/><fieldset><legend>{l s='Feedback' mod='psproductcountdownpro'}</legend>
    {else}
        <div class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i> {l s='Feedback' mod='psproductcountdownpro'}
        </div>
    {/if}

        <div>
            <p>
                <a href="http://addons.prestashop.com/ratings.php" target="_blank">
                    <img src="{$module_path|escape:'quotes':'UTF-8'}views/img/rating.png" alt="#">
                </a>
            </p>
            <br>
            <p>{l s='Please rate this module if you like it. You can do it in your profile at Addons Marketplace' mod='psproductcountdownpro'}
                (<a target="_blank" href="https://addons.prestashop.com/ratings.php" class="pbc-addons-link">{l s='link' mod='psproductcountdownpro'}</a>). {l s='Thanks!' mod='psproductcountdownpro'}</p>
            <br>
            <p>{l s='And of course feel free to contact us if you have any questions!' mod='psproductcountdownpro'}</p>
        </div>

    {if $psv == 1.5}
        </fieldset><br/>
    {else}
        </div>
    {/if}
</div>
