{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2018 Presta.Site
* @license   LICENSE.txt
*}

<div class="pspc-block-page pspc{$psvd|escape:'html':'UTF-8'}">
    {capture name=path}{if $pspc_block->title}{$pspc_block->title|escape:'html':'UTF-8'}{else}{l s='Flash sale' mod='psproductcountdownpro'}{/if}{/capture}
    <div class="rte">
        <h1 class="page-heading h1">{if $pspc_block->title}{$pspc_block->title|escape:'html':'UTF-8'}{else}{l s='Flash sale' mod='psproductcountdownpro'}{/if}</h1>

        {if $pspc_block->desc}
        <div class="pspc-desc-wrp">
            {$pspc_block->desc nofilter} {* HTML *}
        </div>
        {/if}
    </div>

    {if $pspc_products}
        <div class="content_sortPagiBar">
            <div class="sortPagiBar clearfix">
                {include file="$pspc_theme_dir./product-sort.tpl"}
{*                {include file="$pspc_theme_dir./nbr-product-page.tpl"}*}
            </div>
            <div class="top-pagination-content clearfix">
                {include file="$pspc_theme_dir./pagination.tpl" no_follow=1}
            </div>
        </div>

        {include file="$pspc_theme_dir./product-list.tpl" products=$pspc_products class='pspc-products' page_name='index'}

        <div class="content_sortPagiBar">
            <div class="bottom-pagination-content clearfix">
                {include file="$pspc_theme_dir./pagination.tpl" no_follow=1 paginationId='bottom'}
            </div>
        </div>
    {else}
        <p class="alert alert-warning">{l s='No products.' mod='psproductcountdownpro'}</p>
    {/if}
</div>