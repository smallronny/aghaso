{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2020 Presta.Site
* @license   LICENSE.txt
*}

{extends file='catalog/listing/product-list.tpl'}

{block name='head_seo_title'}{$meta_title|escape:'html':'UTF-8'}{/block}
{block name='head_seo_description'}{$meta_description|escape:'html':'UTF-8'}{/block}
{block name='head_seo_keywords'}{$meta_keywords|escape:'html':'UTF-8'}{/block}

{block name='content'}
    <section id="main">
        {block name='product_list_header'}
            <div class="page-content card card-block">
                <h1 id="js-product-list-header" class="h2">{if $listing.label}{$listing.label|escape:'html':'UTF-8'}{else}{l s='Flash sale' mod='psproductcountdownpro'}{/if}</h1>
                {if $pspc_block->desc}{$pspc_block->desc nofilter} {* HTML *}{/if}
            </div>
        {/block}

        <section id="products">
            {if $listing.products|count}

                <div id="">
                    {block name='product_list_top'}
                        {include file='catalog/_partials/products-top.tpl' listing=$listing}
                    {/block}
                </div>

                {block name='product_list_active_filters'}
                    <div id="" class="hidden-sm-down">
                        {$listing.rendered_active_filters nofilter}
                    </div>
                {/block}

                <div id="">
                    {block name='product_list'}
                        {include file='catalog/_partials/products.tpl' listing=$listing}
                    {/block}
                </div>

                <div id="js-product-list-bottom">
                    {block name='product_list_bottom'}
                        {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
                    {/block}
                </div>

            {else}

            <div class="page-content card card-block">
                <p>{l s='No products.' mod='psproductcountdownpro'}</p>
            </div>
            {/if}
        </section>

    </section>
{/block}