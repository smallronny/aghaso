{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2017 Presta.Site
* @license   LICENSE.txt
*}
<script type="text/javascript">
    var pspc_psv = {$psv|floatval};
    var pspc_ajax_url = "{$ajax_url|escape:'quotes':'UTF-8'}";
    var pspc_remove_confirm_txt = "{l s='Are you sure you want to delete this countdown?' mod='psproductcountdownpro'}";
    var pspc_basic_confirm_txt = "{l s='Are you sure?' mod='psproductcountdownpro'}";
    var pspc_flatpickr = false;

    $(document).on('focus', '.pspc-datepicker', function () {
        if (!$(this).hasClass('flatpickr-input')) {
            pspc_loadDatetimepicker();
        }
    });

    function pspc_loadDatetimepicker() {
        if (typeof pspc_flatpickr === 'object' && typeof pspc_flatpickr.destroy === 'function') {
            pspc_flatpickr.destroy();
        }

        {literal}
        pspc_flatpickr = flatpickr('.pspc-datepicker', {
            enableTime: true,
            time_24hr: true,
            dateFormat: 'Z',
            altInput: true,
            altFormat: 'Y-m-d H:i',
            disableMobile: true,
            locale: {
                weekdays: {
                    {/literal}shorthand: ['{l s='Su.' mod='psproductcountdownpro'}', '{l s='Mo.' mod='psproductcountdownpro'}', '{l s='Tu.' mod='psproductcountdownpro'}', '{l s='We.' mod='psproductcountdownpro'}', '{l s='Th.' mod='psproductcountdownpro'}', '{l s='Fr.' mod='psproductcountdownpro'}', '{l s='Sa.' mod='psproductcountdownpro'}'],
                    longhand: ['{l s='Sunday' mod='psproductcountdownpro'}', '{l s='Monday' mod='psproductcountdownpro'}', '{l s='Tuesday' mod='psproductcountdownpro'}', '{l s='Wednesday' mod='psproductcountdownpro'}', '{l s='Thursday' mod='psproductcountdownpro'}', '{l s='Friday' mod='psproductcountdownpro'}', '{l s='Saturday' mod='psproductcountdownpro'}']{literal}
                },
                months: {
                    {/literal}shorthand: ['{l s='Jan' mod='psproductcountdownpro'}', '{l s='Feb' mod='psproductcountdownpro'}', '{l s='Mar' mod='psproductcountdownpro'}', '{l s='Apr' mod='psproductcountdownpro'}', '{l s='May' mod='psproductcountdownpro'}', '{l s='Jun' mod='psproductcountdownpro'}', '{l s='Jul' mod='psproductcountdownpro'}', '{l s='Aug' mod='psproductcountdownpro'}', '{l s='Sep' mod='psproductcountdownpro'}', '{l s='Oct' mod='psproductcountdownpro'}', '{l s='Nov' mod='psproductcountdownpro'}', '{l s='Dec' mod='psproductcountdownpro'}'],
                    longhand: ['{l s='January' mod='psproductcountdownpro'}', '{l s='February' mod='psproductcountdownpro'}', '{l s='March' mod='psproductcountdownpro'}', '{l s='April' mod='psproductcountdownpro'}', '{l s='May' mod='psproductcountdownpro'}', '{l s='June' mod='psproductcountdownpro'}', '{l s='July' mod='psproductcountdownpro'}', '{l s='August' mod='psproductcountdownpro'}', '{l s='September' mod='psproductcountdownpro'}', '{l s='October' mod='psproductcountdownpro'}', '{l s='November' mod='psproductcountdownpro'}', '{l s='December' mod='psproductcountdownpro'}']{literal}
                },
                firstDayOfWeek: 1
            }
        });
        {/literal}
    }
</script>