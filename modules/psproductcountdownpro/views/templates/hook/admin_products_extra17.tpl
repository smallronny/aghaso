{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2017 Presta.Site
* @license   LICENSE.txt
*}
<div id="module_psproductcountdown" class="">
    <input type="hidden" name="submitted_tabs[]" value="{$module_name|escape:'html':'UTF-8'}" />
    <input type="hidden" name="{$module_name|escape:'html':'UTF-8'}-submit" value="1" />

    <div class="row">
        <div class="col-lg-12 col-xl-4">
            <fieldset class="form-group">
                <label class="form-control-label">{l s='Enabled:' mod='psproductcountdownpro'}</label>
                <div id="pspc_active">
                    <div class="radio">
                        <label class="">
                            <input type="radio" id="pspc_active_1" name="pspc_active" value="1" {if !isset($pspc->active) || (isset($pspc->active) && $pspc->active)}checked{/if}>
                            {l s='Yes' mod='psproductcountdownpro'}
                        </label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <label class="">
                            <input type="radio" id="pspc_active_0" name="pspc_active" value="0" {if (isset($pspc->active) && !$pspc->active)}checked{/if}>
                            {l s='No' mod='psproductcountdownpro'}
                        </label>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-xl-4">
            <fieldset class="form-group">
                <label class="form-control-label">{l s='Promo text:' mod='psproductcountdownpro'}</label>
                <div class="translations tabbable" id="pspc_name_wrp">
                    <div class="translationsFields tab-content ">
                        {foreach from=$languages item=language name=pspc_lang_foreach}
                            <div class="translationsFields-pspc_name tab-pane translation-field translation-label-{$language.iso_code|escape:'html':'UTF-8'} {if $smarty.foreach.pspc_lang_foreach.first}active{/if}">
                                <input type="text"
                                       id="pspc_name_{$language.id_lang|intval}"
                                       name="pspc_name_{$language.id_lang|intval}"
                                       class="form-control"
                                       value="{$pspc->name[$language.id_lang]|escape:'html':'UTF-8'}"
                                />
                            </div>
                        {/foreach}
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xl-4">
            <fieldset class="form-group">
                <label class="form-control-label">{l s='Display:' mod='psproductcountdownpro'}</label>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">{l s='from' mod='psproductcountdownpro'}</span></div>
                            <input type="text" name="pspc_from" class="pspc-datepicker form-control pspc-datetime-utc" value="{$pspc->from_tz|escape:'html':'UTF-8'}" style="text-align: center;" id="pspc_from">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">{l s='to' mod='psproductcountdownpro'}</span></div>
                            <input type="text" name="pspc_to" class="pspc-datepicker form-control pspc-datetime-utc" value="{$pspc->to_tz|escape:'html':'UTF-8'}" style="text-align: center;" id="pspc_to">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-xl-4">
            <fieldset class="form-group">
                <label class="form-control-label">{l s='Use dates from specific prices:' mod='psproductcountdownpro'}</label>
                <div id="pspc_specific_price_wrp">
                    <select name="pspc_specific_price" id="pspc_specific_price" class="form-control">
                        <option value="">--</option>
                        {foreach from=$specific_prices item=specific_price}
                            <option value="{$specific_price.id_specific_price|intval}"
                                    data-from="{$specific_price.from|escape:'html':'UTF-8'}"
                                    data-to="{$specific_price.to|escape:'html':'UTF-8'}">
                                {l s='from' mod='psproductcountdownpro'}: {$specific_price.from|escape:'html':'UTF-8'}&nbsp;&nbsp;&nbsp;
                                {l s='to' mod='psproductcountdownpro'}: {$specific_price.to|escape:'html':'UTF-8'}
                            </option>
                        {/foreach}
                    </select>
                </div>
            </fieldset>
        </div>
    </div>

    {if isset($pspc->id)}
        <input type="hidden" name="id_pspc" id="id_pspc" value="{$pspc->id|intval}">
        <div class="form-group">
            <div class="row">
                <div class="col-lg-12 col-xl-4">
                    <fieldset class="form-group">
                        <div>
                            <button type="button" id="pspc-reset-countdown" class="btn btn-default" data-id-countdown="{$pspc->id|intval}">{l s='Reset & remove' mod='psproductcountdownpro'}</button>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    {else}
        <input type="hidden" name="id_pspc" id="id_pspc" value="0">
    {/if}

    <div class="form-group">
        <div id="pspc_error" class="alert alert-danger" style="display: none;"></div>
        <div id="pspc_saved" class="alert alert-success" style="display: none;">{l s='Saved' mod='psproductcountdownpro'}</div>
        <input type="hidden" name="id_product" value="{$id_product|intval}">
        <button class="btn btn-primary" id="pspc_save_product_countdown">{l s='Save countdown' mod='psproductcountdownpro'}</button>
    </div>
</div>
