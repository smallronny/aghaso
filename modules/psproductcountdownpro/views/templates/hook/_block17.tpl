{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2018 Presta.Site
* @license   LICENSE.txt
*}

{if isset($pspc_products) && is_array($pspc_products) && count($pspc_products)}
<section class="featured-products clearfix pspc_block pspc_hook_{$pspc_block_hook|escape:'html':'UTF-8'} {if $pspc_block->hook == 'displayHomeTabContent'}tab-pane{/if}"
         {if $pspc_block->hook == 'displayHomeTabContent'}id="pspc_home_tab_{$pspc_block->id|intval}"{/if}
>
    {if $pspc_block->hook != 'displayHomeTabContent'}
        <h2 class="h2 products-section-title text-uppercase">
            {if $pspc_block->title}
                {$pspc_block->title|escape:'html':'UTF-8'}
            {else}
                {l s='Discounts' mod='psproductcountdownpro' d='Shop.Theme.Catalog'}
            {/if}
        </h2>
    {/if}
    <div class="products">
        {foreach from=$pspc_products item="product"}
            {include file="catalog/_partials/miniatures/product.tpl" product=$product}
        {/foreach}
    </div>
</section>
{/if}
