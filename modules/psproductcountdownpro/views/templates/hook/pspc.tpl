{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2018 Presta.Site
* @license   LICENSE.txt
*}
<div class="pspc-wrp {if $pspc_hook}pspc_{$pspc_hook|escape:'html':'UTF-8'}{/if} {if strpos($pspc_product_list_position, 'over_img') !== false}pspc-wrp-over-img{/if} pspc-valign-{$pspc_vertical_align|escape:'quotes':'UTF-8'}">
    <div class="psproductcountdown pspc-inactive pspc{$pspc_psv|escape:'html':'UTF-8'}
        {if $pspc_ipa}pspc-combi-wrp pspc-cw-{$pspc_ipa|intval}{/if}
        {if strpos($pspc_product_list_position, 'over_img') !== false}pspc-over-img{/if}
        {if $pspc_compact_view && !$pspc_too_long}compact_view{/if}
        {if $pspc_show_promo_text || $pspc_too_long}pspc-show-promo-text{else}pspc-hide-promo-text{/if}
        {if $pspc_background_image}background_image{/if} pspc-{$pspc_theme|escape:'quotes':'UTF-8'}
        {if $pspc_too_long}pspc-long{/if}
        {if $pspc_colon}pspc-show-colon{/if}
        pspc-highlight-{$pspc_highlight|escape:'html':'UTF-8'}"
         data-to="{$pspc->to_time|escape:'html':'UTF-8'}"
         data-name="{if $pspc->name}{$pspc->name|escape:'html':'UTF-8'}{elseif $pspc_promo_text}{$pspc_promo_text|escape:'html':'UTF-8'}{else}{l s='Offer ends in:' mod='psproductcountdownpro'}{/if}"
         data-personal="{$pspc->personal|intval}"
         data-personal-hours="{$pspc->personal_hours|floatval}"
         data-personal-hours-restart="{$pspc->hours_restart|escape:'html':'UTF-8'}"
         data-id-countdown="{$pspc->id|intval}"
    >
        <div class="pspc-main days-diff-{$pspc_days_diff|intval} {if $pspc_days_diff >= 100}pspc-diff-m100{/if}">
            <div class="pspc-offer-ends">
                {if $pspc_too_long && $pspc->to_date}
                    {if $pspc->name}
                        {$pspc->name|escape:'html':'UTF-8'}
                    {else}
                        {l s='Offer ends on %s' sprintf=[$pspc->to_date] mod='psproductcountdownpro'}
                    {/if}
                {else}
                    {if $pspc->name}
                        {$pspc->name|escape:'html':'UTF-8'}
                    {elseif $pspc_promo_text}
                        {$pspc_promo_text|escape:'html':'UTF-8'}
                    {else}
                        {l s='Offer ends in:' mod='psproductcountdownpro'}
                    {/if}
                {/if}
            </div>
        </div>
    </div>
</div>
<script>
    if (typeof pspc_initCountdown === 'function') {
        pspc_initCountdown();
    }
</script>
