{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2018 Presta.Site
* @license   LICENSE.txt
*}

{if isset($pspc_blocks) && count($pspc_blocks)}
    {foreach from=$pspc_blocks item='pspc_block'}
        {assign var='pspc_products' value=$pspc_block->getProductsFront(true)}
        {if is_array($pspc_products) && count($pspc_products)}
        <li>
            <a data-toggle="tab" href="#pspc_home_tab_{$pspc_block->id|intval}" class="pspc_products">
                {if $pspc_block->title}
                    {$pspc_block->title|escape:'html':'UTF-8'}
                {else}
                    {l s='Discounts' mod='psproductcountdownpro' d='Shop.Theme.Catalog'}
                {/if}
            </a>
        </li>
        {/if}
    {/foreach}
{/if}
