{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2019 Presta.Site
* @license   LICENSE.txt
*}
<style type="text/css">
    {if $pspc_custom_css}
        {$pspc_custom_css|escape:'quotes':'UTF-8'}
    {/if}
</style>

<script type="text/javascript">
    var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': '{l s='days' mod='psproductcountdownpro'}',
        'hours': '{l s='hours' mod='psproductcountdownpro'}',
        'minutes': '{l s='min.' mod='psproductcountdownpro'}',
        'seconds': '{l s='sec.' mod='psproductcountdownpro'}'
    };
    var pspc_labels_lang_1 = {
        'days': '{l s='day' mod='psproductcountdownpro'}',
        'hours': '{l s='hour' mod='psproductcountdownpro'}',
        'minutes': '{l s='min.' mod='psproductcountdownpro'}',
        'seconds': '{l s='sec.' mod='psproductcountdownpro'}'
    };
    var pspc_offer_txt = "{l s='Offer ends in:' mod='psproductcountdownpro'}";
    var pspc_theme = "{$pspc_theme|escape:'html':'UTF-8'}";
    var pspc_psv = {$psv|floatval};
    var pspc_hide_after_end = {$pspc_hide_after_end|intval};
    var pspc_hide_expired = {$pspc_hide_expired|intval};
    var pspc_highlight = "{$pspc_highlight|escape:'html':'UTF-8'}";
    var pspc_position_product = "{$pspc_position_product|escape:'html':'UTF-8'}";
    var pspc_position_list = "{$pspc_position_list|escape:'html':'UTF-8'}";
    var pspc_adjust_positions = {$pspc_adjust_positions|intval};
    var pspc_promo_side = "{$pspc_promo_side|escape:'html':'UTF-8'}";
    var pspc_token = "{Tools::getToken(false)}";
    {if $pspc_custom_js}
    function pspc_callbackBeforeDisplay($pspc_container, $pspc) {
        try {
            {$pspc_custom_js nofilter} /* JS code */
        } catch (e) {
            console.log(e.name + ":" + e.message + "\n" + e.stack);
        }
    }
    {/if}
</script>