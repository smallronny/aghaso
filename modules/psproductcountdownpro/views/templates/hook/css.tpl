{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2019 Presta.Site
* @license   LICENSE.txt
*}
/* Auto generated from css.tpl */

{* both positions can be in the hook displayProductPriceBlock, so we have to hide duplicates via css *}
{if $pspc_module->product_position != 'displayProductPriceBlock' && ($pspc_module->product_list_position == 'over_img' || $pspc_module->product_list_position == 'displayProductPriceBlock' || $pspc_module->product_list_position == 'over_img_price' || $pspc_module->product_list_position == 'over_img_unit_price' || $pspc_module->product_list_position == 'over_before_price' || $pspc_module->product_list_position == 'over_img_after_price')}
    #product .pspc-wrp.pspc_displayProductPriceBlock {ldelim}
        display: none !important;
    {rdelim}
    #product .ajax_block_product .pspc-wrp.pspc_displayProductPriceBlock,
    #product .product_list .pspc-wrp.pspc_displayProductPriceBlock,
    #product #product_list .pspc-wrp.pspc_displayProductPriceBlock,
    #product .product-miniature .pspc-wrp.pspc_displayProductPriceBlock {ldelim}
        display: block !important;
    {rdelim}
{elseif $pspc_module->product_position == 'displayProductPriceBlock' && !in_array($pspc_module->product_list_position, ['over_img', 'displayProductPriceBlock'])}
    #product .pspc-wrp.pspc_displayProductPriceBlock {ldelim}
        display: block !important;
    {rdelim}
    .ajax_block_product .pspc-wrp.pspc_displayProductPriceBlock,
    .product_list .pspc-wrp.pspc_displayProductPriceBlock,
    #product_list .pspc-wrp.pspc_displayProductPriceBlock,
    .product-miniature .pspc-wrp.pspc_displayProductPriceBlock {ldelim}
        display: none !important;
    {rdelim}
{elseif $pspc_module->product_position == 'displayProductPriceBlock' && in_array($pspc_module->product_list_position, ['over_img_price', 'over_img_unit_price', 'over_before_price', 'over_img_after_price'])}
    .ajax_block_product .pspc-wrp.pspc_displayProductPriceBlock,
    .product_list .pspc-wrp.pspc_displayProductPriceBlock,
    #product_list .pspc-wrp.pspc_displayProductPriceBlock,
    .product-miniature .pspc-wrp.pspc_displayProductPriceBlock {ldelim}
        display: none !important;
    {rdelim}
{/if}

{if $pspc_base_font_size}
    {literal}
    body .psproductcountdown {
        font-size: {/literal}{$pspc_base_font_size*100|floatval}%{literal} !important;
    }
    {/literal}
{/if}
{if $pspc_base_font_size_list}
    {literal}
    body .product_list .psproductcountdown,
    body #product_list .psproductcountdown,
    body .ajax_block_product .psproductcountdown,
    body .product-miniature .psproductcountdown {
        font-size: {/literal}{$pspc_base_font_size_list*100|floatval}%{literal} !important;
    }
    {/literal}
{/if}
{if $pspc_base_font_size_col}
    {literal}
    body #left_column .psproductcountdown, body #right_column .psproductcountdown,
    body #left-column .psproductcountdown, body #right-column .psproductcountdown {
        font-size: {/literal}{$pspc_base_font_size_col*100|floatval}%{literal} !important;
    }
    {/literal}
{/if}
{if $pspc_show_promo_text}
    {literal}
    .psproductcountdown .pspc_h,
    .psproductcountdown .pspc-offer-ends {
        display: block;
    }
    {/literal}
{else}
    {literal}
    .psproductcountdown .pspc_h,
    .psproductcountdown .pspc-offer-ends {
        display: none;
    }
    .psproductcountdown.pspc-long .pspc_h,
    .psproductcountdown.pspc-long .pspc-offer-ends {
        display: block;
    }
    {/literal}
{/if}
{if $pspc_background_image}
    body div .psproductcountdown.background_image {
        padding-top: 5px;
    }
    {literal}
    body div .psproductcountdown.background_image,
    #product div .psproductcountdown.background_image {
        background: url('{/literal}{$pspc_upload_dir|escape:'html':'UTF-8'}{$pspc_background_image|escape:'html':'UTF-8'}{literal}') center center !important;
        -webkit-background-size: cover;
        background-size: cover;
    }
    .pspc-main .pspc-count {
        background: none;
    }
    {/literal}
{/if}

{if $pspc_border_radius}
    {if $pspc_theme == '2-dark' || $pspc_theme == '3-light'}
        {literal}
        .psproductcountdown .pspc-main .pspc-time {
            border-radius: {/literal}{$pspc_border_radius|intval}px{literal};
        }
        .psproductcountdown .pspc-main .pspc-count.pspc-top {
            border-radius: {/literal}{$pspc_border_radius|intval}px {$pspc_border_radius|intval}px 0 0{literal};
        }
        .psproductcountdown .pspc-main .pspc-count.pspc-bottom {
            border-radius: {/literal}0 0 {$pspc_border_radius|intval}px {$pspc_border_radius|intval}px{literal};
        }
        {/literal}
    {elseif $pspc_theme == '5'}
        {literal}
        .psproductcountdown {
            border-radius: {/literal}{$pspc_border_radius|intval}px{literal} !important;
        }
        {/literal}
    {elseif $pspc_theme == '6'}
        {literal}
        .psproductcountdown .pspc-main .pspc-time.days {
            border-radius: {/literal}{$pspc_border_radius|intval}px 0 0 {$pspc_border_radius|intval}px{literal};
        }
        .psproductcountdown .pspc-main .pspc-time.seconds {
            border-radius: {/literal}0 {$pspc_border_radius|intval}px {$pspc_border_radius|intval}px 0{literal};
        }
        {/literal}
    {else}
        {literal}
        .pspc-main .pspc-count {
            border-radius: {/literal}{$pspc_border_radius|intval}{literal}px;
        }
        {/literal}
    {/if}
{/if}

{assign var="bg_key" value="`$pspc_theme`-bg"}
{if isset($pspc_colors_data[$bg_key]) && $pspc_colors_data[$bg_key]['value']}
    {if $pspc_theme == '6'}
        {literal}
        body .psproductcountdown .pspc-highlight {
            background: {/literal}{$pspc_colors_data["`$pspc_theme`-bg"]['value']|escape:'quotes':'UTF-8'}{literal};
        }
        body .pspc-main .pspc-time {
            border-color: {/literal}{$pspc_colors_data["`$pspc_theme`-bg"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {elseif $pspc_theme == '8-clock' || $pspc_theme == '9-clock-b'}
    {literal}
        body .pspc-main .pspc-time {
            border-color: {/literal}{$pspc_colors_data["`$pspc_theme`-bg"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
    {/literal}
    {elseif strpos($pspc_theme, 'minimal') !== false}
        {literal}
        #product div .psproductcountdown, div .product-miniature .psproductcountdown, div .ajax_block_product .psproductcountdown, body div .psproductcountdown {
            background-color: {/literal}{$pspc_colors_data["`$pspc_theme`-bg"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {else}
        {literal}
        #products .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #product .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #product_list .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #js-product-list .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #products .psproductcountdown .pspc-main .pspc-time .pspc-count,
        .psproductcountdown .pspc-main .pspc-time .pspc-count {
            background: {/literal}{$pspc_colors_data["`$pspc_theme`-bg"]['value']|escape:'quotes':'UTF-8'}{literal};
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-digits"]['value']|escape:'quotes':'UTF-8'}{literal};
        }
        {/literal}
    {/if}
{/if}

{if isset($pspc_colors_data["`$pspc_theme`-digits"]) && $pspc_colors_data["`$pspc_theme`-digits"]['value']}
    {if $pspc_theme == '6'}
        {literal}
            body .pspc-main .pspc-count,
            body .psproductcountdown.pspc-show-colon .pspc-main .pspc-count:after {
                color: {/literal}{$pspc_colors_data["`$pspc_theme`-digits"]['value']|escape:'quotes':'UTF-8'}{literal};
            }
        {/literal}
    {elseif $pspc_theme == '4'}
        {literal}
        .psproductcountdown .pspc-time .pspc-digit span {
            background-color: {/literal}{$pspc_colors_data["`$pspc_theme`-digits"]['value']|escape:'quotes':'UTF-8'}{literal};
            border-color: {/literal}{$pspc_colors_data["`$pspc_theme`-digits"]['value']|escape:'quotes':'UTF-8'}{literal};
        }
        body .psproductcountdown.pspc-show-colon .pspc-main .pspc-count:after {
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-digits"]['value']|escape:'quotes':'UTF-8'}{literal};
        }
        {/literal}
    {elseif strpos($pspc_theme, 'minimal') !== false}
        {literal}
        .psproductcountdown .pspc-countdown-wrp,
        body .psproductcountdown.pspc-show-colon .pspc-main .pspc-count:after {
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-digits"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {else}
        {literal}
        #products .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #product .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #product_list .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #js-product-list .psproductcountdown .pspc-main .pspc-time .pspc-count,
        #products .psproductcountdown .pspc-main .pspc-time .pspc-count,
        .psproductcountdown .pspc-main .pspc-time .pspc-count,
        body .psproductcountdown.pspc-show-colon .pspc-main .pspc-count:after {
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-digits"]['value']|escape:'quotes':'UTF-8'}{literal};
        }
        {/literal}
    {/if}
{/if}

{assign var="key_high" value="`$pspc_theme`-high"}
{if isset($pspc_colors_data[$key_high]) && $pspc_colors_data["`$pspc_theme`-high"]['value']}
    {literal}
        body .psproductcountdown .pspc-main .pspc-highlight .pspc-count {
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-high"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
    {/literal}
    {if $pspc_theme == '6'}
        {literal}
        body .psproductcountdown .pspc-highlight .pspc-label {
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-high"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {elseif $pspc_theme == '4'}
        {literal}
        .psproductcountdown .pspc-time.pspc-highlight .pspc-digit span {
            background-color: {/literal}{$pspc_colors_data["`$pspc_theme`-high"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
            border-color: {/literal}{$pspc_colors_data["`$pspc_theme`-high"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {/if}
{/if}

{assign var="key_promo" value="`$pspc_theme`-promo"}
{if isset($pspc_colors_data[$key_promo]) && $pspc_colors_data[$key_promo]['value']}
    {if strpos($pspc_theme, 'minimal') !== false}
        {literal}
        #product .psproductcountdown .pspc_h,
        .product-miniature .psproductcountdown .pspc_h,
        .ajax_block_product .psproductcountdown .pspc_h,
        body .psproductcountdown .pspc_h,
        .psproductcountdown.pspc-long .pspc-main {
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-promo"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {else}
        {literal}
        body .pspc-offer-ends,
        .psproductcountdown.pspc-long .pspc-main {
            color: {/literal}{$pspc_colors_data["`$pspc_theme`-promo"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {/if}
{/if}

{assign var="key_labels" value="`$pspc_theme`-labels"}
{if isset($pspc_colors_data[$key_labels]) && $pspc_colors_data[$key_labels]['value']}
{literal}
    body .pspc-main .pspc-label {
        color: {/literal}{$pspc_colors_data["`$pspc_theme`-labels"]['value']|escape:'quotes':'UTF-8'}{literal} !important;
    }
{/literal}
{/if}

{assign var='param_name' value="`$pspc_theme`-border"}
{if isset($pspc_colors_data[$param_name]) && $pspc_colors_data[$param_name]['value']}
    {if $pspc_theme == '8-clock' || $pspc_theme == '9-clock-b'}
        {literal}
        body div .psproductcountdown {
            border-color: {/literal}{$pspc_colors_data[$param_name]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {else}
        {literal}
        body .pspc-main .pspc-count.pspc-bottom {
            border-color: {/literal}{$pspc_colors_data[$param_name]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {/if}
{/if}

{assign var='param_name' value="`$pspc_theme`-shadow"}
{if isset($pspc_colors_data[$param_name]) && $pspc_colors_data[$param_name]['value']}
    {literal}
    body .pspc-main .pspc-time {
        box-shadow: 0 0 10px 0 {/literal}{$pspc_colors_data[$param_name]['value']|escape:'quotes':'UTF-8'}{literal};
    }
    {/literal}
{/if}

{assign var='param_name' value="`$pspc_theme`-list-bg"}
{if isset($pspc_colors_data[$param_name]) && $pspc_colors_data[$param_name]['value']}
    {if $pspc_theme == '12-minimal-3'}
        {literal}
            body .product_list .psproductcountdown .pspc-main,
            body #product_list .psproductcountdown .pspc-main,
            body .ajax_block_product .psproductcountdown .pspc-main,
            body .product-miniature .psproductcountdown .pspc-main,
            #product .product_list .psproductcountdown .pspc-main,
            #product #product_list .psproductcountdown .pspc-main,
            #product .ajax_block_product .psproductcountdown .pspc-main,
            #product .product-miniature .psproductcountdown .pspc-main {
                background-color: {/literal}{$pspc_colors_data[$param_name]['value']|escape:'quotes':'UTF-8'}{literal} !important;
                padding: 0 4px !important;
                border-radius: 2px !important;
            }
        {/literal}
    {else}
        {literal}
        body .product_list .psproductcountdown,
        body #product_list .psproductcountdown,
        body .ajax_block_product .psproductcountdown,
        body .product-miniature .psproductcountdown,
        #product .product_list .psproductcountdown,
        #product #product_list .psproductcountdown,
        #product .ajax_block_product .psproductcountdown,
        #product .product-miniature .psproductcountdown {
            background-color: {/literal}{$pspc_colors_data[$param_name]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {/if}
{/if}
{assign var='param_name' value="`$pspc_theme`-product-bg"}
{if isset($pspc_colors_data[$param_name]) && $pspc_colors_data[$param_name]['value']}
    {if $pspc_theme == '12-minimal-3'}
    {literal}
        #product .psproductcountdown .pspc-main {
            background-color: {/literal}{$pspc_colors_data[$param_name]['value']|escape:'quotes':'UTF-8'}{literal} !important;
            padding: 0 4px !important;
            border-radius: 2px !important;
        }
    {/literal}
    {else}
        {literal}
        #product .psproductcountdown {
            background-color: {/literal}{$pspc_colors_data[$param_name]['value']|escape:'quotes':'UTF-8'}{literal} !important;
        }
        {/literal}
    {/if}
{/if}
