{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2018 Presta.Site
* @license   LICENSE.txt
*}
{if isset($pspc_blocks) && count($pspc_blocks)}
    {foreach from=$pspc_blocks item='pspc_block'}
        {include file=$pspc_block_tpl_file pspc_products=$pspc_block->getProductsFront()}
    {/foreach}
{/if}
