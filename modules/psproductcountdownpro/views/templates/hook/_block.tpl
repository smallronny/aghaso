{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2018 Presta.Site
* @license   LICENSE.txt
*}

{if isset($pspc_products) && is_array($pspc_products) && count($pspc_products)}
    <div class="block products_block exclusive pspc_block pspc_hook_{$pspc_block_hook|escape:'html':'UTF-8'} {if $pspc_block->hook == 'displayHomeTabContent'}tab-pane{/if}"
         {if $pspc_block->hook == 'displayHomeTabContent'}id="pspc_home_tab_{$pspc_block->id|intval}"{/if}
    >
        {if $pspc_block->hook != 'displayHomeTabContent'}
            <h4 class="title_block">
                {if $pspc_block->title}
                    {$pspc_block->title|escape:'html':'UTF-8'}
                {else}
                    {l s='Discounts' mod='psproductcountdownpro' d='Shop.Theme.Catalog'}
                {/if}
            </h4>
        {/if}
        <div class="block_content">
            {assign var='pspc_classes' value="pspc_products tab-pane"}
            {include file="$tpl_dir./product-list.tpl" class=$pspc_classes id='pspc_products' products=$pspc_products}
        </div>
    </div>
{/if}
