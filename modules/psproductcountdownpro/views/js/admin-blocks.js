/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2019 Presta.Site
 * @license   LICENSE.txt
 */
$(function () {
    pspc_updateDisplayPageOptions();

    $(document).on('click', '#pspc-new-block-btn', function (e) {
        e.preventDefault();

        $('#pspc_block_form').slideToggle('100');
    });

    $(document).on('submit', '#pspc_block_form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        try {
            tinyMCE.triggerSave();
        } catch (e) {
            console.log(e);
        }

        var $this = $(this);
        var data = $(this).serialize();

        $.ajax({
            url: pspc_ajax_url,
            cache: false,
            method: 'post',
            data: data,
            success: function (data) {
                if (data === '1') {
                    pspc_reloadBlocks();
                } else {
                    $this.find('.pspc-errors-wrp').html(data);
                }
            }
        });

        return false;
    });

    // Delete block from the list
    var block_del_selector = '.pspc-block-list .delete';
    $(document).on('click', block_del_selector, function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        $.ajax({
            url: url,
            method: 'post',
            beforeSend: function () {
                $('#pspc_block-wrp').addClass('pst-loading');
            },
            complete: function () {
                pspc_reloadBlocks();
            }
        });
    });

    $(document).on('click', '.pspc_active_toggle', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var id_block = $(this).data('id-block');

        if (id_block) {
            $.ajax({
                url: pspc_ajax_url,
                data: {ajax: true, action: 'changeBlockStatus', id_block: id_block},
                method: 'post',
                beforeSend: function () {
                    $('#pspc_block-wrp').addClass('pst-loading');
                },
                success: function () {
                    pspc_reloadBlocks();
                }
            });
        }
    });

    // Edit block in the list
    $(document).on('click', '#pspc_block-list-wrp .edit, #pspc_block-list-wrp td.pointer', function (e) {
        e.preventDefault();

        $(this).parents('td:first').find('.pspc-edit-wrp').toggle(100);
        $(this).parents('tr:first').next('.pspc_edit_row').toggle(200);
        pspc_updateDisplayPageOptions();
    });

    $(document).on('click', '.pspc_edit_row [name="submitpspc_block"]', function (e) {
        e.preventDefault();

        try {
            tinyMCE.triggerSave();
        } catch (e) {
            console.log(e);
        }

        var $edit_row = $(this).parents('.pspc_edit_row:first');
        var data = $edit_row.find(':input').serialize();

        $(this).addClass('loading').prop('disabled', true);

        $.ajax({
            url: pspc_ajax_url,
            data: data,
            method: 'post',
            success: function (data) {
                if (data === '1') {
                    pspc_reloadBlocks();
                } else {
                    $edit_row.find('.pspc-errors-wrp').html(data);
                }
            }
        });
    });

    $(document).on('click', '.pspc-toggle-children-sources', function (e) {
        e.preventDefault();

        // toggle children display
        $(this).next('.pspc-children-sources').slideToggle(100);
        // toggle button text +-
        var text = $(this).text();
        var new_text = (text === '+' ? '-' : '+');
        $(this).text(new_text);
    });

    // check / uncheck parent source checkboxes
    $(document).on('change', '.pspc_source_checkbox', function (e) {
        e.preventDefault();

        var $parent = $(this).parents('.pspc-sources-wrp:first');

        // checkbox 'all timers'
        if ($parent.find('.pspc_source_pspc_item').length) {
            if (!$parent.find('.pspc_source_pspc_item:not(:checked)').length) {
                $parent.find('.pspc_source_pspc').prop('checked', true);
            } else {
                $parent.find('.pspc_source_pspc').prop('checked', false);
            }
        }

        // checkbox 'All'
        if (!$parent.find('.pspc_source_checkbox:not(:checked)').length && !$parent.find('.pspc_source_pspc:not(:checked)').length) {
            $parent.find('.pspc_source_all').prop('checked', true);
        } else {
            $parent.find('.pspc_source_all').prop('checked', false);
        }
    });

    // check / uncheck child pspc source checkboxes
    $(document).on('change', '.pspc_source_pspc', function (e) {
        e.preventDefault();

        var $parent = $(this).parents('.pspc-sources-wrp:first');

        if ($(this).prop('checked')) {
            $parent.find('.pspc_source_pspc_item').prop('checked', true);
        } else {
            $parent.find('.pspc_source_pspc_item').prop('checked', false);
        }

        // checkbox 'All'
        if (!$parent.find('.pspc_source_checkbox:not(:checked)').length && !$parent.find('.pspc_source_pspc:not(:checked)').length) {
            $parent.find('.pspc_source_all').prop('checked', true);
        } else {
            $parent.find('.pspc_source_all').prop('checked', false);
        }
    });

    // check / uncheck child all source checkboxes
    $(document).on('change', '.pspc_source_all', function (e) {
        e.preventDefault();

        var $parent = $(this).parents('.pspc-sources-wrp:first');

        if ($(this).prop('checked')) {
            $parent.find('.pspc_source_checkbox, .pspc_source_pspc').prop('checked', true);
        } else {
            $parent.find('.pspc_source_checkbox, .pspc_source_pspc').prop('checked', false);
        }
    });

    // Bulk delete blocks
    $(document).on('submit', '.pspc-block-list form:first', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var action = $(this).attr('action');
        if (action.indexOf('submitBulkdeletepspc_block') !== -1) {
            var ids = [];
            $('[name="pspc_blockBox[]"]:checked').each(function () {
                ids.push($(this).val());
            });
            $.ajax({
                url: pspc_ajax_url,
                data: {ajax: true, action: 'bulkDeletePSPCBlock', ids: ids},
                method: 'post',
                success: function () {
                    pspc_reloadBlocks();
                }
            });
        }

        return false;
    });

    $(document).on('change', '.pspc_block_page_radio input', function (e) {
        pspc_updateDisplayPageOptions();
    });
});

function pspc_reloadBlocks() {
    $.ajax({
        url: pspc_ajax_url,
        method: 'post',
        data: {ajax: true, action: 'getBlockList'},
        beforeSend: function () {
            $('#pspc_block-wrp').addClass('pst-loading');
        },
        success: function (html) {
            $('#pspc_block-wrp').replaceWith(html);
        },
        complete: function () {
            $('#pspc_block-wrp').removeClass('pst-loading');
        }
    });
}

function pspc_updateDisplayPageOptions() {
    var $option = $('[name=page]:checked');
    $option.each(function () {
        var page_enabled = $(this).val();
        var $parent = $(this).closest('form');

        // if extra options are visible
        if (page_enabled === '1') {
            $parent.find('.pspc_block_page_options_row').slideDown(200);
        } else {
            $parent.find('.pspc_block_page_options_row').slideUp(100);
        }
    });
}
