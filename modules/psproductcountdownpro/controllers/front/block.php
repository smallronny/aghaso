<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2020 Presta.Site
 * @license   LICENSE.txt
 */

class PSProductCountdownProBlockModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;
    public $display_column_right = false;
    public $php_self = '';
    public $block = null;
    public $module;

    public function __construct()
    {
        parent::__construct();

        if ($this->module->getPSVersion() == 1.7 && class_exists('ProductListingFrontControllerCore')) {
            require_once _PS_MODULE_DIR_.'psproductcountdownpro/classes/PSPCProductSearchProvider.php';
            require_once _PS_MODULE_DIR_.'psproductcountdownpro/classes/PSPCProductListingFrontController.php';
        }
    }

    public function init()
    {
        $block = $this->loadBlock();
        if (!$block || !$block->page || !$block->active) {
            Controller::getController('PageNotFoundController')->run();
            exit;
        }

        parent::init();
    }

    public function initContent()
    {
        $this->module->slashRedirect();
        $this->module->langRedirect();
        parent::initContent();

        $block = $this->loadBlock();
        if (!$block) {
            return false;
        }

        $nb_products = $block->getProductsFront(false, true);

        $range = 2;
        if ($this->getPSVersion() <= 1.6) {
            $p = Tools::getValue('p', 1);
            $n = Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'));
        } else {
            $p = Tools::getValue('page', 1);
            $n = Tools::getValue('resultsPerPage', Configuration::get('PS_PRODUCTS_PER_PAGE'));
        }
        $start = (int)($p - $range);
        if ($start < 1) {
            $start = 1;
        }
        
        $pages_nb = ceil((int)$nb_products / (int)$n);
        $stop = (int)($p + $range);
        if ($stop > $pages_nb) {
            $stop = (int)($pages_nb);
        }

        $this_link = $block->getUrl();

        // Product listing for PS1.7
        if ($this->getPSVersion() == 1.7) {
            $listing_controller = new PSPCProductListingFrontController();
            $listing_controller->custom_total = $nb_products;
            $order_by = null;
            $order_way = null;
            if ($encodedSortOrder = Tools::getValue('order')) {
                $sort_order = PrestaShop\PrestaShop\Core\Product\Search\SortOrder::newFromString(
                    $encodedSortOrder
                );
                $order_by = $sort_order->getField();
                $order_way = $sort_order->getDirection();
            }
            $listing_controller->custom_products =
                $block->getProductsFront(false, false, $p, $n, $order_by, $order_way, false);

            if ($this->ajax) {
                ob_end_clean();
                header('Content-Type: application/json');
                $listing = $listing_controller->getAjaxProductListing($block->title);
                if (method_exists($this, 'ajaxRender')) {
                    $this->ajaxRender(json_encode($listing));
                } elseif (method_exists($this, 'ajaxDie')) {
                    $this->ajaxDie(json_encode($listing));
                }

                return false;
            } else {
                $listing = $listing_controller->getProductListing($block->title);
            }

            $this->context->smarty->assign(array(
                'listing' => $listing,
            ));
        } else {
            $order_by = Tools::getValue('orderby');
            $order_way = Tools::getValue('orderway');
            $products = $block->getProductsFront(false, false, $p, $n, $order_by, $order_way);
            $this->productSort();
            $this->pagination();
            $this->context->smarty->assign(array(
                'pspc_products' => $products,
            ));
        }

        $this->context->smarty->assign(array(
            'psv' => $this->module->getPSVersion(),
            'psvd' => $this->module->getPSVersion(true),
            'pspc_block' => $block,
            'pspc_theme_dir' => _PS_THEME_DIR_,
            'meta_title' => ($block->meta_title ? $block->meta_title : $block->title),
            'meta_description' => ($block->meta_desc ? $block->meta_desc : $block->title),
            'meta_keywords' => ($block->meta_keywords ? $block->meta_keywords : $block->title),
            'navigationPipe' => Configuration::get('PS_NAVIGATION_PIPE'),
            'nb_products' => $nb_products,
            'p' => $p,
            'n' => $n,
            'start' => $start,
            'stop' => $stop,
            'products_per_page' => Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE')),
            'page_name' => 'product',
            'pages_nb' => $pages_nb,
            'request' => $this_link,
        ));

        if ($this->module->getPSVersion() <= 1.6) {
            $this->setTemplate('block_page.tpl');
        } else {
            $this->setTemplate('module:psproductcountdownpro/views/templates/front/block_page17.tpl');
        }
    }

    public function getBreadcrumbLinks()
    {
        $block = $this->loadBlock();

        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = array(
            'title' => ($block->title ? $block->title : $this->l('Flash sale')),
            'url' => $this->block->getUrl(),
        );

        return $breadcrumb;
    }

    protected function loadBlock()
    {
        if (!$this->block) {
            $link_rewrite = Tools::getValue('link_rewrite');
            $block = PSPCBlock::getBlockByLinkRewrite($link_rewrite);
            if (!$block) {
                return null;
            }
            $this->block = $block;
            $this->block->product_count = 9999999;
        }

        return $this->block;
    }

    protected function getPSVersion()
    {
        return $this->module->getPSVersion();
    }

    public function setMedia()
    {
        parent::setMedia();

        if ($this->getPSVersion() <= 1.6) {
            $this->context->controller->addCSS(array(
                _THEME_CSS_DIR_.'category.css'     => 'all',
                _THEME_CSS_DIR_.'product_list.css' => 'all',
            ));
        }
    }

    protected static function getProductId($product)
    {
        if (is_object($product) && method_exists($product, 'getId')) {
            return $product->getId();
        } elseif (is_object($product) && property_exists($product, 'id')) {
            return $product->id;
        } elseif (is_array($product) && isset($product['id_product'])) {
            return $product['id_product'];
        } else {
            return false;
        }
    }
}
