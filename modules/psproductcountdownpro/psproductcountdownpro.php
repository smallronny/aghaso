<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2017 Presta.Site
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
    require_once(_PS_MODULE_DIR_ . 'psproductcountdownpro/classes/PSPCModule16.php');
} else {
    require_once(_PS_MODULE_DIR_ . 'psproductcountdownpro/classes/PSPCModule17.php');
}

require_once(_PS_MODULE_DIR_ . 'psproductcountdownpro/classes/PSPC.php');
require_once(_PS_MODULE_DIR_ . 'psproductcountdownpro/classes/PSPCBlock.php');

class PSProductCountdownPro extends PSPCModule
{
    protected $html;
    protected $errors = array();
    protected $clear_cache = true;
    public $settings_prefix = 'PSPC_';

    public $theme;
    public $product_list_position;
    public $product_position;
    public $bg_color;
    public $compact_view;
    public $background_image;
    public $border_color;
    public $text_color;
    public $label_color;
    public $seconds_color;
    public $activate_all_special;
    public $personal_specials;
    public $personal_specials_only_emp;
    public $personal_hours;
    public $personal_hours_restart;
    public $randomize_personal;
    public $border_radius;
    public $base_font_size;
    public $base_font_size_list;
    public $base_font_size_col;
    public $vertical_align;
    public $hide_after_end;
    public $hide_expired;
    public $highlight;
    public $colors;
    public $show_promo_text;
    public $promo_text;
    public $promo_side;
    public $adjust_positions;
    public $show_colon;
    public $custom_js;
    public $custom_css;
    public $with_subcategories;
    public $display_days_before_end;

    public function __construct()
    {
        $this->name = 'psproductcountdownpro';
        $this->tab = 'front_office_features';
        $this->version = '2.2.1';
        $this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => '1.7.99.99');
        $this->author = 'PrestaSite';
        $this->bootstrap = true;
        $this->module_key = '595eb298fd53616159b3364d0ba87a4a';
        $this->confirmUninstall = $this->l('Are you sure? All module data will be PERMANENTLY DELETED.');

        parent::__construct();
        $this->loadSettings();

        $this->displayName = $this->l('Product countdown PRO');
        $this->description = $this->l('Countdown timer for products.');
    }

    public function install()
    {
        if (!parent::install() || !$this->installHooks()) {
            return false;
        }

        // Create tables
        $this->installDB();

        //default values:
        $this->installDefaultSettings();

        // Disable free version of this module in order to prevent double countdowns
        $free_version = Module::getInstanceByName('psproductcountdown');
        if ($free_version) {
            $free_version->disable();
        }

        // Update from v1 to v2 and migrate all data
        $this->migrateTo20(true);

        // Generate settings CSS
        $this->loadSettings();
        $this->regenerateCSS();

        // set friendly URL for the controller
        $this->installMetaPage();

        return true;
    }

    public function installMetaPage()
    {
        try {
            $page = 'module-' . $this->name . '-' . 'block';
            $id_meta = Db::getInstance()->getValue(
                'SELECT `id_meta` FROM ' . _DB_PREFIX_ . 'meta WHERE `page` = "' . pSQL($page) . '"'
            );
            if ($id_meta) {
                $meta = new Meta($id_meta);
                $save = false;
                foreach ($meta->url_rewrite as &$meta_url) {
                    if (!$meta_url) {
                        $meta_url = 'countdown';
                        $save = true;
                    }
                }
                if ($save) {
                    $meta->save();
                }
            }
        } catch (Exception $e) {
            // do nothing
        }
    }

    public function installHooks()
    {
        if (!$this->registerHook('pspc')
            || !$this->registerHook('pspcBlock')
            || !$this->registerHook('PSProductCountdown')
            || !$this->registerHook('displayLeftColumnProduct')
            || !$this->registerHook('displayRightColumnProduct')
            || !$this->registerHook('displayFooterProduct')
            || !$this->registerHook('displayProductAdditionalInfo')
            || !$this->registerHook('displayProductButtons')
            || !$this->registerHook('displayProductPriceBlock')
            || !$this->registerHook('displayProductListReviews')
            || !$this->registerHook('displayBackOfficeHeader')
            || !$this->registerHook('actionAdminControllerSetMedia')
            || !$this->registerHook('header')
            || !$this->registerHook('moduleRoutes')
            || !$this->registerHook('displayAdminProductsExtra')) {
            return false;
        }

        return true;
    }

    protected function installDB()
    {
        $install_queries = $this->getDbTables();
        foreach ($install_queries as $query) {
            if (!Db::getInstance()->execute($query)) {
                return false;
            }
        }

        return true;
    }

    protected function getDbTables()
    {
        return array(
            'pspc' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc` (
                `id_pspc` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `from` DATETIME,
                `to` DATETIME,
                `system_name` VARCHAR(255),
                `reduction` FLOAT(20, 6),
                `reduction_type` ENUM("amount", "percentage"),
                `reduction_tax` TINYINT,
                `personal` TINYINT(1) DEFAULT 0,
                `personal_hours` DECIMAL(10, 4),
                `hours_restart` DECIMAL(10, 4),
                `action_start` VARCHAR(64),
                `action_start_done` TINYINT(1) DEFAULT 0,
                `action_start_qty` INT(6),
                `action_end` VARCHAR(64),
                `action_end_done` TINYINT(1) DEFAULT 0,
                `action_end_qty` INT(6),
                `all_groups` TINYINT(1) DEFAULT 1,
                PRIMARY KEY (`id_pspc`),
                INDEX (`action_start`, `action_start_done`, `from`),
                INDEX (`action_end`, `action_end_done`, `to`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_lang' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_lang` (
                `id_pspc` INT(11) NOT NULL,
                `id_lang` INT(11) NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                UNIQUE (`id_pspc`, `id_lang`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_shop' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_shop` (
                `id_pspc` INT(11) NOT NULL,
                `id_shop` INT(11) NOT NULL,
                `active` TINYINT(1) DEFAULT 1,
                UNIQUE (`id_pspc`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_object' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_object` (
                `id_pspc_object` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_pspc` INT(11) NOT NULL,
                `type` VARCHAR(64) NOT NULL,
                `id_object` INT(11) UNSIGNED NOT NULL,
                `id_product_attribute` INT(11) UNSIGNED NOT NULL DEFAULT 0,
                PRIMARY KEY (`id_pspc_object`),
                UNIQUE (`id_pspc`, `type`, `id_object`, `id_product_attribute`),
                INDEX (`id_pspc`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_relations' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_relations` (
                `id_pspc` INT(11) NOT NULL,
                `id_product` INT(11) NOT NULL,
                `id_product_attribute` INT(11) NOT NULL,
                `id_specific_price` INT(11) NOT NULL,
                `id_shop` INT(11) NOT NULL,
                `id_group` INT(11) NOT NULL,
                UNIQUE (`id_pspc`, `id_product`, `id_product_attribute`, `id_specific_price`, `id_shop`, `id_group`),
                INDEX (`id_pspc`),
                INDEX (`id_pspc`, `id_product`),
                INDEX (`id_pspc`, `id_product`, `id_product_attribute`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_block' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_block` (
                `id_pspc_block` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `hook` VARCHAR(255),
                `product_count` INT(11) UNSIGNED,
                `sort` VARCHAR(255),
                `source_all` TINYINT(1) DEFAULT 0,
                `source_specific_prices` TINYINT(1) DEFAULT 0,
                `source_pspc` TINYINT(1) DEFAULT 0,
                `page` TINYINT(1) DEFAULT 0,
                PRIMARY KEY (`id_pspc_block`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_block_shop' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_block_shop` (
                `id_pspc_block` INT(11) NOT NULL,
                `id_shop` INT(11) NOT NULL,
                `active` TINYINT(1) DEFAULT 1,
                UNIQUE (`id_pspc_block`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_block_lang' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_block_lang` (
                `id_pspc_block` INT(11) UNSIGNED NOT NULL,
                `id_lang` INT(11) UNSIGNED NOT NULL,
                `title` VARCHAR(255),
                `link_rewrite` VARCHAR(255),
                `meta_title` VARCHAR(255),
                `meta_desc` VARCHAR(255),
                `meta_keywords` VARCHAR(255),
                `desc` TEXT,
                UNIQUE (`id_pspc_block`, `id_lang`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_block_source' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_block_source` (
                `id_pspc_block_source` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_pspc_block` INT(1) UNSIGNED NOT NULL,
                `id_pspc` INT(11) UNSIGNED,
                PRIMARY KEY (`id_pspc_block_source`),
                INDEX (`id_pspc_block`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
            'pspc_group' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pspc_group` (
                `id_pspc` INT(11) NOT NULL,
                `id_group` INT(11) NOT NULL,
                UNIQUE (`id_pspc`, `id_group`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
        );
    }

    protected function installDefaultSettings()
    {
        foreach ($this->getSettings() as $item) {
            if ($item['type'] == 'html') {
                continue;
            }
            $item_name = Tools::strtoupper($item['name']);
            if (isset($item['default']) && (Configuration::get($this->settings_prefix . $item_name) === false)) {
                if (isset($item['lang']) && $item['lang']) {
                    $lang_value = array();
                    $set = false;
                    foreach (Language::getLanguages() as $lang) {
                        $lang_value[$lang['id_lang']] = $item['default'];
                        if (Configuration::get($this->settings_prefix . $item_name, $lang['id_lang']) !== false) {
                            $set = true;
                        }
                    }
                    if (!$set && sizeof($lang_value)) {
                        Configuration::updateValue($this->settings_prefix . $item_name, $lang_value, true);
                    }
                } elseif ($item['type'] == 'colors') {
                    if (is_array($item['default'])) {
                        foreach ($item['default'] as $key => $color) {
                            if (Configuration::get($this->settings_prefix . $item_name . '_' . $key) === false) {
                                Configuration::updateValue(
                                    $this->settings_prefix . $item_name . '_' . $key,
                                    $color['default']
                                );
                            }
                        }
                    }
                } else {
                    Configuration::updateValue($this->settings_prefix . $item_name, $item['default']);
                }
            }
        }
        Configuration::updateValue($this->settings_prefix . 'VERTICAL_ALIGN', 'bottom');
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        // delete all timers, specific prices etc
        PSPC::deleteAll();

        // drop tables
        foreach ($this->getDbTables() as $table_name => $query) {
            Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . pSQL($table_name) . '`;');
        }
        // reset settings
        Db::getInstance()->execute(
            'DELETE FROM `' . _DB_PREFIX_ . 'configuration` WHERE `name` LIKE "'.pSQL($this->settings_prefix).'%";'
        );

        return true;
    }

    protected function loadSettings()
    {
        foreach ($this->getSettings() as $item) {
            if ($item['type'] == 'html') {
                continue;
            }

            $name = Tools::strtolower($item['name']);
            $conf_name = Tools::strtoupper($item['name']);
            if (isset($item['lang']) && $item['lang']) {
                $this->$name = array();
                foreach (Language::getLanguages() as $language) {
                    $this->{$name}[$language['id_lang']] = Configuration::get(
                        $this->settings_prefix . $conf_name,
                        $language['id_lang']
                    );
                }
            } elseif ($item['type'] == 'colors') {
                $this->$name = array();
                foreach ($this->getColorsData() as $i => $color) {
                    $this->{$name}[$i] = Configuration::get(
                        $this->settings_prefix . $conf_name . '_' . $i
                    );
                }
            } else {
                $this->$name = Configuration::get(
                    $this->settings_prefix .
                    $conf_name
                );
            }
        }
    }

    public function getSettings($render_html = false)
    {
        $settings = array(
            array(
                'type' => 'select',
                'name' => 'PRODUCT_POSITION',
                'label' => $this->l('Position at the product page:'),
                'class' => 't',
                'options' => array(
                    'query' => $this->getProductPageSelectOptions(),
                    'id' => 'id_option',
                    'name' => 'name',
                ),
                'default' => 'displayProductPriceBlock',
                'default_15' => 'displayLeftColumnProduct',
                'validate' => 'isString',
            ),
            array(
                'type' => 'list_position',
                'addon' => array(
                    'name' => 'VERTICAL_ALIGN',
                    'options' => array(
                        'query' => $this->getVerticalAlignTypes(),
                        'id' => 'id_option',
                        'name' => 'name',
                    ),
                ),
                'name' => 'PRODUCT_LIST_POSITION',
                'label' => $this->l('Position in the product list:'),
                'class' => 't',
                'options' => array(
                    'query' => $this->getProductListSelectOptions(),
                    'id' => 'id_option',
                    'name' => 'name',
                ),
                'default' => 'over_img',
                'validate' => 'isString',
                'hint' => $this->l('Please use a custom hook if your theme does not support standard hooks OR if you want to place a countdown in the non-standard place. See the "Additional Instructions" block for the reference.'),
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'ACTIVATE_ALL_SPECIAL',
                'label' => $this->l('Show countdown for all products with specific prices:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'activate_all_special_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'activate_all_special_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('Activate this module for all products with specific prices OR activate it manually for chosen products. Specific prices will be used only if they have appropriate availability dates.'),
                'default' => 1,
                'validate' => 'isInt',
            ),
            array(
                'type' => 'html',
                'name' => '',
                'label' => '',
                'html_content' => $this->renderMoreOptionsBtn(
                    $this->l('Show more options'),
                    'pspc_more_options_row',
                    $render_html,
                    'pspc_more_main_options'
                ),
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'PERSONAL_SPECIALS',
                'label' => $this->l('Set custom time for products with specific prices:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'personal_specials_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'personal_specials_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('Display countdown for each customer for some specified time since first viewing of related product.'),
                'default' => 0,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'PERSONAL_SPECIALS_ONLY_EMP',
                'label' => $this->l('Set custom time only for specific prices without end date:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'personal_specials_oe_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'personal_specials_oe_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'default' => 1,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => 'text',
                'name' => 'PERSONAL_HOURS',
                'label' => $this->l('Custom time:'),
                'default' => '24',
                'suffix' => $this->l('hours'),
                'col' => 1,
                'validate' => 'isFloat',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => 'text',
                'name' => 'PERSONAL_HOURS_RESTART',
                'label' => $this->l('Restart after:'),
                'default' => '24',
                'suffix' => $this->l('hours'),
                'col' => 1,
                'validate' => 'isInt',
                'hint' => $this->l('Restart personal countdown after its end. Set to empty to disable this feature.'),
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'RANDOMIZE_PERSONAL',
                'label' => $this->l('Randomize personal timers'),
                'hint' => $this->l('Slightly randomize personal countdown duration to make it look more natural'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'randomize_personal_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'randomize_personal_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'default' => 0,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'HIDE_AFTER_END',
                'label' => $this->l('Hide a countdown after its end:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'hide_after_end_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'hide_after_end_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('Hide a countdown as soon as it reaches 0 (animation)'),
                'default' => 1,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'HIDE_EXPIRED',
                'label' => $this->l('Hide expired countdown timers:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'hide_expired_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'hide_expired_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('Do not show expired countdown timers'),
                'default' => 1,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'SHOW_PROMO_TEXT',
                'label' => $this->l('Show promo text'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'show_promo_text_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'show_promo_text_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('"Offer ends in"'),
                'default' => 1,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => 'text',
                'name' => 'PROMO_TEXT',
                'label' => $this->l('Promo text'),
                'lang' => true,
                'hint' => $this->l('A promo text displayed above a countdown. Leave empty for using default text "Offer ends in:"'),
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'ADJUST_POSITIONS',
                'label' => $this->l('Adjust countdown position:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'adjust_positions_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'adjust_positions_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('Try to toggle this option if you have issues with countdown displaying.'),
                'default' => 1,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'WITH_SUBCATEGORIES',
                'label' => $this->l('Include subcategories:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'adjust_positions_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'adjust_positions_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('When creating timers for a category of products, search products also in its subcategories'),
                'default' => 0,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => 'text',
                'name' => 'DISPLAY_DAYS_BEFORE_END',
                'label' => $this->l('Show countdown only before its end for:'),
                'hint' => $this->l('You can hide too long timers. They will be visible only X days before expiration. And before that they will be shown as a simple text: Offer ends on _DATE_'),
                'desc' => $this->l('Enter 0 to disable this feature'),
                'default' => '0',
                'suffix' => $this->l('days'),
                'col' => 1,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => 'textarea',
                'name' => 'CUSTOM_JS',
                'label' => $this->l('Custom JS:'),
                'hint' => $this->l('Here you can write JavaScript code that will be executed before displaying a countdown. Useful for repositioning a countdown. Parameters: $pspc - jQuery object of a particular countdown, $pspc_container - a container of the countdown.'),
                'desc' => $this->l('Only for developers.'),
                'validate' => 'isString',
                'resize' => true,
                'cols' => '',
                'rows' => '',
                'form_group_class' => 'pspc_more_options_row',
            ),
            array(
                'type' => 'html',
                'name' => '',
                'label' => '',
                'html_content' => $this->renderSettingsDivider($render_html),
            ),
            array(
                'type' => 'theme',
                'name' => 'THEME',
                'label' => $this->l('Theme:'),
                'hint' => $this->l('Here you can choose a theme that best suits your shop'),
                'class' => 't',
                'values' => $this->getThemesOptions(),
                'default' => '1-simple.css',
                'col' => 9,
                'validate' => 'isString',
            ),
            array(
                'type' => 'colors',
                'name' => 'COLORS',
                'label' => $this->l('Colors:'),
                'hint' => $this->l('Here you can configure countdown colors'),
                'default' => $this->getColorsData(),
                'colors_data' => $this->getColorsData(),
                'validate' => 'isCleanHtml',
            ),
            array(
                'type' => 'radio',
                'name' => 'PROMO_SIDE',
                'label' => $this->l('Place the promo text'),
                'hint' => $this->l('Choose at which side of the timer will be displayed the text'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'promo_side_left',
                        'value' => 'left',
                        'label' => $this->l('Before timer'),
                    ),
                    array(
                        'id' => 'promo_side_right',
                        'value' => 'right',
                        'label' => $this->l('After timer'),
                    ),
                ),
                'default' => 'left',
                'validate' => 'isString',
                'form_group_class' => 'pspc_custom_option pspc_options_promo_side',
            ),
            array(
                'type' => 'html',
                'name' => '',
                'label' => '',
                'html_content' => $this->renderMoreOptionsBtn(
                    $this->l('More appearance options'),
                    'pspc_more_app_options_row',
                    $render_html,
                    'pspc_more_app_options'
                ),
            ),
            array(
                'type' => 'font_size',
                'name' => 'BASE_FONT_SIZE',
                'label' => $this->l('Font size for the product page:'),
                'hint' => $this->l('You can change countdown size by changing this option'),
                'default' => '1',
                'suffix' => 'px',
                'col' => 1,
                'validate' => 'isFloat',
                'form_group_class' => 'pspc_more_app_options_row',
            ),
            array(
                'type' => 'font_size',
                'name' => 'BASE_FONT_SIZE_LIST',
                'label' => $this->l('Font size for product list:'),
                'hint' => $this->l('You can change countdown size by changing this option'),
                'default' => '1',
                'suffix' => 'px',
                'col' => 1,
                'validate' => 'isFloat',
                'form_group_class' => 'pspc_more_app_options_row',
            ),
            array(
                'type' => 'font_size',
                'name' => 'BASE_FONT_SIZE_COL',
                'label' => $this->l('Font size for left and right columns:'),
                'hint' => $this->l('You can change countdown size by changing this option'),
                'default' => '0.8',
                'suffix' => 'px',
                'col' => 1,
                'validate' => 'isFloat',
                'form_group_class' => 'pspc_more_app_options_row',
            ),
            array(
                'type' => 'select',
                'name' => 'HIGHLIGHT',
                'label' => $this->l('Highlight:'),
                'class' => 't',
                'options' => array(
                    'query' => $this->getHighlightSelectOptions(),
                    'id' => 'id_option',
                    'name' => 'name',
                ),
                'default' => 'seconds',
                'validate' => 'isString',
                'hint' => $this->l('Highlight the needed part of countdown'),
                'form_group_class' => 'pspc_more_app_options_row pspc_custom_option pspc_options_highlight',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'COMPACT_VIEW',
                'label' => $this->l('Compact view:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'compact_view_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'compact_view_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('More compact view.'),
                'default' => 0,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_app_options_row pspc_custom_option pspc_options_compact',
            ),
            'bg_img' => array(
                'type' => 'file',
                'name' => 'BACKGROUND_IMAGE',
                'label' => $this->l('Background image:'),
                'ajax' => false,
                'default' => '',
                'validate' => 'isString',
                'form_group_class' => 'pspc_more_app_options_row pspc_custom_option pspc_options_bg',
            ),
            array(
                'type' => 'text',
                'name' => 'BORDER_RADIUS',
                'label' => $this->l('Border radius'),
                'suffix' => 'px',
                'default' => '0',
                'col' => 1,
                'hint' => $this->l('Set to 0 to disable this feature.'),
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_app_options_row pspc_custom_option pspc_options_radius',
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'switch',
                'name' => 'SHOW_COLON',
                'label' => $this->l('Show colon:'),
                'class' => 't',
                'values' => array(
                    array(
                        'id' => 'show_colon_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'show_colon_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'hint' => $this->l('Show colon between digits'),
                'default' => 0,
                'validate' => 'isInt',
                'form_group_class' => 'pspc_more_app_options_row pspc_custom_option pspc_options_colon',
            ),
            array(
                'type' => 'textarea',
                'name' => 'CUSTOM_CSS',
                'label' => $this->l('Custom CSS:'),
                'hint' => $this->l('Add your styles directly in this field without editing files'),
                'desc' => $this->l('Example: .psproductcountdown { font-style: italic; }'),
                'validate' => 'isCleanHtml',
                'resize' => true,
                'cols' => '',
                'rows' => '',
                'form_group_class' => 'pspc_more_app_options_row',
            ),
        );

        if ($this->background_image) {
            $settings['bg_img']['files'] = array(
                array(
                    'image' => '<img src="'.$this->_path.'upload/'.$this->background_image.'" />',
                    'type' => 'image',
                    'delete_url' => 'index.php?controller=AdminModules&configure=' .
                        $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&remove_bg=1'
                )
            );
        }

        if ($this->getPSVersion() < 1.6) {
            foreach ($settings as &$item) {
                $desc = isset($item['desc']) ? $item['desc'] : '';
                $hint = isset($item['hint']) ? $item['hint'] . '<br/>' : '';
                $item['desc'] = $hint . $desc;
                $item['hint'] = '';
            }
        }

        return $settings;
    }

    public function getContent()
    {
        $this->html = '';

        if (!$this->active) {
            $this->html .= $this->displayWarning(
                $this->l('The module is deactivated. Please activate it for proper working.')
            );
        }

        $this->html .= $this->postProcess();

        $tabs = array(
            array(
                'name' => $this->l('Countdown timers'),
                'content' => array(
                    'pspc-countdown-list' => $this->renderCountdownList()
                ),
            ),
            array(
                'name' => $this->l('Blocks'),
                'content' => $this->renderBlocks(),
            ),
            array(
                'name' => $this->l('Settings'),
                'content' => $this->renderForm(),
            ),
            array(
                'name' => $this->l('Additional instructions'),
                'content' => $this->renderAdditionalInstructions().$this->renderFeedbackBlock(),
            ),
        );

        $this->context->smarty->assign(array(
            'pspc_tabs' => $tabs,
        ));
        $this->html .= $this->context->smarty->fetch(
            $this->local_path . 'views/templates/admin/tabs.tpl'
        );

        return $this->html;
    }

    protected function postProcess()
    {
        $html = '';
        $this->errors = array();
        $settings_updated = false;
        $confirmation = 6;
        
        // Check if this is an ajax call / PS1.5
        if ($this->getPSVersion() < 1.6 && Tools::getIsset('ajax')
            && Tools::getValue('ajax') && Tools::getValue('action')) {
            if (is_callable(array($this, 'ajaxProcess'.Tools::getValue('action')))) {
                call_user_func(array($this, 'ajaxProcess' . Tools::getValue('action')));
            }
            die();
        }

        if (Tools::isSubmit('submitModule')) {
            //saving settings:
            $settings = $this->getSettings();
            $extensions = array('.png', '.jpeg', '.gif', '.jpg');

            foreach ($settings as $item) {
                if ($item['type'] == 'html'
                    || (isset($item['lang']) && $item['lang'] == true)
                    || $item['type'] == 'colors') {
                    continue;
                }
                // Upload files:
                if ($item['type'] == 'file') {
                    $file_attachment = $this->fileAttachment($item['name']);
                    if ($file_attachment) {
                        if (!empty($file_attachment['name']) && $file_attachment['error'] != 0) {
                            $this->errors[] = $this->l('An error occurred during the file-upload process.');
                        } elseif (!empty($file_attachment['name'])
                            && !in_array(Tools::strtolower(Tools::substr($file_attachment['name'], -4)), $extensions)
                            && !in_array(Tools::strtolower(Tools::substr($file_attachment['name'], -5)), $extensions)
                        ) {
                            $this->errors[] = $this->l('Bad file extension');
                        } elseif (!@getimagesize($file_attachment['tmp_name'])) {
                            $this->errors[] = $this->l('Invalid image.');
                        } else {
                            $path = _PS_MODULE_DIR_ . $this->name . '/upload/';
                            if (isset($file_attachment['rename']) && !empty($file_attachment['rename']) &&
                                @rename($file_attachment['tmp_name'], $path . basename($file_attachment['rename']))
                            ) {
                                // Delete old file
                                @unlink($path.basename(Configuration::get($this->settings_prefix . $item['name'])));

                                $filename = $file_attachment['rename'];
                                @chmod($path . basename($file_attachment['rename']), 0664);
                                Configuration::updateValue($this->settings_prefix . $item['name'], $filename);
                                $settings_updated = true;
                            } else {
                                $this->errors[] = $this->l('Unable to upload an image.');
                            }
                        }
                    }
                    // Saving other fields:
                } else {
                    if (Tools::isSubmit($item['name'])) {
                        $validated = true;
                        $val_method = $item['validate'];
                        if (Tools::strlen(Tools::getValue($item['name']))) {
                            // Validation:
                            if (Tools::strlen($val_method) && is_callable(array('Validate', $val_method))) {
                                $validated =
                                    call_user_func(array('Validate', $val_method), Tools::getValue($item['name']));
                            }
                        }
                        if ($validated) {
                            Configuration::updateValue(
                                $this->settings_prefix . $item['name'],
                                Tools::getValue($item['name']),
                                true
                            );
                            $settings_updated = true;
                        } else {
                            $label = trim($item['label'], ':');
                            $this->errors[] = sprintf($this->l('The "%s" field is invalid'), $label);
                        }
                    }
                    if (isset($item['addon']) && is_array($item['addon'])
                        && isset($item['addon']['name']) && $item['addon']['name']) {
                        $addon_name = $item['addon']['name'];
                        Configuration::updateValue(
                            $this->settings_prefix . $addon_name,
                            Tools::getValue($addon_name),
                            true
                        );
                    }
                }
            }

            // update colors field
            foreach ($settings as $item) {
                if ($item['type'] != 'colors') {
                    continue;
                }
                foreach ($this->getColorsData() as $i => $color) {
                    $iname = $item['name'] . '_' . $i;
                    if (Tools::isSubmit($iname)) {
                        $validated = true;
                        $val_method = (isset($item['validate']) ? $item['validate'] : '');

                        if (Tools::strlen(Tools::getValue($iname))) {
                            // Validation:
                            if (Tools::strlen($val_method) && is_callable(array('Validate', $val_method))) {
                                $validated =
                                    call_user_func(array('Validate', $val_method), Tools::getValue($iname));
                            }
                        }
                        if ($validated) {
                            try {
                                Configuration::updateValue(
                                    $this->settings_prefix . $iname,
                                    Tools::getValue($iname),
                                    true
                                );
                            } catch (Exception $e) {
                                // do nothing, maybe conf name length exception in PS15
                            }
                            $settings_updated = true;
                        } else {
                            $label = trim($item['label'], ':');
                            $this->errors[] = sprintf($this->l('The "%s" field is invalid'), $label);
                        }
                    }
                }
            }

            //update lang fields:
            $languages = Language::getLanguages();
            foreach ($settings as $item) {
                if (!(isset($item['lang']) && $item['lang'])) {
                    continue;
                }
                $val_method = (isset($item['validate']) ? $item['validate'] : '');
                $lang_value = array();
                foreach ($languages as $lang) {
                    if (Tools::isSubmit($item['name'] . '_' . $lang['id_lang'])) {
                        // Check if it's required but empty
                        if (isset($item['required']) && $item['required']
                            && !Tools::getValue($item['name'] . '_' . $lang['id_lang'])
                        ) {
                            $label = trim($item['label'], ':');
                            $this->errors[] = sprintf(
                                $this->l('The "%s" field is required and cannot be empty (%s)'),
                                $label,
                                $lang['name']
                            );
                            continue;
                        }
                        $validated = true;
                        if (Tools::strlen(Tools::getValue($item['name'] . '_' . $lang['id_lang']))) {
                            // Validation:
                            if (Tools::strlen($val_method) && is_callable(array('Validate', $val_method))) {
                                $validated = call_user_func(
                                    array('Validate', $val_method),
                                    Tools::getValue($item['name'] . '_' . $lang['id_lang'])
                                );
                            }
                        }
                        if ($validated) {
                            $lang_value[$lang['id_lang']] = Tools::getValue($item['name'] . '_' . $lang['id_lang']);
                            $settings_updated = true;
                        } else {
                            $label = trim($item['label'], ':');
                            $this->errors[] = sprintf($this->l('The "%s" field is invalid'), $label);
                        }
                    }
                }
                if (sizeof($lang_value)) {
                    Configuration::updateValue($this->settings_prefix . $item['name'], $lang_value, true);
                }
            }
        }

        // Remove background image
        if (Tools::getValue('remove_bg')) {
            @unlink(_PS_MODULE_DIR_.$this->name.'/upload/'.$this->background_image);
            Configuration::updateValue($this->settings_prefix . 'BACKGROUND_IMAGE', '');
            $settings_updated = 1;
        }

        // Reset colors
        if (Tools::getValue('reset_colors')) {
            $this->resetColors();
            $settings_updated = 1;
        }

        // Delete pspc
        if (Tools::isSubmit('deletepspc')) {
            $id_pspc = Tools::getValue('id_pspc');
            if ($id_pspc) {
                $pspc = new PSPC($id_pspc);
                $pspc->delete();
                $this->clearSmartyCache();
            }
        }

        // Delete block
        if (Tools::isSubmit('deletepspc_block')) {
            $id_block = Tools::getValue('id_pspc_block');
            $block = new PSPCBlock($id_block);
            $block->delete();
            $settings_updated = true;
            $confirmation = 1;
        }

        $this->loadSettings();

        if ($this->hide_after_end) {
            Configuration::updateValue($this->settings_prefix.'HIDE_EXPIRED', 1);
        }

        // regen custom css
        $this->regenerateCSS();

        if ($settings_updated && !sizeof($this->errors)) {
            // Clear smarty cache
            $this->clearSmartyCache();
            // redirect after save
            $token = Tools::getAdminTokenLite('AdminModules');
            $redirect_url = 'index.php?controller=AdminModules&configure=' .
                $this->name . '&token=' . $token . '&conf='.$confirmation;
            Tools::redirectAdmin($redirect_url);
        } elseif (sizeof($this->errors)) {
            foreach ($this->errors as $err) {
                $html .= $this->displayError($err);
            }
        }

        return $html;
    }

    protected function fileAttachment($input = 'fileUpload')
    {
        $file_attachment = null;
        if (isset($_FILES[$input]['name']) && !empty($_FILES[$input]['name']) && !empty($_FILES[$input]['tmp_name'])) {
            $file_attachment['rename'] = uniqid().Tools::strtolower(Tools::substr($_FILES[$input]['name'], -5));
            $file_attachment['tmp_name'] = $_FILES[$input]['tmp_name'];
            $file_attachment['name']     = $_FILES[$input]['name'];
            $file_attachment['mime']     = $_FILES[$input]['type'];
            $file_attachment['error']    = $_FILES[$input]['error'];
            $file_attachment['size']     = $_FILES[$input]['size'];
        }

        return $file_attachment;
    }

    protected function renderCountdownList()
    {
        $table = 'pspc';
        $helper = $this->createListHelper($table, 'id_pspc');
        $helper->actions = array('edit', 'delete');
        $helper->title = $this->l('Countdown timers');
        $fields_list = array(
            'system_name' => array(
                'title' => $this->l('Campaign'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'remove_onclick' => true,
                'badges' => array('hours_restart', 'groups'),
            ),
            'items' => array(
                'title' => $this->l('Items'),
                'type' => 'text',
                'search' => true,
                'orderby' => false,
                'remove_onclick' => true,
                'show_item_list' => true,
                'width' => '300',
                'class' => 'pspc-td-items-toggle',
            ),
            'name' => array(
                'title' => $this->l('Promo text'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'remove_onclick' => true,
            ),
            'from' => array(
                'title' => $this->l('From'),
                'type' => 'text',
                'search' => false,
                'orderby' => true,
                'value_class' => 'pspc-datetime-utc',
                'remove_onclick' => true,
                'badges' => array('action_start'),
                'badge_text' => array('action_start' => $this->getActionsStart()),
            ),
            'to' => array(
                'title' => $this->l('To'),
                'type' => 'text',
                'search' => false,
                'orderby' => true,
                'value_class' => 'pspc-datetime-utc',
                'remove_onclick' => true,
                'badges' => array('action_end'),
                'badge_text' => array('action_end' => $this->getActionsEnd()),
            ),
            'reduction' => array(
                'title' => $this->l('Discount'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'remove_onclick' => true,
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'type' => 'pspc_status',
                'search' => false,
                'orderby' => true,
                'remove_onclick' => true,
            ),
        );

        // if multishop
        if (Shop::isFeatureActive()) {
            $fields_list['shops'] = array(
                'title' => $this->l('Shops'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            );
        }

        $content =
            $this->getItemsBO(false, $helper->orderBy, $helper->orderWay, $helper->page, $helper->n, $fields_list, $table);
        $helper->listTotal =
            $this->getItemsBO(true, $helper->orderBy, $helper->orderWay, $helper->page, $helper->n, $fields_list, $table);

        $types = array(
            'product' => array($this->l('%s product'), $this->l('%s products')),
            'category' => array($this->l('%s category'), $this->l('%s categories')),
            'manufacturer' => array($this->l('%s manufacturer'), $this->l('%s manufacturers')),
        );
        foreach ($content as &$row) {
            $row['system_name'] = ($row['system_name'] ? $row['system_name'] : '#'.$row['id_pspc']);

            $objects = array();
            $items_count = 0;
            foreach ($types as $type => $text) {
                $count = PSPC::getObjectsCount($row['id_pspc'], $type);
                $items_count += $count;
                if ($count) {
                    $objects[] = ($count == 1 ? sprintf($text[0], $count) : sprintf($text[1], $count));
                }
            }
            $row['items_count'] = $items_count;
            $row['items'] = implode(', ', $objects);

            if ((float)$row['reduction'] > 0) {
                $row['reduction'] = ($row['reduction_type'] == 'amount'
                    ? Tools::displayPrice($row['reduction'])
                    : ($row['reduction'] * 1) . '%');
            } else {
                $row['reduction'] = '';
            }

            if (strpos($row['to'], '0000-00-00') !== false) {
                $row['to'] = '';
            }
            if (strpos($row['from'], '0000-00-00') !== false) {
                $row['from'] = '';
            }

            if ($row['personal']) {
                $row['active'] = -3;
            }

            $row['form'] = true;

            if (Shop::isFeatureActive()) {
                $shops = PSPC::getShopsStatic($row['id_pspc'], false);
                $shops_names = array();
                foreach ($shops as $id_shop) {
                    $shops_names[] = $this->getShopName($id_shop);
                }
                $row['shops'] = implode(', ', $shops_names);
            }
        }

        if (Tools::getValue('submitFilter'.$table) == 0) {
            $this->context->smarty->assign(array(
                'filters_has_value' => false,
            ));
        }

        $this->context->smarty->assign(array(
            'psv' => $this->getPSVersion(),
            'psvd' => $this->getPSVersion(true),
            'link' => $this->context->link,
            'form_id' => $table,
            'preTable' => $this->renderCountdownForm(),
            'icon' => 'icon-cogs',
            'pspc_list_class' => 'pspc-countdown-list',
            'pspc_colspan' => (count($content) > 1 ? count($fields_list) + 2 : count($fields_list) + 1),
            'pspc_module' => $this,
        ));

        return $helper->generateList($content, $fields_list);
    }

    protected function renderAdditionalInstructions()
    {
        $this->context->smarty->assign(array(
            'psv' => $this->getPSVersion(),
            'pspc_ps_theme' => _THEME_NAME_
        ));

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/instructions.tpl');
    }

    protected function renderFeedbackBlock()
    {
        $this->context->smarty->assign(array(
            'psv' => $this->getPSVersion(),
            'module_path' => $this->_path,
        ));

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/feedback.tpl');
    }

    protected function renderForm()
    {
        $settings = $this->getSettings(true);
        $field_forms = array(
            array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Settings'),
                        'icon' => 'icon-cogs'
                    ),
                    'input' => $settings,
                    'submit' => array(
                        'title' => $this->l('Save'),
                    )
                ),
            ),
        );

        $helper = $this->createFormHelper($field_forms, 'Module');

        foreach ($settings as $item) {
            if ($item['type'] == 'html' || $item['type'] == 'hidden') {
                continue;
            }
            $name = Tools::strtoupper($item['name']);
            if (isset($item['lang']) && $item['lang']) {
                foreach (Language::getLanguages() as $language) {
                    $helper->tpl_vars['fields_value'][$item['name']][$language['id_lang']] = Configuration::get(
                        $this->settings_prefix . $name,
                        $language['id_lang']
                    );
                }
            } elseif ($item['type'] == 'colors') {
                foreach ($this->getColorsData() as $i => $color) {
                    $helper->tpl_vars['fields_value'][$item['name']][$i] = Configuration::get(
                        $this->settings_prefix . $item['name'] . '_' . $i
                    );
                }
            } else {
                $helper->tpl_vars['fields_value'][$item['name']] = Configuration::get(
                    $this->settings_prefix .
                    $name
                );
            }
            if (isset($item['addon']) && is_array($item['addon'])
                && isset($item['addon']['name']) && $item['addon']['name']) {
                $addon_name = $item['addon']['name'];
                $helper->tpl_vars['fields_value'][$addon_name] = html_entity_decode(
                    Configuration::get($this->settings_prefix . Tools::strtoupper($addon_name))
                );
            }
            if ($item['name'] == 'CUSTOM_CSS') {
                $helper->tpl_vars['fields_value'][$item['name']] = html_entity_decode(
                    Configuration::get($this->settings_prefix . $name)
                );
            }
        }

        return $helper->generateForm($field_forms);
    }

    protected function createFormHelper(&$form_settings, $table, $item = null)
    {
        if ($this->getPSVersion() == 1.5) {
            foreach ($form_settings as &$form) {
                $form['form']['submit']['class'] = 'button';
            }
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang =
            Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
                Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') :
                0;
        $this->fields_form = array();

        $helper->identifier = 'id_'.$table;
        $helper->submit_action = 'submit'.$table;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) .
            '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'psvd' => $this->getPSVersion(true),
            'psv' => $this->getPSVersion(),
            'PS_ALLOW_ACCENTED_CHARS_URL' => Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL'),
        );
        $helper->module = $this;
        if (Validate::isLoadedObject($item) && $item->id) {
            $helper->id = $item->id;
        }

        $languages = Language::getLanguages();
        foreach ($form_settings as $form) {
            foreach ($form['form']['input'] as $row) {
                if ($row['type'] != 'html' && $row['type'] != 'hidden') {
                    if (Validate::isLoadedObject($item)) {
                        if (property_exists($item, $row['name'])) {
                            $helper->tpl_vars['fields_value'][$row['name']] = $item->{$row['name']};
                        }
                        if (Tools::isSubmit($row['name'])) {
                            $helper->tpl_vars['fields_value'][$row['name']] = Tools::getValue($row['name']);
                        }
                    } else {
                        if (isset($row['lang']) && $row['lang']) {
                            foreach ($languages as $language) {
                                $helper->tpl_vars['fields_value'][$row['name']][$language['id_lang']] =
                                    Tools::getValue($row['name'] . '_' . $language['id_lang']);
                            }
                        } else {
                            $helper->tpl_vars['fields_value'][$row['name']] = Tools::getValue($row['name']);
                        }
                    }
                }
            }
        }

        $iso = $this->context->language->iso_code;
        $helper->tpl_vars['iso'] = file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en';
        $helper->tpl_vars['path_css'] = _THEME_CSS_DIR_;
        $helper->tpl_vars['ad'] = __PS_BASE_URI__.basename(_PS_ADMIN_DIR_);
        $helper->tpl_vars['tinymce'] = true;

        return $helper;
    }

    public function getPSVersion($without_dots = false)
    {
        $ps_version = _PS_VERSION_;
        $ps_version = Tools::substr($ps_version, 0, 3);

        if ($without_dots) {
            $ps_version = str_replace('.', '', $ps_version);
        }

        return (float)$ps_version;
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        $html = '';
        // check whether it's a product page or the module's page
        if (Tools::getValue('configure') == $this->name
            || $this->context->controller->controller_name == 'AdminProducts') {
            $token = Tools::getAdminTokenLite('AdminModules');
            $ajax_url = 'index.php?controller=AdminModules&configure=' . $this->name . '&token=' . $token;

            $this->context->smarty->assign(array(
                'psv' => $this->getPSVersion(),
                'ajax_url' => $ajax_url,
            ));

            $html = $this->context->smarty->fetch($this->local_path . 'views/templates/hook/admin_header.tpl');
        }

        // init countdown if it's a preview page
        if (Tools::getValue('controller') == 'AdminLayerSlider') {
            $html .= $this->hookHeader();
        }

        return $html;
    }

    public function hookActionAdminControllerSetMedia($params)
    {
        if (Tools::getValue('configure') == $this->name
            || $this->context->controller->controller_name == 'AdminProducts') {
            $this->context->controller->addCSS(array(
                $this->_path . 'views/css/flatpickr.min.css',
                $this->_path . 'views/css/spectrum.css',
                $this->_path . 'views/css/admin.css',
            ));
            if ($this->getPSVersion() == 1.5) {
                $this->context->controller->addCSS(array(
                    $this->_path . 'views/css/admin15.css',
                    $this->_path . 'views/css/grid15.css',
                ));
            }
            if ($this->context->controller->controller_name == 'AdminModules') {
                $this->context->controller->addJquery();
                $this->context->controller->addJqueryUI('ui.slider');
                $this->context->controller->addJqueryPlugin(array('typewatch'));
            }
            $this->context->controller->addJS(array(
                $this->_path . 'views/js/moment.min.js',
                $this->_path . 'views/js/flatpickr.min.js',
                $this->_path . 'views/js/jquery.spectrum.min.js',
                $this->_path . 'views/js/jquery.autocomplete.min.js',
            ));

            if ($this->context->controller->controller_name == 'AdminProducts') {
                $this->context->controller->addJS(array(
                    $this->_path . 'views/js/admin-product.js',
                ));
            } else {
                $this->context->controller->addJS(array(
                    $this->_path . 'views/js/admin.js',
                    $this->_path . 'views/js/admin-options.js',
                    $this->_path . 'views/js/admin-select.js',
                    $this->_path . 'views/js/admin-blocks.js',
                ));
            }
        }
    }

    protected function getProductListSelectOptions()
    {
        $results = array(
            array(
                'id_option' => 'no',
                'name' => '-- '.$this->l('(custom hook only)'),
            ),
            array(
                'id_option' => 'custom_over_img',
                'name' => '-- '.$this->l('(custom hook overlay)'),
            ),
            array(
                'id_option' => 'over_img',
                'name' => $this->l('Over product image'),
            ),
            array(
                'id_option' => 'over_img_price',
                'name' => ' - '.$this->l('alternative hook #1'),
            ),
            array(
                'id_option' => 'over_img_unit_price',
                'name' => ' - '.$this->l('alternative hook #2'),
            ),
            array(
                'id_option' => 'over_img_before_price',
                'name' => ' - '.$this->l('alternative hook #3'),
            ),
            array(
                'id_option' => 'over_img_after_price',
                'name' => ' - '.$this->l('alternative hook #4'),
            ),
            array(
                'id_option' => 'displayProductListReviews',
                'name' => $this->l('After product name'),
            ),
            array(
                'id_option' => 'displayProductPriceBlock',
                'name' => $this->l('After price'),
            ),
        );

        return $results;
    }

    protected function getVerticalAlignTypes()
    {
        $results = array(
            array(
                'id_option' => 'top',
                'name' => $this->l('Top'),
            ),
            array(
                'id_option' => 'middle',
                'name' => $this->l('Middle'),
            ),
            array(
                'id_option' => 'bottom',
                'name' => $this->l('Bottom'),
            ),
        );

        return $results;
    }

    protected function getProductPageSelectOptions()
    {
        $results = array(
            array(
                'id_option' => 'no',
                'name' => '-- '.$this->l('(custom hook only)'),
            ),
            array(
                'id_option' => 'displayProductPriceBlock',
                'name' => $this->l('After price'),
            ),
            array(
                'id_option' => 'displayProductAdditionalInfo',
                'name' => $this->l('Additional info'),
            ),
            array(
                'id_option' => 'displayProductButtons',
                'name' => $this->l('Product buttons'),
            ),
            array(
                'id_option' => 'displayLeftColumnProduct',
                'name' => $this->l('Extra left'),
            ),
            array(
                'id_option' => 'displayRightColumnProduct',
                'name' => $this->l('Extra right'),
            ),
            array(
                'id_option' => 'displayFooterProduct',
                'name' => $this->l('Product footer'),
            ),
        );

        return $results;
    }

    protected function getHighlightSelectOptions()
    {
        $results = array(
            array(
                'id_option' => 'seconds',
                'name' => $this->l('Seconds'),
            ),
            array(
                'id_option' => 'minutes',
                'name' => $this->l('Minutes'),
            ),
            array(
                'id_option' => 'hours',
                'name' => $this->l('Hours'),
            ),
            array(
                'id_option' => 'days',
                'name' => $this->l('Days'),
            ),
        );

        return $results;
    }

    protected function getThemesOptions()
    {
        $options = array();

        foreach ($this->getThemes() as $theme) {
            $options[] = array(
                'id' => $theme['file'],
                'value' => $theme['file'],
                'label' => $theme['name'],
                'img' => $this->_path.'views/img/themes/'.$theme['name'].'.png',
            );
        }

        return $options;
    }

    protected function getThemes()
    {
        $themes = array();

        if (file_exists(_PS_MODULE_DIR_ . $this->name . '/views/css/themes/')) {
            $themes_files = scandir(_PS_MODULE_DIR_ . $this->name . '/views/css/themes/');
            natsort($themes_files);
            foreach ($themes_files as $file) {
                if (strpos($file, '.css') !== false) {
                    $pos = strpos($file, '.css');
                    $themes[] = array('file' => $file, 'name' => Tools::substr($file, 0, $pos),);
                }
            }
        }

        return $themes;
    }

    public function getColorsData($with_values = false)
    {
        $default_colors = array(
            '1-simple' => array('rgba(214, 214, 214, 0.7)', '#232323', '#333333', '#f13340', '#202020', null, null, 'rgba(255, 255, 255, 0.8)', ''),
            '2-dark' => array('#202020', '#232323', '#f8f8f8', '#f8f8f8', '#202020', '#000000', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.3)', ''),
            '3-light' => array('#ffffff', '#232323', '#333333', '#333333', '#202020', '#e1e1e1', 'rgba(0, 0, 0, 0.2)', 'rgba(255, 255, 255, 0.5)', ''),
            '4' => array('', '#232323', '#333333', '#f13340', '#3f3f3f', null, null, 'rgba(255, 255, 255, 0.8)', ''),
            '5' => array('', '#232323', '#333333', '#f13340', '#3f3f3f', null, null, 'rgba(255, 255, 255, 0.8)', ''),
            '6' => array('#cc0000', '#232323', '#333333', '#ffffff', '#3f3f3f', null, null, 'rgba(255, 255, 255, 0.9)', ''),
            '7-minimal' => array('rgba(255, 255, 255, 0.8)', '#232323', '#232323'),
            '8-clock' => array('rgba(255, 255, 255, 0.8)', '#232323', '#333333', '#f13340', '#9b9b9b', '#dedede'),
            '9-clock-b' => array('rgba(255, 255, 255, 0.8)', '#232323', '#333333', '#af0404', '#9b9b9b', '#b9b9b9'),
            '10-minimal-1' => array(null, '#232323', '#d80f00', null, null, null, null, 'rgba(255, 255, 255, 0.8)', ''),
            '11-minimal-2' => array(null, '#232323', '#232323', null, null, null, null, 'rgba(255, 255, 255, 0.8)', ''),
            '12-minimal-3' => array(null, '#232323', '#232323', null, null, null, null, 'rgba(255, 255, 255, 0)', ''),
        );

        $result = array();
        foreach ($default_colors as $theme => $colors) {
            if (isset($colors[0]) && $colors[0]) {
                $result[$theme . '-bg'] = array(
                    'name' => $this->l('Background'),
                    'default' => $colors[0],
                    'theme' => $theme,
                );
            }
            if (isset($colors[1]) && $colors[1]) {
                $result[$theme . '-promo'] = array(
                    'name' => $this->l('Promo text'),
                    'default' => $colors[1],
                    'theme' => $theme,
                );
            }
            if (isset($colors[2]) && $colors[2]) {
                $result[$theme . '-digits'] = array(
                    'name' => $this->l('Digits'),
                    'default' => $colors[2],
                    'theme' => $theme,
                );
            }
            if (isset($colors[3]) && $colors[3]) {
                $result[$theme . '-high'] = array(
                    'name' => $this->l('Highlight'),
                    'default' => $colors[3],
                    'theme' => $theme,
                );
            }
            if (isset($colors[4]) && $colors[4]) {
                $result[$theme . '-labels'] = array(
                    'name' => $this->l('Labels'),
                    'default' => $colors[4],
                    'theme' => $theme,
                );
            }

            if (isset($colors[5]) && $colors[5]) {
                $result[$theme.'-border'] = array(
                    'name' => $this->l('Border'),
                    'default' => $colors[5],
                    'theme' => $theme,
                );
            }
            if (isset($colors[6]) && $colors[6]) {
                $result[$theme.'-shadow'] = array(
                    'name' => $this->l('Shadow'),
                    'default' => $colors[6],
                    'theme' => $theme,
                );
            }
            if (isset($colors[7]) && $colors[7]) {
                $result[$theme.'-list-bg'] = array(
                    'name' => $this->l('Block background in the product list'),
                    'default' => $colors[7],
                    'theme' => $theme,
                    'newline' => true,
                );
            }
            if (isset($colors[8]) && $colors[8] !== null) {
                $result[$theme.'-product-bg'] = array(
                    'name' => $this->l('Block background at the product page'),
                    'default' => $colors[8],
                    'theme' => $theme,
                );
            }
        }

        if ($with_values) {
            foreach ($result as $key => &$item) {
                if ($item['theme'] == rtrim($this->theme, '.css')) {
                    $item['value'] = Configuration::get('PSPC_COLORS_'.$key);
                }
            }
        }

        return $result;
    }

    protected function renderMoreOptionsBtn($text, $group, $render = true, $class = '')
    {
        if (!$render) {
            return '';
        }

        $this->context->smarty->assign(array(
            'pspc_text' => $text,
            'pspc_option_group' => $group,
            'pspc_class' => $class,
        ));

        return $this->context->smarty->fetch(
            $this->local_path . 'views/templates/admin/more_options.tpl'
        );
    }

    protected function renderSettingsDivider($render = true)
    {
        if (!$render) {
            return '';
        }

        return $this->context->smarty->fetch(
            $this->local_path . 'views/templates/admin/divider.tpl'
        );
    }

    protected function createListHelper($table, $identifier = null)
    {
        if ($identifier === null) {
            $identifier = 'id_'.$table;
        }

        $this->context->cookie->{$table.'_pagination'} =
            Tools::getValue($table.'_pagination', $this->context->cookie->{$table.'_pagination'});
        if (!$this->context->cookie->{$table.'_pagination'}) {
            $this->context->cookie->{$table.'_pagination'} = 20;
        }
        $this->context->cookie->{$table.'_pagination'} = 1000; // todo del, pagination doesn't work

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = $identifier;
        $helper->actions = array();
        $helper->show_toolbar = false;
        $helper->_defaultOrderBy = 'id_pspc';
        $helper->list_id = $table;
        $helper->table_id = $table;
        $helper->actions = array('edit', 'delete');
        $helper->table = $table;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->currentIndex = str_replace('adminmodules', 'AdminModules', $helper->currentIndex);
        $helper->no_link = false;
        $helper->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );

        if (version_compare(_PS_VERSION_, '1.6.1.0', '>=')) {
            if (isset($this->context->cookie->{$helper->table . '_pagination'})
                && $this->context->cookie->{$helper->table . '_pagination'}) {
                $helper->_default_pagination = $this->context->cookie->{$helper->table . '_pagination'};
            } elseif ($this->getPSVersion() > 1.5) {
                $helper->_default_pagination = $helper->_pagination[0];
            } else {
                $helper->_default_pagination = 20;
            }
        }
        $helper->module = $this;

        $order_way = Tools::strtolower(Tools::getValue($table.'Orderway'));
        $order_way = ($order_way == 'desc' ? 'desc' : 'asc');
        $order_by = Tools::getValue($table.'Orderby', 'pspc.`id_pspc`');
        $helper->orderBy = $order_by;
        $helper->orderWay = $order_way;
        $p = (int)Tools::getValue('submitFilter'.$table, Tools::getValue('page', 1));
        if ($p < 1) {
            $p = 1;
        }
        $helper->page = $p;

        $helper->n = Tools::getValue(
            $table.'_pagination',
            isset($this->context->cookie->{$table.'_pagination'}) ?
                $this->context->cookie->{$table.'_pagination'} :
                $helper->_default_pagination
        );

        return $helper;
    }

    public function getItemsBO($only_count = false, $order_by = null, $order_way = null, $page = 1, $n = 20, $fields_list = array(), $table = '')
    {
        $filters_query = '';
        if (!Tools::isSubmit('submitResetpspc')) {
            $filters = $this->getFilters($fields_list, $table);
            $filters_query = $this->getFiltersQuery($filters);
        }

        $order_by_replace = array(
            'from' => '`from`',
            'to' => '`to`',
        );
        if (isset($order_by_replace[$order_by])) {
            $order_by = $order_by_replace[$order_by];
        }

        $select = 'SELECT *,
                   IF(pspc.`from` > UTC_TIMESTAMP(), "-1", IF(pspc.`to` < UTC_TIMESTAMP(), "-2", pspcs.`active`)) as `active`,
                   pspcs.`active` as `active_orig`';
        $limit = 'LIMIT '.(((int)$page - 1) * (int)$n).', '.(int)$n;

        if ($only_count) {
            $select = 'SELECT DISTINCT COUNT(pspc.`id_pspc`)';
            $limit = '';
        }

        $query = $select.'
             FROM `' . _DB_PREFIX_ . 'pspc` pspc
             LEFT JOIN  `' . _DB_PREFIX_ . 'pspc_lang` pspcl
              ON pspc.`id_pspc` = pspcl.`id_pspc` AND pspcl.`id_lang` = '.(int)$this->context->language->id.'
             LEFT JOIN  `' . _DB_PREFIX_ . 'pspc_shop` pspcs
              ON pspc.`id_pspc` = pspcs.`id_pspc`
             WHERE 1
             '.($filters_query ? $filters_query : '').'
              AND pspcs.`id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')
             '.($only_count ? '' : ' GROUP BY pspc.`id_pspc` ').'
             ORDER BY
              '.($order_by && $order_way ? pSQL($order_by).' '.pSQL($order_way) : '').'
             '.$limit;

        if ($only_count) {
            $count = Db::getInstance()->getValue($query);
            if ($count > 0) { // todo delete, pagination doesn't work
                return 1;
            }
            return $count;
        }

        return Db::getInstance()->executeS($query);
    }

    protected function getFilters($fields_list, $table)
    {
        if (!Tools::getIsset('submitFilter'.$table)) {
            return array();
        }

        $filters = array();
        foreach ($fields_list as $key => $field) {
            $val = Tools::getValue($table.'Filter_'.$key);
            if ($val || $val === 0 || $val === '0') {
                $filters[$key] = $val;
            }
            if (isset($field['filter_key']) && $field['filter_key']) {
                $val = Tools::getValue($table . 'Filter_' . $field['filter_key']);
                if ($val || $val === 0 || $val === '0') {
                    $key = str_replace('!', '.', $field['filter_key']);
                    $filters[$key] = $val;
                }
            }
        }

        return $filters;
    }

    protected function getFiltersQuery($filters)
    {
        $filters_query = '';
        if (is_array($filters) && count($filters)) {
            foreach ($filters as $key => $value) {
                if ($key == 'system_name') {
                    $filters_query .= ' AND ' . pSQL($key) . ' LIKE "%' . pSQL($value) . '%" ';
                } elseif ($key == 'name') {
                    $filters_query .= ' AND ' . pSQL($key) . ' LIKE "%' . pSQL($value) . '%" ';
                } elseif ($key == 'items') {
                    $products = PSPC::searchByProductName($value);
                    $categories = PSPC::searchByCategoryName($value);
                    $manufacturers = PSPC::searchByManufacturerName($value);
                    $filters_query .=
                        ' AND pspc.`id_pspc` IN (
                            SELECT `id_pspc` FROM `'._DB_PREFIX_.'pspc_object`
                            WHERE 
                             (`id_object` IN ('.implode(',', array_map('intval', $products)).') AND `type` = "product")
                             OR (`id_object` IN ('.implode(',', array_map('intval', $categories)).')
                              AND `type` = "category")
                             OR (`id_object` IN ('.implode(',', array_map('intval', $manufacturers)).')
                              AND `type` = "manufacturer")
                         )';
                } else {
                    $filters_query .= ' AND ' . pSQL($key) . ' = "' . pSQL($value) . '" ';
                }
            }
        }

        return $filters_query;
    }

    protected function renderCountdownForm($id_pspc = 0)
    {
        $pspc = new PSPC($id_pspc);

        $last_groups = Configuration::get('PSPC_LAST_GROUPS');
        if ($last_groups) {
            $last_groups = explode(',', $last_groups);
        }

        if (!$id_pspc) {
            $pspc->reduction_tax = Configuration::get('PS_TAX');
            $pspc->personal_hours = 24;

            if (!(is_array($last_groups) && count($last_groups))) {
                $pspc->all_groups = true;
            }
        }

        $chosen_products = $pspc->getObjects('product');
        $chosen_categories = $pspc->getObjects('category', true);
        $chosen_manufacturers = $pspc->getObjects('manufacturer', true);

        // category filter for product search
        $product_filter_category_tree =
            $this->renderCategoryTree('itemsCategoryFilter', $id_pspc, $this->l('Category filter'));

        // category select
        $select_category_tree =
            $this->renderCategoryTree('categories', $id_pspc, $this->l('Select category'), true, $chosen_categories);

        $token = Tools::getAdminTokenLite('AdminPerformance');
        $performance_url = 'index.php?controller=AdminPerformance&token=' . $token;

        $this->context->smarty->assign(array(
            'pspc' => $pspc,
            'pspc_module' => $this,
            'pspc_default_currency' => $this->context->currency,
            'actions_start' => $this->getActionsStart(),
            'actions_end' => $this->getActionsEnd(),
            'product_category_tree' => $product_filter_category_tree,
            'select_category_tree' => $select_category_tree,
            'pspc_tpl_dir' => _PS_MODULE_DIR_.$this->name.'/views/templates/admin',
            'pspc_manufacturers' => Manufacturer::getManufacturers(),
            'pspc_chosen_products' => $chosen_products,
            'pspc_chosen_categories' => $chosen_categories,
            'pspc_chosen_manufacturers' => $chosen_manufacturers,
            'pspc_groups' => Group::getGroups($this->context->language->id),
            'pspc_last_groups' => $last_groups,
            'pspc_perf_url' => $performance_url,
        ));

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/pspc_form.tpl');
    }

    public function getProductReference($id_product, $id_product_attribute)
    {
        $reference = '';
        if ($id_product_attribute) {
            $combination = new Combination($id_product_attribute);
            $reference = $combination->reference;
        }
        if (!$reference && $id_product) {
            $product = new Product($id_product);
            $reference = $product->reference;
        }

        return $reference;
    }

    public function generateInput($params)
    {
        if ($params) {
            $this->context->smarty->assign(array(
                'params' => $params,
                'psv' => $this->getPSVersion(),
                'languages' => Language::getLanguages(),
                'id_lang_default' => Configuration::get(
                    'PS_LANG_DEFAULT',
                    null,
                    $this->context->shop->id_shop_group,
                    $this->context->shop->id
                ),
            ));

            return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/input.tpl');
        }
    }

    protected function getActionsStart()
    {
        return array(
            'activate' => $this->l('Activate the product'),
            'enable_orders' => $this->l('Enable orders'),
            'disable_orders' => $this->l('Disable orders'),
            'set_qty' => $this->l('Change quantity'),
        );
    }
    protected function getActionsEnd()
    {
        return array(
            'deactivate' => $this->l('Deactivate the product'),
            'enable_orders' => $this->l('Enable orders'),
            'disable_orders' => $this->l('Disable orders'),
            'set_qty' => $this->l('Change quantity'),
        );
    }

    /**
     * Ajax get products for autocomplete
     */
    public function ajaxProcessGetProducts()
    {
        $context = Context::getContext();
        $query = Tools::getValue('query', false);
        $search_combinations = Tools::getValue('search_combinations');
        $categories = Tools::getValue('categories');
        if (is_array($categories)) {
            $categories = array_filter($categories);
        }
        $id_manufacturer = Tools::getValue('id_manufacturer');
        if (!$query && !$categories && !$id_manufacturer) {
            die();
        }

        /*
         * In the SQL request the "q" param is used entirely to match result in database.
         * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
         * they are no return values just because string:"(ref : #ref_pattern#)"
         * is not write in the name field of the product.
         * So the ref pattern will be cut for the search request.
         */
        if ($pos = strpos($query, ' (ref:')) {
            $query = Tools::substr($query, 0, $pos);
        }

        $sql =
            'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, p.`cache_default_attribute`
             FROM `'._DB_PREFIX_.'product` p
             '.Shop::addSqlAssociation('product', 'p').'
             LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON
              (pl.id_product = p.id_product
              AND pl.id_lang = '.(int)$context->language->id.Shop::addSqlRestrictionOnLang('pl').')
             '.($categories ?
                ' LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product` ' : '').'
             WHERE 1
              '.($categories ? ' AND cp.`id_category` IN ('.implode(',', array_map('intval', $categories)).')' : '').'
              '.($query ? ' AND (pl.name LIKE "%'.pSQL($query).'%" OR p.reference LIKE "%'.pSQL($query).'%"
                OR p.`id_product` = '.(int)$query.')' : '').'
              '.($id_manufacturer ? ' AND p.id_manufacturer = '.(int)$id_manufacturer : '').'
             GROUP BY p.id_product';
        $products = Db::getInstance()->executeS($sql);

        $result = array();
        if ($products) {
            foreach ($products as $product) {
                $result[] = array(
                    'name' =>
                        '#'.$product['id_product'].' '.
                        trim($product['name']) .
                        (!empty($product['reference']) ? ' (ref: ' . $product['reference'] . ')' : ''),
                    'id_product' => (int)$product['id_product'],
                );

                // add combinations
                if ($search_combinations) {
                    $obj = new Product($product['id_product']);
                    $attributes = $obj->getAttributesGroups($this->context->language->id);
                    if (count($attributes)) {
                        $combinations = array();
                        foreach ($attributes as $attribute) {
                            $ipa = $attribute['id_product_attribute'];
                            $combinations[$ipa]['id_product_attribute'] = $ipa;
                            $combinations[$ipa]['reference'] = $attribute['reference'];
                            if (!isset($combinations[$ipa]['attributes'])) {
                                $combinations[$ipa]['attributes'] = '';
                            }
                            $combinations[$ipa]['attributes'] .= $attribute['attribute_name'].' - ';
                        }
                        foreach ($combinations as &$combination) {
                            $combination['attributes'] = rtrim($combination['attributes'], ' - ');

                            $result[] = array(
                                'name' =>
                                    '  --- '.
                                    trim($product['name']).
                                    ' ('.$combination['attributes'].')'.
                                    (!empty($combination['reference']) ? ' (ref: '.$combination['reference'].')'
                                      : (!empty($product['reference']) ? ' (ref: ' . $product['reference'] . ')' : '')),
                                'id_product' => $product['id_product'].'-'.$combination['id_product_attribute']
                            );
                        }
                    }
                }
            }
        }

        die(Tools::jsonEncode($result));
    }

    public function ajaxProcessSaveCountdown()
    {
        $save_result = $this->saveCountdown();

        // Display errors if any
        if ($save_result !== true) {
            die($save_result);
        }

        die('1');
    }

    public function ajaxProcessBulkDeletePSPC()
    {
        $ids = Tools::getValue('ids');

        foreach ($ids as $id) {
            $pspc = new PSPC($id);
            $pspc->delete();
        }

        die('1');
    }

    public function ajaxProcessBulkDeletePSPCBlock()
    {
        $ids = Tools::getValue('ids');

        foreach ($ids as $id) {
            $pspc = new PSPCBlock($id);
            $pspc->delete();
        }

        die('1');
    }

    public function saveCountdown()
    {
        $html = '';
        $id_pspc = Tools::getValue('id_pspc');

        // products/categories/manufacturers
        $products = Tools::getValue('products');
        $categories = Tools::getValue('categories');
        $manufacturers = Tools::getValue('manufacturers');
        if (!$products && !$categories && !$manufacturers) {
            return $this->l('Please select products');
        }

        $save_success = true;
        $pspc = new PSPC($id_pspc);
        $pspc->action_start_done = 0;
        $pspc->action_end_done = 0;
        foreach (PSPC::$definition['fields'] as $field_name => $field_data) {
            if (isset($field_data['lang']) && $field_data['lang']
                && !(isset($field_data['file']) && $field_data['file'])
            ) {
                $pspc->$field_name = array();
                foreach (Language::getLanguages() as $lang) {
                    $pspc->{$field_name}[$lang['id_lang']] =
                        trim(Tools::getValue($field_name . '_' . $lang['id_lang']));
                }
            } elseif (Tools::isSubmit($field_name) || $field_name == 'all_groups') {
                $pspc->{$field_name} = trim(Tools::getValue($field_name));
            }
        }
        if (!$pspc->from && !$pspc->personal) {
            $from_time = time() - (24 * 60 * 60); // now - 24h
            $pspc->from = date('Y-m-d H:i:s', $from_time);
        }

        // Check for errors
        $field_errors = $pspc->validateAllFields();

        if (!(is_array($field_errors) && count($field_errors))) {
            if ($pspc->save(false, true, false)) {
                $pspc->setObjects($products, 'product');
                $pspc->setObjects($categories, 'category');
                $pspc->setObjects($manufacturers, 'manufacturer');
                $pspc->setGroups(Tools::getValue('groups'));
                $this->clearSmartyCache();
                $pspc->saveSpecificPrices();
            } else {
                $field_errors[] = $this->l('Unable to save the countdown');
            }
        }

        if (is_array($field_errors) && count($field_errors)) {
            $html .= implode('', $field_errors);
            $save_success = false;
        }

        return ($save_success ? true : $html);
    }

    public function ajaxProcessRenderCountdownList()
    {
        die($this->renderCountdownList());
    }

    public function ajaxProcessChangeCountdownStatus()
    {
        $id_pspc = Tools::getValue('id_pspc');
        $pspc = new PSPC($id_pspc);
        if (Validate::isLoadedObject($pspc)) {
            $pspc->active = !$pspc->active;
            $pspc->save(false, true, false);
            $pspc->toggleSpecificPrices();
            $this->clearSmartyCache();
        }
    }

    public function clearSmartyCache()
    {
        $directory = _PS_MODULE_DIR_.$this->name.'/views/templates/hook/';
        $templates = array_diff(scandir($directory), array('..', '.'));
        foreach ($templates as $key => &$template) {
            if (strpos($template, '.tpl') === false) {
                continue;
            }
            $template = basename($template, '.tpl');

            if (method_exists($this, '_clearCache')) {
                $this->_clearCache($template);
            }

            if ($this->getPSVersion() == 1.7 && method_exists($this, '_deferedClearCache')) {
                $this->_deferedClearCache($this->getTemplatePath($template), null, null);
            }
        }
    }

    public function hookHeader()
    {
        $this->processActions();

        // Add JQuery
        $this->context->controller->addJquery();

        // Register JS
        $this->context->controller->addJS(
            array(
                $this->_path . 'views/js/underscore.min.js',
                $this->_path . 'views/js/jquery.countdown.min.js',
                $this->_path . 'views/js/front.js',
                $this->_path . 'views/js/custom.js',
            )
        );

        // Register CSS
        $css_time = Configuration::get($this->settings_prefix.'CSS_SAVED');
        if ($css_time) {
            $settings_css = $this->_path . 'views/css/settings-'.$css_time.'.css';
        } else {
            $settings_css = $this->_path . 'views/css/settings.css';
        }
        $this->context->controller->addCSS(array(
            $this->_path . 'views/css/front.css',
            $settings_css,
        ));

        // Register theme CSS
        if ($this->theme) {
            $this->context->controller->addCSS(
                $this->_path . 'views/css/themes/' . $this->theme
            );
        }

        $this->context->smarty->assign(array(
            'pspc_theme' => rtrim($this->theme, '.css'),
            'pspc_highlight' => $this->highlight,
            'pspc_hide_after_end' => $this->hide_after_end,
            'pspc_hide_expired' => $this->hide_expired,
            'pspc_custom_css' => html_entity_decode($this->custom_css),
            'pspc_custom_js' => html_entity_decode($this->custom_js),
            'pspc_position_product' => $this->product_position,
            'pspc_position_list' => $this->product_list_position,
            'pspc_adjust_positions' => $this->adjust_positions,
            'pspc_promo_side' => $this->promo_side,
            'psv' => $this->getPSVersion(),
        ));

        return $this->display(__FILE__, 'header.tpl');
    }

    public function hookPSPC($params)
    {
        $return = null;
        $id_product = null;

        // Get id_product
        if (isset($params['id_product']) && $params['id_product'] > 0) {
            $id_product = $params['id_product'];
        } elseif (isset($params['product']) && $params['product']) {
            $product = $params['product'];
            if (is_array($product) && isset($product['id_product'])) {
                $id_product = $product['id_product'];
            } elseif (is_object($product)) {
                $id_product = $product->id;
            } else {
                return false;
            }
        } else {
            $id_product = Tools::getValue('id_product');
        }
        
        // Get id_product_attribute
        $id_product_attribute = null;
        if (isset($params['id_product_attribute'])) {
            $id_product_attribute = $params['id_product_attribute'];
        } elseif (Tools::getValue('group')) {
            $groups = Tools::getValue('group');

            if (!empty($groups) && method_exists('Product', 'getIdProductAttributesByIdAttributes')) {
                $id_product_attribute = (int) Product::getIdProductAttributesByIdAttributes(
                    $id_product,
                    $groups
                );
            }
        }
        if ($id_product_attribute === null) {
            $id_product_attribute = Tools::getValue('id_product_attribute');
        }
        $has_attributes = $this->checkProductHasAttributes($id_product);
        if (!$id_product_attribute && $has_attributes) {
            $id_product_attribute = $this->getDefaultIdProductAttribute($id_product);
        }

        $hook = (isset($params['hook']) ? $params['hook'] : '');

        // render timers for all combinations at once at the product page in PS1.6
        if ($has_attributes && $this->getPSVersion() < 1.7 && $this->context->controller->php_self == 'product') {
            $ipas = Product::getProductAttributesIds($id_product, true);
            foreach ($ipas as $ipa) {
                $return .= $this->renderCountdown($id_product, $ipa['id_product_attribute'], $hook);
            }
        } else {
            $return = $this->renderCountdown($id_product, $id_product_attribute, $hook);
        }

        return $return;
    }

    public function hookPSProductCountdown($params)
    {
        return $this->hookPSPC($params);
    }

    public function hookDisplayProductListReviews($params)
    {
        if ($this->product_list_position == 'displayProductListReviews') {
            $params['hook'] = 'displayProductListReviews';
            return $this->hookPSPC($params);
        }
    }

    public function hookDisplayProductPriceBlock($params)
    {
        $type = (isset($params['type']) ? $params['type'] : null);

        if (($type == 'weight' && $this->product_position == 'displayProductPriceBlock') ||
            ($type == 'weight' && $this->product_list_position == 'over_img') ||
            ($type == 'weight' && $this->product_list_position == 'displayProductPriceBlock') ||
            ($type == 'price' && $this->product_list_position == 'over_img_price') ||
            ($type == 'unit_price' && $this->product_list_position == 'over_img_unit_price') ||
            ($type == 'before_price' && $this->product_list_position == 'over_img_before_price') ||
            ($type == 'after_price' && $this->product_list_position == 'over_img_after_price')
        ) {
            $params['hook'] = 'displayProductPriceBlock'.($type != 'weight' ? 'Alt' : '');
            return $this->hookPSPC($params);
        }
    }

    public function hookDisplayProductButtons($params)
    {
        if ($this->product_position == 'displayProductButtons') {
            $params['hook'] = 'displayProductButtons';
            return $this->hookPSPC($params);
        }
    }

    public function hookDisplayLeftColumnProduct($params)
    {
        if ($this->product_position == 'displayLeftColumnProduct') {
            $params['hook'] = 'displayLeftColumnProduct';
            return $this->hookPSPC($params);
        }
    }

    public function hookDisplayRightColumnProduct($params)
    {
        if ($this->product_position == 'displayRightColumnProduct') {
            $params['hook'] = 'displayRightColumnProduct';
            return $this->hookPSPC($params);
        }
    }

    public function hookDisplayFooterProduct($params)
    {
        $html = '';

        if ($this->product_position == 'displayFooterProduct') {
            $params['hook'] = 'displayFooterProduct';
            $html .= $this->hookPSPC($params);
        }

        $html .= $this->renderWidget('displayFooterProduct', $params);

        return $html;
    }

    public function hookDisplayProductAdditionalInfo($params)
    {
        $html = '';

        if ($this->product_position == 'displayProductAdditionalInfo') {
            $params['hook'] = 'displayProductAdditionalInfo';
            $html .= $this->hookPSPC($params);
        }

        $html .= $this->renderWidget('displayProductAdditionalInfo', $params);

        return $html;
    }

    public function hookPSPCBlock($params)
    {
        $hookName = 'custom';

        return $this->renderWidget($hookName, $params);
    }

    public function hookDisplayHome($params)
    {
        $hookName = 'displayHome';

        return $this->renderWidget($hookName, $params);
    }

    public function hookDisplayLeftColumn($params)
    {
        $hookName = 'displayLeftColumn';

        return $this->renderWidget($hookName, $params);
    }

    public function hookDisplayRightColumn($params)
    {
        $hookName = 'displayRightColumn';

        return $this->renderWidget($hookName, $params);
    }

    public function hookDisplayHomeTabContent($params)
    {
        $hookName = 'displayHomeTabContent';

        return $this->renderWidget($hookName, $params);
    }

    public function hookDisplayHomeTab($params)
    {
        $hookName = 'displayHomeTab';

        if ($this->getPSVersion() == 1.7) {
            $tpl_file = 'module:' . $this->name . '/views/templates/hook/tab.tpl';
        } else {
            $tpl_file = 'tab.tpl';
        }

        $cache_key = $hookName.date('Y-m-d H');
        if (!$this->isCached($tpl_file, $this->getCacheId($cache_key))) {
            $this->context->smarty->assign($this->getWidgetVariables('displayHomeTabContent', $params));
        }

        if ($this->getPSVersion() == 1.7) {
            return $this->fetch($tpl_file, $this->getCacheId($cache_key));
        } else {
            return $this->display(__FILE__, $tpl_file, $this->getCacheId($cache_key));
        }
    }

    protected function getDefaultIdProductAttribute($id_product)
    {
        if (!Combination::isFeatureActive()) {
            return 0;
        }

        return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue(
            'SELECT pa.`id_product_attribute`
			 FROM `'._DB_PREFIX_.'product_attribute` pa
			 '.Shop::addSqlAssociation('product_attribute', 'pa').'
			 WHERE pa.`id_product` = '.(int)$id_product.'
			 AND product_attribute_shop.default_on = 1'
        );
    }

    protected function checkProductHasAttributes($id_product)
    {
        $attrs = Product::getAttributesInformationsByProduct($id_product);
        return !empty($attrs);
    }

    protected function renderCountdown($id_product, $id_product_attribute = 0, $hook = '')
    {
        $html = '';

        if ($id_product) {
            $pspc = $this->getCountdown($id_product, $id_product_attribute);

            if ($pspc) {
                $datetime_current = new DateTime('now', new DateTimeZone('UTC'));
                $datetime_to = new DateTime($pspc->to, new DateTimeZone('UTC'));
                $days_diff = abs($datetime_to->getTimestamp() - $datetime_current->getTimestamp()) / 60 / 60 / 24;
                
                $this->context->smarty->assign(array(
                    'pspc' => $pspc,
                    'pspc_days_diff' => $days_diff,
                    'pspc_theme' => str_replace('.css', '', $this->theme),
                    'pspc_product_position' => $this->product_position,
                    'pspc_product_list_position' => $this->product_list_position,
                    'pspc_compact_view' => $this->compact_view,
                    'pspc_background_image' => $this->background_image,
                    'pspc_show_promo_text' => $this->show_promo_text,
                    'pspc_promo_text' => (isset($this->promo_text[$this->context->language->id])
                        ? $this->promo_text[$this->context->language->id] : ''),
                    'pspc_psv' => $this->getPSVersion(true),
                    'pspc_ipa' => $id_product_attribute,
                    'pspc_vertical_align' => Configuration::get($this->settings_prefix.'VERTICAL_ALIGN'),
                    'pspc_hook' => $hook,
                    'pspc_colon' => $this->show_colon,
                    'pspc_days_before_end' => $this->display_days_before_end,
                    'pspc_too_long' =>
                        ($this->display_days_before_end > 0 && $days_diff > $this->display_days_before_end),
                ));

                $key = 'pspc'.'-'.$pspc->id.'-'.$hook.($this->getPSVersion() == 1.6 ? '-'.$id_product_attribute : '');
                $html = $this->display(
                    __FILE__,
                    'pspc.tpl',
                    $this->getCacheId($key)
                );
            }
        }
        
        return $html;
    }

    /**
     * @param $id_product
     * @param int $id_product_attribute
     * @return PSPC | null
     * For Front Office
     * Get a pre-configured countdown or generate a countdown from specific prices
     */
    public function getCountdown($id_product, $id_product_attribute = 0)
    {
        $product = new Product($id_product);

        // Search for product countdown
        $pspc = PSPC::findPSPC('product', $id_product, $id_product_attribute, $this);

        // if countdown is disabled then return false and don't search another timers
        if ($pspc && !$pspc->active) {
            return null;
        }

        // Search for category countdown if no countdown found yet
        if (!$pspc) {
            $ids_categories = Product::getProductCategories($id_product);
            if ($this->with_subcategories) {
                $ids_categories = $this->getCategoriesParents($ids_categories);
            }
            $pspc = PSPC::findPSPC('category', $ids_categories, 0, $this);

            // if countdown is disabled or not found then return false
            if ($pspc && !$pspc->active) {
                return null;
            }
        }

        // Search for manufacturer countdown if no countdown found yet
        if (!$pspc) {
            $pspc = PSPC::findPSPC('manufacturer', $product->id_manufacturer, 0, $this);

            // if countdown is disabled or not found then return false
            if ($pspc && !$pspc->active) {
                return null;
            }
        }

        // If countdown not found and countdowns activated for all products with specific prices:
        if (!$pspc && $this->activate_all_special) {
            $qty = max((int)$product->minimal_quantity, 1);
            $specific_price = SpecificPrice::getSpecificPrice(
                $id_product,
                $this->context->shop->id,
                $this->context->currency->id,
                $this->context->country->id,
                $this->context->customer->id_default_group,
                $qty,
                $id_product_attribute,
                $this->context->customer->id,
                $this->context->cart->id,
                $qty
            );
            if ($specific_price && is_array($specific_price) && isset($specific_price['to'])) {
                $tz = Configuration::get('PS_TIMEZONE');
                $dt_to = new DateTime($specific_price['to'], new DateTimeZone($tz));
                $dt_to->setTimezone(new DateTimeZone('UTC'));
                $dt_from = new DateTime($specific_price['from'], new DateTimeZone($tz));
                $dt_from->setTimezone(new DateTimeZone('UTC'));
                $dt_current = new DateTime('now', new DateTimeZone('UTC'));
                if (($dt_from > $dt_current || $dt_to <= $dt_current) && !$this->personal_specials) {
                    return null;
                }

                $pspc = new PSPC();
                $pspc->id = $specific_price['id_specific_price'];
                $pspc->id_object = $id_product;
                $pspc->name = '';
                $pspc->from = $dt_from->format('Y-m-d H:i:s');
                $pspc->to = $dt_to->format('Y-m-d H:i:s');
                $pspc->loadTz();
                $pspc->active = 1;
                $pspc->type = 'specific_price';

                if (!$this->personal_specials_only_emp || strtotime($pspc->to.' UTC') < 0) {
                    if ($this->personal_specials && $this->personal_hours) {
                        $pspc->personal = true;
                        $pspc->personal_hours = $this->personal_hours;
                        $pspc->hours_restart = $this->personal_hours_restart;
                    }
                }
            }
        }

        // final preparations
        if ($pspc) {
            // Personal countdown settings
            if ($pspc->personal && $pspc->personal_hours) {
                $pspc->from = '2000-01-01 00:00:00';
                $pspc->personal_hours = $this->randomizePersonalHours($pspc->personal_hours);
                $pspc->to = date("Y-m-d H:i:s", strtotime(sprintf("+%d hours", $pspc->personal_hours)));
                $pspc->loadTz();
            }

            return $pspc;
        }

        return null;
    }

    public function randomizePersonalHours($personal_hours)
    {
        if ($this->randomize_personal) {
            $diff = 10; // 10 percents

            $random = mt_rand(100 - $diff, 100 + $diff) / 100;
            if ($random == 1) {
                $random = mt_rand(100 - $diff, 100 + $diff) / 100;
            }
            $personal_hours = $personal_hours * $random;
        }

        return $personal_hours;
    }

    protected function renderCategoryTree($name, $id_pspc, $display_name, $checkbox = false, $selected_cats = array())
    {
        if (class_exists('HelperTreeCategories')) {
            $root = Category::getRootCategory();
            $tree = new HelperTreeCategories($name.$id_pspc, $display_name);
            $tree->setRootCategory((int)$root->id)
                ->setUseCheckBox($checkbox)
                ->setInputName($name)
                ->setSelectedCategories($selected_cats)
                ->setUseSearch(true);
            if (count($selected_cats)) {
                $reset = new TreeToolbarLink('Reset', '#');
                $reset->setAttribute('id', $name.'-reset');
                $tree->addAction($reset);
            }
            $product_tree_html = $tree->render();
        } else {
            $category = Category::getRootCategory();
            $categories = $category->recurseLiteCategTree(100);
            $this->context->smarty->assign(array(
                'tree_id_root' => $category->id,
                'tree_categories' => $categories['children'],
                'tree_name' => $name,
                'tree_multiple' => $checkbox,
                'tree_selected' => $selected_cats,
                'pspc_admin_tpl_dir' => _PS_MODULE_DIR_.$this->name.'/views/templates/admin',
            ));
            $product_tree_html =
                $this->context->smarty->fetch($this->local_path . 'views/templates/admin/category_tree15.tpl');
        }

        return $product_tree_html;
    }

    protected function regenerateCSS()
    {
        $this->context->smarty->assign(array(
            'pspc_bg_color' => $this->bg_color,
            'pspc_theme' => rtrim($this->theme, '.css'),
            'pspc_compact_view' => $this->compact_view,
            'pspc_background_image' => $this->background_image,
            'pspc_border_color' => $this->border_color,
            'pspc_upload_dir' => $this->_path.'upload/',
            'pspc_text_color' => $this->text_color,
            'pspc_label_color' => $this->label_color,
            'pspc_seconds_color' => $this->seconds_color,
            'pspc_border_radius' => $this->border_radius,
            'pspc_base_font_size' => $this->base_font_size,
            'pspc_base_font_size_list' => $this->base_font_size_list,
            'pspc_base_font_size_col' => $this->base_font_size_col,
            'pspc_highlight' => $this->highlight,
            'pspc_show_promo_text' => $this->show_promo_text,
            'pspc_hide_after_end' => $this->hide_after_end,
            'pspc_hide_expired' => $this->hide_expired,
            'pspc_custom_css' => html_entity_decode($this->custom_css),
            'pspc_colors_data' => $this->getColorsData(true),
            'pspc_module' => $this,
            'psv' => $this->getPSVersion(),
        ));

        $code = $this->context->smarty->fetch($this->local_path . 'views/templates/hook/css.tpl');

        $css_dir = _PS_MODULE_DIR_.$this->name.'/views/css/';
        $time = time();
        Configuration::updateValue($this->settings_prefix.'CSS_SAVED', $time);
        array_map('unlink', glob(_PS_MODULE_DIR_.$this->name.'/views/css/settings-*.css'));
        $css_file = $css_dir.'settings-'.$time.'.css';
        if (is_writable($css_dir)) {
            file_put_contents($css_file, $code);
        } else {
            $this->errors[] = $this->l('Please make the "css" directory writable:')
                .' /modules/psproductcountdownpro/views/css/';
        }
    }

    public function processActions()
    {
        $id_product = Tools::getValue('id_product');
        $this->reload = false;
        // Get active timers with actions not marked 'done'
        $timers = Db::getInstance()->executeS(
            'SELECT pspc.`id_pspc`
             FROM `'._DB_PREFIX_.'pspc` pspc
             LEFT JOIN `' . _DB_PREFIX_ . 'pspc_shop` pspcs
              ON pspc.`id_pspc` = pspcs.`id_pspc`
             WHERE pspcs.`active` = 1 AND `personal` = 0
              AND pspcs.`id_shop` = '.(int)$this->context->shop->id.'
              AND (
                (`action_start` != "" AND `action_start` IS NOT NULL
                 AND `action_start_done` = 0 AND `from` <= UTC_TIMESTAMP())
                OR
                (`action_end` != "" AND `action_end` IS NOT NULL
                 AND `action_end_done` = 0 AND `to` <= UTC_TIMESTAMP())
              )'
        );
        foreach ($timers as $timer) {
            $this->processCountdownAction($timer['id_pspc']);
        }

        // Get active expired timers that should be restarted
        $timers = Db::getInstance()->executeS(
            'SELECT pspc.`id_pspc`
             FROM `'._DB_PREFIX_.'pspc` pspc
             LEFT JOIN `' . _DB_PREFIX_ . 'pspc_shop` pspcs
              ON pspc.`id_pspc` = pspcs.`id_pspc`
             WHERE pspcs.`active` = 1 AND `personal` = 0
              AND pspcs.`id_shop` = '.(int)$this->context->shop->id.'
              AND `hours_restart` > 0 AND `to` <= UTC_TIMESTAMP()'
        );
        // Restart timers
        foreach ($timers as $timer) {
            $restarted = PSPC::restartStatic($timer['id_pspc']);
            if ($restarted && $this->context->controller->php_self == 'product') {
                if (PSPC::checkCountdownAppliedToProduct($timer['id_pspc'], $id_product)) {
                    $this->reload |= true;
                }
            }
        }

        // reload product page if any actions with this product were performed
        if ($this->reload && $this->context->controller->php_self == 'product') {
            $ipa = Tools::getValue('id_product_attribute', 0);
            Tools::redirect($this->context->link->getProductLink($id_product, null, null, null, null, null, $ipa));
        }
    }

    protected function processCountdownAction($id_pspc)
    {
        $pspc = new PSPC($id_pspc);

        // Mark actions as done
        if (!$pspc->action_start_done && strtotime($pspc->from.' UTC') <= gmdate('U')) {
            Db::getInstance()->update('pspc', array('action_start_done' => 1), 'id_pspc = ' . (int) $pspc->id);
        }
        if (!$pspc->action_end_done && strtotime($pspc->to.' UTC') <= gmdate('U')) {
            Db::getInstance()->update('pspc', array('action_end_done' => 1), 'id_pspc = ' . (int) $pspc->id);
        }

        $products = $pspc->getRelatedProducts();
        foreach ($products as $product) {
            $id_product = $product['id_product'];
            $id_product_attribute = (isset($product['id_product_attribute']) ? $product['id_product_attribute'] : 0);
            $this->processProductAction($id_product, $id_product_attribute, $pspc);
        }
    }

    /**
     * @param $id_product int
     * @param $pspc PSPC
     */
    protected function processProductAction($id_product, $id_product_attribute, $pspc)
    {
        if (!$pspc->action_start_done && strtotime($pspc->from.' UTC') <= gmdate('U')) {
            $this->reload = ($this->checkReload($id_product) ? true : $this->reload);
            $this->executeProductAction(true, $pspc->action_start, $id_product, $id_product_attribute, $pspc);
        }
        
        if (!$pspc->action_end_done && strtotime($pspc->to.' UTC') <= gmdate('U')) {
            $this->reload = ($this->checkReload($id_product) ? true : $this->reload);
            $this->executeProductAction(false, $pspc->action_end, $id_product, $id_product_attribute, $pspc);
        }
    }

    protected function checkReload($id_product)
    {
        if ($this->context->controller->php_self == 'product' && Tools::getValue('id_product') == $id_product) {
            return true;
        }

        return false;
    }

    public function renderWidget($hookName, array $params)
    {
        if (!$hookName) {
            $hookName = (isset($params['hook']) ? $params['hook'] : 'custom');
        }

        if ($this->getPSVersion() == 1.7) {
            $tpl_file = 'module:' . $this->name . '/views/templates/hook/blocks.tpl';
        } else {
            $tpl_file = 'blocks.tpl';
        }

        $cache_key = $hookName.date('Y-m-d H');
        if (!$this->isCached($tpl_file, $this->getCacheId($cache_key))) {
            $this->context->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        if ($this->getPSVersion() == 1.7) {
            return $this->fetch($tpl_file, $this->getCacheId($cache_key));
        } else {
            return $this->display(__FILE__, $tpl_file, $this->getCacheId($cache_key));
        }
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $tpl_file = $this->name.'/views/templates/hook/_block'.($this->getPSVersion() == 1.7 ? '17' : '').'.tpl';
        return array(
            'psv' => $this->getPSVersion(),
            'psvd' => $this->getPSVersion(true),
            'pspc_blocks' => PSPCBlock::getBlocksFront($hookName),
            'pspc_block_hook' => $hookName,
            'pspc_block_tpl_file' => (file_exists(_PS_THEME_DIR_ . 'modules/' . $tpl_file)
                ? _PS_THEME_DIR_ . 'modules/' . $tpl_file : _PS_MODULE_DIR_ . $tpl_file),
        );
    }

    protected function renderBlocks()
    {
        $title = $this->l('New block');

        $this->context->smarty->assign(array(
            'preTable' =>
                $this->renderNewBlockButton().
                $this->renderBlockForm($title, $this->getBlockFormFields(), 'pspc_block', null),
            'psv' => $this->getPSVersion(),
            'form_id' => 'pspc_block',
        ));

        return $this->renderBlockList();
    }

    public function ajaxProcessGetBlockList()
    {
        die($this->renderBlocks());
    }

    public function ajaxProcessChangeBlockStatus()
    {
        $id_block = Tools::getValue('id_block');
        $block = new PSPCBlock($id_block);

        if (Validate::isLoadedObject($block)) {
            $block->active = ($block->active ? 0 : 1);
            $block->save();
        }

        die('1');
    }

    protected function renderNewBlockButton()
    {
        $token = Tools::getAdminTokenLite('AdminModules');
        $form_url = 'index.php?controller=AdminModules&configure=' . $this->name . '&createNewBlock=1&token=' . $token;

        $this->context->smarty->assign(array(
            'form_url' => $form_url,
            'psv' => $this->getPSVersion(),
            'psvd' => $this->getPSVersion(true),
        ));

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/new_block.tpl');
    }

    public function ajaxProcessGetBlockForm()
    {
        $id_block = Tools::getValue('id_block');
        $block = new PSPCBlock($id_block);
        $title = $this->l('Edit block');

        return $this->renderBlockForm($title, $this->getBlockFormFields($id_block), 'pspc_block', $block);
    }

    protected function renderBlockForm($title, $settings, $form_name, $block_obj)
    {
        $field_forms = array(
            array(
                'form' => array(
                    'legend' => array(
                        'title' => $title,
                        'icon' => 'icon-cogs'
                    ),
                    'input' => $settings,
                    'submit' => array(
                        'title' => $this->l('Save'),
                    ),
                ),
            ),
        );

        $helper = $this->createFormHelper($field_forms, $form_name, $block_obj);

        foreach ($settings as $option) {
            if (isset($option['default']) && !(isset($helper->tpl_vars['fields_value'][$option['name']])
                    && $helper->tpl_vars['fields_value'][$option['name']])) {
                $helper->tpl_vars['fields_value'][$option['name']] = $option['default'];
            }
        }

        return $helper->generateForm($field_forms);
    }

    protected function getBlockFormFields($id_item = null)
    {
        $fields = array(
            'hooks' => array(
                'type' => 'select',
                'name' => 'hook',
                'class' => 't pspc-block-hook',
                'id' => 'hook'.($id_item ? $id_item : ''),
                'label' => $this->l('Position:'),
                'hint' => $this->l('Choose a hook for displaying this block.').' '.
                          $this->l('Please note that your theme may not support some hooks.'),
                'validate' => 'isCleanHtml',
                'options' => array(
                    'query' => $this->getBlockHookList(),
                    'id' => 'id_option',
                    'name' => 'name',
                ),
                'required' => true,
            ),
            array(
                'type' => 'text',
                'name' => 'title',
                'id' => 'title'.($id_item ? $id_item : ''),
                'label' => $this->l('Block title:'),
                'validate' => 'isCleanHtml',
                'required' => false,
                'lang' => true,
            ),
            array(
                'type' => 'text',
                'name' => 'product_count',
                'id' => 'product_count'.($id_item ? $id_item : ''),
                'label' => $this->l('Displayed products:'),
                'hint' => $this->l('Maximum number of products displayed in this block'),
                'validate' => 'isInt',
                'required' => true,
                'class' => 'fixed-width-sm',
                'default' => 12,
            ),
            array(
                'type' => 'product_sources',
                'name' => 'products',
                'id' => 'products'.($id_item ? $id_item : ''),
                'label' => $this->l('Products for display:'),
                'hint' => $this->l('Choose products for this block'),
                'required' => true,
                'validate' => 'isCleanHtml',
            ),
            array(
                'type' => 'select',
                'name' => 'sort',
                'class' => 't',
                'id' => 'sort'.($id_item ? $id_item : ''),
                'label' => $this->l('Sort products by:'),
                'validate' => 'isCleanHtml',
                'options' => array(
                    'query' => $this->getBlockSortOptions(),
                    'id' => 'id_option',
                    'name' => 'name',
                ),
                'required' => false,
            ),
            array(
                'type' => $this->getPSVersion() == 1.5 ? 'radio' : 'custom_switch',
                'name' => 'page',
                'id' => 'page'.($id_item ? $id_item : ''),
                'label' => $this->l('Create a separate page for this block:'),
                'hint' => $this->l('Create new page which will display products from this block. You can access it by direct link.'),
                'class' => 'pspc_block_page_radio',
                'values' => array(
                    array(
                        'id' => 'page'.($id_item ? $id_item : '').'_on',
                        'value' => 1,
                        'label' => $this->l('Yes'),
                    ),
                    array(
                        'id' => 'page'.($id_item ? $id_item : '').'_off',
                        'value' => 0,
                        'label' => $this->l('No'),
                    ),
                ),
                'default' => 0,
                'validate' => 'isInt',
            ),
            array(
                'type' => 'html',
                'name' => '',
                'html_content' => $this->renderBlockLink($id_item),
                'form_group_class' => 'pspc_block_page_options_row ps'.$this->getPSVersion(true),
            ),
            array(
                'type' => 'textarea',
                'lang' => true,
                'autoload_rte' => true,
                'name' => 'desc',
                'id' => 'desc'.($id_item ? $id_item : ''),
                'label' => $this->l('Description:'),
                'hint' => $this->l('Text description for this block. Will be displayed at the block page.'),
                'validate' => 'isCleanHtml',
                'required' => false,
                'form_group_class' => 'pspc_block_page_options_row ps'.$this->getPSVersion(true),
            ),
            array(
                'type' => 'text',
                'lang' => true,
                'name' => 'link_rewrite',
                'id' => 'link_rewrite'.($id_item ? $id_item : ''),
                'label' => $this->l('Link rewrite:'),
                'hint' => $this->l('Friendly URL for this block.'),
                'validate' => 'isCleanHtml',
                'required' => false,
                'form_group_class' => 'pspc_block_page_options_row ps'.$this->getPSVersion(true),
            ),
            array(
                'type' => 'text',
                'lang' => true,
                'name' => 'meta_title',
                'id' => 'meta_title'.($id_item ? $id_item : ''),
                'label' => $this->l('Meta title:'),
                'hint' => $this->l('Meta title for the block page. Necessary for SEO.'),
                'validate' => 'isCleanHtml',
                'required' => false,
                'form_group_class' => 'pspc_block_page_options_row ps'.$this->getPSVersion(true),
            ),
            array(
                'type' => 'text',
                'lang' => true,
                'name' => 'meta_desc',
                'id' => 'meta_desc'.($id_item ? $id_item : ''),
                'label' => $this->l('Meta description:'),
                'hint' => $this->l('Meta description of the block page. Necessary for SEO.'),
                'validate' => 'isCleanHtml',
                'required' => false,
                'form_group_class' => 'pspc_block_page_options_row ps'.$this->getPSVersion(true),
            ),
            array(
                'type' => 'text',
                'lang' => true,
                'name' => 'meta_keywords',
                'id' => 'meta_desc'.($id_item ? $id_item : ''),
                'label' => $this->l('Meta keywords:'),
                'hint' => $this->l('Meta keywords of the block page. Necessary for SEO.'),
                'validate' => 'isCleanHtml',
                'required' => false,
                'form_group_class' => 'pspc_block_page_options_row ps'.$this->getPSVersion(true),
            ),
            array(
                'type' => 'html',
                'name' => '',
                'html_content' => $this->renderAjaxFormParams('saveBlock'),
            ),
            array(
                'type' => 'html',
                'name' => '',
                'id' => 'error_container',
                'html_content' => $this->context->smarty->fetch(
                    $this->local_path . 'views/templates/admin/_errors_wrp.tpl'
                ),
            ),
        );

        if ($this->getPSVersion() == 1.7) {
            $fields['hooks'] = array(
                'type' => 'select',
                'name' => 'hook',
                'class' => 't',
                'id' => 'hook'.($id_item ? $id_item : ''),
                'label' => $this->l('Position:'),
                'hint' => $this->l('Choose a hook for displaying this block.').' '.
                          $this->l('Please note that your theme may not support some hooks.'),
                'validate' => 'isCleanHtml',
                'options' => array(
                    'optiongroup' => array(
                        'label' => 'name',
                        'query' => $this->getBlockHookList(),
                    ),
                    'options' => array(
                        'id' => 'id_option',
                        'name' => 'name',
                        'query' => 'query',
                    ),
                ),
                'required' => true,
            );
        }

        if ($this->getPSVersion() < 1.6) {
            foreach ($fields as &$item) {
                $desc = isset($item['desc']) ? $item['desc'] : '';
                $hint = isset($item['hint']) ? $item['hint'] . '<br/>' : '';
                $item['desc'] = $hint . $desc;
                $item['hint'] = '';
            }
        }

        $pspc_block = new PSPCBlock($id_item);
        if (!$pspc_block->id) {
            $pspc_block->source_all = true;
            $pspc_block->source_specific_prices = true;
            $pspc_block->source_pspc = true;
        }

        $this->context->smarty->assign(array(
            'pspc_all_timers' => PSPC::getAll(),
            'pspc_selected_timers' => PSPC::getBlockTimers($pspc_block->id),
            'pspc_block' => $pspc_block,
        ));

        return $fields;
    }

    protected function getBlockHookList($list = false)
    {
        $hooks = array(
            array(
                'id_option' => 'displayHome',
                'name' => $this->l('Home page'),
            ),
            array(
                'id_option' => 'displayLeftColumn',
                'name' => $this->l('Left column'),
            ),
            array(
                'id_option' => 'displayRightColumn',
                'name' => $this->l('Right column'),
            ),
            array(
                'id_option' => 'displayHomeTabContent',
                'name' => $this->l('Home page tab'),
            ),
            array(
                'id_option' => 'custom',
                'name' => $this->l('Custom'),
            ),
        );

        if ($this->getPSVersion() == 1.7) {
            $all_hooks = array();
            foreach (Hook::getHooks(false, true) as $hook) {
                if (strpos($hook['name'], 'displayAdmin') === false
                    && strpos($hook['name'], 'displayBackOffice') === false
                ) {
                    $all_hooks[] = array(
                        'id_option' => $hook['name'],
                        'name' => $hook['name'],
                    );
                }
            }

            if (!$list) {
                $result = array(
                    'main' => array(
                        'name' => $this->l('Main'),
                        'query' => $hooks,
                    ),
                    'all' => array(
                        'name' => $this->l('All'),
                        'query' => $all_hooks,
                    )
                );
            } else {
                $result = array_merge($hooks, $all_hooks);
            }

            $hooks = $result;
        }

        if ($list) {
            $tmp = array();
            foreach ($hooks as $hook) {
                if (!isset($tmp[$hook['id_option']])) {
                    $tmp[$hook['id_option']] = $hook['name'];
                }
            }
            $hooks = $tmp;
        }

        return $hooks;
    }

    protected function getBlockSortOptions()
    {
        $options = array(
            array(
                'id_option' => '',
                'name' => $this->l('Default'),
            ),
            array(
                'id_option' => 'ending_time',
                'name' => $this->l('Countdown ending time'),
            ),
            array(
                'id_option' => 'discount',
                'name' => $this->l('Discount amount'),
            ),
            array(
                'id_option' => 'random',
                'name' => $this->l('Random'),
            ),
        );

        return $options;
    }

    protected function renderBlockList()
    {
        $table = 'pspc_block';
        $helper = $this->createListHelper($table);
        $helper->actions = array('edit', 'delete');
        $helper->title = $this->l('Blocks');

        $fields_list = array(
            'id_pspc_block' => array(
                'title' => $this->l('ID'),
                'type' => 'text',
                'class' => 'fixed-width-xs',
                'search' => false,
                'orderby' => false,
            ),
            'hook' => array(
                'title' => $this->l('Hook'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
                'remove_onclick' => true,
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
                'remove_onclick' => true,
            ),
            'product_count' => array(
                'title' => $this->l('Displayed products'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
                'remove_onclick' => true,
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'type' => 'status',
                'search' => false,
                'orderby' => false,
                'remove_onclick' => true,
            ),
        );

        // if multishop
        if (Shop::isFeatureActive()) {
            $fields_list['shops'] = array(
                'title' => $this->l('Shops'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            );
        }

        $content = $this->getBlocksBO($helper->page, $helper->n);
        foreach ($content as &$block) {
            $block_obj = new PSPCBlock($block['id_pspc_block']);
            $block['form'] = $this->renderBlockForm(
                $this->l('Edit block'),
                $this->getBlockFormFields($block_obj->id),
                'pspc_block',
                $block_obj
            );

            if (Shop::isFeatureActive()) {
                $shops = PSPCBlock::getShopsStatic($block['id_pspc_block'], false);
                $shops_names = array();
                foreach ($shops as $id_shop) {
                    $shops_names[] = $this->getShopName($id_shop);
                }
                $block['shops'] = implode(', ', $shops_names);
            }
        }
        $helper->listTotal = $this->getBlocksListTotal();

        $this->context->smarty->assign(array(
            'form_id' => $table,
            'pspc_colspan' => (count($content) > 1 ? count($fields_list) + 2 : count($fields_list) + 1),
            'pspc_list_class' => 'pspc-block-list',
        ));

        return $helper->generateList($content, $fields_list);
    }

    public function getBlocksBO($page = 1, $n = 20)
    {
        $query =
            'SELECT bl.*, b.*, pspcs.*
			 FROM `'._DB_PREFIX_.'pspc_block` b
			 LEFT JOIN `'._DB_PREFIX_.'pspc_block_lang` bl
			  ON b.`id_pspc_block` = bl.`id_pspc_block` AND bl.`id_lang` = '.(int)$this->context->language->id.'
			 LEFT JOIN  `' . _DB_PREFIX_ . 'pspc_block_shop` pspcs
              ON b.`id_pspc_block` = pspcs.`id_pspc_block`
			 WHERE pspcs.`id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')
			 GROUP BY b.`id_pspc_block`
			 ORDER BY b.`id_pspc_block` ASC
			 LIMIT '.(((int)$page - 1) * (int)$n).', '.(int)$n;
        $blocks = Db::getInstance()->executeS($query);

        $hooks = $this->getBlockHookList(true);
        foreach ($blocks as &$block) {
            if (isset($hooks[$block['hook']])) {
                $block['hook'] = $hooks[$block['hook']];
            }
        }

        return $blocks;
    }

    public function getBlocksListTotal()
    {
        return Db::getInstance()->getValue(
            'SELECT COUNT(DISTINCT `id_pspc_block`)
             FROM `'._DB_PREFIX_.'pspc_block_shop`
             WHERE `id_shop` IN (' . implode(',', array_map('intval', Shop::getContextListShopID())) . ')'
        );
    }

    protected function resetColors()
    {
        $colors_data = $this->getColorsData();
        $theme = ($this->theme ? $this->theme : '1-simple.css');
        $theme = rtrim($theme, '.css'); //todo preg_replace
        foreach ($colors_data as $key => $item) {
            if ($theme == $item['theme']) {
                $conf_name = $this->settings_prefix . 'COLORS_' . $key;
                Configuration::updateValue(
                    $conf_name,
                    $item['default'],
                    true
                );
            }
        }
    }

    public function ajaxProcessSaveBlock()
    {
        $id_block = Tools::getValue('id_pspc_block');
        $block = new PSPCBlock($id_block);

        foreach (PSPCBlock::$definition['fields'] as $field_name => $field_data) {
            if (isset($field_data['lang']) && $field_data['lang']) {
                $block->{$field_name} = array();
                foreach (Language::getLanguages() as $lang) {
                    $block->{$field_name}[$lang['id_lang']] = trim(Tools::getValue($field_name.'_'.$lang['id_lang']));
                }
            } else {
                $block->$field_name = trim(Tools::getValue($field_name));
            }
        }

        // Default values
        $block->active = 1;

        // Check for errors
        $field_errors = $block->validateAllFields();

        // Check sources
        $sources = Tools::getValue('sources');
        $source_added = false;
        if (in_array('source_all', $sources)) {
            $source_added = true;
        } else {
            foreach ($sources as $source) {
                if (property_exists($block, $source)) {
                    $source_added = true;
                }
            }
            if (isset($sources['pspc']) && count($sources['pspc'])) {
                $source_added = true;
            }
        }
        if (!$source_added) {
            $field_errors[] = $this->l('Please choose products for displaying in this block');
        }

        // Save if no errors
        if (!(is_array($field_errors) && count($field_errors))) {
            if ($block->save()) {
                // Default values
                $block->clearSources();

                if (in_array('source_all', $sources)) {
                    $block->source_all = true;
                } else {
                    foreach ($sources as $source) {
                        if (property_exists($block, $source)) {
                            $block->{$source} = true;
                        }
                    }
                    if (!$block->source_pspc && isset($sources['pspc']) && count($sources['pspc'])) {
                        foreach ($sources['pspc'] as $id_pspc) {
                            $block->addSource($id_pspc);
                        }
                    }
                }
                
                if (!count($field_errors)) {
                    $block->save();
                    $this->clearSmartyCache();
                    die('1');
                }
            }
        }

        // Display errors if any
        if (is_array($field_errors) && count($field_errors)) {
            $html = '';
            foreach ($field_errors as $field_error) {
                $html .= $this->displayError($field_error);
            }
            if (Validate::isLoadedObject($block)) {
                $block->delete(); // delete created block if there were any errors
            }
            die($html);
        }

        die('0');
    }

    protected function renderAjaxFormParams($action)
    {
        $this->context->smarty->assign(array(
            'pspc_action' => $action
        ));

        return $this->context->smarty->fetch(
            $this->local_path . 'views/templates/admin/_ajax_form_params.tpl'
        );
    }

    public function getCategoryName($id_category)
    {
        $name = Db::getInstance()->getValue(
            'SELECT `name`
             FROM `'._DB_PREFIX_.'category_lang`
             WHERE `id_category` = '.(int)$id_category.'
             AND `id_lang` = '.(int)$this->context->language->id
        );

        return $name;
    }

    public function migrateTo20($skip_install = false)
    {
        if (!Configuration::get($this->settings_prefix.'UPDATED20')) {
            if (!$skip_install) {
                // Create new tables
                $this->installDB();
                // Default values
                $this->installDefaultSettings();
                // New hooks
                $this->installHooks();
            }

            require_once(_PS_MODULE_DIR_ . 'psproductcountdownpro/classes/PSPCUpgrade.php');
            PSPCUpgrade::migrateTo20($this);

            // Generate settings CSS
            $this->loadSettings();
            $this->regenerateCSS();
        }
    }

    public function ajaxProcessGetCountdownForm()
    {
        $id_pspc = Tools::getValue('id_pspc');

        die($this->renderCountdownForm($id_pspc));
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        if (isset($params['id_product']) && $params['id_product']) {
            $id_product = $params['id_product'];
        } else {
            $id_product = (int)Tools::getValue('id_product');
        }

        if (!$id_product) {
            return $this->adminDisplayWarning($this->l('You must save this product before using this module.'));
        }

        $token = Tools::getAdminTokenLite('AdminModules');
        $ajax_url = 'index.php?controller=AdminModules&configure=' . $this->name . '&token=' . $token;

        if (Validate::isLoadedObject($product = new Product($id_product))) {
            $this->context->smarty->assign(array(
                'psv' => $this->getPSVersion(),
                'module_name' => $this->name,
                'languages' => Language::getLanguages(),
                'specific_prices' => $this->getProductSpecificPrices($id_product),
                'pspc' => PSPC::findPSPC('product', $id_product, 0, null, false, true),
                'link' => $this->context->link,
                'ajax_url' => $ajax_url,
                'id_product' => $id_product,
            ));

            return $this->display(__FILE__, 'admin_products_extra'.$this->getPSVersion(true).'.tpl');
        }
    }

    /**
     * @param $id_product
     * @return array
     * returns specific prices where 'from' and 'to' dates are set
     */
    public function getProductSpecificPrices($id_product)
    {
        $prices = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT *
            FROM `' . _DB_PREFIX_ . 'specific_price`
            WHERE `id_product` = ' . (int)$id_product . ' AND `from` > 0 AND `to` > 0
             AND (`id_shop` = ' . (int)$this->context->shop->id . ' OR `id_shop` = 0)');

        return $prices;
    }

    /**
     * Ajax remove product countdown
     */
    public function ajaxProcessRemoveProductCountdown()
    {
        $id_countdown = Tools::getValue('id_countdown');

        if ($id_countdown) {
            $pspc = new PSPC($id_countdown);
            $pspc->delete();
            $this->clearSmartyCache();

            die('1');
        }

        die('0');
    }

    public function ajaxProcessProductUpdate()
    {
        $id_product = Tools::getValue('id_product');
        if (!$id_product) {
            die(json_encode(array('success' => 0, 'error' => $this->l('Unable to save the countdown'))));
        }

        $id_pspc = null;
        $success = false;
        //process only if it's a product page
        if (Tools::isSubmit($this->name . '-submit')) {
            $from = Tools::getValue('pspc_from');
            $to = Tools::getValue('pspc_to');
            $active = Tools::getValue('pspc_active');
            $id_pspc = Tools::getValue('id_pspc');

            // save / add
            $pspc = new PSPC($id_pspc);
            $pspc->from = $from;
            $pspc->to = $to;
            $pspc->active = $active;
            $pspc->action_start_done = 0;
            $pspc->action_end_done = 0;
            foreach (Language::getLanguages() as $language) {
                $id_lang = $language['id_lang'];
                $pspc->name[$id_lang] = Tools::getValue('pspc_name_'.$id_lang);
            }

            $filtered_name = array_filter($pspc->name);
            if (!$from && !$to && !$filtered_name && $active) {
                if ($id_pspc) {
                    // If 'from' and 'to' are not submitted and 'active' set to true, then just delete the countdown
                    $pspc = new PSPC($id_pspc);
                    $pspc->delete();
                    $this->clearSmartyCache();
                }
                // If 'from' and 'to' are not submitted, countdown doesn't exist yet and 'active' set to true, then just ignore the countdown
                die(json_encode(array('success' => 1, 'id_pspc' => (int)$id_pspc)));
            }

            if (!$pspc->from && !$pspc->personal) {
                $from_time = time() - (24 * 60 * 60); // now - 24h
                $pspc->from = date('Y-m-d H:i:s', $from_time);
            }

            // Check for errors
            $field_errors = $pspc->validateAllFields();
            if (is_array($field_errors) && count($field_errors)) {
                $this->errors += $field_errors;
            } else {
                if ($pspc->save(false, true, false)) {
                    $pspc->setObjects(array($id_product), 'product');
                    $pspc->saveSpecificPrices();
                    $id_pspc = $pspc->id;
                    $success = true;
                } else {
                    $this->errors[] = $this->l('Unable to save the countdown');
                }
            }

            // clear cache
            $this->clearSmartyCache();
        }

        if ($success) {
            die(json_encode(array('success' => 1, 'id_pspc' => $id_pspc)));
        } else {
            if (!$this->errors) {
                $this->errors[] = $this->l('Unable to save the countdown');
            }

            $errors = implode("/n", $this->errors);
            die(json_encode(array('success' => 0, 'error' => $errors)));
        }
    }
    
    public function getCategoriesParents($ids_categories)
    {
        if (is_array($ids_categories)) {
            $key = 'PSPC::getCategoriesParents_' . implode('-', $ids_categories);
            if (!Cache::isStored($key)) {
                $result = $ids_categories;
                foreach ($ids_categories as $id_category) {
                    $category = new Category($id_category);
                    $parents = Db::getInstance()->executeS(
                        'SELECT `id_category`
                         FROM `' . _DB_PREFIX_ . 'category`
                         WHERE `nleft` < ' . pSQL($category->nleft) . '
                          AND `nright` > ' . pSQL($category->nright)
                    );
                    foreach ($parents as $parent) {
                        $result[] = $parent['id_category'];
                    }
                }

                return $result;
            }

            return Cache::retrieve($key);
        }

        return $ids_categories;
    }

    public function getShopName($id_shop)
    {
        $shop = new Shop($id_shop, $this->context->language->id);
        
        return $shop->name;
    }

    public function genRandomString($length = 10)
    {
        $string = '';

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Generation
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    protected function executeProductAction($is_start_action, $action, $id_product, $id_product_attribute, $pspc)
    {
        $pspc_shops = $pspc->getShops();

        switch ($action) {
            case 'activate':
                Db::getInstance()->update(
                    'product_shop',
                    array('active' => 1),
                    '`id_product` = '.(int)$id_product.' AND `id_shop` IN
                         ('.implode(',', array_map('intval', $pspc_shops)).')'
                );
                break;
            case 'deactivate':
                Db::getInstance()->update(
                    'product_shop',
                    array('active' => 0),
                    '`id_product` = '.(int)$id_product.' AND `id_shop` IN
                         ('.implode(',', array_map('intval', $pspc_shops)).')'
                );
                break;
            case 'enable_orders':
                Db::getInstance()->update(
                    'product_shop',
                    array('available_for_order' => 1),
                    '`id_product` = '.(int)$id_product.' AND `id_shop` IN
                         ('.implode(',', array_map('intval', $pspc_shops)).')'
                );
                break;
            case 'disable_orders':
                Db::getInstance()->update(
                    'product_shop',
                    array('available_for_order' => 0),
                    '`id_product` = '.(int)$id_product.' AND `id_shop` IN
                         ('.implode(',', array_map('intval', $pspc_shops)).')'
                );
                break;
            case 'set_qty':
                foreach ($pspc_shops as $id_shop) {
                    $qty = ($is_start_action ? $pspc->action_start_qty : $pspc->action_end_qty);
                    StockAvailable::setQuantity(
                        $id_product,
                        $id_product_attribute,
                        $qty,
                        $id_shop
                    );
                    // if no combination specified, update all combinations
                    if (!$id_product_attribute) {
                        foreach (Product::getProductAttributesIds($id_product, $id_shop) as $ipa) {
                            $ipa = $ipa['id_product_attribute'];
                            StockAvailable::setQuantity(
                                $id_product,
                                $ipa,
                                $qty,
                                $id_shop
                            );
                        }
                    }
                }
                break;
        }
    }

    public function slashRedirect()
    {
        // remove the ending slash symbol
        $request_uri = $_SERVER['REQUEST_URI'];
        if (rtrim($request_uri, '/') != $request_uri) {
            $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));

            $url = "http".($force_ssl ? 's' : '')."://$_SERVER[HTTP_HOST]".rtrim($request_uri, '/');
            Tools::redirect($url);
        }
    }

    public function langRedirect()
    {
        // Prevent duplicate URLs by redirecting to link with lang code
        if (Language::isMultiLanguageActivated() && !Tools::getValue('id_lang') && $this->context->language->id) {
            $request_uri = $_SERVER['REQUEST_URI'];
            $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
            $iso = Language::getIsoById($this->context->language->id);

            $url = "http".($force_ssl ? 's' : '')."://$_SERVER[HTTP_HOST]/".$iso.$request_uri;
            Tools::redirect($url);
        }
    }

    public function hookModuleRoutes($params)
    {
        $routes = array(
            'module-psproductcountdownpro-block' => array(
                'controller' => 'block',
                'rule' => 'countdown/{link_rewrite}',
                'keywords' => array(
                    'link_rewrite' => array('regexp' => '[_a-zA-Z0-9_\-]+', 'param' => 'link_rewrite'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'psproductcountdownpro',
                    'url_rewrite' => 'countdown',
                ),
            ),
        );

        return $routes;
    }

    protected function renderBlockLink($id_block)
    {
        if (!$id_block) {
            return '';
        }

        $block = new PSPCBlock($id_block);
        $this->context->smarty->assign(array(
            'url' => $block->getUrl()
        ));

        return $this->context->smarty->fetch(
            $this->local_path . 'views/templates/admin/block_link.tpl'
        );
    }

    public function ajaxProcessDeleteExpired()
    {
        // Get all expired countdown ids
        $inactive_countdowns = Db::getInstance()->executeS(
            'SELECT `id_pspc`
             FROM `' . _DB_PREFIX_ . 'pspc`
             WHERE `to` < UTC_TIMESTAMP() AND `personal` = 0 AND `hours_restart` = 0'
        );

        // Convert them into one-dimensional array
        $ids = array_map('current', $inactive_countdowns);
        
        foreach ($ids as $id_pspc) {
            $pspc = new PSPC($id_pspc);
            $pspc->delete();
        }

        // Clear smarty cache
        $this->clearSmartyCache();
    }
}
