<?php
/**
*  @author WAGOOD <wagood@yandex.ru>
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

define('SMUSHIT_USER_AGENT', 'PRESTASHOP 1.7 reSMUSHIT'); // User agent for connecting
define('SMUSHIT_WINDOW', 5); // You can try to change number of curl connections
define('SMUSHIT_URL', 'http://api.resmush.it/ws.php'); // reSmushIt API url
/* set SMUSHIT_DEBUG to true and log request and response from resmush.it server
to /modules/smushit/log.txt file anddon't forget to set it to false back */
define('SMUSHIT_DEBUG', false);

class Smushit extends Module
{

    protected $smushit_user_agent;
    protected $smushit_url;
    protected $smushit_window;
    protected $imageTypes;

	public function __construct()
	{
		$this->name = 'smushit';
		$this->version = '1.0.4';
		$this->author = 'WAGOOD';
		$this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('reSmushIt', [], 'Modules.Smushit');
        $this->description = $this->trans('Optimize images by reSmush.It service.', [], 'Modules.Smushit');

        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);

        foreach (ImageType::getImagesTypes('products') as $type)
            $this->imageTypes[] = $type;

        $this->smushit_url = SMUSHIT_URL;
        $this->smushit_window = SMUSHIT_WINDOW;
        $this->smushit_user_agent = SMUSHIT_USER_AGENT;

        $configuration_jpeg_quality = (int)Configuration::get('PS_JPEG_QUALITY');
        if (isset($configuration_jpeg_quality) && $configuration_jpeg_quality > 0 && $configuration_jpeg_quality < 100)
            $this->smushit_url .= '?qlty='.$configuration_jpeg_quality;
    }

    public function install()
    {
        $result = parent::install() &&
            $this->registerHook('actionOnImageCutAfter') &&
            $this->registerHook('actionOnImageResizeAfter');

        if ($result)
        {
            Configuration::updateValue('SMUSHIT_LIGHT_MODE', 0);
            Configuration::updateValue('SMUSHIT_LOG_SIZE', 0);
        }

        return $result;
    }

    public function uninstall()
    {
        Configuration::deleteByName('SMUSHIT_LIGHT_MODE');
        Configuration::deleteByName('SMUSHIT_LOG_SIZE');

        return parent::uninstall() &&
            $this->unregisterHook('actionOnImageCutAfter') &&
            $this->unregisterHook('actionOnImageResizeAfter');
    }

    public function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        if (Tools::isSubmit('submitSmushitForm')) {

            foreach (array_keys($form_values) as $key)
                Configuration::updateValue($key, Tools::getValue($key));

            return $this->displayConfirmation($this->trans('The settings have been updated.', [], 'Admin.Notifications.Success'));
        }

    }

    public function getContent()
    {
        $this->postProcess();
        return $this->renderForm();
    }

    public function renderForm()
    {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->trans('Settings', [], 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type' => 'switch',
                        'label' => $this->trans('Light Mode', [], 'Modules.Smushit'),
                        'name' => 'SMUSHIT_LIGHT_MODE',
                        'desc' => $this->trans('If "Yes" only send one big image to optimize, if "No" send all images.', [], 'Modules.Smushit'),
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            ],
                        ],
                    ],
                ],
                'submit' => [
                    'title' => $this->trans('Save', [], 'Admin.Actions')
                ],
            ],
        ];

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSmushitForm';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'text_saving' => $this->trans('Total saved:', [], 'Modules.Smushit').' '.Configuration::get('SMUSHIT_LOG_SIZE').' '.$this->trans('bytes' , [], 'Modules.Smushit'),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm([$fields_form]);
    }

    public function getConfigFormValues()
    {
        return [
            'SMUSHIT_LIGHT_MODE' => Configuration::get('SMUSHIT_LIGHT_MODE', 0),
        ];
    }

    public function hookActionOnImageCutAfter ($params)
    {
        // $params = array('dst_file' => $destinationFile, 'file_type' => $fileType)
        return $this->smushitByImage($params['dst_file']);
    }

    public function hookActionOnImageResizeAfter ($params)
    {
        // $params = array('dst_file' => $destinationFile, 'file_type' => $fileType)
        return $this->smushitByImage($params['dst_file']);
    }

    protected function ActionWatermark($params)
    {
        $images_list = [];
        $image = new Image($params['id_image']);
        $image->id_product = $params['id_product'];
        $file = _PS_PROD_IMG_DIR_.$image->getExistingImgPath().'.jpg';

        array_push($images_list, $file);

        if (Configuration::get('SMUSHIT_LIGHT_MODE', 0) == 0)
        {
            //go through file formats and resize them
            foreach ($this->imageTypes as $imageType)
            {
                $newFile = _PS_PROD_IMG_DIR_.$image->getExistingImgPath().'-'.Tools::stripslashes($imageType['name']).'.jpg';
                array_push($images_list, $newFile);
            }
        }

        return $this->smushitByImage($images_list);
    }

    private function smushitByImage($file)
    {
        require_once _PS_MODULE_DIR_.$this->name.'/libs/RollingCurl/RollingCurl.php';
        $this->_images = $images_list = [];

        if (!is_array($file))
            $images_list[] = $file;
        else
            $images_list = $file;

        // multi connect to urls
        $rc = new RollingCurl(array(&$this, 'requestCallback'));
        $rc->window_size = $this->smushit_window;
        foreach ($images_list as $key => $image_file)
        {
            $this->_images[$key] = array('src' => $image_file);
            $request = new RollingCurlRequest($this->smushit_url, 'POST', array('files' => (self::getCurlValue($image_file))));
            $request->userdata = $key;
            $rc->add($request);
        }
        $rc->execute();
        $rc->__destruct();
    }

    public function requestCallback($response, $info, $request)
    {
        if ($info['http_code'] <> '200')
            return false;

        $response = json_decode($response);

        if (SMUSHIT_DEBUG === true) {
            $output = print_r($response, true);
            $output .= print_r($request, true);
            $this->savetolog($output);
        }

        if (isset($response->error))
            return false;

        if (isset($response->dest) && isset($response->percent) && $response->percent > 0)
            if (Tools::copy($response->dest, $this->_images[$request->userdata]['src']))
                Configuration::updateValue('SMUSHIT_LOG_SIZE', Configuration::get('SMUSHIT_LOG_SIZE', 0) + $response->src_size - $response->dest_size);

        return true;
    }

    private function savetolog($data)
    {
        @file_put_contents ( dirname(__FILE__).'/log.txt', $data, FILE_APPEND );
    }

    private static function getCurlValue($filename)
    {
        if (function_exists('curl_file_create'))
            return curl_file_create($filename);

        return '@'.$filename;
    }

}
