{extends file="helpers/form/form.tpl"}
{block name="legend"}
    <div class="panel-heading">
        {if isset($field.image) && isset($field.title)}<img src="{$field.image}" alt="{$field.title|escape:'html':'UTF-8'}" />{/if}
        {if isset($field.icon)}<i class="{$field.icon}"></i>{/if}
        {$field.title}
    </div>
    <div class="panel">
        <h3><i class="icon icon-tags"></i> {l s='Optimization results' mod='smushit'}</h3>
        <p>
            {$text_saving}
        </p>
    </div>

{/block}

