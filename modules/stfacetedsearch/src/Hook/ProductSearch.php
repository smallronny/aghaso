<?php
/**
 * 2007-2019 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\StFacetedSearch\Hook;

use PrestaShop\Module\StFacetedSearch\Product\SearchProvider;
use PrestaShop\Module\StFacetedSearch\URLSerializer;
use PrestaShop\Module\StFacetedSearch\Filters\Converter;

class ProductSearch extends AbstractHook
{
    const AVAILABLE_HOOKS = [
        'productSearchProvider',
        'filterProductSearch'
    ];

    /**
     * Hook project search provider
     *
     * @param array $params
     *
     * @return SearchProvider|null
     */
    public function productSearchProvider(array $params)
    {
        $query = $params['query'];
        // Get template for current page
        $id_object = 0;
        $id_shop = \Context::getContext()->shop->id;
        $page = \Dispatcher::getInstance()->getController();
        if ($page == 'category' && !($id_object = $query->getIdCategory())) {
            return null;
        }
        if ($page == 'manufacturer' && !($id_object = $query->getIdManufacturer())) {
            return null;
        }
        if ($page == 'supplier' && !($id_object = $query->getIdSupplier())) {
            return null;
        }
        // PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery
        if ($this->database->getValue('SELECT COUNT(0) FROM '._DB_PREFIX_.'st_search_category
            WHERE `page` = "'.$page.'" AND `id_object` = '.(int)$id_object.' AND `id_shop` = '.(int)$id_shop)) {
            /*$this->context->controller->registerJavascript(
                'stfacetedsearch_front',
                '/modules/stfacetedsearch/views/dist/front.js',
                ['position' => 'bottom', 'priority' => 100]
            );*/

            return new SearchProvider(
                $this->module,
                new Converter(
                    $this->module->getContext(),
                    $this->module->getDatabase()
                ),
                new URLSerializer()
            );
        }

        return null;
    }

    public function filterProductSearch($params)
    {
        if (!\Module::isEnabled('stfacetedsearch')) {
            return;
        }
        $context = \Context::getContext();
        if(isset($params['searchVariables']) && isset($params['searchVariables']['products'])) {
            $product_ipa_shown = array();
            foreach($params['searchVariables']['products'] as &$product) {
                $linkRewrite = isset($product['link_rewrite']) ? $product['link_rewrite'] : null;
                $category = isset($product['category']) ? $product['category'] : null;
                $ean13 = isset($product['ean13']) ? $product['ean13'] : null;
                $product_attribute = $product->getAttributes();
                // Check the id_product_attribute in the filters.
                $facetedSearchFilters = \StFacetedsearch::getFacetedSearchFilters();
                if (is_array($facetedSearchFilters) && key_exists('id_attribute_group', $facetedSearchFilters)) {
                    
                    foreach($facetedSearchFilters['id_attribute_group'] as $id_attribute_group => $id_attributes) {
                        if (\Db::getInstance()->getValue('SELECT COUNT(0) FROM '._DB_PREFIX_.'attribute_group WHERE group_type = "color" AND id_attribute_group='.(int)$id_attribute_group)) {
                            $id_attribute_array = \Db::getInstance()->executeS('
                                SELECT pac.id_product_attribute 
                                FROM '._DB_PREFIX_.'product_attribute_combination pac 
                                LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON pac.id_product_attribute = pa.id_product_attribute
                                WHERE pa.id_product = '.(int)$product['id_product'].' AND id_attribute IN ('.implode(',', $id_attributes).')');

                            foreach($id_attribute_array as $val) {
                                if (in_array($product['id_product'].'-'.$val['id_product_attribute'],$product_ipa_shown)) {
                                    continue;
                                }
                                $url = $context->link->getProductLink(
                                    $product['id_product'],
                                    $linkRewrite,
                                    $category,
                                    $ean13,
                                    $context->language->id,
                                    null,
                                    $val['id_product_attribute'],
                                    false,
                                    false,
                                    true
                                );
                                $url_name = 'canonical_url';
                                if (\Tools::version_compare(_PS_VERSION_, '1.7.6.1', '>')) {
                                    $url_name = 'url';
                                }
                                if (\is_object($product)) {
                                    $product->offsetSet($url_name, $url, true); 
                                } elseif (\is_array($product)) {
                                    $product[$url_name] = $url;
                                }
                                // select new attrinutes
                                $product_attribute = \Db::getInstance()->executeS('
                                    SELECT al.*,a.id_attribute_group FROM '._DB_PREFIX_.'attribute_lang al
                                    LEFT JOIN '._DB_PREFIX_.'product_attribute_combination pac ON al.id_attribute = pac.id_attribute AND al.id_lang='.\Context::getContext()->language->id.'
                                    LEFT JOIN '._DB_PREFIX_.'attribute a ON a.id_attribute=al.id_attribute
                                    WHERE pac.id_product_attribute = '.(int)$val['id_product_attribute'].'
                                ');
                                $product_ipa_shown[] = $product['id_product'].'-'.$val['id_product_attribute'];
                                break;
                            }
                        }
                    }
                }
                if (\Module::isEnabled('stproductsbyattrs') && \Configuration::get('ST_PRO_ATTR_ATTRIBUTE_NAME')) {
                    require_once(_PS_MODULE_DIR_.'stproductsbyattrs/classes/StProductsByAttrsClass.php');
                    $separator = \Configuration::get('ST_PRO_ATTR_SEPARATOR');
                    $separator || $separator = '-';
                    foreach($product_attribute as $val) {
                        if (\StProductsByAttrsClass::isAvailable($val['id_attribute_group'])) {
                            $product->name .= ' '.$separator.' '.$val['name'];
                        }
                    }
                }
            }
        }
    }
}
