<?php
/**
 * 2007-2019 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\StFacetedSearch\Filters;

use AttributeGroup;
use Category;
use Configuration;
use Context;
use Db;
use Feature;
use FeatureValue;
use Manufacturer;
use PrestaShop\PrestaShop\Core\Product\Search\Facet;
use PrestaShop\PrestaShop\Core\Product\Search\Filter;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\URLFragmentSerializer;
use PrestaShop\Module\StFacetedSearch\Filters\Transit;
use Tools;

class Converter
{
    const WIDGET_TYPE_CHECKBOX = 0;
    const WIDGET_TYPE_RADIO = 1;
    const WIDGET_TYPE_DROPDOWN = 2;
    const WIDGET_TYPE_SLIDER = 3;
    const WIDGET_TYPE_BUTTON = 4;
    const WIDGET_TYPE_COLORBOX = 5;
    const WIDGET_TYPE_IMAGE = 6;
    const WIDGET_TYPE_LINK = 7;
    
    const TYPE_ATTRIBUTE_GROUP = 'id_attribute_group';
    const TYPE_AVAILABILITY = 'availability';
    const TYPE_CATEGORY = 'category';
    const TYPE_CONDITION = 'condition';
    const TYPE_FEATURE = 'id_feature';
    const TYPE_QUANTITY = 'quantity';
    const TYPE_MANUFACTURER = 'manufacturer';
    const TYPE_PRICE = 'price';
    const TYPE_WEIGHT = 'weight';
    /**
     * @var array
     */
    const RANGE_FILTERS = [self::TYPE_PRICE, self::TYPE_WEIGHT];
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var Db
     */
    protected $database;

    public $module;

    public function __construct(Context $context, Db $database)
    {
        $this->context = $context;
        $this->database = $database;
    }

    public function getFacetsFromFilterBlocks(array $filterB)
    {

        $facets = [];

        $_map = array(
            self::TYPE_ATTRIBUTE_GROUP => 'ag',
            self::TYPE_AVAILABILITY => 'stock',
            self::TYPE_CATEGORY => 'subcategories',
            self::TYPE_CONDITION => 'condition',
            self::TYPE_FEATURE => 'feat',
            self::TYPE_MANUFACTURER => 'manufacturer',
            self::TYPE_PRICE => 'price_slider',
            self::TYPE_WEIGHT => 'weight_slider',
        );
        foreach ($filterB['filters'] as $filterBlock) {
            if (empty($filterBlock)) {
                // Empty filter, let's continue
                continue;
            }

            $facet = new Facet();
            $facet
                ->setLabel($filterBlock['name'])
                ->setProperty('filter_show_limit', $filterBlock['filter_show_limit'])
                ->setProperty('id_st_search_filter', $filterB['id_st_search_filter'])
                ->setMultipleSelectionAllowed(true);
            $facet_type = '';
            $facet_item_k = 0;


            $type = $filterBlock['type'];
            switch ($type) {
                case self::TYPE_CATEGORY:
                case self::TYPE_CONDITION:
                case self::TYPE_MANUFACTURER:
                case self::TYPE_QUANTITY:
                case self::TYPE_ATTRIBUTE_GROUP:
                case self::TYPE_FEATURE:
                    $type = $filterBlock['type'];
                    if ($filterBlock['type'] === self::TYPE_QUANTITY) {
                        $type = 'availability';
                    } elseif ($filterBlock['type'] == self::TYPE_ATTRIBUTE_GROUP) {
                        $type = 'attribute_group';
                        $facet->setProperty(self::TYPE_ATTRIBUTE_GROUP, $filterBlock['id_key']);
                        $facet_item_k = $filterBlock['id_key'];
                    } elseif ($filterBlock['type'] == self::TYPE_FEATURE) {
                        $type = 'feature';
                        $facet->setProperty(self::TYPE_FEATURE, $filterBlock['id_key']);
                        $facet_item_k = $filterBlock['id_key'];
                    }

                    $facet->setType($type);
                    $filters = [];
                    $values = array();
                    foreach ($filterBlock['values'] as $id => $filterArray) {
                        $filter = new Filter();
                        $filter
                            ->setType($type)
                            ->setLabel($filterArray['name'])
                            ->setMagnitude($filterArray['nbr'])
                            ->setValue($id);
                        if (array_key_exists('checked', $filterArray)) {
                            $filter->setActive($filterArray['checked']);
                        }

                        if (isset($filterArray['color'])) {
                            if ($filterArray['color'] != '') {
                                $filter->setProperty('color', $filterArray['color']);
                            } elseif (file_exists(_PS_COL_IMG_DIR_ . $id . '.jpg')) {
                                $filter->setProperty('texture', _THEME_COL_DIR_ . $id . '.jpg');
                            }
                        }
                        //st
                        $filter->setProperty('zhuangtai', isset($filterArray['zhuangtai']) ? $filterArray['zhuangtai'] : 0);
                        $filter->setProperty('img', isset($filterArray['img']) && $filterArray['img'] ? $this->fetchMediaServer($filterArray['img']) : '');
                        $filter->setProperty('width', isset($filterArray['width']) ? $filterArray['width'] : 0);
                        $filter->setProperty('height', isset($filterArray['height']) ? $filterArray['height'] : 0);
                        $filter->setProperty('show_img', isset($filterArray['show_img']) ? $filterArray['show_img'] : 0);
                        if($filterBlock['type'] == self::TYPE_FEATURE || $filterBlock['type'] == self::TYPE_ATTRIBUTE_GROUP)
                            $filter->setProperty('id_key', $filterBlock['id_key']);
                        $filters[] = $filter;
                        if($filterBlock['filter_type']==3 && (!isset($filterArray['zhuangtai']) || !$filterArray['zhuangtai']))
                            $values[] = $filterArray['name'];
                    }
                    /*if ((int) $filterBlock['filter_show_limit'] !== 0) {
                        usort($filters, array($this, 'sortFiltersByMagnitude'));
                    }*/
                    // $this->hideZeroValuesAndShowLimit($filters, (int) $filterBlock['filter_show_limit']);
                    $this->hideZeroValuesAndShowLimit($filters, 0);
                    /*if ((int) $filterBlock['filter_show_limit'] !== 0 || $filterBlock['type'] !== self::TYPE_ATTRIBUTE_GROUP) {
                        usort($filters, array($this, 'sortFiltersByLabel'));
                    }*/

                    if($filterBlock['filter_type']==3){
                        $prefix = $suffix = '';
                        /*foreach ($values as $k => $v) {
                            if(preg_match("/^([^\d]*)(.*?)([^\d]*)$/", $v,$match)){
                                if ($match[1] && !$prefix)
                                    $prefix = $match[1];
                                if ($match[2])
                                    $values[$k] = (float)$match[2];
                                else
                                    unset($values[$k]);
                                if ($match[3] && !$suffix)
                                    $suffix = $match[3];
                            }else{
                                unset($values[$k]);
                            }
                        }*/
                        if(count($values)){
                            // $facet->setProperty('min', $values[0])->setProperty('max', $values[count($values)-1]);
                            $facet->setProperty('min', 0)->setProperty('max', count($values)-1);
                            $facet->setProperty('values', $values);
                            $facet->setProperty('prefix', $prefix);
                            $facet->setProperty('suffix', $suffix);
                            $facet->setProperty('range', true);
                        }
                    }
                    // No method available to add all filters
                    foreach ($filters as $filter) {
                        $facet->addFilter($filter);
                    }
                    break;
                case self::TYPE_WEIGHT:
                case self::TYPE_PRICE:
                    $facet
                        ->setType($type)
                        ->setProperty('min', $filterBlock['min'])
                        ->setProperty('max', $filterBlock['max'])
                        ->setProperty('unit', $filterBlock['unit'])
                        ->setProperty('specifications', $filterBlock['specifications'])
                        ->setMultipleSelectionAllowed(false)
                        ->setProperty('range', true);

                    $filter = new Filter();
                    $filter
                        ->setActive($filterBlock['value'] !== null)
                        ->setType($type)
                        ->setMagnitude($filterBlock['nbr'])
                        ->setProperty('symbol', $filterBlock['unit'])
                        ->setValue($filterBlock['value']);

                    $facet->addFilter($filter);

                    break;
            }

            $multi = true;
            $facet_type = array_key_exists($filterBlock['type'], $_map) ? $_map[$filterBlock['type']] : '';
            if($facet_type){
                $facet_item = $this->database->getRow(
                    'SELECT * 
                    FROM ' . _DB_PREFIX_ . 'st_search_facet_item
                    WHERE id_st_search_filter = ' . (int) $filterB['id_st_search_filter'] . '
                    AND facet_type="'.$facet_type.'"
                    '.($facet_item_k ? ' AND facet_item_k='.$facet_item_k : '')
                );
                if($facet_item){
                    $facet->setProperty('facet_item', $facet_item);
                    $multi = $facet_item['multi']==1;
                }
            }

            switch ((int) $filterBlock['filter_type']) {
                case self::WIDGET_TYPE_CHECKBOX:
                    $facet->setMultipleSelectionAllowed(true);
                    $facet->setProperty('multi_selection', true);
                    $facet->setWidgetType('checkbox');
                    break;
                case self::WIDGET_TYPE_RADIO:
                    $facet->setMultipleSelectionAllowed(false);
                    $facet->setProperty('multi_selection', false);
                    $facet->setWidgetType('radio');
                    break;
                case self::WIDGET_TYPE_DROPDOWN:
                    $facet->setMultipleSelectionAllowed(false);
                    $facet->setProperty('multi_selection', false);
                    $facet->setWidgetType('dropdown');
                    break;
                case self::WIDGET_TYPE_SLIDER:
                    $facet->setMultipleSelectionAllowed($multi);
                    $facet->setProperty('multi_selection', $multi);
                    $facet->setWidgetType('slider');
                    break;
                case self::WIDGET_TYPE_BUTTON:
                    $facet->setMultipleSelectionAllowed($multi);
                    $facet->setProperty('multi_selection', $multi);
                    $facet->setWidgetType('button');
                    break;
                case self::WIDGET_TYPE_COLORBOX:
                    $facet->setMultipleSelectionAllowed($multi);
                    $facet->setProperty('multi_selection', $multi);
                    $facet->setWidgetType('colorbox');
                    break;
                case self::WIDGET_TYPE_IMAGE:
                    $facet->setMultipleSelectionAllowed($multi);
                    $facet->setProperty('multi_selection', $multi);
                    $facet->setWidgetType('image');
                    break;
                case self::WIDGET_TYPE_LINK:
                    $facet->setMultipleSelectionAllowed($multi);
                    $facet->setProperty('multi_selection', $multi);
                    $facet->setWidgetType('link');
                    break;
            }
            $facets[] = $facet;
        }

        return $facets;
    }
    public function fetchMediaServer($image)
    {
        $image = _THEME_PROD_PIC_DIR_.$image;
        return $this->context->link->protocol_content.Tools::getMediaServer($image).$image;
    }

    /**
     * @param ProductSearchQuery $query
     *
     * @return array
     */
    public function createFacetedSearchFiltersFromQuery(ProductSearchQuery $query)
    {
        $idShop = (int) $this->context->shop->id;
        $idLang = (int) $this->context->language->id;
        $page = \Dispatcher::getInstance()->getController();
        $parent = null;
        if ($page == 'category') {
            $idObject = (int) Tools::getValue(
                'id_category',
                Tools::getValue('id_category_layered', Configuration::get('PS_HOME_CATEGORY'))
            );
            $parent = new Category($idObject, $idLang);    
        } elseif ($page == 'manufacturer') {
            $idObject = (int) Tools::getValue('id_manufacturer');
        } else {
            $idObject = 0;
        }

        $searchFilters = [];

        /* Get the filters for the current page */
        $filters = $this->database->executeS(
            'SELECT type, id_value, filter_show_limit, filter_type FROM ' . _DB_PREFIX_ . 'st_search_category
            WHERE id_object = ' . (int) $idObject . '
            AND id_shop = ' . (int) $idShop . '
            AND page = "'.$page.'"
            GROUP BY `type`, id_value ORDER BY position ASC'
        );

        $urlSerializer = new URLFragmentSerializer();
        $facetAndFiltersLabels = $urlSerializer->unserialize($query->getEncodedFacets());
        foreach ($filters as $filter) {
            $filterLabel = $this->convertFilterTypeToLabel($filter['type']);

            switch ($filter['type']) {
                case self::TYPE_MANUFACTURER:
                    if (!isset($facetAndFiltersLabels[$filterLabel])) {
                        // No need to filter if no information
                        continue 2;
                    }

                    $manufacturers = Manufacturer::getManufacturers(false, $idLang);
                    $searchFilters[$filter['type']] = [];
                    foreach ($manufacturers as $manufacturer) {
                        if (in_array($manufacturer['name'], $facetAndFiltersLabels[$filterLabel])) {
                            $searchFilters[$filter['type']][$manufacturer['name']] = $manufacturer['id_manufacturer'];
                        }
                    }
                    break;
                case self::TYPE_QUANTITY:
                    if (!isset($facetAndFiltersLabels[$filterLabel])) {
                        // No need to filter if no information
                        continue 2;
                    }

                    $quantityArray = [
                        Transit::L(
                            'Not available'
                        ) => 0,
                        Transit::L(
                            'In stock'
                        ) => 1,
                    ];

                    $searchFilters[$filter['type']] = [];
                    foreach ($quantityArray as $quantityName => $quantityId) {
                        if (isset($facetAndFiltersLabels[$filterLabel]) && in_array($quantityName, $facetAndFiltersLabels[$filterLabel])) {
                            $searchFilters[$filter['type']][] = $quantityId;
                        }
                    }
                    break;
                case self::TYPE_CONDITION:
                    if (!isset($facetAndFiltersLabels[$filterLabel])) {
                        // No need to filter if no information
                        continue 2;
                    }

                    $conditionArray = [
                        Transit::L(
                            'New'
                        ) => 'new',
                        Transit::L(
                            'Used'
                        ) => 'used',
                        Transit::L(
                            'Refurbished'
                        ) => 'refurbished',
                    ];

                    $searchFilters[$filter['type']] = [];
                    foreach ($conditionArray as $conditionName => $conditionId) {
                        if (isset($facetAndFiltersLabels[$filterLabel]) && in_array($conditionName, $facetAndFiltersLabels[$filterLabel])) {
                            $searchFilters[$filter['type']][] = $conditionId;
                        }
                    }
                    break;
                case self::TYPE_FEATURE:
                    //numerical yao gai zhe li, hen nan perfect
                    $features = Feature::getFeatures($idLang);
                    foreach ($features as $feature) {
                        if ($filter['id_value'] == $feature['id_feature']
                            && isset($facetAndFiltersLabels[$feature['name']])
                        ) {
                            $featureValueLabels = $facetAndFiltersLabels[$feature['name']];
                            $featureValues = FeatureValue::getFeatureValuesWithLang($idLang, $feature['id_feature'], true);
                            foreach ($featureValues as $featureValue) {
                                if (in_array($featureValue['value'], $featureValueLabels)) {
                                    $searchFilters['id_feature'][$feature['id_feature']][] =
                                        $featureValue['id_feature_value'];
                                }
                            }
                        }
                    }
                    break;
                case self::TYPE_ATTRIBUTE_GROUP:
                    $attributesGroup = AttributeGroup::getAttributesGroups($idLang);
                    foreach ($attributesGroup as $attributeGroup) {
                        if ($filter['id_value'] == $attributeGroup['id_attribute_group']
                            && isset($facetAndFiltersLabels[$attributeGroup['public_name']])
                        ) {
                            $attributeLabels = $facetAndFiltersLabels[$attributeGroup['public_name']];
                            $attributes = AttributeGroup::getAttributes($idLang, $attributeGroup['id_attribute_group']);
                            foreach ($attributes as $attribute) {
                                if (in_array($attribute['name'], $attributeLabels)) {
                                    $searchFilters['id_attribute_group'][$attributeGroup['id_attribute_group']][] = $attribute['id_attribute'];
                                }
                            }
                        }
                    }
                    break;
                case self::TYPE_PRICE:
                case self::TYPE_WEIGHT:
                    if (isset($facetAndFiltersLabels[$filterLabel])) {
                        $filters = $facetAndFiltersLabels[$filterLabel];
                        if (isset($filters[1]) && isset($filters[2])) {
                            $from = $filters[1];
                            $to = $filters[2];
                            $searchFilters[$filter['type']][0] = $from;
                            $searchFilters[$filter['type']][1] = $to;
                        }
                    }
                    break;
                case self::TYPE_CATEGORY:

                    if (isset($facetAndFiltersLabels[$filterLabel])) {
                        if ($page == 'manufacturer') {
                            $idObject = Configuration::get('PS_HOME_CATEGORY');
                        }
                        foreach ($facetAndFiltersLabels[$filterLabel] as $queryFilter) {
                            $categories = $this->searchByNameAndParentCategoryId($idLang, $queryFilter, $filter['id_value'] ? (int) $filter['id_value'] : $idObject);
                            if ($categories) {
                                $searchFilters[$filter['type']][] = $categories['id_category'];
                            }
                        }
                    }
                    break;
                default:
                    if (isset($facetAndFiltersLabels[$filterLabel])) {
                        foreach ($facetAndFiltersLabels[$filterLabel] as $queryFilter) {
                            $searchFilters[$filter['type']][] = $queryFilter;
                        }
                    }
            }
        }

        // Remove all empty selected filters
        foreach ($searchFilters as $key => $value) {
            switch ($key) {
                case self::TYPE_PRICE:
                case self::TYPE_WEIGHT:
                    if ($value[0] === '' && $value[1] === '') {
                        unset($searchFilters[$key]);
                    }
                    break;
                default:
                    if ($value == '' || $value == []) {
                        unset($searchFilters[$key]);
                    }
                    break;
            }
        }

        return $searchFilters;
    }

    /**
     * Convert filter type to label
     *
     * @param string $filterType
     */
    private function convertFilterTypeToLabel($filterType)
    {
        switch ($filterType) {
            case self::TYPE_PRICE:
                return Transit::L('Price');
            case self::TYPE_WEIGHT:
                return Transit::L('Weight');
            case self::TYPE_CONDITION:
                return Transit::L('Condition');
            case self::TYPE_QUANTITY:
                return Transit::L('Availability');
            case self::TYPE_MANUFACTURER:
                return Transit::L('Brand');
            case self::TYPE_CATEGORY:
                return Transit::L('Categories');
            case self::TYPE_FEATURE:
            case self::TYPE_ATTRIBUTE_GROUP:
            default:
                return null;
        }
    }

    /**
     * Hide entries with 0 results
     * Hide depending of show limit parameter
     *
     * @param array $filters
     *
     * @return array
     */
    private function hideZeroValuesAndShowLimit(array $filters, $showLimit)
    {
        $count = 0;
        foreach ($filters as $filter) {
            if ($filter->getMagnitude() === 0
                || ($showLimit > 0 && $count >= $showLimit)
            ) {
                $filter->setDisplayed(false);
                continue;
            }

            ++$count;
        }

        return $filters;
    }

    /**
     * Sort filters by magnitude
     *
     * @param Filter $a
     * @param Filter $b
     *
     * @return int
     */
    private function sortFiltersByMagnitude(Filter $a, Filter $b)
    {
        $aMagnitude = $a->getMagnitude();
        $bMagnitude = $b->getMagnitude();
        if ($aMagnitude == $bMagnitude) {
            // Same magnitude, sort by label
            return $this->sortFiltersByLabel($a, $b);
        }

        return $aMagnitude > $bMagnitude ? -1 : +1;
    }

    /**
     * Sort filters by label
     *
     * @param Filter $a
     * @param Filter $b
     *
     * @return int
     */
    private function sortFiltersByLabel(Filter $a, Filter $b)
    {
        return strnatcmp($a->getLabel(), $b->getLabel());
    }

    public function searchByNameAndParentCategoryId($idLang, $categoryName, $idParent)
    {
        $parent = new \Category((int)$idParent);
        if (!$parent->id) {
            $parent = Category::getRootCategory();
        }
        return $this->database->getRow('
        SELECT c.*, cl.*
        FROM `' . _DB_PREFIX_ . 'category` c
        LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl
            ON (c.`id_category` = cl.`id_category`
            AND `id_lang` = ' . (int) $idLang . \Shop::addSqlRestrictionOnLang('cl') . ')
        WHERE `name` = \'' . pSQL($categoryName) . '\'
            AND c.`nleft` > '.$parent->nleft.' AND c.`nright` < '.$parent->nright);
    }
}
