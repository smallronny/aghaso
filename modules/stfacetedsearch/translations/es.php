<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{stfacetedsearch}prestashop>stfacetedsearch_5015f5f6af450ad9492bc74f1059244b'] = 'Filtrar por';
$_MODULE['<{stfacetedsearch}prestashop>transit_3601146c4e948c32b6424d2c0a7f0118'] = 'Precio';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_ad24ffca4f9e9c0c7e80fe1512df6db9'] = 'Relevante';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_05d61847840bcddb5d37374de3be0ccc'] = 'A - Z';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_e21f2ef58ff858167655c18a874f9706'] = 'Z - A';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_e2174d7435696694b8f4d984d99eef03'] = 'Menor Precio';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_0ee81913615b71aba7dc8bbf1f144672'] = 'Mayor Precio';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_d2aed35af5b3b2a8a8de1914911a5ed9'] = 'Menos vendido';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_9ff39273a271b8854e4c36ca0811f1b5'] = 'Más vendido';
$_MODULE['<{stfacetedsearch}prestashop>facets_146ffe2fd9fa5bec3b63b52543793ec7'] = 'Mostrar más';
$_MODULE['<{stfacetedsearch}prestashop>facets_c74ea6dbff701bfa23819583c52ebd97'] = 'Mostrar menos';
