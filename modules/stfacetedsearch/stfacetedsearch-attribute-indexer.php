<?php

require_once __DIR__ . '/../../config/config.inc.php';
require_once __DIR__ . '/stfacetedsearch.php';

if (substr(Tools::encrypt('stfacetedsearch/index'), 0, 10) != Tools::getValue('token') || !Module::isInstalled('stfacetedsearch')) {
    die('Bad token');
}

Shop::setContext(Shop::CONTEXT_ALL);

$stFacetedsearch = new StFacetedsearch();
$stFacetedsearch->indexAttributes();
$stFacetedsearch->indexFeatures();
$stFacetedsearch->indexAttributeGroup();

echo 1;
