<?php
class Product extends ProductCore
{
    public function getImages($id_lang, Context $context = null)
    {
        $images = parent::getImages($id_lang);
        if (!($result = $this->getColorIdImage($id_lang))) {
            return $images;
        }
        $cover_index = 0;
        foreach($images as $key => $val) {
            if ($val['cover']) {
                $cover_index = $key;
            }
            if (in_array($val['id_image'], $result)) {
                $images[$key]['cover'] = 1;
                $images[$cover_index]['cover'] = 0;
                // Switch position
                $tmp = $images[$key];
                $images[$key] = $images[$cover_index];
                $images[$cover_index] = $tmp;
                break;
            }
        }
        return $images;
    }

    public function getCombinationImages($id_lang)
    {
        if (!Combination::isFeatureActive()) {
            return false;
        }
        $combinationImages = parent::getCombinationImages($id_lang);
        if (!($result = $this->getColorIdImage($id_lang))) {
            return $combinationImages;
        }
        $combinationImagesNew = array();
        foreach($combinationImages as $ipa => $image) {
            foreach($image as $val) {
                if (in_array($val['id_image'], $result)) {
                    $combinationImagesNew[$ipa][] = $val;
                }    
            }
        }
        return $combinationImagesNew;
    }

    public function getColorIdImage($id_lang)
    {
        $result = array();
        if (Module::isEnabled('stfacetedsearch') && Tools::getValue('q')) {
            if (class_exists('StFacetedsearch')) {
                $facetedSearchFilters = StFacetedsearch::getFacetedSearchFilters();
                if (is_array($facetedSearchFilters) && key_exists('id_attribute_group', $facetedSearchFilters)) {
                    $combinationImages = parent::getCombinationImages($id_lang);
                    foreach($facetedSearchFilters['id_attribute_group'] as $id_attribute_group => $id_attributes) {
                        if (Db::getInstance()->getValue('SELECT COUNT(0) FROM '._DB_PREFIX_.'attribute_group WHERE group_type = "color" AND id_attribute_group='.(int)$id_attribute_group) || Configuration::get('ST_FAC_SEARCH_SHOW_ATTRIBUTE_IMAGE')) {
                            if($combinationImages) {
                                $id_attribute_array = Db::getInstance()->executeS('
                                    SELECT pac.id_product_attribute 
                                    FROM '._DB_PREFIX_.'product_attribute_combination pac 
                                    LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON pac.id_product_attribute = pa.id_product_attribute
                                    WHERE pa.id_product = '.(int)$this->id.' AND id_attribute IN ('.implode(',', $id_attributes).')');
                                $id_attribute_tmp = array();
                                foreach($id_attribute_array as $val) {
                                    $id_attribute_tmp[] = $val['id_product_attribute'];
                                }
                                foreach($combinationImages as $ipa => $row) {
                                    if (in_array($ipa, $id_attribute_tmp)) {
                                        foreach($row as $val) {
                                            $result[$val['id_image']] = $val['id_image'];   
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }
    
}
