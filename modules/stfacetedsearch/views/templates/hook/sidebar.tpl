{assign var='show_on' value=Configuration::get('ST_FAC_SEARCH_SHOW_ON')}
{assign var='loading_effect' value=Configuration::get('ST_FAC_SEARCH_LOADING_EFFECT')}
<div class="feds_offcanvas feds_show_on_{$show_on} feds_show_on_x">
	<div class="feds_offcanvas_background"></div>
	<div id="feds_offcanvas_search_filters" class="feds_offcanvas_content">
		{if $show_on==3 && isset($listing.rendered_facets)}
			{$listing.rendered_facets nofilter}
		{/if}
		<div class="feds_offcanvas_btn stfeds_flex_container">
			<a href="{$clear_all_link}" title="{l s='Reset' mod='stfacetedsearch'}" class="feds_link stfeds_flex_child">{l s='Reset' mod='stfacetedsearch'}</a>
			<a href="javascript:;" title="{l s='Done' mod='stfacetedsearch'}" class="feds_offcanvas_done feds_offcanvas_guan stfeds_flex_child">{l s='Done' mod='stfacetedsearch'}</a>
		</div>
	</div>
	<a href="javascript:;" class="feds_offcanvas_times feds_offcanvas_guan" title="{l s='Close' mod='stfacetedsearch'}">&times;</a>
</div>
<div id="feds_overlay" class="feds_overlay {if $loading_effect==2} feds_overlay_tr {else} feds_overlay_center stfeds_flex_container stfeds_flex_center {/if} feds_overlay_hide {if $loading_effect!=2} feds_overlay_click {/if}"><i class="feds_overlay_loader feds-spin5 feds_animate-spin"></i></div>