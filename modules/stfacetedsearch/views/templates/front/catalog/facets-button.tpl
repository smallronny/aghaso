{assign var="per_row_index" value=0}
{assign var="feds_showmore_watcher" value=0}
{if isset($facet.properties.facet_item) && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}<div class="feds_grid_view row">{/if}
              {foreach from=$facet.filters key=filter_key item="filter"}
                {if !$filter.displayed || $filter.properties.zhuangtai==2}
                  {continue}
                {/if}
                {if !$feds_showmore_watcher && $filter.properties.zhuangtai==1}{$feds_showmore_watcher=1}{/if}
                {if isset($facet.properties.facet_item)  && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}
                {$per_row_index = $per_row_index+1}
                <div class="{if $facet.properties.facet_item.per_row>0} col-md-{(12/$facet.properties.facet_item.per_row)|replace:'.':'-'} {if ($per_row_index)%$facet.properties.facet_item.per_row == 1} feds_first-item-of-descktop-line{/if} {/if} {if $facet.properties.facet_item.per_row_mobile>0} col-{(12/$facet.properties.facet_item.per_row_mobile)|replace:'.':'-'} col-xs-{(12/$facet.properties.facet_item.per_row_mobile)|replace:'.':'-'} {if ($per_row_index)%$facet.properties.facet_item.per_row_mobile == 1} feds_first-item-of-mobile-line{/if} {/if}">
                {/if}
                <div class="facet_filter_item_li feds_facet_item {if $facet.widgetType == 'image' && $filter.properties.img} feds_image_filter_{$facet.properties.facet_item.show_img} {/if} feds_item_{$filter.type}_{$filter.value}">
                  <div class="filter_zhuangtai feds_zhuangtai_{if $filter.properties.zhuangtai==1}2{else}1{/if}">
                    <a
                      href="{$filter.nextEncodedFacetsURL}"
                      class="feds_input-container feds_link {if $facet.widgetType == 'button'} feds_button {/if}
                      {if $filter.active} active {/if}
                      facet_input_{$_expand_id}_{$filter_key}
                      "
                      rel="nofollow"
                      title="{$filter.label}"
                    >
                      {if $facet.widgetType == 'image' && $filter.properties.img}<img src="{$filter.properties.img}" {if $filter.properties.width} width="{$filter.properties.width}" {/if} {if $filter.properties.height} height="{$filter.properties.height}" {/if} title="{$filter.label}" class="feds_image_filter_img">{/if}
                      <span class="feds_radio-label">
                      {$filter.label}
                      {if $filter.magnitude and $show_quantities}
                        <span class="magnitude">({$filter.magnitude})</span>
                      {/if}
                      </span>
                      <span class="feds_input-loading"><i class="feds-spin5 feds_animate-spin"></i></span>
                    </a>
                  </div>
                </div>
                {if isset($facet.properties.facet_item)  && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}</div>{/if}
              {/foreach}
              {if isset($facet.properties.facet_item)  && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}</div>{/if}
              {if $feds_showmore_watcher}<div class="feds_showmore {if isset($facet.properties.facet_item) && $facet.properties.facet_item.per_row>0}{else} inline_showmore {/if}"><div class="filter_zhuangtai"><a href="javascript:;" class="feds_showmore_button" title="{l s='+More' mod='stfacetedsearch'}"><span class="feds_text_showmore">{l s='+More' mod='stfacetedsearch'}</span><span class="feds_text_showless">{l s='-Less' mod='stfacetedsearch'}</span></a></div></div>{/if}