                <select class="form-control form-control-select feds_select">
                  <option value="">{l s='Please select' mod='stfacetedsearch'}</option>
                  {foreach from=$facet.filters item="filter"}
                      {if !$filter.displayed || $filter.properties.zhuangtai==2}
                        {continue}
                      {/if}
                        <option value="{$filter.nextEncodedFacetsURL}" {if $filter.active} selected="selected" {/if}>
                          {$filter.label}
                          {if $filter.magnitude and $show_quantities}
                            ({$filter.magnitude})
                          {/if}
                        </option>
                    {/foreach}
                </select>