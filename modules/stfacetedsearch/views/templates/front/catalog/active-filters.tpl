{**
  * 2007-2019 PrestaShop.
  *
  * NOTICE OF LICENSE
  *
  * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
  * that is bundled with this package in the file LICENSE.txt.
  * It is also available through the world-wide-web at this URL:
  * https://opensource.org/licenses/AFL-3.0
  * If you did not receive a copy of the license and are unable to
  * obtain it through the world-wide-web, please send an email
  * to license@prestashop.com so we can send you a copy immediately.
  *
  * DISCLAIMER
  *
  * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
  * versions in the future. If you wish to customize PrestaShop for your
  * needs please refer to http://www.prestashop.com for more information.
  *
  * @author    PrestaShop SA <contact@prestashop.com>
  * @copyright 2007-2019 PrestaShop SA
  * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
  * International Registered Trademark & Property of PrestaShop SA
  *}
{if $activeFilters|count}
<section id="js-active-search-filters" class="active_filters_block feds_active_filters feds_active_filters_{$show_active_filters}">
    <ul>
      {if $show_active_filters}
      {foreach from=$activeFilters item="filter"}
        {block name='active_filters_item'}
          <li class="filter-block">
            <a class="feds_link" href="{$filter.nextEncodedFacetsURL}">{$filter.label}{l s='(x)' mod='stfacetedsearch'}</a>
          </li>
        {/block}
      {/foreach}
      {/if}
      <li class="filter-block">
        <a class="feds_link" href="{$clear_all_link}">{if $show_active_filters}{l s='Clear all' d='Shop.Theme.Actions'}{l s='(x)' mod='stfacetedsearch'}{else}<i class="feds-cw"></i>{l s='Reset' d='Shop.Theme.Actions'}{/if}</a>
      </li>
    </ul>
</section>
{/if}
