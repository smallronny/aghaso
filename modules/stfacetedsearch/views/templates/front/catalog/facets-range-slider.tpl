{if ($with_inputs && ($facet.type=='price' || $facet.type=='weight')) || 
(isset($facet.properties.facet_item.tooltips) && $facet.properties.facet_item.tooltips) || 
(isset($facet.properties.facet_item.snap) && $facet.properties.facet_item.snap)}
<div class="st-range-top st-range-bar {if $with_inputs && ($facet.type=='price' || $facet.type=='weight')} with_inputs {elseif isset($facet.properties.facet_item.tooltips) && $facet.properties.facet_item.tooltips} space_for_tooltips {/if}">
                  {if $with_inputs && ($facet.type=='price' || $facet.type=='weight')}
                  <input class="st_lower_input form-control" />
                  {if !isset($facet.properties.facet_item.vertical) || !$facet.properties.facet_item.vertical}<div class="value-split">-</div><input class="st_upper_input form-control" />{/if}
                  {elseif isset($facet.properties.facet_item.snap) && $facet.properties.facet_item.snap}
                  <span class="value-lower"></span>
                  {if $facet.properties.multi_selection || $facet.type=='price' || $facet.type=='weight'}<span class="value-split">-</span>
                  <span class="value-upper"></span>{/if}
                  {/if}
                </div>
                {/if}
                <div class="st_range_inner">
                <div class="st-range" 
                data-jiazhong="{if $facet.type!='price' && $facet.type!='weight'}rangeslider{else}{$facet.type}{/if}" 
                data-url="{if $facet.type=='price' || $facet.type=='weight'}{$facet.filters.0.nextEncodedFacetsURL}{else}{$facet.properties.url}{/if}" 
                data-min="{$facet.properties.min}" 
                data-max="{$facet.properties.max}" 
                data-lower="{if isset($facet['properties']['lower'])}{$facet['properties']['lower']}{else}{$facet['properties']['min']}{/if}" 
                {if $facet.properties.multi_selection || $facet.type=='price' || $facet.type=='weight'} data-upper="{if isset($facet['properties']['upper'])}{$facet['properties']['upper']}{else}{$facet['properties']['max']}{/if}" {/if}
                data-nolower="{if isset($facet['properties']['nolower'])}1{else}0{/if}"
                data-values="{if isset($facet.properties.values)}{'#'|implode:$facet.properties.values}{/if}" 
                data-prefix="{if isset($facet.properties.prefix)}{$facet.properties.prefix}{/if}" 
                data-suffix="{if isset($facet.properties.suffix)}{$facet.properties.suffix}{/if}" 
                data-slider-label="{$facet.label}" 
                data-slider-unit="{if isset($facet.properties.unit)}{$facet.properties.unit}{/if}" 
                data-pips="{if isset($facet.properties.facet_item.pips)}{$facet.properties.facet_item.pips}{/if}"
                data-vertical="{if isset($facet.properties.facet_item.vertical)}{$facet.properties.facet_item.vertical}{/if}"
                data-tooltips="{if isset($facet.properties.facet_item.tooltips) && $facet.properties.facet_item.tooltips}1{/if}"
                data-numerical="{if isset($facet.properties.facet_item.numerical) && $facet.properties.facet_item.numerical}1{/if}"
                data-step="{if isset($facet.properties.facet_item.numerical_step)}{(int)$facet.properties.facet_item.numerical_step}{else}1{/if}"></div>
                </div>
                {if $with_inputs && ($facet.type=='price' || $facet.type=='weight') && isset($facet.properties.facet_item.vertical) && $facet.properties.facet_item.vertical}
                <div class="st-range-bottom st-range-bar {if $with_inputs && ($facet.type=='price' || $facet.type=='weight')} with_inputs {/if}">
                  <input class="st_upper_input form-control" />
                </div>
                {/if}