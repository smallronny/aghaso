<li class="st_flex_container" draggable="true">
    <div class="">
      <i class="icon-move sub_move i_btn"></i>
    </div>
    <div class="st_flex_child">
      {$item[$filter['v_k']]}
      <input type="hidden" name="{$filterId}_zhuangtai[{(int)$item[$filter['key']]}]" class="item_zhuangtai" value="{$zhuangtai}" />
    </div>
    <div class="filter_item_image">
      <div class="st_flex_container">
    	<input type="file" name="{$filterId}_img[{(int)$item[$filter['key']]}]">
    	{if isset($item['img']) && $item['img']}
    	<div><img src="{$item['img']}" width="60"/><a class="btn btn-default st_delete_image" data-id="{$item['id_st_search_facet_item']}" data-key="{$item['facet_item_value_k']}" href="javascript:;"><i class="icon-trash"></i></a></div>
    	{/if}
      </div>
    </div>
</li>