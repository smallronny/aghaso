{if isset($listing.rendered_facets) && $listing.rendered_facets}
{assign var='show_on' value=Configuration::get('ST_FAC_SEARCH_SHOW_ON')}
{assign var='show_on_mobile' value=Configuration::get('ST_FAC_SEARCH_SHOW_ON_MOBILE')}
<a href="javascript:;" class="feds_offcanvas_tri feds_offcanvas_tri_{$show_on} feds_offcanvas_tri_mobile_{if $show_on_mobile}1{/if} {if $show_on_mobile==2} feds_offcanvas_tri_tablet {/if} "><i class="feds-sliders"></i>{l s='Filters' mod='stfacetedsearch'}</a>
{if $show_on}
<div class="feds_horizontal_wrap">
{if $show_on==1 || $show_on==2}
<div class="feds_horizontal feds_horizontal_{if $show_on==2}dropdown{else}list{/if}">
	<div id="feds_search_filters" class="feds_show_on_{$show_on} {if $show_on!=2} feds_show_on_x {/if}">
	{$listing.rendered_facets nofilter}
	</div>
</div>
{/if}
</div>
{/if}
{/if}