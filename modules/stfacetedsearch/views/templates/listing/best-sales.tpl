{*
 * This file allows you to customize your best-sales page.
 * You can safely remove it if you want it to appear exactly like all other product listing pages
 *}
{extends file='catalog/listing/best-sales.tpl'}
{block name='product_list_top' prepend}
{include file='module:stfacetedsearch/views/templates/listing/center_column.tpl'}
{/block}
{block name='product_list_active_filters'}{/block}
