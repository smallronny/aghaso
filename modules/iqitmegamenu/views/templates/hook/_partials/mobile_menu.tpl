{*

* 2007-2017 IQIT-COMMERCE.COM

*

* NOTICE OF LICENSE

*

*  @author    IQIT-COMMERCE.COM <support@iqit-commerce.com>

*  @copyright 2007-2017 IQIT-COMMERCE.COM

*  @license   GNU General Public License version 2

*

* You can not resell or redistribute this software.

*

*}

  


<ul class="closeMenuMobile" style="
    border-bottom: 2px solid #f3f3f3;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-2">
    <a href="{$urls.base_url}" >

                <img class="logo img-fluid"

                     src="{$shop.logo}" {if isset($iqitTheme.rm_logo) && $iqitTheme.rm_logo != ''} srcset="{$iqitTheme.rm_logo} 2x"{/if}

                     alt="{$shop.name}">

            </a>
    </div>
    <div class="col-4">
        <a class="close" aria-label="Close" >
          <span><img src="/img/cms/svg/ico-closed.svg"></span>
        </a>
    </div>
</li>
    </ul>
    
{function name="mobile_links" nodes=[] first=false}

	{strip}

		{if $nodes|count}

			{if !$first}<ul>{/if}

			{foreach from=$nodes item=node}

				{if isset($node.title)}

					<li>{if isset($node.children)}<span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon

" aria-hidden="true"></i></span>{/if}<a {if isset($node.href)} href="{$node.href}" {/if}>{$node.title}</a>

						{if isset($node.children)}

							{mobile_links nodes=$node.children first=false}

						{/if}

					</li>

				{/if}

			{/foreach}

			{if !$first}</ul>{/if}

		{/if}

	{/strip}

{/function}





{if isset($menu)}

	{mobile_links nodes=$menu first=true}

{/if}

