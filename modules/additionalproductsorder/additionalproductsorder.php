<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

require_once(dirname(__FILE__).'/config/config.inc.php');

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class AdditionalProductsOrder extends LinevenApoModule implements WidgetInterface
{
    public static $template_standard_classic          = 'CLASSIC';       /* Standard template classic */
    public static $template_standard_list_a           = 'LIST_A';        /* Standard template Model A*/
    public static $template_standard_list_b           = 'LIST_B';        /* Standard template Model B*/
    public static $template_standard_thumbnails       = 'THUMBNAILS';    /* Standard template Thumbnails */
    public static $template_standard_theme            = 'THEME';         /* Standard template Theme */

    public static $product_visibility_both       = 'both';          /* Both */
    public static $product_visibility_catalog    = 'catalog';       /* Catalog */
    public static $product_visibility_search     = 'search';        /* Search */
    public static $product_visibility_none       = 'none';          /* None */
    
    public static $export_delimiter_comma           = 'CO';
    public static $export_delimiter_semicolon       = 'SC';
    public static $export_enclosure_quote           = 'QU';
    public static $export_enclosure_double_quote    = 'DQ';

    public static $order_page_hook_use_footer               = 'FOOTER';                  /* Footer */
    public static $product_page_hook_use_additional_info    = 'ADDITIONAL_INFO';         /* AdditionalInfos */
    public static $product_page_hook_use_product_actions    = 'PRODUCT_ACTIONS';         /* ProductActions */

    public static $hook_to_use_specific_module              = 'SPECIFIC_MODULE';         /* Specific module hook */
    public static $hook_to_use_specific_theme               = 'SPECIFIC_THEME';          /* Specific module hook */

    public static $refresh_method_nothing           = 'NOTHING';
    public static $refresh_method_reload            = 'RELOAD';
    public static $refresh_method_ajax              = 'AJAX';

    public static $research_priority_accessories        = 'ACCESSORIES';
    public static $research_priority_associations       = 'ASSOCIATIONS';
    public static $research_priority_purchased_together = 'PURCHASED_TOGETHER';
    public static $research_priority_nothing            = 'NOTHING';

    public static $sort_display_default             = 'DEFAULT';
    public static $sort_display_name                = 'NAME';
    public static $sort_display_price               = 'PRICE';
    public static $sort_display_random              = 'RANDOM';

    public static $sort_way_asc                     = 'asc';
    public static $sort_way_desc                    = 'desc';

    public static $hook_code_order                = 'OOP';
    public static $hook_code_extra_product        = 'OEP';

    public static $partner_reviews_module_lineven     = 'homecomments';
    public static $partner_reviews_module_prestashop  = 'productcomments';
    public static $partner_reviews_module_hook        = 'hook';

    public static $native_templates;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        $this->name = 'additionalproductsorder';
        $this->tab = 'merchandizing';
        $this->version = '3.0.4';
        $this->need_instance = 0;
        $this->author = 'Lineven';
        $this->module_key = '96586da3bc2edafde083a2597676c9df';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        
        parent::__construct();
        
        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('Additional products on order', 'additionalproductsorder');
        $this->description = $this->l('Increase sales by displaying additional products (cross selling) during the ordering process.', 'additionalproductsorder');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?', 'additionalproductsorder');

        // Context
        $this->module_context->setEnvironment(LinevenApoContext::$environment_production);

        // Configuration
        $configuration = LinevenApoConfiguration::getConfiguration();
        $configuration->setup(
            'APO',
            '5296',
            $this
        );
    }
    
    /**
     * Init.
     * @param boolean $force Force initialization
     *
     * @return void
     */
    public function init($force = false)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (!$this->isInit()) {
            if ($this->module_context->getApplication() == LinevenApoContext::$application_backoffice) {
                // Load backoffice settings
                $configuration->setBackofficeSettings(array(
                    'is_install_files' => true,
                    'is_backoffice_menu_available' => true
                ));
            }
            // Parent initialization
            parent::init($force);

            // Templates
            self::$native_templates = array(
                self::$template_standard_classic => $this->l('Classic', 'additionalproductsorder'),
                self::$template_standard_list_a => $this->l('List (Model A)', 'additionalproductsorder'),
                self::$template_standard_list_b => $this->l('List (Model B)', 'additionalproductsorder'),
                self::$template_standard_thumbnails => $this->l('Thumbnails', 'additionalproductsorder'),
                self::$template_standard_theme => $this->l('Theme', 'additionalproductsorder'),
            );

            // Add partners
            LinevenApoPartners::addPartner(
                self::$partner_reviews_module_lineven,
                'Lineven',
                self::$partner_reviews_module_lineven,
                'Go Reviews - Reviews, Advices, Ratings, SEO and Google Rich Snippets',
                '1.3.7',
                '1.4.1.0'
            );
            LinevenApoPartners::addPartner(
                self::$partner_reviews_module_prestashop,
                'Lineven',
                self::$partner_reviews_module_prestashop,
                'Prestashop - Product Comments',
                '4.0.0',
                '1.7.6.0'
            );
            $this->initializated();
        }
    }

    /**
     * Hook Header.
     * @param array $params
     * @return void / object
     */
    public function hookHeader($params)
    {
        $this->module_context->setApplication(LinevenApoContext::$application_frontoffice);
        $this->init();
        $controller = LinevenApoController::getController('Header', LinevenApoController::$controller_type_front_hook);
        $controller->params = $params;
        return $controller->run();
    }

    /**
     * Hook ShoppingCart.
     * @param Object $params
     * @return object
     */
    public function hookDisplayShoppingCartFooter($params)
    {
        if ($this->module_context->isModuleActive() &&
            Configuration::get('LINEVEN_APO_OOP_IS_ACTIVE') &&
            Configuration::get('LINEVEN_APO_OOP_HOOK_USE') == AdditionalProductsOrder::$order_page_hook_use_footer) {
            $this->module_context->setApplication(LinevenApoContext::$application_frontoffice);
            $this->init();
            $controller = LinevenApoController::getController(
                'ShoppingCart',
                LinevenApoController::$controller_type_front_hook
            );
            $controller->params = $params;
            return $controller->run();
        }
        return false;
    }

    /**
     * Hook called for product additional info.
     *
     * @param Object $params
     * @return void / object
     */
    public function hookDisplayProductAdditionalInfo($params)
    {
        if ($this->module_context->isModuleActive() &&
            !(Tools::isSubmit('action') &&  Tools::getValue('action') == 'quickview') &&
            Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE') &&
            Configuration::get('LINEVEN_APO_OEP_HOOK_USE') == AdditionalProductsOrder::$product_page_hook_use_additional_info) {
            $this->module_context->setApplication(LinevenApoContext::$application_frontoffice);
            $this->init();
            $controller = LinevenApoController::getController(
                'ExtraProductPage',
                LinevenApoController::$controller_type_front_hook
            );
            $controller->params = $params;
            return $controller->run();
        }
        return false;
    }

    /**
     * Hook called for product actions.
     *
     * @param Object $params
     * @return void / object
     */
    public function hookDisplayProductActions($params)
    {
        if ($this->module_context->isModuleActive() &&
            !(Tools::isSubmit('action') &&  Tools::getValue('action') == 'quickview') &&
            Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE') &&
            Configuration::get('LINEVEN_APO_OEP_HOOK_USE') == AdditionalProductsOrder::$product_page_hook_use_product_actions) {
            $this->module_context->setApplication(LinevenApoContext::$application_frontoffice);
            $this->init();
            $controller = LinevenApoController::getController(
                'ExtraProductPage',
                LinevenApoController::$controller_type_front_hook
            );
            $controller->params = $params;
            return $controller->run();
        }
        return false;
    }

    /**
     * Hook called for product extra.
     *
     * @param Object $params
     * @return void / object
     */
    public function hookDisplayAdditionalProductsExtraProduct($params)
    {
        if ($this->module_context->isModuleActive() &&
            !(Tools::isSubmit('action') &&  Tools::getValue('action') == 'quickview') &&
            Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE') &&
            Configuration::get('LINEVEN_APO_OEP_HOOK_USE') == AdditionalProductsOrder::$hook_to_use_specific_module) {
            $this->module_context->setApplication(LinevenApoContext::$application_frontoffice);
            $this->init();
            $controller = LinevenApoController::getController(
                'ExtraProductPage',
                LinevenApoController::$controller_type_front_hook
            );
            $controller->params = $params;
            return $controller->run();
        }
        return false;
    }

    public function renderWidget($hook_name, array $params)
    {
        if ($this->module_context->isModuleActive() &&
            !(Tools::isSubmit('action') &&  Tools::getValue('action') == 'quickview') &&
            Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF') &&
            ((Configuration::get('LINEVEN_APO_OOP_IS_ACTIVE') && Configuration::get('LINEVEN_APO_OOP_HOOK_USE') == AdditionalProductsOrder::$hook_to_use_specific_theme) ||
                (Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE') && Configuration::get('LINEVEN_APO_OEP_HOOK_USE') == AdditionalProductsOrder::$hook_to_use_specific_theme))) {
            $controller_name = '';
            $hooks_def = unserialize(Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF'));
            if (is_array($hooks_def) && array_key_exists($hook_name, $hooks_def)) {
                $controller_name = $hooks_def[$hook_name];
            }
            if ($controller_name != '') {
                $this->module_context->setApplication(LinevenApoContext::$application_frontoffice);
                $this->init();
                $controller = LinevenApoController::getController($controller_name, LinevenApoController::$controller_type_front_hook);
                $controller->params = $params;
                return $controller->run('widget');
            }
        }
        return false;
    }

    public function getWidgetVariables($hook_name, array $params)
    {
    }

    /**
     * Set media for backoffice.
     *
     * @return void
     */
    public static function setBackofficeMedia()
    {
        parent::setBackofficeMedia();
        
        // Add JS
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/admin/vendor/rgraph/RGraph.svg.common.core.js'
        );
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/admin/vendor/rgraph/RGraph.svg.common.tooltips.js'
        );
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/admin/vendor/rgraph/RGraph.svg.common.key.js'
        );
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/admin/vendor/rgraph/RGraph.svg.pie.js'
        );
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/admin/vendor/rgraph/RGraph.svg.line.js'
        );

        // Tooltipster
        Context::getContext()->controller->addCSS(
            LinevenApoTools::getBaseURI().'views/css/vendor/tooltipster/tooltipster.bundle.min.css'
        );
        Context::getContext()->controller->addCSS(
            LinevenApoTools::getBaseURI().'views/css/vendor/tooltipster/tooltipster-sideTip-light.min.css'
        );
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/vendor/tooltipster/tooltipster.bundle.min.js'
        );
    }
}
