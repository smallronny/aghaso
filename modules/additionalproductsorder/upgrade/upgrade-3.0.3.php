<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_3_0_3()
{
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/config/ads.inc.php');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/img/ads/');

    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PREVIOUS_VERSION', Configuration::get('LINEVEN_APO_LAST_VERSION'));
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '3.0.3');
    return true;
}
