ALTER TABLE  `PREFIX_lineven_apo` ADD  `name` text NULL AFTER `id_shop`;
ALTER TABLE  `PREFIX_lineven_apo` ADD  `short_description` text NULL AFTER `name`;
ALTER TABLE  `PREFIX_lineven_apo` ADD  `product_cart_id` int(10) NULL AFTER `category_id`;
ALTER TABLE  `PREFIX_lineven_apo` ADD  `order_display` int(10) NULL AFTER `product_id`;