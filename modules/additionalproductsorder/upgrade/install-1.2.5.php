<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

require_once(dirname(__FILE__).'/../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../init.php');
require_once(_PS_MODULE_DIR_.'/'._LINEVEN_MODULE_APO_DIRNAME_.'/'._LINEVEN_MODULE_APO_FILENAME_.'.php');

function upgrade_module_1_2_5()
{
    Configuration::updateValue('LINEVEN_APO_SUPPORT_DOJO_VRS', '1.9.1');

    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/classes/apo_linevenadmintab_1_0_0.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/classes/apo_linevenadmintab_1_0_0.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/classes/apo_linevenmodule_1_0_0.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/classes/apo_linevenmodule_1_0_0.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/classes/apo_linevenobjectmodel_1_0_0.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/classes/apo_linevenobjectmodel_1_0_0.php');
    }
    // Clean old directory if exists
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/css');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/js');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/img');

    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/public/images/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/public/scripts/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/public/styles/');

    return true;
}
