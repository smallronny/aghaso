<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_2_0_5()
{
    // New configuration
    Configuration::updateGlobalValue('LINEVEN_APO_REFRESH_MODE', 'RELOAD');
    Configuration::updateGlobalValue('LINEVEN_APO_REFRESH_DELAY', 300);

    // Old files
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/sql/');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/_partials/diagnostic/results_repair.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/img/ads/pcp.png');

    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PREVIOUS_VERSION', Configuration::get('LINEVEN_APO_LAST_VERSION'));
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '2.0.5');
    return true;
}
