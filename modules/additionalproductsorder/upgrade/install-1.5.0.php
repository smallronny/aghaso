<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_5_0()
{
    // Database
    if (! file_exists(dirname(__FILE__).'/upgrade-1.5.0.sql')) {
        return (false);
    } else {
        if (! $sql = Tools::file_get_contents(dirname(__FILE__).'/upgrade-1.5.0.sql')) {
            return (false);
        }
    }
    $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
    $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
    $sql = preg_split('/;\s*[\r\n]+/', $sql);
    foreach ($sql as $query) {
        Db::getInstance()->Execute(trim($query));
    }
    
    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '1.5.0');
    return true;
}
