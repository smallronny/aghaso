<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_3_1()
{
    Configuration::updateValue('LINEVEN_APO_COMBINATIONS_ALL', 0);
    Configuration::updateValue('LINEVEN_APO_COMBINATIONS_ADDCART', Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'));
    Configuration::updateValue('LINEVEN_APO_COMBINATIONS_DSP_OPT', 0);
    Configuration::updateValue('LINEVEN_APO_AVL_ZERO_QTY', 1);
    Configuration::updateValue('LINEVEN_APO_AVL_VISIBILITY', serialize(array('both', 'catalog', 'search')));
    
    // Icon for PS 1.6
    if (version_compare(_PS_VERSION_, '1.6.1.2', '>=')) {
        copy(
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/img/logo57.png',
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/logo.png'
        );
    }
    return true;
}
