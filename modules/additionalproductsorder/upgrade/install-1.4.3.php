<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_4_3()
{
    // Database
    if (! file_exists(dirname(__FILE__).'/upgrade-1.4.3.sql')) {
        return (false);
    } else {
        if (! $sql = Tools::file_get_contents(dirname(__FILE__).'/upgrade-1.4.3.sql')) {
            return (false);
        }
    }
    $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
    if (_PS_VERSION_ >= 1.4) {
        $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
    } else {
        $sql = str_replace('MYSQL_ENGINE', 'MyISAM', $sql);
    }
    $sql = preg_split('/;\s*[\r\n]+/', $sql);
    foreach ($sql as $query) {
        Db::getInstance()->Execute(trim($query));
    }
    
    // Configuration
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_ACTIVE', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_IS_DISPLAYED', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_ADD_CART', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_IS_VIEWED', 0);
    $date_now = new DateTime();
    $date_now->modify('first day of this month');
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_LAST_CLEAR', $date_now->format('Y-m-d'));
    if (Configuration::get('LINEVEN_APO_DISPLAYING_MODE') == 'LIST') {
        Configuration::updateGlobalValue('LINEVEN_APO_DISPLAYING_MODE', 'LIST_A');
    }
    
    // Manage new admin tab
    $sql_tab = 'UPDATE `'._DB_PREFIX_.'tab`
        SET
            `class_name` = \'AdminAssociations\'
        WHERE module = \'additionalproductsorder\'';
    Db::getInstance()->Execute(trim($sql_tab));
    
    // Design list
    $design_list = Db::getInstance()->ExecuteS(
        'SELECT * FROM `' . _DB_PREFIX_ . 'configuration`
        WHERE `name` = "LINEVEN_APO_DESIGN_LIST"'
    );
    if (count($design_list)) {
        foreach ($design_list as $row) {
            if ($row['value'] != '') {
                Configuration::updateValue(
                    'LINEVEN_APO_DESIGN_LIST_A',
                    $row['value'],
                    false,
                    $row['id_shop_group'],
                    $row['id_shop']
                );
            }
        }
    }

    // Delete config
    Configuration::deleteByName('LINEVEN_APO_BO_LIST_SORT_BY');
    Configuration::deleteByName('LINEVEN_APO_BO_LIST_SORT_WAY');
    Configuration::deleteByName('LINEVEN_APO_BO_LIST_FILTERS');
    Configuration::deleteByName('LINEVEN_APO_DESIGN_LIST');

    // Delete old config
    Configuration::deleteByName('LINEVEN_APO_HELP_TOOLTIP');
    Configuration::deleteByName('LINEVEN_APO_SUPPORT_IS_DEBUG');
    Configuration::deleteByName('LINEVEN_APO_SUPPORT_KEY');
    Configuration::deleteByName('LINEVEN_APO_REGISTER');
    Configuration::deleteByName('LINEVEN_APO_ACTIVE_LARGE_BDD');
    Configuration::deleteByName('LINEVEN_APO_LARGE_BDD_REF');
    Configuration::deleteByName('LINEVEN_APO_DOJO_VERSION');
    Configuration::deleteByName('LINEVEN_APO_DOJO_BO_ACTIVE');
    Configuration::deleteByName('LINEVEN_APO_DOJO_FO_ACTIVE');
    
    // Delete folders
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/library/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/css/vendor/font-awesome-4.3.0/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/fonts/vendor/font-awesome-4.3.0/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/layout/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/notifications/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/include/');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/config/config_backoffice.inc.php');

    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Alerts.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/AssociatedModules.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Associations.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Dashboard.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Layout.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/QuickLinks.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Settings.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Support.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Tools.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/front/Header.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/front/ShoppingCart.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/front/redirection.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/forms/admin/SettingsDisplaying.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/services/admin/Initialization.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/services/admin/Stores.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/services/module/Installation.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/services/module/QuickLinks.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/services/Product.php');
    
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/services/module/Support.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/_configure/helpers/form/form_group.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/messages.tpl');
    // For old versions
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/associations/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/default/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/settings/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/support/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/tab/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/tools/');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/footer.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/header.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/layout.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/messages.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/css/additionalproductsorder-list.css');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/AdminLinevenAdditionalPrdOrder.php');
    
    // Unactive module if templates change and not recompile templates in PS
    if (Configuration::get('PS_SMARTY_FORCE_COMPILE') == 0) {
        Module::disableByName('additionalproductsorder');
    }
    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_WARNING', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '1.4.3');
    return true;
}
