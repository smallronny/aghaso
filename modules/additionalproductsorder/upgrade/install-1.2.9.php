<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_2_9()
{
    Configuration::updateValue('LINEVEN_APO_THUMB_DISPLAY_HEIGHT', 150);

    /** Clean **/
    // Debug
    if (Configuration::get('LINEVEN_APO_SUPPORT_IS_DEBUG')) {
        Configuration::updateValue('LINEVEN_APO_IS_DEBUG_MODE', Configuration::get('LINEVEN_APO_SUPPORT_IS_DEBUG'));
        Configuration::deleteByName('LINEVEN_APO_SUPPORT_IS_DEBUG');
    }
    // Tooltip
    if (Configuration::get('LINEVEN_APO_SUPPORT_TOOLTIP')) {
        if (_PS_VERSION_ >= '1.5') {
            Configuration::updateGlobalValue('LINEVEN_APO_HELP_TOOLTIP', Configuration::get('LINEVEN_APO_SUPPORT_TOOLTIP'));
        } else {
            Configuration::updateValue('LINEVEN_APO_HELP_TOOLTIP', Configuration::get('LINEVEN_APO_SUPPORT_TOOLTIP'));
        }
        Configuration::deleteByName('LINEVEN_APO_SUPPORT_TOOLTIP');
    }
    // Dojo
    if (Configuration::get('LINEVEN_APO_SUPPORT_DOJO_VRS')) {
        Configuration::deleteByName('LINEVEN_APO_SUPPORT_DOJO_VRS');
    }
    if (_PS_VERSION_ >= '1.5') {
        Configuration::updateGlobalValue('LINEVEN_APO_DOJO_VERSION', '');
    } else {
        Configuration::updateValue('LINEVEN_APO_DOJO_VERSION', '');
    }
    //  BO
    if (Configuration::get('LINEVEN_APO_DOJO_BO_ACTIVE') === false) {
        if (_PS_VERSION_ >= '1.5') {
            Configuration::updateGlobalValue('LINEVEN_APO_DOJO_BO_ACTIVE', 1);
        } else {
            Configuration::updateValue('LINEVEN_APO_DOJO_BO_ACTIVE', 1);
        }
    }
    // FO
    if (Configuration::get('LINEVEN_APO_DOJO_FO_ACTIVE') === false) {
        if (_PS_VERSION_ >= '1.5') {
            Configuration::updateGlobalValue('LINEVEN_APO_DOJO_FO_ACTIVE', 0);
        } else {
            Configuration::updateValue('LINEVEN_APO_DOJO_FO_ACTIVE', 0);
        }
    }
    // Old configurations
    if (Configuration::get('LINEVEN_APO_SUPPORT_KEY')) {
        Configuration::deleteByName('LINEVEN_APO_SUPPORT_KEY');
    }

    if (Configuration::get('LINEVEN_APO_SUPPORT_CODE')) {
        Configuration::deleteByName('LINEVEN_APO_SUPPORT_CODE');
    }

    // Delete old files
    //LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/classes');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/backward_compatibility_apo');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/public/');

    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.0.1/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.1.0/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.1.1/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.0/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.1/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.2/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.3/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.4/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.5/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.6/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.7/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/upgrade/1.2.8/');

    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/AdminLinevenAPO.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/AdminLinevenAPO.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/AdminLinevenAPO15.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/AdminLinevenAPO15.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/compatibility.inc.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/compatibility.inc.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/loaddesign.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/loaddesign.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/postprocess.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/postprocess.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/upgrade.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/upgrade.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/front/controller.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/front/controller.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/list.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/list.tpl');
    }

    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/header-1.3.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/header-1.3.tpl');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/header.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/header.tpl');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/list.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/list.tpl');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/list-1.3.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/list-1.3.tpl');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/thumbnails-1.3.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/thumbnails-1.3.tpl');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/thumbnails.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/thumbnails.tpl');
    }
    return true;
}
