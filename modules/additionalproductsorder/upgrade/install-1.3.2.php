<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_3_2()
{
    
    // Database
    if (! file_exists(dirname(__FILE__).'/upgrade-1.3.2.sql')) {
        return (false);
    } else {
        if (! $sql = Tools::file_get_contents(dirname(__FILE__).'/upgrade-1.3.2.sql')) {
            return (false);
        }
    }
    $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
    if (_PS_VERSION_ >= 1.4) {
        $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
    } else {
        $sql = str_replace('MYSQL_ENGINE', 'MyISAM', $sql);
    }
    $sql = preg_split('/;\s*[\r\n]+/', $sql);
    foreach ($sql as $query) {
        Db::getInstance()->Execute(trim($query));
    }
    
    // Icon for PS 1.6
    if (version_compare(_PS_VERSION_, '1.6.1.2', '>=')) {
        copy(
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/img/logo57.png',
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/logo.png'
        );
    } else {
        copy(
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/img/logo32.png',
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/logo.png'
        );
    }
    return true;
}
