<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_4_2()
{
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/services/admin/Product.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/services/admin/Product.php');
    }

    return true;
}
