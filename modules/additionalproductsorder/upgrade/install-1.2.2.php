<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_2_2()
{
    // Configuration
    if (Configuration::get('LINEVEN_APO_SUPPORT_DOJO_VRS') === false) {
        if (_PS_VERSION_ >= '1.5') {
            Configuration::updateGlobalValue('LINEVEN_APO_SUPPORT_DOJO_VRS', '1.8.1');
        } else {
            Configuration::updateValue('LINEVEN_APO_SUPPORT_DOJO_VRS', '1.8.1');
        }
    }
    if (Configuration::get('LINEVEN_APO_TESTMODE_IS_DEBUG') === false) {
        Configuration::updateValue('LINEVEN_APO_SUPPORT_IS_DEBUG', 0);
    } else {
        Configuration::updateValue('LINEVEN_APO_SUPPORT_IS_DEBUG', Configuration::get('LINEVEN_APO_TESTMODE_IS_DEBUG'));
        Configuration::deleteByName('LINEVEN_APO_TESTMODE_IS_DEBUG');
    }
    if (Configuration::get('LINEVEN_APO_SUPPORT_TOOLTIP') === false) {
        if (_PS_VERSION_ >= '1.5') {
            Configuration::updateGlobalValue('LINEVEN_APO_SUPPORT_TOOLTIP', 1);
        } else {
            Configuration::updateValue('LINEVEN_APO_SUPPORT_TOOLTIP', 1);
        }
    }

    $support_code = Tools::substr(time(), 0, 4).'-'.Tools::substr(time(), 5, 4).'-'.Tools::substr(time(), 3, 2).Tools::substr(time(), 6, 2);
    if (_PS_VERSION_ >= '1.5') {
        Configuration::updateGlobalValue('LINEVEN_APO_SUPPORT_CODE', $support_code);
        Configuration::updateGlobalValue('LINEVEN_APO_SUPPORT_KEY', '');
    } else {
        Configuration::updateValue('LINEVEN_APO_SUPPORT_CODE', $support_code);
        Configuration::updateValue('LINEVEN_APO_SUPPORT_KEY', '');
    }

    return true;
}
