<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_5_1()
{
    // New config
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_ALL_DISPLAYED', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_EXP_DELIMTER', 'SC');
    Configuration::updateGlobalValue('LINEVEN_APO_STATS_EXP_ENCLOSURE', 'DQ');
    Configuration::updateGlobalValue('LINEVEN_APO_DISPLAYING_REDUCTION', 0);

    // Delete files
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/config/smarty.inc.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/css/lineven.css');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/css/module.css');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/js/lineven.js');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/js/module.js');

    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '1.5.1');
    return true;
}
