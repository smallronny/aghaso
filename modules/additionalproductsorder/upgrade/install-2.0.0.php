<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

require_once(dirname(__FILE__).'/../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../init.php');

function upgrade_module_2_0_0()
{
    // New configuration
    Configuration::updateGlobalValue('LINEVEN_APO_DESIGN_CLASSIC', '');

    // Clean old files/folders
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/docs/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/forms/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/models/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/services/');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/src/Core/Presenter/PresenterGlobalVariables.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/src/Core/Presenter/PrestashopVariablesPresenter_1.6.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/src/Core/Presenter/PrestashopVariablesPresenter_1.7.php');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/src/Module/Association/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/src/Module/Partners/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/support/');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/_partials/messages.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/_partials/notifications/for_popovers_demonstration.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/layout.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/js_variables.tpl');

    // Rename old template files
    LinevenApoTools::renameFile(
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/shoppingcart/shopping_cart.tpl',
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/shoppingcart/_old_shopping_cart.tpl'
    );
    LinevenApoTools::renameFile(
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/list_a.tpl',
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/_old_list_a.tpl'
    );
    LinevenApoTools::renameFile(
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/list_b.tpl',
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/_old_list_b.tpl'
    );
    LinevenApoTools::renameFile(
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/thumbnails.tpl',
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/_old_thumbnails.tpl'
    );
    LinevenApoTools::renameFile(
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/title_1.6.tpl',
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/_old_title_1.6.tpl'
    );
    LinevenApoTools::renameFile(
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/title_1.7.tpl',
        _PS_MODULE_DIR_.'/additionalproductsorder/views/templates/hook/_partials/_old_title_1.7.tpl'
    );

    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PREVIOUS_VERSION', Configuration::get('LINEVEN_APO_LAST_VERSION'));
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '2.0.0');
    return true;
}
