ALTER TABLE  `PREFIX_lineven_apo` ADD  `is_active_groups` int(1) DEFAULT 0 AFTER `product_id`;
CREATE TABLE IF NOT EXISTS `PREFIX_lineven_apo_groups` (
  `id_apo` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;