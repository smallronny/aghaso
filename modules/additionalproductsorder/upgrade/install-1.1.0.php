<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_1_0()
{
    if (Configuration::get('LINEVEN_APO_DISPLAYING_MODE') === false) {
        Configuration::updateValue('LINEVEN_APO_DISPLAYING_MODE', 'LIST');
    }
    if (Configuration::get('LINEVEN_APO_DISPLAYING_PRICE') === false) {
        Configuration::updateValue('LINEVEN_APO_DISPLAYING_PRICE', '1');
    }
    if (Configuration::get('LINEVEN_APO_DISPLAYING_CART') === false) {
        Configuration::updateValue('LINEVEN_APO_DISPLAYING_CART', '1');
    }
    if (Configuration::get('LINEVEN_APO_DEFAULT_DESIGN') === false) {
        Configuration::updateValue('LINEVEN_APO_DEFAULT_DESIGN', '1');
    }
    if (Configuration::get('LINEVEN_APO_DESIGN_LIST') === false) {
        Configuration::updateValue('LINEVEN_APO_DESIGN_LIST', '');
    }
    if (Configuration::get('LINEVEN_APO_DESIGN_THUMBNAILS') === false) {
        Configuration::updateValue('LINEVEN_APO_DESIGN_THUMBNAILS', '');
    }
    // Database
    if (! file_exists(dirname(__FILE__).'/upgrade-1.1.0.sql')) {
        return (false);
    } else {
        if (! $sql = Tools::file_get_contents(dirname(__FILE__).'/upgrade-1.1.0.sql')) {
            return (false);
        }
    }

    $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
    if (_PS_VERSION_ >= 1.4) {
        $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
    } else {
        $sql = str_replace('MYSQL_ENGINE', 'MyISAM', $sql);
    }
    $sql = preg_split('/;\s*[\r\n]+/', $sql);

    foreach ($sql as $query) {
        Db::getInstance()->Execute(trim($query));
    }
    if (_PS_VERSION_ < 1.5) {
        if (! file_exists(dirname(__FILE__).'/update-content-1.1.0.sql')) {
            return (false);
        } else {
            if (! $sql = Tools::file_get_contents(dirname(__FILE__).'/update-content-1.1.0.sql')) {
                return (false);
            }
        }
        $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
        if (_PS_VERSION_ >= 1.4) {
            $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
        } else {
            $sql = str_replace('MYSQL_ENGINE', 'MyISAM', $sql);
        }
        $sql = preg_split('/;\s*[\r\n]+/', $sql);

        foreach ($sql as $query) {
            Db::getInstance()->Execute(trim($query));
        }
    }
    return true;
}
