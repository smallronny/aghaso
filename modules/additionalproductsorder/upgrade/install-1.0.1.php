<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_0_1()
{
    if (Configuration::get('LINEVEN_APO_IMAGE_TYPE') === false) {
        Configuration::updateValue('LINEVEN_APO_IMAGE_TYPE', '1');
    }
}
