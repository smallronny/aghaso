<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_4_0()
{
    Configuration::updateValue('LINEVEN_APO_IS_DEBUG_MODE', 0);
    Configuration::updateValue('LINEVEN_APO_NOTIFICATION_INST', 0);
    Configuration::updateValue('LINEVEN_APO_PANEL_INFOS', 1);
    
    // Icon for PS 1.6
    if (version_compare(_PS_VERSION_, '1.6.1.2', '>=')) {
        copy(
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/img/logo57.png',
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/logo.png'
        );
    } else {
        copy(
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/img/logo32.png',
            _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/logo.png'
        );
    }
    
    // Delete files
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/backward_compatibility/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/backward_compatibility_apo/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/content/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/css/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/js/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/img/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/library/tab/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/library/form/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/library/navigation/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/library/page/');
    LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/library/view/');
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Default.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/controllers/admin/Default.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/forms/admin/support/Support.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/forms/admin/support/Support.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/forms/admin/Design.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/forms/admin/Design.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/forms/admin/Displaying.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/forms/admin/Displaying.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/services/Alerts.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/services/Alerts.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/services/Installation.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/services/Installation.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/services/QuickLinks.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/services/QuickLinks.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/services/Settings.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/services/Settings.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/services/Stores.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/services/Stores.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/services/Support.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/services/Support.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/AdminController.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/AdminController.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/FrontController.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/FrontController.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/ModuleFrontController_1_3.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/ModuleFrontController_1_3.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/ModuleFrontController_1_4.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/controller/ModuleFrontController_1_4.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Controller.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Controller.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Customer.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Customer.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Model.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Model.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Page.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Page.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Resource.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Resource.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Service.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Service.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Shop.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Shop.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/library/Tab.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/library/Tab.php');
    }
    return true;
}
