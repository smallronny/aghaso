<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_2_0()
{
    // Configuration
    if (Configuration::get('LINEVEN_APO_ACTIVE_BO_MENU') === false) {
        if (_PS_VERSION_ >= '1.5') {
            Configuration::updateGlobalValue('LINEVEN_APO_ACTIVE_BO_MENU', 0);
        } else {
            Configuration::updateValue('LINEVEN_APO_ACTIVE_BO_MENU', 0);
        }
    }

    if (Configuration::get('LINEVEN_APO_ACCESSORIES_PRIORITY') === false) {
        Configuration::updateValue('LINEVEN_APO_ACCESSORIES_PRIORITY', 'BEFORE');
    }
    if (Configuration::get('LINEVEN_APO_THUMB_DISPLAY_TITLE') === false) {
        Configuration::updateValue('LINEVEN_APO_THUMB_DISPLAY_TITLE', '0');
    }
    if (Configuration::get('LINEVEN_APO_THUMB_DISPLAY_DESC') === false) {
        Configuration::updateValue('LINEVEN_APO_THUMB_DISPLAY_DESC', '0');
    }
    if (Configuration::get('LINEVEN_APO_TESTMODE_ACTIVE') === false) {
        Configuration::updateValue('LINEVEN_APO_TESTMODE_ACTIVE', 0);
    }
    if (Configuration::get('LINEVEN_APO_TESTMODE_IP') === false) {
        Configuration::updateValue('LINEVEN_APO_TESTMODE_IP', '');
    }
    // Database
    if (! file_exists(dirname(__FILE__).'/upgrade-1.2.0.sql')) {
        return (false);
    } else {
        if (! $sql = Tools::file_get_contents(dirname(__FILE__).'/upgrade-1.2.0.sql')) {
            return (false);
        }
    }

    $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
    if (_PS_VERSION_ >= 1.4) {
        $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
    } else {
        $sql = str_replace('MYSQL_ENGINE', 'MyISAM', $sql);
    }
    $sql = preg_split('/;\s*[\r\n]+/', $sql);

    foreach ($sql as $query) {
        Db::getInstance()->Execute(trim($query));
    }
    
    // Define order
    $sql = 'SELECT `id` FROM `'._DB_PREFIX_.'lineven_apo` ORDER BY `id` ASC';
    $apo_list = Db::getInstance()->ExecuteS($sql);
    $order = 1;
    $count_apo_list = count($apo_list);
    for ($i = 0; $i < $count_apo_list; $i++) {
        // Update
        Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'lineven_apo` SET `order_display` = '.$order.' where id = '.$apo_list[$i]['id']);
        $order++;
    }

    // Clean directory
    if (is_dir(_PS_MODULE_DIR_.'/additionalproductsorder/css')) {
        LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/css');
    }
    if (is_dir(_PS_MODULE_DIR_.'/additionalproductsorder/scripts')) {
        LinevenApoTools::rrmdir(_PS_MODULE_DIR_.'/additionalproductsorder/scripts');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/additionalproductsorder.css')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/additionalproductsorder.css');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/additionalproductsorder.tpl')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/additionalproductsorder.tpl');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/install.sql')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/install.sql');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/subscription.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/subscription.php');
    }
    if (file_exists(_PS_MODULE_DIR_.'/additionalproductsorder/apo_lineven.php')) {
        unlink(_PS_MODULE_DIR_.'/additionalproductsorder/apo_lineven.php');
    }
    return true;
}
