CREATE TABLE IF NOT EXISTS `PREFIX_lineven_apo_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned,
  `id_shop` int(11) unsigned,
  `id_association` int(11) unsigned,
  `id_product` int(11) unsigned,
  `displayed` int(11) unsigned,
  `add_to_cart` int(11) unsigned,
  `click_to_view` int(11) unsigned,
  `date_add` DATETIME,
  PRIMARY KEY (`id`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;