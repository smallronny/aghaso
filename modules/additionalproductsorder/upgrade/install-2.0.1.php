<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

require_once(dirname(__FILE__).'/../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../init.php');

function upgrade_module_2_0_1()
{
    // New configuration
    $images_types = Db::getInstance()->ExecuteS(
        'SELECT * FROM `' . _DB_PREFIX_ . 'configuration`
        WHERE `name` = "LINEVEN_APO_IMAGE_TYPE"'
    );
    if (count($images_types)) {
        $products_image = ImageType::getImagesTypes();
        foreach ($images_types as $row) {
            $image_type_name = ImageType::getFormattedName('small');
            $image_type_width = 20;
            $image_type_height = 20;
            $found = false;
            foreach ($products_image as $type) {
                if ($type['id_image_type'] == (int)$row['value']) {
                    $image_type_name = $type['name'];
                    $image_type_width = $type['width'];
                    $image_type_height = $type['height'];
                    $found = true;
                    break;
                }
            }
            if ($found) {
                Configuration::updateValue(
                    'LINEVEN_APO_IMAGE_TYPE_NAME',
                    $image_type_name,
                    false,
                    $row['id_shop_group'],
                    $row['id_shop']
                );
                Configuration::updateValue(
                    'LINEVEN_APO_IMAGE_TYPE_WIDTH',
                    $image_type_width,
                    false,
                    $row['id_shop_group'],
                    $row['id_shop']
                );
                Configuration::updateValue(
                    'LINEVEN_APO_IMAGE_TYPE_HEIGHT',
                    $image_type_height,
                    false,
                    $row['id_shop_group'],
                    $row['id_shop']
                );
            }
        }
    }

    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PREVIOUS_VERSION', Configuration::get('LINEVEN_APO_LAST_VERSION'));
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '2.0.1');
    return true;
}
