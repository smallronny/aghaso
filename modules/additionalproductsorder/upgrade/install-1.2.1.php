<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 2.0.6)
 *
 * @author    Lineven
 * @copyright 2019 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

function upgrade_module_1_2_1()
{
    // Configuration
    if (Configuration::get('LINEVEN_APO_ACTIVE_LARGE_BDD') === false) {
        if (_PS_VERSION_ >= '1.5') {
            Configuration::updateGlobalValue('LINEVEN_APO_ACTIVE_LARGE_BDD', 0);
            Configuration::updateGlobalValue('LINEVEN_APO_LARGE_BDD_REF', 0);
        } else {
            Configuration::updateValue('LINEVEN_APO_ACTIVE_LARGE_BDD', 0);
            Configuration::updateValue('LINEVEN_APO_LARGE_BDD_REF', 0);
        }
        if (Configuration::get('LINEVEN_APO_TESTMODE_IS_DEBUG') === false) {
            Configuration::updateValue('LINEVEN_APO_TESTMODE_IS_DEBUG', 0);
        }
    }

    return true;
}
