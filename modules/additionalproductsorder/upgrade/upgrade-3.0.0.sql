ALTER TABLE `PREFIX_lineven_apo` CHANGE `category_id` `id_related_category` int(10) NULL;
ALTER TABLE `PREFIX_lineven_apo` CHANGE `product_cart_id` `id_related_product` int(10) NULL;
ALTER TABLE `PREFIX_lineven_apo` CHANGE `product_id` `product_id_deprecated` int(10) NULL;
ALTER TABLE `PREFIX_lineven_apo_groups` CHANGE `id_apo` `id_association` int(10) unsigned NOT NULL;
ALTER TABLE `PREFIX_lineven_apo` ADD `id_displayed_category` int(10) NULL DEFAULT NULL AFTER `id_related_product`;
ALTER TABLE `PREFIX_lineven_apo` ADD `hooks` varchar(255) NULL DEFAULT "" AFTER `is_active_groups`;