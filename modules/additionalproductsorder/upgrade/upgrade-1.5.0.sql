ALTER TABLE `PREFIX_lineven_apo` ADD `minimum_amount` float NULL DEFAULT 0 AFTER `product_id`;
ALTER TABLE `PREFIX_lineven_apo` ADD `maximum_amount` float NULL DEFAULT 0 AFTER `minimum_amount`;