<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

require_once(dirname(__FILE__).'/../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../init.php');

function upgrade_module_3_0_0()
{
    // Load module
    $module_class = _LINEVEN_MODULE_APO_CLASSNAME_;
    $module = new $module_class();
    $module->registerHook('displayProductAdditionalInfo');
    $module->registerHook('displayAdditionalProductsExtraProduct');
    $module->registerHook('displayProductActions');

    // Install new tables
    require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/DbTools.php');
    $db_tools = new LinevenApoDbTools();
    $db_tools->setInstallFiles(true);
    $db_tools->installTables();

    // Popuplate the products list table
    Db::getInstance()->Execute(
        'INSERT INTO `'._DB_PREFIX_.'lineven_apo_displayed_products`
        (`id_association`, `id_displayed_product`, `order_display`)
        (SELECT `id`, `product_id`, 1 FROM `'._DB_PREFIX_.'lineven_apo` )'
    );

    // Database
    if (! file_exists(dirname(__FILE__).'/upgrade-3.0.0.sql')) {
        return (false);
    } else {
        if (! $sql = Tools::file_get_contents(dirname(__FILE__).'/upgrade-3.0.0.sql')) {
            return (false);
        }
    }
    $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
    $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
    $sql = preg_split('/;\s*[\r\n]+/', $sql);
    foreach ($sql as $query) {
        Db::getInstance()->Execute(trim($query));
    }

    // Initialize titles
    $title_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
        array(
            'en' => 'To complete your order, we offer you those products',
            'fr' => 'Afin de compléter votre commande, nous vous proposons ces produits'
        )
    );
    $title_accessories_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
        array(
            'en' => 'Accessories',
            'fr' => 'Accessoires'
        )
    );
    $title_purchased_together_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
        array(
            'en' => 'Often purchased together',
            'fr' => 'Souvent achetés ensemble'
        )
    );

    $title_extra_product_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
        array(
            'en' => 'Your accessory',
            'fr' => 'Votre accessoire'
        )
    );

    // Default image to use
    $products_image = ImageType::getImagesTypes('products', true);
    $image_type = 1;
    $image_type_width = 20;
    $image_type_height = 20;
    if (count($products_image)) {
        $product_image = end($products_image);
        $image_type = $product_image['id_image_type'];
        $image_type_name = $product_image['name'];
        $image_type_width = $product_image['width'];
        $image_type_height = $product_image['height'];
    }

    // New configuration
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_IS_ACTIVE', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_ACTIVE_ASSOCIATIONS', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_ACTIVE_ACCESSORIES', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_HOOK_USE', 'ADDITIONAL_INFO');
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_TITLE', $title_extra_product_translations);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_MAX_PRODUCTS', 4);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_RESEARCH_PRIORITY', serialize(array('ACCESSORIES', 'ASSOCIATIONS')));
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_IMAGE_TYPE', $image_type);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_IMAGE_TYPE_NAME', $image_type_name);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_IMAGE_TYPE_WIDTH', $image_type_width);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_IMAGE_TYPE_HEIGHT', $image_type_height);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_DISPLAY_MODE', 'LIST_A');
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_DISPLAY_PRICE', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_DISPLAY_REDUCTION', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_DISPLAY_CART', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_DISPLAY_CART_CHK', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_DISPLAY_CART_CHK_ICO', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_THUMB_DISPLAY_TITLE', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_THUMB_DISPLAY_DESC', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_THUMB_DISPLAY_HEIGHT', 150);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_SORT_DISPLAY', 'DEFAULT');
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_SORT_DISPLAY_WAY', 'asc');
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_RANDOM_ASSOCIATIONS', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OEP_RANDOM_SEARCH', 0);

    Configuration::updateGlobalValue('LINEVEN_APO_OOP_IS_ACTIVE', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_ACTIVE_ASSOCIATIONS', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_ACTIVE_PURCHASED_TOGETHER', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_IS_SEPARATE_DISPLAY', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_TITLE_ACCESSORIES', $title_accessories_translations);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_TITLE_ASSOCIATIONS', $title_translations);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_TITLE_PURCHASED_TOGETHER', $title_purchased_together_translations);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_SORT_DISPLAY', 'DEFAULT');
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_SORT_DISPLAY_WAY', 'asc');
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_RANDOM_ASSOCIATIONS', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_RANDOM_SEARCH', 0);
    Configuration::updateGlobalValue('LINEVEN_APO_OOP_HOOK_USE', 'FOOTER');
    Configuration::updateGlobalValue('LINEVEN_APO_SPECIFIC_HOOK_DEF', serialize(array()));
    Configuration::updateGlobalValue('LINEVEN_APO_PARTNER_RVW_MODULE', 'homecomments');


    // Accessories priority
    $accessories_priority = Db::getInstance()->ExecuteS(
        'SELECT * FROM `'._DB_PREFIX_.'configuration`
            WHERE `name` = "LINEVEN_APO_ACCESSORIES_PRIORITY"'
    );
    if (count($accessories_priority)) {
        foreach ($accessories_priority as $row) {
            $default_priorities = serialize(array('ACCESSORIES', 'ASSOCIATIONS', 'NOTHING'));
            if ($row['value'] == 'AFTER') {
                $default_priorities = serialize(array('ASSOCIATIONS', 'ACCESSORIES', 'NOTHING'));
            }
            Configuration::updateValue(
                'LINEVEN_APO_OOP_RESEARCH_PRIORITY',
                $default_priorities,
                false,
                $row['id_shop_group'],
                $row['id_shop']
            );
        }
    }

    // Change old configuration to the new one
    update_configuration_3_0_0('LINEVEN_APO_ACTIVE_ACCESSORIES', 'LINEVEN_APO_OOP_ACTIVE_ACCESSORIES');
    update_configuration_3_0_0('LINEVEN_APO_TITLE', 'LINEVEN_APO_OOP_TITLE');
    update_configuration_3_0_0('LINEVEN_APO_MAX_PRODUCTS', 'LINEVEN_APO_OOP_MAX_PRODUCTS');
    update_configuration_3_0_0('LINEVEN_APO_IMAGE_TYPE', 'LINEVEN_APO_OOP_IMAGE_TYPE');
    update_configuration_3_0_0('LINEVEN_APO_IMAGE_TYPE_NAME', 'LINEVEN_APO_OOP_IMAGE_TYPE_NAME');
    update_configuration_3_0_0('LINEVEN_APO_IMAGE_TYPE_WIDTH', 'LINEVEN_APO_OOP_IMAGE_TYPE_WIDTH');
    update_configuration_3_0_0('LINEVEN_APO_IMAGE_TYPE_HEIGHT', 'LINEVEN_APO_OOP_IMAGE_TYPE_HEIGHT');
    update_configuration_3_0_0('LINEVEN_APO_DISPLAYING_MODE', 'LINEVEN_APO_OOP_DISPLAY_MODE');
    update_configuration_3_0_0('LINEVEN_APO_DISPLAYING_PRICE', 'LINEVEN_APO_OOP_DISPLAY_PRICE');
    update_configuration_3_0_0('LINEVEN_APO_DISPLAYING_REDUCTION', 'LINEVEN_APO_OOP_DISPLAY_REDUCTION');
    update_configuration_3_0_0('LINEVEN_APO_DISPLAYING_CART', 'LINEVEN_APO_OOP_DISPLAY_CART');
    update_configuration_3_0_0('LINEVEN_APO_THUMB_DISPLAY_TITLE', 'LINEVEN_APO_OOP_THUMB_DISPLAY_TITLE');
    update_configuration_3_0_0('LINEVEN_APO_THUMB_DISPLAY_DESC', 'LINEVEN_APO_OOP_THUMB_DISPLAY_DESC');
    update_configuration_3_0_0('LINEVEN_APO_THUMB_DISPLAY_HEIGHT', 'LINEVEN_APO_OOP_THUMB_DISPLAY_HEIGHT');
    update_configuration_3_0_0('LINEVEN_APO_REFRESH_MODE', 'LINEVEN_APO_OOP_REFRESH_MODE');
    update_configuration_3_0_0('LINEVEN_APO_REFRESH_DELAY', 'LINEVEN_APO_OOP_REFRESH_DELAY');

    // Delete old configuration
    Configuration::deleteByName('LINEVEN_APO_ACCESSORIES_PRIORITY');

    // Delete old files
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/classes/association/AssociationsSearch.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/classes/forms/admin/AssociationsAvailability.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/classes/forms/admin/AssociationsSettings.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/classes/forms/admin/Settings.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/classes/forms/admin/SettingsDisplay.php');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/associations/addedit/product_displayed_details.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/associations/addedit/product_displayed_settings.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/associations/addedit/product_selection_details.tpl');
    LinevenApoTools::deleteFile(_PS_MODULE_DIR_.'/additionalproductsorder/views/templates/admin/module/associations/addedit/product_selection_settings.tpl');

    // Update version
    Configuration::updateGlobalValue('LINEVEN_APO_NOTIFICATION_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PANEL_UPD', 1);
    Configuration::updateGlobalValue('LINEVEN_APO_PREVIOUS_VERSION', Configuration::get('LINEVEN_APO_LAST_VERSION'));
    Configuration::updateGlobalValue('LINEVEN_APO_LAST_VERSION', '3.0.0');
    return true;
}

/**
 * Update old/new configuration.
 */
function update_configuration_3_0_0($old_configuration_name, $new_configuration_name)
{
    Db::getInstance()->Execute(
        'UPDATE `'._DB_PREFIX_.'configuration`
            SET `name` = "'.$new_configuration_name.'"
            WHERE `name` = "'.$old_configuration_name.'" '
    );
}
