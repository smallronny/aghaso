<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

return array(
    array('header', array('1.7'), null, array('alias' => array('displayHeader'))),
    array('displayProductAdditionalInfo', array('1.7')),
    array('displayProductActions', array('1.7')),
    array('displayAdditionalProductsExtraProduct', array('1.7')),
    array('displayShoppingCartFooter', array('1.7'))
);
