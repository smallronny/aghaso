<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

// Initialize titles
$title_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
    array(
        'en' => 'To complete your order, we offer you those products',
        'fr' => 'Afin de compléter votre commande, nous vous proposons ces produits',
        'es' => 'Para completar su pedido, ofrecemos estos productos',
        'it' => 'Per completare il tuo ordine, offriamo questi prodotti',
        'pt' => 'Para concluir seu pedido, oferecemos esses produtos'
    )
);
$title_accessories_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
    array(
        'en' => 'Accessories',
        'fr' => 'Accessoires',
        'es' => 'Accesorios',
        'it' => 'Accessori',
        'pt' => 'Acessórios'
    )
);
$title_purchased_together_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
    array(
        'en' => 'Often purchased together',
        'fr' => 'Souvent achetés ensemble',
        'es' => 'A menudo comprados juntos',
        'it' => 'Spesso acquistato insieme',
        'pt' => 'Frequentemente comprados juntos'
    )
);
$title_extra_product_translations = LinevenApoTranslator::setDefaultTranslationsInArray(
    array(
        'en' => 'Your accessory',
        'fr' => 'Votre accessoire',
        'es' => 'Su accesorio',
        'it' => 'Il tuo accessorio',
        'pt' => 'Seu acessório'
    )
);

// Default image to use
$products_image = ImageType::getImagesTypes('products', true);
$image_type = 1;
$image_type_width = 20;
$image_type_height = 20;
if (count($products_image)) {
    $product_image = end($products_image);
    $image_type = $product_image['id_image_type'];
    $image_type_name = $product_image['name'];
    $image_type_width = $product_image['width'];
    $image_type_height = $product_image['height'];
}

return array(
    array('LINEVEN_APO_NOTIFICATION_INST', 0, true),
    array('LINEVEN_APO_NOTIFICATION_UPD', 0, true),
    array('LINEVEN_APO_ACTIVE', 0),
    array('LINEVEN_APO_OEP_IS_ACTIVE', 0),
    array('LINEVEN_APO_OEP_ACTIVE_ASSOCIATIONS', 0),
    array('LINEVEN_APO_OEP_ACTIVE_ACCESSORIES', 0),
    array('LINEVEN_APO_OEP_HOOK_USE', 'ADDITIONAL_INFO'),
    array('LINEVEN_APO_OEP_TITLE', $title_extra_product_translations),
    array('LINEVEN_APO_OEP_MAX_PRODUCTS', 4),
    array('LINEVEN_APO_OEP_RESEARCH_PRIORITY', serialize(array('ACCESSORIES', 'ASSOCIATIONS'))),
    array('LINEVEN_APO_OEP_IMAGE_TYPE', $image_type),
    array('LINEVEN_APO_OEP_IMAGE_TYPE_NAME', $image_type_name),
    array('LINEVEN_APO_OEP_IMAGE_TYPE_WIDTH', $image_type_width),
    array('LINEVEN_APO_OEP_IMAGE_TYPE_HEIGHT', $image_type_height),
    array('LINEVEN_APO_OEP_DISPLAY_MODE', 'LIST_A'),
    array('LINEVEN_APO_OEP_DISPLAY_PRICE', 1),
    array('LINEVEN_APO_OEP_DISPLAY_REDUCTION', 0),
    array('LINEVEN_APO_OEP_DISPLAY_CART', 0),
    array('LINEVEN_APO_OEP_DISPLAY_CART_CHK', 1),
    array('LINEVEN_APO_OEP_DISPLAY_CART_CHK_ICO', 0),
    array('LINEVEN_APO_OEP_THUMB_DISPLAY_TITLE', 0),
    array('LINEVEN_APO_OEP_THUMB_DISPLAY_DESC', 0),
    array('LINEVEN_APO_OEP_THUMB_DISPLAY_HEIGHT', 150),
    array('LINEVEN_APO_OEP_SORT_DISPLAY', 'DEFAULT'),
    array('LINEVEN_APO_OEP_SORT_DISPLAY_WAY', 'asc'),
    array('LINEVEN_APO_OEP_RANDOM_ASSOCIATIONS', 0),
    array('LINEVEN_APO_OEP_RANDOM_SEARCH', 0),
    array('LINEVEN_APO_OOP_IS_ACTIVE', 1),
    array('LINEVEN_APO_OOP_ACTIVE_ASSOCIATIONS', 1),
    array('LINEVEN_APO_OOP_ACTIVE_ACCESSORIES', 0),
    array('LINEVEN_APO_OOP_ACTIVE_PURCHASED_TOGETHER', 0),
    array('LINEVEN_APO_OOP_HOOK_USE', 'FOOTER'),
    array('LINEVEN_APO_OOP_IS_SEPARATE_DISPLAY', 0),
    array('LINEVEN_APO_OOP_TITLE', $title_translations),
    array('LINEVEN_APO_OOP_TITLE_ACCESSORIES', $title_accessories_translations),
    array('LINEVEN_APO_OOP_TITLE_ASSOCIATIONS', $title_translations),
    array('LINEVEN_APO_OOP_TITLE_PURCHASED_TOGETHER', $title_purchased_together_translations),
    array('LINEVEN_APO_OOP_MAX_PRODUCTS', 5),
    array('LINEVEN_APO_OOP_RESEARCH_PRIORITY', serialize(array('ACCESSORIES', 'ASSOCIATIONS', 'PURCHASED_TOGETHER'))),
    array('LINEVEN_APO_OOP_IMAGE_TYPE', $image_type),
    array('LINEVEN_APO_OOP_IMAGE_TYPE_NAME', $image_type_name),
    array('LINEVEN_APO_OOP_IMAGE_TYPE_WIDTH', $image_type_width),
    array('LINEVEN_APO_OOP_IMAGE_TYPE_HEIGHT', $image_type_height),
    array('LINEVEN_APO_OOP_DISPLAY_MODE', 'LIST_A'),
    array('LINEVEN_APO_OOP_DISPLAY_PRICE', 1),
    array('LINEVEN_APO_OOP_DISPLAY_REDUCTION', 0),
    array('LINEVEN_APO_OOP_DISPLAY_CART', 1),
    array('LINEVEN_APO_OOP_THUMB_DISPLAY_TITLE', 0),
    array('LINEVEN_APO_OOP_THUMB_DISPLAY_DESC', 0),
    array('LINEVEN_APO_OOP_THUMB_DISPLAY_HEIGHT', 150),
    array('LINEVEN_APO_OOP_REFRESH_MODE', 'NOTHING'),
    array('LINEVEN_APO_OOP_REFRESH_DELAY', 300),
    array('LINEVEN_APO_OOP_SORT_DISPLAY', 'DEFAULT'),
    array('LINEVEN_APO_OOP_SORT_DISPLAY_WAY', 'asc'),
    array('LINEVEN_APO_OOP_RANDOM_ASSOCIATIONS', 0),
    array('LINEVEN_APO_OOP_RANDOM_SEARCH', 0),
    array('LINEVEN_APO_COMBINATIONS_ALL', 0),
    array('LINEVEN_APO_COMBINATIONS_ADDCART', 0),
    array('LINEVEN_APO_COMBINATIONS_DSP_OPT', 0),
    array('LINEVEN_APO_AVL_ZERO_QTY', 1),
    array('LINEVEN_APO_AVL_VISIBILITY', serialize(array('both', 'catalog', 'search'))),
    array('LINEVEN_APO_DEFAULT_DESIGN', 1),
    array('LINEVEN_APO_DESIGN_CLASSIC', ''),
    array('LINEVEN_APO_DESIGN_LIST_A', ''),
    array('LINEVEN_APO_DESIGN_LIST_B', ''),
    array('LINEVEN_APO_DESIGN_THUMBNAILS', ''),
    array('LINEVEN_APO_STATS_ACTIVE', 0),
    array('LINEVEN_APO_STATS_IS_DISPLAYED', 0),
    array('LINEVEN_APO_STATS_ALL_DISPLAYED', 0),
    array('LINEVEN_APO_STATS_ADD_CART', 0),
    array('LINEVEN_APO_STATS_IS_VIEWED', 0),
    array('LINEVEN_APO_STATS_LAST_CLEAR', null),
    array('LINEVEN_APO_STATS_EXP_DELIMTER', 'SC'),
    array('LINEVEN_APO_STATS_EXP_ENCLOSURE', 'DQ'),
    array('LINEVEN_APO_SPECIFIC_HOOK_DEF', serialize(array())),
    array('LINEVEN_APO_PARTNER_RVW_RATE', 0),
    array('LINEVEN_APO_PARTNER_RVW_MODULE', 'homecomments'),
    array('LINEVEN_APO_BO_LIST_FILTER', 1)
);
