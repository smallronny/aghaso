<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

define('_LINEVEN_MODULE_APO_DIRNAME_', 'additionalproductsorder');
define('_LINEVEN_MODULE_APO_FILENAME_', 'additionalproductsorder');
define('_LINEVEN_MODULE_APO_CLASSNAME_', 'AdditionalProductsOrder');
define('_LINEVEN_MODULE_APO_DATABASE_PREFIX_', 'PREFIX_lineven_apo');
define('_LINEVEN_MODULE_APO_REQUIRE_DIR_', _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_);
define('_LINEVEN_MODULE_APO_DOCS_DIR_', _LINEVEN_MODULE_APO_REQUIRE_DIR_.'/docs/');
define('_LINEVEN_MODULE_APO_FONTAWESOME_VERSION_', '4.6.3');
define('_LINEVEN_MODULE_APO_PS_MAJOR_', Tools::substr(_PS_VERSION_, 0, 3));

// Specific constant
define(
    '_LINEVEN_MODULE_APO_TEMPLATES_HOOK_DIR_',
    _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/templates/hook/shoppingcart/'
);

/* Src Core */
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Module.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Context.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Configuration.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Controller.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Model.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Presenter/Presenter.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Presenter/FrontPresenter.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Presenter/HookPresenter.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Cache.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Translator.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Core/Tools.php');

/* Src Plugins */
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Plugins/Partners/Partner.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/src/Plugins/Partners.php');

/* Classes */
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/association/Association.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/search/ProductSearchContext.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/search/ProductSearchQuery.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/search/ProductSearchResult.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/CrossSellingProductsSearch.php');

require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/ProductPresenter.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/ProductClassicPresenter.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/ProductThemePresenter.php');
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/ProductsListPresenter.php');
if (version_compare(_PS_VERSION_, '1.7.6', '>=')) {
    require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/product/ProductLazyArray.php');
}
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/classes/statistics/Statistics.php');

/* Controller */
require_once(_LINEVEN_MODULE_APO_REQUIRE_DIR_.'/controllers/hook/OnHook.php');
