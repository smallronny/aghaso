<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

$translator = new LinevenApoTranslator();

// Admin Tabs
$tab = array(
    'classname_parent' => 'AdminCatalog',
    'classname' => 'AdminAssociations',
    'default_name' => 'Additional Products Order',
    'name' => $translator->l('Additional Products Order', 'navigation.inc')
);
LinevenApoTabs::addTab($tab);

$navigation = array(
    'options' => array(
        'default' => 'dashboard',
        'admin_tab' => array(
            'AdminAssociations' => array(
                'shortcut' => 'cross_selling:associations',
                'show_navigation' => false
            )
        )
    ),
    'tree' => array(
        'dashboard' => array(
            'label' => $translator->l('Dashboard', 'navigation.inc'),
            'controller' => 'Dashboard',
            'action' => 'index',
            'icon' => 'fa fa-tachometer'
        ),
        'settings' => array(
            'label' => $translator->l('General settings', 'navigation.inc'),
            'items' => array(
                'module' => array(
                    'label' => $translator->l('Module', 'navigation.inc'),
                    'controller' => 'Settings',
                    'action' => 'module',
                    'icon' => 'fa fa-puzzle-piece'
                )
            )
        )
    )
);
$navigation['tree']['cross_selling'] = array(
    'label' => $translator->l('Cross selling', 'navigation.inc'),
    'items' => array(
        'associations' => array(
            'label' => $translator->l('Custom associations', 'navigation.inc'),
            'controller' => 'Associations',
            'action' => 'list',
            'icon' => 'fa fa-chain',
            'items' => array(
                'add' => array(
                    'label' => $translator->l('Create an association', 'navigation.inc'),
                    'controller' => 'Associations',
                    'action' => 'addEdit'
                ),
                'edit' => array(
                    'label' => $translator->l('Edit an association', 'navigation.inc'),
                    'controller' => 'Associations',
                    'action' => 'addEdit'
                )
            ),
            'separator' => array('type' => 'line')
        ),
        'on_order_page' => array(
            'label' => $translator->l('Order page', 'navigation.inc'),
            'controller' => 'Hooks',
            'action' => 'onOrderPage',
            'icon' => 'fa fa-shopping-cart'
        ),
        'on_extra_product_page' => array(
            'label' => $translator->l('Extra in product page', 'navigation.inc'),
            'controller' => 'Hooks',
            'action' => 'onExtraProductPage',
            'icon' => 'fa fa-book',
        )
    )
);
$navigation['tree']['additional_settings'] = array(
    'label' => $translator->l('Additional settings', 'navigation.inc'),
    'items' => array(
        'combinations' => array(
            'label' => $translator->l('Combinations', 'navigation.inc'),
            'controller' => 'Settings',
            'action' => 'combinations',
            'icon' => 'fa fa-sliders'
        ),
        'availability' => array(
            'label' => $translator->l('Products availability', 'navigation.inc'),
            'controller' => 'Settings',
            'action' => 'availability',
            'icon' => 'fa fa-check-square-o'
        )
    )
);
if ((Tools::isSubmit('active_statistics') && (int)Tools::getValue('active_statistics')) ||
    (!Tools::isSubmit('active_statistics') && Configuration::get('LINEVEN_APO_STATS_ACTIVE'))) {
    $navigation['tree']['statistics'] = array(
        'label' => $translator->l('Statistcs', 'navigation.inc'),
        'items' => array(
            'statistics_dashboard' => array(
                'label' => $translator->l('Dashboard', 'navigation.inc'),
                'controller' => 'Statistics',
                'action' => 'dashboard',
                'icon' => 'fa fa-pie-chart'
            ),
            'statistics_details' => array(
                'label' => $translator->l('Details', 'navigation.inc'),
                'controller' => 'Statistics',
                'action' => 'details',
                'icon' => 'fa fa-list',
                'separator' => array('type' => 'line')
            ),
            'statistics_settings' => array(
                'label' => $translator->l('Settings', 'navigation.inc'),
                'controller' => 'Statistics',
                'action' => 'settings',
                'icon' => 'fa fa-cogs'
            )
        )
    );
}
$navigation['tree']['associated_modules'] = array(
    'label' => $translator->l('Associated modules', 'navigation.inc'),
    'items' => array(
        'reviews' => array(
            'label' => $translator->l('Reviews', 'navigation.inc'),
            'controller' => 'AssociatedModules',
            'action' => 'reviews',
            'icon' => 'fa fa-comments'
        )
    )
);
$navigation['tree']['tools'] = array(
    'label' => $translator->l('Tools', 'navigation.inc'),
    'items' => array(
        'design' => array(
            'label' => $translator->l('Design', 'navigation.inc'),
            'controller' => 'Tools',
            'action' => 'design',
            'icon' => 'fa fa-paint-brush'
        ),
        'custom_css' => array(
            'label' => $translator->l('Custom CSS', 'navigation.inc'),
            'controller' => 'Tools',
            'action' => 'customCss',
            'icon' => 'fa fa-css3',
        )
    )
);
$navigation['tree']['support'] = array(
    'label' => $translator->l('Support', 'navigation.inc'),
    'items' => array(
        'changelog' => array(
            'label' => $translator->l('Change log', 'navigation.inc'),
            'controller' => 'Support',
            'action' => 'changelog',
            'icon' => 'fa fa-history',
        ),
        'informations' => array(
            'label' => $translator->l('Informations', 'navigation.inc'),
            'controller' => 'Support',
            'action' => 'informations',
            'icon' => 'fa fa-info',
            'environment' => LinevenApoContext::$environment_production
        ),
        'diagnostic' => array(
            'label' => $translator->l('Diagnostic', 'navigation.inc'),
            'controller' => 'Diagnostic',
            'action' => 'dashboard',
            'icon' => 'fa fa-medkit',
            'environment' => LinevenApoContext::$environment_production
        ),
        'testmode' => array(
            'label' => $translator->l('Test mode', 'navigation.inc'),
            'controller' => 'Support',
            'action' => 'testmode',
            'icon' => 'fa fa-moon-o',
            'environment' => LinevenApoContext::$environment_production
        )
    )
);
return $navigation;
