<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

return array(
    array(
        'name' => 'additionalproductsorder',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/'
    ),
    array(
        'name' => 'additionalproductsorder.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/'
    ),
    array(
        'name' => 'classes',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/admin/'
    ),
    array(
        'name' => 'Alerts.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/admin/'
    ),
    array(
        'name' => 'Diagnostic.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/admin/'
    ),
    array(
        'name' => 'association',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/association/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/association/admin/'
    ),
    array(
        'name' => 'AssociationAdminListingPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/association/admin/'
    ),
    array(
        'name' => 'AssociationAdminListingSearch.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/association/admin/'
    ),
    array(
        'name' => 'AssociationAdminPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/association/admin/'
    ),
    array(
        'name' => 'AssociationAdminProductsSearch.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/association/admin/'
    ),
    array(
        'name' => 'Association.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/association/'
    ),
    array(
        'name' => 'forms',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/forms/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'AssociatedModulesReviews.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'Association.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'OnExtraProductPage.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'OnHookForm.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'OnOrderPage.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'SettingsAvailability.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'SettingsCombinations.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'SettingsModule.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'Statistics.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'support',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/support/'
    ),
    array(
        'name' => 'TestMode.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/support/'
    ),
    array(
        'name' => 'Troubleshooting.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/support/'
    ),
    array(
        'name' => 'ToolsCustomCss.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'ToolsDesign.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/forms/admin/'
    ),
    array(
        'name' => 'product',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/product/'
    ),
    array(
        'name' => 'CrossSellingProductsSearch.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/'
    ),
    array(
        'name' => 'ProductClassicPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/'
    ),
    array(
        'name' => 'ProductLazyArray.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/'
    ),
    array(
        'name' => 'ProductPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/'
    ),
    array(
        'name' => 'ProductsListPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/'
    ),
    array(
        'name' => 'ProductThemePresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/'
    ),
    array(
        'name' => 'search',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/product/search/'
    ),
    array(
        'name' => 'ProductSearchContext.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/search/'
    ),
    array(
        'name' => 'ProductSearchQuery.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/search/'
    ),
    array(
        'name' => 'ProductSearchResult.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/product/search/'
    ),
    array(
        'name' => 'statistics',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/statistics/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/classes/statistics/admin/'
    ),
    array(
        'name' => 'StatisticsAdminListingPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/statistics/admin/'
    ),
    array(
        'name' => 'StatisticsAdminListingSearch.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/statistics/admin/'
    ),
    array(
        'name' => 'StatisticsAdminSearch.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/statistics/admin/'
    ),
    array(
        'name' => 'Statistics.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/classes/statistics/'
    ),
    array(
        'name' => 'config',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/config/'
    ),
    array(
        'name' => 'config.inc.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/config/'
    ),
    array(
        'name' => 'files.inc.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/config/'
    ),
    array(
        'name' => 'hooks.inc.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/config/'
    ),
    array(
        'name' => 'navigation.inc.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/config/'
    ),
    array(
        'name' => 'require_backoffice.inc.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/config/'
    ),
    array(
        'name' => 'settings.inc.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/config/'
    ),
    array(
        'name' => 'controllers',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/controllers/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/controllers/admin/'
    ),
    array(
        'name' => 'AdminAssociations.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/admin/'
    ),
    array(
        'name' => 'front',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/controllers/front/'
    ),
    array(
        'name' => 'servicedispatcher.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/front/'
    ),
    array(
        'name' => 'services',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/controllers/front/services/'
    ),
    array(
        'name' => 'FrontServiceRefresh.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/front/services/'
    ),
    array(
        'name' => 'FrontServiceStatistics.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/front/services/'
    ),
    array(
        'name' => 'hook',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/controllers/hook/'
    ),
    array(
        'name' => 'ExtraProductPage.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/hook/'
    ),
    array(
        'name' => 'Header.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/hook/'
    ),
    array(
        'name' => 'OnHook.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/hook/'
    ),
    array(
        'name' => 'ShoppingCart.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/hook/'
    ),
    array(
        'name' => 'module',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'AssociatedModules.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Associations.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Dashboard.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Diagnostic.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Hooks.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Settings.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Statistics.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Support.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Tools.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'Upgrade.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/controllers/module/'
    ),
    array(
        'name' => 'cron_clear_statistics.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/'
    ),
    array(
        'name' => 'logo.gif',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/'
    ),
    array(
        'name' => 'logo.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/'
    ),
    array(
        'name' => 'src',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/'
    ),
    array(
        'name' => 'Core',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Cache.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Configuration.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Context.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Controller.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'DbTools.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Form.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Helper',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Core/Helper/'
    ),
    array(
        'name' => 'HelperForm.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/Helper/'
    ),
    array(
        'name' => 'HelperList.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/Helper/'
    ),
    array(
        'name' => 'Model.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Module.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Navigation.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Presenter',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Core/Presenter/'
    ),
    array(
        'name' => 'FrontPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/Presenter/'
    ),
    array(
        'name' => 'HookPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/Presenter/'
    ),
    array(
        'name' => 'ModulePresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/Presenter/'
    ),
    array(
        'name' => 'Presenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/Presenter/'
    ),
    array(
        'name' => 'Tools.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Translator.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'version.txt',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Core/'
    ),
    array(
        'name' => 'Module',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Module/'
    ),
    array(
        'name' => 'Alerts.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/'
    ),
    array(
        'name' => 'Controller',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Module/Controller/'
    ),
    array(
        'name' => 'Dashboard.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Controller/'
    ),
    array(
        'name' => 'Diagnostic.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Controller/'
    ),
    array(
        'name' => 'Support.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Controller/'
    ),
    array(
        'name' => 'Upgrade.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Controller/'
    ),
    array(
        'name' => 'Diagnostic.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/'
    ),
    array(
        'name' => 'Form',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Module/Form/'
    ),
    array(
        'name' => 'SupportTestMode.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Form/'
    ),
    array(
        'name' => 'SupportTroubleshooting.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Form/'
    ),
    array(
        'name' => 'Presenter',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Module/Presenter/'
    ),
    array(
        'name' => 'AdminFormPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Presenter/'
    ),
    array(
        'name' => 'AdminListingPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Presenter/'
    ),
    array(
        'name' => 'AdminPresenter.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Presenter/'
    ),
    array(
        'name' => 'Search',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Module/Search/'
    ),
    array(
        'name' => 'AdminListingSearch.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/Search/'
    ),
    array(
        'name' => 'Upgrade.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Module/'
    ),
    array(
        'name' => 'Plugins',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Plugins/'
    ),
    array(
        'name' => 'Partners',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/src/Plugins/Partners/'
    ),
    array(
        'name' => 'Partner.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Plugins/Partners/'
    ),
    array(
        'name' => 'Partners.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Plugins/'
    ),
    array(
        'name' => 'Tabs.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/src/Plugins/'
    ),
    array(
        'name' => 'translations',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/translations/'
    ),
    array(
        'name' => 'en.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/translations/'
    ),
    array(
        'name' => 'es.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/translations/'
    ),
    array(
        'name' => 'fr.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/translations/'
    ),
    array(
        'name' => 'it.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/translations/'
    ),
    array(
        'name' => 'pt.php',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/translations/'
    ),
    array(
        'name' => 'views',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/'
    ),
    array(
        'name' => 'css',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/'
    ),
    array(
        'name' => 'additionalproductsorder-classic.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/'
    ),
    array(
        'name' => 'additionalproductsorder-global.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/'
    ),
    array(
        'name' => 'additionalproductsorder-list-a.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/'
    ),
    array(
        'name' => 'additionalproductsorder-list-b.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/'
    ),
    array(
        'name' => 'additionalproductsorder-theme.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/'
    ),
    array(
        'name' => 'additionalproductsorder-thumbnails.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/admin/'
    ),
    array(
        'name' => 'apo-lineven.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/admin/'
    ),
    array(
        'name' => 'apo-module.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/admin/'
    ),
    array(
        'name' => 'vendor',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/vendor/'
    ),
    array(
        'name' => 'busy-load',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/vendor/busy-load/'
    ),
    array(
        'name' => 'app.min.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/busy-load/'
    ),
    array(
        'name' => 'LICENSE',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/busy-load/'
    ),
    array(
        'name' => 'font-awesome-4.6.3',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'css',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/css/'
    ),
    array(
        'name' => 'font-awesome.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/css/'
    ),
    array(
        'name' => 'font-awesome.min.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/css/'
    ),
    array(
        'name' => 'less',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'animated.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'bordered-pulled.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'core.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'fixed-width.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'font-awesome.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'icons.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'larger.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'list.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'mixins.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'path.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'rotated-flipped.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'screen-reader.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'stacked.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'variables.less',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/less/'
    ),
    array(
        'name' => 'scss',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => 'font-awesome.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_animated.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_bordered-pulled.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_core.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_fixed-width.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_icons.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_larger.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_list.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_mixins.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_path.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_rotated-flipped.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_screen-reader.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_stacked.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => '_variables.scss',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/font-awesome-4.6.3/scss/'
    ),
    array(
        'name' => 'tooltipster',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/css/vendor/tooltipster/'
    ),
    array(
        'name' => 'tooltipster-sideTip-light.min.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/tooltipster/'
    ),
    array(
        'name' => 'tooltipster.bundle.min.css',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/css/vendor/tooltipster/'
    ),
    array(
        'name' => 'fonts',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/fonts/'
    ),
    array(
        'name' => 'times_new_yorker.ttf',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/fonts/'
    ),
    array(
        'name' => 'vendor',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/'
    ),
    array(
        'name' => 'font-awesome-4.6.3',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'fontawesome-webfont.eot',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'fontawesome-webfont.svg',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'fontawesome-webfont.ttf',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'fontawesome-webfont.woff',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'fontawesome-webfont.woff2',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'FontAwesome.otf',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/fonts/vendor/font-awesome-4.6.3/'
    ),
    array(
        'name' => 'img',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/img/'
    ),
    array(
        'name' => 'AdminAssociations.gif',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/'
    ),
    array(
        'name' => 'box',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/img/box/'
    ),
    array(
        'name' => 'box_en.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/box/'
    ),
    array(
        'name' => 'box_es.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/box/'
    ),
    array(
        'name' => 'box_fr.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/box/'
    ),
    array(
        'name' => 'box_it.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/box/'
    ),
    array(
        'name' => 'box_pt.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/box/'
    ),
    array(
        'name' => 'logo16.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/'
    ),
    array(
        'name' => 'logo24.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/'
    ),
    array(
        'name' => 'logo32.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/'
    ),
    array(
        'name' => 'logo57.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/'
    ),
    array(
        'name' => 'notation.png',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/img/'
    ),
    array(
        'name' => 'js',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/js/'
    ),
    array(
        'name' => 'additionalproductsorder.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/js/admin/'
    ),
    array(
        'name' => 'apo-lineven.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/admin/'
    ),
    array(
        'name' => 'apo-module.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/admin/'
    ),
    array(
        'name' => 'vendor',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/js/admin/vendor/'
    ),
    array(
        'name' => 'rgraph',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/js/admin/vendor/rgraph/'
    ),
    array(
        'name' => 'RGraph.svg.common.core.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/admin/vendor/rgraph/'
    ),
    array(
        'name' => 'RGraph.svg.common.key.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/admin/vendor/rgraph/'
    ),
    array(
        'name' => 'RGraph.svg.common.tooltips.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/admin/vendor/rgraph/'
    ),
    array(
        'name' => 'RGraph.svg.line.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/admin/vendor/rgraph/'
    ),
    array(
        'name' => 'RGraph.svg.pie.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/admin/vendor/rgraph/'
    ),
    array(
        'name' => 'vendor',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/js/vendor/'
    ),
    array(
        'name' => 'busy-load',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/js/vendor/busy-load/'
    ),
    array(
        'name' => 'app.min.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/vendor/busy-load/'
    ),
    array(
        'name' => 'LICENSE',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/vendor/busy-load/'
    ),
    array(
        'name' => 'tooltipster',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/js/vendor/tooltipster/'
    ),
    array(
        'name' => 'tooltipster.bundle.min.js',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/js/vendor/tooltipster/'
    ),
    array(
        'name' => 'templates',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/'
    ),
    array(
        'name' => 'admin',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/'
    ),
    array(
        'name' => 'module',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/'
    ),
    array(
        'name' => 'associatedmodules',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associatedmodules/'
    ),
    array(
        'name' => 'reviews.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associatedmodules/'
    ),
    array(
        'name' => 'associations',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/'
    ),
    array(
        'name' => 'addedit',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/addedit/'
    ),
    array(
        'name' => 'displayed_products_details.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/addedit/'
    ),
    array(
        'name' => 'displayed_products_settings.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/addedit/'
    ),
    array(
        'name' => 'related_product_details.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/addedit/'
    ),
    array(
        'name' => 'related_product_settings.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/addedit/'
    ),
    array(
        'name' => 'addEdit.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/'
    ),
    array(
        'name' => 'list.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/associations/'
    ),
    array(
        'name' => 'dashboard',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/dashboard/'
    ),
    array(
        'name' => 'index.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/dashboard/'
    ),
    array(
        'name' => 'helpers',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/helpers/'
    ),
    array(
        'name' => 'products_list.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/helpers/'
    ),
    array(
        'name' => 'product_informations.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/helpers/'
    ),
    array(
        'name' => 'hooks',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/hooks/'
    ),
    array(
        'name' => 'onhook.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/hooks/'
    ),
    array(
        'name' => 'statistics',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/statistics/'
    ),
    array(
        'name' => 'dashboard.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/statistics/'
    ),
    array(
        'name' => 'details.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/statistics/'
    ),
    array(
        'name' => 'settings.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/statistics/'
    ),
    array(
        'name' => 'tools',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/tools/'
    ),
    array(
        'name' => 'design.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/tools/'
    ),
    array(
        'name' => '_partials',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/'
    ),
    array(
        'name' => 'dashboard',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/dashboard/'
    ),
    array(
        'name' => 'index.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/dashboard/'
    ),
    array(
        'name' => 'diagnostic',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/diagnostic/'
    ),
    array(
        'name' => 'dashboard.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/diagnostic/'
    ),
    array(
        'name' => 'results_diagnostic.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/diagnostic/'
    ),
    array(
        'name' => 'results_execution.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/diagnostic/'
    ),
    array(
        'name' => 'helpers',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/helpers/'
    ),
    array(
        'name' => 'html_content.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/helpers/'
    ),
    array(
        'name' => 'list',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/helpers/list/'
    ),
    array(
        'name' => 'options_icons.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/helpers/list/'
    ),
    array(
        'name' => 'simple_text.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/helpers/list/'
    ),
    array(
        'name' => 'javascript.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/'
    ),
    array(
        'name' => 'layout',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/layout/'
    ),
    array(
        'name' => 'footer.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/layout/'
    ),
    array(
        'name' => 'header.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/layout/'
    ),
    array(
        'name' => 'navigation.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/layout/'
    ),
    array(
        'name' => 'navigation_link.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/layout/'
    ),
    array(
        'name' => 'notifications',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/notifications/'
    ),
    array(
        'name' => 'for_automatic_repair.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/notifications/'
    ),
    array(
        'name' => 'for_demonstration.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/notifications/'
    ),
    array(
        'name' => 'for_installation.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/notifications/'
    ),
    array(
        'name' => 'for_upgrade.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/notifications/'
    ),
    array(
        'name' => 'messages.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/notifications/'
    ),
    array(
        'name' => 'support',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/support/'
    ),
    array(
        'name' => 'changelog.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/support/'
    ),
    array(
        'name' => 'informations.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/module/_partials/support/'
    ),
    array(
        'name' => '_configure',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/'
    ),
    array(
        'name' => 'helpers',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/'
    ),
    array(
        'name' => 'form',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/form/'
    ),
    array(
        'name' => 'form.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/form/'
    ),
    array(
        'name' => 'form_group_specific.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/form/'
    ),
    array(
        'name' => 'list',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'list_action_approve.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'list_action_duplicate.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'list_action_preview.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'list_action_refuse.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'list_action_reminder.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'list_content.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'list_header.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/admin/_configure/helpers/list/'
    ),
    array(
        'name' => 'hook',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/hook/'
    ),
    array(
        'name' => 'extraproductpage',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/hook/extraproductpage/'
    ),
    array(
        'name' => 'classic.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/extraproductpage/'
    ),
    array(
        'name' => 'list_a.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/extraproductpage/'
    ),
    array(
        'name' => 'list_b.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/extraproductpage/'
    ),
    array(
        'name' => 'thumbnails.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/extraproductpage/'
    ),
    array(
        'name' => 'header',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/hook/header/'
    ),
    array(
        'name' => 'header.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/header/'
    ),
    array(
        'name' => 'shoppingcart',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/hook/shoppingcart/'
    ),
    array(
        'name' => 'classic.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/shoppingcart/'
    ),
    array(
        'name' => 'list_a.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/shoppingcart/'
    ),
    array(
        'name' => 'list_b.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/shoppingcart/'
    ),
    array(
        'name' => 'theme.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/shoppingcart/'
    ),
    array(
        'name' => 'thumbnails.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/shoppingcart/'
    ),
    array(
        'name' => '_partials',
        'type' => 'dir',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'attributes.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'buttons.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'description.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'image.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'javascript.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'layout.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'layout_theme.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'name.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'options.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'price.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'reviews.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    ),
    array(
        'name' => 'tooltip.tpl',
        'type' => 'file',
        'relative_path' => 'additionalproductsorder/views/templates/hook/_partials/'
    )
);
