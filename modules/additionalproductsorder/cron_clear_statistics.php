<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

if (!in_array('AdditionalProductsOrder', get_declared_classes())) {
    include(dirname(__FILE__).'/additionalproductsorder.php');
}

if (!Configuration::get('LINEVEN_APO_ACTIVE')) {
    die('Additional Products Order not active');
}

$module = new AdditionalProductsOrder();
$module_context = LinevenApoContext::getContext();
$module_context->setApplication(LinevenApoContext::$application_backoffice);
$module->init(true);
LinevenApoStatistics::clear();
die('OK');
