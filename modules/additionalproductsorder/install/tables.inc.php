<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

return array(
    array(
        'name' => 'PREFIX_lineven_apo',
        'fields' => array(
            array('name' => 'id', 'def' => 'int(10) unsigned NOT NULL AUTO_INCREMENT'),
            array('name' => 'id_shop_group', 'def' => 'int(11) unsigned'),
            array('name' => 'id_shop', 'def' => 'int(11) unsigned'),
            array('name' => 'name', 'def' => 'text NULL'),
            array('name' => 'short_description', 'def' => 'text NULL'),
            array('name' => 'comments', 'def' => 'varchar(255) NULL'),
            array('name' => 'id_related_category', 'def' => 'int(10) NULL'),
            array('name' => 'id_related_product', 'def' => 'int(10) NULL'),
            array('name' => 'id_displayed_category', 'def' => 'int(10) NULL'),
            array('name' => 'minimum_amount', 'def' => 'float NULL DEFAULT 0'),
            array('name' => 'maximum_amount', 'def' => 'float NULL DEFAULT 0'),
            array('name' => 'is_active_groups', 'def' => 'int(1) DEFAULT 0'),
            array('name' => 'hooks', 'def' => 'varchar(255) NULL DEFAULT ""'),
            array('name' => 'order_display', 'def' => 'int(10) NULL')
        ),
        'primary' => array('id'),
        'options' => 'ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8 AUTO_INCREMENT=1'
    ),
    array(
        'name' => 'PREFIX_lineven_apo_displayed_products',
        'fields' => array(
            array('name' => 'id_association', 'def' => 'int(10) unsigned NOT NULL'),
            array('name' => 'id_displayed_product', 'def' => 'int(10) unsigned NOT NULL '),
            array('name' => 'order_display', 'def' => 'int(10) NULL')
        ),
        'primary' => array('id_association', 'id_displayed_product'),
        'options' => 'ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8'
    ),
    array(
        'name' => 'PREFIX_lineven_apo_statistics',
        'fields' => array(
            array('name' => 'id', 'def' => 'int(10) unsigned NOT NULL AUTO_INCREMENT'),
            array('name' => 'id_shop_group', 'def' => 'int(11) unsigned'),
            array('name' => 'id_shop', 'def' => 'int(11) unsigned'),
            array('name' => 'id_association', 'def' => 'int(11) unsigned'),
            array('name' => 'id_product', 'def' => 'int(11) unsigned'),
            array('name' => 'displayed', 'def' => 'int(11) unsigned'),
            array('name' => 'add_to_cart', 'def' => 'int(11) unsigned'),
            array('name' => 'click_to_view', 'def' => 'int(11) unsigned'),
            array('name' => 'date_add', 'def' => 'DATETIME')
        ),
        'primary' => array('id'),
        'options' => 'ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8 AUTO_INCREMENT=1'
    ),
    array(
        'name' => 'PREFIX_lineven_apo_groups',
        'fields' => array(
            array('name' => 'id_association', 'def' => 'int(10) unsigned NOT NULL'),
            array('name' => 'id_group', 'def' => 'int(10) unsigned NOT NULL')
        ),
        'options' => 'ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8'
    )
);
