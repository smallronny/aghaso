<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class AdditionalProductsOrderServiceDispatcherModuleFrontController extends ModuleFrontController
{
    private $controller;

    /**
     *
     * @see ModuleFrontController::postProccess()
     */
    public function postProcess()
    {
        $this->callController('process');
    }

    /**
     * InitContent.
     *
     * @see ModuleFrontController::initContent()
     */
    public function initContent()
    {
        $this->callController('init_content');
        if ($this->controller) {
            if (method_exists(get_class($this->controller), 'send')) {
                $this->controller->send();
                exit;
            }
            if (property_exists(get_class($this->controller), 'params')) {
                die(Tools::jsonEncode($this->controller->params));
            }
            die();
        }
    }

    /**
     * Call controller.
     *
     * @param string $step Step
     */
    private function callController($step = 'process')
    {
        // Token control
        if (!$this->checkToken()) {
            die();
        }
        $configuration = LinevenApoConfiguration::getConfiguration();
        $module_context = LinevenApoContext::getContext();
        if (Tools::isSubmit($configuration->name.'_service_controller') &&
            Tools::getValue($configuration->name.'_service_controller') != '' &&
            ((!Tools::isSubmit($configuration->name.'_service_controller_step') && $step == 'process') ||
                (Tools::isSubmit($configuration->name.'_service_controller_step') &&
                    Tools::getValue($configuration->name.'_service_controller_step') == $step
                ))) {
            if ($step == 'init_content') {
                parent::initContent();
            }
            $module_context->setApplication(LinevenApoContext::$application_frontoffice);
            $this->init();
            $this->controller = LinevenApoController::getController(
                Tools::ucfirst(Tools::getValue($configuration->name.'_service_controller')),
                LinevenApoController::$controller_type_front_service
            );
            $this->controller->run();
        }
    }

    /**
     * Check token.
     */
    private function checkToken()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $id_customer = (int)Tools::getValue($configuration->name.'_ajax_id_customer');
        $customer_token = Tools::getValue($configuration->name.'_ajax_customer_token');
        $id_guest = (int)Tools::getValue($configuration->name.'_ajax_id_guest');
        $guest_token = Tools::getValue($configuration->name.'_ajax_guest_token');
        $customer = Context::getContext()->customer;
        if ($customer->isLogged() === true &&
            $id_customer == (int)Context::getContext()->customer->id) {
            $token = sha1($customer->secure_key);
            if (!isset($customer_token) || $customer_token == $token) {
                return true;
            }
        } else {
            if (Context::getContext()->cart->id_guest == $id_guest) {
                $token = sha1($configuration->name.Context::getContext()->cart->id_guest.$_SERVER['REMOTE_ADDR'].date('Y-m-d'));
                if (!isset($guest_token) || $guest_token == $token) {
                    return true;
                }
            }
        }
        return false;
    }
}
