<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFrontServiceStatisticsController
{
    /**
     * Run.
     *
     * @return void
     */
    public function run()
    {
        if (Tools::isSubmit('type') && Tools::isSubmit('id_association') && Tools::isSubmit('id_product')) {
            LinevenApoStatistics::setData(
                (int)Tools::getValue('id_association'),
                (int)Tools::getValue('id_product'),
                Tools::getValue('type')
            );
        }
    }
}
