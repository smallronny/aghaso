<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFrontServiceRefreshController
{
    public $params;

    /**
     * Run.
     *
     * @return void
     */
    public function run()
    {
        $module_context = LinevenApoContext::getContext();
        if ($module_context->isModuleActive()) {
            $configuration = LinevenApoConfiguration::getConfiguration();
            $module_context->setApplication(LinevenApoContext::$application_frontoffice);
            $configuration->getModule()->init();
            $controller = LinevenApoController::getController('ShoppingCart', LinevenApoController::$controller_type_front_hook);
            $controller->params['refresh'] = true;
            die(
                Tools::jsonEncode(
                    array(
                        'html' => $controller->run('index')
                    )
                )
            );
        }
    }

    /**
     * InitContent.
     *
     * @see LinevenModuleFrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        die();
    }
}
