<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminAssociatedModulesController extends LinevenApoController
{
    /**
     * Reviews settings action.
     * @return void
     */
    public function reviewsAction()
    {
        $module_context = LinevenApoContext::getContext();
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper = new LinevenApoHelperForm('AssociatedModules', 'reviews', 'associated_modules:reviews');
        $helper->base_folder = $this->presenter->path.'associatedmodules/';
        $helper->base_tpl = 'reviews.tpl';
        $helper->tpl_vars['associated_modules_reviews_verify_lineven'] = LinevenApoPartners::verifyModule(
            AdditionalProductsOrder::$partner_reviews_module_lineven
        );
        $helper->tpl_vars['associated_modules_reviews_verify_prestashop'] = LinevenApoPartners::verifyModule(
            AdditionalProductsOrder::$partner_reviews_module_prestashop
        );
        $helper->tpl_vars['language_iso_code'] = $module_context->current_language['iso_code'];
        $helper->form = new LinevenApoFormAdminAssociatedModulesReviews();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your reviews settings have been updated.', 'AssociatedModules')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm());
    }
}
