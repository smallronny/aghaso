<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminDashboardController extends LinevenApoModuleDashboardController
{
    /**
     * Dashboard.
     * @return void
     */
    public function indexAction()
    {
        parent::indexAction();

        // Associations report
        $associations = LinevenApoAssociation::getAssociations();
        $associations_report = array(
            'count' => count($associations)
        );
        $associations_report_statistics = false;
        if (Configuration::get('LINEVEN_APO_STATS_ACTIVE')) {
            $statistics_search = new LinevenApoStatisticsAdminSearch();
            $statistics_result = array(
                'sum_displayed' => 0,
                'sum_add_to_cart' => 0,
                'sum_click_to_view' => 0,
                'day_sum_add_to_cart' => 0,
                'day_sum_click_to_view' => 0,
                'day_sum_displayed' => 0,
                'day_percent_add_to_cart' => 0,
                'day_percent_click_to_view' => 0,
                'day_percent_displayed' => 0,
                'month_percent_add_to_cart' => 100,
                'month_percent_click_to_view' => 100,
                'month_percent_displayed' => 100
            );
            $date = new DateTime();
            $associations_report_statistics_display_graph = false;
            $associations_report_statistics_graph_count = 0;
            $associations_report_statistics_display_month = $date->format('F  Y');
            $associations_report_statistics_display_today = $date->format('l d F Y');
            $statistics = $statistics_search->search();
            if ($statistics != null) {
                $associations_report_statistics = true;
                $statistics_result['sum_displayed'] = $statistics['sum_displayed'];
                $statistics_result['sum_add_to_cart'] = $statistics['sum_add_to_cart'];
                $statistics_result['sum_click_to_view'] = $statistics['sum_click_to_view'];
                if (Configuration::get('LINEVEN_APO_STATS_IS_DISPLAYED') ||
                    Configuration::get('LINEVEN_APO_STATS_ADD_CART') ||
                    Configuration::get('LINEVEN_APO_STATS_IS_VIEWED')) {
                    $associations_report_statistics_display_graph = true;
                    $associations_report_statistics_graph_count =
                        (int)(Configuration::get('LINEVEN_APO_STATS_IS_DISPLAYED') &&
                            (int)$statistics_result['sum_displayed'])
                        + ((int)Configuration::get('LINEVEN_APO_STATS_ADD_CART') &&
                            (int)$statistics_result['sum_add_to_cart'])
                        + ((int)Configuration::get('LINEVEN_APO_STATS_IS_VIEWED') &&
                            (int)$statistics_result['sum_click_to_view']);
                    $today_date = $date->format('Y-m-d');
                    $statistics_search->setBeginDate($today_date);
                    $statistics_search->setEndDate($today_date);
                    $a_statistics_of_the_day = $statistics_search->search();
                    if ($a_statistics_of_the_day != null) {
                        $statistics_result['day_sum_add_to_cart'] = $a_statistics_of_the_day['sum_add_to_cart'];
                        $statistics_result['day_sum_click_to_view'] = $a_statistics_of_the_day['sum_click_to_view'];
                        $statistics_result['day_sum_displayed'] = $a_statistics_of_the_day['sum_displayed'];
                        if ($statistics_result['sum_add_to_cart'] != 0) {
                            $statistics_result['day_percent_add_to_cart'] =
                                $a_statistics_of_the_day['sum_add_to_cart'] * 100 /
                                    $statistics_result['sum_add_to_cart'];
                        }
                        if ($statistics_result['sum_click_to_view'] != 0) {
                            $statistics_result['day_percent_click_to_view'] =
                                $a_statistics_of_the_day['sum_click_to_view'] * 100 /
                                    $statistics_result['sum_click_to_view'];
                        }
                        if ($statistics_result['sum_displayed'] != 0) {
                            $statistics_result['day_percent_displayed'] =
                                $a_statistics_of_the_day['sum_displayed'] * 100 / $statistics_result['sum_displayed'];
                        }
                        $statistics_result['month_percent_add_to_cart'] =
                            100 - $statistics_result['day_percent_add_to_cart'];
                        $statistics_result['month_percent_click_to_view'] =
                            100 - $statistics_result['day_percent_click_to_view'];
                        $statistics_result['month_percent_displayed'] =
                            100 - $statistics_result['day_percent_displayed'];
                    }
                }
                $associations_report = array_merge($associations_report, $statistics_result);
            }
            $this->presenter->addDatas(array(
                'associations_report_statistics_display_graph' => $associations_report_statistics_display_graph,
                'associations_report_statistics_graph_count' => $associations_report_statistics_graph_count,
                'associations_report_statistics_display_month' => $associations_report_statistics_display_month,
                'associations_report_statistics_display_today' => $associations_report_statistics_display_today,
                'associations_report_statistics_is_displayed' => (Configuration::get('LINEVEN_APO_STATS_IS_DISPLAYED') && (int)$statistics_result['sum_displayed']),
                'associations_report_statistics_is_add_to_cart' => (Configuration::get('LINEVEN_APO_STATS_ADD_CART') && (int)$statistics_result['sum_add_to_cart']),
                'associations_report_statistics_is_click_to_view' => (Configuration::get('LINEVEN_APO_STATS_IS_VIEWED') && (int)$statistics_result['sum_click_to_view'])
            ));
        }

        // Reviews
        $report_addons_reviews_associated = false;
        if (LinevenApoPartners::verifyModule(AdditionalProductsOrder::$partner_reviews_module_lineven)
            && Configuration::get('LINEVEN_APO_PARTNER_RVW_RATE')) {
            $report_addons_reviews_associated = true;
        }
        $this->presenter->addDatas(array(
            'associations_report' => $associations_report,
            'associations_report_statistics' => $associations_report_statistics,
            'report_addons_reviews_associated' => $report_addons_reviews_associated
        ));
    }
}
