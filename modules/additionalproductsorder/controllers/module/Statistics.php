<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminStatisticsController extends LinevenApoController
{
    /**
    * Dashboard action.
    * @return void
    */
    public function dashboardAction()
    {
        $statistics_search = new LinevenApoStatisticsAdminSearch();
        $date = new DateTime();
        $this->presenter->addDatas(array(
            'statistics_graph' => $statistics_search->searchByDays(),
            'month' => $date->format('F  Y'),
            'month_number' => $date->format('m'),
            'year_number' => $date->format('Y'),
            'days_in_month' => cal_days_in_month(
                CAL_GREGORIAN,
                $date->format('m'),
                $date->format('Y')
            )
        ));

        // Template
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_default_template);
    }

    /**
     * Details action.
     * @return void
     */
    public function detailsAction()
    {
        $statistics_search = new LinevenApoStatisticsAdminListingSearch();
        $listing_presenter = new LinevenApoStatisticsAdminListingPresenter();
        $helper_options = array(
            'table_id' => 'lapo-statistics-list',
            'order_by' => 'apo.`date_add`',
            'order_way' => 'DESC',
        );
        if (Tools::isSubmit('export')) {
            $helper_options['disable_default_filters'] = true;
        }
        $helper = new LinevenApoHelperList('Statistics', 'details', 'statistics:statistics_details', $helper_options);
        $helper->identifier = 'id_association';
        $helper->title = $this->translator->l('Statistics', 'Statistics');
        $helper->list_id = 'apo_statistics';
        $helper->addToolbarButton('export', array(
            'href' => LinevenApoTools::getBackofficeURI(
                'Statistics',
                'details',
                'statistics:statistics_details'
            ).'&export=csv',
            'target' => true,
            'desc' => $this->translator->l('Export to CSV', 'Statistics')
        ));

        // Prepare the generation
        $helper->prepareList($listing_presenter->getFields());

        if (!Tools::isSubmit('export')) {
            if (Context::getContext()->shop->getContextShopID() != null) {
                $shop = new Shop(Context::getContext()->shop->getContextShopID());
                $listing_presenter->setCleanCronFile(
                    $shop->getBaseURL().'modules/additionalproductsorder/cron_clear_statistics.php'
                );
            }

            // Return output
            $this->presenter->setDatas($listing_presenter->present());
            $statistics_search->runQuery($helper->getDisplaySettings());
            $this->presenter->addOutput(LinevenApoPresenter::$output_type_content, $helper->renderList(
                $listing_presenter->presentList($statistics_search->getResults()),
                $statistics_search->getCount()
            ));
            $this->presenter->addOutput(LinevenApoPresenter::$output_type_default_template);
        } else {
            $this->presenter->noRender();
            $statistics_search->runQuery($helper->getDisplaySettings(), true);
            $this->exportToCsv($listing_presenter->presentExport($statistics_search->getResults()));
            die();
        }
    }

    /**
     * Settings action.
     * @return string
     */
    public function settingsAction()
    {
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper = new LinevenApoHelperForm('Statistics', 'settings', 'statistics:statistics_settings');
        $helper->form = new LinevenApoFormAdminStatistics();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your statistics settings have been updated.', 'Statistics')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $clean_cron_file = '';
        if (Context::getContext()->shop->getContextShopID() != null) {
            $shop = new Shop(Context::getContext()->shop->getContextShopID());
            $clean_cron_file = $shop->getBaseURL().
                'modules/additionalproductsorder/cron_clear_statistics.php';
        }
        $this->presenter->addDatas(array(
            'statistics_form' => $helper->generateForm(),
            'clean_cron_file' => $clean_cron_file
        ));
        // Template
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_default_template);
    }

    /**
     * Statistics settings action.
     * @return void
     */
    public function clearAction()
    {
        $this->presenter->noRender();
        LinevenApoStatistics::clear();
        die();
    }

    /**
     * Export to CSV.
     *
     * @param array $values Values
     * @return void
     */
    public function exportToCsv($values)
    {
        $enclosure = '"';
        $delimiter = ';';
        switch (Configuration::get('LINEVEN_APO_STATS_EXP_DELIMTER')) {
            case AdditionalProductsOrder::$export_delimiter_comma:
                $delimiter = ',';
                break;
            case AdditionalProductsOrder::$export_delimiter_semicolon:
                $delimiter = ';';
                break;
        }
        switch (Configuration::get('LINEVEN_APO_STATS_EXP_ENCLOSURE')) {
            case AdditionalProductsOrder::$export_enclosure_quote:
                $enclosure = '\'';
                break;
            case AdditionalProductsOrder::$export_enclosure_double_quote:
                $enclosure = '"';
                break;
        }
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=additionalproductsorder_'.time().'.csv');
        $out = fopen('php://output', 'w');
        foreach ($values as $fields) {
            fputcsv($out, $fields, $delimiter, $enclosure);
        }
        fclose($out);
    }
}
