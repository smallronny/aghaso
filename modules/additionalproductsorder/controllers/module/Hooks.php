<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminHooksController extends LinevenApoController
{
    /**
     * On order page settings action.
     * @return void
     */
    public function onOrderPageAction()
    {
        $helper = new LinevenApoHelperForm('Hooks', 'onOrderPage', 'cross_selling:on_order_page');
        $helper->base_folder = $this->presenter->path.'hooks/';
        $helper->base_tpl = 'onhook.tpl';
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminOnOrderPage();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your display settings have been updated.', 'Hooks')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $presenter_datas = array(
            'hook_code' => 'OOP'
        );
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm(null, $presenter_datas));
    }

    /**
     * On extra product page settings action.
     * @return void
     */
    public function onExtraProductPageAction()
    {
        $helper = new LinevenApoHelperForm('Hooks', 'onExtraProductPage', 'cross_selling:on_extra_product_page');
        $helper->base_folder = $this->presenter->path.'hooks/';
        $helper->base_tpl = 'onhook.tpl';
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminOnExtraProductPage();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your display settings have been updated.', 'Hooks')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $presenter_datas = array(
            'hook_code' => 'OEP'
        );
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm(null, $presenter_datas));
    }


    /**
     * Link to hook
     * @return void
     */
    public function linkToHookAction()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $return = 0;
        $this->presenter->noRender();
        if (Tools::isSubmit('hook_name') && trim(Tools::getValue('hook_name')) != '') {
            $return = (int)$configuration->getModule()->registerHook(Tools::getValue('hook_name'));
        }
        die(
            json_encode(
                array(
                    'return' => $return
                )
            )
        );
    }
}
