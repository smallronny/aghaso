<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminToolsController extends LinevenApoController
{
    /**
     * Design settings action.
     * @return void
     */
    public function designAction()
    {
        $helper = new LinevenApoHelperForm('Tools', 'design', 'tools:design');
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminToolsDesign($this->getDesignContent());
        $helper->base_folder = $this->presenter->path.'tools/';
        $helper->base_tpl = 'design.tpl';
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your design settings have been updated.', 'Tools')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm());
    }

    /**
     * Load design action.
     * @return void
     */
    public function loadDesignAction()
    {
        echo $this->getDesignContent(true);
        die();
    }

    /**
     * Get design content.
     * @param boolean $display Display to client
     * @return string
     */
    private function getDesignContent($display = false)
    {
        $return = '';
        if (trim(Tools::strlen(Tools::getValue('design_display_mode'))) != 0) {
            $load = true;
            $user_design = '';
            $display_mode = Tools::getValue('design_display_mode');
            $css = 'additionalproductsorder-'.
                Tools::strtolower(str_replace('_', '-', $display_mode)).'.css';

            if (Configuration::get('LINEVEN_APO_DESIGN_'.$display_mode) !== false
                && Configuration::get('LINEVEN_APO_DESIGN_'.$display_mode) != '') {
                $load = false;
                $user_design = (Tools::strlen(trim(Tools::getValue('design_textarea'))) != 0
                    ? Tools::getValue('design_textarea')
                    : Configuration::get('LINEVEN_APO_DESIGN_'.$display_mode));
            }

            if (trim(Tools::strlen(Tools::getValue('default_style'))) != 0
                && Tools::getValue('default_style') == 'true') {
                $load = true;
            }
            if ($load) {
                $return = Tools::file_get_contents(
                    _LINEVEN_MODULE_APO_REQUIRE_DIR_.'/views/css/'.$css
                );
            } else {
                $return = $user_design;
            }
        } else {
            // Return the design of the selected template for hook
            $selected_template_hook = Configuration::get('LINEVEN_APO_OOP_DISPLAY_MODE');
            $default_css_file = 'list-a';
            $get_content_file = true;
            if (array_key_exists($selected_template_hook, AdditionalProductsOrder::$native_templates)) {
                if (Configuration::get('LINEVEN_APO_DESIGN_'.$selected_template_hook) !== false
                    && Configuration::get('LINEVEN_APO_DESIGN_'.$selected_template_hook) != '') {
                    $get_content_file = false;
                    $return = Configuration::get('LINEVEN_APO_DESIGN_'.$selected_template_hook);
                } else {
                    $default_css_file = Tools::strtolower(str_replace('_', '-', $selected_template_hook));
                }
            }
            if ($get_content_file) {
                $return = Tools::file_get_contents(
                    _LINEVEN_MODULE_APO_REQUIRE_DIR_.'/views/css/'.
                    'additionalproductsorder-'.$default_css_file.'.css'
                );
            }
        }
        if ($display) {
            echo $return;
        } else {
            return $return;
        }
    }

    /**
     * Custom css action.
     * @return void
     */
    public function customCssAction()
    {
        $helper = new LinevenApoHelperForm('Tools', 'customCss', 'tools:custom_css');
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminToolsCustomCss();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your custom CSS have been updated.', 'Tools')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_content, $helper->generateForm());
    }
}
