<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminSettingsController extends LinevenApoController
{
    /**
     * Module settings action.
     * @return void
     */
    public function moduleAction()
    {
        $helper = new LinevenApoHelperForm('Settings', 'module', 'settings:module');
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminSettingsModule();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your module settings have been updated.', 'Settings')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm());
    }
    
    /**
     * Display settings action.
     * @return void
     */
    public function displayAction()
    {
        $helper = new LinevenApoHelperForm('Settings', 'display', 'cross_selling:display');
        $helper->base_folder = $this->presenter->path.'settings/';
        $helper->base_tpl = 'display.tpl';
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminSettingsDisplay();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your display settings have been updated.', 'Settings')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm());
    }


    /**
     * Link to hook
     * @return void
     */
    public function linkToHookAction()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $return = 0;
        $this->presenter->noRender();
        if (Tools::isSubmit('hook_name') && trim(Tools::getValue('hook_name')) != '') {
            $return = (int)$configuration->getModule()->registerHook(Tools::getValue('hook_name'));
        }
        die(
            json_encode(
                array(
                    'return' => $return
                )
            )
        );
    }
    
    /**
     * Availability settings action.
     * @return void
     */
    public function availabilityAction()
    {
        $helper = new LinevenApoHelperForm('Settings', 'availability', 'additional_settings:availability');
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminSettingsAvailability();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your availability settings have been updated.', 'Settings')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm());
    }

    /**
     * Combinations settings action.
     * @return void
     */
    public function combinationsAction()
    {
        $helper = new LinevenApoHelperForm('Settings', 'combinations', 'additional_settings:combinations');
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper->form = new LinevenApoFormAdminSettingsCombinations();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your combinations settings have been updated.', 'Settings')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm());
    }
}
