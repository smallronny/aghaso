<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminAssociationsController extends LinevenApoController
{
    /**
     * List action.
     * @return void
     */
    public function listAction()
    {
        $listing_presenter = new LinevenApoAssociationAdminListingPresenter();
        $associations_search = new LinevenApoAssociationAdminListingSearch();
        $helper_options = array(
            'show_filters' => (int)Configuration::get('LINEVEN_APO_BO_LIST_FILTER'),
            'table_id' => 'lapo-associations-list',
            'order_by' => 'order_display',
            'order_way' => 'ASC',
            'reorder_available' => true
        );
        $helper = new LinevenApoHelperList('Associations', 'list', 'cross_selling:associations', $helper_options);
        $helper->identifier = 'id';
        $helper->title = $this->translator->l('Associations list', 'Associations');
        $helper->list_id = 'apo_associations';
        $helper->addToolbarButton('new', array(
            'href' => LinevenApoTools::getBackofficeURI('Associations', 'addEdit', 'cross_selling:associations:add'),
            'desc' => $this->translator->l('Add new association', 'Associations')
        ));
        $helper->setBulkActions(array(
            'delete' => array(
                'text' => $this->translator->l('Delete selected', 'Associations'),
                'icon' => 'icon-trash',
                'confirm' => $this->translator->l('Delete selected associations ?', 'Associations'),
                'callback' => 'bulkActionDelete',
                'callback_object' => $this,
            )
        ));
        $helper->setActions(array(
            'edit' => LinevenApoTools::getBackofficeURI('Associations', 'addEdit', 'cross_selling:associations:edit'),
            'delete' => LinevenApoTools::getBackofficeURI('Associations', 'delete', 'cross_selling:associations:delete')
        ));

        // Prepare the generation
        $helper->prepareList($listing_presenter->getFields());
        // Set sort
        $listing_presenter->setAuthorizedSort($helper->isReorderAuthorized());

        // Set messages for action return
        $listing_presenter->setMessage($this->params);

        // Return output
        $this->presenter->setDatas($listing_presenter->present());
        $associations_search->runQuery($helper->getDisplaySettings());
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_content, $helper->renderList(
            $listing_presenter->presentList($associations_search->getResults()),
            $associations_search->getCount()
        ));
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_default_template);
    }
    
    /**
     * Add or edit action.
     * @return void
     */
    public function addEditAction()
    {
        $association_presenter = new LinevenApoAssociationAdminPresenter();
        $helper = new LinevenApoHelperForm('Associations', 'addEdit', 'cross_selling:associations:edit');
        $helper->form =  new LinevenApoFormAdminAssociation();
        $helper->base_folder = $this->presenter->path.'associations/';
        $helper->base_tpl = 'addEdit.tpl';
        $association = new LinevenApoAssociation();
        $id_association = null;
        if (Tools::isSubmit('id')
            && Tools::getValue('id') != 0 && Tools::getValue('id') != '') {
            $id_association = Tools::getValue('id');
            $association = new LinevenApoAssociation($id_association);
        }
        $helper->form->populate($association);
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $id_association_updated = LinevenApoAssociation::staticUpdate($helper->form);
                if (!Tools::isSubmit('save_stay')) {
                    $this->presenter->noRender();
                    return $this->redirect(
                        'cross_selling:associations',
                        array('action_return' => array(
                            'action' => 'update',
                            'message' => $this->translator->l('Your association has been updated.', 'Associations')
                        ))
                    );
                } else {
                    $association->id = $id_association_updated;
                    $helper->form->setValue('id', $id_association_updated);
                    $association_presenter->setConfirmationMessage(
                        $this->translator->l('Your association has been updated.', 'Associations')
                    );
                }
            } else {
                $association_presenter->setErrorMessage($helper->form->getErrors());
            }
        }
        $association_presenter->setAssociation($association);
        $association_presenter->setCancelUrl(LinevenApoTools::getBackofficeURI(
            'Associations',
            'list',
            'cross_selling:associations'
        ));
        $this->presenter->setDatas($association_presenter->present());
        $this->presenter->addOutput('content', $helper->generateForm(null, $this->presenter->getDatas()));
    }
    
    /**
     * Get products action.
     * @return void
     */
    public function getProductsAction()
    {
        $this->presenter->noRender();
        $products_search = new LinevenApoAssociationAdminProductsSearch();
        $items = $products_search->runQuery(Tools::getValue('q', false), 'query');
        if ($items) {
            foreach ($items as $item) {
                echo (int)$item['id_product'].'|'.
                    $item['name'].'|'.
                    $item['reference'].'|'.
                    LinevenApoProductPresenter::getThumbnailImage((int)$item['id_product'])."\n";
            }
        }
        die;
    }

    /**
     * Preview product action.
     * @return void
     */
    public function previewProductAction()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->presenter->noRender();
        if (Tools::isSubmit('for') && Tools::getValue('for') != '' &&
            Tools::isSubmit('product_object') && Tools::getValue('product_object') != '') {
            $id_product = (int)Tools::getValue(Tools::getValue('for'));
            if (Validate::isUnsignedId($id_product) && $id_product > 0) {
                $products_search = new LinevenApoAssociationAdminProductsSearch();
                $product = $products_search->runQuery((int)$id_product, 'id');
                if ($product != null) {
                    Context::getContext()->smarty->assign(array(
                        'link' => Context::getContext()->link,
                        Tools::getValue('product_object') => $product
                    ));
                    die(
                        Tools::jsonEncode(
                            array(
                                'html' => $configuration->getModule()->display(
                                    _PS_MODULE_DIR_.$configuration->getModule()->name,
                                    $this->presenter->partial_path_view.'associations/addedit/'.
                                        Tools::getValue('product_object').'_details.tpl'
                                )
                            )
                        )
                    );
                }
            }
        }
    }

    /**
     * Delete action.
     * @return void
     */
    public function deleteAction()
    {
        $this->presenter->noRender();
        if (Tools::isSubmit('id') && Tools::getValue('id') != '') {
            LinevenApoAssociation::staticDelete(Tools::getValue('id'));
        }
        return $this->redirect(
            'cross_selling:associations',
            array('action_return' => array(
                'action' => 'delete',
                'message' => $this->translator->l('Your association has been deleted.', 'Associations')
            ))
        );
    }

    /**
     * Bulk delete.
     * @param Array $datas Datas
     * @return void
     */
    public function bulkActionDelete($datas)
    {
        LinevenApoAssociation::staticDelete($datas);
        $this->params['action_return'] = array(
            'action' => 'bulk_delete',
            'message' => $this->translator->l('Selected associations have been deleted.', 'Associations')
        );
    }

    /**
     * Order action.
     * @return void
     */
    public function orderAction()
    {
        $this->presenter->noRender();
        if (Tools::isSubmit('positions') && Tools::isSubmit('first_position') && Tools::isSubmit('last_position')) {
            LinevenApoAssociation::order(
                Tools::getValue('positions'),
                Tools::getValue('first_position'),
                Tools::getValue('last_position')
            );
        }
        die();
    }
}
