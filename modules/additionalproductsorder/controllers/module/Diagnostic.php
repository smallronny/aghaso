<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminDiagnosticController extends LinevenApoModuleDiagnosticController
{
    protected $is_functional_diagnostic = true;

    /*
     * Check product image used.
     *
     * @return array
     */
    protected function checkProductImageUsed()
    {
        $product_image_found = LinevenApoAdminDiagnostic::checkProductImageUsed();
        $this->has_output = !$product_image_found;
    }

    /*
     * Check smarty template used.
     *
     * @return array
     */
    protected function checkSmartyTemplateUsed()
    {
        $template_found = LinevenApoAdminDiagnostic::checkSmartyTemplateUsed();
        $this->has_output = !$template_found;
    }

    /**
     * Set steps
     *
     * @return void
     */
    public function setSteps()
    {
        parent::setSteps();
        $this->steps[] = array(
            'step' => 'product_image',
            'method' => 'checkProductImageUsed',
            'operation' => 'repair',
            'level' => 'warning',
            'repair' => array(
                'is_automatic' => false,
            ),
            'outputs' => array(
                'checking' => array(
                    'progression_text' => $this->translator->l('Check product image used', 'Diagnostic'),
                    'error_message' => $this->translator->l('Product image used is not a correct product image type. Check the display configuration.', 'Diagnostic'),
                    'hide_results' => true
                ),
                'target' => 'functional'
            )
        );
        $this->steps[] = array(
            'step' => 'template_used',
            'method' => 'checkSmartyTemplateUsed',
            'operation' => 'repair',
            'level' => 'warning',
            'repair' => array(
                'is_automatic' => false,
            ),
            'outputs' => array(
                'checking' => array(
                    'progression_text' => $this->translator->l('Check smarty template used', 'Diagnostic'),
                    'error_message' => $this->translator->l('Smarty template used is not exists in folder. Check your configuration.', 'Diagnostic'),
                    'hide_results' => true
                ),
                'target' => 'functional'
            )
        );
    }
}
