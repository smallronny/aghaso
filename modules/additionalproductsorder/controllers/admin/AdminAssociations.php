<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

require_once(dirname(__FILE__).'/../../additionalproductsorder.php');

class AdminAssociationsController extends ModuleAdminController
{
    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        $module_context = LinevenApoContext::getContext();
        $module_context->setAccess(LinevenApoContext::$access_by_admin_tab);

        $this->bootstrap = true;
        $this->table = 'apo';
        $this->list_id = 'apo';
        $this->className = 'AdminAssociations';

        parent::__construct();

        $this->module->init(true);
    }
    
    /**
     * Init page header.
     */
    public function initPageHeaderToolbar()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $translator = new LinevenApoTranslator();
        if (Tools::isSubmit($configuration->name.'_action') &&
            Tools::getValue($configuration->name.'_action') == 'addEdit') {
            $this->page_header_toolbar_btn['save_stay'] = array(
                'href' => '#',
                'js' => 'LinevenApoModule.Associations.doOnClickAssociationSave('.(int)Tools::isSubmit('id').', true)',
                'desc' => $translator->l('Save and stay', 'AdminAssociations'),
                'icon' => 'process-icon-save'
            );
            $this->page_header_toolbar_btn['save'] = array(
                'href' => '#',
                'js' => 'LinevenApoModule.Associations.doOnClickAssociationSave('.(int)Tools::isSubmit('id').', false)',
                'desc' => $translator->l('Save', 'AdminAssociations')
            );
            $this->page_header_toolbar_btn['cancel'] = array(
                'href' => $this->context->link->getAdminLink('AdminAssociations'),
                'desc' => $translator->l('Cancel', 'AdminAssociations')
            );
        } else {
            $this->page_header_toolbar_btn['new'] = array(
                'href' => LinevenApoTools::getBackofficeURI('Associations', 'addEdit', 'associations:list:add'),
                'desc' => $translator->l('New association', 'AdminAssociations')
            );
            $this->page_header_toolbar_btn['refresh'] = array(
                'href' => LinevenApoTools::getBackofficeURI('Associations', 'list', 'associations:list'),
                'desc' => $translator->l('Refresh', 'AdminAssociations')
            );
        }
        $this->page_header_toolbar_btn['module'] = array(
            'href' => LinevenApoTools::getAdminModuleLink(),
            'desc' => $translator->l('Module', 'AdminAssociations'),
            'icon' => 'icon-puzzle-piece lineven-icon-toolbar'
        );
        return parent::initPageHeaderToolbar();
    }
    
    /**
     * Display.
     *
     * @return void
     */
    public function display()
    {
        $this->context->smarty->assign(array(
            'content' => $this->module->getContent()
        ));
        parent::display();
    }
    
    /**
     * Set media.
     * @param boolean $isNewTheme Is new theme
     */
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia(false);
        $this->module->setBackofficeMedia();
    }
}
