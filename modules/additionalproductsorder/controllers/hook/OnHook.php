<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFrontOnHookController extends LinevenApoController
{
    protected $hook_code;
    protected $hook_folder;
    protected $hook_classname;

    /**
     * Get hook presenter.
     * @return void
     */
    protected function getHookPresenter()
    {
        $products_presenter = new LinevenApoProductsListPresenter($this->hook_code, $this->hook_folder, $this->controller_name);

        // Context
        $id_related_product = 0;
        $context = Context::getContext();
        $cart = $context->cart;
        $product_context = LinevenApoProductSearchContext::getContext();
        $product_context->setCartAmount($cart->getOrderTotal());
        if ($context->controller->php_self == 'product') {
            if (Tools::isSubmit('id_product') && (int)Tools::getValue('id_product') != 0) {
                $id_related_product = (int)Tools::getValue('id_product');
            }
        }

        // Products query
        $product_query = new LinevenApoProductSearchQuery();
        $product_query->setHook($this->hook_code);
        $product_query->setLimit((int)Configuration::get('LINEVEN_APO_'.$this->hook_code.'_MAX_PRODUCTS'));
        $product_query->setSortDisplayMethod(
            Configuration::get('LINEVEN_APO_'.$this->hook_code.'_SORT_DISPLAY'),
            Configuration::get('LINEVEN_APO_'.$this->hook_code.'_SORT_DISPLAY_WAY')
        );
        $product_query->setRandomAssociations((int)Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RANDOM_ASSOCIATIONS'));
        $product_query->setRandomSearch((int)Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RANDOM_SEARCH'));
        if ($id_related_product != 0) {
            $product_query->setIdProduct($id_related_product);
            //$product_query->setProductsToExlude($cart->getProducts());
        } else {
            $product_query->setRelatedProducts($cart->getProducts());
        }

        // Search
        $research_priority = array(
            AdditionalProductsOrder::$research_priority_accessories,
            AdditionalProductsOrder::$research_priority_associations,
            AdditionalProductsOrder::$research_priority_purchased_together
        );
        if (Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RESEARCH_PRIORITY') &&
            Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RESEARCH_PRIORITY') != '') {
            $research_priority = unserialize(Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RESEARCH_PRIORITY'));
        }
        $crossselling_search = new LinevenApoCrossSellingProductsSearch($this->hook_code);
        $crossselling_search->setResultsSeparated((int)Configuration::get('LINEVEN_APO_'.$this->hook_code.'_IS_SEPARATE_DISPLAY'));
        $crossselling_search->setSections(
            $research_priority,
            array(
                AdditionalProductsOrder::$research_priority_accessories => LinevenApoTranslator::getTranslationFromConfiguration(
                    'LINEVEN_APO_'.$this->hook_code.'_TITLE_ACCESSORIES',
                    $this->translator->l('Accessoires', 'ShoppingCart')
                ),
                AdditionalProductsOrder::$research_priority_associations => LinevenApoTranslator::getTranslationFromConfiguration(
                    'LINEVEN_APO_'.$this->hook_code.'_TITLE_ASSOCIATIONS',
                    $this->translator->l('To complete your order, we offer you those products', 'ShoppingCart')
                ),
                AdditionalProductsOrder::$research_priority_purchased_together => LinevenApoTranslator::getTranslationFromConfiguration(
                    'LINEVEN_APO_'.$this->hook_code.'_TITLE_PURCHASED_TOGETHER',
                    $this->translator->l('Often purchased together', 'ShoppingCart')
                )
            )
        );
        $products = $crossselling_search->runQuery($product_context, $product_query);

        if (isset($this->params['refresh']) && $this->params['refresh'] == true) {
            $products_presenter->setRefresh(true);
        }
        $products_presenter->setTitle(
            LinevenApoTranslator::getTranslationFromConfiguration(
                'LINEVEN_APO_'.$this->hook_code.'_TITLE',
                $this->translator->l('To complete your order, we offer you those products', 'ShoppingCart')
            )
        );
        $products_presenter->setProducts($products);
        $products_presenter->setIdRelatedProduct($id_related_product);
        $products_presenter->setHookClassname($this->hook_classname);
        $products_presenter->setSeparateResults((int)Configuration::get('LINEVEN_APO_'.$this->hook_code.'_IS_SEPARATE_DISPLAY'));

        return $products_presenter;
    }
}
