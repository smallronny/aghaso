<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFrontExtraProductPageController extends LinevenApoFrontOnHookController
{
    protected $hook_code = 'OEP';
    protected $hook_folder = 'extraproductpage';
    protected $hook_classname = 'product-extra';

    /**
     * Index action.
     * @return void
     */
    public function indexAction()
    {
        if ($this->module_context->isModuleActive() &&
            Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE')) {
            $hook_presenter = $this->getHookPresenter();
            if ($hook_presenter) {
                $hook_presenter->setAjaxPutToCart(false);
                $hook_presenter->setAddToCartDisplay(
                    (int)Configuration::get('LINEVEN_APO_OEP_DISPLAY_CART'),
                    (int)Configuration::get('LINEVEN_APO_OEP_DISPLAY_CART_CHK'),
                    (int)Configuration::get('LINEVEN_APO_OEP_DISPLAY_CART_CHK_ICO')
                );
                $this->presenter->setDatas($hook_presenter->present());
                $this->presenter->addOutput(LinevenApoPresenter::$output_type_template, $hook_presenter->getTemplate());
            }
        }
    }

    /**
     * Widget action.
     * @return void
     */
    public function widgetAction()
    {
        if ($this->module_context->isModuleActive() &&
            Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE') &&
            Configuration::get('LINEVEN_APO_OEP_HOOK_USE') == AdditionalProductsOrder::$hook_to_use_specific_theme) {
            $this->indexAction();
        }
    }
}
