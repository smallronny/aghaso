<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFrontHeaderController extends LinevenApoController
{
    /**
     * Index action.
     * @return void
     */
    public function indexAction()
    {
        if ($this->module_context->isModuleActive()) {
            // Clear Statistics < 2 month
            if (Configuration::get('LINEVEN_APO_STATS_ACTIVE')) {
                $date_last_clear = Configuration::get('LINEVEN_APO_STATS_LAST_CLEAR');
                $date_stored = new DateTime();
                $date_now = new DateTime();
                $to_clear = false;
                if ($date_last_clear != null) {
                    $date_stored = new DateTime($date_last_clear);
                    $interval = $date_now->diff($date_stored);
                    if ($interval->format('%m') >= 2) {
                        $to_clear = true;
                    }
                } else {
                    $to_clear = true;
                }
                if ($to_clear) {
                    $date_now->modify('first day of this month');
                    $date_now->sub(new DateInterval('P1M'));
                    $new_date = $date_now->format('Y-m-d');
                    LinevenApoStatistics::clear($new_date);
                }
            }

            // Get all templates used only for page activated
            $templates_used = array();
            if (Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE')) {
                $templates_used[Configuration::get('LINEVEN_APO_OEP_DISPLAY_MODE')] = array('folder' => 'extraproductpage');
            }
            if (Configuration::get('LINEVEN_APO_OOP_IS_ACTIVE')) {
                $templates_used[Configuration::get('LINEVEN_APO_OOP_DISPLAY_MODE')] = array('folder' => 'shoppingcart');
            }
            $assets = array();
            foreach ($templates_used as $template => $def) {
                $use_template_file = false;
                $template_folder = _LINEVEN_MODULE_APO_REQUIRE_DIR_.'/views/templates/hook/';
                if (!array_key_exists($template, AdditionalProductsOrder::$native_templates)) {
                    if (is_file($template_folder.$def['folder'].'/'.Tools::strtolower($template).'.tpl')) {
                        $use_template_file = true;
                    } else {
                        $template = AdditionalProductsOrder::$template_standard_list_a;
                    }
                }
                if (!$use_template_file && !Configuration::get('LINEVEN_APO_DEFAULT_DESIGN') && Configuration::get('LINEVEN_APO_DESIGN_'.$template) != '') {
                    $assets[] = Minify_CSS::minify(
                        Configuration::get('LINEVEN_APO_DESIGN_'.$template),
                        array(
                            'compress' => true,
                            'removeCharsets' => true,
                            'preserveComments' => false
                        )
                    );
                } else {
                    if (!$use_template_file) {
                        $template = str_replace('_', '-', $template);
                    }
                    Context::getContext()->controller->registerStylesheet(
                        'modules-relatedproducts-'.Tools::strtolower($template),
                        LinevenApoTools::getShortURI().'/views/css/additionalproductsorder-'.Tools::strtolower($template).'.css',
                        array('media' => 'all')
                    );
                }
            }
            Context::getContext()->controller->registerStylesheet(
                'modules-additionalproductsorder-global',
                LinevenApoTools::getShortURI().'views/css/additionalproductsorder-global.css',
                array('media' => 'all')
            );
            Context::getContext()->controller->registerJavascript(
                'modules-additionalproductsorder-js',
                LinevenApoTools::getShortURI().'views/js/additionalproductsorder.js'
            );
            Context::getContext()->controller->registerStylesheet(
                'modules-additionalproductsorder-busy-css',
                LinevenApoTools::getShortURI().'/views/css/vendor/busy-load/app.min.css',
                array('media' => 'all')
            );
            Context::getContext()->controller->registerJavascript(
                'modules-additionalproductsorder-busy-js',
                LinevenApoTools::getShortURI().'/views/js/vendor/busy-load/app.min.js'
            );
            Context::getContext()->controller->registerStylesheet(
                'modules-additionalproductsorder-custom-css',
                LinevenApoTools::getShortURI().'/views/css/additionalproductsorder-custom.css',
                array('media' => 'all')
            );
            if (version_compare(_PS_VERSION_, '1.7.6', '>=') &&
                (int)Configuration::get('LINEVEN_APO_PARTNER_RVW_RATE') &&
                Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE') == AdditionalProductsOrder::$partner_reviews_module_prestashop) {
                $cssUrl = '/modules/productcomments/views/css/productcomments.css';
                $jsUrl = '/modules/productcomments/views/js/jquery.rating.plugin.js';
                Context::getContext()->controller->registerStylesheet(sha1($cssUrl), $cssUrl, ['media' => 'all', 'priority' => 80]);
                Context::getContext()->controller->registerJavascript(sha1($jsUrl), $jsUrl, ['position' => 'bottom', 'priority' => 80]);
            }

            // JS Def
            $partners_reviews_module = false;
            $partners_reviews_module_grade_url = '';
            if ((int)Configuration::get('LINEVEN_APO_PARTNER_RVW_RATE')) {
                if (version_compare(_PS_VERSION_, '1.7.6', '>=') &&
                    Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE') == AdditionalProductsOrder::$partner_reviews_module_prestashop) {
                    $partners_reviews_module = AdditionalProductsOrder::$partner_reviews_module_prestashop;
                    $partners_reviews_module_grade_url = Context::getContext()->link->getModuleLink('productcomments', 'CommentGrade');
                } else {
                    $partners_reviews_module = Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE');
                }
            }
            $current_page = '';
            $refresh_mode = 'NOTHING';
            $refresh_delay = 300;
            if (Configuration::get('LINEVEN_APO_OOP_IS_ACTIVE') &&
                (Context::getContext()->controller->php_self == 'cart' ||
                    Context::getContext()->controller->php_self == 'order' ||
                    Context::getContext()->controller->php_self == 'order-opc')) {
                $current_page = 'cart';
                $refresh_mode = Configuration::get('LINEVEN_APO_OOP_REFRESH_MODE');
                $refresh_delay = Configuration::get('LINEVEN_APO_OOP_REFRESH_DELAY');
            } else {
                if (Configuration::get('LINEVEN_APO_OEP_IS_ACTIVE') &&
                    !(Tools::isSubmit('action') &&  Tools::getValue('action') == 'quickview') &&
                    Context::getContext()->controller->php_self == 'product') {
                    $current_page = 'product';
                    Context::getContext()->controller->registerStylesheet(
                        'modules-additionalproductsorder-tooltip-style',
                        LinevenApoTools::getShortURI().'views/css/vendor/tooltipster/tooltipster.bundle.min.css',
                        array('media' => 'all')
                    );
                    Context::getContext()->controller->registerStylesheet(
                        'modules-additionalproductsorder-tooltip-theme',
                        LinevenApoTools::getShortURI().'views/css/vendor/tooltipster/tooltipster-sideTip-light.min.css',
                        array('media' => 'all')
                    );
                    Context::getContext()->controller->registerJavascript(
                        'modules-additionalproductsorder-tooltip-script',
                        LinevenApoTools::getShortURI().'views/js/vendor/tooltipster/tooltipster.bundle.min.js'
                    );
                }
            }
            LinevenApoPresenter::addJsDef(array(
                'current_page' => $current_page,
                'refresh_mode' => $refresh_mode,
                'refresh_delay' => $refresh_delay,
                'statistics_active' => Configuration::get('LINEVEN_APO_STATS_ACTIVE'),
                'statistics_add_to_cart' => (Configuration::get('LINEVEN_APO_STATS_ACTIVE') &&
                    Configuration::get('LINEVEN_APO_STATS_ADD_CART')),
                'statistics_viewed' => (Configuration::get('LINEVEN_APO_STATS_ACTIVE') &&
                    Configuration::get('LINEVEN_APO_STATS_IS_VIEWED')),
                'partners_reviews_module' => $partners_reviews_module,
                'partners_reviews_module_grade_url' => $partners_reviews_module_grade_url,
            ));
            $this->presenter->addData('assets', $assets);
            $this->presenter->addOutput(LinevenApoPresenter::$output_type_template, 'header');
        }
    }

    /**
     * PostRun.
     *
     * @return void
     */
    public function postRun()
    {
        parent::postRun();
        // Render JS Def
        $this->presenter->renderJsDef(false);
    }
}
