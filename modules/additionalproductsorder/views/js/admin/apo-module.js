/**
 * Script Module.
 *
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

var LinevenApoModule = {
	
	/**
	 * Associations.
	 */
	Associations: {
	
		positions: {first: 0, last: 0}, 

		/**
		 * Prepare content for associations list.
		 * @param boolean dnd_allowed Dnd allowed
		 */
		prepareContentListAssociations: function(dnd_allowed) {
			if (dnd_allowed) {
				LinevenApoModule.Associations.initDndOnList();
			}
			$('.helper-list-options-icon-tooltip').tooltipster({
				theme: ['tooltipster-light', 'tooltipster-apo-associations-list-customized'],
				contentCloning: false,
				delay: 100
			});
			$('.lapo-associations-list-products-toggle-other a').on('click', function (event) {
				if ($(this).hasClass('lapo-associations-list-products-hide')) {
					$(this).find('span:first').hide();
					$(this).find('span:nth-child(2)').show();
					$(this).removeClass('lapo-associations-list-products-hide');
					$(this).addClass('lapo-associations-list-products-show');
					$(this).closest('.lapo-associations-list-products').find('.lapo-associations-list-products-other').show();
				} else {
					$(this).removeClass('lapo-associations-list-products-show');
					$(this).addClass('lapo-associations-list-products-hide');
					$(this).closest('.lapo-associations-list-products').find('.lapo-associations-list-products-other').hide();
					$(this).find('span:first').show();
					$(this).find('span:nth-child(2)').hide();
				}
			});
		},
		
		/**
		 * Prepare content to add / edit association.
		 */
		prepareContentAddEditAssociation: function() {
			// Prepare display product search
			$('#display_product_content_input').autocomplete(LinevenApo.getActionUri()+'&'+LinevenApo.module_name+'_controller=Associations&'+LinevenApo.module_name+'_action=getProducts', {
				minChars: 1,
				autoFill: true,
				max: 40,
				matchContains: true,
				mustMatch: true,
				scroll: false,
				cacheLength: 0,
				formatItem: function(item) {
					return '<div>' +
					   '<table><tr>' +
					   '<td style="padding-right: 10px">'+ item[3] +'</td>' +
					   '<td>' + item[1] + '<br />Ref: ' + item[2] + '</td>' +
					   '</tr></table></div>';
				}
			}).result(LinevenApoModule.Associations.addDisplayProductToAssociation);
			// Prepare related product search
			$('#related_product_content_input').autocomplete(LinevenApo.getActionUri()+'&'+LinevenApo.module_name+'_controller=Associations&'+LinevenApo.module_name+'_action=getProducts', {
				minChars: 1,
				autoFill: true,
				max: 40,
				matchContains: true,
				mustMatch: true,
				scroll: false,
				cacheLength: 0,
				formatItem: function(item) {
                    return '<div>' +
                        '<table><tr>' +
                        '<td style="padding-right: 10px">'+ item[3] +'</td>' +
                        '<td>' + item[1] + '<br />Ref: ' + item[2] + '</td>' +
                        '</tr></table></div>';
				}
			}).result(LinevenApoModule.Associations.addRelatedProductToAssociation);
			if ($('#id_related_product').val() != '') {
				LinevenApoModule.Associations.previewRelatedProduct($('#id_related_product').val());
			}
			// On association type changed
			LinevenApoModule.Associations.doOnAssociationTypeChange();
			$('#association_type').change(function() {
				LinevenApoModule.Associations.doOnAssociationTypeChange();
			});

			// On association display choice changed
			LinevenApoModule.Associations.doOnDisplayChoiceChange();
			$('#display_choice').change(function() {
				LinevenApoModule.Associations.doOnDisplayChoiceChange();
			});

			LinevenApoModule.Associations.initDndOnProductsList();
		},
		
		/**
		 * Add display product to association.
		 */
		addDisplayProductToAssociation: function(event, data, formatted) {
			if (data == null)
				return false;
			var id_product = parseInt(data[0]);
			var productName = data[1];
			$('#display_product_content_input').val('');
			var already_added = false;
			$('input[name="displayed_products_list[]"]').each(function() {
				if ($(this).val() == id_product) {
					already_added = true;
				}
			});
			if (!already_added) {
				LinevenApoModule.Associations.previewDisplayProduct(id_product);
			}
		},
		
		/**
		 * Add related product to association.
		 */
		addRelatedProductToAssociation: function(event, data, formatted) {
			if (data == null)
				return false;
			var id_product = parseInt(data[0]);
			var productName = data[1];
			$('#related_product_content_input').val('');
			$('#id_related_product').val(id_product);
			LinevenApoModule.Associations.previewRelatedProduct(id_product);
		},
		
		/**
		 * Preview for display product.
		 */
		previewDisplayProduct: function(id_product) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: LinevenApo.getActionUri()+'&'+LinevenApo.module_name+'_controller=Associations&'+LinevenApo.module_name+'_action=previewProduct&for=id_product_displayed&product_object=displayed_products',
				data: {
					id_product_displayed: id_product
				},
				cache: false,
				success: function(jsonData, textStatus, jqXHR) {
					if (jsonData != undefined && jsonData.html != undefined) {
						$('#displayed-products-preview-table>tbody').append(jsonData.html);
						LinevenApoModule.Associations.initDndOnProductsList();
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				},
				complete: function(jqXHR, textStatus) {
				}
			});
		},
		
		/**
		 * Preview for related product.
		 */
		previewRelatedProduct: function(id_product) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: LinevenApo.getActionUri()+'&'+LinevenApo.module_name+'_controller=Associations&'+LinevenApo.module_name+'_action=previewProduct&for=id_related_product&product_object=related_product',
				data: {
					id_related_product: id_product
				},
				cache: false,
				success: function(jsonData, textStatus, jqXHR) {
					if (jsonData != undefined && jsonData.html != undefined) {
						$('#related-product-preview-table>tbody').html(jsonData.html);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				},
				complete: function(jqXHR, textStatus) {
				}
			});
		},
		
		/**
		 * Remove related product.
		 */
		removeRelatedProduct: function(id_product) {
			$('#id_related_product').val('');
			$('#related-product-preview-table>tbody').html('');
		},
		
		/**
		 * Remove displayed product.
		 */
		removeDisplayedProduct: function(id_product) {
			$('#displayed-products-preview-table>tbody #displayed-product-'+id_product).remove();
		},
		
		/**
		 * Do on association type changement.
		 */
		doOnAssociationTypeChange: function() {
			if ($('#association_type').val() == 'ALL') {
				$('div[id^="fieldset_related_category"]').hide();
				$('div[id^="fieldset_related_product"]').hide();
	    	}
	    	if ($('#association_type').val() == 'CATEGORY') {
				$('div[id^="fieldset_related_category"]').show();
				$('div[id^="fieldset_related_product"]').hide();
	    	}
	    	if ($('#association_type').val() == 'PRODUCT') {
				$('div[id^="fieldset_related_category"]').hide();
				$('div[id^="fieldset_related_product"]').show();
	    	}
		},

		/**
		 * Do on association display choice changement.
		 */
		doOnDisplayChoiceChange: function() {
			if ($('#display_choice').val() == 'CATEGORY') {
				$('div[id^="fieldset_display_category"]').show();
				$('div[id^="fieldset_displayed_products"]').fadeOut();
			}
			if ($('#display_choice').val() == 'PRODUCTS') {
				$('div[id^="fieldset_display_category"]').fadeOut();
				$('div[id^="fieldset_displayed_products"]').show();
			}
		},

		/**
		 * Save association.
		 * @param int $id Association Id
		 * @param boolean $stay Stay
		 */
		doOnClickAssociationSave: function (id, stay) {
			if ($('#module_form').length) {
				if (stay) {
					$('#module_form').prepend('<input type="hidden" name="save_stay" value="1">');
				}
				$('#module_form').submit();
			}
		},
		
		/**
		 * Cancel association.
		 */
		doOnClickAssociationCancel: function () {
			window.location = LinevenApo.getActionUri()+'&'+LinevenApo.module_name+'_controller=Associations&'+LinevenApo.module_name+'_action=list';
		},

		/**
		 * Drag & Drop initialization for products list.
		 */
		initDndOnProductsList: function() {
			$("#displayed-products-preview-table").tableDnD({
				onDragStart: function(table, row) {
					$('#displayed-products-preview-table > tbody tr').animate({opacity: 0.2}, 200);
					$(row).animate({opacity: 1}, 200);
				},
				onDrop: function(table, row) {
					$('#displayed-products-preview-table > tbody tr').animate({opacity: 1}, 200);
				}
			});
		},

		/**
		 * Drag & Drop initialization.
		 */
		initDndOnList: function() {
			$("#table-lapo-associations-list").tableDnD({
				onDragStart: function(table, row) {
					$('.lapo-associations-list-item').animate({opacity: 0.2}, 200);
					$('#'+$(row).attr('id')).animate({opacity: 1}, 200);
				},
				onDrop: function(table, row) {
					$('.lapo-associations-list-item').animate({opacity: 1}, 200);
					LinevenApoModule.Associations.dndSetPositions();
				}
			});
			LinevenApoModule.Associations.initPositions();
		},
		
		/**
		 * Get Row id for dnd.
		 */
		dndGetRowId: function(row_tag_id) {
			return row_tag_id.substr(4, (row_tag_id.length - 6));
		},
		
		/**
		 * Init positions.
		 */
		initPositions: function () {
			$('#table-lapo-associations-list>tbody tr.lapo-associations-list-item').each(function(index) {
				if (index == 0) {
					LinevenApoModule.Associations.positions.first = parseInt($(this).find('.lineven-helper-list-position').html().trim());
				}
				LinevenApoModule.Associations.positions.last = parseInt($(this).find('.lineven-helper-list-position').html().trim());
			});
		},
		
		/**
		 * Set positions.
		 */
		dndSetPositions: function () {
			var a_positions = [];
			var position = LinevenApoModule.Associations.positions.first;
			$('#table-lapo-associations-list>tbody tr.lapo-associations-list-item').each(function() {
				$(this).find('.lineven-helper-list-position').html(position);
				a_positions.push(LinevenApoModule.Associations.dndGetRowId($(this).attr('id')));
				position = position + 1;
			});
			 // Send
			$.ajax({
				url: LinevenApo.getActionUri(),
				type: 'post',
				data: {
					additionalproductsorder_controller: 'Associations',
					additionalproductsorder_action: 'order',
					positions: a_positions.join(','),
					first_position: LinevenApoModule.Associations.positions.first,
					last_position: LinevenApoModule.Associations.positions.last
				},
				success: function(data)
				{
				}
			});
		},
		
	},

    /**
     * Display.
     */
    Display: {
        /**
         * Prepare content.
         */
        prepareContent: function () {
			if ($('#hook_to_use').length) {
				LinevenApoModule.Display.onHookUsedChange();
				$('#hook_to_use').on('change', function (event) {
					LinevenApoModule.Display.onHookUsedChange();
				});
			}
			LinevenApoModule.Display.toggleSections();
			$('input[name="is_separate_display"]').on('change', function (event) {
				LinevenApoModule.Display.toggleSections();
			});
			if ($('#display_mode').length) {
                LinevenApoModule.Display.onTemplateChange();
                $('#display_mode').on('change', function (event) {
                    LinevenApoModule.Display.onTemplateChange();
                });
            }
			if ($('#display_cart_on').length && $('#display_cart_checkbox_on').length) {
				LinevenApoModule.Display.onAddToCartChange();
				$('input[name="display_cart"]').on('change', function (event) {
					LinevenApoModule.Display.onAddToCartChange();
				});
			}
            if ($('#refresh_method').length) {
                LinevenApoModule.Display.onRefreshMethodChange();
                $('#refresh_method').on('change', function (event) {
                    LinevenApoModule.Display.onRefreshMethodChange();
                });
            }
			LinevenApoModule.Display.onSortChange();
			$('#sort_display_method').on('change', function (event) {
				LinevenApoModule.Display.onSortChange();
			});

			// Hide confirm/error message
			LinevenApoModule.Display.hideSpecificNotifications();
        },

		/**
		 * Hide specific notification message.
		 */
		hideSpecificNotifications: function () {
			if ($('#hook_to_use').val() != 'SPECIFIC_MODULE') {
				$('div[id^="lineven-template-content-hooks-"] div[id^="fieldset_behavior_hook_settings"] .alert-warning').closest('.form-group').hide();
			}
			$('div[id^="lineven-template-content-hooks-"] div[id^="fieldset_behavior_hook_settings"] .alert-success').closest('.form-group').hide();
			$('div[id^="lineven-template-content-hooks-"] div[id^="fieldset_behavior_hook_settings"] .alert-danger').closest('.form-group').hide();
		},

		/**
		 * On hook used change.
		 */
		onHookUsedChange: function () {
			LinevenApoModule.Display.hideSpecificNotifications();
			var hook_to_use = $('#hook_to_use').val();
			if (hook_to_use == 'SPECIFIC_THEME') {
				$('#specific_hook_to_use').closest('.form-group').fadeIn();
			} else {
				$('#specific_hook_to_use').closest('.form-group').fadeOut();
				$('#specific_hook_to_use').val('');
				if (hook_to_use == 'SPECIFIC_MODULE') {
					$('div[id^="lineven-template-content-hooks-"] div[id^="fieldset_behavior_hook_settings"] .alert-warning').closest('.form-group').show();
				}
			}
		},

		/**
		 * Link to a specific hook
		 */
		linkToSpecificHook: function ()
		{
			LinevenApoModule.Display.hideSpecificNotifications();
			if ($('#specific_hook_to_use').val().trim() != '') {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: LinevenApo.getActionUri()+'&'+LinevenApo.module_name+'_controller=Settings&'+LinevenApo.module_name+'_action=linkToHook&no_layout',
					data: {
						hook_name: $('#specific_hook_to_use').val().trim(),

					},
					cache: false,
					success: function(jsonData, textStatus, jqXHR) {
						if (jsonData != undefined && jsonData.return == 1) {
							$('div[id^="lineven-template-content-hooks-"] .alert-success').closest('.form-group').show();
						} else {
							$('div[id^="lineven-template-content-hooks-"] .alert-danger').closest('.form-group').show();
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$('div[id^="lineven-template-content-hooks-"] .alert-danger').closest('.form-group').show();
					},
					complete: function(jqXHR, textStatus) {}
				});
			}
		},

		/**
		 * Toogle sections.
		 */
		toggleSections: function () {
			if ($('div[id^="fieldset_display_sections_"]').length) {
				if ($('#is_separate_display_on').length) {
					if ($('#is_separate_display_on').is(':checked')) {
						if ($('input[id^="section_title_"]').closest('.form-group').parent().closest('.form-group').length) {
							$('input[id^="section_title_"]').closest('.form-group').parent().closest('.form-group').fadeOut();
							$('input[id^="section_accessories_title_"]').closest('.form-group').parent().closest('.form-group').fadeIn();
							$('input[id^="section_associations_title_"]').closest('.form-group').parent().closest('.form-group').fadeIn();
							$('input[id^="section_purchased_together_title_"]').closest('.form-group').parent().closest('.form-group').fadeIn();
						} else {
							$('input[id^="section_title_"]').closest('.form-group').fadeOut();
							$('input[id^="section_accessories_title_"]').closest('.form-group').fadeIn();
							$('input[id^="section_associations_title_"]').closest('.form-group').fadeIn();
							$('input[id^="section_purchased_together_title_"]').closest('.form-group').fadeIn();
						}
					} else {
						if ($('input[id^="section_title_"]').closest('.form-group').parent().closest('.form-group').length) {
							$('input[id^="section_title_"]').closest('.form-group').parent().closest('.form-group').fadeIn();
							$('input[id^="section_accessories_title_"]').closest('.form-group').parent().closest('.form-group').fadeOut();
							$('input[id^="section_associations_title_"]').closest('.form-group').parent().closest('.form-group').fadeOut();
							$('input[id^="section_purchased_together_title_"]').closest('.form-group').parent().closest('.form-group').fadeOut();
						} else {
							$('input[id^="section_title_"]').closest('.form-group').fadeIn();
							$('input[id^="section_accessories_title_"]').closest('.form-group').fadeOut();
							$('input[id^="section_associations_title_"]').closest('.form-group').fadeOut();
							$('input[id^="section_purchased_together_title_"]').closest('.form-group').fadeOut();
						}
					}
				} else {
					if ($('input[id^="section_title_"]').closest('.form-group').parent().closest('.form-group').length) {
						$('input[id^="section_title_"]').closest('.form-group').parent().closest('.form-group').fadeIn();
						$('input[id^="section_accessories_title_"]').closest('.form-group').parent().closest('.form-group').fadeOut();
						$('input[id^="section_associations_title_"]').closest('.form-group').parent().closest('.form-group').fadeOut();
						$('input[id^="section_purchased_together_title_"]').closest('.form-group').parent().closest('.form-group').fadeOut();
					} else {
						$('input[id^="section_title_"]').closest('.form-group').fadeIn();
						$('input[id^="section_accessories_title_"]').closest('.form-group').fadeOut();
						$('input[id^="section_associations_title_"]').closest('.form-group').fadeOut();
						$('input[id^="section_purchased_together_title_"]').closest('.form-group').fadeOut();
					}
				}
			}
		},
		
        /**
         * On template change.
         */
        onTemplateChange: function () {
            var template_to_use = $('#display_mode').val();
            if (template_to_use == 'THUMBNAILS') {
                $('div[id^="fieldset_display_thumbnails_settings"]').fadeIn();
            } else {
                $('div[id^="fieldset_display_thumbnails_settings"]').fadeOut();
            }
			if (template_to_use == 'THEME' || template_to_use.indexOf('_theme', 0) > 0) {
				$('#image_type').closest('.form-group').fadeOut();
				$('#display_price_on').closest('.form-group').fadeOut();
				$('#display_cart_on').closest('.form-group').fadeOut();
				$('#display_new_product_on').closest('.form-group').fadeOut();
				$('#display_reduction_on').closest('.form-group').fadeOut();
			} else {
				$('#image_type').closest('.form-group').fadeIn();
				$('#display_price_on').closest('.form-group').fadeIn();
				$('#display_cart_on').closest('.form-group').fadeIn();
				$('#display_new_product_on').closest('.form-group').fadeIn();
				$('#display_reduction_on').closest('.form-group').fadeIn();
			}
        },

		/**
		 * On add to cart method change.
		 */
		onAddToCartChange: function () {
			if ($('#display_cart_on').length) {
				if ($('#display_cart_on').is(':checked')) {
					$('#display_cart_checkbox_on').closest('.form-group').fadeOut();
					$('#display_cart_checkbox_icon_on').closest('.form-group').fadeOut();
				} else {
					$('#display_cart_checkbox_on').closest('.form-group').fadeIn();
					$('#display_cart_checkbox_icon_on').closest('.form-group').fadeIn();
				}
			} else {
				$('#display_cart_checkbox_on').closest('.form-group').fadeOut();
				$('#display_cart_checkbox_icon_on').closest('.form-group').fadeOut();
			}
		},

        /**
         * On refresh method change.
         */
        onRefreshMethodChange: function () {
        	var refresh_method = $('#refresh_method').val();
            if (refresh_method != 'NOTHING') {
                $('#refresh_delay').closest('.form-group').fadeIn();
            } else {
                $('#refresh_delay').closest('.form-group').fadeOut();
            }
        },

		/**
		 * On sort change.
		 */
		onSortChange: function () {
			if ($('#sort_display_method').val() == 'NAME' || $('#sort_display_method').val() == 'PRICE') {
				$('#sort_display_way').closest('.form-group').fadeIn();
			} else {
				$('#sort_display_way').closest('.form-group').fadeOut();
			}
		}
	},

	/**
	 * Design.
	 */
	Design: {
	
		/**
		 * Prepare Design.
		 */
		prepare: function () {
			LinevenApoModule.Design.toggleMode();
			$('#active_default_design_on').change(function() {
				LinevenApoModule.Design.toggleMode();
			});
			$('#active_default_design_off').change(function() {
				LinevenApoModule.Design.toggleMode();
			});
			$('#design_display_mode').change(function() {
				LinevenApoModule.Design.load();
			});
			$('#reload_styles').click(function() {
				LinevenApoModule.Design.load(true);
			});
		},
		
		/**
		 * Toogle mode.
		 */
		toggleMode: function () {
			var useDefaultValue = false;
			if ($('#active_default_design_on').prop('checked') == true) {
				useDefaultValue = true;
			}
			if (useDefaultValue) {
				$('#design_display_mode').prop('disabled', true);
				$('#design_textarea').prop('disabled', true);
			} else {
				$('#design_display_mode').prop('disabled', false);
				$('#design_textarea').prop('disabled', false);
			}
			LinevenApoModule.Design.load(false);
		},
		
		/**
		 * Load design.
		 * @param boolean default_style Is default style
		 */
		load: function (default_style) {
			// Send
			$.ajax({
				url: LinevenApo.getActionUri(),
				type: 'POST',
				data: {
					additionalproductsorder_controller:'Tools',
					additionalproductsorder_action:'loadDesign',
					design_display_mode: $('#design_display_mode').val(),
					default_style: default_style
				},
				success: function(data)
				{
					$('#design_textarea').val(data);
				}
			});
		},
	},
	
	/**
	 * Statistics.
	 */
	Statistics: {
		
		/**
		 * Clear.
		 */
		clear: function () {
			$('#lapo-statistics-clear-confirmation-message').hide();
			$('#lapo-statistics-clear-error-message').hide();
			if (confirm($('#lapo-statistics-clear-alert-message').html())) {
				$.ajax({
		        	dataType: "json",
		        	url: LinevenApo.getActionUri(),
		            type: 'post',
		            data: {
		            	additionalproductsorder_controller: "Statistics",
		            	additionalproductsorder_action: "clear"
		 	        },
					success: function(jsonData, textStatus, jqXHR) {
						$("html, body").animate({ scrollTop: 0 }, "fast");
		            	$('#lapo-statistics-clear-confirmation-message').show();
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
		            	$('#lapo-statistics-clear-error-message').show();
					},
					complete: function(jqXHR, textStatus) {
					}
		        });
			}
		},
	}
	
};

$(document).ready(function() {
    if ($('div[id^="lineven-template-content-hooks-"]').length) {
        LinevenApoModule.Display.prepareContent();
    }
});