/**
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

var LinevenApo = {
    module_code: null,
	module_name: null,
	action_uri: null,
	is_debug_mode: null,
	is_test_mode: null,
    is_initialize: false,
    navigation_user_preferences: null,
	
	/**
	 * Initialize
	 * @param Object keywordParameters: 
	 * 		{module_name: string}
	 * 		{action_uri: string}
	 * 		{is_test_mode: int}
	 * 		{is_debut_mode: int}
	 */
	initialize: function(/* Object */ keywordParameters)
	{
        if("module_code" in keywordParameters) this.module_code = keywordParameters.module_code;
		if("module_name" in keywordParameters) this.module_name = keywordParameters.module_name;
		if("action_uri" in keywordParameters) this.action_uri = keywordParameters.action_uri;
		if("is_test_mode" in keywordParameters) this.is_test_mode = keywordParameters.is_test_mode;
		if("is_debug_mode" in keywordParameters) this.is_debug_mode = keywordParameters.is_debug_mode;

		LinevenApo.Debug.log('Initialization', 'Module initialization');
		if (LinevenApo.is_initialize == false) {
            LinevenApo.is_initialize = true;
            // keep navigation collapse in cookie
            LinevenApo.navigation_user_preferences = LinevenApo.Cookie.read('l'+LinevenApo.module_code.toLowerCase()+'_navigation_user_preferences');
            if (LinevenApo.navigation_user_preferences == null) {
                LinevenApo.navigation_user_preferences = [];
            } else {
                LinevenApo.navigation_user_preferences = LinevenApo.navigation_user_preferences.split('|');
            }
            document.addEventListener("DOMContentLoaded", function(event) {
                $('.list-group-collapse').each(function(index) {
                    if (LinevenApo.navigation_user_preferences.indexOf($(this).attr('attr-collapse-id')) != -1) {
                        $(this).addClass('collapsed');
                        $($(this).attr('data-target')).removeClass('in');
                    }
                });
                if ($('#lineven-template-navigation').length) {
                    $('#lineven-template-navigation a.list-group-item').on('click', function () {
                        if (!$(this).hasClass('collapsed')) {
                            LinevenApo.navigation_user_preferences.push($(this).attr('attr-collapse-id'));
                        } else {
                            if (LinevenApo.navigation_user_preferences.indexOf($(this).attr('attr-collapse-id')) != -1) {
                                LinevenApo.navigation_user_preferences.splice(LinevenApo.navigation_user_preferences.indexOf($(this).attr('attr-collapse-id')), 1);
                            }
                        }
                        LinevenApo.Cookie.set('l'+LinevenApo.module_code.toLowerCase()+'_navigation_user_preferences', LinevenApo.navigation_user_preferences.join('|'));
                    });
                }
            });
        }
	},

	/**
	 * Get Form action uri.
	 * @return string
	 */
	getActionUri: function ()
	{
        var action_uri = this.action_uri + "&mr="+Math.random();
        LinevenApo.Debug.log('Action called', action_uri);
		return action_uri;
	},

    /**
     * Diagnotic.
     */
    Diagnostic: {
        auto_repair: false,
        is_tables_must_backuped: false,

        /**
         * Launch.
         */
        launch: function () {
            LinevenApo.Debug.log('Diagnostic', 'Launch diagnostic');
            if ($('#lapo-diagnostic-progression-tables-backup').length) {
                LinevenApo.Diagnostic.is_tables_must_backuped = ($('#lapo-diagnostic-progression-tables-backup').prop('checked')&1);
            }
            LinevenApo.Diagnostic.reset();
            if (!LinevenApo.Diagnostic.auto_repair) {
                $("#lineven-diagnostic-progression").modal({
                    keyboard: true
                });
                $('#lineven-diagnostic-report .technical').html('');
                $('#lineven-diagnostic-report .functional').html('');
            } else {
                $('#lapo-diagnostic-progression-message-final').hide();
                $('#lapo-diagnostic-progression-message-begin').show();
                $('#lineven-diagnostic-report').html('');
                $('#lineven-diagnostic-progression #progression-first').show();
            }
            LinevenApo.Diagnostic.prepareChecking(0);
            $('#lineven-diagnostic-report').show();
        },

        /**
         * Reset diagnostic.
         */
        reset: function () {
            if (!LinevenApo.Diagnostic.auto_repair) {
                $('#lineven-diagnostic-progression').modal('hide');
            }
            $('#lineven-diagnostic-progression .text').html($('#lineven-diagnostic-progression .text').attr('data-init-label'));
            $('#lineven-diagnostic-progression .progress-bar').width("0%");
            $('#lineven-diagnostic-progression .progress-bar').attr('aria-valuenow', 0);
            $('#lineven-diagnostic-progression .progress-bar span').html('0');
        },

        /**
         * Progression.
         * @param float $percent Percent
         * @param string $target Target
         */
        progression: function (percent, target) {
            if (target == undefined) {
                target = 'first';
            }
            $('#lineven-diagnostic-progression .progress-bar-'+target).width(percent+'%');
            $('#lineven-diagnostic-progression .progress-bar-'+target).attr('aria-valuenow', percent);
            $('#lineven-diagnostic-progression .progress-bar-'+target+' span').html(percent);
        },

        /**
         * Log report.
         * @param string $report Report
         */
        logReport: function (report, target) {
            if (!LinevenApo.Diagnostic.auto_repair) {
                $('#lineven-diagnostic-report .'+target).append(report);
            } else {
                $('#lineven-diagnostic-report').append(report);
                if ($('#lineven-diagnostic-report').length > 0){
                    $('#lineven-diagnostic-report').scrollTop(1E10);
                }
            }
        },

        /**
         * Prepare checking
		 * @param int $step Step.
         */
        prepareChecking: function (step) {
            LinevenApo.Debug.log('Diagnostic', 'Prepare step ' + step);
            var is_tables_must_backuped = 0;
            $.ajax({
                url: LinevenApo.getActionUri(),
                type: 'post',
                dataType: "json",
                data: {
                    additionalproductsorder_controller: "Diagnostic",
                    additionalproductsorder_action: "prepareChecking",
                    step: step,
                    auto_repair: LinevenApo.Diagnostic.auto_repair|0,
                    is_tables_must_backuped: LinevenApo.Diagnostic.is_tables_must_backuped|0
                },
                cache: false,
                success: function (jsonData, textStatus, jqXHR) {
                    if (jsonData != undefined) {
                        $('#lineven-diagnostic-progression #progression-first .text').html(jsonData.progression_text);
                        setTimeout(function() { LinevenApo.Diagnostic.checking(step); }, 1000);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    LinevenApo.Debug.log('Diagnostic prepare', 'Error ' + textStatus);
                },
                complete: function (jqXHR, textStatus) {
                }
            });
        },

        /**
         * Checking.
         * @param int $step Step.
         */
        checking: function (step) {
            LinevenApo.Debug.log('Diagnostic', 'Execute step ' + step);
            $.ajax({
                url: LinevenApo.getActionUri(),
                type: 'post',
                dataType: "json",
                data: {
                    additionalproductsorder_controller: "Diagnostic",
                    additionalproductsorder_action: "checking",
                    step: step,
                    auto_repair: LinevenApo.Diagnostic.auto_repair|0,
                    is_tables_must_backuped: LinevenApo.Diagnostic.is_tables_must_backuped|0
                },
                cache: false,
                success: function (jsonData, textStatus, jqXHR) {
                    if (jsonData != undefined) {
                        LinevenApo.Diagnostic.logReport(jsonData.html, jsonData.target);
                        LinevenApo.Diagnostic.progression(jsonData.progress_percent);
                        if (jsonData.has_output && LinevenApo.Diagnostic.auto_repair && (jsonData.step_definitions.operation == 'action' || (jsonData.step_definitions.operation == 'repair' && jsonData.step_definitions.repair.is_automatic == true))) {
                            setTimeout(function () {
                                LinevenApo.Diagnostic.execute(jsonData.what, false, jsonData.next_step);
                            }, 1000);
                        } else {
                            if (jsonData.next_step != -1) {
                                setTimeout(function () {
                                    LinevenApo.Diagnostic.prepareChecking(jsonData.next_step);
                                }, 1000);
                            } else {
                                setTimeout(function () {
                                    if (LinevenApo.Diagnostic.auto_repair) {
                                        LinevenApo.Diagnostic.endChecking(true);
                                    } else {
                                        LinevenApo.Diagnostic.endChecking();
                                    }
                                }, 2000);
                            }
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    LinevenApo.Debug.log('Diagnostic', 'Error ' + textStatus);
                },
                complete: function (jqXHR, textStatus) {
                }
            });
        },

        /**
         * Operation.
		 * @param string what What to repair
         * @param boolean reload Reload page
         * @param int next_step Next step
         */
        execute: function (what, reload, next_step) {
            LinevenApo.Debug.log('Diagnostic', 'Execute action ' + alert);
            $.ajax({
                url: LinevenApo.getActionUri(),
                type: 'post',
                dataType: "json",
                data: {
                    additionalproductsorder_controller: 'Diagnostic',
                    additionalproductsorder_action: 'execute',
                    what: what,
                    auto_repair: LinevenApo.Diagnostic.auto_repair|0,
                    is_tables_must_backuped: LinevenApo.Diagnostic.is_tables_must_backuped|0
                },
                cache: false,
                success: function (jsonData, textStatus, jqXHR) {
                    if (jsonData != undefined) {
                        var target = '#lineven-diagnostic-report .'+jsonData.target+' .'+what;
                        if ($(target).length) {
                            $(target+' .repair_results').html(jsonData.html);
                            $(target+' .message').fadeOut('slow');
                            $(target+' .actions').fadeOut('slow');
                            $(target+' .repair_results').fadeIn('slow');
                        } else {
                            var target = '#lineven-diagnostic-report .'+what;
                            $(target+' .repair_results').html(jsonData.html);
                            $(target+' .actions').fadeOut('slow');
                            $(target+' .repair_results').fadeIn('slow');
                        }
                    }
                    if (reload != undefined && reload == true) {
                        window.location.reload();
					}
                    if (next_step != undefined) {
                        if (next_step != -1 && (jsonData.is_success || (!jsonData.is_success && !jsonData.is_success_required))) {
                            setTimeout(function () {
                                LinevenApo.Diagnostic.prepareChecking(next_step);
                            }, 1000);
                        } else {
                            if (jsonData.is_success || (!jsonData.is_success && !jsonData.is_success_required)) {
                                LinevenApo.Diagnostic.endChecking(true);
                            } else {
                                LinevenApo.Diagnostic.endChecking();
                            }
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    LinevenApo.Debug.log('Diagnostic repair', 'Error ' + textStatus);
                },
                complete: function (jqXHR, textStatus) {
                }
            });
        },

        /**
         * End checking.
         */
        endChecking: function(execute_final) {
            setTimeout(function () {
                if (LinevenApo.Diagnostic.auto_repair) {
                    if (execute_final != undefined && execute_final == true) {
                        LinevenApo.Debug.log('Diagnostic', 'Final action ' + alert);
                        $.ajax({
                            url: LinevenApo.getActionUri(),
                            type: 'post',
                            dataType: "json",
                            data: {
                                additionalproductsorder_controller: 'Diagnostic',
                                additionalproductsorder_action: 'final',
                            },
                            cache: false,
                            success: function (jsonData, textStatus, jqXHR) {
                                $('#lapo-diagnostic-progression-message-begin').hide();
                                $('#lapo-diagnostic-progression-message-final').show();
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                LinevenApo.Debug.log('Diagnostic final', 'Error ' + textStatus);
                            },
                            complete: function (jqXHR, textStatus) {
                            }
                        });
                    }
                    $('#lineven-diagnostic-report').scrollTop(1E10);
                    $('#lineven-diagnostic-progression #progression-first').hide();
                    $('#lineven-diagnostic-progression #progression-second').hide();
                    $('#lineven-diagnostic-progression-button-close').removeClass('disabled');
                }
                LinevenApo.Diagnostic.reset();
            }, 2000);
        }
    },

    /**
	 * Upgrade
     */
	Upgrade: {
		is_upgrades: false,
        upgrades_count: 0,
        upgrades_step_progress: 0,
        upgrades_progress: 0,

        /**
         * Progression.
		 * @param string $target Target
         * @param float $percent Percent
         */
        progression: function (target, percent) {
            $('#lineven-extra_upgrade-progression .'+target+' .progress-bar').width(percent+'%');
            $('#lineven-extra_upgrade-progression .'+target+' .progress-bar').attr('aria-valuenow', percent);
            $('#lineven-extra_upgrade-progression .'+target+' .progress-bar span').html(percent);
        },

        /**
         * Initialize upgrade
         */
        initialize: function () {
            $('#lineven-extra_upgrade-progression .close-button').hide();
            $('#lineven-extra_upgrade-progression .start-button').hide();
            $('#lineven-extra_upgrade-progression .upgrade-version').show();
            $('#lineven-extra_upgrade-progression .upgrade-version-content').show();
            LinevenApo.Upgrade.progression('upgrade-version', 0);
            LinevenApo.Upgrade.upgradeModule();
        },

        /**
         * Upgrade module
         */
        upgradeModule: function () {
            LinevenApo.Debug.log('Upgrade', 'Launch ');
            $.ajax({
                url: LinevenApo.getActionUri(),
                type: 'post',
                dataType: "json",
                data: {
                    additionalproductsorder_controller: "Upgrade",
                    additionalproductsorder_action: "upgradeModule",
                },
                cache: false,
                success: function (jsonData, textStatus, jqXHR) {
                    if (jsonData != undefined && jsonData.is_upgrades != false) {
                        LinevenApo.Upgrade.is_upgrades = true;
                        LinevenApo.Upgrade.upgrades_count = jsonData.upgrades_count;
                        LinevenApo.Upgrade.upgrades_step_progress = jsonData.upgrades_step_progress;
                        $('#lineven-extra_upgrade-progression .text').show();
                        $('#lineven-extra_upgrade-progression .text-version').html(jsonData.version);
                        LinevenApo.Upgrade.progression('upgrade-version', 0);
                        setTimeout(function() { LinevenApo.Upgrade.upgradeVersion(jsonData.version, 0); }, 500);
                    } else {
                        $('#lineven-extra_upgrade-progression .upgrade-version').hide();
                        $('#lineven-extra_upgrade-progression .upgrade-version-content').hide();
                        $('#lineven-extra_upgrade-progression .upgrade-version-complete').show();
                        $('#lineven-extra_upgrade-progression .close-button').show();
					}
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    LinevenApo.Debug.log('Upgrade launch', 'Error ' + textStatus);
                },
                complete: function (jqXHR, textStatus) {
                }
            });
        },

        /**
         * Upgrade version.
         * @param string $version Version.
         */
        upgradeVersion: function (version, version_step, version_datas) {
            LinevenApo.Debug.log('Upgrade', 'version ' + version);
            $.ajax({
                url: LinevenApo.getActionUri(),
                type: 'post',
                dataType: "json",
                data: {
                    additionalproductsorder_controller: "Upgrade",
                    additionalproductsorder_action: "upgradeVersion",
					version: version,
                    version_step: version_step,
                    version_datas: version_datas
                },
                cache: false,
                success: function (jsonData, textStatus, jqXHR) {
                    if (jsonData != undefined) {
                        LinevenApo.Upgrade.progression('upgrade-version-content', jsonData.version_upgrade_percent);
                        if (jsonData.version_text_step != undefined) {
                            $('#lineven-extra_upgrade-progression .upgrade-version-content .text').html(jsonData.version_text_step);
                        }
                        if (jsonData.is_complete == false) {
                            setTimeout(function () {
                                LinevenApo.Upgrade.upgradeVersion(version, jsonData.version_next_step, jsonData.version_datas);
                            }, 100);
                        } else {
                            LinevenApo.Upgrade.upgrades_progress = LinevenApo.Upgrade.upgrades_progress + LinevenApo.Upgrade.upgrades_step_progress
                            LinevenApo.Upgrade.progression('upgrade-version', jsonData.version_upgrade_percent);
                            setTimeout(function () {
                                LinevenApo.Upgrade.upgradeModule();
                            }, 500);
                        }
                    } else {
                        $('#lineven-extra_upgrade-progression').modal('hide');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    LinevenApo.Debug.log('Diagnostic prepare', 'Error ' + textStatus);
                },
                complete: function (jqXHR, textStatus) {
                }
            });
        }
	},

	/**
	 * Tools.
	 */
	Tools: {

		/**
		 * Install demonstration datas.
		 */
		installDemonstrationDatas: function () {
			$("#lineven-popup-demontration_datas_pending").modal({
				backdrop: 'static', keyboard: false  
        	});
			// Send
			$.ajax({
				url: LinevenApo.getActionUri(),
				type: 'post',
				dataType: "json",
				data: {
					additionalproductsorder_controller: "Dashboard",
					additionalproductsorder_action: "demonstrationDatasInstall"
				},
				cache: false,
				success: function(jsonData, textStatus, jqXHR) {
					$('#lineven-popup-demontration_datas_pending').modal('hide');
					$("#lineven-template-content-dashboard-panel_demo_datas").hide();
					$("#lineven-popup-demontration_datas_finished").modal();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					LinevenApo.Debug.log('Demonstration data install', 'Error ' + textStatus);
				},
				complete: function(jqXHR, textStatus) {
				}
			});
		},

        /**
         * Reset demos datas.
         */
        resetDemonstrationDatas: function () {
            $("#lineven-popup-demontration_datas_reset_pending").modal({
                backdrop: 'static', keyboard: false
            });
            // Send
            setTimeout(function() {
                $.ajax({
                    url: LinevenApo.getActionUri(),
                    type: 'post',
                    dataType: "json",
                    data: {
                        additionalproductsorder_controller: "Dashboard",
                        additionalproductsorder_action: "demonstrationDatasReset"
                    },
                    cache: false,
                    success: function(jsonData, textStatus, jqXHR) {
                        setTimeout(function() {
							$('#lineven-popup-demontration_datas_reset_pending').modal('hide');
							$("#lineven-popup-demontration_datas_reset_finished").modal();
                        }, 2000);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        LinevenApo.Debug.log('Demonstration datas reset', 'Error ' + textStatus);
                    },
                    complete: function(jqXHR, textStatus) {
                    }
                });
			}, 2000);
        },

		/**
		 * Toggle status.
		 * @param string link Link
		 */
		toggleStatus: function (link) {
			LinevenApo.Debug.log('Toggle status', 'Called link ' + link);
			// Send
			$.ajax({
				url: LinevenApo.getActionUri(),
				type: 'post',
				dataType: "json",
				data: {
					additionalproductsorder_controller: "Dashboard",
					additionalproductsorder_action: "toggleStatus",
					link: link
				},
				cache: false,
				success: function(jsonData, textStatus, jqXHR) {
					if (jsonData != undefined && jsonData.reload != undefined && jsonData.reload) {
						LinevenApo.Debug.log('Toggle status', 'Result ' + jsonData);
						var url = LinevenApo.getActionUri();    
						if (url.indexOf('?') > -1){
						   url += '&';
						}else{
						   url += '?';
						}
						url += 'additionalproductsorder_controller=Dashboard&additionalproductsorder_action=index&dashboard_toggle_link='+jsonData.link+'&dashboard_toggle_value='+jsonData.value;
						$(location).attr('href', url);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					LinevenApo.Debug.log('Toggle status', 'Error ' + textStatus);
				},
				complete: function(jqXHR, textStatus) {
				}
			});
		},
		
		/**
		 * Close panel.
		 * @param string panel_code Panel code
		 */
		closePanel: function (panel_code, configuration_name) {
			LinevenApo.Debug.log('Call close panel', panel_code);
			if (configuration_name == undefined) {
				configuration_name = '';
			}
			if (configuration_name != '')  {
				// Send
				$.ajax({
					url: LinevenApo.getActionUri(),
					type: 'post',
					data: {
						additionalproductsorder_controller: "Dashboard",
						additionalproductsorder_action: "closePanel",
						panel_code: panel_code,
						configuration_name: configuration_name
					},
					success: function(data)
					{
						LinevenApo.Debug.log('Closed panel', panel_code);
						$('#lineven-template-content-dashboard-' + panel_code).css('display', 'none');
					}
				});
			} else {
				$('#lineven-template-content-dashboard-' + panel_code).css('display', 'none');
			}
		},
		
		/**
		 * Scroll to the top of the page.
		 */
		scrollToTopPage: function () {
			$('html, body').animate({ scrollTop: 0 }, 'slow');
		},
		
		/**
		 * Get IP Address.
		 * @param string $ip IP Address
		 */
		getIPAddress: function (ip) {
			var value = $('#testmode_ip').attr('value');
			if (value != "") {
				value = value + "," + ip;
			} else {
				value = ip;
			}
			$('#testmode_ip').attr('value', value);
		},
		
		/**
		 * Trim a string.
		 * @param string value Value to trim
		 * @return string
		 */
		trim: function(value) { 
			return value.replace(/^\s+|\s+$/g, '');
		}
	},

    /**
	 * Debug.
	 */
	Debug: {
		/**
		 * Trim a string.
		 * @param string value Value to trim
		 * @return string
		 */
		log: function(title, message) {
			if (LinevenApo.is_debug_mode)
			{
				console.log(LinevenApo.module_name + ' - ' + title + ' : ' + message);
			}
		}
	},
	
	/**
	 * Cookie.
	 */
	Cookie: {
		
		/**
		 * Set a cookie.
		 * @param string name Name
		 * @param string value Value
		 * @param string options Options
		 */
		set: function(name, value, options) {
			document.cookie = name + '=' + value + ';'+options;
		},
		
		/**
		 * Read cookie.
		 * @param name Name
		 * @return string
		 */
		read: function(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
		}
	}
};