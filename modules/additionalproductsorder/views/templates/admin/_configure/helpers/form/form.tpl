{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file="helpers/form/form.tpl"}

{block name="other_input"}
    {if $key == 'description_multilines'}
		{if is_array($field) && count($field)}
			<div class="alert alert-info">
				{foreach $field as $sentence}
					{$sentence nofilter}<br/>
				{/foreach}
			</div>
		{else}
			{if $field != ''}
				<div class="alert alert-info">{$field nofilter}</div>
			{/if}
		{/if}
    {else}
        {$smarty.block.parent}
    {/if}
{/block}

{block name="field"}
	{if $input.type == 'group' && isset($input.specific) && $input.specific}
		{assign var=groups value=$input.values}
		{include file='./form_group_specific.tpl'}
	{else}
		{if $input.type == 'html' && isset($input.specific) && $input.specific && isset($input.alert_type) && isset($input.html_content)}
			{if $input.specific == 'alert'}
				<div class="col-lg-9 col-lg-offset-3">
					<div class="alert alert-{$input.alert_type|escape:'html':'UTF-8'}">
                        {if is_array($input.html_content) && count($input.html_content)}
                            {foreach $input.html_content as $sentence}
                                {* HTML CONTENT *}
                                {$sentence nofilter}<br/>
                                {* /HTML CONTENT *}
                            {/foreach}
                        {else}
                            {if $input.html_content != ''}
                                {* HTML CONTENT *}
								{$input.html_content nofilter}
                                {* /HTML CONTENT *}
                            {/if}
                        {/if}
					</div>
				</div>
			{else}
				{$smarty.block.parent}
			{/if}
		{else}
			{$smarty.block.parent}
		{/if}
    {/if}
{/block}
