{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if count($groups) && isset($groups)}
<div class="row">
	<div class="col-lg-6">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="fixed-width-xs">
						<span class="title_box">
							<input type="checkbox" name="checkme" id="checkme" onclick="checkDelBoxes(this.form, '{$input.name|escape:'html':'UTF-8'}[]', this.checked)" />
						</span>
					</th>
					<th>
						<span class="title_box">
							{if isset($input.subtype) && isset($input.title_box)}
								{$input.title_box|escape:'html':'UTF-8'}
							{else}
								{l s='Values' mod='additionalproductsorder'}
							{/if}
						</span>
					</th>
				</tr>
			</thead>
			<tbody>
			{foreach $groups as $key => $group}
				<tr>
					<td>
						{assign var=id_checkbox value=$input.name|cat:'_'|cat:$group['id_group']}
						<input type="checkbox" name="{$input.name|escape:'html':'UTF-8'}[]" class="groupBox" id="{$id_checkbox|escape:'html':'UTF-8'}" value="{$group['val']|escape:'html':'UTF-8'}" {if isset($fields_value[$id_checkbox]) && $fields_value[$id_checkbox]}checked="checked"{/if} />
					</td>
					<td>
						<label for="{$id_checkbox|escape:'html':'UTF-8'}">{$group['name']|escape:'html':'UTF-8'}</label>
					</td>
				</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
</div>
{else}
<p>
    {if isset($input.subtype) && isset($input.no_value_message)}
		{$input.no_value_message|escape:'html':'UTF-8'}
	{else}
		{l s='No value created' mod='additionalproductsorder'}
	{/if}
</p>
{/if}