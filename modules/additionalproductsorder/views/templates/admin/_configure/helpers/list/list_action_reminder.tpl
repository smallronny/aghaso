{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $datas.report_status == LinevenApoMonitoring::$report_status_mail_ko || $datas.report_status == LinevenApoMonitoring::$report_status_customer_review_pending || $datas.report_status == LinevenApoMonitoring::$report_status_mail_remind}
	<a href="{$href|escape:'html':'UTF-8'}" class="btn btn-warning" title="{$action|escape:'html':'UTF-8'}" >
		<i class="fa fa-refresh"></i> {$action|escape:'html':'UTF-8'}
	</a>
{/if}