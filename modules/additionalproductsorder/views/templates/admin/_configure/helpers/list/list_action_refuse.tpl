{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<a href="{if !$is_reason_of_refusal}{$href|escape:'html':'UTF-8'}{else}#{/if}" class="" title="{$action|escape:'html':'UTF-8'}" >
	<i class="fa fa-thumbs-o-down"></i> {$action|escape:'html':'UTF-8'}
</a>