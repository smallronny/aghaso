{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file="helpers/list/list_header.tpl"}

{block name="preTable"}
	{if isset($show_filters_fixed)}
		{assign var=show_filters value=$show_filters_fixed}
	{/if}
	{if (!isset($table_id) || (isset($table_id) && $table_id == '')) && isset($table_id_fixed) && $table_id_fixed != ""}
		{assign var=table_id value=$table_id_fixed}
	{/if}
    {if isset($subtitle_informations_message)}
        {if is_array($subtitle_informations_message)}
			<span class="list-description">
				{foreach $subtitle_informations_message as $sentence}
                    {$sentence nofilter}<br/>
                {/foreach}
			</span>
        {else}
            {if $subtitle_informations_message != ''}
				<span class="list-description">{$subtitle_informations_message nofilter}</span>
            {/if}
        {/if}
    {/if}
	{$smarty.block.parent}
{/block}