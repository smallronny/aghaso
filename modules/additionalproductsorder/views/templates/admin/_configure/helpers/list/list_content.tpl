{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file="helpers/list/list_content.tpl"}

{block name="td_content"}
	{if !isset($params.render) || (isset($params.render) && ($params.render != 'fa_icons' && $params.render != 'html'))}
		{if $params.type == 'select' && isset($params.field_value)}
			{$tr[$params.field_value]|escape:'html':'UTF-8'}
		{else}
			{$smarty.block.parent}
		{/if}
	{else}
		{if isset($params.render) && $params.render == 'fa_icons'}
			{if ($tr.$key|is_array) && ($tr.$key.items|is_array)}
				<span class="{if isset($tr.$key.class)}{$tr.$key.class|escape:'html':'UTF-8'}{/if}">
				{foreach from=$tr.$key.items item="item"}
  					<i class="{$item|escape:'html':'UTF-8'}"></i>
  				{/foreach} 
				</span>&nbsp;
			{/if}
		{/if}
	{/if}
{/block}
{block name="default_field"}
	{if isset($params.render) && $params.render == 'specific'}
		{$tr.$key|escape:'html':'UTF-8'}
	{else}
		{$smarty.block.parent}
	{/if}
{/block}