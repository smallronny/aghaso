{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file="./../../_configure/helpers/form/form.tpl"}

{block name="defaultForm"}
	{$smarty.block.parent}
	{if !$associated_modules_reviews_verify_lineven}
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-comments"></i>
				{l s='Go Reviews - Reviews, Advices, Ratings, SEO and Google Rich Snippets Module' mod='additionalproductsorder'}
			</div>
			<div class="alert alert-info">{l s='To display average score for each products displayed, you have the possibility to associate "Go Reviews - Reviews, Advices, Ratings, SEO and Google Rich Snippets" to this module.' mod='additionalproductsorder'}</div>
			<div>
				{l s='To display average score for each products displayed, you have the possibility to associate the module' mod='additionalproductsorder'}
				<u>
					<a href="http://addons.prestashop.com/{$language_iso_code|escape:'html':'UTF-8'}/product.php?id_product=4999'" target="_blank">
						{l s='Go Reviews - Reviews, Advices, Ratings, SEO and Google Rich Snippets Module' mod='additionalproductsorder'}
					</a>
				</u>.
        </div>
		</div>
	{/if}
{/block}
