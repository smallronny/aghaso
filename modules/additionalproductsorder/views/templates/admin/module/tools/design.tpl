{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file="./../../_configure/helpers/form/form.tpl"}

{block name="defaultForm"}
	{$smarty.block.parent}
	<script>
		LinevenApoModule.Design.prepare();
	</script>
{/block}
