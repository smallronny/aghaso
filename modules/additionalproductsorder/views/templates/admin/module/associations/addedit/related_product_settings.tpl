{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="panel" id="fieldset_related_product">
	<div class="panel-heading">
		<i class="fa fa-book"></i>
		{l s='Selection rules' mod='additionalproductsorder'}
	</div>
	<div class="alert alert-info">{l s='Choose the product added in cart by the customer to trigger the association.' mod='additionalproductsorder'}</div>
		<div id="ajax_choose_product">
			<div class="input-group">
				<input type="text" id="related_product_content_input" name="related_product_content_input" />
				<span class="input-group-addon"><i class="icon-search"></i></span>
			</div>
		</div>
		<div class="form-group">
			<table id="related-product-preview-table" class="table configuration">
				<thead>
					<tr class="nodrag nodrop">
						<th class="center">&nbsp;</th>
						<th class="left">{l s='Name' mod='additionalproductsorder'}</th>
						<th class="left">{l s='Description' mod='additionalproductsorder'}</th>
						<th class="center">{l s='Remove' mod='additionalproductsorder'}</th>
					</tr>
				</thead>
				<tbody>
					{include file="./related_product_details.tpl"}
				</tbody>
			</table>
		</div>
	</div>
 