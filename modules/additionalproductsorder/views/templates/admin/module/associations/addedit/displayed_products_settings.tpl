{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="panel" id="fieldset_displayed_products">
	<div class="panel-heading">
		<i class="fa fa-book"></i>
		{l s='Product(s) to display' mod='additionalproductsorder'}
	</div>
	<div class="alert alert-info">{l s='Choose product(s) to display.' mod='additionalproductsorder'}</div>
		<div id="ajax_choose_product">
			<div class="input-group">
				<input type="text" id="display_product_content_input" name="display_product_content_input" />
				<span class="input-group-addon"><i class="icon-search"></i></span>
			</div>
		</div>
		<div class="form-group">
			<table id="displayed-products-preview-table" class="table configuration">
				<thead>
					<tr class="nodrag nodrop">
						<th class="center">&nbsp;</th>
						<th class="left">{l s='Name' mod='additionalproductsorder'}</th>
						<th class="left">{l s='Description' mod='additionalproductsorder'}</th>
						<th class="center">{l s='Remove' mod='additionalproductsorder'}</th>
					</tr>
				</thead>
				<tbody>
					{assign var=displayed_products_list value=null}
					{if isset($smarty.post.displayed_products_list)}
						{assign var=displayed_products_list value=LinevenApoAssociation::getProductsListDetails($smarty.post.displayed_products_list)}
					{else}
						{if is_array($association->displayed_products) && count($association->displayed_products)}
							{assign var=displayed_products_list value=$association->displayed_products}
						{/if}
					{/if}
					{if $displayed_products_list != null}
						{foreach from=$displayed_products_list item=displayed_products}
							{include file="./displayed_products_details.tpl"}
						{/foreach}
					{/if}
				</tbody>
			</table>
		</div>
	</div>
 