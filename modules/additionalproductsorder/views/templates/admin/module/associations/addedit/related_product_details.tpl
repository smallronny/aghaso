{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if isset($related_product) && $related_product}
    {assign var=imageCoverId value=Product::getCover($related_product.id_product)}
    <tr>
        <td class="center">
            {LinevenApoProductPresenter::getThumbnailImage($related_product.id_product, $imageCoverId.id_image) nofilter}
        </td>
        <td class="left">
            <a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&updateproduct&id_product={$related_product.id_product|intval}" target="_blank" title="{l s='Edit product' mod='additionalproductsorder'}"><strong>{$related_product.name|escape:'html':'UTF-8'}</strong></a><br />
            <em>{l s='Ref:' mod='additionalproductsorder'} {$related_product.reference|escape:'html':'UTF-8'}</em>
        </td>
        <td class="left">
            {$related_product.description_short|strip_tags|escape:'html':'UTF-8'|truncate:80:'...'}
        </td>
        <td class="center">
            <button class="remove-related-product btn btn-default" type="button"
                data-related-product-id="{$related_product.id_product|intval}"
                onClick="LinevenApoModule.Associations.removeRelatedProduct({$related_product.id_product|intval});">
                <i class="icon-trash"></i>
            </button>
        </td>
    </tr>
{/if}