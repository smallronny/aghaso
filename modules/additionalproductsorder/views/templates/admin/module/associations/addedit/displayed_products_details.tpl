{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if isset($displayed_products) && $displayed_products}
    {assign var=imageCoverId value=Product::getCover($displayed_products.id_product)}
    <tr id="displayed-product-{$displayed_products.id_product|intval}">
        <td class="center">
            {LinevenApoProductPresenter::getThumbnailImage($displayed_products.id_product, $imageCoverId.id_image) nofilter}
        </td>
        <td class="left">
            <a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&updateproduct&id_product={$displayed_products.id_product|intval}" target="_blank" title="{l s='Edit product' mod='additionalproductsorder'}"><strong>{$displayed_products.name|escape:'html':'UTF-8'}</strong></a><br />
            <em>{l s='Ref:' mod='additionalproductsorder'} {$displayed_products.reference|escape:'html':'UTF-8'}</em>
            <input name="displayed_products_list[]" value="{$displayed_products.id_product|intval}" type="hidden"/>
        </td>
        <td class="left">
            {$displayed_products.description_short|strip_tags|escape:'html':'UTF-8'|truncate:80:'...'}
        </td>
        <td class="center">
            <button class="remove-displayed-product btn btn-default" type="button"
                data-displayed-product-id="{$displayed_products.id_product|intval}"
                onClick="LinevenApoModule.Associations.removeDisplayedProduct({$displayed_products.id_product|intval})">
                <i class="icon-trash"></i>
            </button>
        </td>
    </tr>
{/if}