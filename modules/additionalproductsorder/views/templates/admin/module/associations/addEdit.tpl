{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}
 
{extends file="./../../_configure/helpers/form/form.tpl"}

{block name="defaultForm"}
	<div class="panel">
		<div class="panel-heading">
			<i class="fa fa-edit"></i>
			{if $association->id != null && $association->id != 0}
				{l s='Edit association' mod='additionalproductsorder'}
				<span class="badge">{l s='N' mod='additionalproductsorder'}&deg;{$association->id|intval}</span> 
			{else}
				{l s='Create an association' mod='additionalproductsorder'}
			{/if}
			<div class="panel-heading-action">
				<a class="list-toolbar-btn" href="{$cancel_url|escape:'html':'UTF-8'}">
					<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Cancel' mod='additionalproductsorder'}" data-html="true">
						<i class="process-icon-cancel "></i>
					</span>
				</a>
			</div>
		</div>
		{$smarty.block.parent}
		<div class="panel-footer">
			<button type="button" value="1" id="association_addedit_save" class="btn btn-default pull-left" name="linevenCancelAssociation" onClick="LinevenApoModule.Associations.doOnClickAssociationCancel()">
				<i class="process-icon-cancel"></i> {l s='Cancel' mod='additionalproductsorder'}
			</button>
			<button type="button" value="1" id="association_addedit_save" class="btn btn-default pull-right" name="linevenSubmitAssociation" onClick="LinevenApoModule.Associations.doOnClickAssociationSave({$association->id|intval}, false)">
				<i class="process-icon-save"></i> {l s='Save' mod='additionalproductsorder'}
			</button>
			<button type="button" value="1" id="association_addedit_save_stay" class="btn btn-default pull-right" name="linevenSubmitSaveStayAssociation" onClick="LinevenApoModule.Associations.doOnClickAssociationSave({$association->id|intval}, true)">
				<i class="process-icon-save"></i> {l s='Save and stay' mod='additionalproductsorder'}
			</button>
		</div>
	</div>
	<script>
		LinevenApoModule.Associations.prepareContentAddEditAssociation();
		{literal}
			$(".chosen").chosen({width: "280px"});
		{/literal}
	</script>
{/block}

{block name="fieldset"}
	{$smarty.block.parent}
	{if $f == 'related_category'}
		{include file="./addedit/related_product_settings.tpl"}
		{include file="./addedit/displayed_products_settings.tpl"}
	{/if}
{/block}
