{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<script>
	LinevenApoModule.Associations.prepareContentListAssociations({$lineven.apo.admin.datas.authorized_order|intval});
</script>
{if $lineven.apo.admin.datas.authorized_order != 1}
	<div class="alert alert-info">
		{l s='To reorder items in this list, you need to' mod='additionalproductsorder'} :
		<br/> - {l s='select "All Shops" in the combo box.' mod='additionalproductsorder'}	
		<br/> - {l s='order the list by ascending position.' mod='additionalproductsorder'}	
		<br/> - {l s='have no effective filters.' mod='additionalproductsorder'}
		<br/><br/>
		<strong>{l s='Please' mod='additionalproductsorder'}
		{if $lineven.apo.admin.datas.authorized_order == 'SHOP'}
			{l s='select "All Shops" in the combo box.' mod='additionalproductsorder'}
		{/if}
		{if $lineven.apo.admin.datas.authorized_order == 'ORDER'}
			{l s='reorder the list by ascending position.' mod='additionalproductsorder'}
		{/if}
		{if $lineven.apo.admin.datas.authorized_order == 'FILTERS'}
			{l s='reset your filters.' mod='additionalproductsorder'}
		{/if}
		</strong>
	</div>
{/if}