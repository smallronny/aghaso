{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div id="lineven-notification-demonstration" class="lineven-dashboard-notification modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-thumbs-o-up fa-fw"></i> {l s='To test this module' mod='additionalproductsorder'}</h4>
            </div>
            <div class="modal-body">
                <div class="subtitle">
                    {l s='What you need to know to test this module :' mod='additionalproductsorder'}</span>
                </div>
                <div class="subtitle">
                    {l s='Any questions ?' mod='additionalproductsorder'}
                </div>
                <div class="message">
                    {l s='You can contact our support via the platform PrestaShop Addons.' mod='additionalproductsorder'}
                </div>
                <div class="signature">
                    {l s='The Lineven Team.' mod='additionalproductsorder'}
                </div>
                <br style="clear:both;"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='additionalproductsorder'}</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("#lineven-notification-demonstration").modal({
        keyboard: true
    })
</script>
