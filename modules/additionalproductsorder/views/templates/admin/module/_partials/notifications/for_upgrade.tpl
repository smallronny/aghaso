{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $lineven.apo.admin.datas.notifications.for_upgrade && !$lineven.apo.admin.datas.notifications.for_extra_upgrade}
    <div id="lineven-notification-upgrade" class="lineven-dashboard-notification modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-refresh fa-fw"></i> {l s='This module has been upgrated to version' mod='additionalproductsorder'}&nbsp;{$lineven.apo.version|escape:'html':'UTF-8'}</h4>
                </div>
                <div class="modal-body">
                    <div class="module_box">
                        <img src="{$lineven.apo.urls.base_url|escape:'html':'UTF-8'}views/img/box/box_{$lineven.apo.admin.datas.notifications.image_box_language|escape:'html':'UTF-8'}.png" style="" />
                    </div>
                    <div class="subtitle">
                        {l s='Discover what\'s new in this version.' mod='additionalproductsorder'}
                    </div>
                    <div class="message">
                        {* HTML CONTENT *}
                        {$lineven.apo.admin.datas.notifications.upgrade_content nofilter}
                        {* /HTML CONTENT *}
                    </div>
                    <div class="message">
                        {l s='If you have some troubles or difficulties to use this module you can contact our support via the platform PrestaShop Addons.' mod='additionalproductsorder'}
                    </div>
                    <div class="signature">
                        {l s='The Lineven Team.' mod='additionalproductsorder'}
                    </div>
                    <br style="clear:both;"/>
                </div>
                <div class="modal-footer">
                    <div class="notation">
                        <a href="{$lineven.apo.admin.datas.prestashop_addons_link|escape:'html':'UTF-8'}" target="_blank"><img src="{$lineven.apo.urls.base_url|escape:'html':'UTF-8'}/views/img/notation.png" /><span>{l s='Thank you to vote for this module on the Addons Prestashop.' mod='additionalproductsorder'}</span></a>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='additionalproductsorder'}</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        {literal}
            $("#lineven-notification-upgrade").modal({
                keyboard: true
            });
        {/literal}
    </script>
{/if}
{if $lineven.apo.admin.datas.notifications.for_extra_upgrade}
    <div id="lineven-extra_upgrade-progression" class="lineven-extra_upgrade-progression lineven-dashboard-notification modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="font-size:1.8em;"><span class="fa fa-refresh fa-fw"></span> {l s='Finalizing the update !' mod='additionalproductsorder'}</span></h4>
                </div>
                <div class="modal-body">
                    <strong>
                        {l s='This version of the module requires a complementary update.' mod='additionalproductsorder'}<br/>
                        {l s='The update only applies to module specific datas.' mod='additionalproductsorder'}<br/>
                        {l s='For more security, we recommend that you make a backup of the module tables.' mod='additionalproductsorder'}<br/>
                        {l s='Database tables of the module are describe in the informations menu.' mod='additionalproductsorder'}
                    </strong>
                    <br/><br/>
                    <div class="row upgrade-version" style="display:none;">
                        <div class="col-lg-12">
                            <span class="text" style="display:none;">{l s='Upgrade for version' mod='additionalproductsorder'}</span>
                            <span class="text-version"></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 18px;">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><span></span>%</div>
                            </div>
                        </div>
                    </div>
                    <div class="row upgrade-version-content" style="display:none;">
                        <div class="col-lg-12">
                            <span class="text"></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 18px;">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><span></span>%</div>
                            </div>
                        </div>
                    </div>
                    <div class="row upgrade-version-complete" style="display:none;">
                        <div class="col-lg-12">
                            <span class="text"><i class="fa fa-thumbs-o-up"></i>&nbsp;<strong>{l s='Upgrade is completed !' mod='additionalproductsorder'}</strong></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="close-button btn btn-default" data-dismiss="modal">{l s='Close' mod='additionalproductsorder'}</button>
                    <button type="button" class="start-button btn btn-default" onClick="LinevenApo.Upgrade.initialize()">{l s='Start' mod='additionalproductsorder'} <i class="fa fa-chevron-right"></i></button>
                </div>
            </div>
        </div>
    </div>
    <script>
        {literal}
            $("#lineven-extra_upgrade-progression").modal({
                keyboard: true
            });
        {/literal}
    </script>
{/if}