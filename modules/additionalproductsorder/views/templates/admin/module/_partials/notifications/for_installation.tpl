{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div id="lineven-notification-installation" class="lineven-dashboard-notification modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-smile-o fa-fw"></i> {l s='Congratulations !!' mod='additionalproductsorder'}</h4>
            </div>
            <div class="modal-body">
                <div class="module_box">
                    <img src="{$lineven.apo.urls.base_url|escape:'html':'UTF-8'}views/img/box/box_{$lineven.apo.admin.datas.notifications.image_box_language|escape:'html':'UTF-8'}.png" style="" />
                </div>
                <div class="subtitle">
                    {l s='Thank you for purchasing the module' mod='additionalproductsorder'}<br/><span class="module_name">{$lineven.apo.display_name|escape:'html':'UTF-8'}.</span>
                </div>
                <div class="message">
                    {l s='If you have some troubles or difficulties to use this module you can contact our support via the platform PrestaShop Addons.' mod='additionalproductsorder'}
                </div>
                <div class="signature">
                    {l s='The Lineven Team.' mod='additionalproductsorder'}
                </div>
                <br style="clear:both;"/>
            </div>
            <div class="modal-footer">
                <div class="notation">
                    <a href="{$lineven.apo.admin.datas.prestashop_addons_link|escape:'html':'UTF-8'}" target="_blank"><img src="{$lineven.apo.urls.base_url|escape:'html':'UTF-8'}/views/img/notation.png" /><span>{l s='Thank you to vote for this module on the Addons Prestashop.' mod='additionalproductsorder'}</span></a>
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='additionalproductsorder'}</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("#lineven-notification-installation").modal({
        keyboard: true
    })
</script>
