{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}
 
<div>
{if isset($lineven.apo.admin.datas.message_form_errors) && $lineven.apo.admin.datas.message_form_errors != ''}
    {* HTML CONTENT *}
    {LinevenApoTools::displayHtmlMessage('error', $lineven.apo.admin.datas.message_form_errors) nofilter}
    {* /HTML CONTENT *}
{/if}
{if isset($lineven.apo.admin.datas.message_warning) && $lineven.apo.admin.datas.message_warning != ''}
    {* HTML CONTENT *}
    {LinevenApoTools::displayHtmlMessage('warning', $lineven.apo.admin.datas.message_warning) nofilter}
    {* /HTML CONTENT *}
{/if}
{if isset($lineven.apo.admin.datas.message_confirmation) && $lineven.apo.admin.datas.message_confirmation != ''}
    {* HTML CONTENT *}
    {LinevenApoTools::displayHtmlMessage('confirmation', $lineven.apo.admin.datas.message_confirmation) nofilter}
    {* /HTML CONTENT *}
{/if}
</div>