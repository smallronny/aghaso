{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div id="lineven-diagnostic-progression" class="lineven-dashboard-notification modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" style="font-size:1.8em;"><span class="fa fa-wrench fa-fw"></span> {l s='Automatic repair !' mod='additionalproductsorder'}</h4>
			</div>
			<div class="modal-body">
				<div class="message">
					<strong>{l s='A problem seems to have occurred during upgrade ! The module has been automatically disabled.' mod='additionalproductsorder'}</strong><br/><br/>
					<strong>{l s='This tool will perform an integrity check and attempt an automatic repair.' mod='additionalproductsorder'}</strong><br/><br/>
					<div id="lapo-diagnostic-progression-message-begin">
						{l s='A backup of the module tables will be performed if necessary. Uncheck the setting below to ignore module tables backup.' mod='additionalproductsorder'}<br/>
						<div class="checkbox">
							<label for="lapo-diagnostic-progression-tables-backup">
								<input name="lapo-diagnostic-progression-tables-backup" id="lapo-diagnostic-progression-tables-backup"
								   value="1" type="checkbox" checked>
								{l s='Do a backup of module tables.' mod='additionalproductsorder'}
							</label>
						</div>
					</div>
					<div class="success" id="lapo-diagnostic-progression-message-final" style="display: none;">
                        {l s='The automatic repare is finished !' mod='additionalproductsorder'}<br/>
                        {l s='The module has been activated in test mode. Check the test mode and add your IP address if necessary.' mod='additionalproductsorder'}<br/>
                        {l s='Check the module on your shop and disable the test mode if if everything is ok.' mod='additionalproductsorder'}<br/>
					</div>
				</div>
				<div id="progression-first" class="row" style="display:none;">
					<br/>
					<div class="col-lg-12">
						<span class="text" data-init-label="{l s='Initialization' mod='additionalproductsorder'}"></span>...
					</div>
					<div class="col-lg-12" style="margin-top: 18px;">
						<div class="progress progress-first">
							<div class="progress-bar progress-bar-first progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><span></span>%</div>
						</div>
					</div>
				</div>
				<div id="progression-second" class="row" style="display:none;">
					<div class="col-lg-12">
						<span class="text" data-init-label=""></span>...
					</div>
					<div class="col-lg-12" style="margin-top: 18px;">
						<div class="progress progress-second">
							<div class="progress-bar progress-bar-second progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><span></span>%</div>
						</div>
					</div>
				</div>
				<div id="lineven-diagnostic-report" style="display:none; height:350px; margin-top: 12px; border: 1px solid #6C868E; overflow: auto;"></div>
			</div>
			<div class="modal-footer">
				<button id="lineven-diagnostic-progression-button-close" type="button" class="btn btn-default disabled" data-dismiss="modal" onclick="window.location.reload();">{l s='Close' mod='additionalproductsorder'}</button>
				<button id="lineven-diagnostic-progression-button-launch" type="button" class="btn btn-default"onClick="LinevenApo.Diagnostic.launch()"><i class="fa fa-user-md"></i> {l s='Launch' mod='additionalproductsorder'}</button>
			</div>
		</div>
	</div>
</div>
<script>
    LinevenApo.Diagnostic.auto_repair = true;
    $("#lineven-diagnostic-progression").modal({
        keyboard: true
    })
</script>

