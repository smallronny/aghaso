{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="panel">
	<div class="panel-heading">
		<i class="fa fa-medkit"></i>
		{l s='Diagnostic' mod='additionalproductsorder'}
	</div>
	<div>
        {assign var='col_size' value=12}
		{l s='This tool will allow you to check the technical integrity of the module (files, tables, ...).' mod='additionalproductsorder'}
		<br/>{l s='It will not diagnose compatibilities issues of theme, design, etc.' mod='additionalproductsorder'}
        {if $lineven.apo.admin.datas.is_functional_diagnostic}
            {assign var='col_size' value=6}
			<br/><br/>{l s='This tool will, as far as possible, alert you to functional inconsistency.' mod='additionalproductsorder'}
        {/if}
		<br/><br/>
		<button type="button" class="btn btn-default" onClick="LinevenApo.Diagnostic.launch()"><i class="fa fa-user-md"></i> {l s='Launch diagnostic' mod='additionalproductsorder'}</button>
		<div id="lineven-diagnostic-report" class="row" style="display:none;">
			<div class="col-lg-{$col_size|intval}">
				<div class="panel technical"></div>
			</div>
            {if $lineven.apo.admin.datas.is_functional_diagnostic}
				<div class="col-lg-{$col_size|intval}">
					<div class="panel functional"></div>
				</div>
            {/if}
		</div>
	</div>
</div>
<div id="lineven-diagnostic-progression" class="lineven-diagnostic-progression modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" style="font-size:1.8em;"><span class="fa fa-wrench fa-fw"></span> {l s='Diagnostic in progress !' mod='additionalproductsorder'}</span></h4>
			</div>
			<div class="modal-body">
				<div id="progression-first" class="row">
					<div class="col-lg-12">
						<span class="text" data-init-label="{l s='Initialization' mod='additionalproductsorder'}"></span>...
					</div>
					<div class="col-lg-12" style="margin-top: 18px;">
						<div class="progress progress-first">
							<div class="progress-bar progress-bar-first progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><span></span>%</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
