{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $is_success}
	{if $operation == 'action'}
		<div class="success"><i class="fa fa-smile-o"></i> {$outputs.checking.result_message|escape:'html':'UTF-8'}</div>
	{else}
		<div class="success"><i class="fa fa-smile-o"></i> {l s='The problem is fixed !' mod='additionalproductsorder'}</div>
	{/if}
{else}
    {assign var="class_level" value=$level}
    {if $is_success_required}
        {assign var="class_level" value="critical"}
    {/if}
    {if $operation == 'action'}
		<div class="{$class_level|escape:'html':'UTF-8'}"><i class="fa fa-smile-o"></i> {l s='An error occured during this operation.' mod='additionalproductsorder'}</div>
    {else}
		<div class="{$class_level|escape:'html':'UTF-8'}"><i class="fa fa-smile-o"></i> {l s='An error occured during repair.' mod='additionalproductsorder'}
			<br/>
            {if $outputs.repair}
                {$outputs.repair.error_message|escape:'html':'UTF-8'}
            {/if}
		</div>
    {/if}
	{if $is_success_required}
		<br/>
		<div class="critical"><i class="fa fa-times"></i> {l s='The automatic reparation aborded. You must contact support for help !' mod='additionalproductsorder'}</div>
	{/if}
{/if}