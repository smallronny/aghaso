{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="{$what|escape:'html':'UTF-8'} result">
	<div class="title"><i class="fa fa-angle-double-right"></i> {$outputs.checking.progression_text|escape:'html':'UTF-8'}</div>
	<div class="message">
		{if $has_output}
            {assign var='count_results' value=''}
	        {if !isset($outputs.checking.hide_results) || (isset($outputs.checking.hide_results) && $outputs.checking.hide_results == false)}
				{if is_array($results)}
					{assign var='count_results' value=$results|count}
				{else}
					{assign var='count_results' value=$results}
				{/if}
            {/if}
            {if $operation == 'repair'}
				<div class="{$level|escape:'html':'UTF-8'}"><i class="fa fa-frown-o"></i>&nbsp;{$count_results|escape:'html':'UTF-8'}&nbsp;{$outputs.checking.error_message nofilter}</div>
			{else}
				<div class="information"><i class="fa fa-info-circle"></i>&nbsp;{$count_results|escape:'html':'UTF-8'}&nbsp;{$outputs.checking.information_message nofilter}</div>
            {/if}
	        {if is_array($results)}
				<div class="{$level|escape:'html':'UTF-8'}">
					<ul>
						{foreach from=$results key=first_item_name item=first_item}
							<li>
								{if is_array($first_item)}
									{$first_item_name|escape:'html':'UTF-8'}
									<ul>
										{foreach from=$first_item item=second_item}
											<li>{$second_item|escape:'html':'UTF-8'}</li>
										{/foreach}
									</ul>
								{else}
									{$first_item|escape:'html':'UTF-8'}
								{/if}
							</li>
						{/foreach}
					</ul>
				</div>
			{/if}
		{else}
			<div class="success"><i class="fa fa-smile-o"></i> {l s='No problem detected.' mod='additionalproductsorder'}</div>
		{/if}
	</div>
    {if $has_output}
		<div class="actions">
			{if $operation == 'action'}
				<div>{$outputs.checking.action_message|escape:'html':'UTF-8'}</div>
			{else}
                {if !$is_auto_repair || !$repair.is_automatic}
                    {if isset($outputs.checking.repair_message)}<div class="{$level|escape:'html':'UTF-8'}">{$outputs.checking.repair_message|escape:'html':'UTF-8'}</div>{/if}
                    {if isset($outputs.checking.button)}
						<button type="button" class="btn btn-default" onClick="LinevenApo.Diagnostic.execute('{$what|escape:'html':'UTF-8'}', false)"><i class="fa fa-wrench"></i> {$outputs.checking.button|escape:'html':'UTF-8'}</button>
                    {/if}
                {else}
					<div class="information">{l s='This problem must be fixed.' mod='additionalproductsorder'}</div>
					<div class="information">{l s='Auto repair attempt.' mod='additionalproductsorder'}</div>
                {/if}
			{/if}
		</div>
	{/if}
	<div class="repair_results" style="display: none;"></div>
</div>
