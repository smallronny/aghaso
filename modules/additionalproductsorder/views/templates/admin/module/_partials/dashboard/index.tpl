{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{block name="messages"}
    {if isset($smarty.get.dashboard_toggle_link) && $smarty.get.dashboard_toggle_link == 'test_mode'}
        {if isset($smarty.get.dashboard_toggle_value) && $smarty.get.dashboard_toggle_value == 1}
            <div id="testmode-dashboard-testmode-confirmation-enabled-message" class="module_confirmation conf confirm alert alert-success">
                {l s='Test mode has been activated for the module.' mod='additionalproductsorder'}
            </div>
        {else}
            <div id="testmode-dashboard-testmode-confirmation-disabled-message" class="module_confirmation conf confirm alert alert-success">
                {l s='Test mode has been disabled for the module.' mod='additionalproductsorder'}
            </div>
        {/if}
    {/if}
{/block}
<div class="col-lg-7">
    {block name="before_warning"}{/block}
    {block name="warning"}
        {if $lineven.apo.admin.datas.panel_warning == 1}
            <div class="panel" id="lineven-template-content-dashboard-panel_warning" style="display:inherit;">
                <div class="panel-heading" style="color: #D27C82;">
                    <i class="fa fa-exclamation-triangle"></i>
                    {l s='Warning after this upgrade' mod='additionalproductsorder'}
                    <div class="panel-heading-action">
                        <a class="list-toolbar-btn" href="#" onClick="LinevenApo.Tools.closePanel('panel_warning', '{$lineven.apo.admin.datas.panel_warning_configuration_name|escape:'html':'UTF-8'}')">
                                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Close' mod='additionalproductsorder'}" data-html="true" data-placement="top">
                                    <i class="icon-fa-panel-heading fa fa-times"></i>
                                </span>
                            </a>
                    </div>
                </div>
                <p style="font-weight: bold;">
                        {l s='The Smarty templates of this module have been change in this version.' mod='additionalproductsorder'}
                   <br/><br/>
                   {if $lineven.apo.admin.datas.panel_warning_smarty == 1}
                            {l s='According your Prestashop configuration, your templates seems to be never recompilated after modifications.' mod='additionalproductsorder'}<br/>
                            {l s='Templates of this module have been changed, you need to recompile templates otherwise you may have errors on your shop.' mod='additionalproductsorder'}<br/>
                            {l s='Prestashop configuration for Smarty compilations is in the menu: Advanced Paremeters > Performances.' mod='additionalproductsorder'}
                    <br/><br/>
                    <span style="color: #D27C82;">
                        {l s='For safety reasons, the module has been disabled so you can make the necessary checks.' mod='additionalproductsorder'}
                    </span>
                    <br/><br/>
                   {/if}
                   {l s='If you have changed templates of this module in your theme folder, you need to reproduce the modifications by using new templates. Otherwise you may have errors on your shop.' mod='additionalproductsorder'}
                   <div style="text-align:right;margin-top: 19px;">
                    <button type="button" class="btn btn-default" onClick="LinevenApo.Tools.closePanel('panel_warning', '{$lineven.apo.admin.datas.panel_warning_configuration_name|escape:'html':'UTF-8'}')">{l s='I got it !' mod='additionalproductsorder'}</button>
                   </div>
                </p>
            </div>
        {/if}
    {/block}
    {block name="before_infos"}{/block}
    {block name="infos"}
        {if $lineven.apo.admin.datas.panel_info_display == 1}
            <div class="panel" id="lineven-template-content-dashboard-panel_infos" style="display:inherit;">
                <div class="panel-heading">
                    <i class="fa fa-info"></i>
                    {l s='What does this module do?' mod='additionalproductsorder'}
                    <div class="panel-heading-action">
                        <a class="list-toolbar-btn" href="#" onClick="LinevenApo.Tools.closePanel('panel_infos', '{$lineven.apo.admin.datas.panel_info_configuration_name|escape:'html':'UTF-8'}')">
                                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Close' mod='additionalproductsorder'}" data-html="true" data-placement="top">
                                    <i class="icon-fa-panel-heading fa fa-times"></i>
                                </span>
                            </a>
                    </div>
                </div>
                {block name="infos_content"}{/block}
            </div>
        {/if}
    {/block}
    {block name="before_update"}{/block}
    {block name="update"}
        {if $lineven.apo.admin.datas.panel_update_display == 1}
            <div class="panel" id="lineven-template-content-dashboard-panel_update" style="display:inherit;">
                <div class="panel-heading">
                        <i class="fa fa-refresh"></i>
                    {l s='What is new in this version?' mod='additionalproductsorder'}
                        <div class="panel-heading-action">
                                <a class="list-toolbar-btn" href="#" onClick="LinevenApo.Tools.closePanel('panel_update', '{$lineven.apo.admin.datas.panel_update_configuration_name|escape:'html':'UTF-8'}')">
                                    <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Close' mod='additionalproductsorder'}" data-html="true" data-placement="top">
                                        <i class="icon-fa-panel-heading fa fa-times"></i>
                                    </span>
                                </a>
                    </div>
                </div>
                <p>
                    {* HTML CONTENT *}
                    {$lineven.apo.admin.datas.panel_update_content nofilter}
                    {* /HTML CONTENT *}
                </p>
            </div>
        {/if}
    {/block}
    {block name="before_report"}{/block}
    {block name="report"}
        <div class="panel">
           <div class="panel-heading">
             <i class="{block name="report_icon"}fa fa-tachometer{/block}"></i>
               {block name="report_title"}{l s='Report' mod='additionalproductsorder'}{/block}
            </div>
            <span class="report_content">
                {block name="report_content"}{/block}
            </span>
         </div>
    {/block}
    {block name="before_ads"}{/block}
    {block name="ads"}
        {if isset($lineven.apo.admin.datas.ads_count) && $lineven.apo.admin.datas.ads_count}
            <div class="panel">
                <div class="panel-heading">
                    <i class="fa fa-puzzle-piece"></i>
                    {l s='You may be interested in these modules' mod='additionalproductsorder'}
                </div>
                <div id="carousel-ads" class="carousel slide clearfix" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                    {foreach from=$lineven.apo.admin.datas.ads item=ad name=ad_name}
                          <!-- Wrapper for slides -->
                                <div class="item {if $smarty.foreach.ad_name.first}active{/if}">
                                    <div class="lineven-ad">
                                <a href="{$ad.href|escape:'html':'UTF-8'}" target="_blank">
                                <div class="icon">
                                    <img src="{$lineven.apo.urls.base_url|escape:'html':'UTF-8'}/views/img/ads/{$ad.icon|escape:'html':'UTF-8'}.png"/>
                                  </div>
                                  <div class="">
                                    <div class="name">{$ad.name|escape:'html':'UTF-8'}</div>
                                      {if is_array($ad.description)}
                                          <div class="description">
                                              {foreach $ad.description as $ad_sentence}
                                                  {$ad_sentence|escape:'html':'UTF-8'}<br/>
                                              {/foreach}
                                          </div>
                                      {else}
                                          {if $ad.description != ''}
                                              <div class="description">{$ad.description|escape:'html':'UTF-8'}</div>
                                          {/if}
                                      {/if}
                                  </div>
                               </a>
                            </div>
                        </div>
                      {/foreach}
                   </div>
                   <ol class="carousel-indicators">
                        {foreach from=$lineven.apo.admin.datas.ads item=ad name=ad_name}
                            <li data-target="#carousel-ads" data-slide-to="{$smarty.foreach.ad_name.index|intval}" class="{if $smarty.foreach.ad_name.first}active{/if}" style="background-color: #dfdfdf;"></li>
                        {/foreach}
                   </ol>
               </div>
            </div>
        {/if}
    {/block}
</div>
<div class="col-lg-5">
    {block name="before_alerts"}{/block}
    {block name="alerts"}
        {assign var=alerts_icon value="fa fa-bell-o fa-lg"}
        {if $lineven.apo.admin.datas.is_alerts}
            {assign var=alerts_icon value="fa fa-bell fa-lg"}
        {/if}
        <div class="panel">
            <div class="panel-heading">
                <i class="{$alerts_icon|escape:'html':'UTF-8'}"></i>
                {l s='Notifications' mod='additionalproductsorder'}
                <span class="badge">{$lineven.apo.admin.datas.count_alerts|intval}</span>
            </div>
            {if !$lineven.apo.admin.datas.is_alerts}
                {l s='There is no notification' mod='additionalproductsorder'}
            {else}
                {foreach from=$lineven.apo.admin.datas.alerts item=alert}
                    <div class="lineven-alerts">
                    {assign var=color value=""}
                    {if isset($alert.color)}
                        {assign var=color value="color:{$alert.color};"}
                    {/if}
                    {if isset($alert.items)}
                        <div class="icon {$alert.icon|escape:'html':'UTF-8'}"></div><div class="message">{$alert.message|escape:'html':'UTF-8'}</div>
                        <ul>
                            {foreach from=$alert.items item=alert_item}
                                <li>{$alert_item|escape:'html':'UTF-8'}</li>
                            {/foreach}
                        </ul>
                        <div class="lineven-alerts-link-to-fix">
                            <a href="{if isset($alert.href)}{$alert.href|escape:'html':'UTF-8'}{else}#{/if}" style="{$color|escape:'html':'UTF-8'}" {if isset($alert.on_click)} onClick="{$alert.on_click|escape:'html':'UTF-8'}{/if}">
                                {$alert.label_fix|escape:'html':'UTF-8'}
                            </a>
                        </div>
                    {else}
                        <div>
                            <a href="{if isset($alert.href)}{$alert.href|escape:'html':'UTF-8'}{else}#{/if}" style="{$color|escape:'html':'UTF-8'}" {if isset($alert.on_click)} onClick="{$alert.on_click|escape:'html':'UTF-8'}"{/if}>
                                <div class="icon {$alert.icon|escape:'html':'UTF-8'}"></div><div class="message">{$alert.message|escape:'htmlall':'UTF-8'}</div>
                            </a>
                        </div>
                    {/if}
                    </div>
                {/foreach}
            {/if}
        </div>
    {/block}
    {block name="before_demonstration_reset"}{/block}
    {block name="demonstration_reset"}
        {if $lineven.apo.admin.datas.is_demonstration_reset_available}
            <div id="lineven-popup-demontration_datas_reset_pending" class="lineven-dashboard-notification modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><i class="fa fa-refresh"></i>&nbsp;{l s='Reset demonstration datas' mod='additionalproductsorder'}</h4>
                        </div>
                        <div class="modal-body">
                            {l s='Be patient while resetting demonstration datas...' mod='additionalproductsorder'}
                        </div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
            <div id="lineven-popup-demontration_datas_reset_finished" class="lineven-dashboard-notification modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><i class="fa fa-refresh"></i>&nbsp;{l s='Reset demonstration datas' mod='additionalproductsorder'}</h4>
                        </div>
                        <div class="modal-body">
                            {l s='Demonstration datas are now reseted.' mod='additionalproductsorder'}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='additionalproductsorder'}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel" id="lineven-template-content-dashboard-panel_demo_datas_reset" style="display:inherit;">
                <div class="panel-heading">
                    <i class="fa fa-refresh"></i>
                    {l s='Reset demonstration datas' mod='additionalproductsorder'}
                </div>
                <p>
                    <strong>{l s='To test this module, you can reset demonstration datas.' mod='additionalproductsorder'}</strong><br/><br/>
                    {block name="demonstration_reset_infos"}{/block}
                    {l s='If you want reset demos datas, click on the button below.' mod='additionalproductsorder'}<br/>
                    <div style="text-align:center;"><button type="button" class="btn btn-default" onClick="LinevenApo.Tools.resetDemonstrationDatas()"><i class="fa fa-refresh"></i> {l s='Reset datas' mod='additionalproductsorder'}</button></div>
                </p>
            </div>
        {/if}
    {/block}
    {block name="before_demonstration"}{/block}
    {block name="demonstration"}
        {if $lineven.apo.admin.datas.is_demonstration_datas_available && $lineven.apo.admin.datas.is_demonstration_datas_feasible && $lineven.apo.admin.datas.panel_demo_datas_display == 1}
            <div id="lineven-popup-demontration_datas_pending" class="lineven-dashboard-notification modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><i class="fa fa-database"></i>&nbsp;{l s='Install demonstration datas' mod='additionalproductsorder'}</h4>
                        </div>
                        <div class="modal-body">
                            {l s='Datas will be installed in English for your default language.' mod='additionalproductsorder'}<br/><br/>
                            {l s='Be patient during demonstration datas installation...' mod='additionalproductsorder'}<br/>
                        </div>
                        <div class="modal-footer">&nbsp;</div>
                    </div>
                </div>
            </div>
            <div id="lineven-popup-demontration_datas_finished" class="lineven-dashboard-notification modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><i class="fa fa-database"></i>&nbsp;{l s='Install demonstration datas' mod='additionalproductsorder'}</h4>
                        </div>
                        <div class="modal-body">
                            {l s='Demonstration datas are now installed...' mod='additionalproductsorder'}<br/><br/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='additionalproductsorder'}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel" id="lineven-template-content-dashboard-panel_demo_datas" style="display:inherit;">
                <div class="panel-heading">
                    <i class="fa fa-database"></i>
                    {l s='Install demonstration datas' mod='additionalproductsorder'}
                    <div class="panel-heading-action">
                        <a id="lineven-template-content-dashboard-panel_demo_datas_close_button" class="list-toolbar-btn" href="#" onClick="LinevenApo.Tools.closePanel('panel_demo_datas', '{$lineven.apo.admin.datas.panel_demo_datas_configuration_name|escape:'html':'UTF-8'}')">
                            <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Close' mod='additionalproductsorder'}" data-html="true" data-placement="top">
                                <i class="icon-fa-panel-heading fa fa-times"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <p>
                    {if $lineven.apo.admin.datas.is_demonstration_datas_feasible}
                        <strong>{l s='You can install demonstration datas.' mod='additionalproductsorder'}</strong><br/><br/>
                        {l s='If you want install demos datas, click on the button below.' mod='additionalproductsorder'}<br/>
                        <div style="text-align:center;"><button type="button" class="btn btn-default" onClick="LinevenApo.Tools.installDemonstrationDatas()"><i class="fa fa-database"></i> {l s='Install datas' mod='additionalproductsorder'}</button></div><br/>
                        {l s='You can close this panel to hide this message.' mod='additionalproductsorder'}<br/>
                    {else}
                        {l s='You have saved data.' mod='additionalproductsorder'}<br/>
                        {l s='So it is not possible to load demonstration datas.' mod='additionalproductsorder'}
                    {/if}
                </p>
            </div>
        {/if}
    {/block}
    {block name="before_test_mode"}{/block}
    {block name="test_mode"}
        {if $lineven.apo.environment != LinevenApoContext::$environment_demonstration}
            <div class="panel">
                <div class="panel-heading">
                    <i class="fa fa-moon-o"></i>
                    {l s='Test mode' mod='additionalproductsorder'}
                </div>
                {assign var=checked_test_mode_active value=""}
                {assign var=checked_test_mode_inactive value=""}
                {if $lineven.apo.is_test_mode}
                    {assign var=checked_test_mode_active value=' checked="checked" '}
                {else}
                    {assign var=checked_test_mode_inactive value=' checked="checked" '}
                {/if}
                <p>
                    <div class="form-group" style="clear: both; margin:0;padding:0;height:20px;text-align:left;">
                        <label class="control-label" style="float: left;margin-right: 10px; margin-top:6px; text-align:left;">
                            {l s='Activate test mode' mod='additionalproductsorder'}
                        </label>
                        <div style="float: left">
                            <span class="switch prestashop-switch fixed-width-md">
                                <input type="radio" name="active_test" id="active_test_on" value="1" {$checked_test_mode_active|escape:'html':'UTF-8'}
                                    onClick="LinevenApo.Tools.toggleStatus('test_mode')"/>
                                <label for="active_test_on">{l s='Yes' mod='additionalproductsorder'}</label>
                          <input type="radio" name="active_test" id="active_test_off" value="0" {$checked_test_mode_inactive|escape:'html':'UTF-8'}
                            onClick="LinevenApo.Tools.toggleStatus('test_mode')"/>
                                <label  for="active_test_off">{l s='No' mod='additionalproductsorder'}</label>
                                <a class="slide-button btn"></a>
                            </span>
                        </div>
                    </div>
                </p>
            </div>
        {/if}
    {/block}
    {block name="before_support"}{/block}
    {block name="support"}
        <div class="panel">
            <div class="panel-heading">
                <i class="fa fa-question-circle"></i>
                {l s='Support' mod='additionalproductsorder'}
            </div>
            <table id="lineven-table-details" border="0">
                <tr>
                    <td class="ltd-label">{l s='Prestashop version' mod='additionalproductsorder'}</td>
                    <td class="ltd-value">{$lineven.prestashop.version|escape:'html':'UTF-8'}</td>
                </tr>
                <tr>
                    <td class="ltd-label">{l s='Module technical name' mod='additionalproductsorder'}</td>
                    <td class="ltd-value">{$lineven.apo.name|escape:'html':'UTF-8'}</td>
                </tr>
                <tr>
                    <td class="ltd-label">{l s='Module version' mod='additionalproductsorder'}</td>
                    <td class="ltd-value">{$lineven.apo.version|escape:'html':'UTF-8'}</td>
                </tr>
                {if isset($lineven.apo.admin.datas.prestashop_addons_link) && $lineven.apo.admin.datas.prestashop_addons_link != ''}
                    <tr>
                        <td class="ltd-label">{l s='Prestashop Addons page' mod='additionalproductsorder'}</td>
                        <td class="ltd-value" style="white-space: normal;"><a href="{$lineven.apo.admin.datas.prestashop_addons_link|escape:'html':'UTF-8'}" target="_blank">{$lineven.apo.display_name|escape:'html':'UTF-8'}</a></td>
                    </tr>
                {/if}
            </table>
        </div>
    {/block}
    {block name="after_support"}{/block}
</div>