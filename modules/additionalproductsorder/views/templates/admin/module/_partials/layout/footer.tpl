{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}
 		
		</div>
	</div>
</div>
{if isset($lineven.apo.admin.datas.notifications)}
	{if $lineven.apo.admin.datas.notifications.for_upgrade}
        {include file="./../notifications/for_upgrade.tpl"}
	{/if}
	{if $lineven.apo.admin.datas.notifications.for_installation}
        {include file="./../notifications/for_installation.tpl"}
	{/if}
	{if $lineven.apo.admin.datas.notifications.for_demonstration}
        {include file="./../notifications/for_demonstration.tpl"}
	{/if}
    {if $lineven.apo.admin.datas.notifications.for_automatic_repair}
        {include file="./../notifications/for_automatic_repair.tpl"}
    {/if}
{/if}
{if isset($lineven.apo.admin.datas.message_box) && $lineven.apo.admin.datas.message_box}
	<div id="lineven-messagebox" class="lineven-dashboard-notification modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="font-size:1.8em;"><i class="fa fa-info-circle fa-fw"></i> <span class="title">{$lineven.apo.admin.datas.message_box.title|escape:'html':'UTF-8'}</span></h4>
				</div>
				<div class="modal-body">
					<div class="message">
                        {* HTML CONTENT *}
                        {$lineven.apo.admin.datas.message_box.content nofilter}
                        {* /HTML CONTENT *}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='additionalproductsorder'}</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		{literal}
        $("#lineven-messagebox").modal({
            keyboard: true
        })
        {/literal}
	</script>
{/if}