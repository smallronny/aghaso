{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<script>
    {literal}LinevenApo.initialize({{/literal}
        module_code: '{$lineven.apo.code|escape:'html':'UTF-8'}',
        module_name: '{$lineven.apo.name|escape:'html':'UTF-8'}',
        is_test_mode: {$lineven.apo.is_test_mode|intval},
        is_debug_mode: {$lineven.apo.is_debug_mode|intval},
        action_uri: '{* URL FOR JAVASCRIPT *}{$lineven.apo.urls.backoffice_action_url nofilter}{* /URL FOR JAVASCRIPT *}'
    {literal}});{/literal}
</script>
<div id="lineven-template" class="row {if $lineven.apo.admin.layout.display_in_tab}lineven_in_tab{/if}">
	<a name="lineven_top"></a>
	{if $lineven.apo.admin.layout.navigation.show_navigation}
		<div id="lineven-template-navigation" class="panel-group col-lg-2 col-md-3 col-sm-3">
			{include file="./navigation.tpl" navigation=$lineven.apo.admin.layout.navigation.tree level=0}
		</div>
	{/if}
    <div id="lineven-template-content" {if $lineven.apo.admin.layout.navigation.show_navigation}class="col-lg-10 col-md-9 col-sm-9"{/if}>
		{if $lineven.apo.admin.layout.navigation.tree}
    		<div class="breadcrumbs row">
				{if isset($lineven.apo.admin.layout.navigation.breadcrumbs) && count($lineven.apo.admin.layout.navigation.breadcrumbs)}
					<ol class="breadcrumb">
                    {foreach from=$lineven.apo.admin.layout.navigation.breadcrumbs item=breadcrumb_link}
						<li class="{if isset($breadcrumb_link.class)}{$breadcrumb_link.class|escape:'html':'UTF-8'}{/if}">
                            {if isset($breadcrumb_link.href)}<a href="{$breadcrumb_link.href|escape:'html':'UTF-8'}">{$breadcrumb_link.label|escape:'html':'UTF-8'}</a>{else}{$breadcrumb_link.label|escape:'html':'UTF-8'}{/if}
						</li>
                    {/foreach}
					</ol>
                {/if}
    		</div>
		{/if}
		<div id="lineven-template-content-{$lineven.apo.admin.controller_name|lower|escape:'html':'UTF-8'}-{$lineven.apo.admin.action_name|lower|escape:'html':'UTF-8'}"
			class="center row">			