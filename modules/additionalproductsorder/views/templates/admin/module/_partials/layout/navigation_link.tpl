{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{assign var=navigation_pipe value=$item_first_key}
{if isset($item_second_key)}
    {assign var=navigation_pipe value=$navigation_pipe|cat:':'|cat:$item_second_key}
{/if}
<a id="lineven-template-navigation-{$item_first_key|escape:'html':'UTF-8'}{if isset($item_second_key)}-{$item_second_key|escape:'html':'UTF-8'}{/if}"
	{if $level==0} attr-collapse-id="{$item_index|intval}" {/if}
	class="list-group-item {if $level==0} list-group-collapse {/if} {if isset($item.active) && $item.active}active{/if} {if $level==0 && (!isset($item.collapse) || (isset($item.collapse) && $item.collapse != 'disabled'))} collapse {if isset($item.collapse) && $item.collapse == 'hide'} collapsed{/if}{/if}"
	{if isset($item.controller)}
		href="{$lineven.apo.urls.backoffice_action_url|escape:'html':'UTF-8'}&{$lineven.apo.name|escape:'html':'UTF-8'}_controller={$item.controller|escape:'html':'UTF-8'}&{$lineven.apo.name|escape:'html':'UTF-8'}_action={$item.action|escape:'html':'UTF-8'}&{$lineven.apo.name|escape:'html':'UTF-8'}_navigation={$navigation_pipe|escape:'html':'UTF-8'}{if isset($item.params)}{$item.params|escape:'html':'UTF-8'}{/if}"
	{else}
		{if isset($item.href)}
			href="{$item.href|escape:'html':'UTF-8'}"
		{else}
			{if isset($item.script)}
				href="#" onClick="{$item.script|escape:'html':'UTF-8'}"
			{else}
				{if $level==0}
					data-toggle="collapse" data-target="#panel-body-{$item_first_key|escape:'html':'UTF-8'}"
   						href="javascript:void(0);"
				{/if}
			{/if}
		{/if}
	{/if}
>
{if isset($item.icon)}
	<div class="icon {$item.icon|escape:'html':'UTF-8'}"></div>
{/if}
<div class="{if $level==0}title-{/if}label">{$item.label|escape:'html':'UTF-8'}</div></a>