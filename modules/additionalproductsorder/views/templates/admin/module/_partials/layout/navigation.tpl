{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{foreach from=$navigation item=item key=item_key name=item_name}
	{assign var='is_display' value=true}
    {assign var='is_children_display' value=true}
    {if (isset($item.display) && $item.display == false) || !($lineven.apo.environment == 'DEVELOPMENT' || !isset($item.environment) || (isset($item.environment) && $item.environment == $lineven.apo.environment)) && (!isset($item.prestashop_version) || (isset($item.prestashop_version) && in_array($lineven.prestashop.major_version, $item.prestashop_version)))}
	    {assign var='is_display' value=false}
	{/if}
    {if isset($item.display_items)  && $item.display_items == false}
        {assign var='is_children_display' value=false}
    {/if}
    {if $level == 0}
        {if $is_display && ($lineven.apo.admin.layout.navigation.show_menus == false || (isset($lineven.apo.admin.layout.navigation.show_menus) && is_array($lineven.apo.admin.layout.navigation.show_menus) && in_array($item_key, $lineven.apo.admin.layout.navigation.show_menus)))}
			<div class="panel panel-default">
				<div class="list-group {if isset($item.items) && $is_children_display && !(isset($item.collapse) && $item.collapse == 'disabled')}header{/if}">
					{include file="./navigation_link.tpl" item=$item item_first_key=$item_key level=$level item_index=$smarty.foreach.item_name.index}
				</div>
				{if isset($item.items) && $level == 0 && $is_children_display}
					<div id="panel-body-{$item_key|escape:'html':'UTF-8'}" class="panel-body {if !isset($item.collapse) || (isset($item.collapse) && $item.collapse != 'disabled')}panel-collapse collapse {if !isset($item.collapse) || (isset($item.collapse) && $item.collapse == 'show')} in{/if}{/if}" >
						<div class="list-group">
							{include file="./navigation.tpl" navigation=$item.items level=1 item_parent_key=$item_key item_parent=$item}
						</div>
					</div>
				{/if}
			</div>
        {/if}
	{/if}
	{if $level == 1 && $is_display}
		{include file="./navigation_link.tpl" item=$item item_first_key=$item_parent_key item_second_key=$item_key level=$level}
        {if isset($item.separator)}
			{if $item.separator.type == 'line' && (!isset($item.separator.display) || (isset($item.separator.display) && $item.separator.display == true))}
				<hr class="separator"/>
			{/if}
		{/if}
	{/if}
{/foreach}
