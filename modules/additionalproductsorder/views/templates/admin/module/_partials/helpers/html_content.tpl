{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $type == 'simple_tag'}
    <{$settings.tag|escape:'html':'UTF-8'}>{$settings.text|escape:'html':'UTF-8'}</{$settings.tag|escape:'html':'UTF-8'}>
{/if}
{if $type == 'link'}
    <a href="{$settings.url|escape:'html':'UTF-8'}" target="_blank">{$settings.text|escape:'html':'UTF-8'}</a>
{/if}
{if $type == 'icon'}
    <i class="{$settings|escape:'html':'UTF-8'}"></i>
{/if}
{if $type == 'title'}
    <strong>{$settings|escape:'html':'UTF-8'}</strong><br/>
{/if}