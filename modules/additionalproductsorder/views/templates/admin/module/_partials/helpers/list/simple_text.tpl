{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if isset($helper_listing_text) && is_array($helper_listing_text)}
    <span
        {if isset($helper_listing_text.span_id)} id="{$helper_listing_text.span_id|escape:'html':'UTF-8'}"{/if}
        {if isset($helper_listing_text.span_class)} class="{$helper_listing_text.span_class|escape:'html':'UTF-8'}"{/if}
    >
        {if isset($helper_listing_text.icon) && is_array($helper_listing_text.icon)}
            {if isset($helper_listing_text.icon.span_class)}
                <span class="{$helper_listing_text.icon.span_class|escape:'html':'UTF-8'}">
            {/if}
            {if isset($helper_listing_text.icon.i_class)}
                <i class="{$helper_listing_text.icon.i_class|escape:'html':'UTF-8'}"></i>
            {/if}
            {if isset($helper_listing_text.icon.span_class)}
                </span>
            {/if}
        {/if}
        {$helper_listing_text.text|escape:'html':'UTF-8'}
    </span>
{/if}
