{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if isset($helper_listing_options) && count($helper_listing_options)}
    {foreach from=$helper_listing_options item=option}
        {if is_array($option)}
            {if array_key_exists('link', $option)}
                <a class="customer_image_fancybox" href="{$option.link|escape:'html':'UTF-8'}">
                    <div class="helper-list-options-icon fa {$option.icon|escape:'html':'UTF-8'}"></div>
                </a>
            {else}
                <div class="helper-list-options-icon helper-list-options-icon-tooltip fa {$option.icon|escape:'html':'UTF-8'}"
                     data-tooltip-content="#{$option.id_tooltip_content|escape:'html':'UTF-8'}"></div>
                <div style="display:none;">
                    <span id="{$option.id_tooltip_content|escape:'html':'UTF-8'}">
                        {* HTML Content *}
                        {$option.tooltip nofilter}
                        {* /HTML Content *}
                    </span>
                </div>
            {/if}
        {else}
            <div class="helper-list-options-icon fa {$option|escape:'html':'UTF-8'}"></div>
        {/if}
    {/foreach}
{/if}
