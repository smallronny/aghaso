{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="panel">
	<div class="panel-heading">
		<i class="fa fa-history"></i>
		{l s='Change log' mod='additionalproductsorder'}
	</div>
   <p>
   	{if $lineven.apo.admin.datas.changelog != ''}
        {* HTML CONTENT *}
        {$lineven.apo.admin.datas.changelog nofilter}
        {* /HTML CONTENT *}
    {else}
   		{l s='No change log' mod='additionalproductsorder'}
   	{/if}
	</p>
</div>