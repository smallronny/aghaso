{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="row">
	<div class="col-lg-7">
        {block name="before_assets"}{/block}
        {if isset($lineven.apo.admin.datas.assets_css) || isset($lineven.apo.admin.datas.assets_js)}
			<div class="panel">
				<div class="panel-heading">
					<i class="fa fa-file-code-o"></i>
					{l s='Assets files' mod='additionalproductsorder'}
				</div>
				{if isset($lineven.apo.admin.datas.assets_css)}
					{block name="assets_css"}
						<div>
							<strong>{l s='List of CSS files' mod='additionalproductsorder'} :</strong>
							<ul style="margin-top:6px;">
								{foreach from=$lineven.apo.admin.datas.assets_css item=file}
									<li>
										{$lineven.apo.admin.datas.assets_css_directory|escape:'html':'UTF-8'}{basename($file)|escape:'html':'UTF-8'}
									</li>
								{/foreach}
							</ul>
						</div>
					{/block}
				{/if}
				{if isset($lineven.apo.admin.datas.assets_js)}
					{block name="assets_js"}
						<div>
							<strong>{l s='List of JS files' mod='additionalproductsorder'} :</strong>
							<ul style="margin-top:6px;">
								{foreach from=$lineven.apo.admin.datas.assets_js item=file}
									<li>
										{$lineven.apo.admin.datas.assets_js_directory|escape:'html':'UTF-8'}{basename($file)|escape:'html':'UTF-8'}
									</li>
								{/foreach}
							</ul>
						</div>
					{/block}
				{/if}
			</div>
        {/if}
        {block name="before_tables_definitions"}{/block}
		{if $lineven.apo.admin.datas.tables_definition}
            {block name="tables_definitions"}
				<div class="panel">
					<div class="panel-heading">
						<i class="fa fa-database"></i>
						{l s='Databases informations' mod='additionalproductsorder'}
					</div>
					<div>
						<strong>{l s='List of module tables in database' mod='additionalproductsorder'} :</strong>
						<ul style="margin-top:6px;">
							{foreach from=$lineven.apo.admin.datas.tables_definition item=table}
								<li>
									{$table.name|replace:'PREFIX_':$smarty.const._DB_PREFIX_|escape:'html':'UTF-8'}
									<ul>
										{foreach from=$table.fields item=column}
											<li>{$column.name|escape:'html':'UTF-8'}&nbsp; : {$column.def|escape:'html':'UTF-8'}</li>
										{/foreach}
									</ul>
								</li>
							{/foreach}
						</ul>
					</div>
				</div>
            {/block}
        {/if}
	</div>
	<div class="col-lg-5">
        {block name="before_hooks_used"}{/block}
		{if $lineven.apo.admin.datas.hooks_used|count > 0}
            {block name="hooks_used"}
				<div class="panel">
					<div class="panel-heading">
						<i class="fa fa-link"></i>
						{l s='Hooks used' mod='additionalproductsorder'}
					</div>
					<div>
						<strong>{l s='List of hooks used' mod='additionalproductsorder'} :</strong>
						<ul style="margin-top:6px;">
							{foreach from=$lineven.apo.admin.datas.hooks_used item=hook}
								<li>
									{$hook|escape:'html':'UTF-8'}
								</li>
							{/foreach}
						</ul>
					</div>
				</div>
            {/block}
        {/if}
	</div>
</div>