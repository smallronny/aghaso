{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if isset($lineven_script_content)}
	<script type="text/javascript">
        {* SCRIPT *}
        {$lineven_script_content nofilter}
        {* /SCRIPT *}
	</script>
{/if}