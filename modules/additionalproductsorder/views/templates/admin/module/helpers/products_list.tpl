{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="lapo-associations-list-products">
    {foreach from=$products_list item=product name=product}
        {if $smarty.foreach.product.first}
            <div class="lapo-associations-list-products-first">
        {/if}
        {if $smarty.foreach.product.iteration == 2}
            <div class="lapo-associations-list-products-other" style="display:none;">
        {/if}
        {* HTML CONTENT for product details *}
        {$product nofilter}
        {* /HTML CONTENT *}
        {if $smarty.foreach.product.first || $smarty.foreach.product.last}
            </div>
        {/if}
        {if $smarty.foreach.product.last && count($products_list) > 1}
            <div class="lapo-associations-list-products-toggle-other">
                <a href="javascript:void(0)" class="lapo-associations-list-products-hide btn btn-default ">
                    <span><i class="fa fa-plus-square"></i>&nbsp;{l s='Show products list' mod='additionalproductsorder'} ({count($products_list)-1|intval} {l s='more' mod='additionalproductsorder'})</span>
                    <span style="display:none;"><i class="fa fa-minus-square"></i>&nbsp;{l s='Hide products list' mod='additionalproductsorder'}</span>
                </a>
            </div>
        {/if}
    {/foreach}
</div>
