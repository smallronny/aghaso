{*
 * Library for Lineven Prestashop Modules (Version 4.1.3)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="lapo-associations-list product-thumbnails">
    {* HTML CONTENT for product image *}
    {$product_thumbnail nofilter}
    {* /HTML CONTENT *}
    <span class="text">{$product_name|escape:'html':'UTF-8'}</span>
    {if isset($product_reference) && $product_reference != ''}
        <br/><span class="reference">{$product_reference|escape:'html':'UTF-8'}</span>
    {/if}
</div>
