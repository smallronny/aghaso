{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file='../_partials/dashboard/index.tpl'}

{block name="infos_content"}
    <p>
        <img src="{$lineven.apo.urls.base_url|escape:'html':'UTF-8'}/logo.png" class="pull-left" style="margin-right:10px;" id="{$lineven.apo.name|escape:'html':'UTF-8'}-logo" />
        {l s='The module "Additional Products on Order" allows you to Increase sales by display additional products (cross selling) during the order process.' mod='additionalproductsorder'}
        <br/>
        {l s='You will be able to create all associations as you want.' mod='additionalproductsorder'}
    </p>
{/block}
{block name="report_icon"}fa fa-tachometer{/block}
{block name="report_title"}
    {l s='Associations report' mod='additionalproductsorder'}
    <div class="panel-heading-action">
        <a class="list-toolbar-btn" href="{LinevenApoTools::getBackofficeURI('Associations', 'list', 'associations:list')|escape:'html':'UTF-8'}">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Associations list' mod='additionalproductsorder'}" data-html="true" data-placement="top">
							<i class="icon-fa-panel-heading fa fa-chain"></i>
						</span>
        </a>
        <a class="list-toolbar-btn" href="{LinevenApoTools::getBackofficeURI('Associations', 'addEdit', 'associations:list:add')|escape:'html':'UTF-8'}">
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Create an association' mod='additionalproductsorder'}" data-html="true" data-placement="top">
							<i class="process-icon-new"></i>
						</span>
        </a>
    </div>
{/block}
{block name="report_content"}
    <div class="additionalproductsorder-dashboard-report">
        {if $lineven.apo.admin.datas.associations_report_statistics && $lineven.apo.admin.datas.associations_report_statistics_display_graph}
            {if $lineven.apo.admin.datas.associations_report_statistics_is_displayed}
                <div style="width: {(99/$lineven.apo.admin.datas.associations_report_statistics_graph_count)|intval}%; height: 350px; border: 1px solid #ddd" id="lineven-template-content-dashboard-statistics-graph-displayed"></div>
            {/if}
            {if $lineven.apo.admin.datas.associations_report_statistics_is_add_to_cart}
                <div style="width: {(99/$lineven.apo.admin.datas.associations_report_statistics_graph_count)|intval}%; height: 350px; border: 1px solid #ddd" id="lineven-template-content-dashboard-statistics-graph-cart"></div>
            {/if}
            {if $lineven.apo.admin.datas.associations_report_statistics_is_click_to_view}
                <div style="width: {(99/$lineven.apo.admin.datas.associations_report_statistics_graph_count)|intval}%; height: 350px; border: 1px solid #ddd" id="lineven-template-content-dashboard-statistics-graph-view"></div>
            {/if}
            <div>{$lineven.apo.admin.datas.associations_report_statistics_display_today|escape:'html':'UTF-8'}</div>
            <div>{l s='Daily distribution based on monthly statistics' mod='additionalproductsorder'}&nbsp;({$lineven.apo.admin.datas.associations_report_statistics_display_month|escape:'html':'UTF-8'}).</div>
            <script>
                {if $lineven.apo.admin.datas.associations_report_statistics_is_displayed}
                $(document).ready(function() {
                    var data     = [{$lineven.apo.admin.datas.associations_report.day_percent_displayed|intval}, {$lineven.apo.admin.datas.associations_report.month_percent_displayed|intval}];
                    var labels   = ["{l s='Displayed today' mod='additionalproductsorder'}", "{l s='Displayed' mod='additionalproductsorder'}"];
                    for (var i=0; i< data.length; ++i) {
                        labels[i] = labels[i] + ' : ' + data[i] + '%';
                    }
                    new RGraph.SVG.Pie({
                        id: 'lineven-template-content-dashboard-statistics-graph-displayed',
                        data: data,
                        options: {
                            tooltipsEvent: 'mousemove',
                            highlightStyle: 'outline',
                            labelsSticksHlength: 50,
                            tooltips: labels,
                            key: ["{l s='Displayed' mod='additionalproductsorder'}" + " : " + data[0] + "%"],
                            colors: ['#7CB5EC', '#CCCCCC'],
                            strokestyle: 'white',
                            linewidth: 2,
                            shadow: true,
                            shadowOffsetx: 2,
                            shadowOffsety: 2,
                            shadowBlur: 3,
                            exploded: 7
                        }
                    }).draw();
                });
                {/if}
                {if $lineven.apo.admin.datas.associations_report_statistics_is_add_to_cart}
                $(document).ready(function() {
                    var data     = [{$lineven.apo.admin.datas.associations_report.day_percent_add_to_cart|intval}, {$lineven.apo.admin.datas.associations_report.month_percent_add_to_cart|intval}];
                    var labels   = ["{l s='Added to cart today' mod='additionalproductsorder'}", "{l s='Added to cart' mod='additionalproductsorder'}"];
                    for (var i=0; i< data.length; ++i) {
                        labels[i] = labels[i] + ' : ' + data[i] + '%';
                    }
                    new RGraph.SVG.Pie({
                        id: 'lineven-template-content-dashboard-statistics-graph-cart',
                        data: data,
                        options: {
                            tooltipsEvent: 'mousemove',
                            highlightStyle: 'outline',
                            labelsSticksHlength: 50,
                            tooltips: labels,
                            key: ["{l s='Added to cart' mod='additionalproductsorder'}" + " : " + data[0] + "%"],
                            colors: ['#A0D300', '#CCCCCC'],
                            strokestyle: 'white',
                            linewidth: 2,
                            shadow: true,
                            shadowOffsetx: 2,
                            shadowOffsety: 2,
                            shadowBlur: 3,
                            exploded: 7
                        }
                    }).draw();
                });
                {/if}
                {if $lineven.apo.admin.datas.associations_report_statistics_is_click_to_view}
                $(document).ready(function() {
                    var data     = [{$lineven.apo.admin.datas.associations_report.day_percent_click_to_view|intval}, {$lineven.apo.admin.datas.associations_report.month_percent_click_to_view|intval}];
                    var labels   = ["{l s='Viewed today' mod='additionalproductsorder'}", "{l s='Viewed' mod='additionalproductsorder'}"];
                    for (var i=0; i< data.length; ++i) {
                        labels[i] = labels[i] + ' : ' + data[i] + '%';
                    }
                    new RGraph.SVG.Pie({
                        id: 'lineven-template-content-dashboard-statistics-graph-view',
                        data: data,
                        options: {
                            tooltipsEvent: 'mousemove',
                            highlightStyle: 'outline',
                            labelsSticksHlength: 50,
                            tooltips: labels,
                            key: ["{l s='Viewed' mod='additionalproductsorder'}" + " : " + data[0] + "%"],
                            colors: ['#FFCD00', '#CCCCCC'],
                            strokestyle: 'white',
                            linewidth: 2,
                            shadow: true,
                            shadowOffsetx: 2,
                            shadowOffsety: 2,
                            shadowBlur: 3,
                            exploded: 7
                        }
                    }).draw();
                });
                {/if}
            </script>
            <br/>
        {/if}
        <table class="report_table" border="0">
            <tr>
                <td class="ltd-label">{l s='Total of associations for this shop' mod='additionalproductsorder'}</td>
                <td class="ltd-value" colspan="2">{$lineven.apo.admin.datas.associations_report.count|intval}</td>
            </tr>
            {if $lineven.apo.admin.datas.associations_report_statistics}
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td class="ltd-label">&nbsp;</td>
                    <td class="ltd-label ltd-label-column">{$lineven.apo.admin.datas.associations_report_statistics_display_month|escape:'html':'UTF-8'}</td>
                    <td class="ltd-label ltd-label-column">{l s='Today' mod='additionalproductsorder'}</td>
                </tr>
                {if $lineven.apo.admin.datas.associations_report_statistics_is_displayed}
                    <tr>
                        <td class="ltd-label">{l s='Number displayed products' mod='additionalproductsorder'}</td>
                        <td class="ltd-value">{$lineven.apo.admin.datas.associations_report.sum_displayed|intval}</td>
                        <td class="ltd-value">{$lineven.apo.admin.datas.associations_report.day_sum_displayed|intval}</td>
                    </tr>
                {/if}
                {if $lineven.apo.admin.datas.associations_report_statistics_is_add_to_cart}
                    <tr>
                        <td class="ltd-label">{l s='Number of products added to cart' mod='additionalproductsorder'}</td>
                        <td class="ltd-value">{$lineven.apo.admin.datas.associations_report.sum_add_to_cart|intval}</td>
                        <td class="ltd-value">{$lineven.apo.admin.datas.associations_report.day_sum_add_to_cart|intval}</td>
                    </tr>
                {/if}
                {if $lineven.apo.admin.datas.associations_report_statistics_is_click_to_view}
                    <tr>
                        <td class="ltd-label">{l s='Number viewed products' mod='additionalproductsorder'}</td>
                        <td class="ltd-value">{$lineven.apo.admin.datas.associations_report.sum_click_to_view|intval}</td>
                        <td class="ltd-value">{$lineven.apo.admin.datas.associations_report.day_sum_click_to_view|intval}</td>
                    </tr>
                {/if}
            {/if}
            {if $lineven.apo.admin.datas.report_addons_reviews_associated}
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td class="ltd-value" colspan="3" style="white-space: normal;">{l s='The module is associated to "Go Reviews - Reviews, Advices, Ratings, SEO and Google Rich Snippets" addons.' mod='additionalproductsorder'}</td>
                </tr>
            {/if}
        </table>
    </div>
{/block}