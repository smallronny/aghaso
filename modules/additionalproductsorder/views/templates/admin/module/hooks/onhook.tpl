{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file="./../../_configure/helpers/form/form.tpl"}

{block name="fieldset"}
	{if $f == 'behavior_cross_selling'}
		<div class="panel">
			<ul class="lineven-tabs-nav nav nav-tabs">
				<li id="tab-1" class="active">
	   				<a href="#additionalproductsorder-display-behavior" data-toggle="tab">{l s='Behavior' mod='additionalproductsorder'}</a>
				</li>
				<li id="tab-2">
					<a href="#additionalproductsorder-display-display" data-toggle="tab">{l s='Display' mod='additionalproductsorder'}</a>
				</li>
				<li id="tab-3">
					<a href="#additionalproductsorder-display-sort" data-toggle="tab">{l s='Sort display & research depth' mod='additionalproductsorder'}</a>
				</li>
			</ul>
			<div class="lineven-tabs-content tab-content">
				<!-- Activation settings -->
				<div id="additionalproductsorder-display-behavior" class="tab-pane active">
	{/if}
	{if $f == 'behavior_cross_selling' || $f == 'behavior_hook_settings' || $f == 'behavior_refresh'}
		{$smarty.block.parent}
	{/if}
	{if ($hook_code == 'OEP' && $f == 'behavior_hook_settings') || ($hook_code == 'OOP' &&  $f == 'behavior_refresh')}
		</div>
	{/if}

	{if $f == 'display_sections'}
		<div id="additionalproductsorder-display-display" class="tab-pane">
	{/if}
	{if $f == 'display_sections' || $f == 'display_settings' || $f == 'display_thumbnails_settings'}
		{$smarty.block.parent}
	{/if}
	{if $f == 'display_thumbnails_settings'}
		</div>
	{/if}

	{if $f == 'research_priority'}
		<div id="additionalproductsorder-display-sort" class="tab-pane">
	{/if}
	{if $f == 'research_priority' || $f == 'sort_display_settings' || $f == 'research_depth'}
		{$smarty.block.parent}
	{/if}
	{if $f == 'research_depth'}
				</div>
			</div>
		</div>
	{/if}

{/block}