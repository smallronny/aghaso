{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}
 <div id="lapo-statistics-clear-confirmation-message" class="module_confirmation conf confirm alert alert-success" style="display: none;">
	{l s='Your statistics have been reset.' mod='additionalproductsorder'}
</div>
<div id="lapo-statistics-clear-error-message" class="module_confirmation conf confirm alert alert-danger" style="display: none;">
	{l s='An error occured during statistics reset !' mod='additionalproductsorder'}
</div>
<div id="lapo-statistics-clear-alert-message" class="module_confirmation conf confirm alert alert-success" style="display: none;">
	{l s='Are you sure you want to clear statistics ?' mod='additionalproductsorder'}
</div>
{* HTML CONTENT *}
{$lineven.apo.admin.datas.statistics_form nofilter}
{* /HTML CONTENT *}
	<div class="panel" id="lapo-statistics-panel-settings" style="display:inherit;">
   	<div class="panel-heading">
     	<i class="fa fa-info"></i>
         {l s='Informations' mod='additionalproductsorder'}
         <div class="panel-heading-action" style="padding-right: 2px;">
				<button type="button" class="btn btn-default" onClick="LinevenApoModule.Statistics.clear()"><i class="fa fa-eraser"></i> {l s='Clear statistics' mod='additionalproductsorder'}</button>
			</div>
      </div>
      <p>
      	<span><strong><i class="fa fa-database"></i> {l s='Statistics in database' mod='additionalproductsorder'}</strong></span><br/>
      	<div>
      		{l s='Statistics are only stored for two month.' mod='additionalproductsorder'}<br/>
      		{l s='Passed this delay, statistics are automatically reset.' mod='additionalproductsorder'}<br/>
      	</div>
      </p>
   	<br/>
   	<p>
   		<span><strong><i class="fa fa-clock-o"></i> {l s='Clear statistics by cron task' mod='additionalproductsorder'}</strong></span><br/>
      	<div>
      		{l s='You can use a cron task to clear periodically statistics.' mod='additionalproductsorder'}<br/><br/>
			{if $lineven.apo.admin.datas.clean_cron_file != ''}
	            {l s='If you opt for a CRON rule, you have to set a CRON rule for each shop.' mod='additionalproductsorder'}<br/>
				<u>{l s='For this shop calls the file:' mod='additionalproductsorder'}</u>
                {$lineven.apo.admin.datas.clean_cron_file nofilter}
				{l s='every time you want to clear statistics.' mod='additionalproductsorder'}
			{else}
				{l s='If you opt for a CRON rule, you have to set a CRON rule for each shop.' mod='additionalproductsorder'}<br/>
				{l s='Select a shop in the prestashop dropdown list to get the file to call.' mod='additionalproductsorder'}
			{/if}
     	</div>
      </p>
	</div>
<script>
	{literal}
	$(function() {$('#module_form_submit_btn').show();});
	{/literal}
</script>