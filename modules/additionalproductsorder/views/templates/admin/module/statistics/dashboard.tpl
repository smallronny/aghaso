{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="panel" id="lapo-statistics-panel-settings" style="display:inherit;">
   	<div class="panel-heading">
        <i class="fa fa-pie-chart"></i>
        {$lineven.apo.admin.datas.month|escape:'html':'UTF-8'}&nbsp;{l s='details' mod='additionalproductsorder'}
    </div>
    <div style="width: 100%; height: 500px" id="chart-container"></div>
</div>
<script>
    $(document).ready(function() {
        var datas = new Array(new Array(), new Array(), new Array());
        var xaxisLabels = new Array();
        {for $nb_day=1 to $lineven.apo.admin.datas.days_in_month}
            {if isset($lineven.apo.admin.datas.statistics_graph.$nb_day)}
                datas[0].push({$lineven.apo.admin.datas.statistics_graph.$nb_day.sum_displayed|intval}); 
                datas[1].push({$lineven.apo.admin.datas.statistics_graph.$nb_day.sum_add_to_cart|intval});
                datas[2].push({$lineven.apo.admin.datas.statistics_graph.$nb_day.sum_click_to_view|intval});
            {else}
                datas[0].push(0); 
                datas[1].push(0); 
                datas[2].push(0); 
            {/if}
            xaxisLabels.push({$nb_day|intval});
        {/for}
        new RGraph.SVG.Line({
            id: 'chart-container',
            data: datas,
            options: {
                linewidth: 3,
                gutterLeft: 50,
                gutterBottom: 50,
                colors: ['#7CB5EC','#A0D300','#FFCD00'],
                key: ['{l s='Displayed' mod='additionalproductsorder'}', '{l s='In cart' mod='additionalproductsorder'}','{l s='Viewed' mod='additionalproductsorder'}'],
                xaxisLabels: xaxisLabels
            }
        }).draw();
    });
</script>