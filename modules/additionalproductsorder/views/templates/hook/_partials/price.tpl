{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if isset($lineven.apo.hook.datas.display_price) && $lineven.apo.hook.datas.display_price == '1' && $additional_product->show_price && !isset($restricted_country_mode) && !$configuration.is_catalog}
    {if $lineven.apo.hook.datas.price_display >= 0 && $lineven.apo.hook.datas.price_display <= 2}
        {assign var=has_discount value=false}
        {if isset($lineven.apo.hook.datas.display_reduction) && $lineven.apo.hook.datas.display_reduction == 1 && isset($additional_product->specific_price) && $additional_product->specific_price && isset($additional_product->specific_price.reduction) && $additional_product->specific_price.reduction > 0}
            {assign var=has_discount value=true}
        {/if}
        <div class="price product-price">
            <span class="price-reduction price">
                {if $has_discount}
                    <span class="old-price product-price regular-price">
                        {$product_price_without_reduction|escape:'html':'UTF-8'}
                    </span>
                {/if}
            </span>
            <span class="current-price{if $has_discount} has-discount{/if}">
                {$product_price|escape:'html':'UTF-8'}
                {if $lineven.apo.hook.datas.tax_enabled  && ((isset($configuration.display_taxes_label) && $configuration.display_taxes_label == 1) || !isset($configuration.display_taxes_label))}
                    {if $lineven.apo.hook.datas.price_display == 1} {l s='tax excl.' mod='additionalproductsorder'}{else} {l s='tax incl.' mod='additionalproductsorder'}{/if}
                {/if}
                {if $has_discount && $additional_product->specific_price.reduction_type == 'percentage'}
                    <span class="discount discount-percentage">-{$additional_product->specific_price.reduction * 100|intval}%</span>
                {/if}
            </span>
            {if $lineven.apo.hook.datas.price_display == 2}
                <span id="pretaxe-price">{strip}
                    <span id="pretaxe-price-display">{$product_price_tax_excluded|escape:'html':'UTF-8'}</span> {l s='tax excl.' mod='additionalproductsorder'}
                {/strip}</span>
            {/if}
        </div>
    {/if}
{/if}