{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<a href="{$additional_product->product_link|escape:'html':'UTF-8'}"
	title="{$additional_product->name|escape:'html':'UTF-8'}"
	class="lapo-view product-image"
    data-id-association="{$additional_product->id_association|intval}" data-id-product="{$additional_product->id|intval}"
	>
	<img src="{$additional_product->image_link|escape:'html':'UTF-8'}"
		 width="{$lineven.apo.hook.datas.image_width|escape:'html':'UTF-8'}"
		 height="{$lineven.apo.hook.datas.image_height|escape:'html':'UTF-8'}"
		 alt="{$additional_product->name|escape:'html':'UTF-8'}" />
</a>