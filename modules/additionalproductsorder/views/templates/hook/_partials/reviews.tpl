{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if isset($lineven.apo.hook.datas.partners_reviews) && $lineven.apo.hook.datas.partners_reviews == true}
    {if $lineven.apo.hook.datas.partners_reviews_module == 'homecomments' && isset($lineven.apo.hook.datas.partners_reviews_module_instance) && $lineven.apo.hook.datas.partners_reviews_module_instance != null}
        <div class="reviews">
            {* HTML CONTENT to display reviews stars from another partners modules *}
            {$lineven.apo.hook.datas.partners_reviews_module_instance->partnerDisplayAverageRate($additional_product->id, 'all', $additional_product) nofilter}
            {* /HTML CONTENT *}
        </div>
    {else}
        {if $lineven.apo.hook.datas.partners_reviews_module == 'productcomments'}
            <div class="hook-reviews">
                <div class="lapo-product-list-reviews-ps_productcomments_{$additional_product->id|intval} product-list-reviews" data-comment-grade="{$additional_product->prestashop_product_comment_average_grade|intval}"  data-comment-nb="{$additional_product->prestashop_product_comment_nb|intval}">
                    <div class="grade-stars small-stars"></div>
                    <div class="comments-nb"></div>
                    {if $additional_product->prestashop_product_comment_nb !=0}
                        <div itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating" itemscope>
                            <meta itemprop="reviewCount" content="{$additional_product->prestashop_product_comment_nb|intval}" />
                            <meta itemprop="ratingValue" content="{$additional_product->prestashop_product_comment_average_grade|intval}" />
                        </div>
                        <script>
                            if (typeof jQuery == 'function') {
                                AdditionalProductsOrder.ProductsListComments.displayPrestashopProductCommentsForClassic({$additional_product->id|intval}, {$additional_product->prestashop_product_comment_average_grade|intval}, {$additional_product->prestashop_product_comment_nb|intval});
                            } else {
                                document.addEventListener("DOMContentLoaded", function() {
                                    window.setTimeout(function() { AdditionalProductsOrder.ProductsListComments.displayPrestashopProductCommentsForClassic({$additional_product->id|intval}, {$additional_product->prestashop_product_comment_average_grade|intval}, {$additional_product->prestashop_product_comment_nb|intval}); }, 80);
                                });
                            }
                        </script>
                    {/if}
                </div>
            </div>
        {else}
            {hook h='displayProductListReviews' product=$additional_product->product_lazy_array}
        {/if}
    {/if}
{/if}
