{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if !isset($truncate)}
    {assign var=truncate value=65}
{/if}

<div class="product-description">
    <a class="lapo-view" href="{$additional_product->product_link|escape:'html'}" title="{$additional_product->name|escape:'htmlall':'UTF-8'}" data-id-association="{$additional_product->id_association|intval}" data-id-product="{$additional_product->id|intval}">
        <span>{$additional_product->description_short|strip_tags|truncate:$truncate:'...' nofilter}</span>
    </a>
</div>