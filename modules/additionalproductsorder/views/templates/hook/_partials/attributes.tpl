{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $additional_product->has_attributes && $lineven.apo.hook.datas.combinations_all && isset($product_attribute_designation)}
	<div class="product-attribute-designation">
        {$product_attribute_designation|escape:'html':'UTF-8'}
	</div>
{/if}