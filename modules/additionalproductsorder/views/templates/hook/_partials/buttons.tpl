{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if !isset($button_rule)}
    {assign var=button_rule value="display_button_cart"}
{/if}
{if !isset($button_icon)}
    {assign var=button_icon value=false}
{/if}

{if ($additional_product->has_attributes == 0 OR (isset($lineven.apo.hook.datas.combinations_add_to_cart) && ($lineven.apo.hook.datas.combinations_add_to_cart == 1)))
    && $additional_product->available_for_order && !isset($restricted_country_mode)
    && ($additional_product->quantity > 0 OR ($additional_product->allow_oosp && $additional_product->quantity <= 0)) && $additional_product->minimal_quantity <= 1
    && $additional_product->customizable == false
    && !$configuration.is_catalog}
    {if isset($lineven.apo.hook.datas.display_cart_button) && $lineven.apo.hook.datas.display_cart_button == '1'}
        {if $button_rule == 'display_button_cart' || $button_rule == 'display_config'}
            <div class="buttons">
                {if isset($lineven.apo.hook.datas.ajax_put_cart) && $lineven.apo.hook.datas.ajax_put_cart}
                    <a class="lapo-add-to-cart lapo-add-to-cart-stats btn btn-primary add-to-cart" data-button-action="add-to-cart" href="{$lineven.apo.hook.datas.link->getPageLink('cart', {$lineven.prestashop.is_ssl|intval}, NULL, "add=1&amp;id_product={$additional_product->id|intval}&amp;id_product_attribute={$id_product_attribute|intval}&amp;token={$lineven.apo.hook.datas.static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='additionalproductsorder'}" data-id-association="{$additional_product->id_association|intval}" data-id-product="{$additional_product->id|intval}" data-id-product-attribute="{$id_product_attribute|intval}" data-product-attribute="{$id_product_attribute|intval}">
                        <span>{if $button_icon}<i class="material-icons">add_shopping_cart</i>{else}{l s='Add to cart' mod='additionalproductsorder'}{/if}</span>
                    </a>
                {else}
                    <a class="lapo-add-to-cart lapo-add-to-cart-stats btn btn-primary" href="{$lineven.apo.hook.datas.link->getPageLink('cart', {$lineven.prestashop.is_ssl|intval}, NULL, "add=1&amp;id_product={$additional_product->id|intval}&amp;id_product_attribute={$id_product_attribute|intval}&amp;token={$lineven.apo.hook.datas.static_token}", false)|escape:'html':'UTF-8'}" data-id-association="{$additional_product->id_association|intval}" rel="nofollow" title="{l s='Add to cart' mod='additionalproductsorder'}" data-id-product="{$additional_product->id|intval}" data-id-product-attribute="{$id_product_attribute|intval}" data-product-attribute="{$id_product_attribute|intval}">
                        <span>{if $button_icon}<i class="material-icons">add_shopping_cart</i>{else}{l s='Add to cart' mod='additionalproductsorder'}{/if}</span>
                    </a>
                {/if}
            </div>
        {/if}
    {else}
        {if isset($lineven.apo.hook.datas.display_cart_checkbox) && $lineven.apo.hook.datas.display_cart_checkbox}
            {if $button_rule == 'display_checkbox_cart' || $button_rule == 'display_config'}
                <div class="lapo-cart-checkbox">
                    <input id="lapo-add-to-cart-checkbox-{$additional_product->id|intval}-{$id_product_attribute|intval}" name="lapo-add-to-cart-checkbox[]" class="checkbox lapo-add-to-cart-checkbox" type="checkbox" {if isset($lineven.apo.hook.datas.display_cart_checkbox_icon) && $lineven.apo.hook.datas.display_cart_checkbox_icon}style="display:none;"{/if}
                           data-add-to-cart-url="{$lineven.apo.hook.datas.link->getPageLink('cart', {$lineven.prestashop.is_ssl|intval}, NULL, "add=1&amp;id_product={$additional_product->id|intval}&amp;id_product_attribute={$id_product_attribute|intval}&amp;token={$lineven.apo.hook.datas.static_token}", false)|escape:'html':'UTF-8'}"
                           data-id-association="{$additional_product->id_association|intval}"
                           data-id-product="{$additional_product->id|intval}"
                           data-id-product-attribute="{$id_product_attribute|intval}"
                           data-product-attribute="{$id_product_attribute|intval}">
                    {if isset($lineven.apo.hook.datas.display_cart_checkbox_icon) && $lineven.apo.hook.datas.display_cart_checkbox_icon}
                        <a class="lapo-add-to-cart lapo-add-to-cart-button-check btn btn-primary" href="javascript:void(0)" rel="nofollow" title="{l s='Add to cart' mod='additionalproductsorder'}" data-id-product="{$additional_product->id|intval}" data-id-product-attribute="{$id_product_attribute|intval}" data-product-attribute="{$id_product_attribute|intval}">
                            <span><i class="material-icons">add_shopping_cart</i></span>
                        </a>
                    {/if}
                </div>
            {/if}
        {else}
            <div class="buttons"></div>
        {/if}
    {/if}
{else}
    {if $button_rule == 'display_config' || $button_rule == 'display_button_cart'}
        <div class="buttons">
            <a class="lapo-view btn btn-info" href="{$additional_product->product_link|escape:'html':'UTF-8'}" title="{l s='View' mod='additionalproductsorder'}" data-id-association="{$additional_product->id_association|intval}" data-id-product="{$additional_product->id|intval}">
			<span>
				{if ($additional_product->has_attributes == 1 && isset($lineven.apo.hook.datas.combinations_add_to_cart) && !$lineven.apo.hook.datas.combinations_add_to_cart) || $additional_product->customizable}
                    {if $button_icon}<i class="material-icons">remove_red_eye</i>{else}{l s='Customize' mod='additionalproductsorder'}{/if}
                {else}
                    {if $button_icon}<i class="material-icons">remove_red_eye</i>{else}{l s='More' mod='additionalproductsorder'}{/if}
                {/if}
			</span>
            </a>
        </div>
    {/if}
{/if}