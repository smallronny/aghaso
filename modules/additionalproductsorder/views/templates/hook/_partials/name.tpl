{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if !isset($truncate)}
	{assign var=truncate value=50}
{/if}

<div class="product-name">
	<a class="lapo-view" href="{$additional_product->product_link|escape:'html':'UTF-8'}" title="{$additional_product->name|escape:'htmlall':'UTF-8'}"
	   data-id-association="{$additional_product->id_association|intval}" data-id-product="{$additional_product->id|intval}">
		<span>{$additional_product->name|strip_tags|escape:'html':'UTF-8'|truncate:$truncate:'...'}</span>
	</a>
</div>