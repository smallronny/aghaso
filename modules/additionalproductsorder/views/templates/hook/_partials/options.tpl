{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $lineven.apo.hook.datas.combinations_all == 0 && $lineven.apo.hook.datas.combinations_display_options && $additional_product->has_attributes == 1}
	<div class="product-options">
        {l s='This product is available with options.' mod='additionalproductsorder'}
	</div>
{/if}