{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

<div class="lapo-tooltip-content" data-id-product="{$additional_product->id|intval}"
    data-id-product-attribute="{if isset($additional_product->id_product_attribute)}{$additional_product->id_product_attribute|intval}{else}0{/if}">
    <div id="lapo-tooltip-product-{$additional_product->id|intval}-{if isset($additional_product->id_product_attribute)}{$additional_product->id_product_attribute|intval}{else}0{/if}"
        class="lapo-tooltip-content-product">
        <div class="lapo-tooltip-product-image">
            <img src="{$additional_product->image_link|escape:'html':'UTF-8'}"
                 width="{$lineven.apo.hook.datas.image_width|escape:'html':'UTF-8'}"
                 height="{$lineven.apo.hook.datas.image_height|escape:'html':'UTF-8'}"
                 alt="{$additional_product->name|escape:'html':'UTF-8'}" />
        </div>
        <div class="lapo-tooltip-product-description">
            <span>{$additional_product->description_short|strip_tags nofilter}</span>
        </div>
        {if $additional_product->has_attributes && $lineven.apo.hook.datas.combinations_all && isset($product_attribute_designation)}
            <div class="lapo-tooltip-product-attribute-designation">
                {$product_attribute_designation|escape:'html':'UTF-8'}
            </div>
        {/if}
    </div>
</div>
