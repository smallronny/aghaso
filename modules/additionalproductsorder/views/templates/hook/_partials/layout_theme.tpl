{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $lineven.apo.is_active == true}
    {if !$lineven.apo.hook.datas.is_refresh}
        <section class="lineven-additionalproductsorder">
    {/if}
    {if isset($lineven.apo.hook.datas.sections) && $lineven.apo.hook.datas.sections}
        {assign var=section_title value={$lineven.apo.hook.datas.title}}
        {block name="lapo_before_sections"}{/block}
            {foreach from=$lineven.apo.hook.datas.sections key=section_key item=section name=section}
                {if $lineven.apo.hook.datas.is_separate_results}
                    {assign var=section_title value={$section.title}}
                {/if}
                {if $section.products && count($section.products)}
                    <section class="lineven-additionalproductsorder-section">
                        <div class="apo-{$lineven.apo.hook.datas.hook_class_name|escape:'htmlall':'UTF-8'}">
                            {block name="lapo_header"}
                                <div class="{$lineven.apo.hook.datas.template_class_name|escape:'htmlall':'UTF-8'}">
                            {/block}
                            {block name="lapo_title"}{/block}
                            {block name="lapo_content_header"}{/block}
                            {block name="lapo_content"}
                                {foreach from=$section.products item=additional_product name=additional_product key="position"}
                                    {block name="lapo_product"}{/block}
                                {/foreach}
                            {/block}
                            {block name="lapo_content_footer"}{/block}
                            {block name="lapo_footer"}
                                </div>
                            {/block}
                        </div>
                    </section>
                {/if}
            {/foreach}
        {block name="lapo_after_sections"}{/block}
    {/if}
    {if !$lineven.apo.hook.datas.is_refresh}
        </section>
    {/if}
{/if}