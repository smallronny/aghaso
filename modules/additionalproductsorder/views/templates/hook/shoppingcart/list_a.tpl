{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file='module:additionalproductsorder/views/templates/hook/_partials/layout.tpl'}

{block name="lapo_header"}
	<div id="products" class="card list-a">
{/block}
{block name="lapo_title"}
	<div class="card-block">
		<div class="h1 header-title">{$section_title|escape:'htmlall':'UTF-8'}</div>
	</div>
	<hr class="header-hr"/>
{/block}
{block name="lapo_content_header"}
	<ul class="apo-products">
{/block}
{block name="lapo_product"}
	<li class="apo-product">
        {include file="module:additionalproductsorder/views/templates/hook/_partials/image.tpl"}
		<div class="container-description">
            {include file="module:additionalproductsorder/views/templates/hook/_partials/name.tpl"}
            {include file="module:additionalproductsorder/views/templates/hook/_partials/options.tpl"}
            {include file="module:additionalproductsorder/views/templates/hook/_partials/attributes.tpl"}
            {include file="module:additionalproductsorder/views/templates/hook/_partials/reviews.tpl" class_name="list"}
            {include file="module:additionalproductsorder/views/templates/hook/_partials/description.tpl"}
            {include file="module:additionalproductsorder/views/templates/hook/_partials/price.tpl"}
		</div>
        {include file="module:additionalproductsorder/views/templates/hook/_partials/buttons.tpl"}
		<br style="clear:both;" />
	</li>
{/block}

{block name="lapo_content_footer"}
	</ul>
	<br class="clear"/>
{/block}
{block name="lapo_footer"}
	</div>
{/block}