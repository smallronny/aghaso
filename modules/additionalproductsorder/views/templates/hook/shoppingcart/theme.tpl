{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file='module:additionalproductsorder/views/templates/hook/_partials/layout_theme.tpl'}

{block name="lapo_header"}
	<div class="theme">
{/block}
{block name="lapo_title"}
	<div class="card-block">
		<div class="h1 header-title">{$section_title|escape:'htmlall':'UTF-8'}</div>
	</div>
	<hr class="header-hr"/>
{/block}
{block name="lapo_content_header"}
	<section id="products">
		<div class="products">
{/block}
{block name="lapo_product"}
	{include file="catalog/_partials/miniatures/product.tpl" product=$additional_product position=$position}
{/block}
{block name="lapo_content_footer"}
		</div>
	</section>
{/block}
{block name="lapo_footer"}
	</div>
{/block}
