{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file='module:additionalproductsorder/views/templates/hook/_partials/layout.tpl'}

{block name="lapo_header"}
	<div id="products" class="card thumbnails">
{/block}
{block name="lapo_title"}
	<div class="card-block">
		<div class="h1 header-title">{$section_title|escape:'htmlall':'UTF-8'}</div>
	</div>
	<hr class="header-hr"/>
{/block}
{block name="lapo_content_header"}
	<ul class="apo-products">
{/block}
{block name="lapo_product"}
	{assign var=thumbnail_image_class value='product-image'}
	{if version_compare($lineven.prestashop.version, '1.7.6', '>=')}
		{assign var=thumbnail_image_class value='product-image product-thumbnail'}
	{/if}
	<li class="apo-product">
        {include file="module:additionalproductsorder/views/templates/hook/_partials/image.tpl" image_class=$thumbnail_image_class}
		<br/>
        {include file="module:additionalproductsorder/views/templates/hook/_partials/reviews.tpl" class_name="thumbnails"}
        {if ((isset($lineven.apo.hook.datas.display_title) && $lineven.apo.hook.datas.display_title == '1') || (isset($lineven.apo.hook.datas.display_description) && $lineven.apo.hook.datas.display_description == '1'))}
			<div class="container-description"  {if !$lineven.apo.hook.datas.is_mobile}style="width:{$lineven.apo.hook.datas.image_width|escape:'htmlall':'UTF-8'}px;height:{$lineven.apo.hook.datas.description_height|escape:'htmlall':'UTF-8'}px;"{/if}>
                {if (isset($lineven.apo.hook.datas.display_title) && $lineven.apo.hook.datas.display_title == '1')}
                    {include file="module:additionalproductsorder/views/templates/hook/_partials/name.tpl"}
                {/if}
                {include file="module:additionalproductsorder/views/templates/hook/_partials/attributes.tpl"}
                {if (isset($lineven.apo.hook.datas.display_description) && $lineven.apo.hook.datas.display_description == '1')}
                    {include file="module:additionalproductsorder/views/templates/hook/_partials/description.tpl"}
                {/if}
			</div>
        {/if}
		<div style="min-height: 60px;">
			{include file="module:additionalproductsorder/views/templates/hook/_partials/price.tpl"}
			{include file="module:additionalproductsorder/views/templates/hook/_partials/options.tpl"}
		</div>
        {include file="module:additionalproductsorder/views/templates/hook/_partials/buttons.tpl"}
	</li>
{/block}

{block name="lapo_content_footer"}
	</ul>
	<br class="clear"/>
{/block}
{block name="lapo_footer"}
	</div>
{/block}