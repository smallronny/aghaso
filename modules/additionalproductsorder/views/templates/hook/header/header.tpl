{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{if $lineven.apo.is_active == true}
	{if count($lineven.apo.hook.datas.assets)}
		<style>
			{foreach from=$lineven.apo.hook.datas.assets item=asset name=asset}
			{$asset|escape:'htmlall':'UTF-8'}
			{/foreach}
		</style>
	{/if}
{/if}
