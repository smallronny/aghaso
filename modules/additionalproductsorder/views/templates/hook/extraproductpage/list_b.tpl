{*
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 *}

{extends file='module:additionalproductsorder/views/templates/hook/_partials/layout.tpl'}

{block name="lapo_header"}
	<div id="products" class="card list-b">
{/block}
{block name="lapo_title"}
	<div class="card-block">
		<div class="h5 header-title">{$section_title|escape:'htmlall':'UTF-8'}</div>
	</div>
{/block}
{block name="lapo_content_header"}
	<div class="apo-products">
{/block}
{block name="lapo_product"}
	<div class="row apo-product">
		<div class="col-md-11 col-xs-12 product-container lapo-row-col">
			{include file="module:additionalproductsorder/views/templates/hook/_partials/tooltip.tpl"}
			<div class="col-md-3 col-xs-12 lapo-row-col">
				{include file="module:additionalproductsorder/views/templates/hook/_partials/image.tpl"}
			</div>
			<div class="col-md-9 col-xs-12 lapo-row-col">
				<div class="container-description">
					{include file="module:additionalproductsorder/views/templates/hook/_partials/name.tpl"}
					{include file="module:additionalproductsorder/views/templates/hook/_partials/options.tpl"}
					{include file="module:additionalproductsorder/views/templates/hook/_partials/attributes.tpl"}
					{include file="module:additionalproductsorder/views/templates/hook/_partials/reviews.tpl" class_name="list"}
					{include file="module:additionalproductsorder/views/templates/hook/_partials/description.tpl" truncate=150}
					{include file="module:additionalproductsorder/views/templates/hook/_partials/price.tpl"}
				</div>
			</div>
		</div>
		<div class="col-md-1 col-xs-12 lapo-row-col">
        	{include file="module:additionalproductsorder/views/templates/hook/_partials/buttons.tpl" button_icon=true button_rule="display_config"}
		</div>
	</div>
{/block}

{block name="lapo_content_footer"}
	</div>
{/block}
{block name="lapo_footer"}
	</div>
{/block}