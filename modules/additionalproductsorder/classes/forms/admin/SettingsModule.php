<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminSettingsModule extends LinevenApoForm
{

    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $this->fields['activation'] = array(
            'form' => array(
            'legend' => array(
                    'title' => $this->translator->l('Activation', 'SettingsModule'),
                    'icon' => 'fa fa-bolt'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Activate module', 'SettingsModule'),
                        'name' => 'active_module',
                        'configuration_name' => 'LINEVEN_APO_ACTIVE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_module_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'SettingsModule')
                            ),
                            array(
                                'id' => 'active_module_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'SettingsModule')
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SettingsModule'),
                )
            ),
        );
        $this->fields['statistics'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Statistics', 'SettingsModule'),
                    'icon' => 'fa fa-pie-chart'
                ),
                'description_multilines' => array(
                    LinevenApoTools::displayHtmlInCode('icon', 'fa fa-fire').
                    $this->translator->l('Use statistics require more computation by the module.', 'SettingsModule'),
                    $this->translator->l('Requires the storage of cookies on your customer\'s browser.', 'SettingsModule')
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Activate statistics', 'SettingsModule'),
                        'name' => 'active_statistics',
                        'configuration_name' => 'LINEVEN_APO_STATS_ACTIVE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_statistics_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'SettingsModule')
                            ),
                            array(
                                'id' => 'active_statistics_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'SettingsModule')
                            )
                        ),
                    ),
                    array(
                        'type' => 'html',
                        'name' => 'text_info_1',
                        'specific' => 'alert',
                        'alert_type' => 'info',
                        'html_content' => $this->translator->l('For statistics export in CSV format, you can choose delimiter and enclosure.', 'SettingsModule'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Fields delimter', 'SettingsModule'),
                        'name' => 'export_fields_delimiter',
                        'configuration_name' => 'LINEVEN_APO_STATS_EXP_DELIMTER',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => AdditionalProductsOrder::$export_delimiter_comma,
                                    'name' => ','
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$export_delimiter_semicolon,
                                    'name' => ';'
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('String enclosure', 'SettingsModule'),
                        'name' => 'export_string_enclosure',
                        'configuration_name' => 'LINEVEN_APO_STATS_EXP_ENCLOSURE',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => AdditionalProductsOrder::$export_enclosure_quote,
                                    'name' => '\''
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$export_enclosure_double_quote,
                                    'name' => '"'
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SettingsModule'),
                )
            ),
        );
        $this->fields['backoffice_menu'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('In the backoffice menu', 'SettingsModule'),
                    'icon' => 'fa fa-bars'
                ),
                'description' => $this->translator->l('You can activate the backoffice menu to manage your products without passing through the module.', 'SettingsModule'),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Activate backoffice menu', 'SettingsModule'),
                        'name' => 'active_bo_menu',
                        'configuration_name' => 'LINEVEN_APO_OOP_ACTIVE_BO_MENU',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_bo_menu_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'SettingsModule')
                            ),
                            array(
                                'id' => 'active_bo_menu_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'SettingsModule')
                            ),
                        ),
                        'desc' => $this->translator->l('You can manage products in "Catalog > Additional Products".', 'SettingsModule')
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SettingsModule'),
                )
            ),
        );
        $this->fields['preferences'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Preferences', 'SettingsModule'),
                    'icon' => 'fa fa-eye'
                ),
                'description' => $this->translator->l('You can set preferences for module administration.', 'SettingsModule'),
                'input' => array(
                    array(
                        'type' => 'checkbox',
                        'name' => 'bo_list_filter',
                        'configuration_name' => 'LINEVEN_APO_BO_LIST_FILTER',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'on',
                                    'name' => $this->translator->l('Show filters in associations list.', 'SettingsModule'),
                                    'val' => '1'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SettingsModule'),
                )
            ),
        );
    }

    /**
     * Update settings
     *
     * @return void
     */
    public function updateSettings()
    {
        parent::updateSettings();
        // Admin tab management
        if (Tools::isSubmit('active_bo_menu')) {
            LinevenApoTabs::manage(
                Tools::getValue('active_bo_menu')
            );
        }
    }
}
