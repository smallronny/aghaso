<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminToolsDesign extends LinevenApoForm
{

    private $default_design_content = '';

    /**
     * Constructor.
     * @param string $default_design_content Default design content
     */
    public function __construct($default_design_content = '')
    {
        $this->default_design_content = $default_design_content;
        parent::__construct();
    }

    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $design_display_mode_value = AdditionalProductsOrder::$template_standard_list_a;
        if (array_key_exists(
            Configuration::get('LINEVEN_APO_OOP_DISPLAY_MODE'),
            AdditionalProductsOrder::$native_templates
        )) {
            $design_display_mode_value = Configuration::get('LINEVEN_APO_OOP_DISPLAY_MODE');
        }
        $this->fields[0] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('CSS', 'ToolsDesign'),
                    'icon' => 'fa fa-align-justify'
                ),
                'description_multilines' => array(
                    $this->translator->l('You can work on the design of the module displayed to your customers.', 'ToolsDesign'),
                    $this->translator->l('CSS reserved for experts.', 'ToolsDesign'),
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Use default design', 'ToolsDesign'),
                        'name' => 'active_default_design',
                        'configuration_name' => 'LINEVEN_APO_DEFAULT_DESIGN',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_default_design_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'ToolsDesign')
                            ),
                            array(
                                'id' => 'active_default_design_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'ToolsDesign')
                            )
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('CSS for', 'ToolsDesign'),
                        'name' => 'design_display_mode',
                        'value' => $design_display_mode_value,
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => AdditionalProductsOrder::$template_standard_classic,
                                    'name' => $this->translator->l('Classic', 'ToolsDesign')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$template_standard_list_a,
                                    'name' => $this->translator->l('List (template A)', 'ToolsDesign')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$template_standard_list_b,
                                    'name' => $this->translator->l('List (template B)', 'ToolsDesign')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$template_standard_thumbnails,
                                    'name' => $this->translator->l('Thumbnails', 'ToolsDesign')
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->translator->l('Description', 'ToolsDesign'),
                        'name' => 'design_textarea',
                        'rows' => 20,
                        'cols' => 60,
                        'value' => $this->default_design_content
                    )
                ),
                'buttons' => array(
                    'reload_styles' => array(
                        'title' => $this->translator->l('Reload defaults styles', 'ToolsDesign'),
                        'class' => 'pull-right',
                        'id' => 'reload_styles',
                        'icon' => 'process-icon-refresh',
                        'href' => '#'
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'ToolsDesign'),
                )
            ),
        );
    }

    /**
     * Validate.
     * @return boolean
     */
    public function validate()
    {
        if ((int)Tools::getValue('active_default_design') == 0) {
            if (trim(Tools::strlen(Tools::getValue('design_display_mode'))) == 0) {
                $this->addError(
                    $this->translator->l('You must select a display mode for the design.', 'ToolsDesign')
                );
            }
            if (trim(Tools::strlen(Tools::getValue('design_textarea'))) == 0) {
                $this->addError(
                    $this->translator->l('The text box contains no design. You must define the design of the module.', 'ToolsDesign')
                );
            }
        }
        return parent::validate();
    }

    /**
     * Update settings
     *
     * @return void
     */
    public function updateSettings()
    {
        parent::updateSettings();
        if ((int)Tools::getValue('active_default_design') == 0) {
            Configuration::updateValue(
                'LINEVEN_APO_DESIGN_'.Tools::getValue('design_display_mode'),
                Tools::getValue('design_textarea')
            );
        }
        Configuration::updateValue('LINEVEN_APO_DEFAULT_DESIGN', (int)Tools::getValue('active_default_design'));
    }
}
