<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminStatistics extends LinevenApoForm
{

    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $this->fields['activation'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Activation', 'Statistics'),
                    'icon' => 'fa fa-bolt'
                ),
                'description_multilines' => array(
                    LinevenApoTools::displayHtmlInCode('icon', 'fa fa-fire').
                    $this->translator->l('Use statistics require more computation by the module.', 'Statistics'),
                    $this->translator->l('Requires the storage of cookies on your customer\'s browser.', 'Statistics')
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Activate statistics', 'Statistics'),
                        'name' => 'active_statistics',
                        'configuration_name' => 'LINEVEN_APO_STATS_ACTIVE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_statistics_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'Statistics')
                            ),
                            array(
                                'id' => 'active_statistics_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'Statistics')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Count displayed products', 'Statistics'),
                        'name' => 'active_is_displayed',
                        'configuration_name' => 'LINEVEN_APO_STATS_IS_DISPLAYED',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_is_displayed_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'Statistics')
                            ),
                            array(
                                'id' => 'active_is_displayed_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'Statistics')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Count add to cart', 'Statistics'),
                        'name' => 'active_add_to_cart',
                        'configuration_name' => 'LINEVEN_APO_STATS_ADD_CART',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_add_to_cart_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'Statistics')
                            ),
                            array(
                                'id' => 'active_add_to_cart_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'Statistics')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Count viewed', 'Statistics'),
                        'name' => 'active_viewed',
                        'configuration_name' => 'LINEVEN_APO_STATS_IS_VIEWED',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_viewed_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'Statistics')
                            ),
                            array(
                                'id' => 'active_viewed_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'Statistics')
                            )
                        )
                    ),
                    array(
                        'type' => 'html',
                        'name' => 'text_info_1',
                        'specific' => 'alert',
                        'alert_type' => 'info',
                        'html_content' => array(
                            $this->translator->l('For displayed product, you can choose to count each time displayed products or count only once the same product for a given customer during the session.', 'Statistics'),
                            $this->translator->l('Count all displayed products each time requires more server and database resources.', 'Statistics')
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Count all products displayed', 'Statistics'),
                        'name' => 'active_dispmayed_all',
                        'configuration_name' => 'LINEVEN_APO_STATS_ALL_DISPLAYED',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_dispmayed_all_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'Statistics')
                            ),
                            array(
                                'id' => 'active_dispmayed_all_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'Statistics')
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'Statistics'),
                )
            ),
        );
    }
}
