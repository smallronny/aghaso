<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminAssociatedModulesReviews extends LinevenApoForm
{
    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $is_lineven_module = LinevenApoPartners::verifyModule(AdditionalProductsOrder::$partner_reviews_module_lineven);
        $is_prestashop_module = LinevenApoPartners::verifyModule(AdditionalProductsOrder::$partner_reviews_module_prestashop);
        $modules_select = array();
        if ($is_lineven_module) {
            $modules_select[] = array(
                'id' => AdditionalProductsOrder::$partner_reviews_module_lineven,
                'name' => $this->translator->l('Go Reviews - Reviews, Advices, Ratings, SEO and Google Rich Snippets', 'AssociatedModulesReviews')
            );
        }
        if ($is_prestashop_module) {
            $modules_select[] = array(
                'id' => AdditionalProductsOrder::$partner_reviews_module_prestashop,
                'name' => $this->translator->l('Prestashop - Product Comments Module', 'AssociatedModulesReviews')
            );
        }
        $modules_select[] = array(
            'id' => AdditionalProductsOrder::$partner_reviews_module_hook,
            'name' => $this->translator->l('Use the Hook \'displayProductListReviews\'', 'AssociatedModulesReviews')
        );

        $this->fields[0] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Associated reviews module', 'AssociatedModulesReviews'),
                    'icon' => 'fa fa-comments'
                ),
                'description_multilines' =>  array(
                    $this->translator->l('To display average score for each products displayed, you have the possibility to associate reviews modules to this module.', 'AssociatedModulesReviews'),
                    $this->translator->l('This module is compatible with :', 'AssociatedModulesReviews'),
                    $this->translator->l(' - Go Reviews - Reviews, Advices, Ratings, SEO and Google Rich Snippets Module', 'AssociatedModulesReviews'),
                    $this->translator->l(' - Prestashop - Product Comments Module', 'AssociatedModulesReviews'),
                    $this->translator->l(' - Standard hook \'displayProductListReviews\' (if your reviews module use it to display average in lists)', 'AssociatedModulesReviews'),
                    $this->translator->l('For the homepage products list, or if you choose Prestashop Theme, the reviews average will be display according your global settings of your shop.', 'AssociatedModulesReviews')
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Display average note', 'AssociatedModulesReviews'),
                        'name' => 'active_reviews_module',
                        'configuration_name' => 'LINEVEN_APO_PARTNER_RVW_RATE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_reviews_module_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'AssociatedModulesReviews')
                            ),
                            array(
                                'id' => 'active_reviews_module_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'AssociatedModulesReviews')
                            )
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Choose the module/hook to use', 'AssociatedModulesReviews'),
                        'name' => 'witch_reviews_module',
                        'class' => 'fixed-width-xxl',
                        'configuration_name' => 'LINEVEN_APO_PARTNER_RVW_MODULE',
                        'options' => array(
                            'query' => $modules_select,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'AssociatedModulesReviews'),
                )
            )
        );
    }
}
