<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminToolsCustomCss extends LinevenApoForm
{
    private $css_file;
    private $css_file_path;
    private $is_file_exists;
    private $css_content;
    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->css_file = $configuration->name.'/views/css/additionalproductsorder-custom.css';
        $this->css_file_path = _PS_MODULE_DIR_.$this->css_file;
        $this->css_content = '';
        $this->is_file_exists = true;
        if (!file_exists(_PS_THEME_DIR_.'modules/'.$this->css_file)) {
            if (!file_exists($this->css_file_path)) {
                $this->is_file_exists = false;
            }
        } else {
            $this->css_file_path = _PS_THEME_DIR_.'modules/'.$this->css_file;
        }
        if ($this->is_file_exists) {
            $this->css_content = Tools::file_get_contents($this->css_file_path);
        }
        parent::__construct();
    }

    /**
     * Init.
     * @return void
     */
    public function init()
    {

        $this->fields[0] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Custom CSS', 'ToolsCustomCss'),
                    'icon' => 'fa fa-css3'
                ),
                'description' => $this->translator->l('You can define custom CSS to apply to the module.', 'ToolsCustomCss'),
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'label' => $this->translator->l('CSS', 'ToolsCustomCss'),
                        'name' => 'custom_css',
                        'rows' => 30,
                        'cols' => 60,
                        'value' => $this->css_content
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'ToolsCustomCss'),
                )
            ),
        );
    }

    /**
     * Update settings
     *
     * @return void
     */
    public function updateSettings()
    {
        parent::updateSettings();
        if (Tools::isSubmit('custom_css')) {
            if (Tools::strlen(trim(Tools::getValue('custom_css'))) != 0) {
                file_put_contents($this->css_file_path, trim(Tools::getValue('custom_css')));
            } else {
                Tools::deleteFile($this->css_file_path, trim(Tools::getValue('custom_css')));
            }
        }
    }
}
