<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminSettingsAvailability extends LinevenApoForm
{

    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $this->fields[0] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Product quantity', 'SettingsAvailability'),
                    'icon' => 'fa fa-check-square-o'
                ),
                'description' => $this->translator->l('You can choose to select products (or not) according to your stock.', 'SettingsAvailability'),
                'input' => array(
                    array(
                        'type' => 'checkbox',
                        'name' => 'zero_quantity',
                        'configuration_name' => 'LINEVEN_APO_AVL_ZERO_QTY',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'on',
                                    'name' => $this->translator->l('Only select products in stock or available for order if out of stock.', 'SettingsAvailability'),
                                    'val' => '1'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SettingsAvailability'),
                )
            ),
        );
        $product_visibility_value = unserialize(Configuration::get('LINEVEN_APO_AVL_VISIBILITY'));
        $this->fields[1] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Product visibility', 'SettingsAvailability'),
                    'icon' => 'fa fa-search',
                ),
                'description' => $this->translator->l('You can choose the visibility to use according the visibility defined for each product.', 'SettingsAvailability'),
                'input' => array(
                    array(
                        'type' => 'group',
                        'label' => $this->translator->l('Products visibilities to use', 'SettingsAvailability'),
                        'name' => 'product_visibility',
                        'specific' => true,
                        'subtype' => 'visibilities',
                        'value' => $product_visibility_value,
                        'values' => array(
                            array(
                                'id_group' => AdditionalProductsOrder::$product_visibility_both,
                                'name' => $this->translator->l('Everywhere', 'SettingsAvailability'),
                                'val' => AdditionalProductsOrder::$product_visibility_both
                            ),
                            array(
                                'id_group' => AdditionalProductsOrder::$product_visibility_catalog,
                                'name' => $this->translator->l('Catalog only', 'SettingsAvailability'),
                                'val' => AdditionalProductsOrder::$product_visibility_catalog
                            ),
                            array(
                                'id_group' => AdditionalProductsOrder::$product_visibility_search,
                                'name' => $this->translator->l('Search only', 'SettingsAvailability'),
                                'val' => AdditionalProductsOrder::$product_visibility_search
                            ),
                            array(
                                'id_group' => AdditionalProductsOrder::$product_visibility_none,
                                'name' => $this->translator->l('Nowhere', 'SettingsAvailability'),
                                'val' => AdditionalProductsOrder::$product_visibility_none
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SettingsAvailability'),
                )
            ),
        );
    }

    /**
     * Update settings
     *
     * @return void
     */
    public function updateSettings()
    {
        parent::updateSettings();
        $values = array();
        if (Tools::isSubmit('product_visibility')) {
            $values = Tools::getValue('product_visibility');
        }
        Configuration::updateValue('LINEVEN_APO_AVL_VISIBILITY', serialize($values));
    }
}
