<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminOnOrderPage extends LinevenApoFormAdminOnHookForm
{

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct('OOP', 'shoppingcart', 'ShoppingCart');
    }

    /**
     * Init.
     * @return void
     */
    public function init()
    {

        // Render activation fieldset
        $this->renderCrossSellingFieldset(true);


        $this->renderHookToUseFieldset(
            array(
                $this->translator->l('Choose the hook on the order page to display products.', 'OnOrderPage'),
                $this->translator->l('The module use the Prestashop standard hooks : ', 'OnOrderPage'),
                ' - '.$this->translator->l('Shopping cart footer', 'OnOrderPage').' (displayShoppingCartFooter)',
                '',
                $this->translator->l('You can configure the module to use a specific hook implemented in your theme (and so not recognized by the module).', 'OnOrderPage'),
            ),
            array(
                array(
                    'id' => AdditionalProductsOrder::$order_page_hook_use_footer,
                    'name' => $this->translator->l('Shopping cart footer', 'OnOrderPage')
                ),
                array(
                    'id' => AdditionalProductsOrder::$hook_to_use_specific_theme,
                    'name' => $this->translator->l('A specific hook', 'OnOrderPage')
                )
            )
        );

        $this->fields['behavior_refresh'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Refresh settings', 'OnOrderPage'),
                    'icon' => 'fa fa-refresh'
                ),
                'description_multilines' => array(
                    $this->translator->l('When your customer changes their cart directly into the order page, you can refresh the products to display.', 'OnOrderPage'),
                    '',
                    $this->translator->l('Choose the refresh mehod:', 'OnOrderPage'),
                    ' - '.$this->translator->l('No action needed because your theme refreshes your page.', 'OnOrderPage'),
                    ' - '.$this->translator->l('Forcing the refresh of your page by the module (with a configurable delay).', 'OnOrderPage'),
                    ' - '.$this->translator->l('Only refresh the display section of the module (with a configurable delay).', 'OnOrderPage'),
                    $this->translator->l('This operation may depend on the specifics of the themes.', 'OnOrderPage')
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Refresh method', 'OnOrderPage'),
                        'name' => 'refresh_method',
                        'configuration_name' => 'LINEVEN_APO_OOP_REFRESH_MODE',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => AdditionalProductsOrder::$refresh_method_nothing,
                                    'name' => $this->translator->l('Do nothing', 'OnOrderPage')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$refresh_method_reload,
                                    'name' => $this->translator->l('Reload the page', 'OnOrderPage')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$refresh_method_ajax,
                                    'name' => $this->translator->l('Only refresh the section', 'OnOrderPage')
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'desc' => $this->translator->l('Choose the most suitable template for your theme.', 'OnOrderPage'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->translator->l('Delay before refresh', 'OnOrderPage'),
                        'name' => 'refresh_delay',
                        'configuration_name' => 'LINEVEN_APO_OOP_REFRESH_DELAY',
                        'required' => true,
                        'suffix' => $this->translator->l('milliseconds', 'OnOrderPage'),
                        'class' => 'fixed-width-md',
                        'maxlength' => 5,
                        'hint' => $this->translator->l('This value is required and must be numeric.', 'OnOrderPage'),
                        'desc' => $this->translator->l('Set to 0 for an imediate refresh.', 'OnOrderPage')
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnOrderPage'),
                )
            ),
        );

        // Render section activation
        $this->renderSectionActivationFieldset();

        // Render display settings
        $this->renderDisplaySettingsFieldset();

        // Render sort settings
        $this->renderSortSettingsFieldset();
    }

    /**
     * Validate.
     * @return boolean
     */
    public function validate()
    {
        if (Tools::strlen(trim(Tools::getValue('refresh_delay'))) == 0
            || !Validate::isInt(Tools::getValue('refresh_delay'))) {
            $this->addError(
                $this->translator->l('You must fill a correct delay for refresh.', 'OnOrderPage')
            );
        }
        return parent::validate();
    }
}
