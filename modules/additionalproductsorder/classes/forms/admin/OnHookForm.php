<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminOnHookForm extends LinevenApoForm
{
    private $hook_code;
    private $hook_name;
    private $controller_name;

    /**
     * Constructor.
     * @param string $hook_code Hook code
     * @param string $hook_name Hook name
     * @param string $controller_name Controller name
     *
     * @return void
     */
    public function __construct($hook_code, $hook_name, $controller_name = '')
    {
        $this->hook_code = $hook_code;
        $this->hook_name = $hook_name;
        $this->controller_name = $controller_name;
        parent::__construct();
    }

    /**
     * Get products images types.
     * @return array
     */
    protected function getProductsImagesTypes()
    {
        // Specific settings for product
        $images_products = array();
        foreach (ImageType::getImagesTypes('products') as $image_type_product) {
            $images_products[] = array(
                'id' => $image_type_product['id_image_type'],
                'name' => $image_type_product['name'].
                    ' ('.$image_type_product['width'].'x'.$image_type_product['height'].'px)'
            );
        }
        return $images_products;
    }


    /**
     * Get templates.
     * @return array
     */
    protected function getTemplates()
    {
        // Templates
        $templates = array();
        foreach (AdditionalProductsOrder::$native_templates as $code => $name) {
            $templates[$code] = array(
                'id' => $code,
                'name' => $name
            );
        }
        switch ($this->hook_code) {
            case 'OEP':
                unset($templates[AdditionalProductsOrder::$template_standard_theme]);
                break;
        }
        $folder = _LINEVEN_MODULE_APO_REQUIRE_DIR_.'/views/templates/hook/'.$this->hook_name.'/';
        // Custom templates
        if (is_dir($folder)) {
            $templates_dir = opendir($folder);
            $native_templates = array_change_key_case(AdditionalProductsOrder::$native_templates, CASE_LOWER);
            while ($entry = @readdir($templates_dir)) {
                if (!is_dir($folder.$entry) &&
                    $entry != '.' && $entry != '..') {
                    $entry_info = pathinfo($entry);
                    $template_name = basename($entry, '.'.$entry_info['extension']);
                    if (!array_key_exists($template_name, $native_templates) &&
                        Tools::substr($entry, 0, 1) != '_' && $entry_info['extension'] == 'tpl') {
                        $template_specific_name = $template_name.' ('.$this->translator->l('Custom', 'OnHookForm').')';
                        if (Tools::strpos($entry, 'theme')) {
                            $template_specific_name = $template_name.' ('.$this->translator->l('Theme custom', 'OnHookForm').')';
                        }
                        $templates[] = array(
                            'id' => $template_name,
                            'name' => $template_specific_name
                        );
                    }
                }
            }
        }
        return $templates;
    }

    /**
     * Render hook to use fieldset.
     * @param int  $is_purchased_together_allowed Is purchased togheter allowed
     * @param array $description Fieldset description
     *
     * @return array
     */
    protected function renderCrossSellingFieldset($is_purchased_together_allowed = true, $additional_description = null)
    {
        $fieldset_description = array(
            $this->translator->l('If you want to display cross-selling products on this page, you must activate cross-selling. ', 'OnHookForm'),
            '',
            $this->translator->l('To display cross-selling products, you can use:', 'OnHookForm'),
            ' - '.$this->translator->l('Customized associations you defined in the dedicated feature', 'OnHookForm'),
            ' - '.$this->translator->l('Products accessories defined in your catalog', 'OnHookForm')
        );
        if ($is_purchased_together_allowed) {
            $fieldset_description[] = ' - '.$this->translator->l('Products often purchased together', 'OnHookForm');
        }
        if ($additional_description != null) {
            $fieldset_description = array_merge(
                $fieldset_description,
                $additional_description
            );
        }
        $this->fields['behavior_cross_selling'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Cross selling', 'OnHookForm'),
                    'icon' => 'fa fa-recycle'
                ),
                'description_multilines' => $fieldset_description,
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Display cross-selling on this page', 'OnHookForm'),
                        'name' => 'active_hook',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_IS_ACTIVE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_hook_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'OnHookForm')
                            ),
                            array(
                                'id' => 'active_hook_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'OnHookForm')
                            )
                        )
                    ),
                    array(
                        'type' => 'html',
                        'name' => 'text_info_1',
                        'specific' => 'alert',
                        'alert_type' => 'info',
                        'html_content' => $this->translator->l('You can choose what type of cross-selling you want to display.', 'OnHookForm'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Suggest customized associations I defined', 'OnHookForm'),
                        'name' => 'suggest_associations',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_ACTIVE_ASSOCIATIONS',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'suggest_associations_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'OnHookForm')
                            ),
                            array(
                                'id' => 'suggest_associations_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'OnHookForm')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Suggest accessories defined in your catalog', 'OnHookForm'),
                        'name' => 'suggest_acccessories',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_ACTIVE_ACCESSORIES',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'suggest_acccessories_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'OnHookForm')
                            ),
                            array(
                                'id' => 'suggest_acccessories_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'OnHookForm')
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnHookForm'),
                )
            ),
        );
        if ($is_purchased_together_allowed) {
            $this->fields['behavior_cross_selling']['form']['input'][] = array(
                'type' => 'switch',
                'label' => $this->translator->l('Suggest products often purchased together', 'OnHookForm'),
                'name' => 'suggest_purchased_toghether',
                'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_ACTIVE_PURCHASED_TOGETHER',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'suggest_purchased_toghether_on',
                        'value' => 1,
                        'label' => $this->translator->l('Yes', 'OnHookForm')
                    ),
                    array(
                        'id' => 'suggest_purchased_toghether_off',
                        'value' => 0,
                        'label' => $this->translator->l('No', 'OnHookForm')
                    )
                )
            );
        }
    }

    /**
     * Render hook to fieldset.
     * @param array $description Fieldset description
     * @param array $select_options Select list options
     * @param int $is_specific_hook_module Is specific hook module
     * @param string $specific_hook_name_module Specific hook name module
     *
     * @return array
     */
    protected function renderHookToUseFieldset(
        $description,
        $select_options,
        $is_specific_hook_module = false,
        $specific_hook_name_module = ''
    ) {
        $specific_hook_value = '';
        if ($this->controller_name != '' &&
            Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF') &&
            Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF') != '') {
            $hooks_def = unserialize(Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF'));
            if (is_array($hooks_def)) {
                foreach ($hooks_def as $hook_name => $controller_name) {
                    if ($controller_name == $this->controller_name) {
                        $specific_hook_value = $hook_name;
                        break;
                    }
                }
            }
        }
        $this->fields['behavior_hook_settings'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Hook settings', 'OnHookForm'),
                    'icon' => 'fa fa-link'
                ),
                'description_multilines' => $description,
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Hook to use', 'OnHookForm'),
                        'name' => 'hook_to_use',
                        'configuration_name' => 'LINEVEN_APO_' . $this->hook_code . '_HOOK_USE',
                        'class' => 'fixed-width-xxl',
                        'options' => array(
                            'query' => $select_options,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnHookForm'),
                )
            )
        );
        if ($is_specific_hook_module) {
            $this->fields['behavior_hook_settings']['form']['input'][] = array(
                'type' => 'html',
                'name' => 'specific_hook_module_description',
                'specific' => 'alert',
                'alert_type' => 'warning',
                'html_content' => array(
                    $this->translator->l('To use this specific hook on your page, you must modify your theme template page to include the specific hook :', 'OnHookForm'),
                    LinevenApoTools::displayHtmlInCode(
                        'simple_tag',
                        array('tag'=> 'strong', 'text' => '{hook h=\''.$specific_hook_name_module.'\'}')
                    )
                )
            );
        }
        $this->fields['behavior_hook_settings']['form']['input'][] = array(
            'type' => 'html',
            'name' => 'link_to_hook_confirm',
            'specific' => 'alert',
            'alert_type' => 'success',
            'html_content' => $this->translator->l('The module has been linked to this hook.', 'OnHookForm')
        );
        $this->fields['behavior_hook_settings']['form']['input'][] = array(
            'type' => 'html',
            'name' => 'link_to_hook_error',
            'specific' => 'alert',
            'alert_type' => 'danger',
            'html_content' => $this->translator->l('An error occured when link module to this hook. Please do it manually in the Prestashop Position menu.', 'OnHookForm')
        );
        $this->fields['behavior_hook_settings']['form']['input'][] = array(
            'type' => 'textbutton',
            'label' => $this->translator->l('Specific hook to use', 'OnHookForm'),
            'name' => 'specific_hook_to_use',
            'maxlength' => 255,
            'value' => $specific_hook_value,
            'button' => array(
                'label' => $this->translator->l('Link the module to this hook', 'OnHookForm'),
                'attributes' => array(
                    'onclick' => 'LinevenApoModule.Display.linkToSpecificHook();',
                )
            )
        );
    }

    /**
     * Render section activation fieldset.
     * @param int $is_several_sections Is several sections allowed
     * @param string $additional_description Additional description for the fieldset
     * @return array
     */
    protected function renderSectionActivationFieldset($is_several_sections = true, $additional_description = null)
    {
        $fieldset_description = array();
        $section_title = $this->translator->l('Section title', 'OnHookForm');
        if ($is_several_sections) {
            $section_title = $this->translator->l('Display sections method', 'OnHookForm');
            $fieldset_description = array(
                $this->translator->l('If you used several cross-selling functionalities for cross-selling (accessories, associations, purchased together), you can display products in separate section.', 'OnHookForm'),
                $this->translator->l('In this case, each activated functionalities will have is own section to display products.', 'OnHookForm'),
                $this->translator->l('If you choose a single section, all products to display will be grouped in a single section.', 'OnHookForm'),
                $this->translator->l('You can define the one section title or each section title by functionalities.', 'OnHookForm'),
            );
        }
        if ($additional_description != null) {
            $fieldset_description = array_merge(
                $fieldset_description,
                $additional_description
            );
        }
        $this->fields['display_sections'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $section_title,
                    'icon' => 'fa fa-toggle-on'
                )
            )
        );
        if (count($fieldset_description)) {
            $this->fields['display_sections']['form']['description_multilines'] = $fieldset_description;
        }
        $this->fields['display_sections']['form']['input'] = array();
        $this->fields['display_sections']['form']['submit'] =  array(
            'title' => $this->translator->l('Save', 'OnHookForm'),
        );
        if ($is_several_sections) {
            $this->fields['display_sections']['form']['input'][] = array(
                'type' => 'switch',
                'label' => $this->translator->l('Display in separate sections', 'OnHookForm'),
                'name' => 'is_separate_display',
                'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_IS_SEPARATE_DISPLAY',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'is_separate_display_on',
                        'value' => 1,
                        'label' => $this->translator->l('Yes', 'OnHookForm')
                    ),
                    array(
                        'id' => 'is_separate_display_off',
                        'value' => 0,
                        'label' => $this->translator->l('No', 'OnHookForm')
                    )
                )
            );
        }
        $this->fields['display_sections']['form']['input'][] = array(
            'type' => 'text',
            'label' => $this->translator->l('Section title', 'OnHookForm'),
            'name' => 'section_title',
            'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_TITLE',
            'required' => true,
            'maxlength' => 255,
            'lang' => true,
            'desc' => $this->translator->l('The section title appears to your customers before the list of products.', 'OnHookForm')
        );
        if ($is_several_sections) {
            $this->fields['display_sections']['form']['input'][] = array(
                'type' => 'text',
                'label' => $this->translator->l('Accessories section title', 'OnHookForm'),
                'name' => 'section_accessories_title',
                'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_TITLE_ACCESSORIES',
                'required' => true,
                'maxlength' => 255,
                'lang' => true,
                'desc' => $this->translator->l('This section title is for the accessories section.', 'OnHookForm')
            );
            $this->fields['display_sections']['form']['input'][] = array(
                'type' => 'text',
                'label' => $this->translator->l('Associations section title', 'OnHookForm'),
                'name' => 'section_associations_title',
                'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_TITLE_ASSOCIATIONS',
                'required' => true,
                'maxlength' => 255,
                'lang' => true,
                'desc' => $this->translator->l('This section title is for the associations section.', 'OnHookForm')
            );
            $this->fields['display_sections']['form']['input'][] = array(
                'type' => 'text',
                'label' => $this->translator->l('Purchased together section title', 'OnHookForm'),
                'name' => 'section_purchased_together_title',
                'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_TITLE_PURCHASED_TOGETHER',
                'required' => true,
                'maxlength' => 255,
                'lang' => true,
                'desc' => $this->translator->l('This section title is for the purchased together section.', 'OnHookForm')
            );
        }
    }

    /**
     * Render section display settings fieldset.
     * @param boolean $display_price Display price
     * @param boolean $display_add_cart_button Display add to cart button
     * @param array $other_inputs Other inputs
     * @return array
     */
    protected function renderDisplaySettingsFieldset(
        $display_price = true,
        $display_add_cart_button = true,
        $other_inputs = array()
    ) {
        $this->fields['display_settings'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Display settings', 'OnHookForm'),
                    'icon' => 'fa fa-eye'
                ),
                'input' => array(),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnHookForm'),
                )
            ),
        );
        $this->fields['display_settings']['form']['input'] = array_merge(
            $other_inputs,
            array(
                array(
                    'type' => 'text',
                    'label' => $this->translator->l('Maximum products to display', 'OnHookForm'),
                    'name' => 'max_products',
                    'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_MAX_PRODUCTS',
                    'required' => true,
                    'class' => 'fixed-width-xs',
                    'maxlength' => 2,
                    'hint' => $this->translator->l('This value is required and must be numeric.', 'OnHookForm').
                        ' '.$this->translator->l('Set to 0 to display all products.', 'OnHookForm'),
                    'desc' => $this->translator->l('Maximum product to display. Set 0 to display all products.', 'OnHookForm')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->translator->l('Template to use', 'OnHookForm'),
                    'name' => 'display_mode',
                    'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_DISPLAY_MODE',
                    'options' => array(
                        'query' => $this->getTemplates(),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                    'desc' => array(
                        $this->translator->l('Choose the most suitable template for your theme.', 'OnHookForm'),
                        $this->translator->l('Note : Statistics are not avalaible with the theme template.', 'OnHookForm')
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->translator->l('Image type to use', 'OnHookForm'),
                    'name' => 'image_type',
                    'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_IMAGE_TYPE',
                    'options' => array(
                        'query' => $this->getProductsImagesTypes(),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                    'desc' => $this->translator->l('Select the correct image according your theme.', 'OnHookForm')
                )
            )
        );
        if ($display_price) {
            $this->fields['display_settings']['form']['input'][] = array(
                'type' => 'switch',
                'label' => $this->translator->l('Display price', 'OnHookForm'),
                'name' => 'display_price',
                'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_DISPLAY_PRICE',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'display_price_on',
                        'value' => 1,
                        'label' => $this->translator->l('Yes', 'OnHookForm')
                    ),
                    array(
                        'id' => 'display_price_off',
                        'value' => 0,
                        'label' => $this->translator->l('No', 'OnHookForm')
                    )
                )
            );
        }
        if ($display_add_cart_button) {
            $this->fields['display_settings']['form']['input'][] = array(
                'type' => 'switch',
                'label' => $this->translator->l('Display "Add to cart" button', 'OnHookForm'),
                'name' => 'display_cart',
                'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_DISPLAY_CART',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'display_cart_on',
                        'value' => 1,
                        'label' => $this->translator->l('Yes', 'OnHookForm')
                    ),
                    array(
                        'id' => 'display_cart_off',
                        'value' => 0,
                        'label' => $this->translator->l('No', 'OnHookForm')
                    )
                )
            );
            if ($this->hook_code == 'OEP') {
                $this->fields['display_settings']['form']['input'][] = array(
                    'type' => 'switch',
                    'label' => $this->translator->l('Display a checkbox for adding to cart', 'OnHookForm'),
                    'name' => 'display_cart_checkbox',
                    'configuration_name' => 'LINEVEN_APO_' . $this->hook_code . '_DISPLAY_CART_CHK',
                    'values' => array(
                        array(
                            'id' => 'display_cart_checkbox_on',
                            'value' => 1,
                            'label' => $this->translator->l('Yes', 'OnHookForm')
                        ),
                        array(
                            'id' => 'display_cart_checkbox_off',
                            'value' => 0,
                            'label' => $this->translator->l('No', 'OnHookForm')
                        )
                    )
                );
                $this->fields['display_settings']['form']['input'][] = array(
                    'type' => 'checkbox',
                    'name' => 'display_cart_checkbox_icon',
                    'configuration_name' => 'LINEVEN_APO_' . $this->hook_code . '_DISPLAY_CART_CHK_ICO',
                    'values' => array(
                        'query' => array(
                            array(
                                'id' => 'on',
                                'name' => $this->translator->l('Display an "add to cart" icon instead of the default checkbox.', 'OnHookForm'),
                                'val' => '1'
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                );
            }
        }
        $this->fields['display_settings']['form']['input'][] = array(
            'type' => 'switch',
            'label' => $this->translator->l('Display percent reduction', 'OnHookForm'),
            'name' => 'display_reduction',
            'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_DISPLAY_REDUCTION',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'display_reduction_on',
                    'value' => 1,
                    'label' => $this->translator->l('Yes', 'OnHookForm')
                ),
                array(
                    'id' => 'display_reduction_off',
                    'value' => 0,
                    'label' => $this->translator->l('No', 'OnHookForm')
                )
            )
        );

        $this->fields['display_thumbnails_settings'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Thumbnails settings', 'OnHookForm'),
                    'icon' => 'fa fa-picture-o'
                ),
                'description' => $this->translator->l('Related settings only for the thumbnails mode.', 'OnHookForm'),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Display title', 'OnHookForm'),
                        'name' => 'display_title',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_THUMB_DISPLAY_TITLE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'display_title_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'OnHookForm')
                            ),
                            array(
                                'id' => 'display_title_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'OnHookForm')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Display description', 'OnHookForm'),
                        'name' => 'display_description',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_THUMB_DISPLAY_DESC',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'display_description_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'OnHookForm')
                            ),
                            array(
                                'id' => 'display_description_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'OnHookForm')
                            )
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->translator->l('Height block description', 'OnHookForm'),
                        'name' => 'description_height',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_THUMB_DISPLAY_HEIGHT',
                        'required' => true,
                        'suffix' => 'px',
                        'class' => 'fixed-width-xs',
                        'maxlength' => 3,
                        'hint' => $this->translator->l('This value is required and must be numeric and positive.', 'OnHookForm'),
                        'desc' => $this->translator->l('This value determines the height of the description block (title and/or description).', 'OnHookForm')
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnHookForm'),
                )
            ),
        );
    }

    /**
     * Render sort fieldset.
     * @param int $is_purchased_together_allowed Is purchased togheter allowed
     *
     * @return array
     */
    protected function renderSortSettingsFieldset($is_purchased_together_allowed = true)
    {
        $research_priority = array(
            AdditionalProductsOrder::$research_priority_accessories,
            AdditionalProductsOrder::$research_priority_associations
        );
        $research_priority_query = array(
            array(
                'id' => AdditionalProductsOrder::$research_priority_nothing,
                'name' => ' - '.$this->translator->l('Not used', 'OnHookForm').' - '
            ),
            array(
                'id' => AdditionalProductsOrder::$research_priority_accessories,
                'name' => $this->translator->l('Accessories', 'OnHookForm')
            ),
            array(
                'id' => AdditionalProductsOrder::$research_priority_associations,
                'name' => $this->translator->l('Associations', 'OnHookForm')
            )
        );
        if ($is_purchased_together_allowed) {
            $research_priority[] = AdditionalProductsOrder::$research_priority_purchased_together;
            $research_priority_query[] = array(
                'id' => AdditionalProductsOrder::$research_priority_purchased_together,
                'name' => $this->translator->l('Purchased together', 'OnHookForm')
            );
        }
        if (Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RESEARCH_PRIORITY') &&
            Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RESEARCH_PRIORITY') != '') {
            $research_priority = unserialize(Configuration::get('LINEVEN_APO_'.$this->hook_code.'_RESEARCH_PRIORITY'));
        }

        $this->fields['research_priority'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Research priority', 'OnHookForm'),
                    'icon' => 'fa fa-sort-down'
                ),
                'description' => $this->translator->l('For each  cross-selling functionalities, you can define their execution priority.', 'OnHookForm'),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Research priority 1', 'OnHookForm'),
                        'name' => 'research_priority_1',
                        'value' => $research_priority[0],
                        'options' => array(
                            'query' => $research_priority_query,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Research priority 2', 'OnHookForm'),
                        'name' => 'research_priority_2',
                        'value' => $research_priority[1],
                        'options' => array(
                            'query' => $research_priority_query,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnHookForm'),
                )
            ),
        );
        if ($is_purchased_together_allowed) {
            $this->fields['research_priority']['form']['input'][] = array(
                'type' => 'select',
                'label' => $this->translator->l('Research priority 3', 'OnHookForm'),
                'name' => 'research_priority_3',
                'value' => $research_priority[2],
                'options' => array(
                    'query' => $research_priority_query,
                    'id' => 'id',
                    'name' => 'name'
                )
            );
        }

        $this->fields['sort_display_settings'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Sort display settings', 'OnHookForm'),
                    'icon' => 'fa fa-sort'
                ),
                'description' => $this->translator->l('When products to display are found by the module, you can choose the sort method to present products to your customer.', 'OnHookForm'),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Sort method for display products in list', 'OnHookForm'),
                        'name' => 'sort_display_method',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_SORT_DISPLAY',
                        'class' => ' fixed-width-xxl',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => AdditionalProductsOrder::$sort_display_default,
                                    'name' => $this->translator->l('In the order found', 'OnHookForm')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$sort_display_name,
                                    'name' => $this->translator->l('By name', 'OnHookForm')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$sort_display_price,
                                    'name' => $this->translator->l('By price', 'OnHookForm')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$sort_display_random,
                                    'name' => $this->translator->l('Random', 'OnHookForm')
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Sort way', 'OnHookForm'),
                        'name' => 'sort_display_way',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_SORT_DISPLAY_WAY',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => AdditionalProductsOrder::$sort_way_asc,
                                    'name' => $this->translator->l('Ascendant', 'OnHookForm')
                                ),
                                array(
                                    'id' => AdditionalProductsOrder::$sort_way_desc,
                                    'name' => $this->translator->l('Descendant', 'OnHookForm')
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnHookForm'),
                )
            ),
        );
        $this->fields['research_depth'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Research depth', 'OnHookForm'),
                    'icon' => 'fa fa-search'
                ),
                'description_multilines' => array(
                    $this->translator->l('By default, the module search products in the order of your associations.', 'OnHookForm'),
                    $this->translator->l('You can choose to random the search between associations.', 'OnHookForm'),
                    '',
                    $this->translator->l('Moreover, in order not to always offer the same products, you can choose to random your products catalog before products search.', 'OnHookForm'),
                    '',
                    $this->translator->l('Be careful, think about your server resources if you have a large products catalog. Do some tests before activate those feature in production.', 'OnHookForm')
                ),
                'input' => array(
                    array(
                        'type' => 'checkbox',
                        'name' => 'randomize_associations',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_RANDOM_ASSOCIATIONS',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'on',
                                    'name' => $this->translator->l('Ignore sort configured for associations and execute them randomly.', 'OnHookForm'),
                                    'val' => '1'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'name' => 'randomize_search',
                        'configuration_name' => 'LINEVEN_APO_'.$this->hook_code.'_RANDOM_SEARCH',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'on',
                                    'name' => $this->translator->l('Randomize the products catalog before search.', 'OnHookForm'),
                                    'val' => '1'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'OnHookForm'),
                )
            ),
        );
    }

    /**
     * Validate.
     * @return boolean
     */
    public function validate()
    {
        $module_context = LinevenApoContext::getContext();
        $languages = Language::getLanguages(false);

        $is_separate_display = false;
        if (Tools::isSubmit('separate_display') && (int)Tools::getValue('separate_display')) {
            $is_separate_display = true;
        }

        if (Tools::isSubmit('hook_to_use') &&
            Tools::getValue('hook_to_use') == AdditionalProductsOrder::$hook_to_use_specific_theme &&
            Tools::strlen(trim(Tools::getValue('specific_hook_to_use'))) == 0) {
            $this->addError(
                $this->translator->l('You must fill the specific hook name to use by the module.', 'OnHookForm')
            );
        }
        if ($is_separate_display) {
            if (Tools::strlen(trim(Tools::getValue('section_accessories_title_'.$module_context->default_id_lang))) == 0) {
                $this->addError(
                    $this->translator->l('You must enter the accessories section title for your default language.', 'OnHookForm')
                );
            }
            if (Tools::strlen(trim(Tools::getValue('section_associations_title_'.$module_context->default_id_lang))) == 0) {
                $this->addError(
                    $this->translator->l('You must enter the associations section title for your default language.', 'OnHookForm')
                );
            }
            if (Tools::isSubmit('section_purchased_together_title_'.$module_context->default_id_lang) &&
                Tools::strlen(trim(Tools::getValue('section_purchased_together_title_'.$module_context->default_id_lang))) == 0) {
                $this->addError(
                    $this->translator->l('You must enter the purchased together section title for your default language.', 'OnHookForm')
                );
            }
        } else {
            if (Tools::strlen(trim(Tools::getValue('section_title_'.$module_context->default_id_lang))) == 0) {
                $this->addError(
                    $this->translator->l('You must enter the section title for your default language.', 'OnHookForm')
                );
            }
        }
        if (Tools::strlen(trim(Tools::getValue('max_products'))) == 0
            || !Validate::isInt(Tools::getValue('max_products'))) {
            $this->addError(
                $this->translator->l('You must fill a correct maximum products value.', 'OnHookForm')
            );
        }
        if (Tools::isSubmit('description_height') &&
            Tools::strlen(trim(Tools::getValue('description_height'))) == 0
                || !Validate::isInt(Tools::getValue('description_height'))) {
            $this->addError(
                $this->translator->l('You must fill a correct height block description.', 'OnHookForm')
            );
        }
        // Check for each language
        foreach ($languages as $language) {
            $language_name = $language['name'];
            if ($is_separate_display) {
                if (Tools::strlen(Tools::getValue('section_title_'.$language['id_lang'])) > 255) {
                    $this->addError(
                        $this->translator->l('The section title is too long for the language:', 'OnHookForm') . ' ' . $language_name . '.'
                    );
                }
            } else {
                if (Tools::strlen(Tools::getValue('section_accessories_title_'.$language['id_lang'])) > 255) {
                    $this->addError(
                        $this->translator->l('The accessories section title is too long for the language:', 'OnHookForm') . ' ' . $language_name . '.'
                    );
                }
                if (Tools::strlen(Tools::getValue('section_associations_title_'.$language['id_lang'])) > 255) {
                    $this->addError(
                        $this->translator->l('The associations section title is too long for the language:', 'OnHookForm') . ' ' . $language_name . '.'
                    );
                }
                if (Tools::isSubmit('section_purchased_together_title_'.$language['id_lang']) &&
                    Tools::strlen(Tools::getValue('section_purchased_together_title_'.$language['id_lang'])) > 255) {
                    $this->addError(
                        $this->translator->l('The purchased together section title is too long for the language:', 'OnHookForm') . ' ' . $language_name . '.'
                    );
                }
            }
        }
        return parent::validate();
    }

    /**
     * Update settings
     *
     * @return void
     */
    public function updateSettings()
    {
        parent::updateSettings();

        $configuration = LinevenApoConfiguration::getConfiguration();

        // Specific hook to use
        $hooks_def = array();
        if (Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF') &&
            Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF') != '') {
            $hooks_def = unserialize(Configuration::get('LINEVEN_APO_SPECIFIC_HOOK_DEF'));
        }
        if (Tools::getValue('hook_to_use') == AdditionalProductsOrder::$hook_to_use_specific_theme) {
            if (Tools::getValue('specific_hook_to_use') != '') {
                foreach ($hooks_def as $hook_name => $controller_name) {
                    if ($controller_name == $this->controller_name &&
                        $hook_name != Tools::getValue('specific_hook_to_use')) {
                        $configuration->getModule()->unregisterHook($hook_name);
                        unset($hooks_def[$hook_name]);
                        break;
                    }
                }
                $hooks_def[Tools::getValue('specific_hook_to_use')] = $this->controller_name;
                Configuration::updateValue('LINEVEN_APO_SPECIFIC_HOOK_DEF', serialize($hooks_def));
            }
        } else {
            foreach ($hooks_def as $hook_name => $controller_name) {
                if ($controller_name == $this->controller_name) {
                    $configuration->getModule()->unregisterHook($hook_name);
                    unset($hooks_def[$hook_name]);
                    Configuration::updateValue('LINEVEN_APO_SPECIFIC_HOOK_DEF', serialize($hooks_def));
                    break;
                }
            }
        }

        // Save research priority
        $research_priority = array(
            AdditionalProductsOrder::$research_priority_accessories,
            AdditionalProductsOrder::$research_priority_associations,
            AdditionalProductsOrder::$research_priority_purchased_together
        );
        for ($i = 0; $i < 3; $i++) {
            if (Tools::isSubmit('research_priority_'.($i+1))) {
                $research_priority[$i] = Tools::getValue('research_priority_'.($i+1));
            }
        }
        Configuration::updateValue('LINEVEN_APO_'.$this->hook_code.'_RESEARCH_PRIORITY', serialize($research_priority));

        // Save image definitions
        if (Tools::isSubmit('image_type')) {
            $image_types = ImageType::getImagesTypes();
            foreach ($image_types as $type) {
                if ($type['id_image_type'] == (int)Tools::getValue('image_type')) {
                    Configuration::updateValue('LINEVEN_APO_'.$this->hook_code.'_IMAGE_TYPE_NAME', $type['name']);
                    Configuration::updateValue('LINEVEN_APO_'.$this->hook_code.'_IMAGE_TYPE_WIDTH', $type['width']);
                    Configuration::updateValue('LINEVEN_APO_'.$this->hook_code.'_IMAGE_TYPE_HEIGHT', $type['height']);
                    break;
                }
            }
        }
    }
}
