<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminAssociation extends LinevenApoForm
{
    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $module_context = LinevenApoContext::getContext();
        // Categories
        $categories = Category::getCategories($module_context->current_id_lang, true, false);
        $categories_select = array();
        foreach ($categories as $category) {
            $categories_select[] = array(
                'id' => $category['id_category'],
                'name' => $category['name'].' ('.$category['id_category'].')'
            );
        };
        $this->fields['association_type'] = array(
            'form' => array(
                'legend' => array(
                        'title' => $this->translator->l('Association type', 'Association'),
                        'icon' => 'fa fa-chain'
                    ),
                'description_multilines' => array(
                    $this->translator->l('Select the rule for the reference products:', 'Association'),
                    ' - '.LinevenApoTools::displayHtmlInCode('simple_tag', array(
                        'tag' => 'u', 'text' => $this->translator->l('Systematic:', 'Association')
                    )).' '.$this->translator->l('the product is always displayed to the customer.', 'Association'),
                    ' - '.LinevenApoTools::displayHtmlInCode('simple_tag', array(
                        'tag' => 'u', 'text' => $this->translator->l('For category :', 'Association')
                    )).' '.$this->translator->l('If your customer placed in cart a product belongs to the selected category,', 'Association').' '.
                        $this->translator->l('the product will be displayed to the customer.', 'Association'),
                    ' - '.LinevenApoTools::displayHtmlInCode('simple_tag', array(
                        'tag' => 'u', 'text' => $this->translator->l('For a product :', 'Association')
                    )).' '.$this->translator->l('If the selected product is placed in cart by your customer,', 'Association').' '.
                        $this->translator->l('the product will be displayed to the customer.', 'Association'),
                    '',
                    $this->translator->l('And the rule to display products:', 'Association'),
                    ' - '.LinevenApoTools::displayHtmlInCode('simple_tag', array(
                        'tag' => 'u', 'text' => $this->translator->l('Products by category :', 'Association')
                    )).' '.$this->translator->l('All products associated to the category will be used to display.', 'Association'),
                    ' - '.LinevenApoTools::displayHtmlInCode('simple_tag', array(
                        'tag' => 'u', 'text' => $this->translator->l('List of products :', 'Association')
                    )).' '.$this->translator->l('The selected products will be used to display.', 'Association').' '
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_related_product',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Association type', 'Association'),
                        'name' => 'association_type',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => LinevenApoAssociation::$association_type_all,
                                    'name' => $this->translator->l('Systematic', 'Association')
                                ),
                                array(
                                    'id' => LinevenApoAssociation::$association_type_category,
                                    'name' => $this->translator->l('For category', 'Association')
                                ),
                                array(
                                    'id' => LinevenApoAssociation::$association_type_product,
                                    'name' => $this->translator->l('For a product', 'Association')
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Your display choice', 'Association'),
                        'name' => 'display_choice',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => LinevenApoAssociation::$related_display_products,
                                    'name' => $this->translator->l('Product(s)', 'Association')
                                ),
                                array(
                                    'id' => LinevenApoAssociation::$related_display_category,
                                    'name' => $this->translator->l('Products by category', 'Association')
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    )
                )
            ),
        );
        $this->fields['related_category'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Selection rules', 'Association'),
                    'icon' => 'fa fa-folder-open-o'
                ),
                'description' => $this->translator->l('If products belongs to the selected category, the product will be displayed to the customer.', 'Association'),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Category', 'Association'),
                        'name' => 'id_related_category',
                        'options' => array(
                            'query' => $categories_select,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'class' => 'chosen fixed-width-xxl'
                    )
                )
            ),
        );
        $this->fields['display_category'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Products to display', 'Association'),
                    'icon' => 'fa fa-folder-open-o'
                ),
                'description' => $this->translator->l('Select the category to display all products in.', 'Association'),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->translator->l('Category', 'Association'),
                        'name' => 'id_displayed_category',
                        'options' => array(
                            'query' => $categories_select,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'class' => 'chosen fixed-width-xxl'
                    )
                )
            ),
        );
        $this->fields['cart_rules'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Cart rules', 'Association'),
                    'icon' => 'fa fa-money'
                ),
                'description_multilines' => array(
                    $this->translator->l('The minimum / maximum amount of cart parameters are only used to display the product.', 'Association'),
                    $this->translator->l('If the customer changes the cart and the amount no longer respects the amounts defined in the association, the product will not be removed from the cart.', 'Association')
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->translator->l('Minimum cart amount', 'Association'),
                        'name' => 'minimum_amount',
                        'required' => true,
                        'class' => 'fixed-width-xs',
                        'maxlength' => 5,
                        'hint' => $this->translator->l('This value is required, must be numeric and equal or greater than 0.', 'Association'),
                        'desc' => $this->translator->l('This is the minimum order amount needed to display product.', 'Association').
                            ' '.$this->translator->l('Fix to 0 for no minimum order.', 'Association')
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->translator->l('Maximum cart amount', 'Association'),
                        'name' => 'maximum_amount',
                        'required' => true,
                        'class' => 'fixed-width-xs',
                        'maxlength' => 5,
                        'hint' => $this->translator->l('This value is required, must be numeric and equal or greater than 0.', 'Association'),
                        'desc' => $this->translator->l('This is the maximum order amount needed to display product in cart.', 'Association').
                            ' '.$this->translator->l('Fix to 0 for no maximum order.', 'Association')
                    ),
                )
            ),
        );
        $this->fields['which_hooks'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Hooks restrictions', 'Association'),
                    'icon' => 'fa fa-link'
                ),
                'description_multilines' => array(
                    $this->translator->l('Choose the hooks you want to authorize for this association.', 'Association'),
                    $this->translator->l('No selection or all selected values equals "all hooks will be used"', 'Association')
                ),
                'input' => array(
                    array(
                        'type' => 'group',
                        'label' => $this->translator->l('Hooks', 'Association'),
                        'name' => 'which_hooks',
                        'specific' => true,
                        'subtype' => 'which_hooks',
                        'value' => array(),
                        'values' => array(
                            array(
                                'id_group' => AdditionalProductsOrder::$hook_code_order,
                                'name' => $this->translator->l('Order page', 'Association'),
                                'val' => AdditionalProductsOrder::$hook_code_order
                            ),
                            array(
                                'id_group' => AdditionalProductsOrder::$hook_code_extra_product,
                                'name' => $this->translator->l('Extra product page', 'Association'),
                                'val' => AdditionalProductsOrder::$hook_code_extra_product
                            )
                        ),
                        'desc' =>$this->translator->l('No selection or all selected values equals "all hooks will be used"', 'Association')
                    )
                )
            ),
        );
        $this->fields['description'] = array(
            'form' => array(
                'legend' => array(
                        'title' => $this->translator->l('Product name & description', 'Association'),
                        'icon' => 'fa fa-info'
                    ),
                'description_multilines' => array(
                    $this->translator->l('You can set a title and / or description for the product to display on the order page.', 'Association'),
                    $this->translator->l('By default, the title and product description are taken.', 'Association')
                ),
                'input' => array(
                   array(
                        'type' => 'text',
                        'label' => $this->translator->l('Product name', 'Association'),
                        'name' => 'name',
                        'maxlength' => 255,
                        'lang' => true,
                        'desc' => $this->translator->l('By default, product name used. You can set an alternative name to use for each languages.', 'Association')
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->translator->l('Description', 'Association'),
                        'name' => 'short_description',
                        'rows' => 5,
                        'cols' => 60,
                        'lang' => true,
                        'desc' => $this->translator->l('By default, product short description used.', 'Association').
                            ' '.$this->translator->l('You can set an alternative description to use for each languages.', 'Association'),
                    )
                )
            ),
        );
        $groups = Group::getGroups($module_context->current_id_lang);
        $groups_values = array();
        $groups_details = array();
        foreach ($groups as $group) {
            $groups_details[] = array(
                'id_group' => $group['id_group'],
                'name' => $group['name'],
                'val' => $group['id_group']
            );
        }
        $this->fields['groups'] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Customers groups', 'Association'),
                    'icon' => 'fa fa-money'
                ),
                'description' => $this->translator->l('You can choose to activate this association only for selected customers groups.', 'Association'),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Activate groups', 'Association'),
                        'name' => 'is_active_groups',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'is_active_groups_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'Association')
                            ),
                            array(
                                'id' => 'is_active_groups_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'Association')
                            )
                        )
                    ),
                    array(
                        'type' => 'group',
                        'label' => $this->translator->l('Choose group(s) for this association', 'Association'),
                        'name' => 'apo_groups',
                        'specific' => true,
                        'subtype' => 'customers_groups',
                        'value' => $groups_values,
                        'values' => $groups_details
                    )
                )
            ),
        );
        $this->fields['comments'] = array(
            'form' => array(
                'legend' => array(
                        'title' => $this->translator->l('Comments', 'Association'),
                        'icon' => 'fa fa-info'
                    ),
                'description' => $this->translator->l('The comment is for your own use only.', 'Association'),
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'label' => $this->translator->l('Comments', 'Association'),
                        'name' => 'comments',
                        'rows' => 5,
                        'cols' => 60
                    )
                )
            ),
        );
    }

    /**
     * Validate.
     * @return boolean
     */
    public function validate()
    {
        $languages = Language::getLanguages(false);

        if (Tools::getValue('association_type') == LinevenApoAssociation::$association_type_product) {
            if (!Tools::isSubmit('id_related_product') || (Tools::isSubmit('id_related_product') &&
                    (int)Tools::getValue('id_related_product') == 0)) {
                $this->addError(
                    $this->translator->l('You must fill a correct product for the selection rule.', 'Association')
                );
            }
        }
        if (Tools::getValue('display_choice') == LinevenApoAssociation::$related_display_products) {
            if (!Tools::isSubmit('displayed_products_list') ||
                (Tools::isSubmit('displayed_products_list') && count(Tools::getValue('displayed_products_list')) == 0)) {
                $this->addError(
                    $this->translator->l('You must fill product(s) to display.', 'Association')
                );
            }
        }

        // Minimum amout
        if (Tools::strlen(trim(Tools::getValue('minimum_amount'))) == 0
            || !Validate::isFloat(Tools::getValue('minimum_amount'))) {
            $this->addError(
                $this->translator->l('You must fill a correct minimum amount for cart.', 'Association').' '.
                $this->translator->l('Set to 0 for no minimum amount.', 'Association')
            );
        }
            
        // Maximum amout
        if (Tools::strlen(trim(Tools::getValue('maximum_amount'))) == 0
            || !Validate::isFloat(Tools::getValue('maximum_amount'))) {
            $this->addError(
                $this->translator->l('You must fill a correct maximum amount for cart.', 'Association').' '.
                $this->translator->l('Set to 0 for no maximum amount.', 'Association')
            );
        }

        // Check for each language
        foreach ($languages as $language) {
            $language_name = $language['name'];
            if (Tools::strlen(Tools::getValue('name_'.$language['id_lang'])) > 255) {
                $this->addError(
                    $this->translator->l('The product name is too long for the language:', 'Association').' '.$language_name.'.'
                );
            }
            if (Tools::strlen(trim(Tools::getValue('short_description_'.$language['id_lang']))) > 4000) {
                $this->addError(
                    $this->translator->l('The product description is too long for the language:', 'Association').' '.$language_name.'.'
                );
            }
        }
        
        return parent::validate();
    }

    /**
     * Populate.
     *
     * @param Object $object
     * @return void
     */
    public function populate($object)
    {
        parent::populate($object);

        //  Populate groups
        if ((int)$object->is_active_groups == 1 && $object->id != null && is_array($object->groups)) {
            $groups_values = array();
            foreach ($object->groups as $group) {
                $groups_values[] = $group['id_group'];
            }
            $this->setValue('apo_groups', $groups_values);
        }

        // Populate options
        if ($object != null && property_exists(get_class($object), 'hooks') && $object->hooks != '') {
            $values = explode('|', $object->hooks);
            foreach ($values as $value) {
                $this->setValue('which_hooks_'.$value, 1);
            }
            $this->setValue('which_hooks', $values);
        }
    }
}
