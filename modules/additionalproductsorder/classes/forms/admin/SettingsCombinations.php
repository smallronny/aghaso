<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminSettingsCombinations extends LinevenApoForm
{

    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $this->fields[] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Combination', 'SettingsCombinations'),
                    'icon' => 'fa fa-compress',
                ),
                'description_multilines' => array(
                    $this->translator->l('For products with combinations you have the possibility to display :', 'SettingsCombinations'),
                    ' - '.$this->translator->l('all product combinations (in the limit of the max products you have fixed)', 'SettingsCombinations'),
                    ' - '.$this->translator->l('only the default combination', 'SettingsCombinations')
                ),
                'input' => array(
                    array(
                        'type' => 'checkbox',
                        'name' => 'combinations_all',
                        'configuration_name' => 'LINEVEN_APO_COMBINATIONS_ALL',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'on',
                                    'name' => $this->translator->l('Display all combinations of products when exists.', 'SettingsCombinations'),
                                    'val' => '1'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'html',
                        'name' => 'text_info_1',
                        'specific' => 'alert',
                        'alert_type' => 'warning',
                        'html_content' => $this->translator->l('If you choose to display only the default combination, you can define settings bellow.', 'SettingsCombinations'),
                    ),
                    array(
                        'type' => 'checkbox',
                        'name' => 'combinations_add_cart',
                        'configuration_name' => 'LINEVEN_APO_COMBINATIONS_ADDCART',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'on',
                                    'name' => $this->translator->l('Do not display "Add to cart" button but display a "Customize" button.', 'SettingsCombinations'),
                                    'val' => '1'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'name' => 'combinations_display_options',
                        'configuration_name' => 'LINEVEN_APO_COMBINATIONS_DSP_OPT',
                        'values' => array(
                            'query' => array(array(
                                'id' => 'on',
                                'name' => $this->translator->l('Display a message to inform your customer options are availaible for the product.', 'SettingsCombinations'),
                                'val' => '1'
                            )),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SettingsCombinations'),
                )
            ),
        );
    }
}
