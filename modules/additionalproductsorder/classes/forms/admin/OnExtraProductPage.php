<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFormAdminOnExtraProductPage extends LinevenApoFormAdminOnHookForm
{

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct('OEP', 'extraproductpage', 'ExtraProductPage');
    }

    /**
     * Init.
     * @return void
     */
    public function init()
    {
        // Render activation fieldset
        $this->renderCrossSellingFieldset(false);

        $this->renderHookToUseFieldset(
            array(
                $this->translator->l('Choose the hook on the order page to display products.', 'OnExtraProductPage'),
                $this->translator->l('The module use the Prestashop standard hooks : ', 'OnExtraProductPage'),
                ' - '.$this->translator->l('Product additional informations', 'OnExtraProductPage').' (displayProductAdditionalInfo)',
                ' - '.$this->translator->l('Product actions - available according Prestashop version & themes', 'OnExtraProductPage').' (displayProductActions)',
                '',
                $this->translator->l('For specific hook you have two ways : ', 'OnExtraProductPage'),
                ' - '.$this->translator->l('Use the hook "displayAdditionalProductsExtraProduct" give by the module (requires modifying your theme template).', 'OnExtraProductPage'),
                ' - '.$this->translator->l('Can configure the module to use a specific hook implemented in your theme (and so not recognized by the module).', 'OnExtraProductPage'),
            ),
            array(
                array(
                    'id' => AdditionalProductsOrder::$product_page_hook_use_additional_info,
                    'name' => $this->translator->l('Additional info', 'OnExtraProductPage')
                ),
                array(
                    'id' => AdditionalProductsOrder::$product_page_hook_use_product_actions,
                    'name' => $this->translator->l('Product actions', 'OnExtraProductPage')
                ),
                array(
                    'id' => AdditionalProductsOrder::$hook_to_use_specific_module,
                    'name' => $this->translator->l('Module hook', 'OnExtraProductPage').' : displayAdditionalProductsExtraProduct'
                ),
                array(
                    'id' => AdditionalProductsOrder::$hook_to_use_specific_theme,
                    'name' => $this->translator->l('A specific hook', 'OnExtraProductPage')
                )
            ),
            true,
            'displayAdditionalProductsExtraProduct'
        );

        // Render section activation
        $this->renderSectionActivationFieldset(false);

        // Render display settings
        $this->renderDisplaySettingsFieldset();

        // Render sort settings
        $this->renderSortSettingsFieldset(false);
    }

    /**
     * Validate.
     * @return boolean
     */
    public function validate()
    {
        return parent::validate();
    }
}
