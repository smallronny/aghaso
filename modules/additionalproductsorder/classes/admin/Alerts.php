<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminAlerts
{
    /**
     * Check for alerts.
     *
     * @return boolean
     */
    public static function isAlerts()
    {
        return (count(self::getAlerts()) > 0 ? true : false);
    }
    
    /**
     * Get alerts.
     *
     * @return array
     */
    public static function getAlerts()
    {
        $translator = new LinevenApoTranslator();
        $return = array();
        // Count associations to display
        $count_associations = count(LinevenApoAssociation::getAssociations());
        if ($count_associations == 0) {
            $return[] = array(
                'icon' => 'fa fa-exclamation-circle',
                'message' => $translator->l('You have not defined associations. Click here to create associations.', 'Alerts'),
                'color' => '#D2A63C',
                'href' => LinevenApoTools::getBackofficeURI('Associations', 'addEdit', 'associations:list:add')
            );
        }
        // Check product image
        if (!LinevenApoAdminDiagnostic::checkProductImageUsed()) {
            $return[] = array(
                'icon' => 'fa fa-exclamation-circle',
                'message' => $translator->l('Product image used is not a correct product image type. Check the display configuration.', 'Alerts'),
                'color' => '#D2A63C',
                'href' => LinevenApoTools::getBackofficeURI('Settings', 'display', 'settings:display')
            );
        }
        // Check smarty template used
        if (!LinevenApoAdminDiagnostic::checkSmartyTemplateUsed()) {
            $return[] = array(
                'icon' => 'fa fa-exclamation-circle',
                'message' => $translator->l('Smarty template used is not exists in folder. Check your configuration.', 'Alerts'),
                'color' => '#D2A63C',
                'href' => LinevenApoTools::getBackofficeURI('Settings', 'display', 'settings:display')
            );
        }
        $return = array_merge(LinevenApoModuleAlerts::getAlerts(), $return);
        return $return;
    }
}
