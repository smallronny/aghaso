<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAdminDiagnostic extends LinevenApoModuleDiagnostic
{
    /**
     * Execute.
     *
     * @return array
     */
    public function execute()
    {
        parent::execute();
        // Specific execution here
        if ($this->is_success) {
            switch ($this->what) {
                case 'hooks':
                    $this->is_success = self::repairHooksByConfiguration();
                    break;
            }
        }
        return $this->is_success;
    }

    /**
     * Check hooks by configuration.
     *
     * @return array
     */
    public static function checkHooksByConfiguration()
    {
        $items = array();
        return $items;
    }

    /**
     * Check product image used.
     *
     * @return boolean
     */
    public static function checkProductImageUsed()
    {
        $id_image_type_used = Configuration::get('LINEVEN_APO_OOP_IMAGE_TYPE');
        $name_image_type_used = Configuration::get('LINEVEN_APO_OOP_IMAGE_TYPE_NAME');
        $products_image = ImageType::getImagesTypes('products', true);
        $found = false;
        if (count($products_image)) {
            foreach ($products_image as $image) {
                if ($image['id_image_type'] == $id_image_type_used &&
                    $image['name'] == $name_image_type_used
                ) {
                    $found = true;
                    break;
                }
            }
        }
        return $found;
    }

    /*
     * Check smarty template used.
     *
     * @return boolean
     */
    public static function checkSmartyTemplateUsed()
    {
        $template_file = Configuration::get('LINEVEN_APO_OOP_DISPLAY_MODE');
        $template_file = Tools::strtolower($template_file).'.tpl';
        return is_file(_LINEVEN_MODULE_APO_TEMPLATES_HOOK_DIR_.$template_file);
    }

    /**
     * Repair hooks by configuration.
     */
    public static function repairHooksByConfiguration()
    {
        return true;
    }
}
