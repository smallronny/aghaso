<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoProductsListPresenter
{
    protected $hook_code;                   // Hook code
    protected $hook_folder;                 // Hook folder
    protected $controller_name;             // Controller name
    protected $hook_classname;              // Hook classname
    protected $is_separate_results;         // Is separate results
    protected $presenter;                   // Presenter
    protected $is_ajax_put_to_cart;         // Ajax put to cart
    protected $is_price_display;            // Price display
    protected $is_cart_button_display;      // Cart button display
    protected $is_cart_checkbox_display;    // Cart checkbox display
    protected $is_cart_checkbox_icon;       // Cart checkbox display
    protected $id_related_product;          // Id related product
    protected $is_refresh;                  // Is refresh
    protected $template;                    // Template
    protected $title;                       // Title
    protected $products;                    // Products

    /**
     * constructor.
     * @param string $hook_code Hook code
     * @param string $hook_folder Hook folder
     * @param string $controller_name Controller name
     */
    public function __construct($hook_code, $hook_folder, $controller_name)
    {
        $this->products = array();
        $this->hook_code = $hook_code;
        $this->hook_folder = $hook_folder;
        $this->controller_name = $controller_name;
        $this->hook_classname = '';
        $this->is_separate_results = false;
        $this->is_ajax_put_to_cart = true;
        $this->is_price_display = (int)$this->getConfiguration('DISPLAY_PRICE');
        $this->is_cart_button_display = true;
        $this->is_cart_checkbox_display = false;
        $this->is_cart_checkbox_icon = false;
        $this->template = Tools::strtolower($this->getConfiguration('DISPLAY_MODE'));
        $this->id_related_product = 0;
        $this->is_refresh = false;
        $this->title = '';
        $this->products = array();
    }

    /**
     * Set separate results.
     *
     * @param boolean $is_separate_results
     * @return void
     */
    public function setSeparateResults($is_separate_results)
    {
        $this->is_separate_results = $is_separate_results;
    }

    /**
     * Is separate results.
     *
     * @return boolean
     */
    public function isSeparateResults()
    {
        return $this->is_separate_results;
    }

    /**
     * Get template.
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set Ajax put to cart.
     *
     * @param boolean $is_ajax
     * @return void
     */
    public function setAjaxPutToCart($is_ajax)
    {
        $this->is_ajax_put_to_cart = $is_ajax;
    }

    /**
     * Set display price.
     *
     * @param boolean $is_price_display Is price display
     * @return void
     */
    public function setPriceDisplay($is_price_display)
    {
        $this->is_price_display = $is_price_display;
    }

    /**
     * Set display cart button.
     *
     * @param boolean $is_cart_button_display Is cart button display
     * @param boolean $is_cart_checkbox_display Is cart checkbox display
     * @param boolean $is_cart_checkbox_icon Is cart checkbox icon
     * @return void
     */
    public function setAddToCartDisplay($is_cart_button_display, $is_cart_checkbox_display = false, $is_cart_checkbox_icon = false)
    {
        $this->is_cart_button_display = $is_cart_button_display;
        $this->is_cart_checkbox_display = $is_cart_checkbox_display;
        $this->is_cart_checkbox_icon = $is_cart_checkbox_icon;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Set hook classname.
     *
     * @param string $classname
     * @return void
     */
    public function setHookClassname($classname)
    {
        $this->hook_classname = $classname;
    }

    /**
     * Set id related product.
     *
     * @param int $id_related_product
     * @return void
     */
    public function setIdRelatedProduct($id_related_product)
    {
        $this->id_related_product = $id_related_product;
    }

    /**
     * Set products
     *
     * @param string $products
     * @return void
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * Set refresh
     *
     * @param boolean $is_refresh is refresh (ajax)
     * @return void
     */
    public function setRefresh($is_refresh)
    {
        $this->is_refresh = $is_refresh;
    }

    /**
     * Present.
     */
    public function present()
    {
        $context = Context::getContext();
        $this->presenter = array(
            'hook_code' => $this->hook_code,
            'hook_folder' => $this->hook_folder,
            'controller_name' => $this->controller_name,
            'hook_class_name' => $this->hook_classname,
            'is_separate_results' => $this->is_separate_results,
            'title' => $this->title,
            'id_related_product' => $this->id_related_product,
            'is_refresh' => $this->is_refresh,
            'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'static_token' => Tools::getToken(false),
            'link' => Context::getContext()->link,
            'is_mobile' => $context->isMobile(),
            'price_display' => Product::getTaxCalculationMethod((int)$context->cookie->id_customer),
            'tax_enabled' => Configuration::get('PS_TAX') && !Configuration::get('AEUC_LABEL_TAX_INC_EXC'),
            'display_price' => $this->is_price_display,
            'display_cart_button' => (int)$this->is_cart_button_display,
            'display_cart_checkbox' => (int)$this->is_cart_checkbox_display,
            'display_cart_checkbox_icon' => (int)$this->is_cart_checkbox_icon,
            'display_reduction' => (int)$this->getConfiguration('DISPLAY_REDUCTION'),
            'ajax_put_cart' => (int)$this->is_ajax_put_to_cart,
            'display_title' => (int)$this->getConfiguration('THUMB_DISPLAY_TITLE'),
            'display_description' => (int)$this->getConfiguration('THUMB_DISPLAY_DESC'),
            'description_height' => (int)(($this->getConfiguration('THUMB_DISPLAY_HEIGHT') != 0
                && $this->getConfiguration('THUMB_DISPLAY_HEIGHT') != '')
                ? $this->getConfiguration('THUMB_DISPLAY_HEIGHT') : '140'),
            'combinations_all' => (int)Configuration::get('LINEVEN_APO_COMBINATIONS_ALL'),
            'combinations_add_to_cart' => true,
            'combinations_display_options' => (int)Configuration::get('LINEVEN_APO_COMBINATIONS_DSP_OPT'),
            'statistics_add_to_cart' => (Configuration::get('LINEVEN_APO_STATS_ACTIVE') &&
                Configuration::get('LINEVEN_APO_STATS_ADD_CART')),
            'statistics_viewed' => (Configuration::get('LINEVEN_APO_STATS_ACTIVE') &&
                Configuration::get('LINEVEN_APO_STATS_IS_VIEWED')),
            'partners_reviews' => false,
            'partners_reviews_module_instance' => null,
            'template' => $this->template,
            'template_type' => $this->template,
            'template_class_name' => 'list'
        );

        // Products images
        $this->setProductImageDef();

        // Combinations
        if ((int)Configuration::get('LINEVEN_APO_COMBINATIONS_ALL') == false) {
            $this->presenter['combinations_add_to_cart'] = !(int)Configuration::get('LINEVEN_APO_COMBINATIONS_ADDCART');
        }

        // Products
        $products_presenter = null;
        if ($this->presenter['template'] == 'theme' || Tools::strpos($this->presenter['template'], 'theme')) {
            $this->presenter['template_type'] = 'theme';
            $products_presenter = new LinevenApoProductThemePresenter(
                (int)$this->getConfiguration('MAX_PRODUCTS')
            );
        } else {
            $products_presenter = new LinevenApoProductClassicPresenter(
                (int)$this->getConfiguration('MAX_PRODUCTS'),
                $this->getConfiguration('IMAGE_TYPE_NAME')
            );
        }
        $this->presenter['sections'] = $products_presenter->present($this->products);

        // Templates
        switch ($this->getConfiguration('DISPLAY_MODE')) {
            case AdditionalProductsOrder::$template_standard_classic:
            case AdditionalProductsOrder::$template_standard_list_a:
            case AdditionalProductsOrder::$template_standard_list_b:
                $this->presenter['template_class_name'] = 'list';
                break;
            case AdditionalProductsOrder::$template_standard_thumbnails:
                $this->presenter['template_class_name'] = 'thumbnails';
                break;
            case AdditionalProductsOrder::$template_standard_theme:
                $this->presenter['template_class_name'] = 'theme';
                break;
            default:
                $this->presenter['template_class_name'] = $this->getConfiguration('DISPLAY_MODE');
                break;
        }
        
        // Partners
        $this->setPartners();

        return $this->presenter;
    }

    /**
     * Set product image definition.
     */
    private function setProductImageDef()
    {
        $this->presenter['image_type'] = $this->getConfiguration('IMAGE_TYPE_NAME');
        $this->presenter['image_width'] = $this->getConfiguration('IMAGE_TYPE_WIDTH');
        $this->presenter['image_height'] = $this->getConfiguration('IMAGE_TYPE_HEIGHT');
    }

    /**
     * Set partners.
     */
    private function setPartners()
    {
        $this->presenter['partners_reviews'] = false;
        $this->presenter['partners_reviews_module'] = null;
        $this->presenter['partners_reviews_module_instance'] = null;
        if (Configuration::get('LINEVEN_APO_PARTNER_RVW_RATE')) {
            $this->presenter['partners_reviews'] = true;
            if (Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE') == AdditionalProductsOrder::$partner_reviews_module_lineven
                && LinevenApoPartners::verifyModule(AdditionalProductsOrder::$partner_reviews_module_lineven)) {
                $this->presenter['partners_reviews_module'] = AdditionalProductsOrder::$partner_reviews_module_lineven;
                $this->presenter['partners_reviews_module_instance'] =
                    Module::getInstanceByName(AdditionalProductsOrder::$partner_reviews_module_lineven);
            }
            if (Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE') == AdditionalProductsOrder::$partner_reviews_module_prestashop
                && LinevenApoPartners::verifyModule(AdditionalProductsOrder::$partner_reviews_module_prestashop)) {
                $this->presenter['partners_reviews_module'] = AdditionalProductsOrder::$partner_reviews_module_prestashop;
            }
            // Else is the standard hook
        }
    }

    /**
     * Get configuration.
     * @param string $suffix Suffix
     */
    private function getConfiguration($suffix)
    {
        return Configuration::get('LINEVEN_APO_'.$this->hook_code.'_'.$suffix);
    }
}
