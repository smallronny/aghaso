<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoProductClassicPresenter extends LinevenApoProductPresenter
{
    private $image_type = null;

    /**
     * Constructor.
     * @param int $max_products Max products
     * @param int $id_lang Id lang
     * @param string $image_type Image type
     */
    public function __construct($max_products = 100, $image_type = null, $id_lang = null)
    {
        parent::__construct($max_products, $id_lang);
        $this->image_type = ImageType::getFormattedName('small');
        if ($image_type != null) {
            $this->image_type = $image_type;
        }
    }

    /**
     * Return products from list
     *
     * @param array $search_results Search results
     * @return mixed Products
     */
    public function present($search_results = null)
    {
        $sections = null;
        if ($search_results != null && is_array($search_results)) {
            // Get results
            reset($search_results);
            $method = key($search_results);
            $results = current($search_results);
            foreach ($results as $section_code => $section) {
                $products_results = $section['results'];
                $section_products_found = $products_results->getProductsFound();
                $section_products = $products_results->getProducts();
                $products = array();
                $new_section = array('products' => $products);
                if ($method == 'separate') {
                    $new_section['title'] = $section['title'];
                }
                $count_products_to_display = 0;
                // Get the product detail
                foreach ($section_products as $row) {
                    if ($count_products_to_display < $this->max_products) {
                        $id_product = $row['id_product'];
                        if (!$this->isProductPresented($id_product)) {
                            $association_details = null;
                            if ($section_products_found[$id_product]['SECTION'] == AdditionalProductsOrder::$research_priority_associations) {
                                $association_details = $section_products_found[$id_product];
                            }
                            $product = new Product();
                            $product->hydrate($row, $this->id_lang);
                            $product->loadStockData();
                            $product->isFullyLoaded = true;
                            $product->allow_oosp = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
                            $product->id_association = (isset($association_details['id']) ? $association_details['id'] : 0);
                            $product->association = $association_details;
                            $product->has_attributes = ($row['id_product_attribute'] ? 1 : 0);
                            $product->product_lazy_array = array('id' => $product->id, 'id_product' => $product->id);
                            if (version_compare(_PS_VERSION_, '1.7.6', '>=') &&
                                (int)Configuration::get('LINEVEN_APO_PARTNER_RVW_RATE') &&
                                Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE') != AdditionalProductsOrder::$partner_reviews_module_lineven) {
                                if (Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE') == AdditionalProductsOrder::$partner_reviews_module_hook) {
                                    $product->product_lazy_array = new LinevenApoProductLazyArray($row);
                                } else {
                                    if (Configuration::get('LINEVEN_APO_PARTNER_RVW_MODULE') == AdditionalProductsOrder::$partner_reviews_module_prestashop) {
                                        $productCommentRepository = Context::getContext()->controller->getContainer()->get('product_comment_repository');
                                        $comments_nb = $productCommentRepository->getCommentsNumber($product->id, Configuration::get('PRODUCT_COMMENTS_MODERATE'));
                                        $average_grade = $productCommentRepository->getAverageGrade($product->id, Configuration::get('PRODUCT_COMMENTS_MODERATE'));
                                        $product->prestashop_product_comment_nb = $comments_nb;
                                        $product->prestashop_product_comment_average_grade = $average_grade;
                                    }
                                }
                            }
                            $add_statistic = false;

                            // Redefine name and description
                            if (is_array($association_details)) {
                                // Specific name
                                if (array_key_exists('name', $association_details)) {
                                    $name = LinevenApoTranslator::getTranslationFromArray(unserialize($association_details['name']));
                                    if (Tools::strlen(trim($name)) != 0) {
                                        $product->name = $name;
                                    }
                                }
                                // Specific description
                                if (array_key_exists('short_description', $association_details)) {
                                    $description = LinevenApoTranslator::getTranslationFromArray(unserialize($association_details['short_description']));
                                    if (Tools::strlen(trim($description)) != 0) {
                                        $product->description_short = htmlspecialchars($description);
                                    }
                                }
                            }

                            // If product has not attribute
                            if (!$product->has_attributes) {
                                if (self::checkAvailabilityByQuantity(
                                    $product->allow_oosp,
                                    $product->quantity
                                )) {
                                    $cover = Product::getCover((int)$id_product);
                                    $product = $this->hydrateProduct(
                                        array(
                                            'price' => Tools::displayPrice($product->getPrice(true), Context::getContext()->currency),
                                            'price_tax_excluded' => Tools::displayPrice($product->getPrice(false), Context::getContext()->currency),
                                            'id_image' => $cover['id_image']
                                        ),
                                        $product
                                    );
                                    $products[] = $product;
                                    $count_products_to_display++;
                                    $add_statistic = true;
                                }
                            } else {
                                if (!Configuration::get('LINEVEN_APO_COMBINATIONS_ALL') && self::checkAvailabilityByQuantity($product->allow_oosp, $product->quantity)) {
                                    $product = $this->hydrateProduct(
                                        self::getDefaultAttributeDetails($product, $this->id_lang),
                                        $product
                                    );
                                    $products[] = $product;
                                    $count_products_to_display++;
                                    $add_statistic = true;
                                } else {
                                    $product_attributes = $product->getAttributesResume($this->id_lang);
                                    foreach ($product_attributes as $attribute) {
                                        $new_product = clone $product;
                                        if (self::checkAvailabilityByQuantity($new_product->allow_oosp, $attribute['quantity'])
                                            && $count_products_to_display < $this->max_products) {
                                            $new_product = $this->hydrateProduct(
                                                self::getDetailsForAttribute($attribute, $new_product, $this->id_lang),
                                                $new_product
                                            );
                                            $products[] = $new_product;
                                            $count_products_to_display++;
                                            $add_statistic = true;
                                            if ($count_products_to_display >= $this->max_products) {
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if ($add_statistic && isset($association_details['id'])) {
                                $this->addStatisticsForDisplay($association_details['id'], $id_product);
                            }
                        }
                    }
                }

                if (count($products)) {
                    $new_section['products'] = $products;
                }
                $sections[$section_code] = $new_section;
            }
        }
        return $sections;
    }

    /**
     * Hydrate product.
     *
     * @param array $datas Datas
     * @param Object $product Product
     * @return Object
     */
    public function hydrateProduct($datas, $product)
    {
        $context = Context::getContext();
        $link = $context->link;
        $product->price = $datas['price'];
        $product->price_tax_excluded = $datas['price_tax_excluded'];
        $product->id_image = $datas['id_image'];
        $product->image_link = '';
        if ((int)$product->id_image == 0) {
            $imageRetriever = new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever($link);
            if (method_exists($imageRetriever, 'getNoPictureImage')) {
                $no_picture_image = $imageRetriever->getNoPictureImage($context->language);
                if (is_array($no_picture_image) &&
                    isset($no_picture_image['bySize']) && isset($no_picture_image['bySize'][$this->image_type])) {
                    $product->image_link = $no_picture_image['bySize'][$this->image_type]['url'];
                }
            }
        } else {
            $product->image_link = $link->getImageLink(
                $product->link_rewrite,
                $product->id.'-'.$product->id_image,
                $this->image_type
            );
        }
        if (isset($datas['quantity'])) {
            $product->quantity = $datas['quantity'];
        }
        if (isset($datas['minimal_quantity'])) {
            $product->minimal_quantity = $datas['minimal_quantity'];
        }
        if (!property_exists($product, 'id_product_attribute')) {
            $product->id_product_attribute = null;
        }
        if (isset($datas['id_product_attribute'])) {
            $product->id_product_attribute = $datas['id_product_attribute'];
        }
        if (isset($datas['attribute_designation'])) {
            $product->attribute_designation = $datas['attribute_designation'];
        }
        $product->product_link = $link->getProductLink(
            $product->id,
            $product->link_rewrite,
            (property_exists($product, 'category') ? $product->category : null),
            (property_exists($product, 'ean13') ? $product->ean13 : null),
            null,
            null,
            $product->id_product_attribute,
            false,
            false,
            true
        );
        // Hydrate prices details
        $specific_price = array();
        $product->reduction= Product::getPriceStatic(
            $product->id,
            false,
            $product->id_product_attribute,
            6,
            null,
            true,
            true,
            1,
            true,
            null,
            null,
            null,
            $specific_price
        );
        $product->specific_price = $specific_price;
        $product->price_without_reduction = 0;
        $product->price_without_reduction_tax_excluded = 0;
        if (is_array($product->specific_price) && count($product->specific_price)) {
            $product->price_without_reduction = Tools::displayPrice($product->getPriceWithoutReduct(false, $product->id_product_attribute));
            $product->price_without_reduction_tax_excluded = Tools::displayPrice($product->getPriceWithoutReduct(true, $product->id_product_attribute));
        }
        return $product;
    }


    /**
     * Get default attribute details.
     *
     * @param object $product Product object
     * @param integer $id_lang Language ID
     * @return object
     */
    public static function getDefaultAttributeDetails($product, $id_lang)
    {
        // Get product attributes
        $product_attributes = $product->getAttributesResume($id_lang);
        $default_attribute = null;
        if (count($product_attributes)) {
            foreach ($product_attributes as $attribute) {
                if ($attribute['default_on']) {
                    $default_attribute = $attribute;
                    $default_attribute = self::getDetailsForAttribute($default_attribute, $product, $id_lang);
                    break;
                }
            }
        }
        return $default_attribute;
    }

    /**
     * Populate details for attribute.
     *
     * @param array $attribute Attribute
     * @param object $product Product
     * @param integer $id_lang Language ID
     * @return array
     */
    public static function getDetailsForAttribute($attribute, $product, $id_lang)
    {
        $attribute['price'] = Tools::displayPrice($product->getPrice(true, $attribute['id_product_attribute']), Context::getContext()->currency);
        $attribute['price_tax_excluded'] = Tools::displayPrice($product->getPrice(false, $attribute['id_product_attribute']), Context::getContext()->currency);
        $attribute['id_image'] = 0;
        $images = $product->getCombinationImages($id_lang);
        if (is_array($images) && count($images) && isset($images[$attribute['id_product_attribute']])) {
            $images = $images[$attribute['id_product_attribute']];
            if (is_array($images) && count($images)) {
                $attribute['id_image'] = $images[0]['id_image'];
            }
        }
        if (!$attribute['id_image']) {
            $cover = Product::getCover((int)$product->id);
            $attribute['id_image'] = $cover['id_image'];
        }
        $attribute['attribute_reference'] = '';
        if (isset($attribute['reference'])) {
            $attribute['attribute_reference'] = $attribute['reference'];
        }
        return $attribute;
    }
}
