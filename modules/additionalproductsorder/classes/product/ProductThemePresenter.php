<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoProductThemePresenter extends LinevenApoProductPresenter
{
    /**
     * Return products from list
     *
     * @param array $search_results Search results
     * @return mixed Products
     */
    public function present($search_results = null)
    {
        $sections = null;
        if ($search_results != null && is_array($search_results)) {
            reset($search_results);
            $method = key($search_results);
            $results = current($search_results);
            $context = Context::getContext();
            $assembler = new ProductAssembler($context);
            $presenterFactory = new ProductPresenterFactory($context);
            $presentationSettings = $presenterFactory->getPresentationSettings();
            if (version_compare(_PS_VERSION_, '1.7.6', '>=')) {
                $presenter = new PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductListingPresenter(
                    new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                        $context->link
                    ),
                    $context->link,
                    new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
                    new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
                    $context->getTranslator()
                );
            } else {
                $presenter = new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
                    new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                        $context->link
                    ),
                    $context->link,
                    new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
                    new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
                    $context->getTranslator()
                );
            }
            foreach ($results as $section_code => $section) {
                $products_results = $section['results'];
                $section_products_found = $products_results->getProductsFound();
                $section_products = $products_results->getProducts();
                $products = array();
                $new_section = array('products' => $products);
                if ($method == 'separate') {
                    $new_section['title'] = $section['title'];
                }
                $count_products_to_display = 0;
                // Get the product detail
                foreach ($section_products as $row) {
                    if ($count_products_to_display < $this->max_products) {
                        $id_product = $row['id_product'];
                        if (!$this->isProductPresented($id_product)) {
                            $product = Product::getProductProperties($this->id_lang, $row);
                            $association_details = null;
                            if ($section_products_found[$id_product]['SECTION'] == AdditionalProductsOrder::$research_priority_associations) {
                                $association_details = $section_products_found[$id_product];
                            }
                            $product['id_association'] = (isset($association_details['id']) ? $association_details['id'] : 0);
                            if (self::checkAvailabilityByQuantity(
                                $product['allow_oosp'],
                                $product['quantity']
                            )) {
                                $product_for_template = $presenter->present(
                                    $presentationSettings,
                                    $assembler->assembleProduct($product),
                                    $context->language
                                );
                                // Redefine name and description
                                if (is_array($association_details)) {
                                    // Specific name
                                    if (array_key_exists('name', $association_details)) {
                                        $name = LinevenApoTranslator::getTranslationFromArray(unserialize($association_details['name']));
                                        if (Tools::strlen(trim($name)) != 0) {
                                            $product_for_template['name'] = $name;
                                        }
                                    }
                                    // Specific description
                                    if (array_key_exists('short_description', $association_details)) {
                                        $description = LinevenApoTranslator::getTranslationFromArray(unserialize($association_details['short_description']));
                                        if (Tools::strlen(trim($description)) != 0) {
                                            $product_for_template['description_short'] = htmlspecialchars($description);
                                        }
                                    }
                                }
                                $products[] = $product_for_template;
                                $count_products_to_display++;
                            }
                        }
                    }
                }
                if (count($products)) {
                    $new_section['products'] = $products;
                }
                $sections[$section_code] = $new_section;
            }
            return $sections;
        }
        return array();
    }
}
