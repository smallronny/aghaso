<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoProductSearchResult
{
    private $products_found = null;
    private $products = array();

    /**
     * Set products.
     * @param array $products Products
     * @return $this
     */
    public function setProducts(array $products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * Get products.
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set products found.
     * @param array $products Products
     * @return $this
     */
    public function setProductsFound(array $products)
    {
        $this->products_found = $products;
        return $this;
    }

    /**
     * Get products found.
     * @return array
     */
    public function getProductsFound()
    {
        return $this->products_found;
    }

    /**
     * Get products count.
     * @return int
     */
    public function getCount()
    {
        if (is_array($this->products)) {
            return count($this->products);
        }
        return 0;
    }
}
