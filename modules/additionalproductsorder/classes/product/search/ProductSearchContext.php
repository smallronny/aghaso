<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoProductSearchContext
{
    private static $instance; /* Instanciated class */

    private $id_shop_group;
    private $id_shop;
    private $id_lang;
    private $cart_amount = 0;

    private $visibilities;
    private $visibilities_sql;

    /**
     * Get a singleton instance of Context object
     *
     * @return Context
     */
    public static function getContext()
    {
        if (!isset(self::$instance)) {
            self::$instance = new LinevenApoProductSearchContext();
        }
        return self::$instance;
    }

    /**
     * LinevenApoProductSearchContext constructor.
     * @param Context|null $context
     */
    public function __construct(Context $context = null)
    {
        if ($context == null) {
            $context = Context::getContext();
        }
        if ($context) {
            $this->id_shop_group = $context->shop->getContextShopGroupID();
            $this->id_shop = $context->shop->getContextShopID();
            $this->id_lang = $context->language->id;
        }
        $this->loadDefaults();
    }

    /**
     * Load defaults.
     * @return void
     */
    private function loadDefaults()
    {
        // Visibilities
        $this->visibilities = array();
        $this->visibilities_sql = '0';
        if (Configuration::get('LINEVEN_APO_AVL_VISIBILITY') !== false) {
            $this->visibilities = unserialize(Configuration::get('LINEVEN_APO_AVL_VISIBILITY'));
            if (count($this->visibilities)) {
                $array_visibilities = array_map(function ($value) {
                    return '"'.pSQL($value).'"';
                }, $this->visibilities);
                $this->visibilities_sql = implode(',', $array_visibilities);
            }
        }
    }

    /**
     * Get visibilities.
     * @return array
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Get visibilities for sql.
     * @return array
     */
    public function getVisibilitiesSql()
    {
        return $this->visibilities_sql;
    }

    /**
     * Set id shop.
     * @param int $id_shop Shop id
     * @return $this
     */
    public function setIdShop($id_shop)
    {
        $this->id_shop = $id_shop;
        return $this;
    }

    /**
     * Get id shop.
     * @return int
     */
    public function getIdShop()
    {
        return $this->id_shop;
    }

    /**
     * Set id lang.
     * @param int $id_lang Lang id
     * @return $this
     */
    public function setIdLang($id_lang)
    {
        $this->id_lang = $id_lang;
        return $this;
    }

    /**
     * Get id lang.
     * @return int
     */
    public function getIdLang()
    {
        return $this->id_lang;
    }

    /**
     * Set cart amount.
     * @param float $cart_amount Cart amount
     * @return $this
     */
    public function setCartAmount($cart_amount)
    {
        $this->cart_amount = $cart_amount;
        return $this;
    }

    /**
     * Get cart amount.
     * @return float
     */
    public function getCartAmount()
    {
        return $this->cart_amount;
    }
}
