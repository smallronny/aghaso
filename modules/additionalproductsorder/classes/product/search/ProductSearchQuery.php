<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoProductSearchQuery
{
    private $limit;
    private $sort_display_method;
    private $sort_display_way;
    private $is_random_associations;
    private $is_random_search;

    private $id_product;
    private $related_products;
    private $products_to_exclude;
    private $hook;

    public function __construct()
    {
        $this->limit = 80;
        $this->related_products = array();
        $this->products_to_exclude = array();
        $this->hook = null;
        $this->id_product = 0;
        $this->is_random_associations = false;
        $this->is_random_search = false;
        $this->sort_display_method = AdditionalProductsOrder::$sort_display_default;
        $this->sort_display_way = AdditionalProductsOrder::$sort_way_asc;
    }

    /**
     * Set limit.
     * @param int $limit Limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit + 20;
        return $this;
    }

    /**
     * Get limit.
     * @return int
     */
    public function getLimit()
    {
        return (int)$this->limit;
    }

    /**
     * Set hook.
     * @param string $hook Hook
     * @return $this
     */
    public function setHook($hook)
    {
        $this->hook = $hook;
        return $this;
    }

    /**
     * Get hook.
     * @return sring
     */
    public function getHook()
    {
        return $this->hook;
    }

    /**
     * Set is random associations.
     * @param boolean $is_random_associations Randomly associations
     * @return $this
     */
    public function setRandomAssociations($is_random_associations)
    {
        $this->is_random_associations = $is_random_associations;
        return $this;
    }

    /**
     * Is random association.
     * @return boolean
     */
    public function isRandomAssocations()
    {
        return $this->is_random_associations;
    }

    /**
     * Set is random search.
     * @param boolean $is_random_search Randomly search
     * @return $this
     */
    public function setRandomSearch($is_random_search)
    {
        $this->is_random_search = $is_random_search;
        return $this;
    }

    /**
     * Is random search.
     * @return boolean
     */
    public function isRandomSearch()
    {
        return $this->is_random_search;
    }

    /**
     * Set sort display method.
     * @param string $sort_display_method Sort display method
     * @param string $sort_display_way Sort display way
     * @return $this
     */
    public function setSortDisplayMethod($sort_display_method, $sort_display_way)
    {
        $this->sort_display_method = $sort_display_method;
        $this->sort_display_way = $sort_display_way;
        return $this;
    }

    /**
     * Get sort display method.
     * @return string
     */
    public function getSortDisplayMethod()
    {
        return $this->sort_display_method;
    }

    /**
     * Get sort display way.
     * @return string
     */
    public function getSortDisplayWay()
    {
        return $this->sort_display_way;
    }

    /**
     * Add related product.
     * @param int $id_product
     * @return void
     */
    public function addRelatedProduct($id_product)
    {
        if (!in_array((int)$id_product, $this->related_products)) {
            $this->related_products[] = (int)$id_product;
        }
    }

    /**
     * Set default related products.
     * @param array $products
     * @return void
     */
    public function setRelatedProducts($products)
    {
        if (is_array($products) && count($products)) {
            foreach ($products as $product) {
                $this->addRelatedProduct((int)$product['id_product']);
            }
        }
    }

    /**
     * Add product to exclude.
     * @param int $id_product
     * @return void
     */
    public function addProductToExclude($id_product)
    {
        if (!in_array((int)$id_product, $this->related_products)) {
            $this->products_to_exclude[] = (int)$id_product;
        }
    }

    /**
     * Set default related products.
     * @param array $products
     * @return void
     */
    public function setProductsToExlude($products)
    {
        if (is_array($products) && count($products)) {
            foreach ($products as $product) {
                $this->addProductToExclude((int)$product['id_product']);
            }
        }
    }

    /**
     * Set id product
     * @param $id_product Product id
     * @return $this
     */
    public function setIdProduct($id_product)
    {
        $this->id_product = (int)$id_product;
        $this->addRelatedProduct((int)$id_product);
        return $this;
    }

    /**
     * Get related products.
     * @return array
     */
    public function getRelatedProducts()
    {
        return $this->related_products;
    }

    /**
     * Get products to exclude.
     * @return array
     */
    public function getProductsToExclude()
    {
        return $this->products_to_exclude;
    }

    /**
     * Get sql related products ids.
     * @return string
     */
    public function getRelatedProductsSqlIds()
    {
        return implode(',', array_map('intval', $this->related_products));
    }

    /**
     * Get sql products to exclude ids.
     * @return string
     */
    public function getProductsToExcludeSqlIds()
    {
        if (is_array($this->products_to_exclude) && count($this->products_to_exclude)) {
            return implode(',', array_map('intval', $this->products_to_exclude));
        }
        return 0;
    }
}
