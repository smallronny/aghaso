<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoCrossSellingProductsSearch
{
    private $products_displayed;
    private $is_results_separated;
    private $sections;
    private $hook_code;

    /**
     * LinevenApoCrossSellingProductSearchProvider constructor.
     * @param string $hook_code Hook code
     * @return void
     */
    public function __construct($hook_code)
    {
        $this->products_displayed = array();
        $this->is_results_separated = false;
        $this->sections = array();
        $this->hook_code = $hook_code;
    }

    /**
     * Set is results separated
     * @param boolean $is_results_separated Is results separated
     * @return $this
     */
    public function setResultsSeparated($is_results_separated)
    {
        $this->is_results_separated = $is_results_separated;
        return $this;
    }

    /**
     * Is results separated.
     * @return int
     */
    public function isResultsSeparated()
    {
        return $this->is_results_separated;
    }

    /**
     * Set sections.
     * @param boolean $sections Sections
     * @param array $sections_titles Sections titles
     * @return $this
     */
    public function setSections($sections, $sections_titles)
    {
        foreach ($sections as $section_code) {
            if ($section_code != AdditionalProductsOrder::$research_priority_nothing) {
                $this->sections[$section_code] = array(
                    'code' => $section_code,
                    'title' => $sections_titles[$section_code],
                    'results' => array()
                );
            }
        }
        return $this;
    }

    /**
     * Is section exists.
     * @param name $name Section name
     * @return $this
     */
    public function isSectionExists($name)
    {
        if (isset($this->sections[$name])) {
            return true;
        }
        return false;
    }

    /**
     * Run query.
     * @param LinevenApoProductSearchContext $context Context
     * @param LinevenApoProductSearchQuery $query Query
     * @return array
     */
    public function runQuery(
        LinevenApoProductSearchContext $context,
        LinevenApoProductSearchQuery $query
    ) {
        if (count($context->getVisibilities()) &&
            count($this->sections)) {
            $final_results = array();
            $research_section_executed = array();
            foreach ($this->sections as $section_code => $section_options) {
                $section_results = array();
                if ($section_code == AdditionalProductsOrder::$research_priority_accessories &&
                    !in_array(AdditionalProductsOrder::$research_priority_accessories, $research_section_executed) &&
                    Configuration::get('LINEVEN_APO_'.$this->hook_code.'_ACTIVE_ACCESSORIES')) {
                    // Add accessories
                    $section_results = $this->addAccessories($context, $query);
                    $research_section_executed[] = AdditionalProductsOrder::$research_priority_accessories;
                }
                if ($section_code == AdditionalProductsOrder::$research_priority_associations &&
                    !in_array(AdditionalProductsOrder::$research_priority_associations, $research_section_executed) &&
                    Configuration::get('LINEVEN_APO_'.$this->hook_code.'_ACTIVE_ASSOCIATIONS')) {
                    // Add associations
                    $section_results = $this->addAssociations($context, $query);
                    $research_section_executed[] = AdditionalProductsOrder::$research_priority_associations;
                }
                if ($section_code == AdditionalProductsOrder::$research_priority_purchased_together &&
                    !in_array(AdditionalProductsOrder::$research_priority_purchased_together, $research_section_executed) &&
                    Configuration::get('LINEVEN_APO_'.$this->hook_code.'_ACTIVE_PURCHASED_TOGETHER')) {
                    // Add purchased together
                    $section_results = $this->addPurchasedTogether($context, $query);
                    $research_section_executed[] = AdditionalProductsOrder::$research_priority_purchased_together;
                }
                if (is_array($section_results) && count($section_results)) {
                    $count = 0;
                    if ($this->isResultsSeparated()) {
                        $final_results = array();
                    }
                    foreach ($section_results as $product) {
                        if ($count <= $query->getLimit()) {
                            $id_product = $product['id_product'];
                            if (!in_array($id_product, $query->getRelatedProducts())) {
                                if ($this->addProductDisplayed($id_product, $section_code)) {
                                    $product['SECTION'] = $section_code;
                                    $final_results[$id_product] = $product;
                                    $count++;
                                }
                            }
                        } else {
                            break;
                        }
                    }
                    if ($this->isResultsSeparated()) {
                        $products_result = new LinevenApoProductSearchResult();
                        $products_result->setProductsFound($final_results);
                        $products_result->setProducts($this->getProducts(
                            $context,
                            $query,
                            $final_results
                        ));
                        if ($products_result->getCount()) {
                            $this->sections[$section_code]['results'] = $products_result;
                        }
                    }
                } else {
                    unset($this->sections[$section_code]);
                }
            }
            if (!$this->isResultsSeparated()) {
                if (count($final_results)) {
                    $products_result = new LinevenApoProductSearchResult();
                    $products_result->setProductsFound($final_results);
                    $products_result->setProducts($this->getProducts(
                        $context,
                        $query,
                        $final_results
                    ));
                    return array('regroup' => array('SECTION' => array('results' => $products_result)));
                }
            } else {
                foreach ($this->sections as $key => $value) {
                    if ((!is_array($value))) {
                        unset($this->sections[$key]);
                    }
                }
                if (count($this->sections)) {
                    return array('separate' => $this->sections);
                }
            }
        }
        return array();
    }

    /**
     * Add product displayed.
     * @param int $id_product Product id
     * @param string $section_code Section code
     * @return array
     */
    private function addProductDisplayed($id_product, $section_code = null)
    {
        $id_product = (int)$id_product;
        if ($id_product) {
            if ($this->isResultsSeparated() && $section_code != null) {
                if (isset($this->products_displayed[$section_code])) {
                    if (!in_array($id_product, $this->products_displayed[$section_code])) {
                        $this->products_displayed[$section_code][] = $id_product;
                        return true;
                    }
                } else {
                    $this->products_displayed[$section_code][] = $id_product;
                    return true;
                }
            } else {
                if (!in_array($id_product, $this->products_displayed)) {
                    $this->products_displayed[] = $id_product;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Add accessories products.
     * @param LinevenApoProductSearchContext $context Context
     * @param LinevenApoProductSearchQuery $query Query
     * @return array
     */
    public function addAccessories(
        LinevenApoProductSearchContext $context,
        LinevenApoProductSearchQuery $query
    ) {
        if (count($query->getRelatedProducts())) {
            $sql = 'SELECT p.`id_product`
                    FROM `'._DB_PREFIX_.'accessory`
                    LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product` = `id_product_2`)
                    '.Shop::addSqlAssociation('product', 'p').'
                    WHERE `id_product_1` in ('.pSQL($query->getRelatedProductsSqlIds()).')
                    AND p.`id_product` NOT IN ('.pSQL($query->getRelatedProductsSqlIds()).') '.
                    (count($query->getProductsToExclude()) ? 'AND p.`id_product` NOT IN ('.pSQL($query->getProductsToExcludeSqlIds()).')' : '').'
                    AND product_shop.`id_shop` = '.(int)$context->getIdShop().'
                    AND product_shop.`active` = 1
                    AND product_shop.`available_for_order` = 1
                    AND product_shop.`visibility` IN ('.$context->getVisibilitiesSql().')
                    '.($query->isRandomSearch() ? 'ORDER BY RAND()' : '').'
                    LIMIT '.(int)$query->getLimit();
            return Db::getInstance()->executeS($sql);
        }
    }

    /**
     * Add associations.
     * @param LinevenApoProductSearchContext $context Context
     * @param LinevenApoProductSearchQuery $query Query
     *
     * @return array
     */
    public function addAssociations(
        LinevenApoProductSearchContext $context,
        LinevenApoProductSearchQuery $query
    ) {
        // Where shop
        $where_shop = 'WHERE ((apo.`id_shop_group` is null and apo.`id_shop` is null) or ';
        $where_shop .= '(apo.`id_shop_group` = '.(int)Context::getContext()->shop->getContextShopGroupID().' 
                            and apo.`id_shop` is null) or ';
        $where_shop .= '(apo.`id_shop` = '.(int)Context::getContext()->shop->getContextShopID().'))';

        // With customers groups
        $left_join_groups = '';
        $where_groups = '';
        if (!empty(Context::getContext()->customer)) {
            $customer = Context::getContext()->customer;
            $groups = $customer->getGroups();
            $id_groups = implode(',', array_map('intval', $groups));
            if ($id_groups == '') {
                $id_groups = 0;
            }
            $left_join_groups = ' LEFT JOIN `'._DB_PREFIX_.'lineven_apo_groups` apo_groups
                    ON (apo.`id` = apo_groups.`id_association`) ';
            $where_groups = ' AND (apo.`is_active_groups` = 0 OR (apo.`is_active_groups` = 1 AND 
                    apo_groups.`id_group` in ('.$id_groups.'))) ';
        }
        $where_amount = ' AND ((apo.`minimum_amount` = 0 OR apo.`minimum_amount` <= '.pSQL($context->getCartAmount()).')
                AND (apo.`maximum_amount` = 0 OR apo.`maximum_amount` >= '.pSQL($context->getCartAmount()).'))';
        $sql = 'SELECT DISTINCT               
                apo.`id`, `name`, `short_description`, 
                `id_related_category`, `id_related_product`,
                displayed_category_product.`id_product` as `displayed_category_id_product`,
                displayed_products_list.`id_displayed_product` as `displayed_lists_id_product`,
                `minimum_amount`, `maximum_amount`, `is_active_groups`
                FROM `'._DB_PREFIX_.'lineven_apo` apo
                LEFT JOIN `'._DB_PREFIX_.'category_product` related_category ON (related_category.`id_category` = apo.`id_related_category`)
                LEFT JOIN `'._DB_PREFIX_.'category_shop` as related_category_shop ON (related_category.`id_category` = related_category_shop.`id_category` AND related_category_shop.`id_shop` = '.Context::getContext()->shop->getContextShopID().')
                LEFT JOIN `'._DB_PREFIX_.'category_product` displayed_category_product ON (displayed_category_product.`id_category` = apo.`id_displayed_category`)
                LEFT JOIN `'._DB_PREFIX_.'category_shop` as displayed_category_product_shop ON (displayed_category_product.`id_category` = displayed_category_product_shop.`id_category` AND displayed_category_product_shop.`id_shop` = '.Context::getContext()->shop->getContextShopID().') 
                LEFT JOIN `'._DB_PREFIX_.'lineven_apo_displayed_products` displayed_products_list ON (apo.`id` = displayed_products_list.`id_association`) '.
                $left_join_groups.
                $where_shop.$where_groups.$where_amount.
                ' AND (NULLIF(apo.`hooks`, \'\') IS NULL OR apo.`hooks` LIKE (\'%'.$query->getHook().'%\'))  
                AND (
                (
                    apo.`id_related_category` is null AND apo.`id_related_product` is null
                )'.
                ($query->getRelatedProductsSqlIds() != ''
                    ? 'OR
                        (
                            apo.`id_related_category` is not null AND
                            related_category.`id_product` in ('.pSQL($query->getRelatedProductsSqlIds()).') 
                        ) OR
                        (
                            apo.`id_related_product` is not null AND
                            apo.`id_related_product` in ('.pSQL($query->getRelatedProductsSqlIds()).')
                        )'
                    : '').'
                ) 
                ORDER BY apo.`order_display` ASC, displayed_products_list.`order_display` ASC ';

        $apo_list = Db::getInstance()->ExecuteS($sql);
        $results = array();

        // If related products created
        if (count($apo_list) > 0) {
            // Randomize
            if ($query->isRandomAssocations()) {
                shuffle($apo_list);
            }
            for ($i = 0; $i < count($apo_list); $i++) {
                $id_product_to_display = (int)$apo_list[$i]['displayed_lists_id_product'];
                if ((int)$apo_list[$i]['displayed_category_id_product'] != 0) {
                    $id_product_to_display = (int)$apo_list[$i]['displayed_category_id_product'];
                }
                if ($id_product_to_display != 0) {
                    if (!array_key_exists($id_product_to_display, $results)
                        && !in_array($id_product_to_display, $query->getRelatedProducts())
                        && !in_array($id_product_to_display, $query->getProductsToExclude())
                    ) {
                        $results[$id_product_to_display] = array_merge(
                            array('id_product' => $id_product_to_display),
                            $apo_list[$i]
                        );
                    }
                }
                if (count($this->products_displayed) >= $query->getLimit()) {
                    break;
                }
            }
        }
        return $results;
    }

    /**
     * Add purchased together products.
     * @param LinevenApoProductSearchContext $context Context
     * @param LinevenApoProductSearchQuery $query Query
     * @return void
     */
    public function addPurchasedTogether(
        LinevenApoProductSearchContext $context,
        LinevenApoProductSearchQuery $query
    ) {
        if (count($query->getRelatedProducts())) {
            $q_orders = 'SELECT o.id_order
                  FROM '._DB_PREFIX_.'orders o
                    LEFT JOIN '._DB_PREFIX_.'order_detail od ON (od.id_order = o.id_order)
                    WHERE o.valid = 1
                    AND od.product_id IN ('.pSQL($query->getRelatedProductsSqlIds()).')';
            $orders = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($q_orders);
            if (0 < count($orders)) {
                $list = '';
                foreach ($orders as $order) {
                    $list .= (int)$order['id_order'].',';
                }
                $list = rtrim($list, ',');
                $sql = 'SELECT DISTINCT od.product_id as id_product
                    FROM '._DB_PREFIX_.'order_detail od
                    LEFT JOIN '._DB_PREFIX_.'product p ON (p.id_product = od.product_id)
                    '.Shop::addSqlAssociation('product', 'p').'
                    WHERE od.id_order IN ('.$list.')
                    AND od.`product_id` NOT IN ('.pSQL($query->getRelatedProductsSqlIds()).')
                    AND product_shop.active = 1
                    AND product_shop.`available_for_order` = 1
                    '.($query->isRandomSearch() ? 'ORDER BY RAND()' : '').'
                    LIMIT '.(int)$query->getLimit();
                return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
            }
        }
    }

    /**
     * Search products to display.
     *
     * @param LinevenApoProductSearchContext $context Context
     * @param LinevenApoProductSearchQuery $query Query
     * @param array $products Products
     * @return array
     */
    private function getProducts(
        LinevenApoProductSearchContext $context,
        LinevenApoProductSearchQuery $query,
        $products = null
    ) {
        if ($products && count($products)) {
            $in = implode(',', array_map('intval', array_keys($products)));
            $order_by = ' FIND_IN_SET(p.`id_product`, \''.$in.'\')';
            if ($query->getSortDisplayMethod() == AdditionalProductsOrder::$sort_display_name) {
                $order_by = ' pl.`name` '.$query->getSortDisplayWay();
            }
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
                    MAX(product_attribute_shop.id_product_attribute) id_product_attribute,
                    product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`,
                    pl.`description_short`, pl.`available_now`,
                    pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,
                    pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image,
                    DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
                    INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT'))
                    ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
                        DAY)) > 0 AS new, product_shop.price AS orderprice
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                ON (p.`id_product` = pa.`id_product`)
                '.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
                '.Product::sqlStock('p', 'product_attribute_shop', false, Context::getContext()->shop).'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
                    ON (p.`id_product` = pl.`id_product`
                    AND pl.`id_lang` = '.(int)$context->getIdLang().Shop::addSqlRestrictionOnLang('pl').')
                LEFT JOIN `'._DB_PREFIX_.'image` i
                    ON (i.`id_product` = p.`id_product`)'.
                Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
                WHERE product_shop.`id_shop` = '.(int)$context->getIdShop().'
                    AND product_shop.`visibility` IN ('.$context->getVisibilitiesSql().')
                    AND product_shop.`active` = 1
                    AND product_shop.`available_for_order` = 1
                    AND p.`id_product` in ('.$in.')
                GROUP BY product_shop.id_product
                ORDER BY '.$order_by;
            $results = Db::getInstance()->executeS($sql);
            if ($query->getSortDisplayMethod() == AdditionalProductsOrder::$sort_display_price) {
                Tools::orderbyPrice($results, $query->getSortDisplayWay());
            }
            if ($query->getSortDisplayMethod() == AdditionalProductsOrder::$sort_display_random) {
                shuffle($results);
            }
            return $results;
        }
        return array();
    }
}
