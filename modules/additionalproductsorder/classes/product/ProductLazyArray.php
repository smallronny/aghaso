<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

use PrestaShop\PrestaShop\Adapter\Presenter\AbstractLazyArray;

class LinevenApoProductLazyArray extends AbstractLazyArray
{
    /**
     * @var array
     */
    protected $product;

    public function __construct(
        array $product
    ) {
        $this->product = $product;
        parent::__construct();
        $this->appendArray($this->product);
    }

    /**
     * @arrayAccess
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->product['id_product'];
    }
}
