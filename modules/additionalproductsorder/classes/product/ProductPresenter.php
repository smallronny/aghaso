<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoProductPresenter
{
    protected $module_context;
    protected $max_products = 100;
    protected $id_lang = null;
    protected $presented_products = array();
    protected $statistics_is_active = false;
    protected $statistics_is_count_all = false;
    protected $statistics_lineven_cookie = null;

    public static $statistics_products_displayed = array();

    /**
     * Constructor.
     * @param int $max_products Max products
     * @param int $id_lang Id lang
     */
    public function __construct($max_products = 100, $id_lang = null)
    {
        $this->module_context = LinevenApoContext::getContext();
        $this->max_products = 100;
        if ($max_products < 100 && $max_products != 0) {
            $this->max_products = $max_products;
        }
        $this->id_lang = $this->module_context->current_id_lang;
        if ($id_lang != null) {
            $this->id_lang = $id_lang;
        }
        $this->presented_products = array();

        // Statistics
        $this->statistics_lineven_cookie = null;
        $this->statistics_is_active = (Configuration::get('LINEVEN_APO_STATS_ACTIVE') && Configuration::get('LINEVEN_APO_STATS_IS_DISPLAYED'));
        $this->statistics_is_count_all = Configuration::get('LINEVEN_APO_STATS_ALL_DISPLAYED');
        if ($this->statistics_is_active && !$this->statistics_is_count_all) {
            $this->statistics_lineven_cookie = new Cookie('lineven_cookie', '', 0);
            LinevenApoProductPresenter::$statistics_products_displayed = array();
            if ($this->statistics_lineven_cookie->exists() && isset($this->statistics_lineven_cookie->additionalproductsorder_statistics_displayed)) {
                LinevenApoProductPresenter::$statistics_products_displayed = unserialize(
                    $this->statistics_lineven_cookie->additionalproductsorder_statistics_displayed
                );
            }
        }
    }

    /**
     * Is product presented.
     *
     * @param int $id_product Product id
     * @return boolean
     */
    protected function isProductPresented($id_product)
    {
        if (!in_array($id_product, $this->presented_products)) {
            $this->presented_products[] = $id_product;
            return false;
        }
        return true;
    }

    /**
     * Check availability by quantity.
     *
     * @param array $association_row Association row
     * @param integer $allow_oosp Allow oosp
     * @param integer $quantity Quantity
     * @return boolean
     */
    protected static function checkAvailabilityByQuantity($allow_oosp, $quantity)
    {
        if (Configuration::get('LINEVEN_APO_AVL_ZERO_QTY')) {
            if ($quantity > 0 || ($allow_oosp && $quantity <= 0)) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * Add statistics for display.
     *
     * @param int $id_association Association id
     * @param int $id_product Product id
     * @void
     */
    protected function addStatisticsForDisplay($id_association, $id_product)
    {
        if ($this->statistics_is_active) {
            if ($this->statistics_is_count_all) {
                LinevenApoStatistics::setData((int)$id_association, $id_product, 'D');
            } else {
                if (!$this->statistics_is_count_all && is_array(LinevenApoProductPresenter::$statistics_products_displayed) &&
                    !in_array((int)$id_product, LinevenApoProductPresenter::$statistics_products_displayed)) {
                    LinevenApoProductPresenter::$statistics_products_displayed[] = (int)$id_product;
                    LinevenApoStatistics::setData((int)$id_association, (int)$id_product, 'D');
                    $this->statistics_lineven_cookie->additionalproductsorder_statistics_displayed = serialize(LinevenApoProductPresenter::$statistics_products_displayed);
                }
            }
        }
    }

    /**
     * Get thumbnails image product
     * @param $id_product int Product id
     * @param $id_image int Image id
     * @return mixed
     */
    public static function getThumbnailImage($id_product, $id_image = null)
    {
        if (empty($id_image)) {
            $id_image = Product::getCover($id_product);
            if (is_array($id_image) && !empty($id_image['id_image'])) {
                $id_image = (int)$id_image['id_image'];
            }
        }
        $imageManager = new PrestaShop\PrestaShop\Adapter\ImageManager(new PrestaShop\PrestaShop\Adapter\LegacyContext());
        return $imageManager->getThumbnailForListing($id_image);
    }
}
