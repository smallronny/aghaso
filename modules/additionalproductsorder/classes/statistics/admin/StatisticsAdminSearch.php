<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoStatisticsAdminSearch
{

    protected $begin_date;
    protected $end_date;
    protected $id_association;
    protected $id_product;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->setBeginDate(null);
        $this->setEndDate(null);
        $this->id_association = null;
        $this->id_product = null;
    }

    /**
     * Set begin date.
     * @param Date $begin_date Date
     */
    public function setBeginDate($begin_date)
    {
        if ($begin_date == null || !Validate::isDate($begin_date)) {
            $date = new DateTime();
            $begin_date = $date->format('Y-m-01');
        }
        $this->begin_date = $begin_date;
    }

    /**
     * Set end date.
     * @param Date $end_date Date
     */
    public function setEndDate($end_date)
    {
        if ($end_date == null || !Validate::isDate($end_date)) {
            $date = new DateTime();
            // Last day of the month
            $end_date = $date->format('Y-m-t');
            $date = new DateTime($end_date);
            $interval = new DateInterval('P1D');
            $date->add($interval);
            $end_date = $date->format('Y-m-d');
        } else {
            $date = new DateTime($end_date);
            $interval = new DateInterval('P1D');
            $date->add($interval);
            $end_date = $date->format('Y-m-d');
        }
        $this->end_date = $end_date;
    }

    /**
     * Set id association.
     * @param int $id_association Association id;
     */
    public function setIdAssociations($id_association)
    {
        $this->id_association = $id_association;
    }

    /**
     * Set id product.
     * @param int $id_product Product id;
     */
    public function setIdProduct($id_product)
    {
        $this->id_product = $id_product;
    }

    /**
     * Search by days.
     *
     * @return array
     */
    public function searchByDays()
    {
        return $this->runQuery('BY_DAYS');
    }

    /**
     * Search.
     *
     * @return array
     */
    public function search()
    {
        return $this->runQuery();
    }

    /**
     * Run query.
     *
     * @param string $view View
     *
     * @return array
     */
    private function runQuery($view = null)
    {
        $sql = '';
        $where = array();
        $sql_where = '';
        if (Context::getContext()->shop->getContextShopID() != null) {
            $where[] = ' apo.`id_shop` = '.(int)Context::getContext()->shop->getContextShopID().' ';
        } else {
            if (Context::getContext()->shop->getContextShopGroupID() != null) {
                $where[] = ' apo.`id_shop_group` = '.(int)Context::getContext()->shop->getContextShopGroupID().' ';
            }
        }
        if ($this->id_association != null) {
            $where[] = ' apo.`id_association` = '.(int)$this->id_association;
        }
        if ($this-> id_product != null) {
            $where[] = ' apo.`id_product` = '.(int)$this->id_product;
        }
        if ($this->begin_date != null) {
            $where[] = ' apo.`date_add` >= "'.pSQL($this->begin_date).'"';
        }
        if ($this->end_date != null) {
            $where[] = ' apo.`date_add` < "'.pSQL($this->end_date).'"';
        }
        if (is_array($where) && count($where)) {
            $sql_where = ' WHERE '.implode(' AND ', $where);
        }
        $view_field = '';
        if ($view == 'BY_DAYS') {
            $view_field = ' DAY(apo.`date_add`) AS view, ';
        }
        $group_by = array();
        $sql_group_by = '';
        if ($view != null) {
            $group_by[] = 'view';
        }
        if (Context::getContext()->shop->getContextShopGroupID() != null) {
            $group_by[] = 'apo.`id_shop_group`';
        }
        if (Context::getContext()->shop->getContextShopID() != null) {
            $group_by[] = 'apo.`id_shop`';
        }
        if (is_array($group_by) && count($group_by)) {
            $sql_group_by = ' GROUP BY '.implode(' , ', $group_by);
        }
        $sql = 'SELECT apo.`id_shop_group`, apo.`id_shop`, '.$view_field.'  
            SUM(apo.`displayed`) as sum_displayed,
            SUM(apo.`add_to_cart`) as sum_add_to_cart,
            SUM(apo.`click_to_view`) as sum_click_to_view
            FROM `'._DB_PREFIX_.'lineven_apo_statistics` apo '.
            $sql_where.$sql_group_by.' 
            ORDER BY apo.`date_add` ';
        $statistics = Db::getInstance()->ExecuteS($sql);
        if (count($statistics)) {
            if ($view == null) {
                return $statistics[0];
            }
            if ($view == 'BY_DAYS') {
                $statistics_formated = array();
                for ($i = 0; $i < count($statistics); $i++) {
                    if (!isset($statistics_formated[$statistics[$i]['view']])) {
                        $statistics_formated[$statistics[$i]['view']] = array(
                            'sum_displayed' => (int)$statistics[$i]['sum_displayed'],
                            'sum_add_to_cart' => (int)$statistics[$i]['sum_add_to_cart'],
                            'sum_click_to_view' => (int)$statistics[$i]['sum_click_to_view']
                        );
                    } else {
                        $statistics_formated[$statistics[$i]['view']]['sum_displayed'] += (int)$statistics[$i]['sum_displayed'];
                        $statistics_formated[$statistics[$i]['view']]['sum_add_to_cart'] += (int)$statistics[$i]['sum_add_to_cart'];
                        $statistics_formated[$statistics[$i]['view']]['sum_click_to_view'] += (int)$statistics[$i]['sum_click_to_view'];
                    }
                }
                return $statistics_formated;
            }
        }
        return null;
    }
}
