<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoStatisticsAdminListingPresenter extends LinevenApoModuleAdminListingPresenter
{
    protected $clean_cron_file;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->clean_cron_file = '';
    }

    /**
     * Set clean cron file
     * @param string $cron_file Cron file
     */
    public function setCleanCronFile($cron_file)
    {
        $this->clean_cron_file = $cron_file;
    }

    /**
     * Present.
     * @return array
     */
    public function present()
    {
        parent::present();
        $this->presenter['clean_cron_file'] = $this->clean_cron_file;
        return $this->presenter;
    }

    /**
     * Present.
     * @param array $datas Datas
     * @return array
     */
    public function presentList($datas)
    {
        $translator = new LinevenApoTranslator();
        $statistics = array();
        for ($i = 0; $i < count($datas); $i++) {
            $statistic = $datas[$i];
            $group_shop = $translator->l('All shops', 'StatisticsAdminListingPresenter');
            if ($datas[$i]['id_shop'] != null) {
                $group_shop = $datas[$i]['shop_name'];
            } else {
                if ($datas[$i]['id_shop_group'] != null) {
                    $group_shop = $datas[$i]['shop_group_name'];
                }
            }
            $statistic['group_shop'] = $group_shop;
            $statistic['class'] = 'lapo-statistics-list-item';
            $statistics[] = $statistic;
        }
        return $statistics;
    }

    /**
     * Present for export.
     * @param array $datas Datas
     * @return array
     */
    public function presentExport($datas)
    {
        $translator = new LinevenApoTranslator();
        $statistics = array(
            array(
                $translator->l('Group / Shop', 'StatisticsAdminListingPresenter'),
                $translator->l('Association Id.', 'StatisticsAdminListingPresenter'),
                $translator->l('Product Id.', 'StatisticsAdminListingPresenter'),
                $translator->l('Product', 'StatisticsAdminListingPresenter'),
                $translator->l('Display', 'StatisticsAdminListingPresenter'),
                $translator->l('In cart', 'StatisticsAdminListingPresenter'),
                $translator->l('View', 'StatisticsAdminListingPresenter'),
                $translator->l('Date', 'StatisticsAdminListingPresenter')
            )
        );
        for ($i = 0; $i < count($datas); $i++) {
            $statistic = array();
            $group_shop = $translator->l('All shops', 'StatisticsAdminListingPresenter');
            if ($datas[$i]['id_shop'] != null) {
                $group_shop = $datas[$i]['shop_name'];
            } else {
                if ($datas[$i]['id_shop_group'] != null) {
                    $group_shop = $datas[$i]['shop_group_name'];
                }
            }
            $statistic[] = $group_shop;
            $statistic[] = $datas[$i]['id_association'];
            $statistic[] = $datas[$i]['id_product'];
            $statistic[] = $datas[$i]['product_displayed_name'];
            $statistic[] = $datas[$i]['sum_displayed'];
            $statistic[] = $datas[$i]['sum_add_to_cart'];
            $statistic[] = $datas[$i]['sum_click_to_view'];
            $statistic[] = $datas[$i]['date_add'];
            $statistics[] = $statistic;
        }
        return $statistics;
    }

    /**
     * Get fields.
     */
    public function getFields()
    {
        $fields_description = array(
            'id_association' => array(
                'title' => $this->translator->l('Association Id.', 'StatisticsAdminListingPresenter'),
                'type' => 'int',
                'align' => 'center',
                'class' => 'lineven-helper-list-width-80',
                'orderby' => true,
                'search' => true,
                'value' => '',
                'default_filter' => '*'
            ),
            'product_displayed_name' => array(
                'title' => $this->translator->l('Product', 'StatisticsAdminListingPresenter'),
                'type' => 'text',
                'render' => 'specific',
                'callback' => 'getHtmlProductDisplayed',
                'callback_object' => 'LinevenApoAssociationAdminListingPresenter',
                'filter_clause' => 'having',
                'orderby' => true,
                'search' => true,
                'value' => '',
                'default_filter' => '*'
            ),
            'sum_displayed' => array(
                'title' => $this->translator->l('Displayed', 'StatisticsAdminListingPresenter'),
                'type' => 'int',
                'align' => 'center',
                'class' => 'lineven-helper-list-width-80',
                'orderby' => true,
                'search' => false,
                'value' => ''
            ),
            'sum_add_to_cart' => array(
                'title' => $this->translator->l('In cart', 'StatisticsAdminListingPresenter'),
                'type' => 'int',
                'align' => 'center',
                'class' => 'lineven-helper-list-width-80',
                'orderby' => true,
                'search' => false,
                'value' => ''
            ),
            'sum_click_to_view' => array(
                'title' => $this->translator->l('Viewed', 'StatisticsAdminListingPresenter'),
                'type' => 'int',
                'align' => 'center',
                'class' => 'lineven-helper-list-width-80',
                'orderby' => true,
                'search' => false,
                'value' => ''
            ),
            'date_add' => array(
                'title' => $this->translator->l('Date', 'StatisticsAdminListingPresenter'),
                'type' => 'datetime',
                'align' => 'center',
                'orderby' => true,
                'search' => true,
                'table_alias' => 'apo',
                'value' => '',
                'default_filter' => array((new DateTime())->format('Y-m-d'), (new DateTime())->format('Y-m-d'))
            )
        );
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') !== false
            && Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1
            && Shop::getTotalShops() > 1) {
            $fields_description['group_shop'] = array(
                'title' => $this->translator->l('Group / Shop', 'StatisticsAdminListingPresenter'),
                'type' => 'text',
                'orderby' => false
            );
        }
        return $fields_description;
    }
}
