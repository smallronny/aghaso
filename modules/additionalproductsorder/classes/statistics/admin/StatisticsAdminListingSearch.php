<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoStatisticsAdminListingSearch extends LinevenApoModuleAdminListingSearch
{
    /**
     * Run query.
     *
     * @param array $settings Settings
     * @param boolean $for_export For export
     * @return array
     */
    public function runQuery(
        $settings,
        $for_export = false
    ) {
        $this->results = $this->getStatistics($settings, false, $for_export);
        $this->count = $this->getStatistics($settings, true, $for_export);
        return $this->results;
    }

    /**
     * Get statistics.
     *
     * @param array $settings Settings
     * @param boolean $for_count For count
     * @param boolean $for_export For export
     *
     * @return arrays
     */
    public function getStatistics(
        $settings,
        $for_count = false,
        $for_export = false
    ) {
        $module_context = LinevenApoContext::getContext();
        $where_shop = '';
        if (Context::getContext()->shop->getContextShopID() != null) {
            $where_shop = ' apo.`id_shop` = '.(int)Context::getContext()->shop->getContextShopID().' ';
        } else {
            if (Context::getContext()->shop->getContextShopGroupID() != null) {
                $where_shop = ' apo.`id_shop_group` = '.(int)Context::getContext()->shop->getContextShopGroupID().' ';
            }
        }
        // Group by association or product
        $ignore_id_association = false;
        $ignore_product = false;
        if (isset($settings['filters']['filters']['id_association'])
            && $settings['filters']['filters']['id_association'] == '*') {
            $ignore_id_association = true;
            $key = array_search('`id_association` = \'*\'', $settings['filters']['where']);
            if ($key !== false) {
                unset($settings['filters']['where'][$key]);
            }
            $settings['order_by'] = 'apo.`date_add`';
        }
        if (isset($settings['filters']['filters']['product_displayed_name'])
            && $settings['filters']['filters']['product_displayed_name'] == '*') {
            $ignore_product = true;
            $key = array_search('`product_displayed_name` LIKE \'%*%\'', $settings['filters']['having']);
            if ($key !== false) {
                unset($settings['filters']['having'][$key]);
            }
            $settings['order_by'] = 'apo.`date_add`';
        }

        // limit
        $add_limit = true;
        if ($for_count || $for_export) {
            $add_limit = false;
        }
        // Group by
        $group_by = null;
        $statistics_fields = '
            apo.`displayed` as sum_displayed,
            apo.`add_to_cart` as sum_add_to_cart,
            apo.`click_to_view` as sum_click_to_view
        ';
        if ($ignore_product || $ignore_id_association) {
            $group_by = '
                apo.`id_shop_group`,
                apo.`id_shop` '.
                (!$ignore_id_association ? ' ,apo.`id_association` ' : ' ').
                (!($ignore_product && $ignore_id_association) ? ' ,apo.`id_product` ' : ' ').
                (!($ignore_id_association || $ignore_product) ? ' , apo.`date_add` ' : ' ');
            $statistics_fields = '
                SUM(apo.`displayed`) as sum_displayed,
                SUM(apo.`add_to_cart`) as sum_add_to_cart,
                SUM(apo.`click_to_view`) as sum_click_to_view
            ';
        }
        $sql = 'SELECT
                  apo.`id_shop_group`,
                  apo.`id_shop`,'.
            (!$ignore_id_association ? ' apo.`id_association`, ' : ' ').
            (!($ignore_product && $ignore_id_association) ? ' apo.`id_product`, ' : ' ').
            $statistics_fields.
            (!($ignore_id_association || $ignore_product) ? ' , apo.`date_add` ' : ' '). '
                  ,g.`name` `shop_group_name`, s.`name` `shop_name`,
            pdl.`name` product_displayed_name, pd.`reference` product_displayed_reference
            FROM `'._DB_PREFIX_.'lineven_apo_statistics` apo
            LEFT JOIN `'._DB_PREFIX_.'shop_group` g on (apo.`id_shop_group` = g.`id_shop_group`)
            LEFT JOIN `'._DB_PREFIX_.'shop` s on (apo.`id_shop` = s.`id_shop`)
            LEFT JOIN `'._DB_PREFIX_.'product` pd ON (apo.`id_product` = pd.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pdl ON (apo.`id_product` = pdl.`id_product`
                AND pdl.`id_lang` = '.(int)$module_context->current_id_lang.Shop::addSqlRestrictionOnLang('pdl').')'.
            LinevenApoHelperList::addSqlFiltering($settings, $add_limit, $where_shop, null, $group_by);

        $statistics_list = Db::getInstance()->ExecuteS($sql);
        $count_statistics_list = count($statistics_list);
        if ($for_count) {
            return $count_statistics_list;
        }
        $statistics = array();
        for ($i = 0; $i < $count_statistics_list; $i++) {
            $statistic = $statistics_list[$i];
            if ($for_export) {  // For export
                if ($ignore_id_association) {
                    $statistic['id_association'] = '*';
                }
                if ($ignore_product && $ignore_id_association) {
                    $statistic['id_product'] = '*';
                    $statistic['product_displayed_name'] = '';
                }
                if ($ignore_id_association || $ignore_product) {
                    $statistic['date_add'] = '';
                }
            } else {    // For display
                if ($ignore_id_association) {
                    $statistic['id_association'] = '*';
                }
                if ($ignore_id_association || $ignore_product) {
                    $statistic['date_add'] = null;
                }
            }
            $statistics[] = $statistic;
        }
        return $statistics;
    }
}
