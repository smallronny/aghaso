<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoStatistics
{

    /**
     * Delete statistics by association
     * @param int $id_association Id association
     * @return boolean
     */
    public static function deleteByIdAssociation($id_association)
    {
        if ($id_association != null && Configuration::get('LINEVEN_APO_STATS_ACTIVE')) {
            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lineven_apo_statistics` where id_association='.(int)$id_association);
        }
    }
    
    /**
     * Clear all statistics
     *
     * @param date $date Date
     * @return boolean
     */
    public static function clear($date = null)
    {
        $where_shop = '';
        if (Context::getContext()->shop->getContextShopID() != null) {
            $where_shop = ' `id_shop` = '.(int)Context::getContext()->shop->getContextShopID().' ';
        } else {
            if (Context::getContext()->shop->getContextShopGroupID() != null) {
                $where_shop = ' `id_shop_group` = '.(int)Context::getContext()->shop->getContextShopGroupID().' ';
            }
        }
        $sql = 'DELETE FROM `'._DB_PREFIX_.'lineven_apo_statistics` '.($where_shop != '' ? ' WHERE '.$where_shop : '');
        if ($date) {
            $sql .= ' AND `date_add` < \''.pSQL($date).'\'';
        }
        Db::getInstance()->Execute($sql);
        if ($date == null) {
            $date = new DateTime();
            $date = $date->format('Y-m-d');
        }
        Configuration::updateValue('LINEVEN_APO_STATS_LAST_CLEAR', $date);
    }
    
    /**
     * Set datas
     * @param int $id_association Id association
     * @param int $id_product Id product
     * @param string $type Type (D (Displayed), C (Cart), V (viewed))
     * @return boolean
     */
    public static function setData($id_association, $id_product, $type)
    {
        if ($id_association != null) {
            $is_displayed = 0;
            $is_add_to_cart = 0;
            $is_click_to_view = 0;
            $trace = true;
            switch ($type) {
                case 'D':
                    $is_displayed = 1;
                    break;
                case 'C':
                    $is_add_to_cart = 1;
                    break;
                case 'V':
                    $is_click_to_view = 1;
                    break;
                default:
                    $trace = false;
                    break;
            }
            if ($trace) {
                $id_shop_group = 'null';
                if (Context::getContext()->shop->getContextShopGroupID() != null) {
                    $id_shop_group = Context::getContext()->shop->getContextShopGroupID();
                }
                $id_shop = 'null';
                if (Context::getContext()->shop->getContextShopID() != null) {
                    $id_shop = Context::getContext()->shop->getContextShopID();
                }
                Db::getInstance()->Execute(
                    'INSERT INTO `'._DB_PREFIX_.'lineven_apo_statistics`
                    (`id_association`, `id_shop_group`, `id_shop`, `id_product`, `displayed`, `add_to_cart`, `click_to_view`, `date_add`)
                    values('.(int)$id_association.', '.$id_shop_group.', '.$id_shop.', '.(int)$id_product.', '.
                    (int)$is_displayed.', '.(int)$is_add_to_cart.', '.(int)$is_click_to_view.
                    ', "'.date('Y-m-d H:i:s').'")'
                );
            }
        }
    }
}
