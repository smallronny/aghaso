<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAssociationAdminProductsSearch extends LinevenApoModuleAdminListingSearch
{
    /**
     * Run query.
     *
     * @param string $filter Filter
     * @param string $by By (query | id)
     * @return array
     */
    public function runQuery(
        $filter,
        $by
    ) {
        if ($by == 'query') {
            $this->results = $this->getProductsByQuery($filter);
        } else {
            $this->results = $this->getProductsById($filter);
        }
        return $this->results;
    }

    /**
     * Get products.
     *
     * @param string $filter Filter
     *
     * @return arrays
     */
    private function getProductsByQuery($filter = '')
    {
        if (!$filter || $filter == '' || Tools::strlen($filter) < 1) {
            die();
        }
        if ($pos = strpos($filter, ' (ref:')) {
            $filter = Tools::substr($filter, 0, $pos);
        }
        $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, p.`cache_default_attribute`
            FROM `'._DB_PREFIX_.'product` p
            '.Shop::addSqlAssociation('product', 'p').'
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON
                (pl.id_product = p.id_product AND pl.id_lang = '.
            (int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
            WHERE (pl.name LIKE \'%'.pSQL($filter).'%\' OR p.reference LIKE \'%'.pSQL($filter).'%\')
            GROUP BY p.id_product';
        return Db::getInstance()->executeS($sql);
    }

    /**
     * Get products by id.
     *
     * @param int $id Id
     *
     * @return arrays
     */
    private function getProductsById($id = 0)
    {
        $sql_ids = $id;
        $order_by = '';
        if (is_array($id)) {
            if (!count($id)) {
                return array();
            }
            $sql_ids = implode(',', $id);
            $order_by = ' ORDER BY FIND_IN_SET(p.`id_product`, \''.$sql_ids.'\')';
        }
        $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, pl.`description_short`
            FROM `'._DB_PREFIX_.'product` p
            '.Shop::addSqlAssociation('product', 'p').'
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON
                (pl.id_product = p.id_product AND pl.id_lang = '.
            (int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
            WHERE  p.id_product in ('.pSQL($sql_ids).')'.$order_by;
        $result = Db::getInstance()->executeS($sql);
        if (is_array($id)) {
            return $result;
        } else {
            if (count($result)) {
                return $result[0];
            }
        }
        return null;
    }
}
