<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAssociationAdminListingSearch extends LinevenApoModuleAdminListingSearch
{
    private $associations = array();
    private $iteration;

    /**
     * Run query.
     *
     * @param array $settings Settings
     * @return array
     */
    public function runQuery(
        $settings
    ) {
        $this->results = $this->getAssociations($settings, false);
        $this->count = $this->getAssociations($settings, true);
        return $this->results;
    }

    /**
     * Get associations.
     *
     * @param array $settings Settings
     * @param boolean $for_count For count
     *
     * @return array
     */
    public function getAssociations(
        $settings,
        $for_count = false
    ) {
        $module_context = LinevenApoContext::getContext();
        $where_shop = '';
        if (Context::getContext()->shop->getContextShopID() != null) {
            $where_shop = ' apo.`id_shop` = ' . (int)Context::getContext()->shop->getContextShopID() . ' ';
        } else {
            if (Context::getContext()->shop->getContextShopGroupID() != null) {
                $where_shop = ' apo.`id_shop_group` = ' . (int)Context::getContext()->shop->getContextShopGroupID() . ' ';
            }
        }
        if (!$for_count) {
            $settings['additional_order_by'] = array('apo_products.`order_display` ASC');
        }
        $sql = 'SELECT apo.`id`, apo.`id_shop_group`, apo.`id_shop`,
            apo.`id_related_category`, rcl.`name` related_category_name,
            apo.`id_related_product`, rpl.`name` related_product_name, rp.`reference` related_product_reference,
            (CASE 
                WHEN apo.`id_related_category` IS NOT NULL && apo.`id_related_product` IS NULL THEN rcl.`name`
                WHEN apo.`id_related_category` IS NULL && apo.`id_related_product` IS NOT NULL THEN rpl.`name`
                ELSE ""
            END) AS related_object_name,
            apo.`id_displayed_category`, dcl.`name` displayed_category_name, ' .
            (!$for_count ? 'apo_products.`id_displayed_product`, dpl.`name` displayed_product_name, dp.`reference` displayed_product_reference,' : '') . '
            (CASE 
                WHEN apo.`id_displayed_category` IS NOT NULL THEN dcl.`name`
                ELSE ' . (!$for_count ? 'dpl.`name`' : '"PRODUCTS_LIST"') . '
            END) AS object_to_display,
            g.`name` `shop_group_name`, s.`name` `shop_name`,
            apo.`hooks`, apo.`name`, apo.`short_description`, apo.`comments`,
            apo.`is_active_groups`, apo.`minimum_amount`, apo.`maximum_amount`,
            apo.`order_display`
            FROM `' . _DB_PREFIX_ . 'lineven_apo` apo
            LEFT JOIN `' . _DB_PREFIX_ . 'shop_group` g ON (apo.`id_shop_group` = g.`id_shop_group`)
            LEFT JOIN `' . _DB_PREFIX_ . 'shop` s on (apo.`id_shop` = s.`id_shop`)
            LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` rcl
                ON (apo.`id_related_category` = rcl.`id_category`' . Shop::addSqlRestrictionOnLang('rcl') . '
                    AND rcl.`id_lang` = ' . (int)$module_context->current_id_lang .
            Shop::addSqlAssociation('category_lang', 'rcl') . ')                
            LEFT JOIN `' . _DB_PREFIX_ . 'product` rp ON (apo.`id_related_product` = rp.`id_product`)
               LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` rpl ON (apo.`id_related_product` = rpl.`id_product`
                AND rpl.`id_lang` = ' . (int)$module_context->current_id_lang . Shop::addSqlRestrictionOnLang('rpl') . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` dcl ON (apo.`id_displayed_category` = dcl.`id_category`' . Shop::addSqlRestrictionOnLang('dcl') . '
                AND dcl.`id_lang` = ' . (int)$module_context->current_id_lang . Shop::addSqlAssociation('category_lang', 'dcl') . ') ' .
            (!$for_count
                ? '
                    LEFT JOIN `' . _DB_PREFIX_ . 'lineven_apo_displayed_products` apo_products ON (apo.`id` = apo_products.`id_association`)
                    LEFT JOIN `' . _DB_PREFIX_ . 'product` dp ON (apo_products.`id_displayed_product` = dp.`id_product`)
                    LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` dpl ON (dp.`id_product` = dpl.`id_product`
                        AND dpl.`id_lang` = ' . (int)$module_context->current_id_lang . Shop::addSqlRestrictionOnLang('dpl') . ')'
                : '') .
            LinevenApoHelperList::addSqlFiltering($settings, !$for_count, $where_shop);

        $apo_list = Db::getInstance()->ExecuteS($sql);
        if ($for_count) {
            return count($apo_list);
        }

        $this->associations = array();
        $this->iteration = -1;
        foreach ($apo_list as $row) {
            $this->populateRow($row);
        }
        return $this->associations;
    }

    /**
     * Populate association row.
     * @param array $row Association row
     */
    private function populateRow($row)
    {
        if ((int)$row['id_displayed_category'] != 0 ||
            count($this->associations) == 0 ||
            ((int)$row['id_displayed_category'] == 0 && (!isset($this->associations[$this->iteration]) ||
                    (isset($this->associations[$this->iteration]) && $this->associations[$this->iteration]['id'] != $row['id'])))) {
            $this->iteration++;
            $this->associations[$this->iteration] = $this->unsetFields($row);
        }
        if ((int)$row['id_displayed_category'] == 0) {
            $this->populateProducts($row);
        }
    }

    /**
     * Populate product
     * @param array $row Association row
     */
    private function populateProducts($row)
    {
        $product_details = array(
            'id_displayed_product' => $row['id_displayed_product'],
            'displayed_product_name' => $row['displayed_product_name'],
            'displayed_product_reference' => $row['displayed_product_reference']
        );
        if (!isset($this->associations[$this->iteration]['products_list'])) {
            $this->associations[$this->iteration]['products_list'] = array();
        }
        $this->associations[$this->iteration]['products_list'][] = $product_details;
    }

    /**
     * Unset some fields.
     * @param array $row Association row
     * @return array
     */
    private function unsetFields($row)
    {
        unset($row['id_displayed_product']);
        unset($row['displayed_product_name']);
        unset($row['displayed_product_reference']);
        return $row;
    }
}
