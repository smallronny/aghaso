<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAssociationAdminListingPresenter extends LinevenApoModuleAdminListingPresenter
{

    /**
     * Present.
     * @param array $datas Datas
     * @return array
     */
    public function presentList($datas)
    {
        $associations = array();
        for ($i = 0; $i < count($datas); $i++) {
            $association = $datas[$i];
            $association['identifier'] = $datas[$i]['id'];
            $association['name'] = ' : '.$this->translator->l('Association No', 'AssociationAdminListingPresenter').
                ' '.$datas[$i]['id'];

            $group_shop = $this->translator->l('All shops', 'AssociationAdminListingPresenter');
            if ($datas[$i]['id_shop'] != null) {
                $group_shop = $datas[$i]['shop_name'];
            } else {
                if ($datas[$i]['id_shop_group'] != null) {
                    $group_shop = $datas[$i]['shop_group_name'];
                }
            }
            $association['group_shop'] = $group_shop;
            $association['class'] = 'lapo-associations-list-item';
            // Association type
            $type = LinevenApoAssociation::$association_type_all;
            if ((int)$datas[$i]['id_related_category'] != 0) {
                $type = LinevenApoAssociation::$association_type_category;
            }
            if ((int)$datas[$i]['id_related_product'] != 0) {
                $type = LinevenApoAssociation::$association_type_product;
            }
            $association['association_type'] = $type;
            // Display choice
            $display_choice = LinevenApoAssociation::$related_display_products;
            if ((int)$association['id_displayed_category'] != 0) {
                $display_choice = LinevenApoAssociation::$related_display_category;
            }
            $association['display_choice'] = $display_choice;
            $association['product_new_name'] = LinevenApoTranslator::getTranslationFromArray(unserialize($datas[$i]['name']));
            $association['product_new_description'] = LinevenApoTranslator::getTranslationFromArray(unserialize($datas[$i]['short_description']));
            $association['options'] = '';
            $associations[] = $association;
        }
        return $associations;
    }

    /**
     * Get html for related object name to display in associations list.
     * @param string $related_object_name Related name
     * @param array $row Row
     * @return string
     */
    public static function getHtmlRelatedObjectName($related_object_name, $row)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $translator = new LinevenApoTranslator();
        if ($row['association_type'] == LinevenApoAssociation::$association_type_product) {
            if ((int)$row['id_related_product'] != 0) {
                $product_cover = Product::getCover($row['id_related_product']);
                return self::getHtmlProductsInformations(
                    $row['id_related_product'],
                    $related_object_name,
                    $row['related_product_reference'],
                    $product_cover
                );
            }
        }
        // If other text display via smarty
        $simple_text = null;
        if ($row['association_type'] == LinevenApoAssociation::$association_type_all) {
            $simple_text = array(
                'span_class' => 'lapo-associations-list-text',
                'text' => $translator->l('(systematically displayed)', 'AssociationAdminListingPresenter')
            );
        }
        if ($row['association_type'] == LinevenApoAssociation::$association_type_category) {
            $simple_text = array(
                'icon' => array(
                    'span_class' => 'lapo-associations-list-icon imgm img-thumbnail',
                    'i_class' => 'fa fa-folder-open-o fa-2x'
                ),
                'text' => $related_object_name
            );
        }
        Context::getContext()->smarty->assign('helper_listing_text', $simple_text);
        return $configuration->getModule()->display(
            _PS_MODULE_DIR_.$configuration->getModule()->name,
            'views/templates/admin/module/_partials/helpers/list/simple_text.tpl'
        );
    }

    /**
     * Get html for object to display in associations list.
     * @param string $object_to_display Object to display
     * @param array $row Row
     * @return string
     */
    public static function getHtmlObjectToDisplay($object_to_display, $row)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($row['display_choice'] == LinevenApoAssociation::$related_display_products) {
            if (isset($row['products_list']) && count($row['products_list'])) {
                $products_list = array();
                foreach ($row['products_list'] as $product_details) {
                    $product_cover = Product::getCover($product_details['id_displayed_product']);
                    $products_list[] = self::getHtmlProductsInformations(
                        $product_details['id_displayed_product'],
                        $product_details['displayed_product_name'],
                        $product_details['displayed_product_reference'],
                        $product_cover
                    );
                }
                if (count($products_list)) {
                    Context::getContext()->smarty->assign(array(
                        'products_list' => $products_list
                    ));
                    return $configuration->getModule()->display(
                        _PS_MODULE_DIR_.$configuration->getModule()->name,
                        'views/templates/admin/module/helpers/products_list.tpl'
                    );
                }
            }
            return '';
        }
        if ($row['display_choice'] == LinevenApoAssociation::$related_display_category) {
            $simple_text = array(
                'icon' => array(
                    'span_class' => 'lapo-associations-list-icon imgm img-thumbnail',
                    'i_class' => 'fa fa-folder-open-o fa-2x'
                ),
                'text' => $object_to_display
            );
        }
        Context::getContext()->smarty->assign('helper_listing_text', $simple_text);
        return $configuration->getModule()->display(
            _PS_MODULE_DIR_.$configuration->getModule()->name,
            'views/templates/admin/module/_partials/helpers/list/simple_text.tpl'
        );
    }

    /**
     * Get html for product to display in associations list.
     * @param string $product_name Product name
     * @param array $row Row
     * @return string
     */
    public static function getHtmlProductDisplayed($product_name, $row)
    {
        $id_product = 0;
        if (isset($row['product_id'])) {
            $id_product = $row['product_id'];
        }
        if (isset($row['id_product'])) {
            $id_product = $row['id_product'];
        }
        if ($id_product != null && (int)$id_product) {
            $product_cover = Product::getCover($id_product);
            return self::getHtmlProductsInformations(
                $id_product,
                $product_name,
                $row['product_displayed_reference'],
                $product_cover
            );
        }
        return '';
    }

    /**
     * Get html for product to display in associations list.
     *
     * @param int $product_id
     * @param string $product_name Product name
     * @param string $product_reference Product reference
     * @param object $product_cover Product cover
     * @return string
     */
    public static function getHtmlProductsInformations($product_id, $product_name, $product_reference, $product_cover)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $image_html = '';
        if ($product_cover != null) {
            $imageManager = new PrestaShop\PrestaShop\Adapter\ImageManager(new PrestaShop\PrestaShop\Adapter\LegacyContext());
            $image_html = $imageManager->getThumbnailForListing((int)$product_cover['id_image']);
        }
        Context::getContext()->smarty->assign(array(
            'product_thumbnail' => $image_html,
            'product_name' => $product_name,
            'product_reference' => $product_reference,
        ));
        return $configuration->getModule()->display(
            _PS_MODULE_DIR_.$configuration->getModule()->name,
            'views/templates/admin/module/helpers/product_informations.tpl'
        );
    }

    /**
     * Get options icons for list.
     * @param string $value Value
     * @param array $row Row
     * @return string
     */
    public static function getHtmlTargetIcons($value, $row)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $smarty_options = array();
        if (isset($row['hooks'])) {
            $smarty_options[] = 'fa-shopping-cart fa-lg'.(($row['hooks'] != null && Tools::strpos($row['hooks'], AdditionalProductsOrder::$hook_code_order) === false) ? ' lapo-fa-disabled' : '');
            $smarty_options[] = 'fa-book fa-lg'.(($row['hooks'] != null && Tools::strpos($row['hooks'], AdditionalProductsOrder::$hook_code_extra_product) === false) ? ' lapo-fa-disabled' : '');
        }
        Context::getContext()->smarty->assign('helper_listing_options', $smarty_options);
        return $configuration->getModule()->display(
            _PS_MODULE_DIR_.$configuration->getModule()->name,
            'views/templates/admin/module/_partials/helpers/list/options_icons.tpl'
        );
    }

    /**
     * Get html options in associations list.
     * @param string $options Object to display
     * @param array $row Row
     * @return string
     */
    public static function getHtmlOptions($options, $row)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $tranlator = new LinevenApoTranslator();
        $module_context = LinevenApoContext::getContext();
        $smarty_options = array();
        if ((int)$row['minimum_amount'] != 0 || (int)$row['maximum_amount'] != 0) {
            $smarty_options[] = array(
                'icon' => 'icon-dollar fa-lg',
                'id_tooltip_content' => 'tooltip_amount_'.$row['id'],
                'tooltip' => (
                    ((int)$row['minimum_amount'] != 0
                        ? LinevenApoTools::displayHtmlInCode('simple_tag', array(
                            'tag' => 'strong',
                            'text' => $tranlator->l('Minimum:', 'AssociationAdminListingPresenter')
                        )).' '.$row['minimum_amount']
                        : '').'<br/>'.
                    ((int)$row['maximum_amount'] != 0
                        ? LinevenApoTools::displayHtmlInCode('simple_tag', array(
                            'tag' => 'strong',
                            'text' => $tranlator->l('Maximum:', 'AssociationAdminListingPresenter')
                        )).' '.$row['maximum_amount']
                        : '')
                )
            );
        }
        if ($row['is_active_groups']) {
            $smarty_options[] = 'fa-users fa-lg';
        }
        if (trim($row['product_new_name']) != '' || trim($row['product_new_description']) != '') {
            $smarty_options[] = array(
                'icon' => 'icon-info fa-lg',
                'id_tooltip_content' => 'tooltip_infos_'.$row['id'],
                'tooltip' => (
                    (trim($row['product_new_name']) != ''
                        ? LinevenApoTools::displayHtmlInCode('simple_tag', array(
                            'tag' => 'strong',
                            'text' => $tranlator->l('Name for display:', 'AssociationAdminListingPresenter')
                        )).' '.trim($row['product_new_name'])
                        : '').'<br/>'.
                    (trim($row['product_new_description']) != ''
                        ? LinevenApoTools::displayHtmlInCode('simple_tag', array(
                            'tag' => 'strong',
                            'text' => $tranlator->l('Description for display:', 'AssociationAdminListingPresenter')
                        )).' '.trim($row['product_new_description'])
                        : '')
                )
            );
        }
        if ($row['comments'] && trim($row['comments']) != '') {
            $smarty_options[] = array(
                'icon' => 'fa-comment-o fa-lg',
                'id_tooltip_content' => 'tooltip_comment_'.$row['id'],
                'tooltip' => LinevenApoTools::displayHtmlInCode('simple_tag', array(
                    'tag' => 'strong',
                    'text' => $tranlator->l('Your comment about this association:', 'AssociationAdminListingPresenter')
                )).'<br/>'.$row['comments']
            );
        }
        Context::getContext()->smarty->assign('helper_listing_options', $smarty_options);
        return $configuration->getModule()->display(
            _PS_MODULE_DIR_.$configuration->getModule()->name,
            'views/templates/admin/module/_partials/helpers/list/options_icons.tpl'
        );
    }
    
    /**
     * Get fields.
     */
    public function getFields()
    {
        $fields_description = array(
            'id' => array(
                'title' => $this->translator->l('Id.', 'AssociationAdminListingPresenter'),
                'type' => 'int',
                'align' => 'center',
                'class' => 'lineven-helper-list-identifier',
                'orderby' => true,
                'search' => true,
                'value' => ''
            ),
            'related_object_name' => array(
                'title' => $this->translator->l('Selection rule', 'AssociationAdminListingPresenter'),
                'type' => 'text',
                'render' => 'specific',
                'callback' => 'getHtmlRelatedObjectName',
                'callback_object' => 'LinevenApoAssociationAdminListingPresenter',
                'filter_clause' => 'having',
                'orderby' => true,
                'search' => true,
                'value' => ''
            ),
            'object_to_display' => array(
                'title' => $this->translator->l('Display', 'AssociationAdminListingPresenter'),
                'type' => 'text',
                'render' => 'specific',
                'callback' => 'getHtmlObjectToDisplay',
                'callback_object' => 'LinevenApoAssociationAdminListingPresenter',
                'filter_clause' => 'having',
                'orderby' => false,
                'search' => true,
                'value' => ''
            ),
            'hooks' => array(
                'title' => $this->translator->l('Target', 'AssociationAdminListingPresenter'),
                'type' => 'select',
                'list' => array(
                    AdditionalProductsOrder::$hook_code_order => $this->translator->l('Order page', 'ReviewAdminModerationListingPresenter'),
                    AdditionalProductsOrder::$hook_code_extra_product => $this->translator->l('Extra product', 'ReviewAdminModerationListingPresenter')
                ),
                'filter_key' => 'hooks',
                'align' => 'center',
                'class' => 'lapo-helper-hooks-options',
                'render' => 'specific',
                'callback' => 'getHtmlTargetIcons',
                'callback_object' => 'LinevenApoAssociationAdminListingPresenter',
                'orderby' => false,
                'search' => true,
                'search_operator' => 'like',
                'search_where_or' => '`hooks` is null or `hooks` = \'\'',
                'value' => ''
            ),
            'options' => array(
                'title' => $this->translator->l('Options', 'AssociationAdminListingPresenter'),
                'type' => 'text',
                'render' => 'specific',
                'class' => 'lapo-helper-hooks-options',
                'callback' => 'getHtmlOptions',
                'callback_object' => 'LinevenApoAssociationAdminListingPresenter',
                'orderby' => false,
                'search' => false,
                'value' => ''
            )
        );
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') !== false
            && Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1
            && Shop::getTotalShops() > 1) {
            $fields_description['group_shop'] = array(
                'title' => $this->translator->l('Group / Shop', 'AssociationAdminListingPresenter'),
                'type' => 'text',
                'orderby' => false
            );
        }
        $fields_description['order_display'] = array(
            'title' => $this->translator->l('Position', 'AssociationAdminListingPresenter'),
            'class' => 'lineven-helper-list-position',
            'suffix' => '<i class="fa fa-sort"></i>',
            'type' => 'int',
            'align' => 'center',
            'orderby' => true,
            'search' => true,
            'value' => ''
        );
        return $fields_description;
    }
}
