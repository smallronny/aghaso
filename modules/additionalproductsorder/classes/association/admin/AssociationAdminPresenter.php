<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAssociationAdminPresenter extends LinevenApoModuleAdminFormPresenter
{
    private $association;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->association = null;
    }

   /**
     * Set association.
     * @param object $association Association
     */
    public function setAssociation($association)
    {
        $this->association = $association;
    }

    /**
     * Present.
     */
    public function present()
    {
        $form_presenter = parent::present();
        $form_presenter['link'] = Context::getContext()->link;
        $form_presenter['association'] = $this->association;
        return $form_presenter;
    }
}
