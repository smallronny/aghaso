<?php
/**
 * AdditionalProductsOrder Merchandizing (Version 3.0.4)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoAssociation extends LinevenApoModel
{
    /**
     * Id
     * @var int
     */
    public $id;

    /**
     * Id shop group
     * @var int
     */
    public $id_shop_group;

    /**
     * Id shop
     * @var int
     */
    public $id_shop;

    /**
     * Association type
     * @var string
     */
    public $association_type;

    /**
     * Related category id
     * @var integer
     */
    public $id_related_category;

    /**
     * Related product id
     * @var integer
     */
    public $id_related_product;

    /**
     * Displayed category id
     * @var integer
     */
    public $id_displayed_category;

    /**
     * Minimum order
     * @var float
     */
    public $minimum_amount;
    
    /**
     * Maximum order
     * @var float
     */
    public $maximum_amount;
    
    /**
     * Is active groups
     * @var integer
     */
    public $is_active_groups;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Short description
     * @var string
     */
    public $short_description;

    /**
     * Comment
     * @var string
     */
    public $comments;

    /**
     * Groups
     * @var array
     */
    public $groups;

    /**
     * Hooks
     * @var string
     */
    public $hooks;

    /**
     * Order display
     * @var integer
     */
    public $order_display;

    /**
     * Display choice
     * @var string
     */
    public $display_choice;

    /**
     * Display products
     * @var string
     */
    public $displayed_products;

    public static $association_type_all         = 'ALL';         /* All type */
    public static $association_type_category    = 'CATEGORY';    /* Category type */
    public static $association_type_product     = 'PRODUCT';     /* Product type */

    public static $related_display_category         = 'CATEGORY';    /* Category */
    public static $related_display_products         = 'PRODUCTS';    /* Products */

    /**
     * Constructor.
     * @param int $id Identifier
     */
    public function __construct($id = null)
    {
        self::$definition = array(
            'table' => 'lineven_apo',
            'primary' => 'id',
            'fields' => array(
                'id_shop' => array(
                    'type' => self::TYPE_INT,
                    'validate' => 'isUnsignedId',
                    'required' => false
                ),
                'id_shop_group' => array(
                    'type' => self::TYPE_INT,
                    'validate' => 'isUnsignedId',
                    'required' => false
                ),
                'id_related_category' => array(
                    'type' => self::TYPE_INT,
                    'validate' => 'isUnsignedId',
                    'required' => false
                ),
                'id_related_product' => array(
                    'type' => self::TYPE_INT,
                    'validate' => 'isUnsignedId',
                    'required' => false
                ),
                'id_displayed_category' => array(
                    'type' => self::TYPE_INT,
                    'validate' => 'isUnsignedId',
                    'required' => true
                ),
                'minimum_amount' => array(
                    'type' => self::TYPE_FLOAT,
                    'validate' => 'isFloat',
                    'required' => false
                ),
                'maximum_amount' => array(
                    'type' => self::TYPE_FLOAT,
                    'validate' => 'isFloat',
                    'required' => false
                ),
                'is_active_groups' => array(
                    'type' => self::TYPE_BOOL,
                    'validate' => 'isInt',
                    'required' => true
                ),
                'name' => array(
                    'type' => self::TYPE_STRING,
                    'validate' => 'isString',
                    'required' => false
                ),
                'short_description' => array(
                    'type' => self::TYPE_STRING,
                    'validate' => 'isString',
                    'required' => false
                ),
                'comments' => array(
                    'type' => self::TYPE_STRING,
                    'validate' => 'isString',
                    'required' => false
                ),
                'hooks' => array(
                    'type' => self::TYPE_STRING,
                    'validate' => 'isString',
                    'required' => false
                ),
                'order_display' => array(
                    'type' => self::TYPE_INT,
                    'validate' => 'isInt',
                    'required' => false
                ),
            )
        );
        parent::__construct($id);
        if ($id) {
            // Default
            $this->association_type = self::$association_type_all;
            $this->display_choice = self::$related_display_products;

            if ((int)$this->id_related_category != 0) {
                $this->association_type = self::$association_type_category;
            }
            if ((int)$this->id_related_product != 0) {
                $this->association_type = self::$association_type_product;
            }
            if ((int)$this->id_displayed_category != 0) {
                $this->display_choice = self::$related_display_category;
            }
            $this->name = unserialize($this->name);
            $this->short_description = unserialize($this->short_description);
            $this->setDisplayedProducts();
            $this->setGroups();
        } else {
            $this->minimum_amount = 0;
            $this->maximum_amount = 0;
            $this->association_type = self::$association_type_all;
            $this->display_choice = self::$related_display_products;
            $this->hooks = null;
            $this->products = array();
        }
    }

    /**
     * Update association.
     * @param LinevenApoForm $form Form.
     * @return boolean
     */
    public static function staticUpdate($form)
    {
        $fields_values = $form->getFieldsValue();
        $id_association = null;
        if (isset($fields_values['id']) && $fields_values['id'] != 0 && $fields_values['id'] != '') {
            $id_association = (int)$fields_values['id'];
        }

        $id_related_category = 'null';
        $id_related_product = 'null';
        switch ($fields_values['association_type']) {
            case LinevenApoAssociation::$association_type_all:
                break;
            case LinevenApoAssociation::$association_type_category:
                $id_related_category = (int)$fields_values['id_related_category'];
                break;
            case LinevenApoAssociation::$association_type_product:
                $id_related_product = (int)$fields_values['id_related_product'];
                break;
        }

        $id_displayed_category = 'null';
        $displayed_products = array();
        switch ($fields_values['display_choice']) {
            case LinevenApoAssociation::$related_display_category:
                $id_displayed_category = (int)$fields_values['id_displayed_category'];
                break;
            case LinevenApoAssociation::$related_display_products:
                $displayed_products = Tools::getValue('displayed_products_list');
                break;
        }

        // Amount
        $minimum_amount = str_replace(',', '.', $fields_values['minimum_amount']);
        $maximum_amount = str_replace(',', '.', $fields_values['maximum_amount']);

        // Hooks
        $which_hooks = '';
        if (isset($fields_values['which_hooks']) && is_array($fields_values['which_hooks']) && count($fields_values['which_hooks'])) {
            $which_hooks = implode('|', $fields_values['which_hooks']);
        }

        // Add
        if ((int)$id_association == 0) {
            $id_shop_group = 'null';
            if (Context::getContext()->shop->getContextShopGroupID() != null) {
                $id_shop_group = Context::getContext()->shop->getContextShopGroupID();
            }
            $id_shop = 'null';
            if (Context::getContext()->shop->getContextShopID() != null) {
                $id_shop = Context::getContext()->shop->getContextShopID();
            }

            $sql = 'SELECT MAX(`order_display`) `max_order` FROM `'._DB_PREFIX_.'lineven_apo`';
            $apo_max_list = Db::getInstance()->ExecuteS($sql);
            $order = 1;
            if (count($apo_max_list) > 0 && $apo_max_list[0]['max_order'] != '') {
                $order = $apo_max_list[0]['max_order'] + 1;
            }
            Db::getInstance()->Execute(
                'INSERT INTO `'._DB_PREFIX_.'lineven_apo`
                (`id_shop_group`, `id_shop`,
                `name`, `short_description`, `comments`, 
                `id_related_category`, `id_related_product`, `id_displayed_category`,
                `minimum_amount`, `maximum_amount`,
                `is_active_groups`, `hooks`, `order_display`)
                values('.$id_shop_group.', '.$id_shop.',
                \''.addslashes(serialize($fields_values['name'])).'\',
                \''.addslashes(serialize($fields_values['short_description'])).'\',
                \''.addslashes($fields_values['comments']).'\',
                '.$id_related_category.',
                '.$id_related_product.',
                '.$id_displayed_category.',
                '.pSQL($minimum_amount).',
                '.pSQL($maximum_amount).',
                '.(int)$fields_values['is_active_groups'].',
                \''.pSQL($which_hooks).'\',
                '.(int)$order.')'
            );
            $id_association = Db::getInstance()->Insert_ID();
        } else {
            // Update
            Db::getInstance()->Execute(
                'UPDATE `'._DB_PREFIX_.'lineven_apo`
                SET
                `name` = \''.addslashes(serialize($fields_values['name'])).'\',
                `short_description` = \''.addslashes(serialize($fields_values['short_description'])).'\',
                `comments` = \''.addslashes($fields_values['comments']).'\',
                `id_related_category` = '.$id_related_category.',
                `id_related_product` = '.$id_related_product.',
                `id_displayed_category` = '.$id_displayed_category.',
                `minimum_amount` = '.pSQL($minimum_amount).',
                `maximum_amount` = '.pSQL($maximum_amount).',
                `is_active_groups` = '.(int)$fields_values['is_active_groups'].',
                `hooks` = \''.pSQL($which_hooks).'\'
                where id = '.(int)$id_association
            );
            // By default delete groups for this apo
            Db::getInstance()->Execute(
                'DELETE FROM `'._DB_PREFIX_.'lineven_apo_displayed_products`
                where id_association = '.(int)$id_association
            );
            // By default delete groups for this apo
            Db::getInstance()->Execute(
                'DELETE FROM `'._DB_PREFIX_.'lineven_apo_groups`
                where id_association = '.(int)$id_association
            );
        }

        // Add products list
        if ((int)$id_association != 0 && is_array($displayed_products) && count($displayed_products)) {
            for ($i = 0; $i < count($displayed_products); $i++) {
                Db::getInstance()->Execute(
                    'INSERT INTO `'._DB_PREFIX_.'lineven_apo_displayed_products`
                    (`id_association`, `id_displayed_product`, `order_display`)
                    values('.(int)$id_association.', '.(int)$displayed_products[$i].', '.($i+1).')'
                );
            }
        }

        // Add groups
        if ((int)$id_association != 0 && (int)$fields_values['is_active_groups'] == 1
            && count($fields_values['apo_groups'])) {
            foreach ($fields_values['apo_groups'] as $group) {
                Db::getInstance()->Execute(
                    'INSERT INTO `'._DB_PREFIX_.'lineven_apo_groups`
                    (`id_association`, `id_group`)
                    values('.(int)$id_association.', '.(int)$group.')'
                );
            }
        }
        return $id_association;
    }

    /**
     * Delete.
     * @param int|array $ids Ids to delete
     * @return boolean
     */
    public static function staticDelete($ids)
    {
        $in = '';
        if (is_array($ids)) {
            if (count($ids)) {
                $in = implode(',', $ids);
            }
        } else {
            $in = (int)$ids;
        }
        if ($in != '') {
            Db::getInstance()->Execute(
                'DELETE FROM `'._DB_PREFIX_.'lineven_apo` WHERE id in ('.pSQL($in).')'
            );
            Db::getInstance()->Execute(
                'DELETE FROM `'._DB_PREFIX_.'lineven_apo_displayed_products` WHERE id_association in ('.pSQL($in).')'
            );
            Db::getInstance()->Execute(
                'DELETE FROM `'._DB_PREFIX_.'lineven_apo_groups` WHERE id_association in ('.pSQL($in).')'
            );
            Db::getInstance()->Execute(
                'DELETE FROM `'._DB_PREFIX_.'lineven_apo_statistics` WHERE id_association in ('.pSQL($in).')'
            );
            self::reorder();
        }
    }

    /**
     * Change order fields.
     * @param array $positions Positions
     * @param string $position_first First position
     * @return boolean
     */
    public static function order($positions, $position_first)
    {
        $position = $position_first;
        $positions = explode(',', $positions);
        if (is_array($positions)) {
            foreach ($positions as $id) {
                // Update
                Db::getInstance()->Execute(
                    'UPDATE `'._DB_PREFIX_.'lineven_apo`
                    SET `order_display` = '.(int)$position.'
                    WHERE id = '.(int)$id
                );
                $position++;
            }
        }
        return true;
    }

    /**
     * Reorder fields.
     * return @void
     */
    public static function reorder()
    {
        $sql = 'SELECT `id`, `order_display` FROM `'._DB_PREFIX_.'lineven_apo` ORDER BY `order_display` ASC, `id` ASC';
        $apo_list = Db::getInstance()->ExecuteS($sql);
        $order = 1;
        $count_apo_list = count($apo_list);
        for ($i = 0; $i < $count_apo_list; $i++) {
            Db::getInstance()->Execute(
                'UPDATE `'._DB_PREFIX_.'lineven_apo`
                SET `order_display` = '.$order.'
                WHERE id = '.(int)$apo_list[$i]['id']
            );
            $order++;
        }
    }

    /**
     * Set dislayed products.
     */
    public function setDisplayedProducts()
    {
        $sql = 'SELECT *
            FROM `'._DB_PREFIX_.'lineven_apo_displayed_products`
            WHERE `id_association` = '.(int)$this->id.'
            ORDER BY order_display ASC';
        $displayed_products = Db::getInstance()->executeS($sql);
        if (is_array($displayed_products) && count($displayed_products)) {
            $list_id = array();
            foreach ($displayed_products as $row) {
                $list_id[] = (int)$row['id_displayed_product'];
            }
            $this->displayed_products = LinevenApoAssociation::getProductsListDetails($list_id);
        }
    }

    /**
     * Set groups.
     */
    public function setGroups()
    {
        $sql = 'SELECT *
            FROM `'._DB_PREFIX_.'lineven_apo_groups`
            WHERE `id_association` = '.(int)$this->id;

        $this->groups = Db::getInstance()->executeS($sql);
    }

    /**
     * Get all associations.
     * @return array
     */
    public static function getAssociations()
    {
        $where_shop = '';
        if (Context::getContext()->shop->getContextShopID() != null) {
            $where_shop = ' WHERE `id_shop` = '.(int)Context::getContext()->shop->getContextShopID().' ';
        } else {
            if (Context::getContext()->shop->getContextShopGroupID() != null) {
                $where_shop = ' WHERE `id_shop_group` = '.(int)Context::getContext()->shop->getContextShopGroupID().' ';
            }
        }

        $sql = 'SELECT * FROM `'._DB_PREFIX_.'lineven_apo` '.$where_shop.' ORDER BY `order_display` ASC, `id` ASC ';
        $apo_list = Db::getInstance()->ExecuteS($sql);

        return $apo_list;
    }

    /**
     * Get products list from ids.
     */
    public static function getProductsListDetails($products_list_ids = array())
    {
        $products_search = new LinevenApoAssociationAdminProductsSearch();
        if (is_array($products_list_ids) && count($products_list_ids)) {
            return $products_search->runQuery($products_list_ids, 'id');
        }
        return array();
    }
    /**
     * Populate from form.
     *
     * @param Array $array Form value
     * @return void
     */
    public function populateFromFormValues($form_values)
    {
        parent::populateFromFormValues($form_values);
        if (Tools::isSubmit('apo_groups')) {
            $groups = Tools::getValue('apo_groups');
            foreach ($groups as $group) {
                $this->groups[] = $group;
            }
        }
    }
}
