<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoTools
{

    /**
     * Remove dir.
     *
     * @param $path Path
     * @return void
     */
    public static function rrmdir($path)
    {
        if (is_dir($path)) {
            foreach (glob($path.'/*') as $file) {
                if (is_dir($file)) {
                    LinevenApoTools::rrmdir($file);
                } else {
                    unlink($file);
                }
            }
            rmdir($path);
        }
    }
    
    /**
     * Delete file.
     *
     * @param $path Path
     * @return void
     */
    public static function deleteFile($path)
    {
        if (file_exists($path)) {
            return unlink($path);
        }
        return false;
    }

    /**
     * Rename file.
     *
     * @param $old_file Old file
     * @param $new_file New file
     * @return void
     */
    public static function renameFile($old_file, $new_file)
    {
        if (file_exists($old_file)) {
            rename($old_file, $new_file);
        }
    }

    /**
     * Copy dir.
     *
     * @param string $src Folder source
     * @param string $dst Folder destination
     * @return void
     */
    public static function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src.'/'.$file)) {
                    LinevenApoTools::recurseCopy($src.'/'.$file, $dst.'/'.$file);
                } else {
                    copy($src.'/'.$file, $dst.'/'.$file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Array shuffle.
     *
     * @param array $list Array
     * @return Array
     */
    public static function arrayShufflePreserve($list)
    {
        if (! is_array($list)) {
            return $list;
        }
        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }
    
    /**
     * Sort multidimentional array.
     *
     * @param array $arr Array
     * @param string $col Column
     * @param string $dir Direction
     * @return void
     */
    public static function arraySortByColumn(&$arr, $col, $dir = SORT_ASC)
    {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            if (isset($row[$col])) {
                $sort_col[$key] = $row[$col];
            } else {
                $sort_col[$key] = '';
            }
        }
        array_multisort($sort_col, $dir, $arr);
    }
    
    /**
     * Array replace recursive.
     *
     * @param array $array1 Array
     * @param array $array2 Array
     * @return array
     */
    public static function arrayReplaceRecursive($array1, $array2)
    {
        if (!function_exists('array_replace_recursive')) {
            // handle the arguments, merge one by one
            $args = func_get_args();
            $array = $args[0];
            if (!is_array($array)) {
                return $array;
            }
            for ($i = 1; $i < count($args); $i++) {
                if (is_array($args[$i])) {
                    $array = LinevenApoTools::arrayRecurse($array, $args[$i]);
                }
            }
            return $array;
        } else {
            return array_replace_recursive($array1, $array2);
        }
    }
    
    /**
     * Array recursive.
     *
     * @param array $array Array
     * @param array $array1 Array
     * @return array
     */
    public static function arrayRecurse($array, $array1)
    {
        foreach ($array1 as $key => $value) {
            // create new key in $array, if it is empty or not an array
            if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key]))) {
                $array[$key] = array();
            }

            // overwrite the value in the base array
            if (is_array($value)) {
                $value = LinevenApoTools::arrayRecurse($array[$key], $value);
            }
            $array[$key] = $value;
        }
        return $array;
    }

    /**
     * Array column.
     *
     * @param array $array Array
     * @param string $column_name Column name
     * @return Array
     */
    public static function arrayColumn($array, $column_name)
    {
        if (!function_exists('array_column')) {
            return array_map(
                function ($element) use ($column_name) {
                    return $element[$column_name];
                },
                $array
            );
        } else {
            return array_column($array, $column_name);
        }
    }
    
    /**
     * Return hexa to rgb
     *
     * @param string $hex_str exa string
     * @param string $return_string Return string or array
     * @param string $separator Separator to use
     *
     * @return Array/String
     */
    public static function hex2rgb($hex_str, $return_string = false, $separator = ',')
    {
        $hex_str = preg_replace("/[^0-9A-Fa-f]/", '', $hex_str); // Gets a proper hex string
        $rgb_array = array();
        if (Tools::strlen($hex_str) == 6) {
            $color_val = hexdec($hex_str);
            $rgb_array['r'] = 0xFF & ($color_val >> 0x10);
            $rgb_array['g'] = 0xFF & ($color_val >> 0x8);
            $rgb_array['b'] = 0xFF & $color_val;
        } elseif (Tools::strlen($hex_str) == 3) {
            $rgb_array['r'] = hexdec(str_repeat(Tools::substr($hex_str, 0, 1), 2));
            $rgb_array['g'] = hexdec(str_repeat(Tools::substr($hex_str, 1, 1), 2));
            $rgb_array['b'] = hexdec(str_repeat(Tools::substr($hex_str, 2, 1), 2));
        } else {
            return false;
        }
        return $return_string ? implode($separator, $rgb_array) : $rgb_array;
    }

    /**
     * Replace special chars
     *
     * @param string $str string
     *
     * @return String
     */
    public static function replaceSpecialChars($str)
    {
        $str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
        $str = preg_replace('#&([A-za-z])(?:uml|circ|tilde|acute|grave|cedil|ring);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
        $str = preg_replace('#&[^;]+;#', '', $str);
        return $str;
    }

    /**
     * Get all values from $_POST/$_GET
     * @return mixed
     */
    public static function getAllValues()
    {
        if (method_exists('Tools', 'getAllValues')) {
            return Tools::getAllValues();
        }
        return $_POST + $_GET;
    }

    /**
     * Return the short URI for a module.
     * Could be use for return URL process or ajax call.
     *
     * @param string $file of the module you want to target
     * @param array $options
     *      (value=key)
     * @return string
     */
    public static function getShortURI($file = '', $options = array())
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        return ('modules/'.$configuration->name.'/'.(! empty($file) ? $file : '')).
            (! empty($options) ? '?'.http_build_query($options, '', '&') : '');
    }

    /**
     * Return the full URI for a module.
     *
     * @param string $file of the module you want to target
     * @param array $options
     *      (value=key)
     * @return string
     */
    public static function getBaseURI($file = '', $options = array())
    {
        return __PS_BASE_URI__.self::getShortURI($file, $options);
    }
    
    /**
     * Return the complete URI for shop.
     * Could be use for return URL process or ajax call.
     *
     * @param string $file of the module you want to target
     * @param string $id_shop Shop id
     * @param array $options
     *      (value=key)
     * @return string
     */
    public static function getShopURI($file = '', $id_shop = null, $options = array())
    {
        $uri = Tools::getShopProtocol().$_SERVER['HTTP_HOST'].
            (__PS_BASE_URI__.(! empty($file) ? $file : '')).
            (! empty($options) ? '?'.http_build_query($options, '', '&') : '');
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') !== false &&
            Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1 && $id_shop != null) {
            $shop = new Shop($id_shop);
            $uri = $shop->getBaseURL().(! empty($file) ? $file : '').
                (! empty($options) ? '?'.http_build_query($options, '', '&') : '');
        }
        return $uri;
    }
    
    /**
     * Return the complete URI in front.
     *
     * @param string $controller Controller
     * @param string $id_shop Shop id
     * @return string
     */
    public static function getFrontofficeURI($controller, $id_shop = null, $short_uri = false)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $uri = '';
        if (!$short_uri) {
            $uri .= LinevenApoTools::getShopURI('', $id_shop);
        }
        $uri .= 'index.php?fc=module&module='.$configuration->getDirName().'&controller='.$controller;
        return $uri;
    }

    /**
     * Get admin module link.
     *
     * @param string $controller Controller name
     * @param array $params Params
     * @param boolean $secure_page Secure page
     * @param boolean $ssl Ssl
     * @param unknown $id_lang Id lang
     * @param unknown $id_shop Id shop
     * @param string $relative_protocol Relative protocol
     * @return string
     */
    public static function getFrontModuleLink(
        $controller = 'default',
        $secure_page = false,
        array $params = array(),
        $ssl = null,
        $id_lang = null,
        $id_shop = null,
        $relative_protocol = false
    ) {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($secure_page) {
            $ssl = Configuration::get('PS_SSL_ENABLED');
        }
        return Context::getContext()->link->getModuleLink(
            $configuration->name,
            $controller,
            $params,
            $ssl,
            $id_lang,
            $id_shop,
            $relative_protocol
        );
    }

    /**
     * Return the complete URI in admin.
     *
     * @param string $controller Controller
     * @param string $action Action
     * @param string $navigation Navigation
     * @param boolean $token Token If token must added
     * @return string
     */
    public static function getBackofficeURI($controller = null, $action = null, $navigation = null, $token = true)
    {
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $return = '';
        if ($module_context->getAccess() == LinevenApoContext::$access_by_module_configuration) {
            $return = Context::getContext()->link->getAdminLink('AdminModules', false).
                '&configure='.$configuration->name.'&tab_module='.$configuration->getModule()->tab.
                ($token ? '&token='.Tools::getAdminTokenLite('AdminModules') : '').
                '&module_name='.$configuration->name.
                '&'.LinevenApoTools::getControllerUri($controller, $action, $navigation);
        } elseif ($module_context->getAccess() == LinevenApoContext::$access_by_admin_tab) {
            $return = self::getAdminTabURI($controller, $action, $navigation);
        }
        return $return;
    }

    /**
     * Return the complete URI for tab uri.
     *
     * @param string $controller Controller
     * @param string $action Action
     * @param string $navigation Navigation
     * @param string $admin_controller Admin controller
     * @return string
     */
    public static function getAdminTabURI(
        $controller = null,
        $action = null,
        $navigation = null,
        $admin_controller = null
    ) {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($configuration->getParameter('is_backoffice_menu_available')) {
            $backoffice_menu_settings = LinevenApoTabs::getTabs();
            if ($admin_controller == null) {
                $admin_controller = Tools::getValue('controller');
            }
            if ($admin_controller && isset($backoffice_menu_settings[$admin_controller])) {
                return Context::getContext()->link->getAdminLink(
                    $admin_controller,
                    false
                ).'&token='.Tools::getAdminTokenLite($admin_controller).
                    '&'.LinevenApoTools::getControllerUri($controller, $action, $navigation);
            }
        }
        return '';
    }
    
    /**
     * Get admin module link.
     * @return string
     */
    public static function getAdminModuleLink()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        return Context::getContext()->link->getAdminLink('AdminModules', false).
            '&configure='.$configuration->name.'&tab_module='.$configuration->getModule()->tab.
            '&token='.Tools::getAdminTokenLite('AdminModules').'&module_name='.$configuration->name;
    }
    
    /**
     * Return admin token.
     *
     * @return string
     */
    public static function getAdminToken()
    {
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($module_context->getAccess() == LinevenApoContext::$access_by_module_configuration) {
            return Tools::getAdminTokenLite('AdminModules');
        } elseif ($module_context->getAccess() == LinevenApoContext::$access_by_admin_tab) {
            if ($configuration->getParameter('is_backoffice_menu_available')) {
                $backoffice_menu_settings = LinevenApoTabs::getTabs();
                $admin_tab_controller = Tools::getValue('controller');
                if ($admin_tab_controller && isset($backoffice_menu_settings[$admin_tab_controller])) {
                    return Tools::getAdminTokenLite($admin_tab_controller);
                }
            }
        }
        return false;
    }
    
    /**
     * Return the controller URI.
     *
     * @param string $controller Controller
     * @param string $action Action
     * @param string $navigation Navigation
     * @return string
     */
    public static function getControllerUri($controller = null, $action = null, $navigation = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $return = '';
        if ($controller != null) {
            if ($action == null) {
                $action = 'index';
            }
            $return .= $configuration->name.'_controller='.$controller.
            '&'.$configuration->name.'_action='.$action.
            '&'.$configuration->name.'_navigation='.$navigation;
        }
        return $return;
    }
    
    /**
     * Display message.
     *
     * @param string $type Type
     * @param string|array $message Message
     * @return void
     */
    public static function displayHtmlMessage($type, $message)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        switch ($type) {
            case 'error':
                return $configuration->getModule()->displayError($message);
                //break;
            case 'warning':
                return $configuration->getModule()->displayWarning($message);
                //break;
            case 'confirmation':
                return $configuration->getModule()->displayConfirmation($message);
                //break;
        }
    }

    /**
     * Display html in code.
     *
     * @param string $type Type
     * @param string|array $message Message
     * @return void
     */
    public static function displayHtmlInCode($type, $settings)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        Context::getContext()->smarty->assign('type', $type);
        Context::getContext()->smarty->assign('settings', $settings);
        return $configuration->getModule()->display(
            _PS_MODULE_DIR_.$configuration->getModule()->name,
            'views/templates/admin/module/_partials/helpers/html_content.tpl'
        );
    }
}
