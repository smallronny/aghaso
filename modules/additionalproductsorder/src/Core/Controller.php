<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoController
{
    /**
     * Controller name
     * @var string
     */
    public $controller_name;

    /**
     * Action name
     * @var string
     */
    public $action_name;

    /**
     * Params
     * @var array
     */
    public $params = array();

    /**
     * Controller type
     * @var string
     */
    protected $controller_type;

    /**
     * Translator
     * @var array
     */
    protected $translator;

    /**
     * Module context
     * @var LinevenApoContext
     */
    protected $module_context;

    /**
     * Presenter
     * @var object
     */
    protected $presenter;

    /**
     * Html rendering
     * @var object
     */
    protected $html_rendering;

    public static $controller_type_module = 'module';                // Module
    public static $controller_type_admin = 'admin';                  // Admin
    public static $controller_type_admin_hook = 'admin_hook';        // Admin hook
    public static $controller_type_front = 'front';                  // Front
    public static $controller_type_front_service = 'front_service';  // Front service
    public static $controller_type_front_hook = 'front_hook';        // Front hook
    
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->module_context = LinevenApoContext::getContext();
        $this->translator = new LinevenApoTranslator();
    }
    
    /**
     * Run.
     *
     * @param string $action Action (optional)
     * @return void
     */
    public function run($action = null)
    {
        // Disable load module on module page
        if ($this->controller_type == LinevenApoController::$controller_type_admin_hook &&
            ((Tools::isSubmit('controller') && Tools::getValue('AdminModules')) ||
            (isset($_SERVER['PATH_INFO']) && 0 === strpos($_SERVER['PATH_INFO'], '/module/')))) {
            return false;
        }
        $configuration = LinevenApoConfiguration::getConfiguration();
        $action_value = '';
        if ($action == null) {
            $action_name = '';
            $action_name = $configuration->name.'_action';
            if (! Tools::isSubmit($action_name) ||
                (Tools::isSubmit($action_name) && Tools::getValue($action_name) == '')) {
                $action_value = 'index';
            } else {
                $action_value = Tools::getValue($action_name);
            }
        } else {
            $action_value = $action;
        }
        $this->action_name = $action_value;
        $this->presenter = LinevenApoPresenter::getPresenter($this->controller_type);
        $this->preInit();
        $this->init();
        $this->preRun();
        $action_method = $action_value.'Action';
        if (method_exists(get_class($this), $action_method)) {
            $action_return = $this->$action_method();
        }
        $this->postRun();
        // Render
        if ($this->presenter->isRender()) {
            $this->preRender();
            $this->presenter->preRender();
            $this->html_rendering = $this->presenter->render();
            $this->presenter->postRender();
            $this->postRender();
            return $this->html_rendering;
        }
        return $action_return;
    }
    
    /**
     * Pre-init.
     *
     * @return void
     */
    private function preInit()
    {
        $this->presenter->controller_name = $this->controller_name;
        $this->presenter->action_name = $this->action_name;
        $this->presenter->params = $this->params;
        $this->presenter->preInit();
    }
    
    /**
     * Init.
     *
     * @return void
     */
    public function init()
    {
    }
    
    /**
     * PreRun.
     *
     * @return void
     */
    public function preRun()
    {
    }
    
    /**
     * PostRun.
     *
     * @return void
     */
    public function postRun()
    {
        $this->presenter->params = $this->params;
        $this->presenter->init();
    }
    /**
     * PreRender.
     *
     * @return void
     */
    public function preRender()
    {
    }

    /**
     * Render the presenter.
     *
     * @return mixed
     */
    public function render()
    {
        return $this->presenter->render();
    }

    /**
     * PostRender.
     *
     * @return void
     */
    public function postRender()
    {
    }

    /**
     * Get html rendering.
     *
     * @return string
     */
    public function getHtmlRendering()
    {
        return $this->html_rendering;
    }

    /**
     * Redirection to another controller.
     *
     * @param string $navigation Navigation
     * @param string $params Params (optional)
     */
    public function redirect($navigation, $params = array())
    {
        $items = LinevenApoNavigation::getItemByNavigationPath($navigation);
        $action = null;
        if (isset($items['action'])) {
            $action = $items['action'];
        }
        $params['navigation_pipe'] = $navigation;
        $this->params['redirection'] = true;
        $controller = LinevenApoController::getController($items['controller'], $this->controller_type);
        $controller->params = $params;
        $controller->params['controller'] = $items['controller'];
        $controller->params['action'] = $action;
        $controller->params['called_by_redirection'] = true;
        return $controller->run($action);
    }

    /**
     * Get controller.
     * @param string $controller Controller
     * @param string $controller_type Controller type
     *
     * @return void
     */
    public static function getController($controller = null, $controller_type = 'module')
    {
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $controller_post_name = '';
        $controller_name = '';
        if ($controller == null) {
            $controller_post_name = $configuration->name.'_controller';
            if (! Tools::isSubmit($controller_post_name) ||
                (Tools::isSubmit($controller_post_name) && Tools::getValue($controller_post_name) == '')) {
                $controller_name = 'Index';
            } else {
                $controller_name = Tools::getValue($controller_post_name);
            }
        } else {
            $controller_name = $controller;
        }
        $class = 'Lineven'.Tools::ucfirst(Tools::strtolower($configuration->getCode()));
        $class .= Tools::ucfirst($module_context->getApplication());
        if ($controller_type == LinevenApoController::$controller_type_front_service) {
            $class .= 'Service';
        }
        $class .= Tools::ucfirst($controller_name).'Controller';
        self::includeController($controller_name, $class, $controller_type);
        $obj_class = new $class();
        if ($controller_type == LinevenApoController::$controller_type_front_service) {
            return $obj_class;
        }
        $obj_class->controller_type = $controller_type;
        $obj_class->controller_name = $controller_name;
        return $obj_class;
    }
    
    /**
     * Include controller.
     * @param string $controller_name Controller name
     * @param string $class Class name
     * @param string $controller_type Controller type
     *
     * @return void
     */
    private static function includeController($controller_name, $class, $controller_type)
    {
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $path = $configuration->getDirectoryPath(). '/controllers/';
        if ($module_context->getApplication() == LinevenApoContext::$application_backoffice) {
            if ($controller_type == LinevenApoController::$controller_type_module) {
                $path .= 'module/';
            }
            if ($controller_type == LinevenApoController::$controller_type_admin) {
                $path .= 'admin/';
            }
            if ($controller_type == LinevenApoController::$controller_type_admin_hook) {
                $path .= 'hook/admin/';
            }
        } else {
            if ($controller_type == LinevenApoController::$controller_type_front) {
                $path .= 'front/';
            }
            if ($controller_type == LinevenApoController::$controller_type_front_service) {
                $path .= 'front/services/FrontService';
            }
            if ($controller_type == LinevenApoController::$controller_type_front_hook) {
                $path .= 'hook/';
            }
        }
        if (! file_exists($path.$controller_name.'.php')) {
            exit();
        }
        if (! in_array($class, get_declared_classes())) {
            require_once($path.$controller_name.'.php');
        }
    }
}
