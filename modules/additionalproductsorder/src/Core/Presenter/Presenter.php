<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoPresenter
{
    /**
     * Controller name
     * @var string
     */
    public $controller_name;
    
    /**
     * Action name
     * @var string
     */
    public $action_name;
    
    /**
     * Controller type
     * @var string
     */
    public $controller_type;
    
    /**
     * Path
     * @var string
     */
    public $path;

    /**
     * Partial path view
     * @var string
     */
    public $partial_path_view;

    /**
     * Partial path for the presenter
     * @var string
     */
    public $partial_path_presenter;

    /**
     * Params
     * @var array
     */
    public $params = array();
    
    /**
     * Enable cache
     *
     * @var boolean
     */
    public $enable_cache;

    protected $module_context;
    protected $default_template;
    protected $no_render = false;                      /* No render */
    protected $datas = array();                        /* Datas */
    protected $outputs;                                /* Outputs */
    protected $html_output;                            /* Html output */
    protected static $smarty_variables = array();      /* Smarty variables */

    protected static $js_def = array();                /* Js def */

    public static $output_type_content = 'content';                     // Output content
    public static $output_type_template = 'template';                   // Output a template
    public static $output_type_path_template = 'path_template';         // Output a path and template
    public static $output_type_default_template = 'default_template';   // Output a default template
    public static $output_type_script = 'script';                       // Output script
    public static $output_type_js_def = 'js_def';                       // Output js def

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->outputs = array();
        if ($this->module_context->getApplication() != LinevenApoContext::$application_backoffice) {
            $this->enable_cache = $configuration->isCacheActive();
        } else {
            $this->enable_cache = false;
        }
    }

    /**
     * Get presenter.
     * @param string $controller_type Controller type
     *
     * @return void
     */
    public static function getPresenter($controller_type = null)
    {
        $presenter = null;
        switch ($controller_type) {
            case LinevenApoController::$controller_type_admin_hook:
            case LinevenApoController::$controller_type_front_hook:
                $presenter = new LinevenApoHookPresenter();
                break;
            case LinevenApoController::$controller_type_front:
                $presenter = new LinevenApoFrontPresenter();
                break;
            default:
                $presenter = new LinevenApoModulePresenter();
                break;
        }
        if ($presenter != null) {
            $presenter->controller_type = $controller_type;
        }
        return $presenter;
    }

    /**
     * PreInit
     * @return void
     */
    public function preInit()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        
        // Default template
        $this->default_template = 'index.tpl';
        if ($this->action_name != '') {
            $this->default_template = $this->action_name.'.tpl';
        }
        // Set path
        $this->partial_path_view = 'views/templates/';
        if ($this->module_context->getApplication() == LinevenApoContext::$application_backoffice) {
            if ($this->controller_type == LinevenApoController::$controller_type_module) {
                $this->partial_path_view .= 'admin/module/';
            }
            if ($this->controller_type == LinevenApoController::$controller_type_admin) {
                $this->partial_path_view .= 'admin/';
            }
            if ($this->controller_type == LinevenApoController::$controller_type_admin_hook) {
                $this->partial_path_view .= 'hook/admin/';
            }
        } else {
            if ($this->controller_type == LinevenApoController::$controller_type_front) {
                $this->partial_path_view .= 'front/';
            }
            if ($this->controller_type == LinevenApoController::$controller_type_front_hook) {
                $this->partial_path_view .= 'hook/';
            }
        }
        $this->partial_path_presenter = $this->partial_path_view.Tools::strtolower($this->controller_name).'/';
        $this->path = $configuration->getDirectoryPath().'/'.$this->partial_path_view;
    }
    
    /**
     * Init.
     * @return void
     */
    public function init()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($this->isRender()) {
            if (!isset(self::$smarty_variables['lineven'])) {
                $ssl = Configuration::get('PS_SSL_ENABLED');
                // Assign default variables to Smarty
                $link = new Link();
                self::$smarty_variables = array(
                    'lineven' => array(
                        'prestashop' => array(
                            'version' => _PS_VERSION_,
                            'major_version' => Tools::substr(_PS_VERSION_, 0, 3),
                            'shop_name' => Configuration::get('PS_SHOP_NAME'),
                            'shop_uri' => LinevenApoTools::getShopURI(),
                            'shop_logo' => LinevenApoTools::getShopURI().'img/logo.jpg',
                            'is_ssl' => $ssl,
                            
                        ),
                        'global' => array(
                            'class_style_version' => 'ps'.str_replace(
                                '.',
                                '',
                                Tools::substr(_PS_VERSION_, 0, 3)
                            )
                        ),
                        Tools::strtolower($configuration->getCode()) => array (
                            'environment' => $this->module_context->getEnvironment(false),
                            'code' => $configuration->getCode(),
                            'id' => $configuration->getModule()->id,
                            'name' => $configuration->name,
                            'display_name' => $configuration->getModule()->displayName,
                            'is_active' => $this->module_context->isModuleActive(),
                            'version' => $configuration->version,
                            'is_debug_mode' =>(int)$this->module_context->isDebugModeActive(),
                            'is_test_mode' =>(int)$this->module_context->isTestModeActive(),
                            'urls' => array(
                                'service_dispatcher_url' => Context::getContext()->link->getModuleLink(
                                    $configuration->name,
                                    'servicedispatcher'
                                ),
                                'service_dispatcher_ssl_url' => LinevenApoTools::getFrontModuleLink(
                                    'servicedispatcher',
                                    true
                                ),
                                'short_url' => LinevenApoTools::getShortURI(),
                                'base_url' => LinevenApoTools::getBaseURI(),
                                'css_url' => LinevenApoTools::getBaseURI().'views/css/',
                                'cart_url' => $link->getPageLink('cart', $ssl).'?action=show',
                                'order_url' => $link->getPageLink('order', $ssl)
                            )
                        )
                    )
                );
            }
            if ($this->controller_type == LinevenApoController::$controller_type_admin_hook ||
                $this->controller_type == LinevenApoController::$controller_type_front_hook) {
                // Admin or Front Hooks
                self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())]['hook'] = array(
                    'controller_name' => $this->controller_name,
                    'action_name' => $this->action_name,
                    'datas' => $this->datas
                );
            }
        }
    }

    /**
     * PreRender.
     *
     * @return void
     */
    public function preRender()
    {
        if ($this->isRender()) {
            $this->assignSmartyVariables();
        }
    }
    
    /**
     * Render template.
     *
     * @param object $cache_id Cache id
     * @return mixed
     */
    public function render($cache_id = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($this->isRender() && count($this->outputs)) {
            $this->html_output = '';
            foreach ($this->outputs as $output) {
                if ($cache_id == null) {
                    $cache_id = $this->getCacheId($output);
                }
                switch ($output['type']) {
                    case self::$output_type_content:
                        $this->html_output .= $output['output'];
                        break;
                    case self::$output_type_path_template:
                    case self::$output_type_template:
                    case self::$output_type_default_template:
                        $this->html_output .= $this->displayTemplate(
                            $this->getTemplatePath($output),
                            $cache_id
                        );
                        break;
                    case self::$output_type_js_def:
                        $script = 'var '.'lineven_'.Tools::strtolower($configuration->getCode()).' = '.
                            json_encode(LinevenApoPresenter::$js_def).';';
                        Context::getContext()->smarty->assign('lineven_js_variables', $script);
                        $this->html_output .= $configuration->getModule()->display(
                            _PS_MODULE_DIR_.$configuration->getModule()->name,
                            $this->partial_path_view.'_partials/javascript.tpl'
                        );
                        break;
                    case self::$output_type_script:
                        Context::getContext()->smarty->assign('lineven_script_content', $output['output']);
                        $this->html_output .= $configuration->getModule()->display(
                            _PS_MODULE_DIR_.$configuration->getModule()->name,
                            $this->partial_path_view.'_partials/javascript.tpl'
                        );
                        break;
                }
            }
            return $this->html_output;
        }
        return false;
    }

    /**
     * Dislay template.
     *
     * @param object $cache_id Cache id
     * @return mixed
     */
    private function displayTemplate($template, $cache_id = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($this->controller_type == LinevenApoController::$controller_type_front_hook) {
            return $configuration->getModule()->fetch(
                'module:'.$configuration->getModule()->name.'/'.$template,
                $cache_id
            );
        }
        return $configuration->getModule()->display(
            _PS_MODULE_DIR_.$configuration->getModule()->name,
            $template,
            $cache_id
        );
    }

    /**
     * Get template path.
     *
     * @param array $output Output
     * @return string
     */
    private function getTemplatePath($output)
    {
        $template = $output['output'];
        switch ($output['type']) {
            case self::$output_type_path_template:
                $template = $this->partial_path_view.$output['output'].'.tpl';
                break;
            case self::$output_type_template:
                $template = $this->partial_path_presenter.$output['output'].'.tpl';
                break;
            case self::$output_type_default_template:
                $template = $this->partial_path_presenter.$this->default_template;
                break;
        }
        return $template;
    }

    /**
     * PostRender.
     *
     * @return void
     */
    public function postRender()
    {
    }
    
    /**
     * No render the view.
     *
     * @return void
     */
    public function noRender()
    {
        $this->no_render = true;
    }

    /**
     * Is render view.
     *
     * @return boolean
     */
    public function isRender()
    {
        return !$this->no_render;
    }

    /**
     * Get datas.
     *
     * @return array
     */
    public function getDatas()
    {
        return $this->datas;
    }

    /**
     * Get data.
     *
     * @param string $key Key
     * @return mixed
     */
    public function getData($key)
    {
        if (isset($this->datas[$key])) {
            return $this->datas[$key];
        }
        return null;
    }

    /**
     * Set datas.
     *
     * @param string $key Key
     * @param mixed $value Value
     * @return void
     */
    public function addData($key, $value)
    {
        $this->datas[$key] = $value;
    }

    /**
     * Add datas.
     *
     * @param array $values
     * @return void
     */
    public function addDatas($value)
    {
        $this->datas = array_merge($this->datas, $value);
    }

    /**
     * Set datas.
     *
     * @param array $values
     * @return void
     */
    public function setDatas($values)
    {
        if (is_array($values)) {
            foreach ($values as $key => $value) {
                $this->datas[$key] = $value;
            }
        }
    }

    /**
     * Add output to the view.
     *
     * @param string $type Type
     * @param mixed $output Output (optional)
     * @param boolean $enable_cache (optional)
     * @return void
     */
    public function addOutput($type, $output = null, $enable_cache = true)
    {
        $this->outputs[$output] = array(
            'type' => $type,
            'output' => $output,
            'enable_cache' => $enable_cache,
            'cache_id' => null
        );
    }
    
    /**
     * Get outputs.
     *
     * @return array
     */
    public function getOutputs()
    {
        return $this->outputs;
    }
    
    /**
     * Get output.
     * @param $name Output name
     * @return array
     */
    public function getOutput($name)
    {
        if (count($this->outputs) && isset($this->outputs[$name])) {
            return $this->outputs[$name];
        }
        return $this->outputs;
    }

    /**
     * Get html output.
     * @return string
     */
    public function getHtmlOutput()
    {
        return $this->html_output;
    }

    /**
     * Clear outputs.
     *
     * @return void
     */
    public function clearOutputs()
    {
        $this->outputs = array();
    }

    /**
     * Get smarty variables.
     *
     * @return array
     */
    public function getSmartyVariables()
    {
        return self::$smarty_variables;
    }

    /**
     * Assign smarty variables.
     *
     * @return void
     */
    public function assignSmartyVariables()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $existing_smarty_variables = Context::getContext()->smarty->getTemplateVars('lineven');
        if ($existing_smarty_variables) {    // Lineven variables exists in smarty context
            $existing_smarty_variables[Tools::strtolower($configuration->getCode())] =
                self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())];
            self::$smarty_variables = array('lineven' => $existing_smarty_variables);
        }
        Context::getContext()->smarty->assign(self::$smarty_variables);
    }
    
    /**
     * Render js def.
     * @param bool $only_datas Only modules datas
     * @return array
     */
    public function renderJsDef($only_datas = true)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (!$only_datas &&
            !isset(self::$js_def['id']) &&
            isset(self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())])) {
            $id_customer = Context::getContext()->customer->id;
            $id_guest = 0;
            if ($id_customer == null) {
                $id_guest = Context::getContext()->cart->id_guest;
                $id_customer = 0;
            }
            self::$js_def['id'] = $configuration->getModule()->id;
            self::$js_def['name'] = $configuration->name;
            self::$js_def['is_debug_mode'] = (int)$this->module_context->isDebugModeActive();
            self::$js_def['is_test_mode'] = (int)$this->module_context->isTestModeActive();
            self::$js_def['ajax'] = array(
                'id_guest' => $id_guest,
                'id_customer' => $id_customer,
                'customer_token' => sha1(Context::getContext()->customer->secure_key),
                'guest_token' => sha1($configuration->name.$id_guest.$_SERVER['REMOTE_ADDR'].date('Y-m-d')),
            );
            self::$js_def['urls'] = self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())]['urls'];
        }
        if (!$only_datas || isset(self::$js_def['datas'])) {
            $this->addOutput(self::$output_type_js_def);
        }
    }

    /**
     * Add js def.
     *
     * @param array $datas Datas (optional)
     * @return array
     */
    public static function addJsDef($datas = array())
    {
        self::$js_def['datas'] = $datas;
    }


    /**
     * Get js def.
     *
     * @return array
     */
    public static function getJsDef()
    {
        return self::$js_def;
    }

    /**
     * Get cache id.
     * @param mixed $value (array for output or string for cache id)
     *
     * @return string
     */
    public function getCacheId($value)
    {
        if ($this->enable_cache) {
            $configuration = LinevenApoConfiguration::getConfiguration();
            if ($value != null && is_array($value) && $value['enable_cache']) {
                return LinevenApoCache::getCacheId(
                    $configuration->getModule()->name.'-'.
                    Tools::strtolower($this->controller_name).
                    '-'.$value['output']
                );
            } else {
                if ($value != null) {
                    return LinevenApoCache::getCacheId($value);
                }
            }
        }
        return null;
    }
    
    /**
     * Is cached.
     * @param string $output Output
     * @param string $cache_id Cache Id
     *
     * @return boolean
     */
    public function isCached($output = null, $cache_id = null)
    {
        if ($this->enable_cache) {
            if (is_array($this->outputs) && count($this->outputs)) {
                $configuration = LinevenApoConfiguration::getConfiguration();
                $template = '';
                if ($output == null) {
                    reset($this->outputs);
                    $output = current($this->outputs);
                    $template = $this->getTemplatePath($output);
                } else {
                    if (isset($this->outputs[$output])) {
                        $template = $this->getTemplatePath($this->outputs[$output]);
                    }
                }
                if ($output != null) {
                    $template = 'module:'.$configuration->getModule()->name.'/'.$template;
                    return LinevenApoCache::isCached($template, $this->getCacheId($output));
                }
            }
        }
        return false;
    }
}
