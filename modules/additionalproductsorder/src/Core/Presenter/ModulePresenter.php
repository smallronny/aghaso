<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModulePresenter extends LinevenApoPresenter
{
    /**
     * Only content.
     * @var boolean
     */
    public $only_content = false;

    /**
     * No layout.
     * @var boolean
     */
    public $no_layout = false;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * PreRender.
     *
     * @return void
     */
    public function preRender()
    {
        if (Tools::isSubmit('no_layout')) {
            $this->no_layout = true;
        }
        if ($this->isRender()) {
            $module_context = LinevenApoContext::getContext();
            $configuration = LinevenApoConfiguration::getConfiguration();
            // Set media
            $configuration->getModule()->setBackofficeMedia();
            // Navivation
            $show_navigation = true;
            $show_menus = false;
            $display_in_tab = false;
            if ($module_context->getAccess() == LinevenApoContext::$access_by_admin_tab) {
                $options_navigation = LinevenApoNavigation::getOptions();
                $ps_controller = '';
                if (Tools::isSubmit('controller')) {
                    $ps_controller = Tools::getValue('controller');
                }
                if ($ps_controller != '' && isset($options_navigation['admin_tab']) &&
                    isset($options_navigation['admin_tab'][$ps_controller]) &&
                    isset($options_navigation['admin_tab'][$ps_controller]['show_navigation'])) {
                    $show_navigation = (int)$options_navigation['admin_tab'][$ps_controller]['show_navigation'];
                    if (isset($options_navigation['admin_tab'][$ps_controller]['show_menus'])) {
                        $show_menus = $options_navigation['admin_tab'][$ps_controller]['show_menus'];
                    }
                    $navigation_path = $options_navigation['admin_tab'][$ps_controller]['shortcut'];
                    if (Tools::isSubmit($configuration->name.'_navigation')) {
                        $navigation_path = Tools::getValue($configuration->name.'_navigation');
                    }
                } else {
                    $navigation_path = $options_navigation['default'];
                }
                $display_in_tab = true;
            } else { // Navigation if AdminTab
                $navigation_path = Tools::getValue($configuration->name.'_navigation');
            }
            if (isset($this->params['called_by_redirection']) && $this->params['called_by_redirection'] &&
                isset($this->params['navigation_pipe'])) {
                $navigation_path = $this->params['navigation_pipe'];
            }
            LinevenApoNavigation::setNavigationPath($navigation_path);
            $navigation_tree = LinevenApoNavigation::getTree();
            $additional_path = '';
            if (isset($this->params['breadcrumbs_additional_path'])) {
                $additional_path = $this->params['breadcrumbs_additional_path'];
            }
            $breadcrumbs = LinevenApoNavigation::getBreadcrumbs($additional_path);

            // Assign smarty variables
            self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())]['urls']['backoffice_action_url'] = LinevenApoTools::getBackofficeURI();
            self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())]['admin'] = array(
                'controller_name' => $this->controller_name,
                'action_name' => $this->action_name,
                'datas' => $this->datas
            );
            self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())]['admin']['layout'] = array(
                'display_in_tab' => $display_in_tab,
                'navigation' => array(
                    'show_menus' => $show_menus,
                    'show_navigation' => $show_navigation,
                    'breadcrumbs' => $breadcrumbs,
                    'tree' => $navigation_tree
                )
            );
            parent::preRender();
        }
    }
    
    /**
     * Render template.
     * @param object $cache_id Cache id
     *
     * @return mixed
     */
    public function render($cache_id = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $html_layout_header_output = '';
        $html_layout_header_output = '';
        $html_view_output = '';
        $html_layout_footer_output = '';
        if (!$this->no_render) {
            if (!$this->no_layout) {
                $html_layout_header_output = $configuration->getModule()->display(
                    _PS_MODULE_DIR_.$configuration->getModule()->name,
                    $this->partial_path_view.'_partials/layout/header.tpl'
                );
            }
            $html_view_output = $configuration->getModule()->display(
                _PS_MODULE_DIR_.$configuration->getModule()->name,
                $this->partial_path_view.'_partials/notifications/messages.tpl'
            );
            $html_view_output .= parent::render();
            if (!$this->no_layout) {
                $html_layout_footer_output = $configuration->getModule()->display(
                    _PS_MODULE_DIR_.$configuration->getModule()->name,
                    $this->partial_path_view.'_partials/layout/footer.tpl'
                );
            }
        } else {
            return '';
        }
        if ($this->only_content) {
            echo $html_view_output;
            die();
        }
        return $html_layout_header_output.$html_view_output.$html_layout_footer_output;
    }
}
