<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoFrontPresenter extends LinevenApoPresenter
{

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->init();
    }
    
    /**
     * Present.
     */
    public function present()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $path_to_templates = 'module:'.$configuration->getName().'/views/templates/front/';
        self::$smarty_variables['lineven'][Tools::strtolower($configuration->getCode())]['front'] = array(
            'path_to_templates' => $path_to_templates,
            'datas' => $this->datas
        );
        $this->assignSmartyVariables();
    }
    
    /**
     * Get template.
     * @param string $template name
     * @param array $options
     *      path (string) : optional path
     *      include_version_in_name (boolean) : Include Prestashop version in template file name
     *      include_version_in_path (boolean) : Include Prestashop version in template path
     */
    public function getTemplate($template, $options = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $path = '';
        if (isset($options['path'])) {
            $path = $options['path'];
        }
        if (isset($options['include_version_in_name']) && $options['include_version_in_name']) {
            $template .= '_'.self::$smarty_variables['lineven']['prestashop']['major_version'];
        }
        $template .= '.tpl';
        if (isset($options['include_version_in_path']) && $options['include_version_in_path']) {
            $path .= self::$smarty_variables['lineven']['prestashop']['major_version'];
        }
        if ($path != null) {
            $template = $path.'/'.$template;
        }
        $template = 'module:'.$configuration->getName().'/views/templates/front/'.$template;
        return $template;
    }
}
