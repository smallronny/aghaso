<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModule extends Module
{
    public static $is_init = 0; // Is module init

    protected $module_context;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->module_context = LinevenApoContext::getContext();
    }

    /**
     * Init.
     * @param boolean $force Force initialization
     *
     * @return void
     */
    public function init($force = false)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        // Backoffice initialization
        if ($force || $this->module_context->getApplication() == LinevenApoContext::$application_backoffice) {
            // Require Backoffice files
            require_once($configuration->getDirectoryPath().'/config/require_backoffice.inc.php');
            // Initialize data descriptions
            $this->loadDatasDefinition();
            // Initialize hooks descriptions
            $this->loadHooksDefinition();
            // Load navigation
            $this->loadNavigation();
        }
    }

    /**
     * Is init.
     *
     * @return boolean
     */
    public function isInit()
    {
        return self::$is_init;
    }

    /**
     * Initializated
     *
     * @return void
     */
    public function initializated()
    {
        self::$is_init = 1;
    }
    
    /**
     * For display the content of this module on the administration panel.
     *
     * @return string
     */
    public function getContent()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->module_context->setApplication(LinevenApoContext::$application_backoffice);
        $this->init();
        $controller_to_call = $configuration->getParameter('backoffice_default_controller');
        $action_to_call = $configuration->getParameter('backoffice_default_action');
        $params = array();
        if ($this->module_context->getAccess() == LinevenApoContext::$access_by_admin_tab) {
            $options_navigation = LinevenApoNavigation::getOptions();
            $ps_controller = '';
            if (Tools::isSubmit('controller')) {
                $ps_controller = Tools::getValue('controller');
            }
            if ($ps_controller != '' && isset($options_navigation['admin_tab']) &&
                isset($options_navigation['admin_tab'][$ps_controller]) &&
                isset($options_navigation['admin_tab'][$ps_controller]['shortcut'])) {
                $item = LinevenApoNavigation::getItemByNavigationPath(
                    $options_navigation['admin_tab'][$ps_controller]['shortcut']
                );
                $controller_to_call = $item['controller'];
                $action_to_call = $item['action'];
                if (isset($options_navigation['admin_tab'][$ps_controller]['params'])) {
                    $params = $options_navigation['admin_tab'][$ps_controller]['params'];
                }
            }
        }

        if (!Tools::isSubmit('setShopContext')) {
            if (Tools::isSubmit($configuration->name . '_controller') ||
                (Tools::isSubmit($configuration->name . '_controller'))) {
                $controller_to_call = Tools::getValue($configuration->name . '_controller');
            }
            if (Tools::isSubmit($configuration->name . '_action') ||
                (Tools::isSubmit($configuration->name . '_controller'))) {
                $action_to_call = Tools::getValue($configuration->name . '_action');
            }
        } else {
            Tools::redirectAdmin(LinevenApoTools::getBackofficeURI());
        }
        $controller = LinevenApoController::getController(
            $controller_to_call,
            LinevenApoController::$controller_type_module
        );
        if (count($params)) {
            $controller->params = $params;
        }
        return $controller->run($action_to_call);
    }
    
    /**
     * Install.
     * @see ModuleCore::install()
     * @return boolean
     */
    public function install()
    {
        // Installation service
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->module_context->setApplication(LinevenApoContext::$application_backoffice);
        $configuration->getModule()->init(true);
        if (!parent::install()) {
            return false;
        }

        // Clear cache
        LinevenApoCache::clearCache();

        // Data tables creation
        $db = new LinevenApoDbTools();
        if (!$db->install()) {
            return false;
        }

        // Initialize datas values
        if (!$this->installConfigurationDatas()) {
            return false;
        }
        // Initialize hooks
        if (!$this->installHooks(0)) {
            return false;
        }
        Configuration::updateGlobalValue($configuration->getPrefixConfigurationModule().'NOTIFICATION_INST', 1);
        return true;
    }

    /**
     * Uninstall.
     * @see ModuleCore::uninstall()
     * @return boolean
     */
    public function uninstall()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->module_context->setApplication(LinevenApoContext::$application_backoffice);
        $configuration->getModule()->init(true);
        // Delete Admin Tab
        if ($configuration->getParameter('is_backoffice_menu_available')) {
            LinevenApoTabs::manage(false, true);
        }

        // Data tables delation
        $db = new LinevenApoDbTools();
        if (!$db->uninstall()) {
            return false;
        }

        // Delete datas values
        if (!$this->uninstallConfigurationDatas()) {
            return false;
        }

        if (!parent::uninstall()) {
            return false;
        }
        return true;
    }
    
    /**
     * Get Table.
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Get identifier.
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Get cache id.
     * @param string $cache_name
     *
     * @return string
     */
    public function getSmartyCacheId($cache_name = null)
    {
        if (method_exists('Module', 'getCacheId')) {
            return $this->getCacheId($cache_name);
        }
        return null;
    }

    /**
     * Clear cache.
     *
     * @return string
     */
    public function clearSmartyCache($template, $cache_id = null)
    {
        $this->_clearCache(
            $template,
            $cache_id
        );
    }

    /**
     * Set media for backoffice.
     *
     * @return void
     */
    public static function setBackofficeMedia()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();

        //  CSS
        Context::getContext()->controller->addCSS(
            LinevenApoTools::getBaseURI().'views/css/vendor/font-awesome-'.
            constant('_LINEVEN_MODULE_'.$configuration->getCode().'_FONTAWESOME_VERSION_').
            '/css/font-awesome.min.css'
        );
        Context::getContext()->controller->addCSS(
            LinevenApoTools::getBaseURI().'views/css/admin/'.Tools::strtolower($configuration->getCode()).'-lineven.css'
        );
        Context::getContext()->controller->addCSS(
            LinevenApoTools::getBaseURI().'views/css/admin/'.Tools::strtolower($configuration->getCode()).'-module.css'
        );
    
        // Add JS
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/admin/'.Tools::strtolower($configuration->getCode()).'-lineven.js'
        );
        Context::getContext()->controller->addJS(
            LinevenApoTools::getBaseURI().'views/js/admin/'.Tools::strtolower($configuration->getCode()).'-module.js'
        );
        Context::getContext()->controller->addJS(array(
            _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_.'admin/tinymce.inc.js',
        ));
    
        // Add JQuery UI
        Context::getContext()->controller->addJqueryUI(array(
            'ui.core',
            'ui.widget'
        ));
    
        // Add JQuery Plugin
        Context::getContext()->controller->addjQueryPlugin(array(
            'autocomplete',
            'tablednd',
            'ajaxfileupload',
            'date',
            'tagify',
            'select2',
            'validate',
            'colorpicker',
            'chosen'
        ));
    
        Context::getContext()->controller->addJS(
            _PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.
            Context::getContext()->language->iso_code.'.js'
        );
        Context::getContext()->controller->addJS(
            _PS_JS_DIR_.'jquery/plugins/validate/localization/messages_'.
            Context::getContext()->language->iso_code.'.js'
        );
    }
    
    /**
     * Load datas definition.
     *
     * @return void
     */
    private function loadDatasDefinition()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $configuration->setDatasDefinition(
            require_once($configuration->getDirectoryPath().'/config/settings.inc.php')
        );
    }
    
    /**
     * Load hooks definition.
     *
     * @return void
     */
    private function loadHooksDefinition()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $configuration->setHooksDefinition(
            require_once($configuration->getDirectoryPath().'/config/hooks.inc.php')
        );
    }

    /**
     * Load navigations.
     *
     * @return void
     */
    private function loadNavigation()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        LinevenApoNavigation::init(
            require_once($configuration->getDirectoryPath().'/config/navigation.inc.php')
        );
    }

    /**
     * Install configuratio datas.
     *
     * @return boolean
     */
    public function installConfigurationDatas()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        foreach ($configuration->getCurrentDatasDefinition() as $name => $value) {
            if ($value['global']) {
                Configuration::updateGlobalValue($name, $value['default_value'], $value['html']);
            } else {
                Configuration::updateValue($name, $value['default_value'], $value['html']);
            }
        }
        return true;
    }

    /**
     * Install hooks.
     *
     * @param int $filter Filter
     * @return boolean
     */
    private function installHooks($filter = 0)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $result = true;
        if ($configuration->getCurrentHooksDefinition($filter)) {
            foreach ($configuration->getCurrentHooksDefinition($filter) as $name) {
                if (!$this->registerHook($name)) {
                    $result = false;
                }
            }
        }
        return $result;
    }

    /**
     * Repair hooks.
     *
     * @return boolean
     */
    public static function repairHooks()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        return $configuration->getModule()->installHooks(1);
    }

    /**
     * Delete configuration datas.
     *
     * @return boolean
     */
    private function uninstallConfigurationDatas()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $keys_name = array_keys($configuration->getDatasDefinition());
        foreach ($keys_name as $name) {
            Configuration::deleteByName($name);
        }
        return true;
    }

    /**
     * Enable module.
     *
     * @param boolean $in_prestashop If true, enable module in Prestashop module list
     * @param boolean $force_all If true, enable module for all shop
     * @return boolean
     */
    public function enableState($in_prestashop = false, $force_all = false)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $return = true;
        if ($in_prestashop) {
            $return = parent::enable($force_all);
        }
        Configuration::updateValue($configuration->getPrefixConfigurationModule().'ACTIVE', true);
        return $return;
    }

    /**
     * Disable module.
     *
     * @param boolean $in_prestashop If true, enable module in Prestashop module list
     * @param bool $force_all If true, disable module for all shop
     * @return boolean
     */
    public function disableState($in_prestashop = false, $force_all = false)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $return = true;
        if ($in_prestashop) {
            $return = parent::disable($force_all);
        }
        Configuration::updateValue($configuration->getPrefixConfigurationModule().'ACTIVE', false);
        return $return;
    }

    /**
     * Set hook state.
     * @param string $hook_name Hook name
     * @param boolean $enable Enable
     *
     * @return boolean
     */
    public function setHookEnable($hook_name, $enable = true)
    {
        $context = Context::getContext();
        $is_registred = $this->isRegisteredInHook($hook_name);
        // Only unhook if id shop not null
        if ($context->shop->getContextShopID() != null && $is_registred && !$enable) {
            $this->unregisterHook($hook_name, array($context->shop->getContextShopID()));
            $this->unregisterExceptions($hook_name, array($context->shop->getContextShopID()));
        }
        // Install
        if ($enable) {
            $this->registerHook($hook_name);
        }
    }

    /**
     * Toggle test mode.
     *
     * @return boolean
     */
    public function toggleTestMode()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $value = !$this->module_context->isTestModeActive();
        if ($value && $this->module_context->getTestModeListIps(false) == '') {
            $configuration->setTestModeListIps($_SERVER['REMOTE_ADDR']);
        }
        $configuration->setTestModeActive((int)$value);
        return true;
    }

    /**
     * Toggle debug mode.
     *
     * @return boolean
     */
    public function toggleDebugMode()
    {
        $module_context = LinevenApoConfiguration::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $value = !$module_context->isDebugModeActive();
        $configuration->setDebugModeActive((int)$value);
        return true;
    }
}
