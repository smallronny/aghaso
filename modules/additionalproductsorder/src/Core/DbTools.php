<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoDbTools
{
    private $is_install_files;
    private $tables_def;
    private $files;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->is_install_files = $configuration->getParameter('is_install_files');
        $this->tables_def = null;
        $this->files = null;
    }

    /**
     * Set is install files.
     * @param boolean $is_install_files Is install files
     */
    public function setInstallFiles($is_install_files)
    {
        $this->is_install_files = $is_install_files;
    }

    /**
     * Load database files.
     */
    private function loadDatabaseFiles()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($this->is_install_files) {
            // Install tables
            if ($this->tables_def == null &&
                file_exists(_PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/install/tables.inc.php')) {
                $this->tables_def = require($configuration->getDirectoryPath() . '/install/tables.inc.php');
            }
            if ($this->files == null &&
                $configuration->getParameter('install_files_list') !== false) {
                $this->files = $configuration->getParameter('install_files_list');
            }
        }
    }

    /**
     * Install tables.
     * @return boolean
     */
    public function install()
    {
        // Install tables
        if (!$this->installTables()) {
            return false;
        }
        // Execute other files
        if (!$this->executeSqlInstallFiles()) {
            return false;
        }
        return true;
    }

    /**
     * Install tables.
     * @return boolean
     */
    public function installTables()
    {
        // Load files
        $this->loadDatabaseFiles();

        // Install tables
        if ($this->is_install_files && $this->tables_def != null) {
            foreach ($this->tables_def as $table) {
                $sql_creation = 'CREATE TABLE IF NOT EXISTS `' .$table['name'].'` (';
                for ($i = 0; $i < count($table['fields']); $i++) {
                    $sql_creation .= ' `'.$table['fields'][$i]['name'].'` '.$table['fields'][$i]['def'];
                    if ($i != (count($table['fields']) - 1)) {
                        $sql_creation .= ', ';
                    }
                }
                if (isset($table['primary'])) {
                    $sql_creation .= ', PRIMARY KEY (`'.implode('`,`', $table['primary']).'`)';
                }
                $sql_creation .= ') ';
                if (isset($table['options'])) {
                    $sql_creation .= $table['options'];
                }
                $sql_creation .= '; ';
                $sql_creation = $this->replaceSqlVars($sql_creation);
                if (!Db::getInstance()->Execute(trim($sql_creation))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Execute sql files.
     * @return boolean
     */
    public function executeSqlInstallFiles()
    {
        // Load files
        $this->loadDatabaseFiles();

        // Execute other files
        if ($this->is_install_files && $this->files != null) {
            foreach ($this->files as $file) {
                if (!$this->executeSqlFile(_PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/install/'.$file)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Execute sql file.
     *
     * @param string $file File
     * @return boolean
     */
    public function executeSqlFile($file)
    {
        if (!file_exists($file)) {
            return (false);
        } else {
            if (! $sql = Tools::file_get_contents(
                $file
            )) {
                return (false);
            }
        }
        $sql = $this->replaceSqlVars($sql);
        $sql = preg_split('/;\s*[\r\n]+/', $sql);
        foreach ($sql as $query) {
            if (! Db::getInstance()->Execute(trim($query))) {
                return (false);
            }
        }
        return true;
    }

    /**
     * Check if table exists in database.
     *
     * @param string $table Table Optional (if null check all tables)
     * @return mixed
     */
    public function isTableExist($table = null)
    {
        if (!$table) {
            $tables_not_exists = array();
            // Load files
            $this->loadDatabaseFiles();
            if ($this->tables_def != null) {
                foreach ($this->tables_def as $table) {
                    $table = $this->replaceSqlVars($table['name']);
                    $result = Db::getInstance()->ExecuteS('SHOW TABLES IN `'._DB_NAME_.'` LIKE "'.$table. '"');
                    if (count($result) != 1) {
                        $tables_not_exists[] = $table;
                    }
                }
                return $tables_not_exists;
            }
        } else {
            $table = $this->replaceSqlVars($table);
            $result = Db::getInstance()->ExecuteS('SHOW TABLES IN `'._DB_NAME_.'` LIKE "'.$table. '"');
            if (count($result) != 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Is columns in database tables exists.
     *
     * @return boolean
     */
    public function isColumnsTablesExist()
    {
        $columns_not_exists = array();

        // Load files
        $this->loadDatabaseFiles();
        if ($this->tables_def != null) {
            foreach ($this->tables_def as $table) {
                if ($this->isTableExist($table['name'])) {
                    foreach ($table['fields'] as $field) {
                        $table_name = $this->replaceSqlVars($table['name']);
                        $sql = 'SHOW COLUMNS FROM `'.$table_name. '` LIKE "'.$field['name'].'"';
                        $result = Db::getInstance()->ExecuteS($sql);
                        if (count($result) != 1) {
                            $columns_not_exists[$table_name][] = $field['name'];
                        }
                    }
                }
            }
        }
        return $columns_not_exists;
    }

    /**
     * Install tables.
     * @return boolean
     */
    public function uninstall()
    {
        // Load files
        $this->loadDatabaseFiles();

        if ($this->is_install_files && $this->tables_def != null) {
            foreach ($this->tables_def as $table) {
                $sql_creation = 'DROP TABLE IF EXISTS `'.$table['name'].'`;';
                $sql_creation = $this->replaceSqlVars($sql_creation);
                if (!Db::getInstance()->Execute(trim($sql_creation))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Repair tables columns in database.
     *
     * @return boolean
     */
    public function repairTablesColumns()
    {
        // Load files
        $this->loadDatabaseFiles();
        if ($this->tables_def != null) {
            foreach ($this->tables_def as $table) {
                if ($this->isTableExist($table['name'])) {
                    $position = ' FIRST ';
                    $sql_alter = '';
                    $existing_fields = array();
                    foreach ($table['fields'] as $field) {
                        $table_name = $this->replaceSqlVars($table['name']);
                        $sql = 'SHOW COLUMNS FROM `'.$table_name. '` LIKE "'.$field['name'].'"';
                        $result = Db::getInstance()->ExecuteS($sql);
                        if (count($result) != 1) {
                            if ($sql_alter == '') {
                                $sql_alter = 'ALTER TABLE `'.$table_name.'` ';
                            } else {
                                $sql_alter .= ',';
                            }
                            // Alter table
                            $sql_alter .= ' ADD `'.$field['name'].'` '.$field['def'].$position;
                        } else {
                            $existing_fields[] = $field['name'];
                        }
                        $position = ' AFTER `'.$field['name'].'` ';
                    }
                    if ($sql_alter != '') {
                        $sql_drop_primaries = '';
                        if (isset($table['primary'])) {
                            $count_def_primary = count($table['primary']);
                            $count_exists_primary = count(array_intersect($existing_fields, $table['primary']));
                            if ($count_def_primary
                                && $count_exists_primary <> $count_def_primary && $count_exists_primary <> 0) {
                                $sql_drop_primaries = 'ALTER TABLE `'.$table_name.'`  DROP PRIMARY KEY;';
                            }
                            if ($count_def_primary && ($count_exists_primary == 0 || $sql_drop_primaries != '')) {
                                $sql_alter .= ', ADD PRIMARY KEY (`'.implode('`,`', $table['primary']).'`)';
                            }
                        }
                        if (!Db::getInstance()->Execute($sql_drop_primaries.$sql_alter)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Backup tables.
     * @return boolean
     */
    public function backupTables()
    {
        $sql = str_replace('PREFIX_', _DB_PREFIX_, _LINEVEN_MODULE_APO_DATABASE_PREFIX_);
        $results = Db::getInstance()->ExecuteS('SHOW TABLES IN `'._DB_NAME_.'` LIKE "'.$sql. '%"');
        if (is_array($results) && count($results)) {
            foreach ($results as $result) {
                foreach ($result as $table) {
                    if (Tools::strpos($table, '_ar_bck') === false) {
                        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'.$table.'_ar_bck`');
                        if (!Db::getInstance()->Execute('CREATE TABLE `'.$table.'_ar_bck` LIKE `'.$table.'`')) {
                            return false;
                        }
                        if (!Db::getInstance()->Execute('INSERT INTO `'.$table.'_ar_bck` SELECT * FROM `'.$table.'`')) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Get tables definitions.
     * @return boolean
     */
    public function getTablesDefinitions()
    {
        // Load files
        $this->loadDatabaseFiles();

        // Install tables
        if ($this->is_install_files && $this->tables_def != null) {
            return $this->tables_def;
        }
        return false;
    }

    /**
     * Replace sql vars.
     *
     * @param string $sql Sql
     * @return string
     */
    private function replaceSqlVars($sql)
    {
        $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
        $sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
        return $sql;
    }
}
