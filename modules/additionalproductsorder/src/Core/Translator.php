<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoTranslator
{
    /**
     * Get translation for a given module text
     *
     * Note: $specific parameter is mandatory for library files.
     * Otherwise, translation key will not match for Module library
     * when module is loaded with eval() Module::getModulesOnDisk()
     *
     * @param string $string String to translate
     * @param bool|string $specific filename to use in translation key
     * @param string|null $locale Give a context for the translation
     * @return string Translation
     */
    public function l($string, $specific = false, $locale = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        return $configuration->getModule()->l($string, $specific, $locale);
    }

    /**
     * Get displayed content from array
     *
     * @param array $translations Translations in array
     * @param string $default Default if not find for language
     * @return string Translation
     */
    public static function getTranslationFromArray($translation, $default = '', $is_html = false)
    {
        $module_context = LinevenApoContext::getContext();
        if (!isset($translation[$module_context->current_id_lang])
            || (isset($translation[$module_context->current_id_lang])
                && $translation[$module_context->current_id_lang] == '')) {
            if (isset($translation[$module_context->default_id_lang])) {
                if (!$is_html) {
                    return $translation[$module_context->default_id_lang];
                }
                return html_entity_decode($translation[$module_context->default_id_lang]);
            } else {
                return $default;
            }
        } else {
            if (!$is_html) {
                return $translation[$module_context->current_id_lang];
            }
            return html_entity_decode($translation[$module_context->current_id_lang]);
        }
    }

    /**
     * Set default translations in array
     *
     * @param array $defaults_translations Default translations
     * @param string $default_iso_language Default iso language
     * @return array
     */
    public static function setDefaultTranslationsInArray($defaults_translations, $default_iso_language = 'en')
    {
        $translations = array();
        foreach (Language::getLanguages(false) as $language) {
            if (isset($defaults_translations[$language['iso_code']])) {
                $translations[(int)$language['id_lang']] = $defaults_translations[$language['iso_code']];
            } else {
                $translations[(int)$language['id_lang']] = $defaults_translations[$default_iso_language];
            }
        }
        return $translations;
    }

    /**
     * Get displayed content from Configuration
     *
     * @param string $configuration_name Configuration name
     * @param string $default Default if not find for language
     * @return string Translation
     */
    public static function getTranslationFromConfiguration($configuration_name, $default = '')
    {
        $module_context = LinevenApoContext::getContext();
        $translation = Configuration::get($configuration_name, $module_context->default_id_lang, null, null, $default);
        if (Configuration::get($configuration_name, $module_context->current_id_lang)) {
            $translation = Configuration::get($configuration_name, $module_context->current_id_lang);
        }
        return $translation;
    }
}
