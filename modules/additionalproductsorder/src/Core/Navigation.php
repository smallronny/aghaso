<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoNavigation
{
    private static $current_path = array();     /* Current path  */
    private static $datas = array();            /* Datas */
    private static $breadcrumbs = '';           /* Breadcrumbs */

    /**
     * Initialize navigation.
     * @param Array $datas
     * @return void
     */
    public static function init($datas = null)
    {
        self::$datas = $datas;
    }

    /**
     * Get tree.
     *
     * @return array
     */
    public static function getTree()
    {
        return self::$datas['tree'];
    }
    
    /**
     * Get options.
     * @param Array $options
     * @return array
     */
    public static function getOptions()
    {
        return self::$datas['options'];
    }
    
    /**
     * Set current path.
     * @param $string $navigation_path Navigation path
     * @return void
     */
    public static function setNavigationPath($navigation_pipe)
    {
        $current_path = array();
        // Active item
        if ($navigation_pipe != '') {
            $current_path = explode(':', $navigation_pipe);
            if (is_array($current_path)) {
                self::activeItem(self::$datas['tree'], $current_path);
            }
        } else {
            $default = self::$datas['options']['default'];
            self::$datas['tree'][$default]['active'] = 'true';
            $current_path = array($default);
        }
        self::$current_path = $current_path;
        // Construct breadcrumbs
        self::$breadcrumbs = self::constructBreadcrumbs(self::$datas['tree'], $current_path);
    }
    
    /**
     * Get item ny navigation.
     * @param $string $navigation_path Navigation path
     * @return array
     */
    public static function getItemByNavigationPath($navigation_pipe)
    {
        // Active item
        if ($navigation_pipe != '') {
            $navigation_pipe = explode(':', $navigation_pipe);
            if (is_array($navigation_pipe)) {
                $item = array();
                $item = self::$datas['tree'];
                for ($i=0; $i <= count($navigation_pipe); $i++) {
                    $item_key = $navigation_pipe[$i];
                    if (isset($item[$item_key])) {
                        if (isset($item[$item_key]['items']) && $i < count($navigation_pipe)-1) {
                            $item = $item[$item_key]['items'];
                        } else {
                            $item = $item[$item_key];
                            break;
                        }
                    }
                }
                return $item;
            }
        }
        return array();
    }
    
    /**
     * Active item in tree (recursive).
     *
     * @param array $datas Datas tree
     * @param array $current_path Current path
     * @param int $level Level
     * @return boolean
     */
    private static function activeItem(&$datas, $current_path, $level = 0)
    {
        foreach ($datas as $key => &$item) {
            foreach ($current_path as $current_item) {
                if ($key == $current_item) {
                    if (count($current_path) > 1 && (!isset($item['display_items']) || ($item['display_items']))) {
                        if (isset($item['items']) && $level < 1) {
                            array_shift($current_path);
                            self::activeItem($item['items'], $current_path, $level + 1);
                        } else {
                            $item['active'] = 'true';
                            return true;
                        }
                    } else {
                        $item['active'] = 'true';
                        return true;
                    }
                }
            }
        }
    }

    /**
     * Construct breadcrumbs.
     *
     * @param array $datas Datas tree
     * @param array $current_path Current path
     * @param int $level Level
     * @param array $additional_path Additional path (label, action)
     * @return string
     */
    private static function constructBreadcrumbs(
        $datas,
        $current_path,
        $level = 0,
        $navigation_path = '',
        $additional_path = null
    ) {
        $links = array();
        foreach ($datas as $key => $item) {
            foreach ($current_path as $current_item) {
                if ($key == $current_item) {
                    if ($navigation_path != '') {
                        $navigation_path .= ':';
                    }
                    $navigation_path .= $current_item;
                    $href = '#';
                    if (isset($item['controller'])) {
                        $href = LinevenApoTools::getBackofficeURI(
                            $item['controller'],
                            $item['action'],
                            $navigation_path
                        );
                        if (isset($item['params'])) {
                            $href .= $item['params'];
                        }
                    }
                    if (isset($item['items'])) {
                        array_shift($current_path);
                        if ($level == 1 && count($current_path) == 0) {
                            $links[] = array(
                                'class' => 'active',
                                'label' => $item['label']
                            );
                        } else {
                            $links[] = array(
                                'href' => $href,
                                'label' => $item['label']
                            );
                        }
                        $links = array_merge($links, self::constructBreadcrumbs(
                            $item['items'],
                            $current_path,
                            $level+1,
                            $navigation_path,
                            $additional_path
                        ));
                        break;
                    } else {
                        if ($additional_path == null) {
                            if ($level == 0) {
                                $links[] = array(
                                    'class' => 'active',
                                    'href' => $href,
                                    'label' => $item['label']
                                );
                            } else {
                                $links[] = array(
                                    'class' => 'active',
                                    'label' => $item['label']
                                );
                            }
                        } else {
                            $links[] = array(
                                'class' => 'active',
                                'label' => $item['label']
                            );
                            if ($additional_path != null && is_array($additional_path)) {
                                for ($i = 0; $i < count($additional_path); $i++) {
                                    if ($i == count($additional_path) - 1) {
                                        $links[] = array(
                                            'class' => 'active',
                                            'label' => $additional_path[$i]['label']
                                        );
                                    } else {
                                        $links[] = array(
                                            'class' => 'active',
                                            'href' => $href,
                                            'label' => $additional_path[$i]['label']
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $links;
    }
    
    /**
     * Get breadcrumbs.
     *
     * @return string
     */
    public static function getBreadcrumbs()
    {
        return self::$breadcrumbs;
    }
}
