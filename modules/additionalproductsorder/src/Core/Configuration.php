<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoConfiguration
{
    protected static $instance; /* Instanciated class */

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Version
     * @var string
     */
    public $version;

    protected $module;                    /* Module */
    protected $code;                      /* Code */
    protected $id_prestashop;             /* Id prestashop */
    protected $settings;                  /* Settings */

    protected $datas_definition = array();                            /* Prestashop datas definition */
    protected $hooks_definition = array();                            /* Prestashop hooks definition */

    /**
     * Get configuration.
     *
     * @return LinevenApoConfiguration
     */
    public static function getConfiguration()
    {
        if (!isset(self::$instance)) {
            self::$instance = new LinevenApoConfiguration();
        }
        return self::$instance;
    }

    /**
     * Setup.
     *
     * @param string $code Module code
     * @param string $id_prestashop Id Prestashop
     * @param object $module Module
     * @param array $settings Settings.
     *          is_cache_enable (boolean) : Is cache enable
     *          has_routes (boolean) : Has routes
     * @return void
     */
    public function setup($code, $id_prestashop, $module, $settings = array())
    {
        $this->code = Tools::strtoupper($code);
        $this->name = $module->name;
        $this->version = $module->version;
        $this->id_prestashop = $id_prestashop;
        $this->module = $module;
        $this->settings = $settings;
        if (!isset($this->settings['has_routes'])) {
            $this->settings['has_routes'] = false;
        }
        if (!isset($this->settings['is_cache_enable'])) {
            $this->settings['is_cache_enable'] = false;
        }
    }

    /**
     * Set backoffice settings.
     *
     * @param array $settings Settings.
     *          is_install_files (boolean) : Is install files
     *          install_files_list (array) : Install files list
     *          is_backoffice_menu_available (boolean) : Is access module is available from backoffice menu
     *          backoffice_default_controller (string) : Backoffice default controller
     *          backoffice_default_action (string) : Backoffice default action
     *          is_smarty_templates_changed (boolean) : If smarty templates changed
     *          is_demonstration_datas_available (boolean) : If demonstration datas are available
     *          demonstration_datas_class (string) : Class to call for demonstration data
     *          is_demonstration_message (boolean) : If a demonstration message need to be display
     *          is_demonstration_reset_available (boolean) : If reset demonstration datas is available
     * @return void
     */
    public function setBackofficeSettings($settings = null)
    {
        $module_context = LinevenApoContext::getContext();
        if ($settings != null) {
            $this->settings = array_merge($this->settings, $settings);
        }
        if (!isset($this->settings['is_install_files'])) {
            $this->settings['is_install_files'] = false;
        }
        if (!isset($this->settings['is_backoffice_menu_available'])) {
            $this->settings['is_backoffice_menu_available'] = false;
        }
        if (!isset($this->settings['backoffice_default_controller'])) {
            $this->settings['backoffice_default_controller'] = 'Dashboard';
        }
        if (!isset($this->settings['backoffice_default_action'])) {
            $this->settings['backoffice_default_action'] = 'index';
        }
        if (!isset($this->settings['is_smarty_templates_changed'])) {
            $this->settings['is_smarty_templates_changed'] = false;
        }
        if (!isset($this->settings['is_demonstration_datas_available'])) {
            $this->settings['is_demonstration_datas_available'] = false;
        }
        if (!isset($this->settings['is_demonstration_message'])) {
            $this->settings['is_demonstration_message'] = false;
        }
        if (!isset($this->settings['is_demonstration_reset_available'])) {
            $this->settings['is_demonstration_reset_available'] = false;
        }
        // Assign default datas
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'PREVIOUS_VERSION',
            null,
            true
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'EXTRA_UPGRADE',
            null,
            true
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'LAST_VERSION',
            $this->version,
            true
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'TESTMODE_ACTIVE',
            (int)$module_context->isTestModeActive()
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'TESTMODE_IP',
            $module_context->getTestModeListIps()
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'IS_DEBUG_MODE',
            (int)$module_context->isDebugModeActive()
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'ACTIVE_BO_MENU',
            (int)$this->isBackofficeMenuActive(),
            true
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'ACTIVE_CACHE',
            (int)$this->isCacheActive(),
            false
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'NOTIFICATION_INST',
            0,
            false
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'NOTIFICATION_UPD',
            0,
            false
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'PANEL_UPD',
            0,
            true
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'PANEL_INFOS',
            1,
            true
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'PANEL_WARNING',
            0,
            false
        );
        $this->addDataDefinition(
            $this->getPrefixConfigurationModule().'PANEL_DEMO_DATAS',
            ($this->settings['is_demonstration_datas_available'] ? 1 : 0),
            false
        );
    }

    /**
     * Get module.
     *
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Get module code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get id prestashop.
     *
     * @return string
     */
    public function getIdPrestashop()
    {
        return $this->id_prestashop;
    }

    /**
     * Get id prestashop.
     *
     * @return string
     */
    public function getPrestashopAddonsLink()
    {
        $module_context = LinevenApoContext::getContext();
        return 'http://addons.prestashop.com/'.
            $module_context->current_language['iso_code'].
            '/product.php?id_product='.$this->getIdPrestashop();
    }

    /**
     * Get module name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get parameters.
     *
     * @return mixed
     */
    public function getParameter($name)
    {
        if (isset($this->settings[$name])) {
            return $this->settings[$name];
        }
        return false;
    }

    /**
     * Get prefix configuration module.
     *
     * @return string
     */
    public function getPrefixConfigurationModule()
    {
        return 'LINEVEN_'.$this->code.'_';
    }

    /**
     * Add data definition.
     *
     * @param string $name Name
     * @param string $default_value Default value
     * @param boolean $global For multishops
     * @param array $allow_versions Only prestashop versions allowed
     * @param array $restricted_versions Prestashop versions forbidden
     * @param array $html Html
     * @return void
     */
    public function addDataDefinition(
        $name,
        $default_value = null,
        $global = false,
        $allow_versions = null,
        $restricted_versions = null,
        $html = false
    ) {
        $this->datas_definition[$name] = array(
            'name' => $name,
            'default_value' => $default_value,
            'global' => $global,
            'allow_versions' => $allow_versions,
            'restricted_versions' => $restricted_versions,
            'html' => $html
        );
    }

    /**
     * Set datas definition.
     *
     * @param array $datas
     * @return void
     */
    public function setDatasDefinition($datas)
    {
        if (is_array($datas) && count($datas)) {
            foreach ($datas as $data) {
                $this->addDataDefinition(
                    $data[0],
                    (isset($data[1]) ? $data[1] : null),
                    (isset($data[2]) ? $data[2] : false),
                    (isset($data[3]) ? $data[3] : null),
                    (isset($data[4]) ? $data[4] : null),
                    (isset($data[5]) ? $data[5] : false)
                );
            }
        }
    }
    
    /**
     * Assign hook definition.
     *
     * @param string $name Name
     * @param array $allow_versions Only prestashop versions allowed
     * @param array $restricted_versions Prestashop versions forbidden
     * @param array $options Options
     *      (boolean) available_for_install: default true
     *      (boolean) check_for_alerts: default true
     * @return void
     */
    public function addHookDefinition($name, $allow_versions = null, $restricted_versions = null, $options = null)
    {
        $this->hooks_definition[$name] = array(
            'name' => $name,
            'allow_versions' => $allow_versions,
            'restricted_versions' => $restricted_versions,
            'options' => $options
        );
    }

    /**
     * Set hooks descriptions.
     *
     * @param array $hooks
     * @return void
     */
    public function setHooksDefinition($hooks)
    {
        if (is_array($hooks) && count($hooks)) {
            foreach ($hooks as $hook) {
                $this->addHookDefinition(
                    $hook[0],
                    (isset($hook[1]) ? $hook[1] : null),
                    (isset($hook[2]) ? $hook[2] : null),
                    (isset($hook[3]) ? $hook[3] : null)
                );
            }
        }
    }

    /**
     * Get directory name.
     *
     * @return string
     */
    public function getDirName()
    {
        return (constant('_LINEVEN_MODULE_'.$this->code.'_DIRNAME_'));
    }

    /**
     * Get file name.
     *
     * @return string
     */
    public function getFileName()
    {
        return (constant('_LINEVEN_MODULE_'.$this->code.'_FILENAME_'));
    }

    /**
     * Get class name.
     *
     * @return string
     */
    public function getClassName()
    {
        return (constant('_LINEVEN_MODULE_'.$this->code.'_CLASSNAME_'));
    }

    /**
     * Get directory path.
     *
     * @return string
     */
    public function getDirectoryPath()
    {
        return (constant('_LINEVEN_MODULE_'.$this->code.'_REQUIRE_DIR_'));
    }

    /**
     * Get installed version.
     *
     * @return string
     */
    public function getInstalledVersion()
    {
        return ((Configuration::getGlobalValue($this->getPrefixConfigurationModule().'LAST_VERSION') !== false)
            ? Configuration::getGlobalValue($this->getPrefixConfigurationModule().'LAST_VERSION')
            : null);
    }

    /**
     * Set current version.
     *
     * @param string $version Version
     * @return void
     */
    public function setInstalledVersion($version)
    {
        Configuration::updateGlobalValue($this->getPrefixConfigurationModule().'LAST_VERSION', $version);
    }

    /**
     * Set test mode active.
     *
     * @param boolean $value Value
     * @return void
     */
    public function setTestModeActive($value)
    {
        Configuration::updateValue($this->getPrefixConfigurationModule().'TESTMODE_ACTIVE', $value);
    }

    /**
     * Set test mode list IPs.
     *
     * @param string $value Value
     * @return void
     */
    public function setTestModeListIps($value)
    {
        Configuration::updateValue($this->getPrefixConfigurationModule().'TESTMODE_IP', trim($value));
    }

    /**
     * Set debug mode active.
     *
     * @param boolean $value Value
     * @return void
     */
    public function setDebugModeActive($value)
    {
        Configuration::updateValue($this->getPrefixConfigurationModule().'IS_DEBUG_MODE', $value);
    }

    /**
     * Is backoffice menu active.
     *
     * @return boolean
     */
    public function isBackofficeMenuActive()
    {
        if ($this->settings['is_backoffice_menu_available']) {
            if (Configuration::get($this->getPrefixConfigurationModule().'ACTIVE_BO_MENU') === false) {
                return false;
            } else {
                return Configuration::get($this->getPrefixConfigurationModule().'ACTIVE_BO_MENU');
            }
        } else {
            return false;
        }
    }

    /**
     * Set backoffice menu active/inactive.
     *
     * @param boolean $value Value
     * @return void
     */
    public function setBackofficeMenuActive($value)
    {
        if ($this->settings['is_backoffice_menu_available']) {
            Configuration::updateGlobalValue($this->getPrefixConfigurationModule().'ACTIVE_BO_MENU', (int)$value);
        }
    }

    /**
     * Is cache active.
     *
     * @return boolean
     */
    public function isCacheActive()
    {
        if ($this->settings['is_cache_enable']) {
            if (Configuration::get($this->getPrefixConfigurationModule().'ACTIVE_CACHE') === false) {
                return false;
            } else {
                return Configuration::get($this->getPrefixConfigurationModule().'ACTIVE_CACHE');
            }
        } else {
            return false;
        }
    }

    /**
     * Set cache active.
     *
     * @param boolean $value Value
     * @return void
     */
    public function setCacheActive($value)
    {
        if ($this->settings['is_cache_enable']) {
            Configuration::updateValue($this->getPrefixConfigurationModule().'ACTIVE_CACHE', (int) $value);
        }
    }

    /**
     * Is smarty templates changed.
     *
     * @return boolean
     */
    public function isSmartyTemplatesChanged()
    {
        if ($this->settings['is_smarty_templates_changed']) {
            if (Configuration::get($this->getPrefixConfigurationModule().'PANEL_WARNING') === false) {
                return true;
            } else {
                return Configuration::get($this->getPrefixConfigurationModule().'PANEL_WARNING');
            }
        } else {
            return false;
        }
    }
    
    /**
     * Get Prestashop datas definition.
     *
     * @return array
     */
    public function getDatasDefinition()
    {
        return $this->datas_definition;
    }

    /**
     * Get Prestashop datas definition.
     *
     * @param string $key Key
     * @return array
     */
    public function getDataDefinition($key)
    {
        if (isset($this->datas_definition[$key])) {
            return $this->datas_definition[$key];
        }
        return array();
    }

    /**
     * Get current Prestashop datas definition used by shop (shop and globals).
     *
     * @return array
     */
    public function getCurrentDatasDefinition()
    {
        $datas = array();
        foreach ($this->getDatasDefinition() as $name => $value) {
            $allow = false;
            if ($value['allow_versions'] == null ||
                (is_array($value['allow_versions']) &&
                    (array_search(_PS_VERSION_, $value['allow_versions']) !== false ||
                        array_search(Tools::substr(_PS_VERSION_, 0, 3), $value['allow_versions']) !== false))) {
                $allow = true;
            }
            $restricted = false;
            if (is_array($value['restricted_versions']) &&
                (array_search(_PS_VERSION_, $value['restricted_versions']) !== false ||
                    array_search(Tools::substr(_PS_VERSION_, 0, 3), $value['restricted_versions']) !== false)) {
                $restricted = true;
            }
            if ($allow && !$restricted) {
                $datas[$name] = $value;
            }
        }
        return $datas;
    }

    /**
     * Get hooks definition.
     *
     * @return array
     */
    public function getHooksDefinition()
    {
        return $this->hooks_definition;
    }

    /**
     * Get current Prestashop hooks definition used by shop.
     *
     * @param int $filter (0: for installation, 1: for alerts, 2 : all)
     * @return array
     */
    public function getCurrentHooksDefinition($filter = 2)
    {
        $hooks = array();
        foreach ($this->getHooksDefinition() as $name => $value) {
            $allow = false;
            if ($value['allow_versions'] == null ||
                (is_array($value['allow_versions']) &&
                    (array_search(_PS_VERSION_, $value['allow_versions']) !== false ||
                        array_search(Tools::substr(_PS_VERSION_, 0, 3), $value['allow_versions']) !== false))) {
                $allow = true;
            }
            $restricted = false;
            if (is_array($value['restricted_versions']) &&
                (array_search(_PS_VERSION_, $value['restricted_versions']) !== false ||
                    array_search(Tools::substr(_PS_VERSION_, 0, 3), $value['restricted_versions']) !== false)) {
                $restricted = true;
            }
            if (($filter == 0 &&
                    isset($value['options']['available_for_install']) && !$value['options']['available_for_install'])
                || ($filter == 1
                    && isset($value['options']['check_for_alerts']) && !$value['options']['check_for_alerts'])) {
                $allow = false;
            }
            if ($allow && ! $restricted) {
                $hooks[] = $name;
            }
        }
        return $hooks;
    }

    /**
     * Get hooks.
     *
     * @return array
     */
    public function getHooks()
    {
        $array_hooks = array();
        foreach ($this->getHooksDefinition() as $name => $value) {
            $allow = false;
            if (is_array($value['allow_versions']) == null || (is_array($value['allow_versions']) &&
                (array_search(_PS_VERSION_, $value['allow_versions']) !== false ||
                    array_search(Tools::substr(_PS_VERSION_, 0, 3), $value['allow_versions']) !== false))) {
                $allow = true;
            }
            $restricted = false;
            if (is_array($value['restricted_versions']) &&
                (array_search(_PS_VERSION_, $value['restricted_versions']) !== false ||
                    array_search(Tools::substr(_PS_VERSION_, 0, 3), $value['restricted_versions']) !== false)) {
                $restricted = true;
            }
            if ($allow && ! $restricted) {
                $array_hooks[$name] = $value;
            }
        }
        return $array_hooks;
    }

    /**
     * Is hooks registred.
     * @param boolean $check_for_alerts If check for alerts
     *
     * @return Array of non registered hooks
     */
    public function isHooksRegistred($check_for_alerts = false)
    {
        $array_hooks = $this->getHooks();
        $items = array();
        $count_shop = 1;
        if (in_array('Shop', get_declared_classes())) {
            if (method_exists('Shop', 'getTotalShops')) {
                $count_shop = Shop::getTotalShops();
            }
        }
        if (method_exists('Module', 'isRegisteredInHook')) {
            foreach ($array_hooks as $hook => $hook_details) {
                $hook_name = $hook;
                if ($this->getModule()->isRegisteredInHook($hook_name) < $count_shop) {
                    $declare = true;
                    if (isset($hook_details['options']['alias'])) {
                        $declare = false;
                        foreach ($hook_details['options']['alias'] as $hook_alias) {
                            if ($this->getModule()->isRegisteredInHook($hook_alias) < $count_shop) {
                                $declare = true;
                                if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
                                    $hook_without_aliases = Hook::getModulesFromHook(
                                        Hook::getIdByName($hook_name, false),
                                        $this->getModule()->id
                                    );
                                    if (is_array($hook_without_aliases) && count($hook_without_aliases)) {
                                        $declare = false;
                                    }
                                }
                            } else {
                                $declare = false;
                                break;
                            }
                        }
                    }
                    if ($declare &&
                        (!$check_for_alerts ||
                            !isset($hook_details['options']['check_for_alerts']) ||
                            (isset($hook_details['options']['check_for_alerts'])
                                && $hook_details['options']['check_for_alerts'])
                    )) {
                        $items[] = $hook_name;
                    }
                }
            }
        }
        return $items;
    }

    /**
     * Is hook registred
     *
     * @return boolean
     */
    public function isHookRegistred($hook_name, $array_hook_alias = array())
    {
        $count_shop = 1;
        if (in_array('Shop', get_declared_classes())) {
            if (method_exists('Shop', 'getTotalShops')) {
                $count_shop = Shop::getTotalShops();
            }
        }
        if (method_exists('Module', 'isRegisteredInHook')) {
            if ($this->getModule()->isRegisteredInHook($hook_name) < $count_shop) {
                $exists = false;
                if (count($array_hook_alias) > 0) {
                    $exists = true;
                    foreach ($array_hook_alias as $hook_alias) {
                        if ($this->getModule()->isRegisteredInHook($hook_alias) < $count_shop) {
                            $exists = false;
                            if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
                                $hook_without_aliases = Hook::getModulesFromHook(
                                    Hook::getIdByName($hook_name, false),
                                    $this->getModule()->id
                                );
                                if (is_array($hook_without_aliases) && count($hook_without_aliases)) {
                                    $exists = true;
                                }
                            }
                        } else {
                            $exists = true;
                            break;
                        }
                    }
                }
                return $exists;
            } else {
                return true;
            }
        }
        return false;
    }
}
