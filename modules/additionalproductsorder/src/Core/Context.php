<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoContext
{
    private static $instance; /* Instanciated class */

    public $default_id_lang = null;             /* Default Lang id */
    public $default_language = null;            /* Default Language */
    public $current_id_lang = null;             /* Current Lang id */
    public $current_language = null;            /* Current Language */

    private $environment = 'PRODUCTION';         /* Environment */
    private $application = 'admin';              /* Application */
    private $access = 'module';                  /* Access mode */

    public static $environment_development = 'DEVELOPMENT';             /* Development */
    public static $environment_demonstration = 'DEMONSTRATION';         /* Demonstration */
    public static $environment_production = 'PRODUCTION';               /* Production */
    public static $application_frontoffice = 'front';                   /* Frontoffice */
    public static $application_backoffice = 'admin';                    /* Backoffice */
    public static $access_by_module_configuration = 'module';            /* Module */
    public static $access_by_admin_tab = 'tab';                  /* Tab */

    /**
     * Get a singleton instance of Context object
     *
     * @return Context
     */
    public static function getContext()
    {
        if (!isset(self::$instance)) {
            self::$instance = new LinevenApoContext();
            self::$instance->loadDefaults();
        }
        return self::$instance;
    }

    /**
     * Get environment.
     *
     * @param boolean $filter Real or filtered environment
     * @return string
     */
    public function getEnvironment($filter = true)
    {
        if ($filter) {
            if ($this->environment != self::$environment_demonstration) {
                return '';
            } else {
                return $this->environment.'.';
            }
        } else {
            return $this->environment;
        }
    }

    /**
     * Set environment.
     *
     * @param string $environment Environment
     * @return void
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    /**
     * Get application.
     *
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set application.
     *
     * @param string $application Application
     * @return void
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * Get access.
     *
     * @return string
     */
    public function getAccess()
    {
        return $this->access;
    }
    
    /**
     * Set access.
     *
     * @param string $access Access
     * @return void
     */
    public function setAccess($access)
    {
        $this->access = $access;
    }

    /**
     * Load defaults.
     *
     * @return void
     */
    private function loadDefaults()
    {
        // Load default language
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->default_language = Language::getLanguage((int)$this->default_id_lang);
        // Load current language
        $this->current_id_lang = (int)Context::getContext()->cookie->id_lang;
        $this->current_language = Language::getLanguage((int)$this->current_id_lang);
    }

    /**
     * Test if is active in production or for testing.
     *
     * @return boolean
     */
    public function isModuleActive()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $active = false;
        if ($configuration->version == Configuration::get($configuration->getPrefixConfigurationModule().'LAST_VERSION')
            && version_compare(
                _PS_VERSION_,
                $configuration->getModule()->ps_versions_compliancy['min'],
                '>='
            ) && ($configuration->getModule()->ps_versions_compliancy['min'] == ''
                || ($configuration->getModule()->ps_versions_compliancy['min'] != '' &&
                    version_compare(
                        _PS_VERSION_,
                        $configuration->getModule()->ps_versions_compliancy['min'],
                        '>='
                    )
            ))) {
            // If test mode is active
            if ($this->isTestModeActive() && $this->getTestModeListIps(false) != '') {
                $array_ips = $this->getTestModeListIps(true);
                if (in_array($_SERVER['REMOTE_ADDR'], $array_ips)) {
                    $active = true;
                }
            }
            if (! $this->isTestModeActive() && ! $active) {
                // If production mode is active.
                if (Configuration::get($configuration->getPrefixConfigurationModule().'ACTIVE')) {
                    $active = true;
                }
            }
        }
        return $active;
    }

    /**
     * Test if is active for testing.
     *
     * @return boolean
     */
    public function isModuleInTestModeActive()
    {
        $active = false;
        // If test mode is active
        if ($this->isTestModeActive() && $this->getTestModeListIps(false) != '') {
            $array_ips = $this->getTestModeListIps(true);
            if (in_array($_SERVER['REMOTE_ADDR'], $array_ips)) {
                $active = true;
            }
        }
        return $active;
    }

    /**
     * Is test mode active.
     *
     * @return boolean
     */
    public function isTestModeActive()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (Configuration::get($configuration->getPrefixConfigurationModule().'TESTMODE_ACTIVE') === false) {
            return false;
        } else {
            return Configuration::get($configuration->getPrefixConfigurationModule().'TESTMODE_ACTIVE');
        }
    }

    /**
     * Get test mode list IPs.
     *
     * @param boolean $in_array In an array
     * @return mixed
     */
    public function getTestModeListIps($in_array = false)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (Configuration::get($configuration->getPrefixConfigurationModule().'TESTMODE_IP') === false) {
            return '';
        } else {
            if ($in_array) {
                return explode(
                    ',',
                    trim(Configuration::get($configuration->getPrefixConfigurationModule().'TESTMODE_IP'))
                );
            } else {
                return trim(Configuration::get($configuration->getPrefixConfigurationModule().'TESTMODE_IP'));
            }
        }
    }

    /**
     * Is debug mode active.
     *
     * @return boolean
     */
    public function isDebugModeActive()
    {
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($module_context->getEnvironment() == LinevenApoContext::$environment_development
            || $module_context->isTestModeActive()) {
            return true;
        } else {
            if (Configuration::get($configuration->getPrefixConfigurationModule().'IS_DEBUG_MODE') === false) {
                return false;
            } else {
                return Configuration::get($configuration->getPrefixConfigurationModule().'IS_DEBUG_MODE');
            }
        }
    }
}
