<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoCache
{
    private static $templates = array();

    /**
     * Set templates.
     * @param array $templates Templates
     *
     * @return void
     */
    public static function setTemplates($templates = array())
    {
        self::$templates = $templates;
    }

    /**
     * Clear cache.
     * @param boolean $all All cache
     *
     * @return boolean
     */
    public static function clearCache($all = false)
    {
        $return = false;
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($configuration->isCacheActive() || count(self::$templates)) {
            if ($all) {
                $return = Tools::clearCache(Context::getContext()->smarty);
            } else {
                // Clear cache for manually templates
                // Clear cache for manually templates
                if (is_array(self::$templates) && count(self::$templates)) {
                    $view_path = 'views/templates/hook';
                    $path = array();
                    foreach (self::$templates as $templates) {
                        $path[] = _PS_MODULE_DIR_.$templates[0].'/'.$templates[1];
                        $path[] = _PS_MODULE_DIR_.$templates[0].'/'.$view_path.'/'.$templates[1];
                        $path[] = _PS_THEME_DIR_.'modules/'.$templates[0].'/'.$templates[1];
                        $path[] = _PS_THEME_DIR_.'modules/'.$templates[0].'/'.$view_path.'/'.$templates[1];
                        if (version_compare(_PS_VERSION_, '1.7.4', '>=')) {
                            $configuration->getModule()->clearSmartyCache(
                                'module:'.$templates[0].'/'.$view_path.'/'.$templates[1],
                                $templates[2]
                            );
                        }
                    }
                    for ($i = 0; $i < count($path); $i++) {
                        if (is_file($path[$i])) {
                            Tools::clearCache(null, $path[$i]);
                        }
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Get cache id.
     * @param string $cache_id Cache id
     *
     * @return string
     */
    public static function getCacheId($cache_id = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($configuration->isCacheActive()) {
            return $configuration->getModule()->getSmartyCacheId($cache_id);
        }
        return null;
    }

    /**
     * Is cached.
     * @param string $template Template
     * @param string $cache_id Cache Id
     *
     * @return boolean
     */
    public static function isCached($template, $cache_id = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($configuration->isCacheActive()) {
            return $configuration->getModule()->isCached($template, $cache_id);
        }
        return false;
    }
}
