<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoHelperList extends HelperList
{
    /**
     * Controller name
     * @var string
     */
    public $lineven_controller_name = '';
    
    /**
     * Action name
     * @var string
     */
    public $lineven_action_name = '';
    
    /**
     * Navigation
     * @var string
     */
    public $navigation = '';
    
    /**
     * Fields description.
     * @var array
     */
    public $fields_description = array();

    /**
     * Filtering search
     * @var string
     */
    public $filters = array();
    
    /**
     * Save filters
     * @var string
     */
    public $save_filters = true;
    
    /**
     * Disable default filters
     * @var string
     */
    public $disable_default_filters = false;
    
    /**
     * Reorder available
     * @var boolean
     */
    public $reorder_available = false;
    
    /**
     * Is bulk actions submited
     * @var boolean
     */
    public $is_bulk_actions_submited = false;
    
    /**
     * Bulk actions datas
     * @var boolean
     */
    public $bulk_actions_datas = null;

    /**
     * Bulk actions controllers
     * @var array
     */
    private $bulk_actions_controllers = array();

    /**
     * Actions controllers
     * @var array
     */
    private $actions_controllers = array();

    /**
     * Options
     * @var array
     */
    private $options = '';
    
    /**
     * Default Index
     * @var string
     */
    private $default_index = '';

    /**
     * Has filters
     * @var boolean
     */
    private $has_filters = false;
    
    /**
     * Constructor.
     * @param string $controller Controller
     * @param string $action Action
     * @param string $navigation Navigation
     * @param array $options Options (optional)
     *            fields_description (array) : Fields description.
     *            show_filters (boolean) : Show filters.
     *            table_id (string) : Table id.
     *            row_hover (boolean) : Row hover.
     *            bootstrap (boolean) : Bootstrap.
     *            simple_header (boolean) : Simple header.
     *            reorder_available (boolean) : Reorder available
     *            order_by (string) : Order by.
     *            order_way (string) : Order way.
     *            save_filters (boolean) : Save filters.
     *            disable_default_filters (boolean) : Disable default filters
     * @return void
     */
    public function __construct($controller = null, $action = null, $navigation = null, $options = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        parent::__construct();
        
        // Options
        $this->options = $options;
        $this->row_hover = true;
        $this->bootstrap = true;
        $this->simple_header = false;
        $this->orderBy = 'id';
        $this->orderWay = 'ASC';
        $this->save_filters = true;
        $this->disable_default_filters = false;
        if (isset($options['fields_description'])) {
            $this->fields_description = (int)$options['fields_description'];
        }
        if (isset($options['row_hover'])) {
            $this->row_hover = (int)$options['row_hover'];
        }
        if (isset($options['bootstrap'])) {
            $this->bootstrap = (int)$options['bootstrap'];
        }
        if (isset($options['simple_header'])) {
            $this->simple_header = (int)$options['simple_header'];
        }
        if (isset($options['order_by'])) {
            $this->orderBy = $options['order_by'];
        }
        if (isset($options['order_way'])) {
            $this->orderWay = Tools::strtoupper($options['order_way']);
        }
        if (isset($options['reorder_available'])) {
            $this->reorder_available = $options['reorder_available'];
        }
        if (isset($options['save_filters'])) {
            $this->save_filters = $options['save_filters'];
        }
        if (isset($options['disable_default_filters'])) {
            $this->disable_default_filters = $options['disable_default_filters'];
        }
        $this->module = $configuration->getModule();
        $this->title = '';
        $this->table = Tools::strtolower($configuration->getModule()->getTable());
        $this->identifier = $configuration->getModule()->getIdentifier();
        $this->bootstrap = true;
        $this->no_link = true;
        $this->shopLinkType = '';
        $this->actions = array();
        $this->listTotal = 0;
        $this->tpl_vars = array('show_filters' => false);
        $this->toolbar_btn = array();
        $this->token = LinevenApoTools::getAdminToken();
        $this->currentIndex = LinevenApoTools::getBackofficeURI($controller, $action, $navigation);
        $this->default_index = $this->currentIndex;
        $this->bulk_actions = false;
        $this->force_show_bulk_actions = false;

        // Specific Fields
        $this->lineven_controller_name = $controller;
        $this->lineven_action_name = $action;
        $this->navigation = $navigation;
        $this->fields_description = array();
    }

    /**
     * Prepare.
     * @param array $fields_description Fields description
     * @return void
     */
    public function prepareList($fields_description = null)
    {
        // Fields
        if ($fields_description != null) {
            $this->fields_description = $fields_description;
        }
        // Operations
        $this->initSort();
        $this->initSearch();
        
        // Bulk action delete
        if (Tools::isSubmit('submitBulkdelete'.$this->table) && Tools::isSubmit($this->list_id.'Box')) {
            $datas = Tools::getValue($this->list_id.'Box');
            $this->is_bulk_actions_submited = true;
            $this->bulk_actions_datas['delete'] = $datas;
            if (isset($this->bulk_actions_controllers['delete']) &&
                isset($this->bulk_actions_controllers['delete']['callback_object']) &&
                isset($this->bulk_actions_controllers['delete']['callback'])) {
                $class = $this->bulk_actions_controllers['delete']['callback_object'];
                $method = $this->bulk_actions_controllers['delete']['callback'];
                if (class_exists(get_class($class)) && method_exists($class, $method)) {
                    if (isset($this->bulk_actions_controllers['delete']['callback_params'])) {
                        $class->$method($this->bulk_actions_controllers['delete']['callback_params'], $datas);
                    } else {
                        $class->$method($datas);
                    }
                }
            }
        }
    }
    
    /**
     * Render list.
     * @param array $values Values Values
     * @param int $count (optional)
     * @param array $template_vars Template vars to passed to the helper template
     * @param array $fields_description Fields description
     *
     * @return object
     */
    public function renderList($values, $count = null, $template_vars = null, $fields_description = null)
    {
        // Templates options
        $this->tpl_vars['show_filters'] = true;
        $this->tpl_vars['show_filters_fixed'] = true;
        if (isset($this->options['show_filters'])) {
            $this->tpl_vars['show_filters'] = (int)$this->options['show_filters'];
            $this->tpl_vars['show_filters_fixed'] = (int)$this->options['show_filters'];
        }
        if (isset($this->options['table_id'])) {
            $this->tpl_vars['table_id'] = $this->options['table_id'];
            $this->tpl_vars['table_id_fixed'] = 'table-'.$this->options['table_id'];
        }
        if ($template_vars != null) {
            foreach ($template_vars as $key_data => $value_data) {
                $this->tpl_vars[$key_data] = $value_data;
            }
        }

        // Count
        if ($count == null) {
            $count = count($values);
        }
        // Bulk action
        $this->force_show_bulk_actions = false;
        if (count($this->bulk_actions_controllers) && $count) {
            $this->force_show_bulk_actions = true;
            $this->bulk_actions = $this->bulk_actions_controllers;
        }

        $this->listTotal = $count;
        // Fields
        if ($fields_description == null) {
            $fields_description = $this->fields_description;
        }
        // Render
        return parent::generateList($values, $fields_description);
    }

    /**
     * Has filters.
     * @return boolean
     */
    public function hasFilters()
    {
        return $this->has_filters;
    }
    
    /**
     * Get page.
     * @return int
     */
    public function getPage()
    {
        if (Tools::isSubmit('submitFilter'.$this->list_id)) {
            return (int)Tools::getValue('submitFilter'.$this->list_id);
        }
        return 1;
    }

    /**
     * Get pagination.
     * @return int
     */
    public function getPagination()
    {
        $pagination = $this->_default_pagination;
        if (in_array((int)Tools::getValue($this->list_id.'_pagination'), $this->_pagination)) {
            $pagination = (int)Tools::getValue($this->list_id.'_pagination');
        } elseif (isset($this->context->cookie->{$this->list_id.'_pagination'})
            && $this->context->cookie->{$this->list_id.'_pagination'}) {
            $pagination = $this->context->cookie->{$this->list_id.'_pagination'};
        }
        $this->context->cookie->{$this->list_id.'_pagination'} = $pagination;
        return $pagination;
    }

    /**
     * Inactive pagination.
     * @return void
     */
    public function inactivePagination()
    {
        $this->_pagination = array(100000);
    }
    
    /**
     * Add toolbar button.
     *
     * @param string $type Type
     * @param array $options Options
     * @return void
     */
    public function addToolbarButton($type, $options)
    {
        $this->toolbar_btn[$type] = $options;
    }

    /**
     * Add actions.
     *
     * @param string $type Type
     * @param string $action Action to call
     * @return void
     */
    public function addAction($type, $action)
    {
        if ($type != 'enable') {
            $this->actions[] = $type;
        }
        $this->actions_controllers[$type] = $action;
    }
    
    /**
     * Set actions.
     *
     * @param array $actions
     * @return void
     */
    public function setActions($actions)
    {
        foreach ($actions as $type => $action) {
            $this->addAction($type, $action);
        }
    }

    /**
     * Add bulk actions.
     *
     * @param string $type Type
     * @param string $action Action to call
     * @return void
     */
    public function addBulkAction($type, $action)
    {
        $this->bulk_actions_controllers[$type] = $action;
    }

    /**
     * Set bulk actions.
     *
     * @param array $actions
     * @return void
     */
    public function setBulkActions($actions)
    {
        foreach ($actions as $type => $action) {
            $this->addBulkAction($type, $action);
        }
    }

    /**
     * Get display settings.
     *
     * @return array
     */
    public function getDisplaySettings()
    {
        return array(
            'order_by' => $this->orderBy,
            'order_way' => $this->orderWay,
            'filters' => $this->filters,
            'page' => $this->getPage(),
            'pagination' => $this->getPagination()
        );
    }
    
    /**
     * Add sql filtering.
     *
     * @param array $settings Settings
     * @param boolean $add_limit Add limit
     * @param array $where Where clause
     * @param array $having Having clause
     * @param string $group_by Group by clause
     * @param string $order_by Add order by
     * @return string
     */
    public static function addSqlFiltering(
        $settings,
        $add_limit = true,
        $where = null,
        $having = null,
        $group_by = null,
        $order_by = true
    ) {
        $sql_where = self::getSqlWhereClause($settings, $where);
        $sql_having = '';
        $sql_order = '';
        $limit = '';
        if (isset($settings['filters']['having'])
            && is_array($settings['filters']['having']) && count(($settings['filters']['having']))) {
            $sql_having = trim(implode(' AND ', $settings['filters']['having']));
        }
        if ($having != null && is_array($having) && count($having)) {
            $having = implode(' AND ', $having);
        }
        if ($sql_having != '' && $having != '') {
            $having = ' AND '.$having;
        }
        if ($sql_where != '' || $where != '') {
            $sql_where = ' WHERE '.$sql_where;
        }
        if ($sql_having != '' || $having != '') {
            $sql_having = ' HAVING '.$sql_having;
        }
        if ($group_by != null) {
            $group_by = ' GROUP BY '.$group_by;
        }
        
        if ($add_limit) {
            $limit = self::getSqlLimit($settings);
            $limit = ' LIMIT '.(int)$limit['offset'].', '.(int)(int)$limit['limit'];
        }

        if ($order_by) {
            $sql_order_array = array();
            if (isset($settings['order_by']) && isset($settings['order_way']) &&
                $settings['order_by'] != '' && $settings['order_way'] != '') {
                $sql_order_array[] = pSQL($settings['order_by']).' '.pSQL($settings['order_way']);
            }
            if (isset($settings['additional_order_by'])) {
                foreach ($settings['additional_order_by'] as $additional_order_by_details) {
                    $sql_order_array[] = pSQL($additional_order_by_details);
                }
            }
            if (count($sql_order_array)) {
                $sql_order = ' ORDER BY '.implode(',', $sql_order_array).' ';
            }
        }
        return $sql_where.$group_by.$sql_having.$having.$sql_order.$limit;
    }

    /**
     * Get sql where clause.
     *
     * @param array $settings Settings
     * @param array $where Where clause
     * @return string
     */
    public static function getSqlWhereClause($settings, $where = null)
    {
        $sql_where = '';
        if (isset($settings['filters']['where'])
            && is_array($settings['filters']['where']) && count(($settings['filters']['where']))) {
            $sql_where = trim(implode(' AND ', $settings['filters']['where']));
        }
        if ($where != null && is_array($where) && count($where)) {
            $where = implode(' AND ', $where);
        }
        if ($sql_where != '' && $where != '') {
            $where = ' AND '.$where;
        }
        return $sql_where.$where;
    }

    /**
     * Get sql limit.
     *
     * @param array $settings Settings
     * @return array(offset, limit)
     */
    public static function getSqlLimit($settings)
    {
        $page_number = (int)$settings['page'];
        $number_per_page = (int)$settings['pagination'];
        if ($page_number <= 0) {
            $page_number = 1;
        }
        if ($number_per_page < 1) {
            $number_per_page = 10;
        }
        return array(
            'offset' => (int)(($page_number-1)* $number_per_page),
            'limit' => (int)$number_per_page
        );
    }
    
    /**
     * Is reorder authorized.
     *
     * @return boolean
     */
    public function isReorderAuthorized()
    {
        $authorized_order_because_shop = true;
        $authorized_order_because_position = true;
        $authorized_order_because_filters = true;
        $reason_if_not = '';
        if ($this->reorder_available) {
            if (!((Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') === false)
                || (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') !== false
                    && ((Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1
                        && (Context::getContext()->shop->getContextShopGroupID() == null
                            || (Context::getContext()->shop->getContextShopGroupID() != null
                                    && Shop::getTotalShops() <= 1)))
                        || Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 0)))) {
                $authorized_order_because_shop = false;
                $reason_if_not = 'SHOP';
            }
            if (!($this->orderBy == $this->options['order_by']
                && Tools::strtolower($this->orderWay) == Tools::strtolower($this->options['order_way']))) {
                $authorized_order_because_position = false;
                $reason_if_not = 'ORDER';
            }
            if ($this->hasFilters()) {
                $authorized_order_because_position = false;
                $reason_if_not = 'FILTERS';
            }
        }
        if ($authorized_order_because_shop && $authorized_order_because_position && $authorized_order_because_filters) {
            return true;
        } else {
            return $reason_if_not;
        }
        return false;
    }
    
    /**
     * Initialization sort.
     * @return void
     */
    private function initSort()
    {
        // Sorting
        $sort_by = $this->identifier;
        $sort_way = 'ASC';
        if ($this->orderBy != '') {
            $sort_by = $this->orderBy;
        }
        if ($this->orderWay != '') {
            $sort_way = $this->orderWay;
        }
        if (!isset($this->options['show_filters']) ||
            (isset($this->options['show_filters']) && $this->options['show_filters'] == true)) {
            if (Tools::isSubmit($this->list_id.'Orderby')) {
                $sort_by = Tools::getValue($this->list_id.'Orderby');
                Context::getContext()->cookie->{$this->list_id.'Orderby'} = $sort_by;
            } else {
                if (Context::getContext()->cookie->{$this->list_id.'Orderby'}) {
                    $sort_by = Context::getContext()->cookie->{$this->list_id.'Orderby'};
                }
            }
            if (Tools::isSubmit($this->list_id.'Orderway')) {
                $sort_way = Tools::getValue($this->list_id.'Orderway');
                Context::getContext()->cookie->{$this->list_id.'Orderway'} = $sort_way;
            } else {
                if (Context::getContext()->cookie->{$this->list_id.'Orderway'}) {
                    $sort_way = Context::getContext()->cookie->{$this->list_id.'Orderway'};
                }
            }
        }
        $this->position_identifier = $sort_by;
        $this->orderBy = $sort_by;
        $this->orderWay =  Tools::strtoupper($sort_way);
    }

    /**
     * Init search.
     * @return void
     */
    private function initSearch()
    {
        $helper_filters = array();
        $filters = array();
        $where_clause = array();
        $having_clause = array();
        $this->has_filters = false;

        if (Tools::isSubmit('submitReset'.$this->list_id)) {
            $helper_filters = array();
            foreach ($this->fields_description as $field_key => $field) {
                $this->fields_description[$field_key]['value'] = '';
                Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key} = '';
                unset($_POST[$this->list_id.'Filter_'.$field_key]);
            }
        } else {
            if (Tools::isSubmit('submitFilter'.$this->list_id) && Tools::getValue('submitFilter'.$this->list_id) != 0) {
                $helper_filters = $_POST;
            } else {
                foreach ($this->fields_description as $field_key => $field) {
                    $value = Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key};
                    if ($field['type'] == 'date' || $field['type'] == 'datetime') {
                        if (version_compare(_PS_VERSION_, '1.7.3', '>=')) {
                            $value = json_decode($value);
                        } else {
                            $value = unserialize($value);
                        }
                        if (!$this->disable_default_filters
                            &&($value == null || !count($value))
                            && isset($field['default_filter']) && is_array($field['default_filter'])) {
                            $value = $field['default_filter'];
                            Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key} = serialize($value);
                        }
                    } else {
                        if (!$value && !$this->disable_default_filters && isset($field['default_filter'])
                            && $field['default_filter'] != '') {
                            $value = $field['default_filter'];
                            Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key} = $value;
                        }
                    }
                    $helper_filters[$this->list_id.'Filter_'.$field_key] = $value;
                    $this->fields_description[$field_key]['value'] = $value;
                }
            }
            if (is_array($helper_filters) && count($helper_filters)) {
                foreach ($this->fields_description as $field_key => $field) {
                    $sql = '';
                    $reinit_field = false;
                    $table_alias = (isset($field['table_alias']) ? ' '.$field['table_alias'].'.' : '');
                    if (isset($helper_filters[$this->list_id.'Filter_'.$field_key])) {
                        if (!is_array($helper_filters[$this->list_id.'Filter_'.$field_key]) &&
                            Tools::strlen(trim($helper_filters[$this->list_id.'Filter_'.$field_key])) != 0) {
                            $partial_sql = '';
                            if ($field['type'] == 'text' ||
                                (isset($field['search_operator']) && $field['search_operator'] == 'like')) {
                                $partial_sql = $table_alias.'`'.$field_key.'` LIKE \'%'.
                                    pSQL($helper_filters[$this->list_id.'Filter_'.$field_key]).'%\'';
                            } else {
                                $partial_sql = $table_alias.'`'.$field_key.'` = \''.
                                    pSQL($helper_filters[$this->list_id.'Filter_'.$field_key]).'\'';
                            }
                            if (isset($field['search_where_or'])) {
                                if (is_array($field['search_where_or'])) {
                                    $partial_sql = '('.$partial_sql.' OR '.implode(' OR ', $field['search_where_or']).') ';
                                } else {
                                    $partial_sql = '('.$partial_sql.' OR '.$field['search_where_or'].') ';
                                }
                            }
                            $sql .= $partial_sql;
                            $filters[$field_key] = $helper_filters[$this->list_id.'Filter_'.$field_key];
                            if ($this->save_filters) {
                                Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key} =
                                    $helper_filters[$this->list_id.'Filter_'.$field_key];
                            }
                        } else {
                            if ($field['type'] == 'date' || $field['type'] == 'datetime') {
                                $date_filter = $helper_filters[$this->list_id.'Filter_'.$field_key];
                                if (Tools::strlen(trim($date_filter[0])) != ''
                                    || Tools::strlen(trim($date_filter[1])) != '') {
                                    $begin_date = false;
                                    if (Tools::strlen(trim($date_filter[0])) != '') {
                                        $begin_date = true;
                                        $sql .= $table_alias.'`'.$field_key.'` >= \''.pSQL(Tools::dateFrom($date_filter[0])).'\'';
                                    }
                                    if (Tools::strlen(trim($date_filter[1])) != '') {
                                        if ($begin_date == true) {
                                            $sql .= ' AND ';
                                        }
                                        $sql .= $table_alias.'`'.$field_key.'` < \''.
                                            pSQL(Tools::dateTo($date_filter[1])).'\'';
                                    }
                                    $filters[$field_key] = $helper_filters[$this->list_id.'Filter_'.$field_key];
                                    if ($this->save_filters) {
                                        if (version_compare(_PS_VERSION_, '1.7.3', '>=')) {
                                            Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key} =
                                                json_encode($helper_filters[$this->list_id.'Filter_'.$field_key]);
                                        } else {
                                            Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key} =
                                                serialize($helper_filters[$this->list_id.'Filter_'.$field_key]);
                                        }
                                    }
                                } else {
                                    $reinit_field = true;
                                }
                            } else {
                                $reinit_field = true;
                            }
                        }
                        if ($reinit_field) {
                            $this->fields_description[$field_key]['value'] = '';
                            Context::getContext()->cookie->{$this->list_id.'Filter_'.$field_key} = '';
                            unset($_POST[$this->list_id.'Filter_'.$field_key]);
                        }
                    }
                    if ($sql != '') {
                        $this->has_filters = true;
                        if (!isset($field['filter_clause']) ||
                            (isset($field['filter_clause']) && $field['filter_clause'] == 'where')) {
                            $where_clause[] = $sql;
                        } else {
                            if (isset($field['filter_clause']) && $field['filter_clause'] == 'having') {
                                $having_clause[] = $sql;
                            }
                        }
                    }
                }
            }
        }
        $this->filters['helper_filters'] = $helper_filters;
        $this->filters['filters'] = $filters;
        $this->filters['where'] = $where_clause;
        $this->filters['having'] = $having_clause;
        $this->filters['sql_where'] = implode(' AND ', $where_clause);
        $this->filters['sql_having'] = implode(' AND ', $having_clause);
    }
    
    /**
     * Display edit action link.
     *
     * @param string $token Token
     * @param int $id Id
     * @param string $name Name
     * @return string
     */
    public function displayEditLink($token, $id, $name = null)
    {
        if (isset($this->actions_controllers['edit'])) {
            $this->currentIndex = $this->actions_controllers['edit'];
        }
        $html = parent::displayEditLink($token, $id, $name);
        $this->currentIndex = $this->default_index;
        return $html;
    }
    
    /**
     * Display delete action link.
     *
     * @param string $token Token
     * @param int $id Id
     * @param string $name Name
     * @return string
     */
    public function displayDeleteLink($token = null, $id = null, $name = null)
    {
        if (isset($this->actions_controllers['delete'])) {
            $this->currentIndex = $this->actions_controllers['delete'];
        }
        $html = parent::displayDeleteLink($token, $id, $name);
        $this->currentIndex = $this->default_index;
        return $html;
    }

    /**
     * Fetch the template for action enable
     *
     * @param string $token
     * @param string $id
     * @param int $value state enabled or not
     * @param string $active status
     * @param int $id_category
     * @param int $id_product
     * @return string
     */
    public function displayEnableLink(
        $token,
        $id,
        $value,
        $active,
        $id_category = null,
        $id_product = null,
        $ajax = false
    ) {
        if (isset($this->actions_controllers['enable'])) {
            $this->currentIndex = $this->actions_controllers['enable'];
        }
        $html = parent::displayEnableLink($token, $id, $value, $active, $id_category, $id_product, $ajax);
        $this->currentIndex = $this->default_index;
        return $html;
    }
    
    /**
     * Display action show details of a table row
     * This action need an ajax request with a return like this:
     *   {
     *     use_parent_structure: true // If false, data need to be an html
     *     data:
     *       [
     *         {field_name: 'value'}
     *       ],
     *     fields_display: // attribute $fields_list of the admin controller
     *   }
     * or somethins like this:
     *   {
     *     use_parent_structure: false // If false, data need to be an html
     *     data:
     *       '<p>My html content</p>',
     *     fields_display: // attribute $fields_list of the admin controller
     *   }
     */
    public function displayDetailsLink($token = null, $id = null, $name = null)
    {
        if (isset($this->actions_controllers['details'])) {
            $this->currentIndex = $this->actions_controllers['details'];
        }
        $html = parent::displayDetailsLink($token, $id, $name);
        $this->currentIndex = $this->default_index;
        return $html;
    }

    /**
     * Display duplicate action link
     * @param string $token Token
     * @param int $id Id
     * @param string $name Name
     * @return string
     */
    public function displayDuplicateLink($token = null, $id = null, $name = null)
    {
        if (isset($this->actions_controllers['duplicate'])) {
            $this->currentIndex = $this->actions_controllers['duplicate'];
        }
        $html = parent::displayDuplicateLink($token, $id, $name);
        $this->currentIndex = $this->default_index;
        return $html;
    }

    /**
     * Display preview action link
     */
    public function displayPreviewLink($token = null, $id = null, $name = null)
    {
        $translator = new LinevenApoTranslator();
        $tpl = $this->createTemplate('list_action_preview.tpl');
        $tpl->assign(array(
            'href' => $this->actions_controllers['preview'].$id,
            'action' => $translator->l('Preview', 'HelperList')
        ));
    
        return $tpl->fetch();
    }

    /**
     * Display approve action link
     * @param string $token Token
     * @param int $id Id
     * @param string $name Name
     * @return string
     */
    public function displayApproveLink($token = null, $id = null, $name = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $translator = new LinevenApoTranslator();
        $tpl = $this->context->smarty->createTemplate(
            $configuration->getDirectoryPath().'/views/templates/admin/_configure/helpers/list/list_action_approve.tpl'
        );
        if (isset($this->actions_controllers['approve'])) {
            $this->currentIndex = $this->actions_controllers['approve'];
        }
        $href = $this->currentIndex.'&'.$this->identifier.'='.$id.
            '&update'.$this->table.($this->page && $this->page > 1 ? '&page='.(int)$this->page : '').
            '&token='.($token != null ? $token : $this->token);
        $tpl->assign(array(
            'href' => $href,
            'action' => $translator->l('Approve', 'HelperList'),
        ));
        return $tpl->fetch();
    }
    
    /**
     * Display refuse action link
     * @param string $token Token
     * @param int $id Id
     * @param string $name Name
     * @return string
     */
    public function displayRefuseLink($token = null, $id = null, $name = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $translator = new LinevenApoTranslator();
        $tpl = $this->context->smarty->createTemplate(
            $configuration->getDirectoryPath().'/views/templates/admin/_configure/helpers/list/list_action_refuse.tpl'
        );
        if (isset($this->actions_controllers['refuse'])) {
            $this->currentIndex = $this->actions_controllers['refuse'];
        }
        $href = $this->currentIndex.'&'.$this->identifier.'='.$id.
            '&update'.$this->table.($this->page && $this->page > 1 ? '&page='.(int)$this->page : '').
            '&token='.($token != null ? $token : $this->token);
        $tpl->assign(array(
            'href' => $href,
            'id' => $id,
            'is_reason_of_refusal' => Configuration::get($configuration->getPrefixConfigurationModule().'ACTIVE_REASON_REF'),
            'action' => $translator->l('Refuse', 'HelperList'),
        ));
        return $tpl->fetch();
    }
    
    /**
     * Display reminder action link
     * @param string $token Token
     * @param int $id Id
     * @param string $name Name
     * @return string
     */
    public function displayReminderLink($token = null, $id = null, $name = null)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $translator = new LinevenApoTranslator();
        $tpl = $this->context->smarty->createTemplate(
            $configuration->getDirectoryPath().'/views/templates/admin/_configure/helpers/list/list_action_reminder.tpl'
        );
        if (isset($this->actions_controllers['reminder'])) {
            $this->currentIndex = $this->actions_controllers['reminder'];
        }
        // Search report
        $key = array_search($id, LinevenApoTools::arrayColumn($this->_list, 'id_order'));
        $href = $this->currentIndex.'&'.$this->identifier.'='.$id.
            '&update'.$this->table.($this->page && $this->page > 1 ? '&page='.(int)$this->page : '').
            '&token='.($token != null ? $token : $this->token);
        $tpl->assign(array(
            'datas' => $this->_list[$key],
            'id' => $id,
            'href' => $href,
            'action' => $translator->l('Reminder', 'HelperList'),
        ));
        return $tpl->fetch();
    }
}
