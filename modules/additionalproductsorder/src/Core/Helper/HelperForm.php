<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

/**
 * @since 1.5.0
 */
class LinevenApoHelperForm extends HelperForm
{
    /**
     * Form
     * @var LinevenApoForm
     */
    public $form;
    
    /**
     * Constructor.
     * @param string $controller Controller
     * @param string $action Action
     * @param string $navigation Navigation
     * @return void
     */
    public function __construct($controller, $action, $navigation)
    {
        parent::__construct();
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->module = $configuration->getModule();
        $this->show_toolbar = false;
        $this->table =  $configuration->getModule()->getTable();
        $this->identifier = $configuration->getModule()->getIdentifier();
        $this->submit_action = 'lineven_submit_form';
        $this->currentIndex = LinevenApoTools::getBackofficeURI(
            $controller,
            $action,
            $navigation,
            false
        ).'&lineven_form=1';
        $this->languages = Language::getLanguages(false);
        $this->default_form_language = $module_context->current_id_lang;
        $this->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->token = LinevenApoTools::getAdminToken();
    }
    
    /**
     * Generate form.
     * @param array $fields_form Field form
     * @param array $template_vars Template vars to passed to the helper template
     * @return mixed
     */
    public function generateForm($fields_form = null, $template_vars = null)
    {
        $module_context = LinevenApoContext::getContext();
        if ($fields_form == null) {
            $fields_form = $this->form->getFields();
            $this->fields_value = $this->form->getFieldsValue();
        } else {
            $fields_form = $this->form->getFields($fields_form);
            $this->fields_value = $this->form->getFieldsValue();
        }
        if ($template_vars != null) {
            foreach ($template_vars as $key_data => $value_data) {
                $this->tpl_vars[$key_data] = $value_data;
            }
        }
        $this->tpl_vars['languages'] = Context::getContext()->controller->getLanguages(false);
        $this->tpl_vars['id_language'] = $module_context->current_id_lang;
        return parent::generateForm($fields_form);
    }
}
