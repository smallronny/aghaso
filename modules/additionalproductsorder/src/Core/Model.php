<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModel extends ObjectModel
{
    /**
     * Populate from form.
     *
     * @param Array $array Form value
     * @return void
     */
    public function populateFromFormValues($form_values)
    {
        if (is_array($form_values)) {
            foreach ($form_values as $key => $value) {
                if (property_exists(get_called_class(), $key)) {
                    $this->$key = $value;
                }
            }
        }
    }
}
