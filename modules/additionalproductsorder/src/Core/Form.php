<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoForm
{
    protected $fields = array();
    protected $fields_value = array();
    protected $elements = array();
    protected $errors = array();
    protected $translator;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->translator = new LinevenApoTranslator();
        $this->init();
        if (count($this->fields) && count($this->elements) == 0) {
            $submit_count = 1;
            foreach ($this->fields as $key => $field) {
                if (isset($field['form']['input'])) {
                    $this->setElements($field['form']['input']);
                }
                if (isset($this->fields[$key]['form']['submit'])) {
                    $this->fields[$key]['form']['submit']['name'] = 'lineven_submit_form_'.$submit_count;
                    $submit_count++;
                }
            }
        }
    }

    /**
     * Init.
     *
     * @return void
     */
    public function init()
    {
    }

    /**
     * Update settings
     *
     * @return void
     */
    public function updateSettings()
    {
        $elements = $this->getElements();
        $configuration = LinevenApoConfiguration::getConfiguration();
        foreach ($elements as $element) {
            if (isset($element['configuration_name']) && strpos($element['name'], 'active_bo_menu') === false) {
                $data_definition = $configuration->getDataDefinition($element['configuration_name']);
                $is_html = false;
                if (isset($data_definition['html']) && $data_definition['html']) {
                    $is_html = true;
                }
                Configuration::updateValue($element['configuration_name'], $element['value'], $is_html);
            }
        }
        return true;
    }

    /**
     * Add element.
     *
     * @param object $element
     *            Element to add
     * @return void
     */
    public function addElement($element)
    {
        $this->elements[$element['name']] = $element;
        if (isset($this->elements[$element['name']]['value'])) {
            $this->setValue($element['name'], $this->elements[$element['name']]['value']);
        }
    }
    
    /**
     * Set elements.
     *
     * @param array $elements Elements to add
     * @return void
     */
    public function setElements($elements)
    {
        foreach ($elements as $element) {
            $this->addElement($element);
        }
    }

    /**
     * Get element.
     *
     * @param string $name Element name
     * @return object
     */
    public function getElement($name)
    {
        if (isset($this->elements[$name])) {
            return $this->elements[$name];
        } else {
            return null;
        }
    }

    /**
     * Element exists.
     *
     * @param string $name Element name
     * @return boolean
     */
    public function elementExists($name)
    {
        if (isset($this->elements[$name])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get elements.
     *
     * @return array
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Get fields.
     *
     * @param array $field Specifics fieldset to return
     *
     * @return array
     */
    public function getFields($fields = null)
    {
        if (is_array($fields)) {
            $return = array();
            foreach ($fields as $field) {
                $return[$field] = $this->fields[$field];
            }
            return $return;
        }
        return $this->fields;
    }
    
    /**
     * Get fields value.
     *
     * @return array
     */
    public function getFieldsValue()
    {
        return $this->fields_value;
    }
    
    /**
     * Get errors.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    
    /**
     * Add error.
     *
     * @param string $message Message
     * @return void
     */
    public function addError($message)
    {
        if (is_array($message)) {
            $message = implode("\n", $message);
            $message = Tools::nl2br($message);
        }
        $this->errors[] = $message;
    }

    /**
     * Is errors.
     *
     * @return boolean
     */
    public function isErrors()
    {
        return ((count($this->errors) == 0) ? false : true);
    }

    /**
     * Validate.
     *
     * @return boolean
     */
    public function validate()
    {
        return !$this->isErrors();
    }

    /**
     * Is submit.
     *
     * @return boolean
     */
    public static function isSubmit()
    {
        return (Tools::getIsset('lineven_form') ? true : false);
    }

    /**
     * Set value.
     *
     * @param string $element_name Element name
     * @param string $value Value
     * @return boolean
     */
    public function setValue($element_name, $value)
    {
        $name = $element_name;
        if (isset($this->elements[$name])) {
            if ($this->elements[$element_name]['type'] == 'checkbox') {
                $name = $element_name.'_on';
            }
            if ($this->elements[$element_name]['type'] == 'group' &&
                is_array($value)) {
                $this->fields_value[$element_name] = $value;
                foreach ($this->elements[$element_name]['values'] as $group_value) {
                    $this->fields_value[$element_name.'_'.$group_value['id_group']] = in_array(
                        $group_value['val'],
                        $value,
                        true
                    );
                    $this->elements[$element_name]['value'] = $value;
                }
            } else {
                $element_value = $value;
                if (!is_array($value)) {
                    $element_value = Tools::stripslashes($value);
                }
                $this->fields_value[$name] = $element_value;
                if ($name != $element_name) {
                    $this->fields_value[$element_name] = $element_value;
                }
                $this->elements[$element_name]['value'] = $element_value;
            }
        }
    }

    /**
     * Set language value.
     *
     * @param string $element_name Element name
     * @param string $value Value
     * @param string $id_lang Id lang
     * @return boolean
     */
    public function setLanguageValue($element_name, $value, $id_lang)
    {
        $this->fields_value[$element_name][$id_lang] = Tools::stripslashes($value);
        $this->elements[$element_name]['value'][$id_lang] = Tools::stripslashes($value);
    }
    
    /**
     * Populate from request.
     *
     * @return void
     */
    public function populateFromRequest()
    {
        foreach ($this->elements as $element) {
            $name = $element['name'];
            if ($element['type'] == 'checkbox') {
                $name = $element['name'].'_on';
            }
            if (Tools::isSubmit($name)) {
                $this->setValue($element['name'], Tools::getValue($name));
            } else {
                if ($element['type'] == 'checkbox') {
                    $this->setValue($element['name'], 0);
                } else {
                    if ($element['type'] == 'group') {
                        $values = array();
                        if (Tools::isSubmit($element['name'])) {
                            $values = Tools::getValue($element['name']);
                        }
                        $this->setValue($element['name'], $values);
                    } else {
                        if (isset($element['lang']) && $element['lang']) {
                            $languages = Language::getLanguages(false);
                            foreach ($languages as $language) {
                                $submit_name = $name.'_'.$language['id_lang'];
                                if (Tools::isSubmit($submit_name)) {
                                    $this->setLanguageValue(
                                        $name,
                                        Tools::getValue($submit_name),
                                        $language['id_lang']
                                    );
                                } else {
                                    $this->setLanguageValue($name, '', $language['id_lang']);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this->fields_value;
    }

    /**
     * Populate from data value in configuration.
     *
     * @return void
     */
    public function populateFromConfigurationDatasValue()
    {
        foreach ($this->elements as $element) {
            if (isset($element['configuration_name']) &&  $element['configuration_name'] != '') {
                if (isset($element['lang']) && $element['lang']) {
                    $languages = Language::getLanguages(false);
                    foreach ($languages as $language) {
                        if (Configuration::get($element['configuration_name'], $language['id_lang']) !== false) {
                            $this->setLanguageValue(
                                $element['name'],
                                Configuration::get($element['configuration_name'], $language['id_lang']),
                                $language['id_lang']
                            );
                        } else {
                            $this->setLanguageValue($element['name'], '', $language['id_lang']);
                        }
                    }
                } else {
                    $this->setValue($element['name'], Configuration::get($element['configuration_name']));
                }
            }
        }
        return $this->fields_value;
    }
    
    /**
     * Populate.
     *
     * @param Object $object
     * @return void
     */
    public function populate($object)
    {
        foreach ($this->elements as $element) {
            if (property_exists(get_class($object), $element['name'])) {
                $property = $element['name'];
                if (isset($element['lang']) && $element['lang']) {
                    $languages = Language::getLanguages(false);
                    foreach ($languages as $language) {
                        $property_value = $object->$property;
                        if (isset($property_value[$language['id_lang']])) {
                            $this->setLanguageValue(
                                $element['name'],
                                $property_value[$language['id_lang']],
                                $language['id_lang']
                            );
                        } else {
                            $this->setLanguageValue($element['name'], '', $language['id_lang']);
                        }
                    }
                } else {
                    $this->setValue($element['name'], $object->$property);
                }
            }
        }
    }

    /**
     * Populate from array.
     *
     * @param Array $array
     * @return void
     */
    public function populateFromArray($array)
    {
        if (is_array($array)) {
            foreach ($this->elements as $element) {
                if (isset($array[$element['name']])) {
                    $this->setValue($element['name'], $array[$element['name']]);
                }
            }
        }
    }
}
