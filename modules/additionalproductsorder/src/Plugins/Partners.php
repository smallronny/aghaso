<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoPartners
{
    /**
     * Partners
     *
     * @var array
     */
    private static $partners;

    /**
     * Get partners.
     *
     * @return array
     */
    public static function getPartners()
    {
        return LinevenApoPartners::$partners;
    }

    /**
     * Get Partner.
     *
     * @param string $code Code
     * @return array
     */
    public static function getPartner($code)
    {
        return LinevenApoPartners::$partners[$code];
    }

    /**
     * Add partner.
     *
     * @param string $code
     *            Code
     * @param string $company
     *            Company
     * @param string $module_name
     *            Module name
     * @param string $module_label
     *            Module label
     * @param string $module_version_compatibility
     *            Module version compatibility
     * @param string $prestashop_version_compatibility
     *            Prestashop version compatibility
     * @param string $configuration_name (optional)
     *            Configuration name
     * @return void
     */
    public static function addPartner(
        $code,
        $company,
        $module_name,
        $module_label,
        $module_version_compatibility,
        $prestashop_version_compatibility,
        $configuration_name = null
    ) {
        $partner = new LinevenApoPartner();
        $partner->code = $code;
        $partner->company = $company;
        $partner->module_name = $module_name;
        $partner->module_label = $module_label;
        $partner->module_version_compatibility = $module_version_compatibility;
        $partner->prestashop_version_compatibility = $prestashop_version_compatibility;
        $partner->configuration_name = $configuration_name;
        LinevenApoPartners::$partners[$code] = $partner;
    }

    /**
     * Check if the module compatible.
     *
     * @param string $code Code
     * @return boolean
     */
    public static function verifyModule($code)
    {
        if ($code != '' && isset(self::$partners[$code]) && Module::isInstalled($code)) {
            $enabled = true;
            if (method_exists('Module', 'isEnabled')) {
                $enabled = Module::isEnabled($code);
            }
            if ($enabled) {
                $partner = self::$partners[$code];
                $module = Module::getInstanceByName($code);
                if (version_compare($module->version, $partner->module_version_compatibility, '>=') &&
                    version_compare($partner->prestashop_version_compatibility, _PS_VERSION_, '<=')) {
                    return true;
                }
            }
        }
        return false;
    }
}
