<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoTabs
{
    protected static $tabs = array();                        /* Backoffice tabs menu */

    /**
     * Set tabs.
     *
     * @param array $tabs Tabs
     *         classname_parent (string) : Parent classname
     *         classname (string) : Classname
     *         default_name (string) : Default name
     *         name (string) : Name
     * @return void
     */
    public static function setTabs($tabs)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($configuration->getParameter('is_backoffice_menu_available')) {
            self::$tabs = $tabs;
        }
    }

    /**
     * Add tab.
     *
     * @param array $tabs Tabs
     *         classname_parent (string) : Parent classname
     *         classname (string) : Classname
     *         default_name (string) : Default name
     *         name (string) : Name
     * @return void
     */
    public static function addTab($tab)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($configuration->getParameter('is_backoffice_menu_available')) {
            $key = $tab['classname'];
            if (isset($tab['key'])) {
                $key = $tab['key'];
            }
            self::$tabs[$key] = $tab;
        }
    }

    /**
     * Get tabs.
     *
     * @return array
     */
    public static function getTabs()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if ($configuration->getParameter('is_backoffice_menu_available')) {
            return self::$tabs;
        }
        return null;
    }

    /**
     * Manage.
     *
     * @param int $value Value
     * @param boolean $force_delete Force or not deletation tab
     * @return void
     */
    public static function manage($value, $force_delete = false)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $settings = self::getTabs();
        if ($settings != null && count($settings)) {
            foreach ($settings as $menu_settings) {
                if ($menu_settings['classname'] != null) {
                    if ($force_delete || (! $force_delete && Configuration::get(
                        $configuration->getPrefixConfigurationModule().'ACTIVE_BO_MENU'
                    ) != $value)) {
                        if ($force_delete || (!$force_delete && $value == 0)) {
                            // Delete Tab
                            if (($id_tab = Tab::getIdFromClassName($menu_settings['classname']))) {
                                $tab = new Tab($id_tab);
                                $tab->delete();
                                if (file_exists(_PS_IMG_DIR_.'/t/'.$menu_settings['classname'].'.gif')) {
                                    unlink(_PS_IMG_DIR_.'/t/'.$menu_settings['classname'].'.gif');
                                }
                            }
                        } else {
                            if (Tab::getIdFromClassName($menu_settings['classname']) == null) {
                                // Add Tab
                                $tab = new Tab();
                                $languages = Language::getLanguages(false);
                                foreach ($languages as $language) {
                                    $name = $menu_settings['name'];
                                    $size_name = 64;
                                    if (Tools::strlen($name) > $size_name) {
                                        $name = Tools::truncate($name, $size_name - 3).'...';
                                    }
                                    if ($name == '') {
                                        $name = $menu_settings['default_name'];
                                    }
                                    $tab->name[$language['id_lang']] = $name;
                                }
                                $tab->class_name = $menu_settings['classname'];
                                $tab->module = $configuration->getFileName();
                                $tab->id_parent = 0;
                                if (isset($menu_settings['classname_parent'])) {
                                    $tab->id_parent = Tab::getIdFromClassName($menu_settings['classname_parent']);
                                }
                                if (is_dir(_PS_MODULE_DIR_.$configuration->getDirName().'/') &&
                                    file_exists(_PS_MODULE_DIR_.$configuration->getDirName().
                                        '/views/img/'.$menu_settings['classname'].'.gif')) {
                                    copy(
                                        _PS_MODULE_DIR_.$configuration->getDirName().
                                            '/views/img/'.$menu_settings['classname'].'.gif',
                                        _PS_IMG_DIR_.'/t/'.$menu_settings['classname'].'.gif'
                                    );
                                }
                                $tab->add();
                            }
                        }
                    }
                }
            }
            $configuration->setBackofficeMenuActive($value);
        }
    }
}
