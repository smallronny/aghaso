<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoPartner
{
    /**
     * Code
     *
     * @var string
     */
    public $code;
    /**
     * Company
     *
     * @var string
     */
    public $company;
    /**
     * Module name
     *
     * @var string
     */
    public $module_name;
    /**
     * Module label
     *
     * @var string
     */
    public $module_label;
    /**
     * Module version compatibility
     *
     * @var string
     */
    public $module_version_compatibility;
    /**
     * Prestashop version compatibility
     *
     * @var string
     */
    public $prestashop_version_compatibility;
    /**
     * Configuration name
     *
     * @var string
     */
    public $configuration_name;
}
