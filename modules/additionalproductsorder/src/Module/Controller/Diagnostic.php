<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleDiagnosticController extends LinevenApoController
{
    protected $auto_repair = false;
    protected $has_output = false;
    protected $is_success = false;
    protected $results = null;
    protected $steps = array();
    protected $is_functional_diagnostic = false;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if (Tools::isSubmit('auto_repair') && (int)Tools::getValue('auto_repair')) {
            $this->auto_repair = (int)Tools::getValue('auto_repair');
        }
    }

    /**
     * Dashboard action.
     *
     * @return void
     */
    public function dashboardAction()
    {
        // Template
        $this->presenter->addData('is_functional_diagnostic', $this->is_functional_diagnostic);
        $this->presenter->addOutput(
            LinevenApoPresenter::$output_type_path_template,
            './_partials/diagnostic/dashboard'
        );
    }

    /**
     * Prepare checking action.
     *
     * @return void
     */
    public function prepareCheckingAction()
    {
        $this->presenter->noRender();
        if (Tools::isSubmit('step')) {
            $step = $this->getStep((int)Tools::getValue('step'));
            if ($step) {
                die(
                    json_encode(
                        $step['outputs']['checking']
                    )
                );
            }
        }
        die();
    }

    /**
     * Checking action.
     *
     * @return void
     */
    public function checkingAction()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->presenter->noRender();
        if (Tools::isSubmit('step')) {
            $step = $this->getStep((int)Tools::getValue('step'));
            if ($step) {
                $next_step = -1;
                $this->what = $step;

                // Call method
                $method  = $step['method'];
                $this->$method();
                // Next step
                if (isset($this->steps[(int)Tools::getValue('step')+1])) {
                    $next_step = (int)Tools::getValue('step') + 1;
                }
                // Percent calculation
                $progress_percent = round((((int)Tools::getValue('step') + 1) * 100)/count($this->steps), 2);

                // Generate report
                Context::getContext()->smarty->assign(array(
                    'what' => $step['step'],
                    'has_output' => $this->has_output,
                    'is_auto_repair' => $this->auto_repair,
                    'results' => $this->results,
                ));
                Context::getContext()->smarty->assign($step);
                $html = $configuration->getModule()->display(
                    _PS_MODULE_DIR_.$configuration->getModule()->name,
                    $this->presenter->partial_path_view.'_partials/diagnostic/results_diagnostic.tpl'
                );
                die(
                    json_encode(array(
                        'what' => $step['step'],
                        'next_step' => $next_step,
                        'progress_percent' => $progress_percent,
                        'target' => (isset($step['outputs']['target']) ? $step['outputs']['target'] : 'technical'),
                        'has_output' => $this->has_output,
                        'html' => $html,
                        'step_definitions' => $step
                    ))
                );
            }
        }
        die();
    }

    /**
     * Execute.
     *
     * @return void
     */
    public function executeAction()
    {
        if (Tools::isSubmit('what')) {
            $this->presenter->noRender();
            $configuration = LinevenApoConfiguration::getConfiguration();
            $step = $this->getStep(Tools::getValue('what'));
            if ($step) {
                $diagnostic = new LinevenApoAdminDiagnostic($step['step']);
                $diagnostic->execute();
                $this->is_success = $diagnostic->isSuccess();
                Context::getContext()->smarty->assign(
                    array(
                        'what' => $step['step'],
                        'is_success' => $this->is_success,
                    )
                );
                Context::getContext()->smarty->assign($step);
                die(
                    json_encode(array(
                        'html' => $configuration->getModule()->display(
                            _PS_MODULE_DIR_ . $configuration->getModule()->name,
                            $this->presenter->partial_path_view . '_partials/diagnostic/results_execution.tpl'
                        ),
                        'is_success' => $this->is_success,
                        'is_success_required' => (isset($step['is_success_required']) ? $step['is_success_required'] : false),
                        'target' => (isset($step['outputs']['target']) ? $step['outputs']['target'] : 'technical')
                    ))
                );
            }
            die();
        }
    }

    /**
     * Final.
     *
     * @return void
     */
    public function finalAction()
    {
        $this->presenter->noRender();
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (!($configuration->version == Configuration::get($configuration->getPrefixConfigurationModule().'LAST_VERSION'))) {
            Configuration::updateGlobalValue(
                $configuration->getPrefixConfigurationModule().'PREVIOUS_VERSION',
                Configuration::get($configuration->getPrefixConfigurationModule().'LAST_VERSION')
            );
            Configuration::updateGlobalValue(
                $configuration->getPrefixConfigurationModule().'LAST_VERSION',
                $configuration->version
            );
            if (!$this->module_context->isTestModeActive()) {
                $configuration->setTestModeActive(true);
                if ($this->module_context->getTestModeListIps(false) == '') {
                    $configuration->setTestModeListIps($_SERVER['REMOTE_ADDR']);
                }
            }
        }
        die();
    }

    /*
     * Database table backup.
     *
     * @return array
     */
    protected function checkDatabaseTablesToBackup()
    {
        $sql = str_replace('PREFIX_', _DB_PREFIX_, _LINEVEN_MODULE_APO_DATABASE_PREFIX_);
        $results = Db::getInstance()->ExecuteS('SHOW TABLES IN `'._DB_NAME_.'` LIKE "'.$sql. '%"');
        $count = 0;
        if (is_array($results) && count($results)) {
            foreach ($results as $result) {
                foreach ($result as $table) {
                    if (Tools::strpos($table, '_ar_bck') === false) {
                        $count++;
                    }
                }
            }
        }
        $this->has_output = ($count ? true : false);
        $this->results = $count;
    }

    /*
     * Check hooks.
     *
     * @return array
     */
    private function checkHooks()
    {
        $hooks = LinevenApoModuleDiagnostic::checkHooks();
        if ($hooks != null) {
            $this->has_output = true;
            $this->results = $hooks;
        }
    }

    /*
     * Check files.
     *
     * @return array
     */
    private function checkFiles()
    {
        $files = LinevenApoModuleDiagnostic::checkFiles();
        if ($files != null) {
            $this->has_output = true;
            $this->results = $files;
        }
    }

    /*
     * Check folders security.
     *
     * @return array
     */
    private function checkFoldersSecurity()
    {
        $folders = array();
        LinevenApoModuleDiagnostic::checkFoldersSecurity($folders);
        if (count($folders)) {
            $this->has_output = true;
            $this->results = $folders;
        }
    }

    /*
     * Check database tables exists.
     *
     * @return array
     */
    private function checkDatabaseTablesExists()
    {
        $db = new LinevenApoDbTools();
        $tables = $db->isTableExist();
        if (count($tables)) {
            $this->has_output = true;
            $this->results = $tables;
        }
    }

    /*
     * Check columns in database tables.
     *
     * @return array
     */
    private function checkDatabaseColumnsExists()
    {
        $db = new LinevenApoDbTools();
        $tables = $db->isColumnsTablesExist();
        if (count($tables)) {
            $this->has_output = true;
            $this->results = $tables;
        }
    }

    /*
     * Check columns in database tables.
     *
     * @return array
     */
    private function checkDefaultConfiguration()
    {
        $configuration_missing = LinevenApoModuleDiagnostic::checkDefaultConfiguration();
        if (count($configuration_missing)) {
            $this->has_output = true;
            $this->results = count($configuration_missing);
        }
    }

    /**
     * Set steps
     *
     * @return void
     */
    public function setSteps()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->steps = array(
            array(
                'step' => 'hooks',
                'method' => 'checkHooks',
                'operation' => 'repair',
                'level' => 'warning',
                'is_success_required' => false,
                'repair' => array(
                    'is_automatic' => false,
                ),
                'outputs' => array(
                    'checking' => array(
                        'progression_text' => $this->translator->l('Check hooks', 'Diagnostic'),
                        'error_message' => $this->translator->l('hooks seems not be associated with the module :', 'Diagnostic'),
                        'button' => $this->translator->l('Repair hooks', 'Diagnostic')
                    ),
                    'repair' => array(
                        'error_message' => $this->translator->l('Please check hooks manually to fixed them.', 'Diagnostic')
                    )
                )
            ),
            array(
                'step' => 'configuration_datas',
                'method' => 'checkDefaultConfiguration',
                'operation' => 'repair',
                'level' => 'warning',
                'is_success_required' => true,
                'repair' => array(
                    'is_automatic' => true,
                ),
                'outputs' => array(
                    'checking' => array(
                        'progression_text' => $this->translator->l('Check default configuration', 'Diagnostic'),
                        'error_message' => $this->translator->l('default configuration datas are missing.', 'Diagnostic'),
                        'button' => $this->translator->l('Repair configuration', 'Diagnostic')
                    )
                )
            ),
            array(
                'step' => 'files',
                'method' => 'checkFiles',
                'operation' => 'repair',
                'level' => 'critical',
                'is_success_required' => true,
                'repair' => array(
                    'is_automatic' => false,
                ),
                'outputs' => array(
                    'checking' => array(
                        'progression_text' => $this->translator->l('Check files', 'Diagnostic'),
                        'error_message' => $this->translator->l('files or folders in the module appear to be missing :', 'Diagnostic'),
                        'repair_message' => $this->translator->l('Do not uninstall the module.', 'Diagnostic').
                            $this->translator->l('You can upload the module in the same version to copy the missing files.', 'Diagnostic'),
                    )
                )
            ),
            array(
                'step' => 'folders_security',
                'method' => 'checkFoldersSecurity',
                'operation' => 'repair',
                'level' => 'warning',
                'is_success_required' => false,
                'repair' => array(
                    'is_automatic' => true,
                ),
                'outputs' => array(
                    'checking' => array(
                        'progression_text' => $this->translator->l('Check security in folders (index.php)', 'Diagnostic'),
                        'error_message' => $this->translator->l('index.php files are missing in those folders :', 'Diagnostic'),
                        'button' => $this->translator->l('Repair index.php files', 'Diagnostic')
                    ),
                    'repair' => array(
                        'error_message' => $this->translator->l('Please check folders manually to add index.php files into.', 'Diagnostic')
                    )
                )
            )
        );
        if ($configuration->getParameter('is_install_files')) {
            $this->steps[] = array(
                'step' => 'tables_exists',
                'method' => 'checkDatabaseTablesExists',
                'operation' => 'repair',
                'level' => 'critical',
                'is_success_required' => true,
                'repair' => array(
                    'is_automatic' => true,
                ),
                'outputs' => array(
                    'checking' => array(
                        'progression_text' => $this->translator->l('Check database tables exists', 'Diagnostic'),
                        'error_message' => $this->translator->l('database tables for the module are missing :', 'Diagnostic'),
                        'repair_message' => $this->translator->l('Do not uninstall the module. You can repair missing tables.', 'Diagnostic'),
                        'button' => $this->translator->l('Create missing tables', 'Diagnostic')
                    ),
                    'repair' => array(
                        'error_message' => $this->translator->l('Please check tables manually in your database.', 'Diagnostic')
                    )
                )
            );
            $this->steps[] = array(
                'step' => 'columns_exists',
                'method' => 'checkDatabaseColumnsExists',
                'operation' => 'repair',
                'level' => 'critical',
                'is_success_required' => true,
                'repair' => array(
                    'is_automatic' => true,
                ),
                'outputs' => array(
                    'checking' => array(
                        'progression_text' => $this->translator->l('Check columns in database tables', 'Diagnostic'),
                        'error_message' => $this->translator->l('database tables have missing columns :', 'Diagnostic'),
                        'repair_message' => $this->translator->l('Do not uninstall the module. You can repair missing columns.', 'Diagnostic'),
                        'button' => $this->translator->l('Add missing columns in tables', 'Diagnostic')
                    ),
                    'repair' => array(
                        'error_message' => $this->translator->l('Please check columns manually in tables of your database.', 'Diagnostic')
                    )
                )
            );
        }
        if ($this->auto_repair) {
            $steps = array();
            if (Tools::isSubmit('is_tables_must_backuped') && (int)Tools::getValue('is_tables_must_backuped')) {
                $steps[] = array(
                    'step' => 'database_tables_backup',
                    'method' => 'checkDatabaseTablesToBackup',
                    'operation' => 'action',
                    'level' => 'information',
                    'is_success_required' => true,
                    'outputs' => array(
                        'checking' => array(
                            'progression_text' => $this->translator->l('Check module tables to backup', 'Diagnostic'),
                            'information_message' => $this->translator->l('module database tables to backup', 'Diagnostic'),
                            'action_message' => $this->translator->l('Backup module tables...', 'Diagnostic'),
                            'result_message' => $this->translator->l('Module tables are backuped.', 'Diagnostic'),
                        )
                    )
                );
            }
            $this->steps = array_merge($steps, $this->steps);
        }
    }

    /**
     * get step
     * @param int|string $step Step (by id or name)
     * @return array
     */
    protected function getStep($step)
    {
        if (!count($this->steps)) {
            $this->setSteps();
        }
        if (is_int($step)) {
            return (isset($this->steps[$step]) ? $this->steps[$step] : false);
        } else {
            foreach ($this->steps as $step_def) {
                if ($step == $step_def['step']) {
                    return $step_def;
                }
            }
        }
        return false;
    }
}
