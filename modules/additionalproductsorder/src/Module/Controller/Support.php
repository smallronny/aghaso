<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleSupportController extends LinevenApoController
{
    /**
     * Test mode action.
     *
     * @return void
     */
    public function testmodeAction()
    {
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper = new LinevenApoHelperForm('Support', 'testmode', 'support:testmode');
        $helper->form = new LinevenApoFormAdminSupportTestMode();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your test mode settings have been updated.', 'Support')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_content, $helper->generateForm());
    }

    /**
     * Changelog action.
     *
     * @return void
     */
    public function changelogAction()
    {
        // Get changelog
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $is_file_exists = false;
        $language = 'en';
        $current_language = $module_context->current_language;
        $changelog = '';
        if (file_exists(
            _PS_MODULE_DIR_.'/'.$configuration->name.
            '/upgrade/whatsnew/whatsnew_'.$current_language['iso_code'].'.html'
        )) {
            $is_file_exists = true;
            $language = $current_language['iso_code'];
        } else {
            if (file_exists(
                _PS_MODULE_DIR_.'/'.$configuration->name.
                '/upgrade/whatsnew/whatsnew_'.$language.'.html'
            )) {
                $is_file_exists = true;
            }
        }
        if ($is_file_exists) {
            $file = _PS_MODULE_DIR_.'/'.$configuration->name.'/upgrade/whatsnew/whatsnew_'.$language.'.html';
            $changelog = Tools::file_get_contents($file);
        }
        $this->presenter->setDatas(array(
            'changelog' => $changelog
        ));
        // Template
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_path_template, './_partials/support/changelog');
    }

    /**
     * Informations action.
     *
     * @return void
     */
    public function informationsAction()
    {
        // Get changelog
        $configuration = LinevenApoConfiguration::getConfiguration();
        $db = new LinevenApoDbTools();
        $this->presenter->setDatas(array(
            'tables_definition' => $db->getTablesDefinitions(),
            'hooks_used' => $configuration->getCurrentHooksDefinition()
        ));
        if (is_dir(_PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/css/')) {
            $this->presenter->addDatas(array(
                'assets_css_directory' => _LINEVEN_MODULE_APO_DIRNAME_.'/views/css/',
                'assets_css' => glob(_PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/css/*.css')
            ));
        }
        if (is_dir(_PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/js/')) {
            $this->presenter->addDatas(array(
                'assets_js_directory' => _LINEVEN_MODULE_APO_DIRNAME_.'/views/js/',
                'assets_js' => glob(_PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/views/js/*.js')
            ));
        }
        $this->presenter->addOutput(
            LinevenApoPresenter::$output_type_path_template,
            './_partials/support/informations'
        );
    }

    /**
     * Troubleshooting action.
     *
     * @return void
     */
    public function troubleshootingAction()
    {
        $presenter = new LinevenApoModuleAdminPresenter();
        $helper = new LinevenApoHelperForm('Support', 'troubleshooting', 'support:troubleshooting');
        $helper->form = new LinevenApoFormAdminSupportTroubleshooting();
        if (LinevenApoForm::isSubmit()) {
            $helper->form->populateFromRequest();
            if ($helper->form->validate()) {
                $helper->form->updateSettings();
                $presenter->setConfirmationMessage(
                    $this->translator->l('Your troubleshooting settings have been updated.', 'Support')
                );
            } else {
                $presenter->setErrorMessage($helper->form->getErrors());
            }
        } else {
            $helper->form->populateFromConfigurationDatasValue();
        }
        $this->presenter->setDatas($presenter->present());
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_content, $helper->generateForm());
    }
}
