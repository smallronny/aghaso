<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleUpgradeController extends LinevenApoController
{

    /**
     * Upgrade module action.
     *
     * @return void
     */
    public function upgradeModuleAction()
    {
        $this->presenter->noRender();
        $return = array(
            'is_upgrades' => false,
            'is_upgrades_complete' => true,
            'upgrades_count' => 0,
            'upgrades_step_progress' => 0,
            'version' => ''
        );
        if (LinevenApoModuleUpgrade::isExtraUpgrade()) {
            $upgrades = LinevenApoModuleUpgrade::getUpgrades();
            $version = $upgrades[0];
            $return = array(
                'is_upgrades' => true,
                'is_upgrades_complete' => false,
                'upgrades_count' => count($upgrades),
                'upgrades_step_progress' => round(100/count($upgrades), 2),
                'version' => $version
            );
        }
        die(json_encode($return));
    }

    /**
     * Launch action.
     *
     * @return void
     */
    public function upgradeVersionAction()
    {
        $this->presenter->noRender();
        if (Tools::isSubmit('version') && Tools::getValue('version') != '' &&
            file_exists(_PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/upgrade/install-'.
            Tools::getValue('version').'.php')) {
            require(_PS_MODULE_DIR_ . _LINEVEN_MODULE_APO_DIRNAME_ . '/upgrade/install-' .
                Tools::getValue('version') . '.php');
            $function_name = 'extra_upgrade_module_'.str_replace('.', '_', Tools::getValue('version'));
            if (function_exists($function_name)) {
                call_user_func($function_name);
            }
        }
        die();
    }
}
