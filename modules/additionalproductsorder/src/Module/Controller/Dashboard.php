<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleDashboardController extends LinevenApoController
{
    /**
     * Index action.
     * @return void
     */
    public function indexAction()
    {
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();

        // Warning Smarty
        $panel_warning = $configuration->isSmartyTemplatesChanged();
        $panel_warning_smarty = false;
        if ($panel_warning&& Configuration::get('PS_SMARTY_FORCE_COMPILE') == 0) {
            $panel_warning_smarty = true;
        }

        // Alerts
        $alerts = LinevenApoAdminAlerts::getAlerts();

        // Variables to the view
        $panel_update_display = Configuration::get($configuration->getPrefixConfigurationModule().'PANEL_UPD');
        $is_demonstration_datas_feasible = false;
        if ($configuration->getParameter('is_demonstration_datas_available') &&
            $configuration->getParameter('demonstration_datas_class') !== false) {
            $demonstration_class = $configuration->getParameter('demonstration_datas_class');
            if (in_array($demonstration_class, get_declared_classes()) &&
                $demonstration_class::isDemonstrationDatasInstallFeasible()) {
                $is_demonstration_datas_feasible = true;
            }
        }

        // Get update informations content
        $panel_update_content = '';
        if ($panel_update_display) {
            $language = 'en';
            $current_language = $module_context->current_language;
            if (file_exists(
                _PS_MODULE_DIR_.'/'.$configuration->name.
                '/upgrade/whatsnew/whatsnew_'.$current_language['iso_code'].'.html'
            )) {
                $language = $current_language['iso_code'];
            }
            $file = _PS_MODULE_DIR_.'/'.$configuration->name.'/upgrade/whatsnew/whatsnew_'.$language.'.html';
            $panel_update_content = Tools::file_get_contents($file);
        }
        
        // Notifications
        $this->loadNotifications();


        $this->presenter->addDatas(array(
            'panel_warning' => $panel_warning ,
            'panel_warning_smarty ' => $panel_warning_smarty,
            'panel_warning_configuration_name' => $configuration->getPrefixConfigurationModule().'PANEL_WARNING',
            'alerts' => $alerts,
            'count_alerts' => count($alerts),
            'is_alerts' => (count($alerts) > 0 ? true : false),
            'panel_info_display' => Configuration::get($configuration->getPrefixConfigurationModule().'PANEL_INFOS'),
            'panel_info_configuration_name' => $configuration->getPrefixConfigurationModule().'PANEL_INFOS',
            'panel_update_display' => $panel_update_display,
            'panel_update_configuration_name' => $configuration->getPrefixConfigurationModule().'PANEL_UPD',
            'is_demonstration_reset_available' => $configuration->getParameter('is_demonstration_reset_available'),
            'is_demonstration_datas_available' => $configuration->getParameter('is_demonstration_datas_available'),
            'is_demonstration_datas_feasible' => $is_demonstration_datas_feasible,
            'panel_demo_datas_display' => Configuration::get($configuration->getPrefixConfigurationModule().'PANEL_DEMO_DATAS'),
            'panel_demo_datas_configuration_name' => $configuration->getPrefixConfigurationModule().'PANEL_DEMO_DATAS',
            'prestashop_addons_link' => $configuration->getPrestashopAddonsLink(),
            'panel_update_content' => $panel_update_content,
        ));

        // Template
        $this->presenter->addOutput(LinevenApoPresenter::$output_type_default_template);
    }

    /**
     * Demonstration datas installation action.
     *
     * @return void
     */
    public function demonstrationDatasInstallAction()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->presenter->noRender();
        if ($configuration->getParameter('is_demonstration_datas_available') &&
            $configuration->getParameter('demonstration_datas_class') !== false) {
            $demonstration_class = $configuration->getParameter('demonstration_datas_class');
            if (in_array($demonstration_class, get_declared_classes())) {
                $demonstration_class::installDatas();
                die(
                    Tools::jsonEncode(array('return' => true))
                );
            }
        }
        die(
            Tools::jsonEncode(array('return' => false))
        );
    }

    /**
     * Demonstration datas reset action.
     *
     * @return void
     */
    public function demonstrationDatasResetAction()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->presenter->noRender();
        if ($this->module_context->getEnvironment(false) == LinevenApoContext::$environment_demonstration &&
            $configuration->getParameter('is_demonstration_reset_available')) {
            include(_PS_ROOT_DIR_.'/cron.php');
            die(
                Tools::jsonEncode(array('return' => true))
            );
        }
        die(
            Tools::jsonEncode(array('return' => false))
        );
    }

    /**
     * Toggle status.
     *
     * @return void
     */
    public function toggleStatusAction()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $return = array(
            'link' => Tools::getValue('link'),
            'value' => '',
            'reload' => false
        );
        $this->presenter->noRender();
        if (Tools::isSubmit('link')) {
            switch (Tools::getValue('link')) {
                case 'active_module':
                    $configuration->getModule()->enableState();
                    $return['value'] = (int)$this->module_context->isModuleActive();
                    $return['reload'] = true;
                    break;
                case 'test_mode':
                    $configuration->getModule()->toggleTestMode();
                    $return['value'] = (int)$this->module_context->isTestModeActive();
                    $return['list_ips'] = $this->module_context->getTestModeListIps(false);
                    $return['reload'] = true;
                    break;
                case 'debug_mode':
                    $configuration->getModule()->toggleDebugMode();
                    $return['value'] = (int)$this->module_context->isDebugModeActive();
                    $return['reload'] = true;
                    break;
            }
            die(
                Tools::jsonEncode($return)
            );
        }
    }
    
    /**
     * Close panel.
     *
     * @return void
     */
    public function closePanelAction()
    {
        if (Tools::isSubmit('panel_code')) {
            if (Tools::getValue('panel_code') == 'panel_warning'
                || Tools::getValue('panel_code') == 'panel_demo_datas') {
                Configuration::updateValue(Tools::getValue('configuration_name'), 0);
            } else {
                Configuration::updateGlobalValue(Tools::getValue('configuration_name'), 0);
            }
            $this->no_layout = true;
            echo 'var panel_code = "'.Tools::getValue('panel_code').'";';
            echo 'var value = 1;';
            die();
        }
    }
    
    /**
     * Load notifications.
     *
     * @return void
     */
    private function loadNotifications()
    {
        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $notifications_message_for_upgrade = false;
        $notifications_upgrade_content = false;
        $notifications_image_box_language = '';
        if (Configuration::get($configuration->getPrefixConfigurationModule().'NOTIFICATION_UPD') == 1) {
            Configuration::updateGlobalValue($configuration->getPrefixConfigurationModule().'NOTIFICATION_UPD', 0);
            $notifications_message_for_upgrade = true;
            // Get update informations content
            $language = 'en';
            $current_language = $module_context->current_language;
            if (file_exists(
                _PS_MODULE_DIR_.'/'.$configuration->name.
                '/upgrade/whatsnew/whatsnew_'.$current_language['iso_code'].
                '.html'
            )) {
                $language = $current_language['iso_code'];
            }
            $file = _PS_MODULE_DIR_.'/'.$configuration->name.'/upgrade/whatsnew/whatsnew_'.$language.'.html';
            $notifications_upgrade_content = Tools::file_get_contents($file);
            $notifications_image_box_language = $language;
        }
        $notifications_message_for_installation = false;
        if (Configuration::get($configuration->getPrefixConfigurationModule().'NOTIFICATION_INST') == 1) {
            Configuration::updateGlobalValue($configuration->getPrefixConfigurationModule().'NOTIFICATION_INST', 0);
            $notifications_message_for_installation = true;
            $language = 'en';
            $current_language = $module_context->current_language;
            if (file_exists(
                _PS_MODULE_DIR_.'/'.$configuration->name.
                '/views/img/box/box_'.$current_language['iso_code'].
                '.png'
            )) {
                $language = $current_language['iso_code'];
            }
            $notifications_image_box_language = $language;
        }
        $notifications_message_for_demonstration = false;
        if ($this->module_context->getEnvironment(false) == LinevenApoContext::$environment_demonstration) {
            $demonstration_message_cookie = new Cookie('lineven_demonstration_cookie', '', 0);
            if ($configuration->getParameter('is_demonstration_message')) {
                if (!($demonstration_message_cookie->exists() && $demonstration_message_cookie->is_message_displayed)) {
                    $notifications_message_for_demonstration = true;
                    $demonstration_message_cookie->is_message_displayed = true;
                }
            }
        }
        $this->presenter->addData(
            'notifications',
            array(
                'for_demonstration' =>  $notifications_message_for_demonstration,
                'for_upgrade' => $notifications_message_for_upgrade,
                'for_extra_upgrade' => LinevenApoModuleUpgrade::isExtraUpgrade(),
                'upgrade_content' => $notifications_upgrade_content,
                'for_installation' => $notifications_message_for_installation,
                'image_box_language' => $notifications_image_box_language,
                'for_automatic_repair' => !($configuration->version == Configuration::get($configuration->getPrefixConfigurationModule().'LAST_VERSION'))
            )
        );
    }
}
