<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleUpgrade
{
    /**
     * Get upgrades.
     */
    public static function getUpgrades()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') !== false &&
            Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') != null) {
            return unserialize(Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE'));
        }
        return false;
    }

    /**
     * Is extra upgrade.
     */
    public static function isExtraUpgrade()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') !== false &&
            Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') != null) {
            if (self::getUpgrades()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add version.
     * @param string $version Version
     */
    public static function addVersion($version)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $upgrade_versions = array();
        if (Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') !== false &&
            Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') != null) {
            $upgrade_versions = unserialize(Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE'));
            $upgrade_versions[] = $version;
        }
        $upgrade_versions[] = $version;
        Configuration::updateGlobalValue($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE', serialize($upgrade_versions));
    }

    /**
     * Delete version.
     * @param string $version Version
     */
    public static function deleteVersion($version)
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        if (Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') !== false &&
            Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE') != null) {
            $upgrade_versions = unserialize(Configuration::get($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE'));
            if (($key = array_search($version, $upgrade_versions)) !== false) {
                unset($upgrade_versions[$key]);
            }
            if (count($upgrade_versions)) {
                Configuration::updateGlobalValue($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE', serialize($upgrade_versions));
            } else {
                self::clear();
            }
        }
    }

    /**
     * Clear all version upgrade.
     * @param string $version Version
     */
    public static function clear()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        Configuration::updateGlobalValue($configuration->getPrefixConfigurationModule().'EXTRA_UPGRADE', null);
    }
}
