<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleDiagnostic
{
    protected $what;
    protected $is_success;

    /**
     * Constructor.
     * @param string $what What
     */
    public function __construct($what)
    {
        $this->what = $what;
        $this->is_success = true;
    }

    /**
     * Execute.
     *
     * @return boolean
     */
    public function execute()
    {
        switch ($this->what) {
            case 'database_tables_backup':
                $this->is_success = LinevenApoModuleDiagnostic::backupDatabaseTables();
                break;
            case 'hooks':
                $this->is_success = LinevenApoModule::repairHooks();
                break;
            case 'configuration_datas':
                $this->is_success = LinevenApoModuleDiagnostic::repairDefaultConfiguration();
                break;
            case 'folders_security':
                $this->is_success = LinevenApoModuleDiagnostic::repairFoldersSecurity();
                break;
            case 'tables_exists':
                $db = new LinevenApoDbTools();
                $this->is_success = $db->installTables();
                break;
            case 'columns_exists':
                $db = new LinevenApoDbTools();
                $this->is_success = $db->repairTablesColumns();
                break;
        }
        return $this->is_success;
    }

    /**
     * Is success.
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->is_success;
    }

    /**
     * Check hooks.
     *
     * @return array
     */
    public static function checkHooks()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $items = array_merge(
            $configuration->isHooksRegistred(true),
            LinevenApoAdminDiagnostic::checkHooksByConfiguration()
        );
        if (count($items) != 0) {
            return $items;
        }
        return array();
    }

    /**
     * Check hooks.
     *
     * @return array
     */
    public static function checkFiles()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $files = require_once($configuration->getDirectoryPath().'/config/files.inc.php');
        $results = array();
        if (count($files) != 0) {
            foreach ($files as $file) {
                $file_to_check = $file['relative_path'];
                $is_ok = true;
                if ($file['type'] == 'dir') {
                    if (!is_dir(_PS_MODULE_DIR_.$file_to_check)) {
                        $is_ok = false;
                    }
                } else {
                    $file_to_check .= $file['name'];
                    if (!file_exists(_PS_MODULE_DIR_.$file_to_check)) {
                        $is_ok = false;
                    }
                }
                if (!$is_ok) {
                    $results[] = $file_to_check;
                }
            }
            return $results;
        }
        return array();
    }

    /**
     * Check folders security.
     *
     * @param array $return Return folders in error
     * @param string $folder Folder input
     * @param string $relative_path Relative path for recursivity
     * @return array
     */
    public static function checkFoldersSecurity(&$return, $folder = '', $relative_path = '')
    {
        if ($folder == '') {
            $folder = _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_;
        }
        $filetype = filetype($folder);
        $basename = basename($folder);
        if ($filetype == 'dir') {
            $relative_path .= $basename.'/';
            if (!is_file($folder.'/index.php')) {
                $return[] = $relative_path;
            }
            $me = opendir($folder);
            while ($child = readdir($me)) {
                if ($child != '.' && $child != '..' && is_dir($folder.DIRECTORY_SEPARATOR.$child)) {
                    LinevenApoModuleDiagnostic::checkFoldersSecurity(
                        $return,
                        $folder.DIRECTORY_SEPARATOR.$child,
                        $relative_path
                    );
                }
            }
        }
    }

    /**
     * Check default configuration datas.
     *
     * @return boolean
     */
    public static function checkDefaultConfiguration()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        $configuration_missing = array();
        foreach ($configuration->getCurrentDatasDefinition() as $name => $value) {
            $check = true;
            if (!$configuration->getParameter('is_backoffice_menu_available') &&
                $name == $configuration->getPrefixConfigurationModule().'ACTIVE_BO_MENU') {
                $check = false;
            }
            if ($check &&
                !(Configuration::get($name) !== false
                    || Configuration::hasKey($name) || Configuration::isLangKey($name))) {
                $configuration_missing[] = $name;
            }
        }
        return $configuration_missing;
    }

    /**
     * Backup database module tables.
     *
     * @return array
     */
    public static function backupDatabaseTables()
    {
        $db = new LinevenApoDbTools();
        return $db->backupTables();
    }

    /**
     * Repair folders security.
     *
     * @return array
     */
    public static function repairFoldersSecurity()
    {
        $folders = array();
        LinevenApoModuleDiagnostic::checkFoldersSecurity($folders);
        $source_file = _PS_MODULE_DIR_._LINEVEN_MODULE_APO_DIRNAME_.'/index.php';
        if (!is_file($source_file)) {
            return false;
        }
        if (count($folders)) {
            foreach ($folders as $folder) {
                if (!copy(
                    $source_file,
                    _PS_MODULE_DIR_.$folder.'/index.php'
                )) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Repair default configuration data.
     *
     * @return array
     */
    public static function repairDefaultConfiguration()
    {
        $configuration = LinevenApoConfiguration::getConfiguration();
        foreach ($configuration->getCurrentDatasDefinition() as $name => $value) {
            if (Configuration::get($name) === false) {
                if ($value['global']) {
                    Configuration::updateGlobalValue($name, $value['default_value'], $value['html']);
                } else {
                    Configuration::updateValue($name, $value['default_value'], $value['html']);
                }
            }
        }
        return true;
    }
}
