<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleFormSupportTestMode extends LinevenApoForm
{
    /**
     * Init.
     *
     * @return void
     */
    public function init()
    {

        $module_context = LinevenApoContext::getContext();
        $configuration = LinevenApoConfiguration::getConfiguration();
        $this->fields[0] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->translator->l('Test mode', 'SupportTestMode'),
                    'icon' => 'fa fa-moon-o'
                ),
                'description_multilines' => array(
                    $this->translator->l('You can test your settings without impact for your customers.', 'SupportTestMode'),
                    $this->translator->l('When test mode is enabled, the module is automatically disabled for your customers.', 'SupportTestMode')
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->translator->l('Activate test mode', 'SupportTestMode'),
                        'name' => 'active_testmode',
                        'configuration_name' => $configuration->getPrefixConfigurationModule().'TESTMODE_ACTIVE',
                        'is_bool' => true,
                        'value' => $module_context->isTestModeActive(),
                        'values' => array(
                            array(
                                'id' => 'active_testmode_on',
                                'value' => 1,
                                'label' => $this->translator->l('Yes', 'SupportTestMode')
                            ),
                            array(
                                'id' => 'active_testmode_off',
                                'value' => 0,
                                'label' => $this->translator->l('No', 'SupportTestMode')
                            ),
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->translator->l('Your IP address', 'SupportTestMode'),
                        'name' => 'testmode_ip',
                        'configuration_name' => $configuration->getPrefixConfigurationModule().'TESTMODE_IP',
                        'required' => true,
                        'value' => $module_context->getTestModeListIps(),
                        'class' => 'fixed-width-ls',
                        'maxlength' => 255,
                        'hint' => $this->translator->l('If you activate test mode, this value is required.', 'SupportTestMode'),
                        'desc' => $this->translator->l('Enter the IP addresses separated by commas.', 'SupportTestMode')
                    )
                ),
                'buttons' => array(
                    'add_ip_adress' => array(
                        'title' => $this->translator->l('Get my IP', 'SupportTestMode'),
                        'class' => 'pull-right',
                        'icon' => 'process-icon-plus',
                        'href' => 'javascript:LinevenApo.Tools.getIPAddress(\''.$_SERVER['REMOTE_ADDR'].'\')'
                    )
                ),
                'submit' => array(
                    'title' => $this->translator->l('Save', 'SupportTestMode')
                )
            ),
        );
    }

    /**
     * Validate.
     *
     * @return boolean
     */
    public function validate()
    {
        if ((int) Tools::getValue('active_testmode') == 1 &&
            Tools::strlen(trim(Tools::getValue('testmode_ip'))) == 0) {
            $description = $this->translator->l('You must give your IP address for testing.', 'SupportTestMode').' ';
            $description .= $this->translator->l('Without IP address, the test mode is inactive.', 'SupportTestMode');
            $this->addError(
                $description,
                'testmode_ip'
            );
        }
        return parent::validate();
    }
}
