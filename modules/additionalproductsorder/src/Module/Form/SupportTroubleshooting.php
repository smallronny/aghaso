<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleFormSupportTroubleshooting extends LinevenApoForm
{
    /**
     * Init.
     * @return void
     */
    public function init()
    {
    }
}
