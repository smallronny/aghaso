<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleAlerts
{
    /**
     * Check for alerts.
     *
     * @return array
     */
    public static function getAlerts()
    {
        $module_context = LinevenApoContext::getContext();
        $translator = new LinevenApoTranslator();
        $return = array();
        $alert = array();
        
        // Is Test mode active
        if ($module_context->isTestModeActive()) {
            $alert['icon'] = 'fa fa-moon-o';
            $alert['message'] =
                $translator->l('The test mode is active. Click to inactive.', 'Alerts');
            $alert['color'] = '#D2A63C';
            $alert['on_click'] = 'LinevenApo.Tools.toggleStatus(\'test_mode\')';
            $return[] = $alert;
        } else {
            if (!$module_context->isModuleActive()) {
                $alert['icon'] = 'fa fa-times-circle';
                $alert['message'] =
                    $translator->l('The module is disabled. Click to active.', 'Alerts');
                $alert['color'] = '#D27C82';
                $alert['on_click'] = 'LinevenApo.Tools.toggleStatus(\'active_module\')';
                $return[] = $alert;
            }
        }

        // Check hooks
        $items = LinevenApoModuleDiagnostic::checkHooks();
        if (count($items) != 0) {
            $alert['icon'] = 'fa fa-exclamation-triangle';
            $alert['message'] =
            $translator->l('This module does not seem to be associated to hooks : ', 'Alerts');
            $alert['items'] = $items;
            $alert['label_fix'] = $translator->l('Click here to repair', 'Alerts');
            $alert['on_click'] = 'LinevenApo.Diagnostic.execute(\'hooks\', true)';
            $return[] = $alert;
        }
        
        return $return;
    }
}
