<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleAdminListingSearch
{
    protected $results;
    protected $count;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        $this->results = array();
        $this->count = 0;
    }

    /**
     * Get results.
     * @return array
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Get count.
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}
