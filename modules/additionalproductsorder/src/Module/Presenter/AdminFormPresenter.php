<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleAdminFormPresenter extends LinevenApoModuleAdminPresenter
{
    protected $show_cancel_button;
    protected $cancel_url;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->cancel_url = '';
        $this->show_cancel_button = false;
    }

    /**
     * Set cancel url.
     * @param string $url Cancel url
     * @return void
     */
    public function setCancelUrl($url)
    {
        $this->cancel_url = $url;
        if ($url != '') {
            $this->setShowCancelButton(true);
        }
    }

    /**
     * Set show cancel button.
     * @param string $url Cancel url
     * @return void
     */
    public function setShowCancelButton($allow)
    {
        $this->show_cancel_button = $allow;
    }

    /**
     * Present.
     * @return array
     */
    public function present()
    {
        parent::present();
        $this->presenter['cancel_url'] = $this->cancel_url;
        $this->presenter['show_cancel_button'] = $this->show_cancel_button;
        return $this->presenter;
    }
}
