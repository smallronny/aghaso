<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleAdminListingPresenter extends LinevenApoModuleAdminPresenter
{
    protected $auhorized_order;
    protected $subtitle_informations_message;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->auhorized_order = false;
        $this->subtitle_informations_message = false;
    }

    /**
     * Set authorized sort.
     * @param boolean $allow Allow sort
     * @return void
     */
    public function setAuthorizedSort($allow)
    {
        $this->auhorized_order = $allow;
    }

    /**
     * Set subtitles informations message.
     * @param string $message Message
     * @return void
     */
    public function setSubtitleInformationsMessage($message)
    {
        $this->subtitle_informations_message = $message;
    }

    /**
     * Present.
     * @return array
     */
    public function present()
    {
        parent::present();
        $this->presenter['authorized_order'] = $this->auhorized_order;
        $this->presenter['subtitle_informations_message'] = $this->subtitle_informations_message;
        return $this->presenter;
    }
}
