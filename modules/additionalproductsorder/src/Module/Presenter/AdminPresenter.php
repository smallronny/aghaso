<?php
/**
 * Library for Lineven Prestashop Modules (Version 4.2.0)
 *
 * @author    Lineven
 * @copyright 2012-2020 Lineven
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of Lineven
 */

class LinevenApoModuleAdminPresenter
{
    protected $confirmation_message;
    protected $warning_message;
    protected $error_message;
    protected $message_box;
    protected $translator;

    public static $message_type_confirmation = 'CNF';
    public static $message_type_warning = 'WRN';
    public static $message_type_error = 'ERR';

    protected $presenter;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        $this->translator = new LinevenApoTranslator();
        $this->confirmation_message = '';
        $this->warning_message = '';
        $this->error_message = '';
        $this->message_box = false;
        $this->presenter = array();
    }

    /**
     * Set message.
     * @param array|string $message Message
     * @return void
     */
    public function setMessage($message, $type = 'ERR')
    {
        // Display actions messages
        if (is_array($message)) {
            if (isset($message['action_return']) && isset($message['action_return']['message'])) {
                if (!isset($message['action_return']['type']) ||
                    ((isset($message['action_return']['type'])
                        && $message['action_return']['type'] == self::$message_type_confirmation))) {
                    $this->setConfirmationMessage($message['action_return']['message']);
                } elseif (isset($message['action_return']['type'])
                    && $message['action_return']['type'] == self::$message_type_warning) {
                    $this->setWarningMessage($message['action_return']['message']);
                } elseif (isset($message['action_return']['type'])
                    && $message['action_return']['type'] == self::$message_type_error) {
                    $this->setErrorMessage($message['action_return']['message']);
                }
            }
        } else {
            switch ($type) {
                case self::$message_type_error:
                    $this->setErrorMessage($message);
                    break;
                case self::$message_type_warning:
                    $this->setWarningMessage($message);
                    break;
                default:
                    $this->setConfirmationMessage($message);
                    break;
            }
        }
    }

    /**
     * Set confirmation message.
     * @param array|string $message Message
     * @return void
     */
    public function setConfirmationMessage($message)
    {
        // Display actions messages
        if (is_array($message)) {
            if (isset($message['action_return']) && isset($message['action_return']['message'])) {
                $this->confirmation_message = $message['action_return']['message'];
            }
        } else {
            $this->confirmation_message = $message;
        }
    }

    /**
     * Set warning message.
     * @param string $message Message
     * @return void
     */
    public function setWarningMessage($message)
    {
        $this->warning_message = $message;
    }

    /**
     * Set error message.
     * @param string $message Message
     * @return void
     */
    public function setErrorMessage($message)
    {
        $this->error_message = $message;
    }

    /**
     * Set message box.
     * @param string $title Title
     * @param string $content Content
     * @param boolean $is_template Is template
     * @return void
     */
    public function setMessageBox($title, $content, $is_template = false)
    {
        if ($is_template) {
            $configuration = LinevenApoConfiguration::getConfiguration();
            $content = $configuration->getModule()->display(
                _PS_MODULE_DIR_.$configuration->getModule()->name,
                'views/templates/admin/module/'.$content.'.tpl'
            );
        }
        $this->message_box = array('title' => $title, 'content' => $content);
    }

    /**
     * Present.
     * @return array
     */
    public function present()
    {
        $this->presenter = array(
            'message_confirmation' => $this->confirmation_message,
            'message_warning' => $this->warning_message,
            'message_form_errors' => $this->error_message,
            'message_box' => $this->message_box
        );
        return $this->presenter;
    }
}
